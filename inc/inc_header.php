<?php
include_once "./inc/inc_header_js.php";
?>

<!-- <script type="text/javascript" src="./js/jquery.blockUI.js"></script> -->

<!-- 공통 메시지 정의 -->
<script type="text/javascript" src="./js/dv_layerPopup.js"></script>
<script type="text/javascript" src="./js/pagination/pagination.js"></script>

<script>

// date picker
$(this).click(function(){
	setup_datepicker();
});
setTimeout(function(){
	setup_datepicker();
	// 모든 datepicker input box 읽기 전용으로 설정 - 팝업처럼 액션이 발생할때 생성되는 date는 해당 코드에서 아래 코드를 추가해 줘야한다.
	// --> 입력되게 해 달라는 요청으로 주석처리, 2016.05.10
	// $('.datepicker').attr('readonly', 'readonly');
}, 100);

// 전역변수 - 로컬여부
var is_local = "<?php echo is_local(); ?>";

//============================================================================================
// page
var _page = 1;

// 페이징 객체
var _pagination = undefined;

// 한 페이지에 보여지는 row 개수
var _itemPerPage = "<?php echo CFG_BACK_PAGINATION_ITEM_PER_PAGE?>";

// set pagination config
var _cfg_pagination = {
	total_item: 1, // 총 게시물 개수
	itemPerPage: _itemPerPage, // 리스트 목록수
	prevIcon: './images/icons/prev.png',	// 이전페이지 아이콘
	nextIcon: './images/icons/next.png',	// 다음페이지 아이콘
	firstIcon: './images/icons/prev02.png', // 첫페이지로 아이콘
	lastIcon : './images/icons/next02.png', // 마지막페이지 아이콘
};
//============================================================================================

//============================================================================================
// 레이어팝업 id
var gLayerId = undefined;

//============================================================================================
var is_master = "<?php echo $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);?>";

// 선택된 대메뉴 id
var sel_menu = "<?php echo $data['selected_menu']; ?>";
var temp = sel_menu.split('_');
// 선택된 대메뉴의 서브메뉴 index
var sel_sub_menu = temp[2];
sel_menu = temp[0] +'_'+ temp[1];

$(document).ready(function(){
	
//		$('a.cls_menu').each(i, function(e){
//			if($(this).attr('data-role-code') == temp
//		});

	// 마우스가 메뉴를 나갔을때 선택된 메뉴 show 처리
	$('#lnb').mouseleave(function(){
		$('ul.lnb_2depth').each(function(index){
			if($(this).attr('id') == sel_menu && $(this).css('display') != 'block') {
				hide_all_submenu();
				$(this).css('display', 'block');
			}
		});
	});
	
	// selected default menu
	$('ul.lnb_2depth').each(function(index){
		if($(this).attr('id') == sel_menu) {
			$(this).css('display', 'block');
			// 선택된 메뉴의 아이콘 및 색상을 선택된 상태로 변경
			$(this).prev().addClass('selected').children().addClass('selected');
			// sub menu 처리
			$(this).find('a').each(function(i){
				if($(this).attr('data-role-index') == sel_sub_menu) {
					unselected_all_submenu();
// 					$(this).css('font-weight', 'bold');
					$(this).addClass('cmm_selected');
				}
			});
		}
	});
	
	// select menu event
	$('a.cls_menu').click(function(e){
		e.preventDefault();
		// 통계 외 메뉴인 경우
		var p = $(this).parents('ul').attr('id');
		var param = '';
		// - 상담,권리구제 메뉴인 경우 메뉴 클릭시 검색 세션 데이터를 초기화하기 위해 구분자를 넘긴다.
		if(p.indexOf('lnb01') != -1 || p.indexOf('lnb07') != -1) {
			param = 'menu';
		}

		// 메뉴 클릭시 쿠키에 저장한 검색데이터 삭제, 2018.08.27
		$.removeCookie("laborsSearchDataCounsel");
		$.removeCookie("laborsSearchDataLawhelp");
		$.removeCookie("laborsSearchDataStatis");
		$.removeCookie("laborsSearchDataCounselBiz");
		$.removeCookie("laborsSearchDataOper");
		
		check_auth_redirect($(this), param);
	});
	
	// 비밀번호변경 팝업
	$('#chgPassword').click(function(e){
		e.preventDefault();
		var url = '/?c=admin&m=chg_pwd_view';
		gLayerId = openLayerModalPopup(url, 450, 330, '', '', '', '', 1, true);
	});
		
	// 비밀번호변경 팝업 - 관리자가 operator이 비번 변경시 사용
	$('#btnChgOperPwd').click(function(e){
		e.preventDefault();
		var url = '/?c=admin&m=chg_pwd_view&ref=oper&tid='+ $(this).attr('data-tid');
		gLayerId = openLayerModalPopup(url, 450, 330, '', '', '', '', 1, true);
	});
	
	// logout event
	$('#logout').click(function(e){
		e.preventDefault();
		location.href = "/?c=admin&m=logout";
	});
	
	// mouse over event when over lnb menu
	$('li.m_lnb').mouseover(function(e){
		e.preventDefault();
		hide_all_submenu();
		$(this).find('ul').css('display', 'block');
	});
	
});

/**
  * tab 메뉴 전환 - 통계 화면에서만 사용함
  */
function go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst) {
	var jqObj = $('#'+ sel_menu).find('li>a.cmm_selected').eq(0);
	check_auth_redirect(jqObj, index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
}

/**
 * 메뉴id 생성 및 페이지 전환
 * 메뉴 전환시 권한체크와 세션처리를 하기 때문에 반드시 페이지 내부 링크도 권한 체크해야하는 페이지인 경우 이 함수를 통해야 한다.
 * param : 두번째,세번째 파라메터는 해당 메뉴의 수정/삭제/리스트 등 페이지 전환용으로 사용된다.
 */
function check_auth_redirect(jqObj) {
	var p = jqObj.parents('ul').attr('id');

	var menu_id = Base64.encode(Aes.Ctr.encrypt(p +'_'+ jqObj.attr('data-role-index'), "<?php echo CFG_ENCRYPT_KEY; ?>", 256));
	var code = jqObj.attr('data-role-code');

	// 권한등록 - 수정/보기 링크 호출 시 예외 처리
	var append_params = '';
	
	//통계 메뉴에서만 사용
	if(p == 'lnb02_menu01'){
		if(arguments[1]) {// 통계메뉴 탭 index
			append_params = '&kind='+ arguments[1];
		}
		else {// 기본, 통계 첫번째 탭 지정
			append_params = '&kind=1';
		}
		if(arguments[2]) {
			append_params += '&begin='+ arguments[2];
		}
		if(arguments[3]) {
			append_params +='&end='+ arguments[3];
		}
		if(arguments[4]) {
			append_params +='&sch_asso_code='+ arguments[4];
		}
		if(arguments[5]) {
			append_params +='&sch_s_code='+ arguments[5];
		}
		if(arguments[6]) {
			append_params +='&sch_csl_proc_rst='+ arguments[6];
		}
	}
	//통계 외 메뉴에서 사용
	else{
		if(arguments[1]) {
			// kind가 edit인 경우만 argument2가 필요하다. add는 id값이 없어도 된다.
			append_params = '&kind='+ arguments[1];
		}
		if(arguments[2]) {
			append_params += '&id='+ arguments[2];
		}
		if(arguments[3]) {
			// 게시판은 3번째 파라메터가 필요하다. 3번째는 게시물 seq 임
			append_params +='&seq='+ arguments[3];
		}
		if(arguments[4]) {
// 			alert("arguments[4] : "+ arguments[4]);
			// 내용보기에서 목록으로 전환시 page data
			append_params +='&page='+ arguments[4];
		}
	}

	// 권한체크 후 이동할 url
	var url = '/?c=admin&m=main&menu='+ menu_id +'&code='+ code + append_params;
	
	// 권한체크
	// - master 제외
	if(is_master == 1 || arguments[1] == 'view') { // 권리구제 내역 뷰 권한 예외처리(kind가 view로 넘어온다.)
		location.href = url;
	}
	else {
		$.ajax({
			url: '/?c=auth&m=req_auth'
			,data: { c: code }
			,cache: false
			,async: false
			,method: 'post'
			,dataType: 'json'
			,success: function(data) {
				if(data.rst == 'succ') {
					location.href = url;
				}
				else if(data.rst == 'session_timeout') {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_09']);
					location.href = '/?c=admin';
				}
				else {
					var msg = CFG_MSG[CFG_LOCALE]['info_auth_01'];
					if(data.msg) msg += '[' + data.msg +']';
					alert(msg);
				}
			}
			,error: function(data) {
				if(is_local) objectPrint(data);
			}
		});
	}
}

// 서브메뉴 모두 숨김
function hide_all_submenu() {
	$('ul.lnb_2depth').css('display', 'none');
}

// 선택된 서브메뉴 모드 해제
function unselected_all_submenu() {
	$('#lnb>ul ul li>a').css('font-weight', '');
}

//  discuss at: http://phpjs.org/functions/bin2hex/
// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// bugfixed by: Onno Marsman
// bugfixed by: Linuxworld
// improved by: ntoniazzi (http://phpjs.org/functions/bin2hex:361#comment_177616)
//   example 1: bin2hex('Kev');
//   returns 1: '4b6576'
//   example 2: bin2hex(String.fromCharCode(0x00));
//   returns 2: '00'
function bin2hex(s) {
	var i, l, o = '', n;
	s += '';
	for (i = 0, l = s.length; i < l; i++) {
		n = s.charCodeAt(i)
		.toString(16);
		o += n.length < 2 ? '0' + n : n;
	}
	return o;
}

// binding to datepicker input tag
function setup_datepicker(oDate) {
	var t = '.datepicker';
	var op = {};
	var settings = {
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dayNamesMin: ['일','월','화','수','목','금','토'],
		weekHeader: 'Wk',
		currentText: '오늘',
		closeText: '닫기',
		changeMonth: true, // 월변경가능
		changeYear: true, // 년변경가능
		dateFormat: 'yy-mm-dd',
		yearRange: '2015:c',
		showButtonPanel: true, // today,close 버튼 패널
		showOtherMonths: true // 나머지 날짜도 화면에 표시, 선택은 불가
		,showMonthAfterYear: true // 년,월 select box 위치 설정
		,onSelect: function(dateText){
			// 선택일의 주 선택처리
			if( $(this).hasClass('applyDayOfWeek') ) {
				var seldate = $(this).datepicker('getDate');
				seldate = seldate.toDateString();
				seldate = seldate.split(' ');
				var weekday=new Array();
				weekday['Mon']="1";
				weekday['Tue']="2";
				weekday['Wed']="3";
				weekday['Thu']="4";
				weekday['Fri']="5";
				weekday['Sat']="6";
				weekday['Sun']="0";
				$(this).next().find('>option[value="'+ weekday[seldate[0]] +'"]').attr('selected', true);
			}
	    }
	    , minDate: op.minDate || ''
	    , maxDate: op.maxDate || ''
	    , onClose: op.onClose || ''
	    , beforeShow: op.beforeShow || ''
	};
	if(oDate) {
		t = oDate.date;
		op = oDate.option || {};
		$.extend(true, settings, op);
	}
	$(t).datepicker(settings);
	// console.log(settings);
	$('select.ui-datepicker-year').css("height", "29px").css('padding', '0 4px'); // select year 높이 조절
	$('select.ui-datepicker-month').css("height", "29px").css('padding', '0 4px'); // select month 높이 조절
	$('a.ui-datepicker-next').css("margin-top", "4px"); // 다음버튼
	$('a.ui-datepicker-prev').css("margin-top", "4px"); // 이전버튼
}


(function (){
	/*
	 * 검색 : 상담내역조회, 주요상담사례, 권리구제내역조회, 통계 - 월별 조회
	 */
	var root = this;
	var gen_two_digit = function(data) {
		if(data.toString().length == 1)
			return '0' + data;
		else 
			return data;
	};
	var get_last_day = function(yy,mm) {
		var rst;
		if(mm == "04" || mm == "06" || mm == "09" || mm == "11"){
				rst = "30";
			}
		else if(mm == "02"){
			if(yy % 4 == 0){
				rst = "29";
			}
			else{
				rst = "28";
			}
		}
		else{
			rst = "31";
		}
		return rst;
	};
	var fill_list_search_dates = function(select_month, default_this_year) {
		var year = $('#sel_year').val();
		var now = new Date();
		var date_begin = '';
		var date_end = '';

		if(select_month) {
			if(year == '') {
				year = now.getFullYear();
				$('#sel_year').val(year).prop('selected', true);
			}
			date_begin = year +"-"+ select_month + "-01";
			date_end = year +"-"+ select_month +"-"+ get_last_day(year, select_month);
		}
		else {
			if(default_this_year || year) {
				var mm1 = '01',
					mm2 = '12',
					dd = '31';
				// 통계에서 호출한 경우 : 상담사례 - 당월 기간으로 설정한다.
				if(default_this_year == 1 && !year) {
					year = now.getFullYear();
					mm1 = mm2 = gen_two_digit(now.getMonth() + 1);
					dd = get_last_day(year, mm2);
				}
				// 통계에서 호출한 경우 : 기본통계,교차통계 - 당월 기간으로 설정한다.
				else if(default_this_year == 2 && !year) {
					year = now.getFullYear();
					mm2 = gen_two_digit(now.getMonth() + 1);
					dd = gen_two_digit(now.getDate());
				}
				date_begin = year +"-"+ mm1 +"-01";
				date_end = year +"-"+ mm2 +"-"+ dd;
			}
		}
		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
	};

	root.fill_list_search_dates = fill_list_search_dates;
}());

/**
 * 접속자 브라우저가 ie인지 체크 <br>
 * ie ? true : false
 * 
 * @return boolean
 */
function isIE() {
	var isIE = false || !!document.documentMode; // Internet Explorer 6-11
	var isEdge = !isIE && !!window.StyleMedia; // Edge 20+
	
	return isIE || isEdge;
}
</script>