
</head>

<body>
    <div id="top">
        <a href="#main-nav" id="top01">메인메뉴 바로가기</a>
        <a href="#contents" id="top02">컨텐츠 바로가기</a>
    </div>    
   
	<header id="header">
		<div class="head">
			<h1 id="logo">
				<a href="/"><img src="/images/common/logo.png" alt="서울노동권익센터 로고"></a>
			</h1>				
			<div class="top-nav">					
				<div class="user-nav">
					<ul>
						<li><a href="#" id="chgPassword">비밀번호변경</a></li>
						<li><a href="#" id="logout">로그아웃</a></li>
					</ul>
				</div>
			</div>
			<div class="top-name">
				<span class="fBold marginR10 icon_member"><?php echo $data[CFG_SESSION_ADMIN_NAME] ?></span> 님
			</div>
		</div>			
	</header>
	<div id="contents">
		<!-- lnb 메뉴 area -->
		<div id="lnb">
			<ul>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_COUNSEL_MANAGE); ?>">
					<a href="#" id="lnb01"><span class="icon_menu01 imgM"></span>상담내역 관리</a>
					<ul id="lnb01_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S002'); ?>">- 상담내역입력</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S001'); ?>">- 상담내역조회</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_BIZ_COUNSEL); ?>">
					<a href="#" id="lnb08"><span class="icon_menu01 imgM"></span>사용자상담 관리</a>
					<ul id="lnb08_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S038'); ?>">- 사용자상담입력</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S037'); ?>">- 사용자상담조회</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_COUNSEL_STATISTIC); ?>">
					<a href="#" id="lnb02"><span class="icon_menu02 imgM"></span>통계</a>
					<ul id="lnb02_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S003'); ?>">- 상담내역 통계</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S047'); ?>">- 사용자상담 통계</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_LAW_HELP); ?>">
					<a href="#" id="lnb07"><span class="icon_menu01 imgM"></span>권리구제지원 관리</a>
					<ul id="lnb07_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S029'); ?>">- 권리구제내역 입력</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S030'); ?>">- 권리구제내역 조회</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_BOARD); ?>">
					<a href="#" id="lnb03"><span class="icon_menu03 imgM"></span>게시판</a>
					<ul id="lnb03_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S005'); ?>">- 운영게시판</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S006'); ?>">- 주요상담사례</a></li>
						<li><a href="#" class="cls_menu" data-role-index="3" data-role-code="<?php echo base64_encode('S039'); ?>">- 사용자상담사례</a></li>
						<li><a href="#" class="cls_menu" data-role-index="4" data-role-code="<?php echo base64_encode('S024'); ?>">- FAQ</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_OPERATOR); ?>">
					<a href="#" id="lnb04"><span class="icon_menu04 imgM"></span>운영자관리</a>
					<ul id="lnb04_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S007'); ?>">- 운영자조회</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S008'); ?>">- 운영자등록</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_AUTH); ?>">
					<a href="#" id="lnb05"><span class="icon_menu05 imgM"></span>권한관리</a>
					<ul id="lnb05_menu01" class="lnb_2depth" style="display:none">
						<li><a href="#" class="cls_menu" data-role-index="1" data-role-code="<?php echo base64_encode('S009'); ?>">- 권한조회</a></li>
						<li><a href="#" class="cls_menu" data-role-index="2" data-role-code="<?php echo base64_encode('S010'); ?>">- 권한등록</a></li>
					</ul>
				</li>
				<li class="m_lnb" data-role-code="<?php echo base64_encode(CFG_MASTER_CODE_MANAGE_CODE); ?>">
					<a href="#" id="lnb06"><span class="icon_menu06 imgM"></span>코드관리</a>
					<ul id="lnb06_menu01" class="lnb_2depth" style="height:500px;display:none;overflow-y:auto;overflow-x:hidden;">
					<?php
					$index = 0;
					if(isset($data['lnb_code']) && is_array($data['lnb_code'])) {
						foreach($data['lnb_code'] as $rs) {
							echo '<li><a href="#" class="cls_menu" data-role-index="'. (++$index) .'" data-role-code="'. base64_encode($rs->s_code) .'">- '. $rs->scode_name .'</a></li>';
						}
					}
					?>
					</ul>
				</li>
			</ul>
		</div>
		<!-- //lnb 메뉴 area -->		