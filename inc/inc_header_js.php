<?php
$u = $_SERVER['HTTP_USER_AGENT'];
$is_IE  = (bool)preg_match('/msie/i', $u );
// $isIE7  = (bool)preg_match('/msie 7./i', $u );
// $isIE8  = (bool)preg_match('/msie 8./i', $u );
$isIE9  = (bool)preg_match('/msie 9./i', $u );
// $isIE10 = (bool)preg_match('/msie 10./i', $u );

?>
<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow" />
	<meta name="googlebot" content="noindex, nofollow, noimageindex nosnippet" />
	<title>서울시통합노동상담관리시스템 관리자</title>
    <?php
    // ie9만 호환성보기 관련처리(ie8 렌더링엔진 적용)
    if($isIE9):
    ?>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=8"> -->
    <meta http-equiv="X-UA-Compatible" content="IE=9">
    <?php
    else:
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--, chrome=1"-->
    <?php
    endif;
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <link href="./css/admin-style.css" rel="stylesheet" type="text/css">

     <link href="./css/jquery-ui-1.11.2.min.css" rel="stylesheet" type="text/css">
    <!--<link rel="stylesheet" href="./css/jquery-ui.theme.min.css" /> -->
		<link href="./css/dv_loading.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script type="text/javascript" src="./js/html5shiv.js"></script>
        <script type="text/javascript" src="./js/html5shiv.printshiv.js"></script>
    <![endif]-->
    <?php
    // ie9만 qjuery2.x 로 변경
    if($isIE9) echo '<script type="text/javascript" src="./js/jquery-2.1.4.min.js"></script>'. PHP_EOL;
    else echo '<script type="text/javascript" src="./js/jquery.1.9.1.min.js"></script>'. PHP_EOL;
    ?>
    <script type="text/javascript" src="./js/jquery-ui.1.9.2.min.js"></script>
    <script type="text/javascript" src="./js/jquery.cookie.js"></script>
	
	<!-- common -->
	<script type="text/javascript" src="./js/ai_common.js"></script>
	<script type="text/javascript" src="./js/ai_aes.js"></script>
	<script type="text/javascript" src="./js/ai_cmm_msg.js"></script>
	<script type="text/javascript" src="./js/html2canvas.js"></script>
	
	<!-- for pqGrid -->
	<!--
	<link rel="stylesheet" type="text/css" href="./css/pqgrid.min.css">
	<script type="text/javascript" src="./js/pqgrid.min.js"></script>
	-->
	