
<!-- 코드설명용 레이어팝업 codeDesc : BEGIN -->
<div class="codeDescLayerPop pop-layer" style="display:none;">
	<div class="pop-container">
		<div class="pop-inner-container">
			<p class="pop-content"></p>
		</div>
	</div>
	<div class="btn-container">
		<div class="no-more-see">
			<input type="checkbox" id="enoughShow"><label for="enoughShow">다시 보지 않기</label>
		</div>
		<div class="btn-right"><a href="#" class="closeCodeDescPopup">Close</a></div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// 페이지 이름(view)
	var pageNm = "<?php echo $_ci_view;?>";

	var isInPlace = false, intervalId = undefined;
	// 코드설명팝업위로 mouseenter시 닫히지 않도록 한다. mouseleave시 닫음
	$('.codeDescLayerPop').hover(function(){
		isInPlace = true;
		clearInterval(intervalId);
	}	,function(){
		isInPlace = false;
		closeDelayPopup();
	});
	// 코드설명팝업 닫기 함수, delay 3초
	// 마우스가 코드설명팝업 위에 있으면 닫히지 않게 한다.
	var closeDelayPopup = function(){
		// 다시보지않기 체크한 경우 
		if($.cookie("laborsShowCodeDesc") == 1) {
			return;
		}
		intervalId = setInterval(function(){
			if(isInPlace == true) return;
			$('.codeDescLayerPop').fadeOut(30, function(){
				$('.codeDescLayerPop .pop-content').html('');
			});
		}, 3000);
	};
	// mouse enter 이벤트 핸들러
	// 이벤트 발생시 닫기 이벤트 핸들러가 실행중이면 종료시킨다.
	// 코드그룹별로 팝업 위치 조절
	var mouseenterHandler = function(e){
		e.preventDefault();
		// 다시보지않기 체크한 경우 팝업 오픈하지 않도록한다.
		if($.cookie("laborsShowCodeDesc") == 1) {
			return;
		}
		isInPlace = true;
		clearInterval(intervalId);
		$('.codeDescLayerPop').fadeIn(100, function(){
			$('.codeDescLayerPop .pop-content').html( $(e.currentTarget).children('textarea').val() );
		});
		// 고용형태 코드인 경우 위치 조절
		var pText = $(e.currentTarget).closest('tr').find('>th').text();
		var pTop = '450px';
		if(pageNm.indexOf('biz_') != -1) {//사용자상담인 경우
			pTop = '410px';
		}
		$('.codeDescLayerPop').css('top', pTop);
	};
	$('.openCodeDescPopup').on('mouseenter', mouseenterHandler);
	$('.openCodeDescPopup').on('mouseleave', function(e) {
		e.preventDefault();
		closeDelayPopup();
		isInPlace = false;
	});
	// 닫기 버튼
	$('.closeCodeDescPopup').on('click', function(e) {
		e.preventDefault();
		clearInterval(intervalId);
		isInPlace = false;
		$('.codeDescLayerPop').fadeOut(20, function(){
			$('.codeDescLayerPop .pop-content').html('');
		});
		var btnBgColor = '#eaeaea';
		if($('#enoughShow').is(':checked')) {
			btnBgColor = '#dae1ff';
		}
		$('.cssShowCodeDescPopup').css('background', btnBgColor);
		$.cookie("laborsShowCodeDesc", $('#enoughShow').is(':checked') ? 1 : 0);
	});
	if($.cookie("laborsShowCodeDesc") == 1) {
		$('.cssShowCodeDescPopup').css('background', '#dae1ff');
	}
	
	// 다시보지않기 체크 선택시 처리
	$(this).on('change', '.codeDescLayerPop #enoughShow', function(e){
		$.cookie("laborsShowCodeDesc", $(this).is(':checked') ? 1 : 0);
	});
	// 코드설명팝업 보기 설정 버튼 클릭시 처리
	$('.cssShowCodeDescPopup').click(function(e){
		e.preventDefault();
		clearInterval(intervalId);
		isInPlace = false;
		$.cookie("laborsShowCodeDesc", 0);
		$('#enoughShow').prop('checked', false);
		$(this).css('background', '#eaeaea');
	});
});
</script>
<!-- 코드설명용 레이어팝업 codeDesc : END -->
