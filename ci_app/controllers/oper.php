<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// admin Class
require_once 'admin.php';



/******************************************************************************************************
 * 운영자 Class
 * Oper
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Oper extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

    //========================================================================
    // construct
    //========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');
	}
	
	
	
	
    //########################################################################
    // Oper
    //########################################################################
	
	//========================================================================
    // index
    //========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// id_check : id 중복체크
	//-----------------------------------------------------------------------------------------------------------------
	public function id_check() {
		$param = $this->input->post(NULL, TRUE);
		$data = array(
			'oper_id' => $param['oid']
		);
		
		$rstRtn['data'] = $this->manager->id_check($data);
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit : 실제 DB 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function edit() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		// update
		$data = array(
			's_code' => $param['sltS_Code']
			, 'oper_auth_grp_id' => $param['sltGrpId']
			, 'oper_name' => $param['txtOperName']
			, 'oper_kind' => $param['oper_kind']
			, 'oper_kind_sub' => $param['oper_kind_sub'] // 2016.08.09 추가 - 옴부즈만, 자치구공무원인 경우 소속 자치구 정보
			, 'hp' => $param['sltHP01'] .'-'. $param['sltHP02'] .'-'. $param['sltHP03']
			, 'tel' => $param['sltTel01'] .'-'. $param['sltTel02'] .'-'. $param['sltTel03']
			, 'use_yn' => $param['rdoUse']
			, 'etc' => $param['txaEtc']
			, 'oper_pwd' => $param['txtPwd']
			, 'oper_id' => $param['txtId']
			, 'is_master' => $param['is_master']
		);
		if(isset($param['txtEmail'])) {
			$data['email'] = $param['txtEmail'];
		}
		
		// echof($data);
		// exit;
		
		$rst = $this->manager->edit_oper($data);
		
		$rstRtn['data'] = $rst;
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// auth : 실제 DB 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function add() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		$data = array(
			's_code' => $param['sltS_Code']
			, 'oper_auth_grp_id' => $param['sltGrpId']
			, 'oper_name' => $param['txtOperName']
			, 'oper_kind' => $param['oper_kind']
			, 'oper_kind_sub' => $param['oper_kind_sub'] // 2016.08.09 추가 - 옴부즈만, 자치구공무원인 경우 소속 자치구 정보
			, 'hp' => $param['sltHP01'] .'-'. $param['sltHP02'] .'-'. $param['sltHP03']
			, 'tel' => $param['sltTel01'] .'-'. $param['sltTel02'] .'-'. $param['sltTel03']
			, 'use_yn' => $param['rdoUse']
			, 'etc' => $param['txaEtc']
			, 'oper_pwd' => dv_hash($param['txtPwd'], CFG_ENCRYPT_KEY)
			, 'oper_id' => $param['txtId']
		);
		if(isset($param['txtEmail'])) {
			$data['email'] = $param['txtEmail'];
		}
		
		$rst = $this->manager->add_oper($data);
		
		$rstRtn['data'] = $rst;
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// oper_list : 운영자 LIST
	//-----------------------------------------------------------------------------------------------------------------
	public function oper_list($rtn_data=FALSE) {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// use
		$param = $this->input->post(NULL, TRUE);

		$use_yn = $param['rdoUse'];
		if($use_yn == 2) $use_yn = 0;
		
		// is_master
		$is_master = isset($param['is_master']) && $param['is_master'] ? $param['is_master'] : 0;

		// keyword
		$keyword = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// search_target
		$target = isset($param['search_target']) && $param['search_target'] ? $param['search_target'] : '';

		// search date
		$search_date_begin = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';
		$search_date_end = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';
		
		// search sltS_Code - 소속코드
		$search_s_code = isset($param['sltS_Code']) && $param['sltS_Code'] ? base64_decode($param['sltS_Code']) : '';
		// search sltGrpId - 권한코드
		$search_grp_id = isset($param['sltGrpId']) && $param['sltGrpId'] ? base64_decode($param['sltGrpId']) : '';

		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
			, 'use_yn' => $use_yn
		);
	
		$where['is_master'] = $is_master;

		$where['keyword'] = $keyword;
		
		$where['target'] = $target;
				
		$where['search_date_begin'] = $search_date_begin;
		
		$where['search_date_end'] = $search_date_end;
				
		$where['s_code'] = $search_s_code;
		
		$where['oper_auth_grp_id'] = $search_grp_id;
		
		$rst = $this->manager->get_oper_list($where);
		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
// 		$rstRtn['data']['query'] = $rst['query'];
		
		// 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
		}
		else {
			$this->load->view('json_view', $rstRtn);
		}
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del : oper 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		$data['oper_id'] = $param['oper_id'];
		
		$rst = $this->manager->del_oper($data);
		
		if($rst['rst'] == 'exist') {
			$rstRtn['data']['rst'] = $rst['rst'];
			$this->load->view('json_view', $rstRtn);
		}
		else {
			// return a list data
			$rstRtn = self::oper_list(TRUE);
			$rstRtn['data']['rst'] = $rst['rst'];
			$this->load->view('json_view', $rstRtn);
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// using : 권한그룹 사용처리
	//-----------------------------------------------------------------------------------------------------------------
	public function using() {
		self::_use_or_not_and_multi_del(1);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// not_using : 권한그룹 사용중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function not_using() {
		self::_use_or_not_and_multi_del(0);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 권한그룹 사용중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
		self::_use_or_not_and_multi_del(2);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _use_or_not_and_multi_del - 다중 사용,중지,삭제
	//-----------------------------------------------------------------------------------------------------------------
	private function _use_or_not_and_multi_del($args) {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		// del
		$len = 0;
		$rst = '';
		if($args == 2) {
			$rst_tmp1 = $this->manager->del_multi_oper($param['oper_id']);
			if( !empty($rst_tmp1)) {
				$rst_tmp2 = explode(CFG_AUTH_CODE_DELIMITER, $rst_tmp1);
				$rst = 'exist';
				$len = count($rst_tmp2);
			}
		}
		else {
			$data['use_yn'] = $args;
			$data['oper_id'] = $param['oper_id'];
			$rst = $this->manager->use_or_not_use_oper($data);
		}
		
		// return a list data
		$rstRtn = self::oper_list(TRUE);
		$rstRtn['data']['rst'] = $rst;
		$rstRtn['data']['len'] = $len;
		$this->load->view('json_view', $rstRtn);
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_base_code : base code 가져오기
	//-----------------------------------------------------------------------------------------------------------------
	public function get_base_code() {
		
		// 소속코드 코드값 가져오기
		$rstRtn['asso_code'] = $this->manager->get_oper_asso_all_list('s_code,code_name');

		// 권한그룹 데이터 가져오기
		$rstRtn['grp_data'] = $this->manager->get_auth_grp_all_list('oper_auth_grp_id,oper_auth_name');
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_edit_view_data : 등록/수정 뷰화면 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function get_edit_view_data($args=NULL) {
		if(is_null($args)) {
			$param = $this->input->get(NULL, TRUE);
		}
		else {
			$param['kind'] = $args['kind'];
			$param['id'] = $args['id'];
		}
		
		// base code
		$base_code = self::get_base_code();
		$rstRtn['data']['grp_data'] = $base_code['grp_data'];
		$rstRtn['data']['asso_code'] = $base_code['asso_code'];
		
		
		// 처리 구분 : add,view,edit
		$rstRtn['data']['kind'] = $param['kind'];
		
		// setting
		$rstRtn['data']['oper_id'] = '';
		$rstRtn['data']['oper_name_1'] = '';
		$rstRtn['data']['s_code'] = '';
		$rstRtn['data']['code_name'] = '';
		$rstRtn['data']['oper_auth_grp_id'] = '';
		$rstRtn['data']['oper_auth_name'] = '';
		$rstRtn['data']['oper_kind'] = '';
		$rstRtn['data']['oper_kind_sub'] = '';
		$rstRtn['data']['tel'] = '';
		$rstRtn['data']['hp'] = '';
		$rstRtn['data']['email'] = '';
		$rstRtn['data']['use_yn'] = '-1';
		$rstRtn['data']['etc'] = '';
		$rstRtn['data']['reg_date'] = '';
		
		// edit
		if($param['kind'] == 'edit') {
			$data = array(
				'oper_id' => $param['id']
			);
			$rst = $this->manager->get_oper($data);
			
			$rstRtn['data']['oper_id'] = $rst['oper_id'];
			$rstRtn['data']['oper_name_1'] = $rst['oper_name'];
			$rstRtn['data']['s_code'] = $rst['s_code'];
			$rstRtn['data']['code_name'] = $rst['code_name'];
			$rstRtn['data']['oper_auth_grp_id'] = $rst['oper_auth_grp_id'];
			$rstRtn['data']['oper_auth_name'] = $rst['oper_auth_name'];
			$rstRtn['data']['oper_kind'] = $rst['oper_kind'];
			$rstRtn['data']['oper_kind_sub'] = $rst['oper_kind_sub'];
			$rstRtn['data']['tel'] = $rst['tel'];
			$rstRtn['data']['hp'] = $rst['hp'];
			$rstRtn['data']['email'] = $rst['email'];
			$rstRtn['data']['use_yn'] = $rst['use_yn'];
			$rstRtn['data']['etc'] = $rst['etc'];
			$rstRtn['data']['reg_date'] = $rst['reg_date'];
		}
		
		if(is_null($args)) {
			$this->load->view('adm_oper_edit_view', $rstRtn);
		}
		else {
			return $rstRtn['data'];
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		$admin = new Admin();
		$out = $admin->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				$rstRtn['data']['data'] = 'session_timeout';
				return $admin->logout();
			}
		}
		else {
			return $out;
		}
	}
}
