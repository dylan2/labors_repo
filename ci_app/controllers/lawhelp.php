<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// AES encrypt library
require_once './lib/aes.class.php';
require_once './lib/aesctr.class.php'; 

// ref Class
require_once 'admin.php';
// require_once 'auth.php';



/******************************************************************************************************
 * 권리구제지원 Class
 * Lawhelp
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Lawhelp extends Admin {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

	//=================================================================================================================
	// construct
	//=================================================================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_lawhelp', 'manager');

		// helper
		// $this->load->helper('download');
	}
	
	
	
	
	//#################################################################################################################
	// Lawhelp
	//#################################################################################################################

	//=================================================================================================================
	// index
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}


	//-----------------------------------------------------------------------------------------------------------------
	// vdownload_excel : 상담 내역 Excel download view
	//-----------------------------------------------------------------------------------------------------------------
	public function vdownload_excel() {

		$param = $this->input->get(NULL, TRUE);
		
		// 다운로드 구분 - counsel_list, lawhelp_list<권리구제 리스트>
		$data['kind'] = $param['kind'];

		$this->load->view('adm_excel_down_popup_view', $data);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// download - 상담,권리구제 전용 엑셀 다운로드 함수
	//-----------------------------------------------------------------------------------------------------------------
	public function download() {

		$param = $this->input->post(NULL, TRUE);
		$kind = $param['kind'];

		// data 가져오기
		$rstRtn = $this->excel_data(array('kind'=>$kind));

		// excel 인스턴스 생성
		require_once 'excel.php';
		$excel = new Excel();
		$excel->download2(array('data'=>$rstRtn['data'], 'kind'=>$kind));

	}



	//-----------------------------------------------------------------------------------------------------------------
	// lawhelp_print : 상담 내역 인쇄 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function lawhelp_print() {

		$get = $this->input->get(NULL, TRUE);
		$seq = $get['seq'];

		$data = array(
			'seq' => $seq
		);
		$rstRtn = $this->manager->get_lawhelp_print($data);
		// echof($rstRtn['data']);


		$this->load->view('print_form/print_legalsupport', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// add_lid_date : 출석조사일 추가
	//-----------------------------------------------------------------------------------------------------------------
	public function add_lid_date() {

		$params = $this->input->post(NULL, TRUE);
		$seq = $params['seq'];
		$lid_date = $params['lid_date'];

		$data = array(
			'seq' => $seq
			,'lid_date' => $lid_date
		);
		$rstRtn['data'] = $this->manager->add_lawhelp_invtgt_date($data);
		// echof($rstRtn['data']);

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// del_lid_date : 출석조사일 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_lid_date() {

		$params = $this->input->post(NULL, TRUE);
		$lid_seq = $params['lid_seq'];

		$data = array(
			'lid_seq' => $lid_seq
		);
		$rstRtn['data'] = $this->manager->del_lid_date($data);
		// echof($rstRtn['data']);

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// chk_code : 상담 고유코드 중복여부 체크
	//-----------------------------------------------------------------------------------------------------------------
	public function chk_code() {

		$params = $this->input->post(NULL, TRUE);
		$code = $params['code'];

		$data = array(
			'lh_code' => $code
		);
		$rstRtn['data'] = $this->manager->chk_code($data);
		// echof($rstRtn['data']);

		$this->load->view('json_view', $rstRtn);
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// lawhelp_list : LIST
	//-----------------------------------------------------------------------------------------------------------------
	public function lawhelp_list($rtn_data=FALSE) {
		
		$param = $this->input->post(NULL, TRUE);

		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// keyword
		$keyword = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// target
		$target = isset($param['target']) && $param['target'] ? $param['target'] : 'all';

		// target - 검색대상:지원종류
		$target_sprt_kind_cd = 'all';
		if(isset($param['target_sprt_kind_cd']) && $param['target_sprt_kind_cd'] != 'all') {
			$target_sprt_kind_cd = base64_decode($param['target_sprt_kind_cd']);
		}

		// target - 검색대상:지원결과
		$target_apply_rst_cd = 'all';
		if(isset($param['target_apply_rst_cd']) && $param['target_apply_rst_cd'] != '') {
			$target_apply_rst_cd = base64_decode($param['target_apply_rst_cd']);
		}

		// search date
		$search_date_target = isset($param['search_date_target']) && $param['search_date_target'] ? $param['search_date_target'] : '';
		$search_date_begin = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';
		$search_date_end = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';
		
		// 년도 검색
		$search_year = isset($param['search_year']) ? $param['search_year'] : '';

		// 연령 검색
		$search_ages = isset($param['search_ages']) ? $param['search_ages'] : '';

		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
		);

		$where['is_master'] = $param['is_master'];

		$where['target'] = $target;

		$where['keyword'] = $keyword;

		// 검색대상 : 지원종류 
		$where['target_sprt_kind_cd'] = $target_sprt_kind_cd; 

		// 검색대상 : 지원결과 
		$where['target_apply_rst_cd'] = $target_apply_rst_cd; 

		$where['search_date_target'] = $search_date_target;
		
		$where['search_date_begin'] = $search_date_begin;
		
		$where['search_date_end'] = $search_date_end;

		$where['search_year'] = $search_year;
		
		$where['search_ages'] = $search_ages;

		$rst = $this->manager->get_lawhelp_list($where);

		// 추가 - 목록에서 자신의 글인지 여부 판단을 위해 id 리턴, 2017.01.18 dylan
		$rstRtn['data']['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);

		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];

		// 요청에 의한 추가 : dylan 2017.09.12, danvistory.com
		// 검색 결과에서 내용본 뒤 목록으로 이동시 해당 페이지로 이동시키기 위함
		$where['page'] = $param['page'];

		// 엑셀 다운로드시 사용
		$tmp = serialize($where);
		$this->session->set_userdata(CFG_SESSION_SEARCH_DATA, $tmp);
				
		// 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
		}
		else {
			$this->load->view('json_view', $rstRtn);
		}
	}



	//-----------------------------------------------------------------------------------------------------------------
	// add or edit
	//-----------------------------------------------------------------------------------------------------------------
	public function processing() {
		
		$param = $this->input->post(NULL, TRUE);
		
		// 파일 업로드 처리
		$rst_upload = $this->_upload();

		// return variables
		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';

		// 세부유형은 없을수도 있다고 한다.
		$lh_kind_sub_cd = isset($param['lh_kind_sub_cd']) ? $param['lh_kind_sub_cd'] : [];

		//암호화 처리, 2019.07.29 dylan
		$key = $this->manager->get_key();
		
		// 저장 데이터 생성
		$data = array(
			'kind' => $param['kind']
			, 'lh_code' => $param['lh_code']
			, 'sprt_kind_cd' => base64_decode($param['sprt_kind_cd'])
			, 'sprt_kind_cd_etc' => $param['sprt_kind_cd_etc']
			, 'lh_sprt_cfm_date' => $param['lh_sprt_cfm_date']
			, 'lh_kind_cd' => base64_decode($param['lh_kind_cd'])// 권리구제 유형 - 추가 2018.07.05
			, 'lh_kind_sub_cd' => $lh_kind_sub_cd// 권리구제 세부유형 - 추가 2018.07.05
			, 'lh_apply_nm' => "AES_ENCRYPT('". trim($param['lh_apply_nm']) ."', HEX(SHA2('$key', 512)))"//암호화 처리, 2019.07.29 dylan
			, 'gender_cd' => base64_decode($param['gender_cd'])
			, 'lh_apply_addr' => "AES_ENCRYPT('". trim($param['lh_apply_addr']) ."', HEX(SHA2('$key', 512)))"//암호화 처리, 2019.07.29 dylan
			, 'lh_apply_comp_nm' => "AES_ENCRYPT('". trim($param['lh_apply_comp_nm']) ."', HEX(SHA2('$key', 512)))"//암호화 처리, 2019.07.29 dylan
			, 'lh_comp_addr' => "AES_ENCRYPT('". trim($param['lh_comp_addr']) ."', HEX(SHA2('$key', 512)))"//암호화 처리, 2019.07.29 dylan
			, 'apply_organ_cd' => base64_decode($param['apply_organ_cd'])
			, 'lh_labor_asso_nm' => $param['lh_labor_asso_nm']
			, 'lh_labor_nm' => $param['lh_labor_nm']
			, 'ages_cd' => base64_decode($param['ages_cd'])
			, 'ages_etc' => $param['ages_etc']
			, 'sprt_organ_cd' => base64_decode($param['sprt_organ_cd']) // 대상기관(관할) 
			, 'sprt_organ_cd_etc' => $param['sprt_organ_cd_etc'] // 대상기관(관할) - 기타입력내용
			, 'lid_date' => $param['lid_date'] // 출석조사일
			, 'lh_sprt_content' => $param['lh_sprt_content']
			, 'apply_rst_cd' => isset($param['apply_rst_cd']) ? base64_decode($param['apply_rst_cd']) : ''
			, 'apply_rst_cd_etc' => isset($param['apply_rst_cd_etc']) ? $param['apply_rst_cd_etc'] : ''
			, 'lh_etc' => isset($param['lh_etc']) ? $param['lh_etc'] : ''
			, 'file_name_org' => ''
			, 'file_name' => ''
			, 'gender_cd' => base64_decode($param['gender_cd'])
			, 'work_kind' => base64_decode($param['work_kind'])
			, 'work_kind_etc' => $param['work_kind_etc']
			, 'comp_kind' => base64_decode($param['comp_kind'])
			, 'comp_kind_etc' => $param['comp_kind_etc']
		);
		if($param['lh_accept_date'] != '') {
			$data['lh_accept_date'] = $param['lh_accept_date'];
		}
		if($param['lh_case_end_date'] != '') {
			$data['lh_case_end_date'] = $param['lh_case_end_date'];
		}

		// DB 처리 요청
		if($param['kind'] == 'edit') {
			$data['seq'] = $param['seq'];
		}
		else {
			// add 인 경우만 oper_id 저장
			$data['reg_oper_id'] = $param['oper_id'];
		}


		// 첨부파일 처리
		$fail_cnt = 0;
		$up_file_names = '';
		$up_file_names_org = '';
		$err_msg = '';

		// DB 처리
		foreach($rst_upload as $up_data) {
			if($up_data['result'] == 'succ') {
				if($up_file_names != '') $up_file_names .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$up_file_names .= $up_data['file_name'];
				if($up_file_names_org != '') $up_file_names_org .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$up_file_names_org .= $up_data['file_name_org'];

			}
			else if($up_data['result'] == 'fail') {
				$fail_cnt++;
				if($err_msg != '') $err_msg .',';
				$err_msg .= $up_data['error'];
			}
		}

		// 1.업로드 실패한 파일이 1개라도 있는 경우, db 저장하지 않고 file 삭제한다.
		if($fail_cnt > 0) {
			if($up_file_names != '') {
				self::_processing_del_file($up_file_names);
			}
			else {
				$rstRtn['data']['msg'] = 'upload 실패';
				if($err_msg != '') $rstRtn['data']['msg'] .= '['. strip_tags($err_msg) .']';
			}
		}

		// 2.업로드가 있고 모두 성공한 경우, db 저장
		else if($up_file_names != '') {
			$data['file_name'] = $up_file_names;
			$data['file_name_org'] = $up_file_names_org;

			// 2-1.수정인 경우, 기존 파일명을 가져와 합쳐 저장할 데이터를 생성한다.
			if($param['kind'] == 'edit') {

				// 기존 파일 삭제할 때 DB에서 가져온 기존파일명 문자열에서 파일명을 제거한다.
				$exist_data = array(
					'seq' => $data['seq']
					,'table_name' => 'lawhelp'
				);
				$arr_exist_file_data = $this->manager->get_exist_file_data_board($exist_data);
				$exist_file_data = array('file_name'=>'','file_name_org'=>'');

				// 가져온 기존 파일명을 문자열로 만든다.
				foreach($arr_exist_file_data as $ext_file) {
					if($ext_file->file_name != '') {
						if($exist_file_data['file_name'] != '') $exist_file_data['file_name'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
						$exist_file_data['file_name'] .= $ext_file->file_name;
					}
					if($ext_file->file_name_org != '') {
						if($exist_file_data['file_name_org'] != '') $exist_file_data['file_name_org'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
						$exist_file_data['file_name_org'] .= $ext_file->file_name_org;
					}
				}

				// 2-1-1.기존 파일이 변경되는 수정인 경우, 실제파일명으로 기존 파일 삭제
				$chg_real_file_name = isset($param['chg_real_file_name']) ? $param['chg_real_file_name'] : '';
				if($chg_real_file_name != '')  {
					
					// 파일 삭제 데이터 생성
					$append_proc_data = array(
						'file_name'=>$exist_file_data['file_name']
						,'file_name_org'=>$exist_file_data['file_name_org']
					);
					// 파일 삭제 및 기존 파일명에서 삭제하는 파일명 제거

					$rst_arr_del_ext_file_name = self::_processing_del_file($chg_real_file_name, $append_proc_data);

					// 기존 파일명이 하나라도 남아 있으면 새로 업로드된 파일과 합친다.
					if($rst_arr_del_ext_file_name['file_name'] != '' && $rst_arr_del_ext_file_name['file_name_org'] != '' ) {
						$data['file_name'] = $rst_arr_del_ext_file_name['file_name'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name'];
						$data['file_name_org'] = $rst_arr_del_ext_file_name['file_name_org'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name_org'];
					}
					// 기존 파일명이 모두 제거된 경우, 새로 업로드된 파일만 저장된다.

				}
				// 2-1-2.파일 변경없이 추가만 하는 수정인 경우
				else {
					// 기존 파일명에 합친다.
					if($exist_file_data['file_name'] != '') {
						$data['file_name'] = $exist_file_data['file_name'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name'];
					}
					if($exist_file_data['file_name_org'] != '') {
						$data['file_name_org'] = $exist_file_data['file_name_org'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name_org'];
					}
				}
			}
			// 2-2.파일 변경이 없는 경우, 바로 저장
			// DB 저장
			$rstRtn = self::_processing_data($data);
		}

		// 3.업로드가 없는 경우, db 저장
		else {
			$rstRtn = self::_processing_data($data);
		}

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _insert_data
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_data($args) {

		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';

		$kind = $args['kind'];
		unset($args['kind']);

		// db 저장
		if($kind == 'add') {
			$new_data = $this->manager->add_lawhelp($args);
			$new_id = $new_data['new_id'];

			if(!$new_id) {
				$rstRtn['data']['msg'] = 'DB저장 실패';
			}
			else {
				$rstRtn['data']['rst'] = 'succ';
			}
		}
		// edit
		else {
			$rst = $this->manager->edit_lawhelp($args);

			$rstRtn['data']['rst'] = 'succ';
		}

		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _processing_del_file : "|" 문자로 합쳐진 문자열을 받아 file을 삭제한다.
	//-----------------------------------------------------------------------------------------------------------------
	// param : $args 삭제할 파일의 이름 문자열
	// param : $append_args DB에서 가져온 기존 파일명(원본명,저장명) 데이터로 삭제할 파일의 파일명을 제거한다.
	// return : 삭제한 파일의 파일명을 제외한 기존 파일명 문자열
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_del_file($args, $append_args=NULL) {
		$rst_file_name = '';
		$rst_file_name_org = '';

		if(!is_null($append_args)) {
			$arr_file_name = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $append_args['file_name']);
			$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $append_args['file_name_org']);
		}

		// 삭제할 파일 데이터
		$arr_data = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $args);

		foreach($arr_data as $del_fname) {
			// 값이 있으면 실행 - delimiter가 없는 문자열을 자를 경우 빈 값이 하나 더 생긴다.
			if($del_fname != '') {
				// 파일 삭제
				@unlink(CFG_UPLOAD_PATH . $del_fname);
				clearstatcache();
				
				// DB에서 가져온 기존 파일명이 있는 경우
				if(!is_null($append_args)) {
					$index = 0;
					$index2 = 0;
					$arr_not_equal_fnm_real = array();
					$arr_not_equal_fnm_org = array();
					// 실제 파일명으로 비교한다. 원본파일명은 같은 이름이 있을 수 있기 때문..
					foreach($arr_file_name as $target_fname) {
						// 틀린 이름만 문자열로 생성
						if($del_fname != $target_fname) {
							$arr_not_equal_fnm_real[$index2] = $target_fname;
							$arr_not_equal_fnm_org[$index2] = $arr_file_name_org[$index];
							$index2++;
						}
						$index++;
					}
					// 틀린 파일명만 담은 배열로 기존 배열 갱신
					$arr_file_name = $arr_not_equal_fnm_real;
					$arr_file_name_org = $arr_not_equal_fnm_org;
				}
			}
		}

		$rstRtn['file_name'] = join(CFG_UPLOAD_FILE_NAME_DELIMITER, $arr_file_name);
		$rstRtn['file_name_org'] = join(CFG_UPLOAD_FILE_NAME_DELIMITER, $arr_file_name_org);

		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 여러 게시물 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
		
		$param = $this->input->post(NULL, TRUE);
		
		$data = array(
			'seqs' => $param['seqs']
		);

		$this->manager->del_multi_lawhelp($data);
		
		// return a list data
		$rstRtn = self::lawhelp_list(TRUE);

		$this->load->view('json_view', $rstRtn);
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del : 게시물 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
		// self::_chk_session();
		
		$param = $this->input->post(NULL, TRUE);
		
		$data['seqs'] = $param['seq'];

		$rst = $this->manager->del_multi_lawhelp($data);
		

		$rstRtn['data']['rst'] = 'succ';

		$this->load->view('json_view', $rstRtn);
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_data : 등록/수정 화면 데이터 리턴
	// - 권한 정리
	// 1.코드관리 : 권리구제는 입력,목록조회 만 코드로 관리하고 상세조회,목록의 엑셀다운은 상수로 별도 관리한다.
	// 2.상세 페이지 내 권한 체크 : 자신의 글만 수정,삭제,인쇄,출석조사일 추가 가능. 인쇄는 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 허용
	// 3.목록 페이지 수정 권한 체크 : 목록>접수번호 클릭시 상세조회권한이 있는지, 자신이 작성한 글인지 체크하여 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function get_data($args=NULL) {
		if(is_null($args)) {
			$param = $this->input->get(NULL, TRUE);
		}
		// 최초 admin 컨트롤러에서 호출시
		else {
			$param['kind'] = $args['kind'];
			$param['id'] = $args['id'];
		}
		
		// 상담내역 항목 서브 코드 데이터 가져오기
		// - 지원종류
		$rstRtn['data']['code_sprt_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_LAWHELP_SUPPORT_KIND);
		// - 신청기관
		$rstRtn['data']['code_apply_organ'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_LAWHELP_APPLY_CHANNEL);
		// - 대상기관(관할)	
		$rstRtn['data']['code_sprt_organ'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_LAWHELP_TARGET_ORGANS);
		// - 지원결과
		$rstRtn['data']['code_apply_rst'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_LAWHELP_SUPPORT_RESULT);
		// - 연령대
		$rstRtn['data']['ages_cd'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);
		// - 성별 : 추가 2018.07.03 dylan
		$rstRtn['data']['gender_cd'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);
		// - 직종 : 추가 2018.07.03 dylan
		$rstRtn['data']['work_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
		// - 업종 : 추가 2018.07.03 dylan
		$rstRtn['data']['comp_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);

		// - 출석조사일 데이터
		$rstRtn['edit']['lid_date_data'] = array();

		// 처리 구분 : add,view,edit
		$rstRtn['data']['kind'] = $param['kind'];

		// 자신의 글인지 여부 플래그, yes:1, no:0
		$rstRtn['data']['is_owner'] = 0;
		
		// 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - 인쇄 여부 플래그, yes:1, no:0
		$rstRtn['data']['is_printable'] = 0;
		
		$rstRtn['edit']['seq'] = '';
		$rstRtn['edit']['asso_code'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);
		$rstRtn['edit']['asso_name'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_NM);
		$rstRtn['edit']['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
		$rstRtn['edit']['oper_name'] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
		$rstRtn['edit']['lh_code'] = '';
		$rstRtn['edit']['sprt_kind_cd'] = '';
		$rstRtn['edit']['sprt_kind_cd_etc'] = '';
		$rstRtn['edit']['lh_sprt_cfm_date'] = '';
		$rstRtn['edit']['lh_apply_nm'] = '';
		$rstRtn['edit']['lh_apply_addr'] = '';
		$rstRtn['edit']['lh_apply_comp_nm'] = '';
		$rstRtn['edit']['lh_comp_addr'] = '';
		$rstRtn['edit']['ages_cd'] = '';
		$rstRtn['edit']['ages_etc'] = '';
		$rstRtn['edit']['apply_organ_cd'] = '';
		$rstRtn['edit']['lh_labor_asso_nm'] = '';
		$rstRtn['edit']['lh_labor_nm'] = '';
		$rstRtn['edit']['sprt_organ_cd'] = '';
		$rstRtn['edit']['sprt_organ_cd_etc'] = '';
		$rstRtn['edit']['lh_accept_date'] = '';
		$rstRtn['edit']['lh_case_end_date'] = '';
		$rstRtn['edit']['apply_rst_cd'] = '';
		$rstRtn['edit']['apply_rst_cd_etc'] = '';
		$rstRtn['edit']['lh_sprt_content'] = '';
		$rstRtn['edit']['lh_etc'] = '';
		$rstRtn['edit']['reg_date'] = '';
		$rstRtn['edit']['reg_oper_id'] = '';
		$rstRtn['edit']['file_name_org'] = '';
		$rstRtn['edit']['file_name'] = '';
		// 추가 2018.07.03 dylan
		$rstRtn['edit']['gender_cd'] = '';
		$rstRtn['edit']['work_kind'] = '';
		$rstRtn['edit']['work_kind_etc'] = '';
		$rstRtn['edit']['comp_kind'] = '';
		$rstRtn['edit']['comp_kind_etc'] = '';

		// 지원내용 및 결과항목
		$rstRtn['edit']['lid_seq'] = '';
		$rstRtn['edit']['lid_index'] = '';
		$rstRtn['edit']['lid_date'] = '';

		$rstRtn['edit']['lid_date_data'] = [];
		$rstRtn['edit']['lh_kind_cd'] = '';
		$rstRtn['edit']['lh_kind_cd_data'] = [];
		
		// edit
		if($param['kind'] == 'edit' || $param['kind'] == 'view') {
			// 해당 권한그룹에 할당된 권한 코드값 변수
//			$rstRtn['data']['auth_code'] = array();

			$data = array(
				'seq' => $param['id']
			);
			$rst = $this->manager->get_lawhelp($data);
			// echof( $rst );

			// 자신의 글인지 여부 플래그, yes:1, no:0
			$rstRtn['data']['is_owner'] = $rst['data'][0]->reg_oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID) ? 1 : 0;
			
			// 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - 인쇄 여부 플래그, yes:1, no:0
			if($rst['data'][0]->oper_auth_grp_id == $this->session->userdata(CFG_SESSION_ADMIN_AUTH_GRP_ID)
				|| $rst['data'][0]->oper_kind == $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE)) {
				$rstRtn['data']['is_printable'] = 1;
			}
			//
			$rstRtn['edit']['seq'] = $rst['data'][0]->seq;
			$rstRtn['edit']['lh_code'] = $rst['data'][0]->lh_code;
			$rstRtn['edit']['sprt_kind_cd'] = $rst['data'][0]->sprt_kind_cd;
			$rstRtn['edit']['sprt_kind_cd_etc'] = $rst['data'][0]->sprt_kind_cd_etc;
			$rstRtn['edit']['lh_sprt_cfm_date'] = $rst['data'][0]->lh_sprt_cfm_date;
			$rstRtn['edit']['lh_apply_nm'] = $rst['data'][0]->lh_apply_nm;
			$rstRtn['edit']['lh_apply_addr'] = $rst['data'][0]->lh_apply_addr;
			$rstRtn['edit']['lh_apply_comp_nm'] = $rst['data'][0]->lh_apply_comp_nm;
			$rstRtn['edit']['lh_comp_addr'] = $rst['data'][0]->lh_comp_addr;
			$rstRtn['edit']['ages_cd'] = $rst['data'][0]->ages_cd;
			$rstRtn['edit']['ages_etc'] = $rst['data'][0]->ages_etc;
			$rstRtn['edit']['apply_organ_cd'] = $rst['data'][0]->apply_organ_cd;
			$rstRtn['edit']['lh_labor_asso_nm'] = $rst['data'][0]->lh_labor_asso_nm;
			$rstRtn['edit']['lh_labor_nm'] = $rst['data'][0]->lh_labor_nm;
			$rstRtn['edit']['sprt_organ_cd'] = $rst['data'][0]->sprt_organ_cd;
			$rstRtn['edit']['sprt_organ_cd_etc'] = $rst['data'][0]->sprt_organ_cd_etc;
			$rstRtn['edit']['lh_accept_date'] = $rst['data'][0]->lh_accept_date=='0000-00-00' ? '' : $rst['data'][0]->lh_accept_date;
			$rstRtn['edit']['lh_case_end_date'] = $rst['data'][0]->lh_case_end_date=='0000-00-00' ? '' : $rst['data'][0]->lh_case_end_date;
			$rstRtn['edit']['lh_sprt_content'] = $rst['data'][0]->lh_sprt_content;
			$rstRtn['edit']['lh_etc'] = $rst['data'][0]->lh_etc;
			$rstRtn['edit']['apply_rst_cd'] = $rst['data'][0]->apply_rst_cd;
			$rstRtn['edit']['apply_rst_cd_etc'] = $rst['data'][0]->apply_rst_cd_etc;
			$rstRtn['edit']['reg_date'] = $rst['data'][0]->reg_date;
			$rstRtn['edit']['reg_oper_id'] = $rst['data'][0]->reg_oper_id;
			$rstRtn['edit']['reg_oper_id'] = $rst['data'][0]->reg_oper_id;

			// 첨부파일
			$rstRtn['edit']['file_name_org'] = $rst['data'][0]->file_name_org;
			$rstRtn['edit']['file_name'] = $rst['data'][0]->file_name;
			
			// 출석조사일 가져오기
			$rstRtn['edit']['lid_date_data'] = $rst['data']['lid_date_data'];
			// 권리구제 지원 데이터 - 추가 2018.07.05
			$rstRtn['edit']['lh_kind_cd'] = $rst['data'][0]->lh_kind_cd;
			$rstRtn['edit']['lh_kind_cd_data'] = $rst['data']['lh_kind_cd_data'];

			// 추가 2018.07.03 dylan
			$rstRtn['edit']['gender_cd'] = $rst['data'][0]->gender_cd;
			$rstRtn['edit']['work_kind'] = $rst['data'][0]->work_kind;
			$rstRtn['edit']['work_kind_etc'] = $rst['data'][0]->work_kind_etc;
			$rstRtn['edit']['comp_kind'] = $rst['data'][0]->comp_kind;
			$rstRtn['edit']['comp_kind_etc'] = $rst['data'][0]->comp_kind_etc;
		}

		if(is_null($args)) {
			$this->load->view('adm_lawhelp_edit_view', $rstRtn);
		}
		// 최초 admin 컨트롤러에서 호출시
		else {
			return $rstRtn;
		}

	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		// $admin = new Admin();
		// $out = $admin->is_out_session();
		$out = $this->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				// return $admin->logout();
				return $this->logout();
			}
		}
		else {
			return $out;
		}
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_file : 특정 파일만 삭제하고 DB를 업데이트 한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function del_file() {

		$param = $this->input->post(NULL, TRUE);
		
		$table_name = 'lawhelp';

		// 기존 파일 삭제할 때 DB에서 가져온 기존파일명 문자열에서 파일명을 제거한다.
		$exist_data = array(
			'seq' => $param['sq']
			,'table_name' => $table_name
		);
		$arr_exist_file_data = $this->manager->get_exist_file_data_board($exist_data);
		$exist_file_data = array('file_name'=>'','file_name_org'=>'');

		// 가져온 기존 파일명을 문자열로 만든다.
		foreach($arr_exist_file_data as $ext_file) {
			if($ext_file->file_name != '') {
				if($exist_file_data['file_name'] != '') $exist_file_data['file_name'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$exist_file_data['file_name'] .= $ext_file->file_name;
			}
			if($ext_file->file_name_org != '') {
				if($exist_file_data['file_name_org'] != '') $exist_file_data['file_name_org'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$exist_file_data['file_name_org'] .= $ext_file->file_name_org;
			}
		}

		// 실제파일명으로 기존 파일 삭제
		// 파일 삭제 데이터 생성
		$append_proc_data = array(
			'file_name'=>$exist_file_data['file_name']
			,'file_name_org'=>$exist_file_data['file_name_org']
		);

		// 파일 삭제 및 기존 파일명에서 삭제하는 파일명 제거
		$rst_arr_del_ext_file_name = self::_processing_del_file($param['fnr'], $append_proc_data);

		// DB 저장
		$data = array(
			'seq' => $param['sq']
			,'kind' => 'edit'
			,'file_name' => $rst_arr_del_ext_file_name['file_name']
			,'file_name_org' => $rst_arr_del_ext_file_name['file_name_org']
		);

		$rstRtn = self::_processing_data($data);

		$this->load->view('json_view', $rstRtn);
	}





	//=================================================================================================================
	// file 업로드,다운로드 helper
	//=================================================================================================================

	//-----------------------------------------------------------------------------------------------------------------
	// file upload
	//-----------------------------------------------------------------------------------------------------------------
	private function _upload() {
		
		$rstRtn = array();
		
		foreach ($_FILES as $index => $value){
			$rstRtn[$index]['file_name'] = '';
			$rstRtn[$index]['file_name_org'] = '';
			$rstRtn[$index]['error'] = '';
			$rstRtn[$index]['result'] = '';

			if ($value['name'] != ''){
				$this->load->library('upload');
				$this->upload->initialize($this->_set_upload_options());

				//upload the image
				if ( ! $this->upload->do_upload($index)){
					$rstRtn[$index]['error'] = $this->upload->display_errors();
					$rstRtn[$index]['result'] = 'fail';
				}
				else{
					$uploaded_data = $this->upload->data();
					$rstRtn[$index]['file_name'] = $uploaded_data['file_name'];
					$rstRtn[$index]['file_name_org'] = $uploaded_data['orig_name'];
					$rstRtn[$index]['result'] = 'succ';
				}
			}
		}
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// setting a file upload library
	//-----------------------------------------------------------------------------------------------------------------
	private function _set_upload_options() {
		$config = array();
		$config['upload_path'] = CFG_UPLOAD_PATH;
		$config['allowed_types'] = CFG_UPLOAD_ALL_ALLOW_EXT;
		$config['encrypt_name'] = TRUE;
		$config['overwrite'] = FALSE;
		$config['max_size'] = CFG_UPLOAD_MAX_FILE_SIZE;
		$config['max_width'] = 0; // no limit
		$config['max_height'] = 0; // no limit
		$config['max_filename'] = 0; // file name length, '0' is no limit
		$config['remove_spaces'] = TRUE;

		return $config;
	}
}
