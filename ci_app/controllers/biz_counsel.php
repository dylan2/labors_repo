<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// AES encrypt library
// require_once './lib/aes.class.php';
// require_once './lib/aesctr.class.php'; 

// ref Class
require_once 'admin.php';



/******************************************************************************************************
 * 운영상담 Class
 * Counsel
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Biz_Counsel extends Admin {
	
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	
	/**
	 * Construct
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// load model
		$this->load->model('db_biz_counsel', 'db_biz_counsel');
		$this->model = $this->db_biz_counsel;

		// helper
		$this->load->helper('download');
	}
	
	

	/**
	 * index
	 *
	 */
	public function index() {
	}


	/**
	 * 엑셀 다운로드 confirm 레이어 오픈 메서드
	 * 
	 * @param void
	 * @return void
	 */
	public function vdownload_excel() {

		$param = $this->input->get(NULL, TRUE);
		
		// 다운로드 구분 - counsel_list, lawhelp_list<권리구제 리스트>
		$data['kind'] = $param['kind'];

		$this->load->view('adm_excel_down_popup_view', $data);
	}


	/**
	 * 엑셀 다운로드 함수
	 *
	 * @param void
	 * @return void
	 */
	public function download() {

		ini_set('memory_limit', '-1'); // default 120M
		ini_set('max_execution_time', 0); // default 60초
		
		$param = $this->input->post(NULL, TRUE);
		$kind = $param['kind'];

		// data 가져오기
		$rstRtn = $this->excel_data(array('kind'=>$kind));

		// excel 인스턴스 생성
		require_once 'biz_excel.php';
		$excel = new Biz_excel();
		$excel->download2(array('data'=>$rstRtn['data'], 'kind'=>$kind));

	}

	
	/**
	 * 인쇄 데이터 리턴
	 *
	 * @param void
	 * @return void
	 */
	public function counsel_print() {

		$get = $this->input->get(NULL, TRUE);
		$seq = $get['seq'];

		$data = array(
			'seq' => $seq
		);
		$rstRtn = $this->model->get_biz_counsel_print($data);
		// echof($rstRtn['data']);

		$this->load->view('print_form/print_biz_counsel', $rstRtn);
	}
	

	/**
	 * <불러오기> 팝업 LIST view page 호출
	 *
	 * @param void
	 * @return void
	 */
	public function counsel_list_popup_view() {

		// 검색항목 데이터
		// - 상담방법 코드
		$rstRtn['res']['csl_way'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
		// - 업종 코드
		$rstRtn['res']['csl_comp_kind_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
		// - 처리결과 코드
		$rstRtn['res']['csl_proc_rst_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);
		// - 주제어 코드
		$rstRtn['res']['csl_keyword'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD);
		// - 검색 : 연도 데이터
		$rstRtn['res']['search_year'] = $this->model->get_counsel_year();
		
		// - 수정시 상담 seq
		$param = $this->input->get(NULL, TRUE);
		$rstRtn['res']['seq'] = $param['seq'];

		$this->load->view('biz_counsel/list_popup_view', $rstRtn);
	}
		

	/**
	 * <관련상담> 리스트 팝업 view page 호출
	 *
	 * @param void
	 * @return void
	 */
	public function counsel_rel_view() {
		
		$param = $this->input->get(NULL, TRUE);
		$data['data']['seq'] = $param['seq'];
		$data['data']['csl_name'] = $param['nm'];
		
		$this->load->view('biz_counsel/rel_list_popup_view', $data);
	}
	

	/**
	 * 불러오기, 관련상담 팝업 리스트 - 팝업에서 공유된 상담리스트 참조할때 사용
	 * 
	 * 관련상담 팝업 페이지도 같이 사용, 이때는 $param에 nm, tel 파라메터가 넘어온다.  <br>
	 * 불러오기 에서 호출시 "공유"된 상담만 가져옴
	 * 
	 * @param void
	 * @return void
	 */
	public function counsel_list_popup() {
				
		$param = $this->input->post(NULL, TRUE);

		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = 10;
		
		// where
		$where = array(
			'csl_share_yn_cd'=> CFG_SUB_CODE_CSL_SHARE_YN
			,'offset' => $offset
			,'limit' => $limit
		);

		// <관련상담> 수정인 경우 - 자신의 글은 제외한다.
		$where['seq'] = isset($param['seq']) && $param['seq'] ? $param['seq'] : '';

		$where['is_master'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);
		
		$where['target'] = isset($param['target']) && $param['target'] ? $param['target'] : '';

		// target 검색대상
		// - 처리결과
		$where['target_csl_proc_rst'] = isset($param['target_csl_proc_rst']) && $param['target_csl_proc_rst'] != '' ? base64_decode($param['target_csl_proc_rst']) : '';
		// - 업종
		$where['target_comp_kind'] = isset($param['target_comp_kind']) && $param['target_comp_kind'] != '' ? base64_decode($param['target_comp_kind']) : '';
		// - 키워드
		$where['target_keyword'] = isset($param['target_keyword']) && $param['target_keyword'] != '' ? base64_decode($param['target_keyword']) : '';
		// 검색어
		$where['keyword'] = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';
		// 상담방법 csl_way
		$where['csl_way'] = isset($param['csl_way']) && $param['csl_way'] ? base64_decode($param['csl_way']) : '';
		
		// search date	
		$where['search_date_begin'] = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';		
		$where['search_date_end'] = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';

		// 관련상담 팝업 페이지에서 호출한 경우
		if(isset($param['nm'])) {
			unset($where['csl_share_yn_cd']); // 이 파라메터는 상담보기 페이지에서만 사용
			$where['csl_cmp_nm'] = $param['nm'];
		}

		// 마스터인 경우 
		// - 공유여부와 무관하게 가져온다.
		// - 소속코드 제거, 전체 다 볼 수 있게 한다.
		if($where['is_master'] == 1) {
			unset($where['csl_share_yn_cd']);
			unset($where['asso_code']);
		}

		$rst = $this->model->get_counsel_list_pop($where);
		
		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
// 		$rstRtn['data']['query'] = isset($rst['query']) ? $rst['query'] : '';
		
		// 검색용 데이터
		unset($where['offset']);
		unset($where['limit']);
		unset($where['seq']);
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	/**
	 * 상담 목록 조회
	 * 
	 * @param boolean $rtn_data 데이터만 리턴 여부, default FALSE
	 * @return 기본 페이지 또는 $rtn_data=TRUE인 경우 array
	 */
	public function counsel_list($rtn_data=FALSE) {
		
		$param = $this->input->post(NULL, TRUE);
		
		// page
		$page = isset($param['page']) && $param['page'] != '' ? (int)$param['page'] : $this->param_get('page', $this->param_post('page', 1));
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
		);
		
		$where['is_master'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);

		// 주요상담사례 게시판에서 호출한 경우 - 1,0
		$where['is_board'] = isset($param['isbrd']) ? $param['isbrd'] : '';

		$where['target'] = isset($param['target']) && $param['target'] ? $param['target'] : '';

		// target 검색대상
		// - 처리결과 : 요청에 의해 추가 2015.08.14
		$where['target_csl_proc_rst'] = isset($param['target_csl_proc_rst']) && $param['target_csl_proc_rst'] != '' ? base64_decode($param['target_csl_proc_rst']) : '';
		// - 업종
		$where['target_comp_kind'] = isset($param['target_comp_kind']) && $param['target_comp_kind'] != '' ? base64_decode($param['target_comp_kind']) : '';
		// - 키워드
		$where['target_keyword'] = isset($param['target_keyword']) && $param['target_keyword'] != '' ? base64_decode($param['target_keyword']) : '';

		$where['keyword'] = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// 상담방법 csl_way
		$where['csl_way'] = isset($param['csl_way']) && $param['csl_way'] ? base64_decode($param['csl_way']) : '';
		
		// search date		
		$where['search_date_begin'] = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';		
		$where['search_date_end'] = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';

		// 검색 결과에서 내용본 뒤 목록으로 이동시 해당 페이지로 이동시키기 위함
		$where['page'] = $param['page'];
		
		// <캐싱처리> 검색정보가 캐시정보와 다르면 삭제
// 		if ($biz_counsel_search = $this->cache->get( $this->cache_prefix .'biz_counsel_list_search')){
// 			if(serialize($where) !== serialize($biz_counsel_search)) {
// 				$this->del_cache();
// 			}
// 		}
// 		$this->cache->save( $this->cache_prefix .'biz_counsel_list_search', $where, 300); // 초
		// <캐싱처리> 캐시데이터가 없으면 DB에서 가져온다.
// 		if ( ! $biz_counsel_list = $this->cache->get( $this->cache_prefix .'biz_counsel_list')){
// 			// -- DB
// 			$tmp = $this->model->get_counsel_list($where);
// 			$biz_counsel_list = $tmp;
// 			$this->cache->save( $this->cache_prefix .'biz_counsel_list', $biz_counsel_list, 300); // 초
// 		}
		
		$biz_counsel_list = $this->model->get_counsel_list($where);
		
		$rstRtn['data']['csl'] = $biz_counsel_list['data'];
		$rstRtn['data']['tot_cnt'] = $biz_counsel_list['tot_cnt'];
		
		// for pagination
		$rstRtn['page'] = $page;
		$rstRtn['loop'] = $biz_counsel_list['tot_cnt'] - (($page - 1) * CFG_BOARD_PAGINATION_ITEM_PER_PAGE);
		
// 		$rst = $this->model->get_counsel_list($where);
// 		$rstRtn['data']['data'] = $rst['data'];
// 		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
// 		$rstRtn['data']['query'] = isset($rst['query']) ? $rst['query'] : '';

		// 엑셀 다운로드시 사용
		$tmp = serialize($where);
		$this->session->set_userdata(CFG_SESSION_SEARCH_DATA, $tmp);

		// 검색항목 데이터
		// - 상담방법 코드
		$rstRtn['res'] = array();
		$rstRtn['res']['csl_way'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
		// - 업종 코드
		$rstRtn['res']['csl_comp_kind_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
		// - 처리결과 코드
		$rstRtn['res']['csl_proc_rst_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);
		// - 주제어 코드
		$rstRtn['res']['csl_keyword'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD);
		// - 검색 : 연도 데이터
		$rstRtn['res']['search_year'] = $this->model->get_counsel_year();
			
		// <Admin.php> 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
			
		}
		// 상담사례 게시판에서 호출하는 경우
// 		else if($where['is_board'] == 1) {
		else {

			$this->load->view('json_view', $rstRtn);
		}
		// 사용자상담 목록화면
		/*else {
			
			$rstRtn['csl'] = $rstRtn['data']['csl'];
			$rstRtn['tot_cnt'] = $rstRtn['data']['tot_cnt'];

			$rstRtn['data']['selected_menu'] = $param['selected_menu'];
			$rstRtn['data']['m_code'] = $param['m_code'];
			
			// 현재 접속한 oper id, name
			$rstRtn['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
			$rstRtn['oper_name'] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
			$rstRtn['oper_asso_cd'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);
			
			$rstRtn['data'][CFG_SESSION_ADMIN_NAME]  = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
			
			// LNB 메뉴 생성용 데이터 추출
			$rstRtn['data']['lnb_code'] = $this->model->get_lnb_menu_sub_code_by_m_code(CFG_MASTER_CODE_MANAGE_CODE);
			
			$this->load->view('biz_counsel/list_view', $rstRtn);
		}
		*/
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// open_counsel_at : 상담 보기 - 통계->상담사례통계에서 자신의 상담보기시 사용
	//-----------------------------------------------------------------------------------------------------------------
	public function open_counsel_at() {
		parent::open_counsel_at();
	}
	

	//-----------------------------------------------------------------------------------------------------------------
	// add or edit
	//-----------------------------------------------------------------------------------------------------------------
	public function processing() {

		$param = $this->input->post(NULL, TRUE);

		// return variables
		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';

		// 관련삼당 - 상담내용 가져오기 한 경우, 원글 seq
		$csl_ref_seq = isset($param['csl_ref_seq']) && $param['csl_ref_seq'] != '' ? $param['csl_ref_seq'] : 0;

		// 내담자 성명 
		$csl_name = trim($param['csl_name']);
		// 내담자 연락처
		$tel = '';
		$tel01 = trim($param['csl_tel01']);
		$tel02 = trim($param['csl_tel02']);
		$tel03 = trim($param['csl_tel03']);
		if($tel01 !='' && $tel02 != '' && $tel03 != '' || $tel03 != '') {// 뒷4자리만 입력해도 저장한다. --0000 형태
			$tel = $tel01 .'-'. $tel02 .'-'. $tel03;
		}
		$tel_sch = '';
		if($tel03 != '' && strlen($tel03) == 4) {
			$tel_sch = $param['csl_tel03'];
		}
		
		// 사업체명
		$csl_cmp_nm = trim($param['csl_cmp_nm']);

		$key = $this->model->get_key();
		
		// 저장 데이터 생성
		$data = array(
			'kind' => $param['kind']
			, 'csl_ref_seq' => $csl_ref_seq 
			, 'csl_name' => "AES_ENCRYPT('$csl_name', HEX(SHA2('$key', 512)))"
			, 'csl_tel' => "AES_ENCRYPT('$tel', HEX(SHA2('$key', 512)))"
			, 'csl_tel_sch' => "AES_ENCRYPT('$tel_sch', HEX(SHA2('$key', 512)))"
			, 'csl_cmp_nm' => "AES_ENCRYPT('$csl_cmp_nm', HEX(SHA2('$key', 512)))"
			, 'asso_code' => base64_decode($param['asso_code'])
			, 'csl_date' => $param['csl_date']
			, 's_code' => base64_decode($param['s_code'])
			, 's_code_etc' => trim($param['s_code_etc'])
			, 'comp_addr_cd' => base64_decode($param['comp_addr_cd'])
			, 'comp_addr_etc' => trim($param['comp_addr_etc'])
			, 'emp_cnt_cd' => base64_decode($param['emp_cnt_cd'])
			, 'emp_cnt_etc' => trim($param['emp_cnt_etc'])
			, 'comp_kind_cd' => base64_decode($param['comp_kind_cd'])
			, 'comp_kind_etc' => $param['comp_kind_etc']
			, 'oper_period_cd' => base64_decode($param['oper_period_cd'])
			, 'emp_paper_yn_cd' => base64_decode($param['emp_paper_yn_cd'])
			, 'emp_insured_yn_cd' => base64_decode($param['emp_insured_yn_cd'])
			, 'emp_rules_yn_cd' => base64_decode($param['emp_rules_yn_cd'])
			, 'csl_title' => trim($param['csl_title'])
			, 'csl_content' => trim($param['csl_content'])
			, 'csl_reply' => trim($param['csl_reply'])
			, 'csl_proc_rst_cd' => base64_decode($param['csl_proc_rst_cd'])
			, 'csl_proc_rst_etc' => trim($param['csl_proc_rst_etc'])
			, 'csl_share_yn_cd' => base64_decode($param['csl_share_yn_cd'])
		);

		// 상담유형 - 다중선택
		$data['csl_sub_code']['arr_csl_kind'] = array();
		if(isset($param['csl_kind']) && $param['csl_kind'] != '') {
			$data['csl_sub_code']['arr_csl_kind'] = $param['csl_kind'];
		}

		// 주제어 - 다중선택
		$data['csl_sub_code']['arr_keyword'] = array();
		if(isset($param['csl_keyword']) && $param['csl_keyword'] != '') {
			$data['csl_sub_code']['arr_keyword'] = $param['csl_keyword'];
		}

		$data['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
		
		// DB 처리 요청
		if($param['kind'] == 'edit') {
			$data['seq'] = $param['seq'];
		}

		$rstRtn = self::_processing_data($data);

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _processing_data
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_data($args) {
		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';

		$kind = $args['kind'];
		unset($args['kind']);

		// db 저장
		if($kind == 'add') {
			$new_data = $this->model->add_biz_counsel($args);

			if( ! $new_data['new_id']) {
				$rstRtn['data']['msg'] = 'DB저장 실패';
			}
			else {
				$rstRtn['data']['rst'] = 'succ';
			}
		}
		// edit
		else {
			$rst = $this->model->edit_biz_counsel($args);
			$rstRtn['data']['rst'] = 'succ';
		}

		// "관련상담" 처리 로직
		// 원글과 원글 가져와 작성된 상담1, 2, ...n 모두 공유항목을 업데이트 처리한다. 
		// 원글을 가져와 작성한 상담2,3,...n을 다시 가져와 작성한 경우도 마찬가지로 처리한다.
		if(isset($args['csl_ref_seq']) && $args['csl_ref_seq'] != '-1' && $args['csl_ref_seq'] != '' && $args['csl_ref_seq'] != '0') {
			// csl_ref_seq이 같은 모든 상담 업데이트
			$this->model->edit_biz_counsel_ref($args);

		}

		// 캐시파일 삭제
// 		$this->del_cache();
		
		return $rstRtn;
	}


	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 여러 counsel 게시물 삭제
	// 다른 상담에서 참조글로 가져다 쓴 적이 있는지 체크하여 있으면 삭제시키지 않는다.(현업의견은 아님)
	// 위 기능을 끄려면 check_ref 변수를 FALSE 값 할당하면 됨
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
		
		$param = $this->input->post(NULL, TRUE);
		
		$data = array(
			'seqs' => $param['seqs']
			,'check_ref' => FALSE //<==========
		);

		$rst_tmp1 = $this->model->del_multi_counsel($data);
		$rst_tmp2 = '';
		$len = 0;
		if(!empty($rst_tmp1)) {
			$rst_tmp2 = explode(CFG_AUTH_CODE_DELIMITER, $rst_tmp1);
			$len = count($rst_tmp2);
		}
		$rst = 'exist';
		
		// return a list data
		$rstRtn = self::counsel_list(TRUE);
		$rstRtn['data']['rst'] = $rst;
		$rstRtn['data']['len'] = $len;

		// 캐시파일 삭제
// 		$this->del_cache();
		
		$this->load->view('json_view', $rstRtn);
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del : counsel 게시물 삭제
	// 다른 상담에서 참조글로 가져다 쓴 적이 있는지 체크하여 있으면 삭제시키지 않는다.(현업의견은 아님)
	// 위 기능을 끄려면 check_ref 변수를 FALSE 값 할당하면 됨
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
		
		$param = $this->input->post(NULL, TRUE);
		
		$data['seqs'] = $param['seq'];

		$data['check_ref'] = FALSE; //<==========
		
		$rst = $this->model->del_multi_counsel($data);
		

		$rstRtn['data']['rst'] = 'succ';

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_edit_view_data : 등록/수정 화면 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	// 권한 :
	// 소속코드 : asso_code - 자신이 속한 소속의 상담만 가져온다.
	// 고도화 - 위 조건 변경됨
	// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
	// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
	// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
	//-----------------------------------------------------------------------------------------------------------------
	public function get_edit_view_data($args=NULL) {
		
		if(is_null($args)) {
			$param = $this->input->get(NULL, TRUE);
		}
		else {
			$param['kind'] = $args['kind'];
			$param['id'] = $args['id'];
		}
		
		// 상담내역 항목 서브 코드 데이터 가져오기
		$rstRtn['data']['s_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);//상담방법
		$rstRtn['data']['comp_addr_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);//회사 소재지 코드 -- 상담내역 거주지 코드와 동일
		$rstRtn['data']['emp_cnt_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);//근로자수
		$rstRtn['data']['comp_kind_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);//업종
		$rstRtn['data']['oper_period_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD);// 운영기간
		$rstRtn['data']['emp_paper_yn_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN);//근로계약서작성여부 코드
		$rstRtn['data']['emp_insured_yn_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN);//4대보험가입여부 코드
		$rstRtn['data']['emp_rules_yn_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN);//취업규칙작성여부 코드
		$rstRtn['data']['csl_kind_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND);//상담유형 코드
		$rstRtn['data']['csl_proc_rst_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);//상담 처리결과 코드
		$rstRtn['data']['csl_keyword_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD);//주제어 코드
		$rstRtn['data']['csl_share_yn_cd'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_SHARE_YN);//상담내역공유여부 코드

		// 처리 구분 : add,view,edit
		$rstRtn['data']['kind'] = $param['kind'];

		// [권한] 체크용
		$rstRtn['data']['is_auth'] = 1;

		// [권한] 자신의 글인지 여부 플래그, yes:1, no:0
		$rstRtn['data']['is_owner'] = 0;
		
		// [권한] 수정,삭제 가능 여부 플래그, yes:1, no:0 - 서울시 계정만 해당됨 옴부즈만 상대
		// - 서울시 계정은 서울시 소속 그룹의 옴부즈만 글은 수정,삭제 가능하게 처리 요청 : 최진혁 2016.07.28
		$rstRtn['data']['is_editable'] = 0;
		
		// [권한] 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - 인쇄 여부 플래그, yes:1, no:0 <조회,인쇄만 가능>
		$rstRtn['data']['is_printable'] = 0;

		// [권한] 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - <조회,인쇄만 가능>하기 때문에 등록버튼 숨기기 위한 플래그
		$rstRtn['data']['is_only_view'] = 0;
		
		$rstRtn['edit']['seq'] = '';
		$rstRtn['edit']['asso_code'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);
		$rstRtn['edit']['asso_name'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_NM);
		$rstRtn['edit']['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
		$rstRtn['edit']['oper_name'] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
		$rstRtn['edit']['csl_date'] = '';
		$rstRtn['edit']['csl_name'] = '';
		$rstRtn['edit']['csl_tel01'] = '';
		$rstRtn['edit']['csl_tel02'] = '';
		$rstRtn['edit']['csl_tel03'] = '';
		
		$rstRtn['edit']['csl_cmp_nm'] = '';
		$rstRtn['edit']['oper_period_cd'] = '';
		$rstRtn['edit']['emp_paper_yn_cd'] = '';
		$rstRtn['edit']['emp_insured_yn_cd'] = '';
		$rstRtn['edit']['emp_rules_yn_cd'] = '';
		$rstRtn['edit']['csl_proc_rst_cd'] = '';
		
		$rstRtn['edit']['csl_ref_seq'] = '';
		$rstRtn['edit']['s_code'] = '';
		$rstRtn['edit']['s_code_etc'] = '';
		$rstRtn['edit']['comp_kind_cd'] = '';
		$rstRtn['edit']['comp_kind_etc'] = '';
		$rstRtn['edit']['comp_addr_cd'] = '';
		$rstRtn['edit']['comp_addr_etc'] = '';
		$rstRtn['edit']['emp_cnt_cd'] = '';
		$rstRtn['edit']['emp_cnt_etc'] = '';
		$rstRtn['edit']['csl_kind'] = '';
		$rstRtn['edit']['csl_title'] = '';
		$rstRtn['edit']['csl_content'] = '';
		$rstRtn['edit']['csl_reply'] = '';
		$rstRtn['edit']['csl_proc_rst'] = '';
		$rstRtn['edit']['csl_proc_rst_etc'] = '';
		$rstRtn['edit']['csl_share_yn_cd'] = '';
		// 상담유형,주제어 코드
		$rstRtn['edit']['csl_sub_code'] = '';
		$rstRtn['edit']['reg_date'] = '';

		
		// edit
		if($param['kind'] == 'edit' || $param['kind'] == 'view') {

			$data = array(
				'seq' => $param['id']
			);
			$rst = $this->model->get_biz_counsel($data);
			// echof( $rst );

			// [권한] 체크 - 통계>상담사례 - 엑셀 다운로드하여 링크로 접근하는 경우의 처리, 20160729 최진혁
			$rstRtn['data']['is_auth'] = 0;
			// master 제외
			if($this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER) != 1) {
				// 기본 - 권익센터,OO센터 직원 : 소속 상담만 가능
				if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK1) {
					if($this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) == $rst['data'][0]->asso_cd) {
						$rstRtn['data']['is_auth'] = 1;
					}
				}
				// 서울시 : 자신의 글과 옴부즈만 상담만 가능
				else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2) {
					if($rst['data'][0]->oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID) || $rst['data'][0]->oper_kind == CFG_OPERATOR_KIND_CODE_OK3) {
						$rstRtn['data']['is_auth'] = 1;
					}
				}
				// 옴부즈만 : 자신의 상담만 가능
				else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK3) { 
					if($rst['data'][0]->oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID)) {
						$rstRtn['data']['is_auth'] = 1;
					}
				}
				// 자치구공무원인 경우
				else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
					$rstRtn['data']['is_auth'] = 1;
				}
			}
			// master
			else {
				$rstRtn['data']['is_auth'] = 1;
			}

			// [권한] 자신의 글인지 여부 플래그, yes:1, no:0
			$rstRtn['data']['is_owner'] = $rst['data'][0]->oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID) ? 1 : 0;

			// [권한] 수정,삭제 가능 여부 플래그, yes:1, no:0 - 서울시 계정만 해당됨 옴부즈만 상대
			// - 서울시 계정은 서울시 소속 그룹의 옴부즈만 글은 수정,삭제 가능하게 처리 요청 : 최진혁 2016.07.28
			if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2 
				&& $rst['data'][0]->asso_cd == $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) 
				&& $rst['data'][0]->oper_kind == CFG_OPERATOR_KIND_CODE_OK3) {
				$rstRtn['data']['is_editable'] = 1;
			}

			// [권한] 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - 인쇄 여부 플래그, yes:1, no:0
			if($rst['data'][0]->oper_auth_grp_id == $this->session->userdata(CFG_SESSION_ADMIN_AUTH_GRP_ID)
				|| $rst['data'][0]->oper_kind == $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE)) {
				$rstRtn['data']['is_printable'] = 1;
			}

			// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 가진다.
			if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
				// 글 작성자의 운영자구분이 옴부즈만 이고 세부구분이 접속자와 같은 경우 인쇄 권한 부여
				if($rst['data'][0]->oper_kind == $this->session->userdata(CFG_OPERATOR_KIND_CODE_OK3)
					&& $rst['data'][0]->oper_kind_sub == $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE_SUB)) {
					$rstRtn['data']['is_printable'] = 1;

					// <조회,인쇄만 가능>하기 때문에 등록버튼 숨기기 위한 플래그
					$rstRtn['data']['is_only_view'] = 1;
				}
			}
			//
			$rstRtn['edit']['seq'] = $rst['data'][0]->seq;
			$rstRtn['edit']['asso_code'] = $rst['data'][0]->asso_code;
			$rstRtn['edit']['asso_name'] = $rst['data'][0]->asso_name;
			$rstRtn['edit']['oper_id'] = $rst['data'][0]->oper_id;
			$rstRtn['edit']['oper_name'] = $rst['data'][0]->oper_name;
			$rstRtn['edit']['csl_date'] = $rst['data'][0]->csl_date;			
			$rstRtn['edit']['csl_name'] = $rst['data'][0]->csl_name;
			$rstRtn['edit']['csl_cmp_nm'] = $rst['data'][0]->csl_cmp_nm;
			$csl_tel = $rst['data'][0]->csl_tel;
			$rstRtn['edit']['csl_tel01'] = '';
			$rstRtn['edit']['csl_tel02'] = '';
			$rstRtn['edit']['csl_tel03'] = '';
			if(strpos($csl_tel, '-') !== FALSE) {
				$arr_tel = explode('-', $csl_tel);
				$rstRtn['edit']['csl_tel01'] = $arr_tel[0];
				$rstRtn['edit']['csl_tel02'] = $arr_tel[1];
				$rstRtn['edit']['csl_tel03'] = $arr_tel[2];
			}
			else {
				$rstRtn['edit']['csl_tel03'] = $rst['data'][0]->csl_tel_sch;
			}
			$rstRtn['edit']['oper_period_cd'] = $rst['data'][0]->oper_period_cd;
			$rstRtn['edit']['emp_paper_yn_cd'] = $rst['data'][0]->emp_paper_yn_cd;
			$rstRtn['edit']['emp_insured_yn_cd'] = $rst['data'][0]->emp_insured_yn_cd;
			$rstRtn['edit']['emp_rules_yn_cd'] = $rst['data'][0]->emp_rules_yn_cd;			
			$rstRtn['edit']['csl_ref_seq'] = $rst['data'][0]->csl_ref_seq;
			$rstRtn['edit']['s_code'] = $rst['data'][0]->s_code;
			$rstRtn['edit']['s_code_etc'] = $rst['data'][0]->s_code_etc;
			$rstRtn['edit']['comp_kind_cd'] = $rst['data'][0]->comp_kind_cd;
			$rstRtn['edit']['comp_kind_etc'] = $rst['data'][0]->comp_kind_etc;
			$rstRtn['edit']['comp_addr_cd'] = $rst['data'][0]->comp_addr_cd;
			$rstRtn['edit']['comp_addr_etc'] = $rst['data'][0]->comp_addr_etc;
			$rstRtn['edit']['emp_cnt_cd'] = $rst['data'][0]->emp_cnt_cd;
			$rstRtn['edit']['emp_cnt_etc'] = $rst['data'][0]->emp_cnt_etc;
			$rstRtn['edit']['csl_title'] = $rst['data'][0]->csl_title;
			$rstRtn['edit']['csl_content'] = $rst['data'][0]->csl_content;
			$rstRtn['edit']['csl_reply'] = $rst['data'][0]->csl_reply;
			$rstRtn['edit']['csl_proc_rst_cd'] = $rst['data'][0]->csl_proc_rst_cd;
			$rstRtn['edit']['csl_proc_rst_etc'] = $rst['data'][0]->csl_proc_rst_etc;
			$rstRtn['edit']['csl_share_yn_cd'] = $rst['data'][0]->csl_share_yn_cd;
			// 상담유형,주제어 코드
			$rstRtn['edit']['csl_sub_code'] = $rst['data']['csl_sub_code'];

			// 추가 : 2017.01.18 dylan
			$rstRtn['edit']['reg_date'] = $rst['data'][0]->reg_date;
		}

		if(is_null($args)) {
			$this->load->view('biz_counsel/edit_view', $rstRtn);
		}
		else {
			return $rstRtn;
		}
	}



	//-----------------------------------------------------------------------------------------------------------------
	// counsel_help : 상담입력 > 항목 안내 팝업
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_help() {
		$data = $this->input->get(NULL, TRUE);
		$this->load->view($data['url']);
	}

	
}
