<?php

// 서버 개인키는 아래 함수로 생성하였다.
// 2019.07.28
// dylan




// 비대칭 알고리듬인 RSA를 사용하여 문자열을 암호화하는 법.
// 개인키 비밀번호는 암호화할 때는 필요없고 복호화할 때만 입력하면 되므로
// 서버에 저장할 필요 없이 그때그때 관리자가 입력하도록 해도 된다.
// PHP 5.2 이상, openssl 모듈이 필요하다.
// RSA 개인키, 공개키 조합을 생성한다.
// 키 생성에는 시간이 많이 걸리므로, 한 번만 생성하여 저장해 두고 사용하면 된다.
// 단, 비밀번호는 개인키를 사용할 때마다 필요하다.
function rsa_generate_keys($password, $bits=2048, $digest_algorithm='sha256')
{
  $res = openssl_pkey_new(array(
    'digest_alg' => $digest_algorithm,
    'private_key_bits' => $bits,
    'private_key_type' => OPENSSL_KEYTYPE_RSA
  ));
  
  if (false === openssl_pkey_export($res, $private_key, $password)) {
  	throw new CryptoException('Could not export private key', OpenSslUtil::getErrors());
  }
  
  $public_key = openssl_pkey_get_details($res);
  $public_key = $public_key['key'];
  
  return array(
    'private_key' => $private_key,
    'public_key' => $public_key,
  );
}
//----- 공개키와 개인키를 DB에 저장후 사용하도록 하자. (세션에 넣기엔 너무큼) -----------------------------


// RSA 공개키를 사용하여 문자열을 암호화한다.
// 암호화할 때는 비밀번호가 필요하지 않다.
// 오류가 발생할 경우 false를 반환한다.
function rsa_encrypt($plaintext, $public_key)
{
  // 용량 절감과 보안 향상을 위해 평문을 압축한다.  
  $plaintext = gzcompress($plaintext);
  
  // 공개키를 사용하여 암호화한다.  
  $pubkey_decoded = @openssl_pkey_get_public($public_key);
  if ($pubkey_decoded === false) return false;
  
  $ciphertext = false;
  $status = @openssl_public_encrypt($plaintext, $ciphertext, $pubkey_decoded);
  if (!$status || $ciphertext === false) return false;
  
  // 암호문을 base64로 인코딩하여 반환한다.  
  return base64_encode($ciphertext);
}


// RSA 개인키를 사용하여 문자열을 복호화한다.
// 복호화할 때는 비밀번호가 필요하다.
// 오류가 발생할 경우 false를 반환한다.
function rsa_decrypt($ciphertext, $private_key, $password)
{
  // 암호문을 base64로 디코딩한다.  
  $ciphertext = @base64_decode($ciphertext, true);
  if ($ciphertext === false) return false;
  
  // 개인키를 사용하여 복호화한다.  
  $privkey_decoded = @openssl_pkey_get_private($private_key, $password);
  if ($privkey_decoded === false) return false;
  
  $plaintext = false;
  $status = @openssl_private_decrypt($ciphertext, $plaintext, $privkey_decoded);
  @openssl_pkey_free($privkey_decoded);
  if (!$status || $plaintext === false) return false;
  
  // 압축을 해제하여 평문을 얻는다.  
  $plaintext = @gzuncompress($plaintext);
  if ($plaintext === false) return false;
  
  // 이상이 없는 경우 평문을 반환한다.  
  return $plaintext;
}

$serverKey = "serverKey1";
echo "<p>==========</p>";
echo "키생성:";
$keys = rsa_generate_keys($serverKey);
print_r($keys);

echo "<br>";
echo "<p>==========</p>";
$text = 'abcde한글1';
echo "데이터:$text";
echo "<br>";

$enc = rsa_encrypt($text, $keys['public_key']);
echo "인코딩:$enc";
echo "<br>";

$dec = rsa_decrypt($enc, $keys['private_key'], $serverKey);
echo "<br>";
echo "<p>==========</p>";
echo "디코딩:$dec";

echo "<br>";
echo "<br>";
echo "private_key:". $keys['private_key'];

echo "<br>";
echo "<br>";
echo "<p>==========</p>";
echo "압축테스트 : gzdeflate";
$compressed = gzdeflate($keys['private_key'], 9);
echo "압축 - private_key: $compressed";

echo "<br>";
$uncompressed = gzinflate($compressed);
echo "압축해제 - private_key: $uncompressed";

 