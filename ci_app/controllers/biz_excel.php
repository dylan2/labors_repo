<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');




/******************************************************************************************************
 * Excel 생성 파일
 * Biz_excel
 * 
 * 사용자 상담 엑셀 다운로드 처리 전용
 * 
 * 작성자 : dylan
 *
 *
 *
 *****************************************************************************************************/
class Biz_excel extends CI_Controller {
	

	//========================================================================
	// construct
	//========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');

		$this->load->library('PHPExcel');
		
		$this->load->helper('excel_helper');
	}
	
	
	
	
	//########################################################################
	// Excel
	//########################################################################

	//========================================================================
	// index
	//========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}


	function obj_sort(&$objArray, $indexFunction, $sort_flags=0) {
		$indices = array();
		foreach($objArray as $obj) {
			$indeces[] = $indexFunction($obj);
		}
		return array_multisort($indeces, $objArray, $sort_flags);
	}

	function sortByTitle($a, $b){
		return strcmp($a->title, $b->title);
	}


	

	//-----------------------------------------------------------------------------------------------------------------
	// download
	//-----------------------------------------------------------------------------------------------------------------
	public function download($args) {

		// 다운로드 구분
		$kind = $args['kind'];

		// PHPExcel 클래스 선언
		$objPHPExcel = new PHPExcel();
		 
		// Create the worksheet
		$objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();

		if($kind == 'down02') {// 기본통계
			$excel_data = $args['data'];
		}
		else {
			$excel_data = $args['data']['data'];
		}
		$excel_data_tot = isset($args['data']['data_tot']) ? $args['data']['data_tot'] : [];
		
		// rename a sheet
		$sheet->setTitle('사용자상담통계');

		// 
		$alphabet = 65;
		$alpha_char = '';
		$str = '';
		$view_url = 'https://db.labors.or.kr/?c=admin&m=main&menu=a3dCaXhDUUZTRjBUTGtMSkhHYjNqQURnMkh4eCtnPT0=&code=UzAzOA==&kind=edit&id=';
		
		// 상담사례
		if($kind == 'down01') {
			// header cell text
			$header_nm = '번호●상담일●기관●상담자●사업장명●대표자명●상담방법●상담방법-기타●소재지●소재지-기타●근로자수●근로자수-기타●업종●업종-상세●운영기간
				●근로계약서●4대보험●취업규칙●상담유형1●상담유형2●상담유형3●처리결과●주제어●상담내용●답변';
			// cell width
			$cell_width = '7●13●20●20●13●13●13●13●13●13●13●13●13●13
				●13●13●13●13●13●13●13●13●13●100●100';

			// cell에 데이터 할당
			$index = count($excel_data);
			$row = 2;

			foreach($excel_data as $rs) {
				$sheet->setCellValue('A' . $row, $index--);
				$sheet->setCellValue('B' . $row, $rs->csl_date);
				$sheet->setCellValue('C' . $row, $rs->asso_code_nm);
				$sheet->setCellValue('D' . $row, $rs->oper_name);
				$sheet->setCellValue('E' . $row, $rs->csl_cmp_nm);
				$sheet->setCellValue('F' . $row, $rs->csl_name);
				$sheet->setCellValue('G' . $row, $rs->csl_method_nm);//상담방법
				$sheet->setCellValue('H' . $row, $rs->s_code_etc);//상담방법-기타
				$sheet->setCellValue('I' . $row, $rs->comp_addr_cd);
				$sheet->setCellValue('J' . $row, $rs->comp_addr_etc);//소재지-기타
				$sheet->setCellValue('K' . $row, $rs->emp_cnt_cd);//근로자수
				$sheet->setCellValue('L' . $row, $rs->emp_cnt_etc);//근로자수-기타
				$sheet->setCellValue('M' . $row, $rs->comp_kind_cd);
				$sheet->setCellValue('N' . $row, $rs->comp_kind_etc);//업종-상세
				$sheet->setCellValue('O' . $row, $rs->oper_period_cd);
				$sheet->setCellValue('P' . $row, $rs->emp_paper_yn_cd);
				$sheet->setCellValue('Q' . $row, $rs->emp_insured_yn_cd);
				$sheet->setCellValue('R' . $row, $rs->emp_rules_yn_cd);
				// 상담유형 3개 값 분리
				$arr_csl_kind_cd = explode(',', $rs->csl_kind);
				$sheet->setCellValue('S' . $row, isset($arr_csl_kind_cd[0]) ? $arr_csl_kind_cd[0] : '');
				$sheet->setCellValue('T' . $row, isset($arr_csl_kind_cd[1]) ? $arr_csl_kind_cd[1] : '');
				$sheet->setCellValue('U' . $row, isset($arr_csl_kind_cd[2]) ? $arr_csl_kind_cd[2] : '');
				$sheet->setCellValue('V' . $row, $rs->csl_proc_rst_cd);
				$sheet->setCellValue('W' . $row, $rs->csl_keywords);
				$sheet->setCellValue('X' . $row, $rs->csl_content);
				$sheet->setCellValue('Y' . $row, $rs->csl_reply);
				// 상담일에 해당 상담내용 페이지 링크 추가
				$sheet->getCell('B' . $row)->getHyperlink()->setUrl($view_url . $rs->seq);
				$row++;
			}

		}

		// 기본통계
		else if($kind == 'down02') {

// 			echof($args['sch_kind']);
// 			exit;
			
			//통계유형 구분
			if($args['sch_kind'] == 0){
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); // 0.상담방법
			}
			elseif($args['sch_kind'] == 1) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION); // 1.소속
			}
			elseif($args['sch_kind'] == 2) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); // 2.소재지
			}
			elseif($args['sch_kind'] == 3) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT); // 3.근로자수
			}
			elseif($args['sch_kind'] == 4) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND); // 4.업종
			}
			elseif($args['sch_kind'] == 5) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD); // 5.운영기간
			}
			elseif($args['sch_kind'] == 6) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN); // 6.근로계약서
			}
			elseif($args['sch_kind'] == 7) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN); // 7.4대보험
			}
			elseif($args['sch_kind'] == 8) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN); // 8.취업규칙
			}
			elseif($args['sch_kind'] == 9) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND); // 9.상담유형
			}
			elseif($args['sch_kind'] == 10) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT); // 10.처리결과
			}

			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 .'상담건수'. CFG_AUTH_CODE_DELIMITER_2 .'퍼센트';

			// cell width
			$cell_width = '40●40●40';
			// cell에 데이터 할당
			$row = 2;
			$cell_index = 0;
			$tot = 0;
			foreach($args['data']['data_tot'][0] as $key => $rs) {
				if($cell_index == 2){	
					$row++;
					$cell_index = 0;
				}				
				$dbl_alpha_char = '';
				$alpha_index = 0;
				
				$str = $dbl_alpha_char . chr($alphabet + $cell_index) . $row;
				$val = is_hangul($rs) ? ($rs=='총계'&&$args['sch_kind']==9?'총계(실건수)':$rs) : mixedNumberToCurrency($rs);
				$sheet->setCellValue($str, $val);
				if($cell_index == 1){
					$tot = round(($rs/$args['data']['data_tot'][0]->tot)*100, 1);
					$tot .= ' %';
					$sheet->setCellValue('C'.$row, $tot);
				}
				$cell_index++;
			}
			$row++;
		}
		
		// 교차통계
		else if($kind == 'down03') {
			//통계유형 구분
			if($args['sel_col'] == 0){
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); //상담방법
			}
			elseif($args['sel_col'] == 1) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT); //근로자수
			}
			elseif($args['sel_col'] == 2) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND); //업종
			}
			elseif($args['sel_col'] == 3) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD); //운영기간
			}
			elseif($args['sel_col'] == 4) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN); //근로계약서
			}
			elseif($args['sel_col'] == 5) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN); //4대보험
			}
			elseif($args['sel_col'] == 6) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN); //취업규칙
			}
			elseif($args['sel_col'] == 7) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND); //상담유형
			}
			elseif($args['sel_col'] == 8) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); //소재지
			}

			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 . $tmp['header_nm'] . 
				CFG_AUTH_CODE_DELIMITER_2 .'총계'. ($args['sel_col'] == 7 ? '(실건수)' : '') .'(비율)';

			// cell width
			$cell_width = '40●'. $tmp['cell_width'] .'●20';
			// cell에 데이터 할당
			$row = 2;
			foreach($excel_data as $rs) {
				$loop = 1;
				foreach($rs as $item) {
					$str = getColFromNumber($loop++) . $row;
					$sheet->setCellValue($str, mixedNumberToCurrency($item));
				}
				$row++;
			}
			// 총계
			$data_tot = (array)$args['data']['data_tot'][0];
			$tot = trim(explode('(', $data_tot['tot'])[0]);
			$loop = 2;
			$tot_title = '총계'. ($args['csl_code'] == base64_encode(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND) ? '(실건수)' : '') .'(비율)';
			$sheet->setCellValue('A'. $row, $tot_title);//구분
			foreach ($data_tot as $idx => $item) {
				$str = getColFromNumber($loop++) . $row;
				$per = '';
				if($idx != 'tot') {
					$per = '('. round($item/$tot*100, 1) .'%)';
				}
				$sheet->setCellValue($str, mixedNumberToCurrency($item . $per));
			}
			$row++;//총계 영역을 센터정렬로 처리하기 위한조치
		}

		// 소속별통계
		else if($kind == 'down04') {

			//통계유형 구분
			if($args['sch_kind'] == 0){
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); //상담방법
			}
			elseif($args['sch_kind'] == 1) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); //소재지
			}
			elseif($args['sch_kind'] == 2) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT); //근로자수
			}
			elseif($args['sch_kind'] == 3) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND); //업종
			}
			elseif($args['sch_kind'] == 4) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD); //운영기간
			}
			elseif($args['sch_kind'] == 5) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN); //근로계약서
			}
			elseif($args['sch_kind'] == 6) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN); //4대보험
			}
			elseif($args['sch_kind'] == 7) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN); //취업규칙
			}
			elseif($args['sch_kind'] == 8) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND); //상담유형
			}
			elseif($args['sch_kind'] == 9) {
				$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT); //처리결과
			}
			
			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 . $tmp['header_nm'] . 
				CFG_AUTH_CODE_DELIMITER_2 .'총계'. ($args['sch_kind'] == 8 ? '(실건수)' : '') .'(비율)';		
			// cell width
			$cell_width = '40●'. $tmp['cell_width'] .'●20';
			
			// cell에 데이터 할당
			$row = 2;
			foreach ($excel_data as $rs) {
				$loop = 1;
				foreach($rs as $item) {
					$str = getColFromNumber($loop) . $row;
// 					echof('$loop : '. $loop .' / $str : '. $str .'/ $row : '. $row);
					$sheet->setCellValue($str, mixedNumberToCurrency($item));
					$loop++;
				}
				$row++;
			}
			// 총계
			$data_tot = (array)$args['data']['data_tot'][0];
			$tot = trim(explode('(', $data_tot['tot'])[0]);
			$loop = 2;
			$tot_title = '총계(비율)';
			$sheet->setCellValue('A'. $row, $tot_title);//구분
			foreach ($data_tot as $idx => $item) {
				$str = getColFromNumber($loop++) . $row;
				$per = '';
				if($idx != 'tot') {
					$per = '('. round($item/$tot*100, 1) .'%)';
				}
				$sheet->setCellValue($str, mixedNumberToCurrency($item . $per));
			}
			$row++;//총계 영역을 센터정렬로 처리하기 위한조치
		}
		
		// 월별통계
		else if($kind == 'down05') {
			$half_year = $args['half_year'];
			if($half_year == 1) { 
				$header_nm = '1월●2월●3월●4월●5월●6월●7월●8월●9월●10월●11월●12월';
				$cell_width = '10●10●10●10●10●10●10●10●10●10●10●10';
			}
			else if($half_year == 2) { 
				$header_nm = '1월●2월●3월●4월●5월●6월';
				$cell_width = '10●10●10●10●10●10';
			}
			else if($half_year == 3) { 
				$header_nm = '7월●8월●8월●10월●11월●12월';
				$cell_width = '10●10●10●10●10●10';
			}

			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 . $header_nm . CFG_AUTH_CODE_DELIMITER_2 .'총계';

			// cell width
			$cell_width = '40●'. $cell_width .'●20';

			// cell에 데이터 할당
			$row = 2;
			foreach ($excel_data as $rs) {
				$loop = 1;
				foreach($rs as $item) {
					$str = getColFromNumber($loop) . $row;
					// 					echof('$loop : '. $loop .' / $str : '. $str .'/ $row : '. $row);
					$sheet->setCellValue($str, mixedNumberToCurrency($item));
					$loop++;
				}
				$row++;
			}
			// 총계
			$data_tot = (array)$args['data']['data_tot'][0];
			$loop = 2;
			$sheet->setCellValue('A'. $row, "총계");//구분
			foreach ($data_tot as $idx => $item) {
				$str = getColFromNumber($loop++) . $row;
				$sheet->setCellValue($str, mixedNumberToCurrency($item));
			}
			$row++;//총계 영역을 센터정렬로 처리하기 위한조치			
		}

		// 시계열 통계
		else if($kind == 'down06') {
			// cell에 데이터 할당
			$row = 3;
			foreach($excel_data as $idx => $rs1) {
				$rs = (array)$rs1;
				$sheet->setCellValue('A'. $row, $rs['subject']);
				$alpha_char = 66;//B
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					$sheet->setCellValue(chr($alpha_char) . $row, number_to_currency($rs['ck_'. $y]));
					$sheet->setCellValue(chr($alpha_char+1) . $row, $rs['cr_'. $y] .'%');
					// 헤더 년도영역 셀 merge
					$sheet->mergeCells(chr($alpha_char) .'1:'. chr($alpha_char+1) .'1');
					$alpha_char += 2;
				}
				$row++;
			}
			$data_tot = (array)$args['data']['data_tot'];
			
			//총계
			$sheet->setCellValue('A'. $row, '총계'. ($args['sch_kind'] == 9 ? '(실건수)' : ''));
			$alpha_char = 66;//B
			// 마지막 row 총계;
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$t = $data_tot['ck_'. $y];
				$str_tot = '';
				if(strpos($t, "(") !== FALSE) {// 17639(14728)
					$arr = explode("(", $t);
					$arr2 = explode(")", $arr[1]);
					$str_tot = number_to_currency(trim($arr[0])) .'('. number_to_currency(trim($arr2[0])) .')'; 
				}
				else {
					$str_tot = number_to_currency($t);
				}
				$sheet->setCellValue(chr($alpha_char) . $row, $str_tot);
				$sheet->setCellValue(chr($alpha_char+1) . $row, $data_tot['cr_'. $y] .'%');
				$alpha_char += 2;
			}
			$row++;//총계 영역을 센터정렬로 처리하기 위한조치
			
		}				

		// 헤더 텍스트및 크기 설정 
		$index = 0;
		$alpha_index = 0;
		$dbl_alpha_char = '';
		$header_row = 1;

		// 시계열 통계
		if($kind == 'down06') {
			$header_row = 2;
			// header cell text, cell width
			$header_nm = ['구분',''];
			$cell_width = '50';
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$header_nm[0] .= CFG_AUTH_CODE_DELIMITER_2 . $y .'년'. CFG_AUTH_CODE_DELIMITER_2 . $y .'년';//아래 for문에서 동일하게 처리하기 위해 한번 더 추가함
				$header_nm[1] .= CFG_AUTH_CODE_DELIMITER_2 .'건수'. CFG_AUTH_CODE_DELIMITER_2 .'비율';
				$cell_width .= CFG_AUTH_CODE_DELIMITER_2 .'30'. CFG_AUTH_CODE_DELIMITER_2 .'30';
			}
			
			// header cell text
			$arr_header_nm = [[],[]];
			$arr_header_nm[0] = explode(CFG_AUTH_CODE_DELIMITER_2, $header_nm[0]);
			$arr_header_nm[1] = explode(CFG_AUTH_CODE_DELIMITER_2, $header_nm[1]);
			// cell width
			$arr_cell_width = explode(CFG_AUTH_CODE_DELIMITER_2, $cell_width);
			//
			for($i=0; $i<count($arr_cell_width); $i++) {
				// Z까지 갔다면 다시 앞에 A를 붙여 A부터 시작하게 한다.
				if($alphabet+$index > 90) {
					$index = 0;
					$dbl_alpha_char = chr($alphabet+$alpha_index++);
				}
				$alpha_char = $dbl_alpha_char . chr($alphabet + $index);
				$index++;
				// header cell text
				$sheet->setCellValue($alpha_char .'1', $arr_header_nm[0][$i]);
				$sheet->setCellValue($alpha_char .'2', $arr_header_nm[1][$i]);
				// cell width
				$sheet->getColumnDimension($alpha_char)->setWidth($arr_cell_width[$i]);
			}
			// 헤더 구분영역 셀 merge
			$sheet->mergeCells('A1:A2');
		}
		// 시계열 외
		else {
			// header cell text
			$arr_header_nm = explode(CFG_AUTH_CODE_DELIMITER_2, $header_nm);
			// cell width
			$arr_cell_width = explode(CFG_AUTH_CODE_DELIMITER_2, $cell_width);
			//
			for($i=0; $i<count($arr_header_nm); $i++) {
				// Z까지 갔다면 다시 앞에 A를 붙여 A부터 시작하게 한다.
				if($alphabet+$index > 90) {
					$index = 0;
					$dbl_alpha_char = chr($alphabet+$alpha_index++);
				}
				$alpha_char = $dbl_alpha_char . chr($alphabet + $index);
				$index++;
	
				// cell width
				$sheet->getColumnDimension($alpha_char)->setWidth($arr_cell_width[$i]);
	
				// header cell text
				$sheet->setCellValue($alpha_char .'1', $arr_header_nm[$i]);
			}
		}

		// 헤더에 Bold 처리
		$sheet->getStyle('A1:'. $alpha_char . $header_row)->getFont()->setBold(true);

		// cell 가로,세로 가운데 정렬
		$sheet->getStyle('A1:'. $alpha_char . ($row - 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A1:'. $alpha_char . ($row - 1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		// 상담사례 인 경우 주제어,상담내용,답변 셀은 좌측 정렬처리
		if($kind == 'down01') {
			$sheet->getStyle('AE2:AG'. ($row-1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		}

		// 헤더에 스타일 설정
		$sheet->getStyle('A1:'. $alpha_char . $header_row)->applyFromArray(
			array('fill'=>array(
					'type'=> PHPExcel_Style_Fill::FILL_SOLID
					,'color'=> array('rgb' => 'c5d9f1')
				),
				'borders' =>array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => 'aaaaaa')
					)
				)
				,'font' => array(
					'size' => 9
				)
			)
		);

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $args['path'] .'"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '. get_date() .' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
		exit;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// download2 - download2는 기존 download를 일부 개선한 코드로 download는 통계에서 계속 사용한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function download2($args) {

		$excel_data = $args['data'];
		$kind = $args['kind'];

		// excel form 로딩
		// excel file name
		$file_name = 'counsel_list.xlsx';
		$objPHPExcel = PHPExcel_IOFactory::load('./ci_app/views/excel_form/'. $file_name);
		
		// 문서 속성 설정
		$objPHPExcel->getProperties()
			->setTitle('Labors Counsel')
			->setSubject('Labor\'s Counsel Management Statistics Document')
			->setCreator('labors.or.kr')
			->setLastModifiedBy('labors.or.kr')
			->setDescription('Labor\'s Counsel Management Statistics Document');
		 
		// work sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();
		
		// 테두리 그리기 및 center 정렬 범위
		$set_style_range = '';

		// 다운로드할 파일 이름
		$download_filename = 'BizCounsel_List';

		// # 사용자상담 목록에서 상담다운로드
		// - 추가 2019.06.29 dylan
		if($kind == 'biz_counsel_list') {

			// 처음 시작하는 cell row 번호
			$row_num = 3;
			$cnt = count($excel_data);
			
			foreach ($excel_data as $key => $value) {
					
				$sheet->setCellValue('A'. $row_num, $cnt)
					->setCellValue('B'. $row_num, $value->csl_way)
					->setCellValue('C'. $row_num, $value->csl_title)
					->setCellValue('D'. $row_num, $value->oper_name)
					->setCellValue('E'. $row_num, $value->asso_name)
					->setCellValue('F'. $row_num, $value->csl_date)
					->setCellValue('G'. $row_num, $value->csl_content)
					->setCellValue('H'. $row_num, $value->csl_reply);
				// 추가 : 상담내용, 답변 2018.07.03 dylan
				$row_num++;
				$cnt--;
			}
			
			$set_style_range = 'A3:H'. ($row_num-1);

			// 번호,상담방법,상담자,소속,상담일 - 가운데 정렬
			$styleArray = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
			$sheet->getStyle('A3:B'. ($row_num-1))->applyFromArray($styleArray);
			$sheet->getStyle('D3:F'. ($row_num-1))->applyFromArray($styleArray);
			// 상담내용,답변 좌측정렬
			$styleArray = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
				)
			);
			$sheet->getStyle('G3:H'. ($row_num-1))->applyFromArray($styleArray);
		}

		// 셀 정렬, 폰트사이즈, 테두리 그리기
		if($set_style_range != '') {
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
				// ,'alignment' => array(
				// 	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				// )
				,'font' => array(
					'size' => 9
				)
			);
			$sheet->getStyle($set_style_range)->applyFromArray($styleArray);
		}
		
		// 파일 이름에 YmdHms 붙이기
		$YmdHms = get_date('YmdHms');

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Labors_'. $download_filename .'_'. $YmdHms .'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '. get_date() .' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setPreCalculateFormulas(false); //수식셀 사용안함(속도개선)
		
		ob_end_clean();
		ob_start();
		$objWriter->save('php://output');
		
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
		exit;

	}





	
	//-----------------------------------------------------------------------------------------------------------------
	// _gen_code_to_string : 서브코드를 받아 해당 코드의 이름을 문자열로 만들어 리턴한다.
	//-----------------------------------------------------------------------------------------------------------------
	private function _gen_code_to_string($code) {
		$header_nm = '';
		$cell_width = '';

		$sub_codes = $this->manager->get_sub_code_by_m_code($code);
		foreach($sub_codes as $code) {
			if($header_nm != '') $header_nm .= CFG_AUTH_CODE_DELIMITER_2;
			if($cell_width != '') $cell_width .= CFG_AUTH_CODE_DELIMITER_2;
			$header_nm .= $code->code_name;
			$cell_width .= '20';
		}
		return array('header_nm'=>$header_nm, 'cell_width'=>$cell_width);
	}
}
?>