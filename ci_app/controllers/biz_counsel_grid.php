<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// AES encrypt library
// require_once './lib/aes.class.php';
// require_once './lib/aesctr.class.php'; 

// ref Class
require_once 'admin.php';
// require_once 'auth.php';



/******************************************************************************************************
 * 운영상담 Class
 * Counsel
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Biz_Counsel extends Admin {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	

	//=================================================================================================================
	// construct
	//=================================================================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');

		// helper
		$this->load->helper('download');
	}
	
	
	
	
	//#################################################################################################################
	// Biz_Counsel
	//#################################################################################################################

	//=================================================================================================================
	// index
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}


	//-----------------------------------------------------------------------------------------------------------------
	// vdownload_excel : 상담 내역 Excel download view
	//-----------------------------------------------------------------------------------------------------------------
	public function vdownload_excel() {

		$param = $this->input->get(NULL, TRUE);
		
		// 다운로드 구분 - counsel_list, lawhelp_list<권리구제 리스트>
		$data['kind'] = $param['kind'];

		$this->load->view('adm_excel_down_popup_view', $data);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// download - 상담,권리구제 전용 엑셀 다운로드 함수
	//-----------------------------------------------------------------------------------------------------------------
	public function download() {

		// 엑셀 다운로드시 오류 문제로 추가 : 2019.05.09 dylan
		ini_set('memory_limit', '-1'); // default 120M
		ini_set('max_execution_time', 0); // default 60초
		
		$param = $this->input->post(NULL, TRUE);
		$kind = $param['kind'];

		// data 가져오기
		$rstRtn = $this->excel_data(array('kind'=>$kind));

		// excel 인스턴스 생성
		require_once 'excel.php';
		$excel = new Excel();
		$excel->download2(array('data'=>$rstRtn['data'], 'kind'=>$kind));

	}



	//-----------------------------------------------------------------------------------------------------------------
	// counsel_print : 상담 내역 인쇄 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_print() {

		$get = $this->input->get(NULL, TRUE);
		$seq = $get['seq'];

		$data = array(
			'seq' => $seq
		);
		$rstRtn = $this->manager->get_counsel_print($data);
		// echof($rstRtn['data']);

		$this->load->view('print_form/print_counsel', $rstRtn);
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// counsel_list_popup_view : 상담 내역 불러오기 팝업 LIST view page 호출
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_list_popup_view() {
//		self::_chk_session() ;

		// 검색항목 데이터
		// - 상담방법 코드
		$rstRtn['res']['csl_way'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
		// - 직종 코드
		$rstRtn['res']['csl_work_kind_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
		// - 업종 코드
		$rstRtn['res']['csl_comp_kind_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
		// - 처리결과 코드
		$rstRtn['res']['csl_proc_rst_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);
		// - 주제어 코드
		$rstRtn['res']['csl_keyword'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD);
		// - 검색 : 연도 데이터
		$rstRtn['res']['search_year'] = $this->manager->get_counsel_year();
		// - 수정시 상담 seq
		$param = $this->input->get(NULL, TRUE);
		$rstRtn['res']['seq'] = $param['seq'];

		$this->load->view('adm_counsel_list_popup_view', $rstRtn);
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// counsel_rel_view : 관련상담 리스트 팝업 view page 호출
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_rel_view() {
//		self::_chk_session() ;
		
		$param = $this->input->get(NULL, TRUE);
		$data['data']['seq'] = $param['seq'];
		$data['data']['csl_name'] = $param['nm'];
//		$data['data']['csl_tel'] = $param['tel'];
		
		$this->load->view('adm_counsel_rel_list_popup_view', $data);
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// counsel_list_popup : 불러오기, 관련상담 팝업 리스트 - 팝업에서 공유된 상담리스트 참조할때 사용
	//-----------------------------------------------------------------------------------------------------------------
	// 관련상담 팝업 페이지도 같이 사용, 이때는 $param에 nm, tel 파라메터가 넘어온다. 
	// * 수정 : 
	// 불러오기 에서 호출시 "공유"된 상담만 가져오도록 변경, 2017.01.18 by dylan
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_list_popup() {
				
		$param = $this->input->post(NULL, TRUE);

		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = 10;
		
		// where
		$where = array(
			'csl_share_yn'=> CFG_SUB_CODE_CSL_SHARE_YN
			,'offset' => $offset
			,'limit' => $limit
		);

		// <관련상담> 수정인 경우 - 자신의 글은 제외한다.
		$where['seq'] = isset($param['seq']) && $param['seq'] ? $param['seq'] : '';

		$where['is_master'] = isset($param['is_master']) && $param['is_master'] ? $param['is_master'] : 0;
		
		$where['target'] = isset($param['target']) && $param['target'] ? $param['target'] : '';

		// target 검색대상
		// - 처리결과 : 요청에 의해 추가 2015.08.14
		$where['target_csl_proc_rst'] = isset($param['target_csl_proc_rst']) && $param['target_csl_proc_rst'] != '' ? base64_decode($param['target_csl_proc_rst']) : '';
		// - 직종
		$where['target_work_kind'] = isset($param['target_work_kind']) && $param['target_work_kind'] != '' ? base64_decode($param['target_work_kind']) : '';
		// - 업종
		$where['target_comp_kind'] = isset($param['target_comp_kind']) && $param['target_comp_kind'] != '' ? base64_decode($param['target_comp_kind']) : '';
		// - 키워드
		$where['target_keyword'] = isset($param['target_keyword']) && $param['target_keyword'] != '' ? base64_decode($param['target_keyword']) : '';

		// 검색어
		$where['keyword'] = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// 상담방법 csl_way
		$where['csl_way'] = isset($param['csl_way']) && $param['csl_way'] ? base64_decode($param['csl_way']) : '';
		
		// search date	
		$where['search_date_begin'] = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';		
		$where['search_date_end'] = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';

		// 관련상담 팝업 페이지에서 호출한 경우
		if(isset($param['nm'])) {
			unset($where['csl_share_yn']); // 이 파라메터는 상담보기 페이지에서만 사용
			$where['csl_name'] = $param['nm'];
			// $where['csl_tel'] = $param['tel'];
		}

		// 마스터인 경우 
		// - 공유여부와 무관하게 가져온다.
		// - 소속코드 제거, 전체 다 볼 수 있게 한다.
		if($where['is_master'] == 1) {
			unset($where['csl_share_yn']);
			unset($where['asso_code']);
		}

		$rst = $this->manager->get_counsel_list_pop($where);
		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
// 		$rstRtn['data']['query'] = isset($rst['query']) ? $rst['query'] : '';
		
		// 검색용 데이터
		unset($where['offset']);
		unset($where['limit']);
		unset($where['seq']);
// 		$this->session->set_userdata(CFG_SESSION_SEARCH_DATA, $where);
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// counsel_list : 상담 LIST
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_list($rtn_data=FALSE) {
		
		$param = $this->input->post(NULL, TRUE);
		
		// page
		$page = isset($param['page']) ? (int)$param['page'] : $this->param_get('page', 1);
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
		);
		
		$where['is_master'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);

		// 주요상담사례 게시판에서 호출한 경우 - 1,0
		$where['is_board'] = isset($param['isbrd']) ? $param['isbrd'] : '';

		$where['target'] = isset($param['target']) && $param['target'] ? $param['target'] : '';

		// target 검색대상
		// - 처리결과 : 요청에 의해 추가 2015.08.14
		$where['target_csl_proc_rst'] = isset($param['target_csl_proc_rst']) && $param['target_csl_proc_rst'] != '' ? base64_decode($param['target_csl_proc_rst']) : '';
		// - 직종
		$where['target_work_kind'] = isset($param['target_work_kind']) && $param['target_work_kind'] != '' ? base64_decode($param['target_work_kind']) : '';
		// - 업종
		$where['target_comp_kind'] = isset($param['target_comp_kind']) && $param['target_comp_kind'] != '' ? base64_decode($param['target_comp_kind']) : '';
		// - 키워드
		$where['target_keyword'] = isset($param['target_keyword']) && $param['target_keyword'] != '' ? base64_decode($param['target_keyword']) : '';

		$where['keyword'] = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// 상담방법 csl_way
		$where['csl_way'] = isset($param['csl_way']) && $param['csl_way'] ? base64_decode($param['csl_way']) : '';
		
		// search date		
		$where['search_date_begin'] = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';		
		$where['search_date_end'] = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';

		// 요청에 의한 추가 : dylan 2017.09.12, danvistory.com
		// 검색 결과에서 내용본 뒤 목록으로 이동시 해당 페이지로 이동시키기 위함
		$where['page'] = $param['page'];
		
		// <캐싱처리> 검색정보가 캐시정보와 다르면 삭제
		if ($biz_counsel_search = $this->cache->get( $this->cache_prefix .'biz_counsel_list_search')){
			if(serialize($where) !== serialize($biz_counsel_search)) {
				$this->cache->delete( $this->cache_prefix .'biz_counsel_list');
				$this->cache->delete( $this->cache_prefix .'biz_counsel_list_search');
			}
		}
		$this->cache->save( $this->cache_prefix .'biz_counsel_list_search', $where, 300); // 초
		// <캐싱처리> 캐시데이터가 없으면 DB에서 가져온다.
		if ( ! $biz_counsel_list = $this->cache->get( $this->cache_prefix .'biz_counsel_list')){
			$tmp = $this->manager->get_counsel_list($where);
			$biz_counsel_list = $tmp;
			$this->cache->save( $this->cache_prefix .'biz_counsel_list', $biz_counsel_list, 300); // 초
		}
		
// 		$rstRtn['data']['csl'] = $biz_counsel_list['data'];
// 		$rstRtn['data']['tot_cnt'] = $biz_counsel_list['tot_cnt'];
		
// 		// for pagination
		$rstRtn['page'] = $page;
// 		$rstRtn['loop'] = $biz_counsel_list['tot_cnt'] - (($page - 1) * CFG_BOARD_PAGINATION_ITEM_PER_PAGE);
		
		$rst = $this->manager->get_counsel_list($where);
		$rstRtn['data']['csl'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
		$rstRtn['data']['query'] = isset($rst['query']) ? $rst['query'] : '';
// 		$this->session->set_userdata(CFG_SESSION_SEARCH_DATA, $where);
		
		// 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
		}
		else {
			$this->load->view('biz_counsel/list_view', $rstRtn);
		}
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// open_counsel_at : 상담 보기 - 통계->상담사례통계에서 자신의 상담보기시 사용
	//-----------------------------------------------------------------------------------------------------------------
	public function open_counsel_at() {
//		self::_chk_session();
				
		$param = $this->input->get(NULL, TRUE);
		
		$rstRtn['data'] = '';

		// where
		$where = array(
			'seq'=>$param['seq']
		);

		$rst = $this->manager->get_counsel_at($where);

		$rstRtn['data'] = $rst;

		$this->load->view('adm_statistic_csl_popup_view', $rstRtn);
	}


	//-----------------------------------------------------------------------------------------------------------------
	// add or edit
	//-----------------------------------------------------------------------------------------------------------------
	public function processing() {
//		self::_chk_session() ; //<== 이함수 실행으로 업로드가 안되는 문제가 발생한다...
		
		$param = $this->input->post(NULL, TRUE);

		// 파일 업로드 처리
		$rst_upload = $this->_upload();

		// return variables
		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';


		// 관련삼당 - 상담내용 가져오기 한 경우, 원글 seq
		$csl_ref_seq = isset($param['csl_ref_seq']) && $param['csl_ref_seq'] != '' ? $param['csl_ref_seq'] : '-1';

		// # 내담자 정보 encrypt 처리 - 양방향 처리 - 목록:검색시 문제가 되어 인코딩 처리 안함. 검색시 내담자선택하고 이름 입력시 이름을 인코딩해서 검색해야 하는데 인코딩할때마다 결과값이 달라지는 특성으로 검색이 안되는 문제가 있다.
		// 내담자 성명 
		$csl_name = $param['csl_name'];
		// if($csl_name != '') {
		// 	$csl_name = AesCtr::encrypt($csl_name, CFG_ENCRYPT_KEY, 256);
		// }
		// 내담자 연락처
		$tel = $param['csl_tel01'] .'-'. $param['csl_tel02'] .'-'. $param['csl_tel03'];
		// if($tel != '--') {
		// 	$tel = AesCtr::encrypt($tel, CFG_ENCRYPT_KEY, 256);
		// }

		// 저장 데이터 생성
		$data = array(
			'kind' => $param['kind']
			, 'csl_name' => $csl_name
			, 'csl_tel' => $tel
			, 'asso_code' => base64_decode($param['asso_code'])
			, 'csl_date' => $param['csl_date']
			, 'csl_ref_seq' => $csl_ref_seq 
			, 's_code' => base64_decode($param['s_code'])
			, 's_code_etc' => $param['s_code_etc']
			, 'gender' => base64_decode($param['gender'])
			, 'ages' => base64_decode($param['ages'])
			, 'ages_etc' => $param['ages_etc']
			, 'live_addr' => base64_decode($param['live_addr'])
			, 'live_addr_etc' => $param['live_addr_etc']
			, 'work_kind' => isset($param['work_kind']) ? base64_decode($param['work_kind']) : ''
			, 'work_kind_etc' => $param['work_kind_etc']
			, 'comp_kind' => base64_decode($param['comp_kind'])
			, 'comp_kind_etc' => $param['comp_kind_etc']
			, 'comp_addr' => base64_decode($param['comp_addr'])
			, 'comp_addr_etc' => $param['comp_addr_etc']
			, 'emp_kind' => base64_decode($param['emp_kind']) // - 고용형태 : 단일체크
			, 'emp_kind_etc' => $param['emp_kind_etc'] // - 고용형태 : 단일체크
			, 'emp_cnt' => base64_decode($param['emp_cnt'])
			, 'emp_cnt_etc' => $param['emp_cnt_etc']
			, 'emp_paper_yn' => base64_decode($param['emp_paper_yn'])
			, 'emp_insured_yn' => isset($param['emp_insured_yn']) ? base64_decode($param['emp_insured_yn']) : ''
			, 'ave_pay_month' => $param['ave_pay_month']
			, 'work_time_week' => $param['work_time_week']
			, 'csl_title' => $param['csl_title']
			, 'csl_content' => $param['csl_content']
			, 'csl_reply' => $param['csl_reply']
			, 'csl_proc_rst' => base64_decode($param['csl_proc_rst'])
			, 'csl_proc_rst_etc' => $param['csl_proc_rst_etc']
			, 'csl_share_yn' => base64_decode($param['csl_share_yn'])
			, 'is_del_file' => 0
			, 'file_name_org' => ''
			, 'file_name' => ''
		);
		// echof($data);
		// exit;

		// 상담이용동기
		if(isset($param['csl_motive_cd'])) {
			$data['csl_motive_cd'] = base64_decode($param['csl_motive_cd']);
		}
		// 상담이용동기 - 기타
		if(isset($param['csl_motive_etc'])) {
			$data['csl_motive_etc'] = $param['csl_motive_etc'];
		}

		// 상담유형 - 다중선택 가능
		$data['csl_sub_code']['arr_csl_kind'] = array();
		if(isset($param['csl_kind']) && $param['csl_kind'] != '') {
			$data['csl_sub_code']['arr_csl_kind'] = $param['csl_kind'];
		}

		// 주제어 - 다중선택 가능
		$data['csl_sub_code']['arr_keyword'] = array();
		if(isset($param['csl_keyword']) && $param['csl_keyword'] != '') {
			$data['csl_sub_code']['arr_keyword'] = $param['csl_keyword'];
		}

		// DB 처리 요청
		if($param['kind'] == 'edit') {
			$data['seq'] = $param['seq'];
		}
		else {
			// add 인 경우만 oper_id 저장
			$data['oper_id'] = $param['oper_id'];
		}

		// 처리결과 : 옴부즈만 지원신청 인 경우 메일 발송
		if( ! is_local() && $data['csl_proc_rst'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST_OMBUDSMAN) {
			$md = array(
				'from' => "system@labors.or.kr"
				,'from_nm' => "자동발송"
				,'to' => "sangdam@labors.or.kr"
				,'subject' => "[상담DB] 옴부즈만 권리구제 지원신청이 들어왔습니다."
				,'contents' => '<ul><li>내담자 : '. $csl_name 
					.'</li><li>상담일자 : '. $data['csl_date'] 
					.'</li><li>상담제목 : '. $data['csl_title'] 
					.'</li><li>상담내용 : '. $data['csl_content'] 
					.'</li><li>답변 : '. $data['csl_reply'] 
					.'</li></ul>'
			);
			$this->simple_mail($md);
		}

		// 첨부파일 처리
		$fail_cnt = 0;
		$up_file_names = '';
		$up_file_names_org = '';
		$err_msg = '';

		// DB 처리
		foreach($rst_upload as $up_data) {
			if($up_data['result'] == 'succ') {
				if($up_file_names != '') $up_file_names .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$up_file_names .= $up_data['file_name'];
				if($up_file_names_org != '') $up_file_names_org .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$up_file_names_org .= $up_data['file_name_org'];

			}
			else if($up_data['result'] == 'fail') {
				$fail_cnt++;
				if($err_msg != '') $err_msg .',';
				$err_msg .= $up_data['error'];
			}
		}

		// 1.업로드 실패한 파일이 1개라도 있는 경우, db 저장하지 않고 file 삭제한다.
		if($fail_cnt > 0) {
			if($up_file_names != '') {
				self::_processing_del_file($up_file_names);
			}
			else {
				$rstRtn['data']['msg'] = 'upload 실패';
				if($err_msg != '') $rstRtn['data']['msg'] .= '['. strip_tags($err_msg) .']';
			}
		}

		// 2.업로드가 있고 모두 성공한 경우, db 저장
		else if($up_file_names != '') {
			$data['file_name'] = $up_file_names;
			$data['file_name_org'] = $up_file_names_org;

			// 2-1.수정인 경우, 기존 파일명을 가져와 합쳐 저장할 데이터를 생성한다.
			if($param['kind'] == 'edit') {

				// 기존 파일 삭제할 때 DB에서 가져온 기존파일명 문자열에서 파일명을 제거한다.
				$exist_data = array(
					'seq' => $data['seq']
					,'table_name' => 'counsel'
				);
				$arr_exist_file_data = $this->manager->get_exist_file_data_board($exist_data);
				$exist_file_data = array('file_name'=>'','file_name_org'=>'');

				// 가져온 기존 파일명을 문자열로 만든다.
				foreach($arr_exist_file_data as $ext_file) {
					if($ext_file->file_name != '') {
						if($exist_file_data['file_name'] != '') $exist_file_data['file_name'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
						$exist_file_data['file_name'] .= $ext_file->file_name;
					}
					if($ext_file->file_name_org != '') {
						if($exist_file_data['file_name_org'] != '') $exist_file_data['file_name_org'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
						$exist_file_data['file_name_org'] .= $ext_file->file_name_org;
					}
				}

				// 2-1-1.기존 파일이 변경되는 수정인 경우, 실제파일명으로 기존 파일 삭제
				$chg_real_file_name = isset($param['chg_real_file_name']) ? $param['chg_real_file_name'] : '';
				if($chg_real_file_name != '')  {
					
					// 파일 삭제 데이터 생성
					$append_proc_data = array(
						'file_name'=>$exist_file_data['file_name']
						,'file_name_org'=>$exist_file_data['file_name_org']
					);
					// 파일 삭제 및 기존 파일명에서 삭제하는 파일명 제거
//					echof($chg_real_file_name, '변경되는 파일명');
//					echof($append_proc_data, '추가되는 파일명');

					$rst_arr_del_ext_file_name = self::_processing_del_file($chg_real_file_name, $append_proc_data);

					// 기존 파일명이 하나라도 남아 있으면 새로 업로드된 파일과 합친다.
					if($rst_arr_del_ext_file_name['file_name'] != '' && $rst_arr_del_ext_file_name['file_name_org'] != '' ) {
//						echof($data['file_name'], '업로드된 파일명-real');
//						echof($data['file_name_org'], '업로드된 파일명-org');
//						echof($rst_arr_del_ext_file_name['file_name'], '삭제한후 파일명-real');
//						echof($rst_arr_del_ext_file_name['file_name_org'], '삭제한후 파일명-org');
						$data['file_name'] = $rst_arr_del_ext_file_name['file_name'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name'];
						$data['file_name_org'] = $rst_arr_del_ext_file_name['file_name_org'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name_org'];
					}
					// 기존 파일명이 모두 제거된 경우, 새로 업로드된 파일만 저장된다.

				}
				// 2-1-2.파일 변경없이 추가만 하는 수정인 경우
				else {
					// 기존 파일명에 합친다.
					if($exist_file_data['file_name'] != '') {
						$data['file_name'] = $exist_file_data['file_name'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name'];
					}
					if($exist_file_data['file_name_org'] != '') {
						$data['file_name_org'] = $exist_file_data['file_name_org'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name_org'];
					}
				}
			}
			// 2-2.파일 변경이 없는 경우, 바로 저장
			// DB 저장
			$rstRtn = self::_processing_data($data);
		}

		// 3.업로드가 없는 경우, db 저장
		else {

			$rstRtn = self::_processing_data($data);
		}

		$this->load->view('json_view', $rstRtn);
	}



	/**
	 * [테스트] 상담 : 권리구제지원신청이 인 경우 메일 발송 테스트 용
	 *
	 */
	// public function sm() {
	// 	$md = array(
	// 		'from' => "system@labors.or.kr"
	// 		,'from_nm' => "자동발송"
	// 		,'to' => "delee@danvistory.com"
	// 		,'subject' => "[상담DB] 옴부즈만 권리구제 지원신청이 들어왔습니다."
	// 		,'contents' => '<ul><li>내담자 : aaaa' 
	// 			.'</li><li>상담제목 : 121212121212'
	// 			.'</li><li>상담내용 12121: '
	// 			.'</li><li>답변 : 1212121'
	// 			.'</li><li>공유여부 : 공유</li></ul>'
	// 	);
	// 	$this->simple_mail($md);
	// }



	//-----------------------------------------------------------------------------------------------------------------
	// _processing_data
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_data($args) {
		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';

		$kind = $args['kind'];
		unset($args['kind']);

		// db 저장
		if($kind == 'add') {
			$new_data = $this->manager->add_counsel($args);
			$new_id = $new_data['new_id'];

			if(!$new_id) {
				$rstRtn['data']['msg'] = 'DB저장 실패';
			}
			else {
				$rstRtn['data']['rst'] = 'succ';
			}
		}
		// edit
		else {
			$rst = $this->manager->edit_counsel($args);
			$rstRtn['data']['rst'] = 'succ';
		}

		// "관련상담" 처리 로직
		// 원글과 원글 가져와 작성된 상담1, 2, ...n 모두 공유항목을 업데이트 처리한다. 
		// 원글을 가져와 작성한 상담2,3,...n을 다시 가져와 작성한 경우도 마찬가지로 처리한다.
		if(isset($args['csl_ref_seq']) && $args['csl_ref_seq'] != '-1' && $args['csl_ref_seq'] != '' && $args['csl_ref_seq'] != '0') {
			// csl_ref_seq이 같은 모든 상담 업데이트
			$this->manager->edit_counsel_ref($args);

		}
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _processing_del_file : "|" 문자로 합쳐진 문자열을 받아 file을 삭제한다.
	//-----------------------------------------------------------------------------------------------------------------
	// param : $args 삭제할 파일의 이름 문자열
	// param : $append_args DB에서 가져온 기존 파일명(원본명,저장명) 데이터로 삭제할 파일의 파일명을 제거한다.
	// return : 삭제한 파일의 파일명을 제외한 기존 파일명 문자열
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_del_file($args, $append_args=NULL) {
		$rst_file_name = '';
		$rst_file_name_org = '';

		if(!is_null($append_args)) {
			$arr_file_name = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $append_args['file_name']);
			$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $append_args['file_name_org']);
		}

		// 삭제할 파일 데이터
		$arr_data = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $args);

		foreach($arr_data as $del_fname) {
			// 값이 있으면 실행 - delimiter가 없는 문자열을 자를 경우 빈 값이 하나 더 생긴다.
			if($del_fname != '') {
				// 파일 삭제
				@unlink(CFG_UPLOAD_PATH . $del_fname);
				clearstatcache();
				
				// DB에서 가져온 기존 파일명이 있는 경우
				if(!is_null($append_args)) {
					$index = 0;
					$index2 = 0;
					$arr_not_equal_fnm_real = array();
					$arr_not_equal_fnm_org = array();
					// 실제 파일명으로 비교한다. 원본파일명은 같은 이름이 있을 수 있기 때문..
					foreach($arr_file_name as $target_fname) {
						// 틀린 이름만 문자열로 생성
						if($del_fname != $target_fname) {
							$arr_not_equal_fnm_real[$index2] = $target_fname;
							$arr_not_equal_fnm_org[$index2] = $arr_file_name_org[$index];
							$index2++;
						}
						$index++;
					}
					// 틀린 파일명만 담은 배열로 기존 배열 갱신
					$arr_file_name = $arr_not_equal_fnm_real;
					$arr_file_name_org = $arr_not_equal_fnm_org;
				}
			}
		}

		$rstRtn['file_name'] = join(CFG_UPLOAD_FILE_NAME_DELIMITER, $arr_file_name);
		$rstRtn['file_name_org'] = join(CFG_UPLOAD_FILE_NAME_DELIMITER, $arr_file_name_org);

		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 여러 counsel 게시물 삭제
	// 다른 상담에서 참조글로 가져다 쓴 적이 있는지 체크하여 있으면 삭제시키지 않는다.(현업의견은 아님)
	// 위 기능을 끄려면 check_ref 변수를 FALSE 값 할당하면 됨
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		$data = array(
			'seqs' => $param['seqs']
			,'check_ref' => FALSE //<==========
		);

		$rst_tmp1 = $this->manager->del_multi_counsel($data);
		$rst_tmp2 = '';
		$len = 0;
		if(!empty($rst_tmp1)) {
			$rst_tmp2 = explode(CFG_AUTH_CODE_DELIMITER, $rst_tmp1);
			$len = count($rst_tmp2);
		}
		$rst = 'exist';
		
		// return a list data
		$rstRtn = self::counsel_list(TRUE);
		$rstRtn['data']['rst'] = $rst;
		$rstRtn['data']['len'] = $len;

		$this->load->view('json_view', $rstRtn);
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del : counsel 게시물 삭제
	// 다른 상담에서 참조글로 가져다 쓴 적이 있는지 체크하여 있으면 삭제시키지 않는다.(현업의견은 아님)
	// 위 기능을 끄려면 check_ref 변수를 FALSE 값 할당하면 됨
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
		// self::_chk_session();
		
		$param = $this->input->post(NULL, TRUE);
		
		$data['seqs'] = $param['seq'];

		$data['check_ref'] = FALSE; //<==========
		
		$rst = $this->manager->del_multi_counsel($data);
		

		$rstRtn['data']['rst'] = 'succ';

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_edit_view_data : 등록/수정 화면 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	// 권한 :
	// 소속코드 : asso_code - 자신이 속한 소속의 상담만 가져온다.
	// 고도화 - 위 조건 변경됨
	// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
	// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
	// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
	//-----------------------------------------------------------------------------------------------------------------
	// - 파일첨부 기능 고도화시 추가 기능이었으나 첨부파일 보안문제로 사용안한다고 함
	//-----------------------------------------------------------------------------------------------------------------
	public function get_edit_view_data($args=NULL) {
		if(is_null($args)) {
			$param = $this->input->get(NULL, TRUE);
		}
		else {
			$param['kind'] = $args['kind'];
			$param['id'] = $args['id'];
		}

		
		// 상담내역 항목 서브 코드 데이터 가져오기
		$rstRtn['data']['s_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
		$rstRtn['data']['csl_motive_cd'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE);
		$rstRtn['data']['gender'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);
		$rstRtn['data']['ages'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);
		$rstRtn['data']['work_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
		$rstRtn['data']['comp_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
		$rstRtn['data']['emp_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
		// $rstRtn['data']['emp_use_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND); // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
		$rstRtn['data']['emp_cnt'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
		$rstRtn['data']['emp_paper_yn'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
		$rstRtn['data']['emp_insured_yn'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
		$rstRtn['data']['csl_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);
		$rstRtn['data']['csl_keyword'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD);
		$rstRtn['data']['csl_share_yn'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_SHARE_YN);
		$rstRtn['data']['csl_addr'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
		$rstRtn['data']['csl_proc_rst'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);

		// 처리 구분 : add,view,edit
		$rstRtn['data']['kind'] = $param['kind'];

		// [권한] 체크용
		$rstRtn['data']['is_auth'] = 1;

		// [권한] 자신의 글인지 여부 플래그, yes:1, no:0
		$rstRtn['data']['is_owner'] = 0;
		
		// [권한] 수정,삭제 가능 여부 플래그, yes:1, no:0 - 서울시 계정만 해당됨 옴부즈만 상대
		// - 서울시 계정은 서울시 소속 그룹의 옴부즈만 글은 수정,삭제 가능하게 처리 요청 : 최진혁 2016.07.28
		$rstRtn['data']['is_editable'] = 0;
		
		// [권한] 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - 인쇄 여부 플래그, yes:1, no:0 <조회,인쇄만 가능>
		$rstRtn['data']['is_printable'] = 0;

		// [권한] 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - <조회,인쇄만 가능>하기 때문에 등록버튼 숨기기 위한 플래그
		$rstRtn['data']['is_only_view'] = 0;
		
		$rstRtn['edit']['seq'] = '';
		$rstRtn['edit']['asso_code'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);
		$rstRtn['edit']['asso_name'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_NM);
		$rstRtn['edit']['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
		$rstRtn['edit']['oper_name'] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
		$rstRtn['edit']['csl_date'] = '';
		$rstRtn['edit']['csl_name'] = '';
		$rstRtn['edit']['csl_tel01'] = '';
		$rstRtn['edit']['csl_tel02'] = '';
		$rstRtn['edit']['csl_tel03'] = '';
		$rstRtn['edit']['csl_ref_seq'] = '';
		$rstRtn['edit']['s_code'] = '';
		$rstRtn['edit']['s_code_etc'] = '';
		$rstRtn['edit']['csl_motive_cd'] = '';
		$rstRtn['edit']['csl_motive_etc'] = '';
		$rstRtn['edit']['gender'] = '';
		$rstRtn['edit']['ages'] = '';
		$rstRtn['edit']['ages_etc'] = ''; // 추가 2018.07.03 추가개발 건
		$rstRtn['edit']['live_addr'] = '';
		$rstRtn['edit']['live_addr_etc'] = '';
		$rstRtn['edit']['work_kind'] = '';
		$rstRtn['edit']['work_kind_etc'] = '';
		$rstRtn['edit']['comp_kind'] = '';
		$rstRtn['edit']['comp_kind_etc'] = '';
		$rstRtn['edit']['comp_addr'] = '';
		$rstRtn['edit']['comp_addr_etc'] = '';
		$rstRtn['edit']['emp_kind'] = '';
		$rstRtn['edit']['emp_kind_etc'] = '';
		// $rstRtn['edit']['emp_use_kind'] = ''; // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
		$rstRtn['edit']['emp_cnt'] = '';
		$rstRtn['edit']['emp_etc'] = ''; // 추가 2018.07.03 추가개발 건
		$rstRtn['edit']['emp_paper_yn'] = '';
		$rstRtn['edit']['emp_insured_yn'] = '';
		$rstRtn['edit']['ave_pay_month'] = '';
		$rstRtn['edit']['work_time_week'] = '';
		$rstRtn['edit']['csl_kind'] = '';
		$rstRtn['edit']['csl_title'] = '';
		$rstRtn['edit']['csl_content'] = '';
		$rstRtn['edit']['csl_reply'] = '';
		$rstRtn['edit']['csl_proc_rst'] = '';
		$rstRtn['edit']['csl_proc_rst_etc'] = '';
		$rstRtn['edit']['csl_share_yn'] = '';
		// 상담유형,주제어 코드
		$rstRtn['edit']['csl_sub_code'] = '';
		// 첨부파일 : 추가 2016.05.27 고도화
		$rstRtn['edit']['file_name_org'] = '';
		$rstRtn['edit']['file_name'] = '';
		$rstRtn['edit']['reg_date'] = '';

		
		// edit
		if($param['kind'] == 'edit' || $param['kind'] == 'view') {

			$data = array(
				'seq' => $param['id']
			);
			$rst = $this->manager->get_counsel($data);
			// echof( $rst );

			// [권한] 체크 - 통계>상담사례 - 엑셀 다운로드하여 링크로 접근하는 경우의 처리, 20160729 최진혁
			$rstRtn['data']['is_auth'] = 0;
			// master 제외
			if($this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER) != 1) {
				// 기본 - 권익센터,OO센터 직원 : 소속 상담만 가능
				if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK1) {
					if($this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) == $rst['data'][0]->asso_cd) {
						$rstRtn['data']['is_auth'] = 1;
					}
				}
				// 서울시 : 자신의 글과 옴부즈만 상담만 가능
				else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2) {
					if($rst['data'][0]->oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID) || $rst['data'][0]->oper_kind == CFG_OPERATOR_KIND_CODE_OK3) {
						$rstRtn['data']['is_auth'] = 1;
					}
				}
				// 옴부즈만 : 자신의 상담만 가능
				else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK3) { 
					if($rst['data'][0]->oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID)) {
						$rstRtn['data']['is_auth'] = 1;
					}
				}
				// 자치구공무원인 경우
				else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
					$rstRtn['data']['is_auth'] = 1;
				}
			}
			// master
			else {
				$rstRtn['data']['is_auth'] = 1;
			}

			// [권한] 자신의 글인지 여부 플래그, yes:1, no:0
			$rstRtn['data']['is_owner'] = $rst['data'][0]->oper_id == $this->session->userdata(CFG_SESSION_ADMIN_ID) ? 1 : 0;

			// [권한] 수정,삭제 가능 여부 플래그, yes:1, no:0 - 서울시 계정만 해당됨 옴부즈만 상대
			// - 서울시 계정은 서울시 소속 그룹의 옴부즈만 글은 수정,삭제 가능하게 처리 요청 : 최진혁 2016.07.28
			if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2 
				&& $rst['data'][0]->asso_cd == $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) 
				&& $rst['data'][0]->oper_kind == CFG_OPERATOR_KIND_CODE_OK3) {
				$rstRtn['data']['is_editable'] = 1;
			}

			// [권한] 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우 - 인쇄 여부 플래그, yes:1, no:0
			if($rst['data'][0]->oper_auth_grp_id == $this->session->userdata(CFG_SESSION_ADMIN_AUTH_GRP_ID)
				|| $rst['data'][0]->oper_kind == $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE)) {
				$rstRtn['data']['is_printable'] = 1;
			}

			// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 가진다.
			if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
				// 글 작성자의 운영자구분이 옴부즈만 이고 세부구분이 접속자와 같은 경우 인쇄 권한 부여
				if($rst['data'][0]->oper_kind == $this->session->userdata(CFG_OPERATOR_KIND_CODE_OK3)
					&& $rst['data'][0]->oper_kind_sub == $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE_SUB)) {
					$rstRtn['data']['is_printable'] = 1;

					// <조회,인쇄만 가능>하기 때문에 등록버튼 숨기기 위한 플래그
					$rstRtn['data']['is_only_view'] = 1;
				}
			}

			
			//
			$rstRtn['edit']['seq'] = $rst['data'][0]->seq;
			$rstRtn['edit']['asso_code'] = $rst['data'][0]->asso_code;
			$rstRtn['edit']['asso_name'] = $rst['data'][0]->asso_name;
			$rstRtn['edit']['oper_id'] = $rst['data'][0]->oper_id;
			$rstRtn['edit']['oper_name'] = $rst['data'][0]->oper_name;
			$rstRtn['edit']['csl_date'] = $rst['data'][0]->csl_date;

			$csl_name = $rst['data'][0]->csl_name;
			// 아래 인코딩방식은 매번 인코딩마다 결과값이 달라져 검색시 문제가 있어 주석처리 함
			// if($csl_name != '') {
			// 	$csl_name = AesCtr::decrypt($csl_name, CFG_ENCRYPT_KEY, 256);
			// }
			$rstRtn['edit']['csl_name'] = $csl_name;
	
			$csl_tel = $rst['data'][0]->csl_tel;
			// if($csl_tel != '') {
			// 	$csl_tel = AesCtr::decrypt($csl_tel, CFG_ENCRYPT_KEY, 256);
			// }			
			$rstRtn['edit']['csl_tel01'] = '';
			$rstRtn['edit']['csl_tel02'] = '';
			$rstRtn['edit']['csl_tel03'] = '';
			if(strpos($csl_tel, '-') !== FALSE) {
				$arr_tel = explode('-', $csl_tel);
				$rstRtn['edit']['csl_tel01'] = $arr_tel[0];
				$rstRtn['edit']['csl_tel02'] = $arr_tel[1];
				$rstRtn['edit']['csl_tel03'] = $arr_tel[2];
			}
			$rstRtn['edit']['csl_ref_seq'] = $rst['data'][0]->csl_ref_seq;
			$rstRtn['edit']['s_code'] = $rst['data'][0]->s_code;
			$rstRtn['edit']['s_code_etc'] = $rst['data'][0]->s_code_etc;
			$rstRtn['edit']['csl_motive_cd'] = $rst['data'][0]->csl_motive_cd;
			$rstRtn['edit']['csl_motive_etc'] = $rst['data'][0]->csl_motive_etc;
			$rstRtn['edit']['gender'] = $rst['data'][0]->gender;
			$rstRtn['edit']['ages'] = $rst['data'][0]->ages;
			$rstRtn['edit']['ages_etc'] = $rst['data'][0]->ages_etc; // 추가 2018.07.03 추가개발 건
			$rstRtn['edit']['live_addr'] = $rst['data'][0]->live_addr;
			$rstRtn['edit']['live_addr_etc'] = $rst['data'][0]->live_addr_etc;
			$rstRtn['edit']['work_kind'] = $rst['data'][0]->work_kind;
			$rstRtn['edit']['work_kind_etc'] = $rst['data'][0]->work_kind_etc;
			$rstRtn['edit']['comp_kind'] = $rst['data'][0]->comp_kind;
			$rstRtn['edit']['comp_kind_etc'] = $rst['data'][0]->comp_kind_etc;
			$rstRtn['edit']['comp_addr'] = $rst['data'][0]->comp_addr;
			$rstRtn['edit']['comp_addr_etc'] = $rst['data'][0]->comp_addr_etc;
			$rstRtn['edit']['emp_kind'] = $rst['data'][0]->emp_kind;
			$rstRtn['edit']['emp_kind_etc'] = $rst['data'][0]->emp_kind_etc;
			// $rstRtn['edit']['emp_use_kind'] = $rst['data'][0]->emp_use_kind; // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
			$rstRtn['edit']['emp_cnt'] = $rst['data'][0]->emp_cnt;
			$rstRtn['edit']['emp_cnt_etc'] = $rst['data'][0]->emp_cnt_etc; // 추가 2018.07.03 추가개발 건
			$rstRtn['edit']['emp_paper_yn'] = $rst['data'][0]->emp_paper_yn;
			$rstRtn['edit']['emp_insured_yn'] = $rst['data'][0]->emp_insured_yn;
			$rstRtn['edit']['ave_pay_month'] = $rst['data'][0]->ave_pay_month;
			$rstRtn['edit']['work_time_week'] = $rst['data'][0]->work_time_week;
//			$rstRtn['edit']['csl_kind'] = $rst['data'][0]->csl_kind;
			$rstRtn['edit']['csl_title'] = $rst['data'][0]->csl_title;
			$rstRtn['edit']['csl_content'] = $rst['data'][0]->csl_content;
			$rstRtn['edit']['csl_reply'] = $rst['data'][0]->csl_reply;
			$rstRtn['edit']['csl_proc_rst'] = $rst['data'][0]->csl_proc_rst;
			$rstRtn['edit']['csl_proc_rst_etc'] = $rst['data'][0]->csl_proc_rst_etc;
			$rstRtn['edit']['csl_share_yn'] = $rst['data'][0]->csl_share_yn;
			// 상담유형,주제어 코드
			$rstRtn['edit']['csl_sub_code'] = $rst['data']['csl_sub_code'];

			// 첨부파일 : 추가 2016.05.27 고도화
			$rstRtn['edit']['file_name_org'] = $rst['data'][0]->file_name_org;
			$rstRtn['edit']['file_name'] = $rst['data'][0]->file_name;
			
			// 추가 : 2017.01.18 dylan
			$rstRtn['edit']['reg_date'] = $rst['data'][0]->reg_date;
		}

		if(is_null($args)) {
			$this->load->view('adm_counsel_edit_view', $rstRtn);
		}
		else {
			return $rstRtn;
		}
	}



	//-----------------------------------------------------------------------------------------------------------------
	// counsel_help : 상담입력 > 항목 안내 팝업
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_help() {
		$data = $this->input->get(NULL, TRUE);
		$this->load->view($data['url']);
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		// $admin = new Admin();
		// $out = $admin->is_out_session();
		$out = $this->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				// return $admin->logout();
				return $this->logout();
			}
		}
		else {
			return $out;
		}
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_file : 특정 파일만 삭제하고 DB를 업데이트 한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function del_file() {

		$param = $this->input->post(NULL, TRUE);
		
		$table_name = 'counsel';

		// 기존 파일 삭제할 때 DB에서 가져온 기존파일명 문자열에서 파일명을 제거한다.
		$exist_data = array(
			'seq' => $param['sq']
			,'table_name' => $table_name
		);
		$arr_exist_file_data = $this->manager->get_exist_file_data_board($exist_data);
		$exist_file_data = array('file_name'=>'','file_name_org'=>'');

		// 가져온 기존 파일명을 문자열로 만든다.
		foreach($arr_exist_file_data as $ext_file) {
			if($ext_file->file_name != '') {
				if($exist_file_data['file_name'] != '') $exist_file_data['file_name'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$exist_file_data['file_name'] .= $ext_file->file_name;
			}
			if($ext_file->file_name_org != '') {
				if($exist_file_data['file_name_org'] != '') $exist_file_data['file_name_org'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$exist_file_data['file_name_org'] .= $ext_file->file_name_org;
			}
		}

		// 실제파일명으로 기존 파일 삭제
		// 파일 삭제 데이터 생성
		$append_proc_data = array(
			'file_name'=>$exist_file_data['file_name']
			,'file_name_org'=>$exist_file_data['file_name_org']
		);

		// 파일 삭제 및 기존 파일명에서 삭제하는 파일명 제거
		$rst_arr_del_ext_file_name = self::_processing_del_file($param['fnr'], $append_proc_data);

		// DB 저장
		$data = array(
			'seq' => $param['sq']
			,'kind' => 'edit'
			,'is_del_file' => 1
			,'file_name' => $rst_arr_del_ext_file_name['file_name']
			,'file_name_org' => $rst_arr_del_ext_file_name['file_name_org']
		);

		$rstRtn = self::_processing_data($data);

		$this->load->view('json_view', $rstRtn);
	}





	//=================================================================================================================
	// file 업로드,다운로드 helper
	//=================================================================================================================

	//-----------------------------------------------------------------------------------------------------------------
	// file upload
	//-----------------------------------------------------------------------------------------------------------------
	private function _upload() {
		
		$rstRtn = array();

		foreach ($_FILES as $index => $value){
			$rstRtn[$index]['file_name'] = '';
			$rstRtn[$index]['file_name_org'] = '';
			$rstRtn[$index]['error'] = '';
			$rstRtn[$index]['result'] = '';

			if ($value['name'] != ''){
				$this->load->library('upload');
				$this->upload->initialize($this->_set_upload_options());

				//upload the image
				if ( ! $this->upload->do_upload($index)){
					$rstRtn[$index]['error'] = $this->upload->display_errors();
					$rstRtn[$index]['result'] = 'fail';
				}
				else{
					$uploaded_data = $this->upload->data();
					$rstRtn[$index]['file_name'] = $uploaded_data['file_name'];
					$rstRtn[$index]['file_name_org'] = $uploaded_data['orig_name'];
					$rstRtn[$index]['result'] = 'succ';
				}
			}
		}
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// setting a file upload library
	//-----------------------------------------------------------------------------------------------------------------
	private function _set_upload_options() {
		$config = array();
		$config['upload_path'] = CFG_UPLOAD_PATH;
		$config['allowed_types'] = CFG_UPLOAD_ALL_ALLOW_EXT;
		$config['encrypt_name'] = TRUE;
		$config['overwrite'] = FALSE;
		$config['max_size'] = CFG_UPLOAD_MAX_FILE_SIZE;
		$config['max_width'] = 0; // no limit
		$config['max_height'] = 0; // no limit
		$config['max_filename'] = 0; // file name length, '0' is no limit
		$config['remove_spaces'] = TRUE;

		return $config;
	}
}
