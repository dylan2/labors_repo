<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit', -1); 

// admin Class
require_once 'admin.php';



/******************************************************************************************************
 * 운영자 Class
 * Statistic
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Statistic extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

	//========================================================================
	// construct
	//========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');
		
		// session
		// $this->load->library('session');
	}
	
	
	
	
	//########################################################################
	// Statistic
	//########################################################################
	
	//========================================================================
	// index
	//========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// st01 : 통계 1번째 - 상담사례
	//-----------------------------------------------------------------------------------------------------------------
	public function st01() {
		$param = $this->input->post(NULL, TRUE);
		
		$param['kind'] ='down01';
		$param['exclude_seq'] = 0;

		$rstRtn['data'] = self::_get_sttc_data($param);

		// return 
		$this->load->view('json_view', $rstRtn);

		unset($rstRtn['data']);
	}


	//-----------------------------------------------------------------------------------------------------------------
	// st11 : 통계 11번째 - 교차통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st11() {
		$param = $this->input->post(NULL, TRUE);
	
		$param['kind'] ='down11';
	
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// st12 : 통계 12번째 - 월별통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st12() {
		$param = $this->input->post(NULL, TRUE);
	
		$param['kind'] ='down12';
	
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// st13 : 통계 13번째 - 기본통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st13() {
		$param = $this->input->post(NULL, TRUE);
	
		$param['kind'] ='down13';
	
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// st07 : 통계 7번째 - 시계열통계
	//-----------------------------------------------------------------------------------------------------------------
	// 추가: 2019.07.22 dylan
	//-----------------------------------------------------------------------------------------------------------------
	public function st07() {
		$param = $this->input->post(NULL, TRUE);

		$param['kind'] = 'down07_1';// 건수,비율
		// 통계구분
		if($param['sttt_type'] == 2) {
			$param['kind'] = 'down07_2'; //임금근로시간
		}
		
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// st04 : 통계 4번째 - 임금근로시간통계 
	//-----------------------------------------------------------------------------------------------------------------
	// 추가: 2019.07.26 dylan
	//-----------------------------------------------------------------------------------------------------------------
	public function st04() {
		$param = $this->input->post(NULL, TRUE);

		$param['kind'] = 'down04';
		
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// st05 : 통계 5번째 - 소속별통계
	//-----------------------------------------------------------------------------------------------------------------
	// 추가: 2019.07.23 dylan
	//-----------------------------------------------------------------------------------------------------------------
	public function st05() {
		$param = $this->input->post(NULL, TRUE);

		$param['kind'] = 'down05';
		
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// _get_sttc_data
	//-----------------------------------------------------------------------------------------------------------------
	private function _get_sttc_data($args){

		// [상담사례] 에서 seq 제외 여부
		$exclude_seq = isset($args['not_seq']) && $args['not_seq'] != '' ? $args['not_seq'] : 0;

		$args['exclude_seq'] = $exclude_seq;

		// # 공통
		// 상담기간
		if(isset($args['search_date_begin']) && $args['search_date_begin'] != '') {
			$args['csl_date'] = 'C.csl_date BETWEEN "'. $args['search_date_begin'] .' 00:00:00" AND "'. $args['search_date_end'] .' 23:59:59" ';
			unset($args['search_date_begin']);
			unset($args['search_date_end']);
		}

		// 1.상담사례
		// - 공통 검색 항목으로 처리

		// # 2.상담방법
		// - 구분코드
		if(isset($args['csl_code']) && $args['csl_code'] != '') {
			if($args['csl_code'] != 'csl_date') {
				$args['csl_code'] = base64_decode($args['csl_code']);
			}
		}

		// # 3.상담방법
		// - 공통 검색 항목으로 처리

		return $this->manager->get_sttc_list($args);
	}
	
	

	//-----------------------------------------------------------------------------------------------------------------
	// s_code_popup_view : 상담자 리스트
	//-----------------------------------------------------------------------------------------------------------------
	public function oper_popup_view() {
		$param = $this->input->get(NULL, TRUE);
		// 다운로드 구분자
		$data['kind'] = $param['kind'];

		// 상담 방법 코드 data
		$data['res']['oper'] = $this->manager->get_oper_id_by_s_code($param['asso_code']);
		
		$this->load->view('adm_statistic_oper_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// s_code_popup_view : 상담방법 리스트
	//-----------------------------------------------------------------------------------------------------------------
	public function s_code_popup_view() {
		$param = $this->input->get(NULL, TRUE);
		// 다운로드 구분자
// 		$data['kind'] = $param['kind'];

		// 상담 방법 코드 data
		$data['res']['s_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);

		$this->load->view('adm_statistic_s_code_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// csl_proc_rst_popup_view : 처리결과 리스트
	//-----------------------------------------------------------------------------------------------------------------
	public function csl_proc_rst_popup_view() {
		$param = $this->input->get(NULL, TRUE);
		// 다운로드 구분자
// 		$data['kind'] = $param['kind'];

		// 상담 방법 코드 data
		$data['res']['csl_proc_rst'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);

		$this->load->view('adm_statistic_csl_proc_rst_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// asso_code_popup_view : 소속 리스트
	//-----------------------------------------------------------------------------------------------------------------
	public function asso_code_popup_view() {
		$param = $this->input->get(NULL, TRUE);
		// 다운로드 구분자
// 		$data['kind'] = $param['kind'];

		// 상담 방법 코드 data
		$data['res']['asso_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);

		$this->load->view('adm_statistic_asso_code_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// open_down_view : call a exel download view
	//-----------------------------------------------------------------------------------------------------------------
	public function open_down_view() {
		$param = $this->input->get(NULL, TRUE);
		// 다운로드 구분자
		$data['kind'] = $param['kind'];

		// 추가 2019.07.10, dylan
		// - csl_kind : 엑셀다운로드 팝업에서 다운로드 구분을 위한 파라메터(1:상담내역, 2:사용자상담)
		$data['csl_kind'] = 1;
		
		// 상담사례에만 쓰이는 파라메터
		$data['not_seq'] = '';
		if(isset($param['not_seq'])) {
			$data['not_seq'] = $param['not_seq'];
		}

		// 교차통계에서만 쓰이는 파라메터
		$data['sel_col'] = '';
		if(isset($param['sel_col'])) {
			$data['sel_col'] = $param['sel_col'];
		}

		$this->load->view('adm_statistic_excel_down_popup_view', $data);
	}

	

	//-----------------------------------------------------------------------------------------------------------------
	// download a excel
	//-----------------------------------------------------------------------------------------------------------------
	public function download() {
		
		$param = $this->input->post(NULL, TRUE);

		if($param['kind'] == 'down01') {
			$param['oper_asso_cd'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);
		}
		
		// data 가져오기
		if(isset($param['tableData'])) {// 임금근로시간 통계는 테이블 데이터로 엑셀 생성함 
			$param['data']['data'] = explode('●', $param['tableData']);
		}
		else {
			$param['data'] = self::_get_sttc_data($param);
		}
		
		// download url
		$YmdHms = get_date('YmdHms');
		$param['path'] = 'Labor_Counsel_Statistic_'. $YmdHms .'.xlsx';

		// excel 인스턴스 생성
		require_once 'excel.php';
		$excel = new Excel();
		$excel->download($param);
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		$admin = new Admin();
		$out = $admin->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				$rstRtn['data']['data'] = 'session_timeout';
				return $admin->logout();
			}
		}
		else {
			return $out;
		}
	}
}
