<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// ref Class
require_once 'admin.php';



/******************************************************************************************************
 * 
 * 
 * 
 * 성능개선 테스트용
 *
 * 
 *
 *****************************************************************************************************/
class Test__counsel extends Admin {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	

	//=================================================================================================================
	// construct
	//=================================================================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}


	//#################################################################################################################
	/**
	 *
	 * 
	 * 속도 개선 테스트용
	 *
	 * 
	 */
	
	public function counsel() {

		$rstRtn['data']['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
		$rstRtn['data']['oper_name'] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
		$rstRtn['data']['oper_asso_cd'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);

		$rstRtn['data']['selected_menu'] = '';

		$this->load->view('test__counsel_list_view', $rstRtn);
	}


	//-----------------------------------------------------------------------------------------------------------------
	// counsel_list : 상담 LIST
	//-----------------------------------------------------------------------------------------------------------------
	public function counsel_list() {
		
		$param = $this->input->post(NULL, TRUE);

		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// where
		$where = array(
			'offset' => $offset
			,'limit' => $limit
			,'table_name' => $param['table_name']
		);

		$where['is_master'] = isset($param['is_master']) && $param['is_master'] ? $param['is_master'] : 0;

		// 주요상담사례 게시판에서 호출한 경우 - 1,0
		$where['is_board'] = isset($param['isbrd']) ? $param['isbrd'] : '';

		$where['target'] = isset($param['target']) && $param['target'] ? $param['target'] : '';

		// target 검색대상
		// - 처리결과 : 요청에 의해 추가 2015.08.14
		$where['target_csl_proc_rst'] = isset($param['target_csl_proc_rst']) && $param['target_csl_proc_rst'] != '' ? base64_decode($param['target_csl_proc_rst']) : '';
		// - 직종
		$where['target_work_kind'] = isset($param['target_work_kind']) && $param['target_work_kind'] != '' ? base64_decode($param['target_work_kind']) : '';
		// - 업종
		$where['target_comp_kind'] = isset($param['target_comp_kind']) && $param['target_comp_kind'] != '' ? base64_decode($param['target_comp_kind']) : '';
		// - 키워드
		$where['target_keyword'] = isset($param['target_keyword']) && $param['target_keyword'] != '' ? base64_decode($param['target_keyword']) : '';

		$where['keyword'] = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// 상담방법 csl_way
		$where['csl_way'] = isset($param['csl_way']) && $param['csl_way'] ? base64_decode($param['csl_way']) : '';
		
		// search date		
		$where['search_date_begin'] = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';		
		$where['search_date_end'] = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';


		$rst = $this->get_counsel_list($where);

		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];		

		$this->session->set_userdata(CFG_SESSION_SEARCH_DATA, $where);
		
		$this->load->view('json_view', $rstRtn);
	}


	/**
	 *
	 * DB
	 *
	 * 
	 */
	public function get_counsel_list($args) {
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// 소속코드 : asso_code - 자신이 속한 소속의 상담만 가져온다.
		// 고도화 - 위 조건 변경됨
		// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
		// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
		// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
		$where = 'WHERE 1=1 ';


		// 주요상담사례 게시판에서 호출한 경우 - 공유된 전체 상담만 가져옴(권한과 무관)
		if($args['is_board'] == 1) {
			$where .= 'AND C.csl_share_yn = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
		}
		// master 제외 모든 상담자 대상
		else if($args['is_master'] != 1) {

			// 권한 부분 쿼리
			$where .= self::_get_counsel_comm_query();
		}


		// 검색 시작 ------------------------------------------------------
		$search = 'WHERE 1=1 ';
		$inner_join_counsel_keyword = '';
		$keyword = $args['keyword'];
		
		// 상담방법 : csl_way
		if($args['csl_way']) {
			$search .= 'AND Q1.s_code = "'. $args['csl_way'] .'" ';
		}

		// 상담일 : search_date_begin, search_date_end
		// - 양쪽 다 있는 경우
		if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
			$search .= 'AND Q1.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
			$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND Q1.csl_date LIKE "'. $begin_year[0] .'%" ';
		}

		// 검색
		// 수정 : 직종,업종 처리 추가 2017.02.10 dylan
		// 직종
		if($args['target'] == 'counsel_work_kind') {
			if($args['target_work_kind'] != '') {
				$search .= 'AND Q1.work_kind = "'. $args['target_work_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.work_kind_etc LIKE "%'. $keyword .'%" ';
			}

		}
		// 업종
		else if($args['target'] == 'counsel_comp_kind') {
			if($args['target_comp_kind'] != '') {
				$search .= 'AND Q1.comp_kind = "'. $args['target_comp_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.comp_kind_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 주제어 
		else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') { 
				$inner_join_counsel_keyword = 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq AND CS.s_code="'. $args['target_keyword'] .'" '
					.'INNER JOIN sub_code CS1 ON CS.s_code = CS1.s_code AND CS1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'"';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$inner_join_counsel_keyword = 'INNER JOIN (SELECT TCS.csl_seq FROM counsel_sub TCS INNER JOIN sub_code SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'" GROUP BY TCS.csl_seq) CS1 ON C.seq=CS1.csl_seq ';
			}
		}
		// 처리결과
		else if($args['target'] == 'counsel_proc_rst') {
			if($args['target_csl_proc_rst'] != '') {
				$search .= 'AND Q1.csl_proc_rst = "'. $args['target_csl_proc_rst'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 상담자 id - 2015.11.23 요청에 의한 추가 delee
		else if($args['target'] == 'counsel_oper_id') {
			if($keyword != '') {
				$search .= 'AND Q1.oper_id LIKE "%'. $keyword .'%" ';
			}
		}
		// 상담자명 
		else if($args['target'] == 'counsel_oper_name') {
			if($keyword != '') {
				$search .= 'AND Q1.oper_name LIKE "%'. $keyword .'%" ';
			}
		}
		// 추가 : dylan 2017.09.12, danvistory.com
		// 내담자 전화번호 뒤 4자리 검색
		else if($args['target'] == 'counsel_csl_tel') {
			if($keyword != '') {
				$search .= 'AND Q1.csl_tel LIKE "%'. $keyword .'%" ';
			}
		}
		// 전체검색
		else if($args['target'] == '') {
			if($keyword != '') {
				$search .= ' AND ( Q1.csl_title LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_content LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_reply LIKE "%'. $keyword .'%" '
					.'OR Q1.oper_name LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_name LIKE "%'. $keyword .'%" '
					.'OR Q1.work_kind_etc LIKE "%'. $keyword .'%" ' // 업종 기타
					.'OR Q1.comp_kind_etc LIKE "%'. $keyword .'%" ' // 직종 기타
					.'OR Q1.csl_tel LIKE "%'. $keyword .'%" ' // 내담자 전화번호 뒤 4자리 - 추가 : dylan 2017.09.12, danvistory.com
					.') ';
			}
		}
		else {
			if($keyword != '') {
				$search .= 'AND '. $args['target'] .' LIKE "%'. $keyword .'%" ';
			}
		}
		
		// 검색 끝 ------------------------------------------------------


		// fields
		// 필드명 변경, reg_date->csl_date - 2015.11.23 요청에 의한 변경 delee
		// 필드명 추가 : 원글 key 2017.02.01 dylan
		$fields2 = 'Q1.seq,Q1.csl_title,Q1.csl_content,Q1.csl_date,Q1.oper_name,Q1.csl_way'
			.',Q1.oper_id,Q1.oper_kind,Q1.csl_proc_rst,Q1.csl_name,Q1.csl_tel,Q1.gender,Q1.ages,Q1.ages_etc,Q1.live_addr,Q1.live_addr_etc,Q1.work_kind'
			.',Q1.work_kind_etc,Q1.comp_kind,Q1.comp_addr,Q1.comp_addr_etc,Q1.emp_kind,Q1.emp_kind_etc,Q1.emp_cnt,Q1.emp_cnt_etc,Q1.emp_paper_yn'
			.',Q1.emp_insured_yn,Q1.ave_pay_month,Q1.work_time_week,Q1.csl_reply,Q1.csl_share_yn,Q1.s_code,Q1.csl_ref_seq,Q1.S2code_name,Q1.S3code_name '
			.',Q1.csl_proc_rst_etc,Q1.S4code_name,Q1.comp_kind_etc,SC.code_name as asso_name ';
		$fields1 = 'C.seq,C.csl_title,C.csl_content,C.csl_date,O.oper_name,O.s_code as asso_code,S.code_name as csl_way'
			.',O.oper_id,O.oper_kind,C.csl_proc_rst,C.csl_name,C.csl_tel,C.gender,C.ages,C.ages_etc,C.live_addr,C.live_addr_etc,C.work_kind,C.work_kind_etc'
			.',C.comp_kind,C.comp_addr,C.comp_addr_etc,C.emp_kind,C.emp_kind_etc,C.emp_cnt,C.emp_cnt_etc,C.emp_paper_yn,C.emp_insured_yn'
			.',C.ave_pay_month,C.work_time_week,C.csl_reply,C.csl_share_yn,S.s_code,C.csl_ref_seq,S2.code_name as S2code_name,S3.code_name as S3code_name '
			.',C.csl_proc_rst_etc,S4.code_name as S4code_name,C.comp_kind_etc ';

		$query1 = 'SELECT '. $fields2 .' ';
		$query2 = 'FROM (SELECT '. $fields1 .' ' 
			.'FROM '. $args['table_name'] .' C '
			.$inner_join_counsel_keyword
			.'INNER JOIN sub_code S ON C.s_code = S.s_code ' // 상담방법
			.'INNER JOIN sub_code S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
			.'INNER JOIN sub_code S3 ON C.work_kind = S3.s_code ' // 직종
			.'INNER JOIN sub_code S4 ON C.comp_kind = S4.s_code ' // 업종
			.'INNER JOIN operator O ON C.oper_id = O.oper_id '
			.$where .') Q1 INNER JOIN sub_code SC ON Q1.asso_code=SC.s_code ';
		
		// 엑셀 다운로드 일 경우 limit 제외
		$orderby = ' ORDER BY Q1.csl_date DESC, Q1.seq DESC ';
		if(! isset($args['is_excel'])) {
			$orderby .= 'LIMIT '. $offset .', '. $limit;
		}

		$q = $query1 . $query2 . $search . $orderby ;
		// $rstRtn['query'] = $q;
		echof($q);

		// query
		$rs = $this->db->query($q);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$q =  $query1 . $query2 . $search;
		$rs = $this->db->query($q);
		$tmp = $rs->result();
		// $rstRtn['query2'] = $q;

		$rstRtn['tot_cnt'] = count($tmp);
		
		return $rstRtn;
	}

	
	/**
	 * 상담목록, 관련상담/불러오기 팝업 목록에서 사용하는 권한에 따른 공통 퀴리 생성 함수
	 *
	 */
	private function _get_counsel_comm_query() {
		$query = '';
		// 기본 - 권익센터,OO센터 직원 : 소속 상담만 노출
		if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK1) {
			$query .= 'AND O.s_code = "'. $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) .'" ';
		}
		// 서울시 : 자신의 글과 옴부즈만 상담만 노출(참고 : 서울시는 상담등록을 하지 않는다고 한다.)
		else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2) {
			$query .= 'AND ('
					.'O.oper_kind = "'. CFG_OPERATOR_KIND_CODE_OK3 .'" OR C.oper_id = "'. $this->session->userdata(CFG_SESSION_ADMIN_ID) .'"'
				.') ';
		}
		// 옴부즈만 : 자신의 상담만 노출
		else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK3) {
			$query .= 'AND C.oper_id = "'. $this->session->userdata(CFG_SESSION_ADMIN_ID) .'" ';
		}
		// 자치구 공무원 : 소속 자치구의 옴부즈만 상담만 조회,인쇄 가능 - 2016.08.09 추가
		else {
			$query .= 'AND ('
					.'O.oper_kind = "'. CFG_OPERATOR_KIND_CODE_OK3 .'" AND O.oper_kind_sub = "'. $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE_SUB) .'"'
				.') ';
		}

		return $query;
	}

}
