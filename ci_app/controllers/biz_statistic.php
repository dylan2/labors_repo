<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ini_set('memory_limit', -1); 

// admin Class
require_once 'admin.php';



/******************************************************************************************************
 * 사용자상담 통계 전용 Class
 * Biz_statistic
 * 작성자 : dylan
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Biz_statistic extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

	/**
	 * construct
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_biz_statistics', 'model');
	}
	
	
	/**
	 * index
	 */
	public function index() {
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// st01 : 통계 1번째 - 상담사례
	//-----------------------------------------------------------------------------------------------------------------
	public function st01() {
		$param = $this->input->post(NULL, TRUE);
		
		$param['kind'] ='down01';
		$param['exclude_seq'] = 0;

		$rstRtn['data'] = self::_get_sttc_data($param);

		// return 
		$this->load->view('json_view', $rstRtn);

		unset($rstRtn['data']);
	}
	

	//-----------------------------------------------------------------------------------------------------------------
	// st02 : 기본통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st02() {
		$param = $this->input->post(NULL, TRUE);

		$param['kind'] ='down02';

		$rstRtn['data'] = self::_get_sttc_data($param);

		// return 
		$this->load->view('json_view', $rstRtn);
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// st03 : 교차통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st03() {
		$param = $this->input->post(NULL, TRUE);
	
		$param['kind'] ='down03';
	
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// st04 : 소속별통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st04() {
		$param = $this->input->post(NULL, TRUE);
	
		$param['kind'] = 'down04';
	
		$rstRtn['data'] = self::_get_sttc_data($param);
	
		// return
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// st05 : 월별통계
	//-----------------------------------------------------------------------------------------------------------------
	public function st05() {
		$param = $this->input->post(NULL, TRUE);
		
		$param['kind'] ='down05';

		$rstRtn['data'] = self::_get_sttc_data($param);

		// return 
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// st06 : 시계열통계 - 건수/비율
	//-----------------------------------------------------------------------------------------------------------------
	public function st06() {
		$param = $this->input->post(NULL, TRUE);
		
		$param['kind'] ='down06';

		$rstRtn['data'] = self::_get_sttc_data($param);

		// return 
		$this->load->view('json_view', $rstRtn);
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// _get_sttc_data
	//-----------------------------------------------------------------------------------------------------------------
	private function _get_sttc_data($args){

		// [상담사례] 에서 seq 제외 여부
		$exclude_seq = isset($args['not_seq']) && $args['not_seq'] != '' ? $args['not_seq'] : 0;

		$args['exclude_seq'] = $exclude_seq;

		// 통계 화면의 검색시 전달되는 파라메터명과 일치시키기 위한 처리
		// - 상담일
		if(isset($args['search_date_begin']) && $args['search_date_begin'] != '') {
			$args['begin'] = $args['search_date_begin'];
			unset($args['search_date_begin']);
			$args['end'] = $args['search_date_end'];
			unset($args['search_date_end']);
		}
		// * 상담에서 검색시 파라메터를 통계에서 사용하는 파라메터로 일치시킨다.// BEGIN
		// - 소속
		if(isset($args['sch_asso_code'])) {
			$args['asso_code'] = $args['sch_asso_code'];
			unset($args['sch_asso_code']);
		}
		// - 상담방법
		if(isset($args['sch_s_code'])) {
			$args['s_code'] = $args['sch_s_code'];
			unset($args['sch_s_code']);
		}
		// - 처리결과
		if(isset($args['sch_csl_proc_rst'])) {
			$args['csl_proc_rst'] = $args['sch_csl_proc_rst'];
			unset($args['sch_csl_proc_rst']);
		}
		// * 상담에서 검색시 파라메터를 통계에서 사용하는 파라메터로 일치시킨다.// END

		// - 구분코드
		if(isset($args['csl_code']) && $args['csl_code'] != '') {
			if($args['csl_code'] != 'csl_date') {
				$args['csl_code'] = base64_decode($args['csl_code']);
			}
		}
		else {
			$args['csl_code'] = '';
		}
		
		return $this->model->get_sttc_list($args);
	}


	//-----------------------------------------------------------------------------------------------------------------
	// 소속 선택 팝업
	//-----------------------------------------------------------------------------------------------------------------
	public function asso_code_popup_view() 
	{
		$param = $this->input->get(NULL, TRUE);

		// 상담 방법 코드 data
		$data['res']['asso_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);

		$this->load->view('adm_statistic_asso_code_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// 상담방법 선택 팝업
	//-----------------------------------------------------------------------------------------------------------------
	public function s_code_popup_view() {
		$param = $this->input->get(NULL, TRUE);

		// 상담 방법 코드 data
		$data['res']['s_code'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);

		$this->load->view('adm_statistic_s_code_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// 처리결과 선택 팝업
	//-----------------------------------------------------------------------------------------------------------------
	public function csl_proc_rst_popup_view() {
		$param = $this->input->get(NULL, TRUE);

		// 상담 방법 코드 data
		$data['res']['csl_proc_rst'] = $this->model->get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);

		$this->load->view('adm_statistic_csl_proc_rst_popup_view', $data);
	}

	//-----------------------------------------------------------------------------------------------------------------
	// open_down_view : call a exel download view
	//-----------------------------------------------------------------------------------------------------------------
	public function open_down_view() {
		$param = $this->input->get(NULL, TRUE);

		// 다운로드 구분자
		$data['kind'] = $param['kind'];
		// 추가 2019.07.10, dylan
		// - csl_kind : 엑셀다운로드 팝업에서 다운로드 구분을 위한 파라메터(1:상담내역, 2:사용자상담)
		$data['csl_kind'] = 2;

		// 상담사례에만 쓰이는 파라메터
		$data['not_seq'] = '';
		if(isset($param['not_seq'])) {
			$data['not_seq'] = $param['not_seq'];
		}

		$this->load->view('adm_statistic_excel_down_popup_view', $data);
	}

	

	//-----------------------------------------------------------------------------------------------------------------
	// 통계 excel 다운로드 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function download() {
		
		$param = $this->input->post(NULL, TRUE);

		if($param['kind'] == 'down01') {
			$param['oper_asso_cd'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);
		}
// 		echof($param);
// 		exit;
		
		// 출력할 데이터 생성
		$param['data'] = self::_get_sttc_data($param);
// 		echof($param['data']);
		
		// download url
		$YmdHms = get_date('YmdHms');
		$url = 'Labor_BizCounsel_Statistic_'. $YmdHms .'.xlsx';
		$param['path'] = $url;

		// excel 인스턴스 생성
		require_once 'biz_excel.php';
		$excel = new Biz_excel();
		$excel->download($param);
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		$admin = new Admin();
		$out = $admin->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				$rstRtn['data']['data'] = 'session_timeout';
				return $admin->logout();
			}
		}
		else {
			return $out;
		}
	}
}
