<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// admin Class
// require_once 'admin.php';

// PHPExcel 라이브러리
// require_once './lib/php_excel/PHPExcel.php';

// AES encrypt library
require_once './lib/aes.class.php';
require_once './lib/aesctr.class.php'; 



/******************************************************************************************************
 * Excel 생성 파일
 * Excel
 * 작성자 : delee
 *
 *
 *
 *****************************************************************************************************/
class Chart_download extends CI_Controller {
	

	//========================================================================
	// construct
	//========================================================================
	public function __construct() {
		parent::__construct();
	}
	
	
	
	
	//########################################################################
	// Excel
	//########################################################################
	
	//========================================================================
	// index
	//========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}

	/**
	 * 차트 다운로드 메서드
	 */
	public function download() {
		
		$data = $_POST['dataUrl'];
		
		self::__download($data);
	}
	
	
	/**
	 * 실제 다운로드 처리
	 */
	protected function __download(&$args) {

		$str = str_replace('data:image/png;base64,', '', $args);
		$str = str_replace(' ', '+', $str);
		$img = base64_decode($str);
		
		$filename = md5(uniqid()) . '.png';
		$filepath = CFG_UPLOAD_PATH . $filename;
		file_put_contents($filepath, $img);
		
		//이미지 파일 다운로드
		$filesize = filesize($filepath);
		
		header("Pragma: public");
		header("Expires: 0");
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: $filesize");
		
		readfile($filepath);
		
		//이미지 파일 서버에서 삭제
		@unlink($filepath);
	}
}
?>