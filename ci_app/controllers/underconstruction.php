<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/******************************************************************************************************
 * 
 * 작업 안내 컨트롤러
 * 
 * 
 *****************************************************************************************************/
class Underconstruction extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
	
	}
	
	
	public function index() {

		$this->load->view('underconstruction_view');
	}
	
}
