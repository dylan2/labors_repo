<?php



require_once './lib/aes.class.php';
require_once './lib/aesctr.class.php'; 



class Test extends CI_Controller {

	function __construct() {
		parent::__construct();


		$this->load->database();
		
		// base model
		$this->load->model('db_encode');

    }


	public function gen_id() {
		// $pwd = 'ugOkYW/9EFd6FPHW';

		// $dec_pwd = AesCtr::decrypt($pwd, CFG_ENCRYPT_KEY, 256);
		// echof($dec_pwd);

	}


	public function test() {
		// print_r( $_SERVER );
		echo basename($_SERVER['SCRIPT_NAME']) .'<br>';
		echo $_SERVER['HTTP_HOST'] .'<br>';
	}




	public function encode_csl_data() {
		
		$rst = $this->db_encode->get_csl_data();

		foreach ($rst as $key => $value) {
			$enc_csl_name = trim($value->csl_name);
			if($enc_csl_name != '') {
				$enc_csl_name = AesCtr::encrypt($value->csl_name, CFG_ENCRYPT_KEY, 256);
			}

			$enc_csl_tel = trim($value->csl_tel);
			if($value->csl_tel != '' && $value->csl_tel != '--') {
				$enc_csl_tel = AesCtr::encrypt($value->csl_tel, CFG_ENCRYPT_KEY, 256);
			}

			$data = array(
				'csl_name' => $enc_csl_name
				,'csl_tel' => $enc_csl_tel
				,'seq' => $value->seq
			);
			$this->db_encode->update_csl_data($data);
		
		}

		echof('ok');


	}





	public function decode_csl_data() {
		
		$rst = $this->db_encode->get_csl_data2();

		foreach ($rst as $key => $value) {
			$enc_csl_name = trim($value->csl_name);
			if($enc_csl_name != '') {
				$enc_csl_name = AesCtr::decrypt($value->csl_name, CFG_ENCRYPT_KEY, 256);
			}

			$enc_csl_tel = trim($value->csl_tel);
			if($value->csl_tel != '' && $value->csl_tel != '--') {
				$enc_csl_tel = AesCtr::decrypt($value->csl_tel, CFG_ENCRYPT_KEY, 256);
			}

			$data = array(
				'csl_name' => $enc_csl_name
				,'csl_tel' => $enc_csl_tel
				,'seq' => $value->seq
			);
			$this->db_encode->update_csl_data($data);
		
		}

		echof('ok');


	}	

}



?>