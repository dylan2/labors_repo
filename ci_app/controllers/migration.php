<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/******************************************************************************************************
 * 데이터 수정용 클래스
 * 작성자 : dylan
 *
 *****************************************************************************************************/

class Migration extends CI_Controller {
	
	var $rstRtn = NULL;
	
	/**
	 * Construct
	 * 
	 */
	public function __construct() {
		parent::__construct();
		
		$this->load->database();

		// htaccess 파일 실행안되는 서버 설정으로 아래 적용안됨
// 		ini_set('memory_limit', '-1'); // default 120M
// 		ini_set('max_execution_time', 0); // default 60초
// 		ini_set('max_input_time', 120); // default 60
// 		ini_set('max_input_vars', 3000); // default 1000
// 		ini_set('post_max_size', '200M'); // default 110M
		
		$this->rstRtn['data']['is_master'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);
		$this->rstRtn['data']['selected_menu'] = "";
		
	}
	
	

	/**
	 * index
	 *
	 */
	public function index() {
	}


	/**
	 * 수정 화면
	 * 
	 * 상담내역 - 임금,근로시간 비정형데이터 수정용
	 * 
	 * @param void
	 * @return void
	 */
	public function pay() {
		
		$this->load->view('migration/pay_worktime_view', $this->rstRtn);
	}

	/**
	 * 목록
	 *
	 * 상담내역 - 임금,근로시간 비정형데이터 수정용
	 * 
	 * @param void
	 * @return void
	 */
	public function pay_lists() {
	
		$columns = "select  A.seq, B.oper_name, C.code_name, A.ave_pay_month,A.ave_pay_month_org, A.work_time_week,A.work_time_week_org,A.csl_content, A.csl_reply, A.csl_date ";
		
		// 임금, 근로시간 원본 데이터로 조회한다.
		$q = "from counsel A
			inner join operator B ON A.oper_id=B.oper_id
			inner join sub_code C ON A.asso_code=C.s_code
			where ";
		// 3차 임금 5백만원이상, 한글,특수문자 재처리
		$q .= "(ave_pay_month <> '' AND ave_pay_month > 0 AND REPLACE(ave_pay_month, ',', '') >= 5000000) OR
			(ave_pay_month <> '' AND ave_pay_month > 0 AND REPLACE(ave_pay_month, ',', '') < 100000) OR
			(work_time_week <> '' AND work_time_week > 0 AND REPLACE(work_time_week, ',', '') >= 100) OR
			(( A.ave_pay_month_org <> '' OR A.work_time_week_org <> '')
			AND ( A.ave_pay_month_org REGEXP '[가-힣]|[.]+|[\+]+|[~]+' OR 
			A.work_time_week_org REGEXP '[가-힣]|[.]+|[\+]+|[~]+' )	)";
		// 2차 임금,근로시간 입력자체가 잘못된 건 처리
// 		$q .= "(ave_pay_month <> '' AND ave_pay_month > 0 AND REPLACE(ave_pay_month, ',', '') < 100000) OR
// 			 (work_time_week <> '' AND work_time_week > 0 AND REPLACE(work_time_week, ',', '') >= 100)";
		// 1차 한글,특수문제 제거
// 		$q .= "A.reg_date < '2019-06-18 00:00:00' "
// 		$q .= "A.reg_date >= '2019-06-18 00:00:00' 
// 		$q .= "(A.ave_pay_month_org <> '' OR A.work_time_week_org <> '')
// 			AND ( A.ave_pay_month_org REGEXP '[가-힣]|[.]+|[\+]+|[~]+' OR 
// 			     A.work_time_week_org REGEXP '[가-힣]|[.]+|[\+]+|[~]+' ) ";
		$q .= "order by C.code_name ASC, B.oper_name ASC";
		$rst = $this->db->query($columns . $q);
		$this->rstRtn['data']['data'] = $rst->result();
		
		$columns = "select  count(*) as cnt ";
		$rst = $this->db->query($columns . $q);
		$this->rstRtn['data']['tot_cnt'] = $rst->result()[0]->cnt;
		
		$this->load->view('json_view', $this->rstRtn);
	}

	/**
	 * 저장
	 *
	 * 상담내역 - 임금,근로시간 비정형데이터 수정용
	 * 
	 * @param void
	 * @return void
	 */
	public function pay_edit() {
		
		$param = $this->input->post(NULL, TRUE);
		
		$data = array();
		foreach ($param['seq'] as $i => $v) {
			$data[$i] = array();
			$data[$i]['seq'] = $v;
			$data[$i]['ave_pay_month'] = $param['ave_pay_month'][$i];
			$data[$i]['work_time_week'] = $param['work_time_week'][$i];
		}
		
		$this->db->update_batch('counsel', $data, "seq");
		
		$rstRtn['data']['rst'] = 'succ';
		
		$this->load->view('json_view', $rstRtn);
	}

	
	/**
	 * 
	 * 상담내역: 내담자명, 연락처 암호화 업데이트 처리
	 * 
	 */
	public function enc_counsel() {
		
		// 처리요청구분, n:이름, t:전번
		$kind = @$_GET['kind'];
		if(empty($kind) || !$kind) {
			echo "요청구분을 입력하세요.";
			exit;
		}
		
		$key = self::get_key();
		
		$column = $kind == 'n' ? 'csl_name_org' : 'csl_tel_org';
		$rst = $this->db->select("seq,". $column)
			->from('counsel C')
			->where($column. '!=""', null, false)
			->order_by($column)
			->get();
		$rstRtn = $rst->result();
// 		echof($rstRtn);
// 		exit;
		
		// tel
		if($kind == 't') {
			foreach ($rstRtn as $i => $items) {
				$item = (array)$items;
				$arr_tel = explode("-", $item['csl_tel_org']);
				echo ($i .' ok..'. $item['seq'] .','. $item['csl_tel_org']);
				if(isset($arr_tel[2]) && $arr_tel[2] != '' && is_numeric($arr_tel[2])) {
					$this->db->set('csl_tel', "AES_ENCRYPT(TRIM(csl_tel_org), HEX(SHA2('$key',512)))", FALSE);
					$this->db->set('csl_tel_sch', "AES_ENCRYPT(TRIM('". $arr_tel[2] ."'), HEX(SHA2('$key',512)))", FALSE);
					$this->db->where(['seq'=>$item['seq']]);
					$this->db->update('counsel');
				}
				else {
					echo ' - Bad number ==> '. @$arr_tel[2];
				}
				echo '<BR>';
			}
			
		}
		// csl_name
		else {
			foreach ($rstRtn as $i => $items) {
				$item = (array)$items;
				echo ($i .' ok..'. $item['seq'] .','. $item['csl_name_org'] .'<BR>');
				$this->db->set('csl_name', "AES_ENCRYPT(TRIM(csl_name_org), HEX(SHA2('$key',512)))", FALSE);
				$this->db->where(['seq'=>$item['seq']]);
				$this->db->update('counsel');
			}
			
		}
		
	}

	/**
	 *
	 * 권리구제 : 신청자,거주지,회사명 암호화 업데이트 처리
	 *
	 */
	public function enc_lawhelp() {
		
		$key = self::get_key();
	
		$column = 'lh_apply_nm_org,lh_apply_addr_org,lh_apply_comp_nm_org,lh_comp_addr_org';
		$rst = $this->db->select("seq,". $column)
			->from('lawhelp')
			->or_where('lh_apply_nm_org !=""', null, false)
			->or_where('lh_apply_addr_org !=""', null, false)
			->or_where('lh_apply_comp_nm_org !=""', null, false)
			->order_by('lh_apply_nm')
			->get();
		$rstRtn = $rst->result();
// 		echof($this->db->last_query());
// 		echof($rstRtn);
// 		exit;
	
		foreach ($rstRtn as $i => $item) {
			$this->db->set('lh_apply_nm', "AES_ENCRYPT(TRIM('". $item->lh_apply_nm_org ."'), HEX(SHA2('$key',512)))", FALSE);
			$this->db->set('lh_apply_addr', "AES_ENCRYPT(TRIM('". $item->lh_apply_addr_org ."'), HEX(SHA2('$key',512)))", FALSE);
			$this->db->set('lh_apply_comp_nm', "AES_ENCRYPT(TRIM('". $item->lh_apply_comp_nm_org ."'), HEX(SHA2('$key',512)))", FALSE);
			$this->db->set('lh_comp_addr', "AES_ENCRYPT(TRIM('". $item->lh_comp_addr_org ."'), HEX(SHA2('$key',512)))", FALSE);
			$this->db->where(['seq'=>$item->seq]);
			$this->db->update('lawhelp');
			
			echo $i .' ';
			echof($item, "== OK");
			echo '<BR>';
		}
	}
	
	
	public function get_key() {
		return $this->db->select('pkey')->limit(1)->get('config')->result()[0]->pkey . CFG_ENCRYPT_KEY;
	}
	
	
}
