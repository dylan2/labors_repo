<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



// Auth Class
require_once 'admin.php';



/******************************************************************************************************
 * Board Class
 * 작성자 : delee
 *
 * 게시판 : 운영게시판, FAQ, 주요상담사례 게시판 3개
 *
 *****************************************************************************************************/

class Board extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	



	//=================================================================================================================
	// construct
	//=================================================================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		$this->load->model('db_admin', 'manager');
		$this->load->helper('download');
	}

	
	//=================================================================================================================
	// Board
	//=================================================================================================================

	//-----------------------------------------------------------------------------------------------------------------
	// open_notice : 운영자게시판의 공지 중 팝업 공지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function open_notice_popup() {

		$get = $this->input->get(NULL, TRUE);
		$seq = $get['seq'];
		$rstRtn['data']['seq'] = $seq;

		$this->load->view('adm_notice_popup_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_notice_popup_data : 운영자게시판의 공지 중 팝업 공지 데이터 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_notice_popup_data() {

		$param = $this->input->post(NULL, TRUE);
		$seq = $param['seq'];
		$data = array(
			'seq' => $seq
		);
		$rstRtn['data'] = $this->manager->get_notice_popup_data($data);
		

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_edit_view_data : 등록/수정 뷰화면 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function get_edit_view_data($args=NULL) {
		self::_chk_session() ;
		
		if(is_null($args)) {
			$param = $this->input->get(NULL, TRUE);
		}
		else {
//			$param['kind'] = $args['kind'];
//			$param['id'] = isset($args['id']) && $args['id'] ? $args['id'] : '';
			$param = $args;
		}
		
		$table_name = self::_get_table_name($param['id']);

		// 처리 구분 : add,view,edit
		$rstRtn['data']['kind'] = $param['kind'];
		
		
		// setting
		$rstRtn['data']['seq'] = '';
		$rstRtn['data']['s_code'] = ''; //상담방법코드
		$rstRtn['data']['title'] = '';
		$rstRtn['data']['oper_id'] = '';
		$rstRtn['data']['oper_name'] = '';
		$rstRtn['data']['content'] = '';
		$rstRtn['data']['view_cnt'] = '';
		$rstRtn['data']['file_name'] = '';
		$rstRtn['data']['file_name_org'] = '';
		$rstRtn['data']['reg_date'] = '';
		$rstRtn['data']['mod_date'] = '';
		$rstRtn['data']['code_name'] = ''; //상담방법코드명

		// 운영게시판 전용
		$rstRtn['data']['notice_yn'] = '';
		$rstRtn['data']['notice_dt_begin'] = '';
		$rstRtn['data']['notice_dt_end'] = '';
		$rstRtn['data']['popup_yn'] = 0;// popup_yn
		
		// edit
		if($param['kind'] == 'edit' || $param['kind'] == 'view') {
			$data = array(
				'seq' => $param['seq']
				,'brd_id' => $table_name
			);

			// view counting 용도
			if($param['kind'] == 'view') {
				$data['kind'] = $param['kind'];
			}
//			echof( $data);
			$rst = $this->manager->get_board($data);
//			
			$rstRtn['data']['seq'] = $rst[0]->seq;
			$rstRtn['data']['s_code'] = isset($rst[0]->s_code) ? $rst[0]->s_code : '';
			$rstRtn['data']['title'] = $rst[0]->title;
			$rstRtn['data']['oper_id'] = $rst[0]->oper_id;
			$rstRtn['data']['oper_name'] = $rst[0]->oper_name;
			$rstRtn['data']['content'] = $rst[0]->content;
			$rstRtn['data']['view_cnt'] = $rst[0]->view_cnt;
			$rstRtn['data']['file_name'] = $rst[0]->file_name;
			$rstRtn['data']['file_name_org'] = $rst[0]->file_name_org;
			$rstRtn['data']['reg_date'] = $rst[0]->reg_date;
			$rstRtn['data']['mod_date'] = $rst[0]->mod_date;

			// 운영게시판 전용
			if($param['id'] == '1') {
				$rstRtn['data']['notice_yn'] = $rst[0]->notice_yn;
				$dt = explode(' ', $rst[0]->notice_dt_begin);
				$rstRtn['data']['notice_dt_begin'] = $dt[0];
				$dt = explode(' ', $rst[0]->notice_dt_end);
				$rstRtn['data']['notice_dt_end'] = $dt[0];
				// popup_yn
				$rstRtn['data']['popup_yn'] = $rst[0]->popup_yn;
			}
			else {
				$csl_way = $rst[0]->s_code; //상담방법코드
				$rstRtn['data']['s_code'] = $csl_way;
				$rstRtn['data']['code_name'] = $rst[0]->code_name; //상담방법코드명
			}
		}

		
		if(is_null($args)) {
			$this->load->view('adm_board_edit_view', $rstRtn);
		}
		else {
			return $rstRtn['data'];
		}
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_file : 특정 파일만 삭제하고 DB를 업데이트 한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function del_file() {
//		self::_chk_session() ;

		$param = $this->input->post(NULL, TRUE);
		
		$table_name = self::_get_table_name($param['bid']);

		// 기존 파일 삭제할 때 DB에서 가져온 기존파일명 문자열에서 파일명을 제거한다.
		$exist_data = array(
			'seq'=>$param['sq']
			,'table_name'=>$table_name
		);
		$arr_exist_file_data = $this->manager->get_exist_file_data_board($exist_data);
		$exist_file_data = array('file_name'=>'','file_name_org'=>'');

		// 가져온 기존 파일명을 문자열로 만든다.
		foreach($arr_exist_file_data as $ext_file) {
			if($ext_file->file_name != '') {
				if($exist_file_data['file_name'] != '') $exist_file_data['file_name'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$exist_file_data['file_name'] .= $ext_file->file_name;
			}
			if($ext_file->file_name_org != '') {
				if($exist_file_data['file_name_org'] != '') $exist_file_data['file_name_org'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
				$exist_file_data['file_name_org'] .= $ext_file->file_name_org;
			}
		}

		// 실제파일명으로 기존 파일 삭제
		// 파일 삭제 데이터 생성
		$append_proc_data = array(
			'file_name'=>$exist_file_data['file_name']
			,'file_name_org'=>$exist_file_data['file_name_org']
		);

		// 파일 삭제 및 기존 파일명에서 삭제하는 파일명 제거
		$rst_arr_del_ext_file_name = self::_processing_del_file($param['fnr'], $append_proc_data);

		// DB 저장
		$data = array(
			'seq' => $param['sq']
			,'kind' => 'edit'
			,'file_name' => $rst_arr_del_ext_file_name['file_name']
			,'file_name_org' => $rst_arr_del_ext_file_name['file_name_org']
			,'table_name' => $table_name
		);

		$rstRtn = self::_processing_data($data);

		$this->load->view('json_view', $rstRtn);
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// add or edit
	//-----------------------------------------------------------------------------------------------------------------
	public function processing() {
//		self::_chk_session() ; //<== 이함수 실행으로 업로드가 안되는 문제가 발생한다...
		
		// wysiwyg 기능으로 html코드 제거하지 않도록 따로 저장
		$content = $this->input->post('content');

		$param = $this->input->post(NULL, TRUE);
		$param['content'] = $content;
		
		// 파일 업로드 처리
		$rst_upload = $this->_upload();
//		echof($rst_upload);
//		echof($param);
//		exit;

		// 저장 정보 설정
		$table_name = self::_get_table_name($param['brd_id']);

		// return variables
		$rstRtn['data']['rst_rtn'] = 'fail';
		$rstRtn['data']['rst_msg'] = '';
				
		$data = array(
			'oper_id' => $param['oper_id']
			,'title' => $param['title']
			,'content' => $param['content']
			,'table_name' => $table_name
		);

		// 처리 구분
		// add : 저장, edit : 수정
		$kind = $param['kind'];
		$data['kind'] = $kind;

		// 수정일 경우
		$data['seq'] = isset($param['seq']) ? $param['seq'] : '';

		// 운영게시판
		if($param['brd_id'] == '1') {
			if(isset($param['notice_yn']) && $param['notice_yn'] == 'on') {
				$notice_yn = 1;
				$data['notice_dt_begin'] = $param['notice_dt_begin'] .' 00:00:00';
				$data['notice_dt_end'] = $param['notice_dt_end'] .' 23:59:59';
			}
			else {
				$notice_yn = 0;
				// $notice_dt_begin = '';
				// $notice_dt_end = '';
			}
			$data['notice_yn'] = $notice_yn;
			// $data['notice_dt_begin'] = $notice_dt_begin;
			// $data['notice_dt_end'] = $notice_dt_end;
			$data['popup_yn'] = isset($param['popup_yn']) && $param['popup_yn'] == 'on' ? 1 : 0;
		}
		// faq만 csl_way 사용
		else {
			$csl_way = '';
			if(isset($param['csl_way']) && $param['csl_way']) {
				$csl_way = base64_decode($param['csl_way']);
			}
			$data['s_code'] = $csl_way;
		}

		//
		$fail_cnt = 0;
		$up_file_names = '';
		$up_file_names_org = '';
		$err_msg = '';

//		echof($param, 'param');

		// DB 처리
		foreach($rst_upload as $up_data) {
			if($up_data['result'] == 'succ') {
					if($up_file_names != '') $up_file_names .= CFG_UPLOAD_FILE_NAME_DELIMITER;
					$up_file_names .= $up_data['file_name'];
					if($up_file_names_org != '') $up_file_names_org .= CFG_UPLOAD_FILE_NAME_DELIMITER;
					$up_file_names_org .= $up_data['file_name_org'];

			}
			else if($up_data['result'] == 'fail') {
				$fail_cnt++;
				if($err_msg != '') $err_msg .',';
				$err_msg .= $up_data['error'];
			}
		}

		// 1.업로드 실패한 파일이 1개라도 있는 경우, db 저장하지 않고 file 삭제한다.
		if($fail_cnt > 0) {
			if($up_file_names != '') {
				self::_processing_del_file($up_file_names);
			}
			else {
				$rstRtn['data']['msg'] = 'upload 실패';
				if($err_msg != '') $rstRtn['data']['msg'] .= '['. strip_tags($err_msg) .']';
			}
		}

		// 2.업로드가 있고 모두 성공한 경우, db 저장
		else if($up_file_names != '') {
			$data['file_name'] = $up_file_names;
			$data['file_name_org'] = $up_file_names_org;

			// 2-1.수정인 경우, 기존 파일명을 가져와 합쳐 저장할 데이터를 생성한다.
			if($kind == 'edit') {

				// 기존 파일 삭제할 때 DB에서 가져온 기존파일명 문자열에서 파일명을 제거한다.
				$exist_data = array(
					'seq'=>$data['seq']
					,'table_name'=>$table_name
				);
				$arr_exist_file_data = $this->manager->get_exist_file_data_board($exist_data);
				$exist_file_data = array('file_name'=>'','file_name_org'=>'');

				// 가져온 기존 파일명을 문자열로 만든다.
				foreach($arr_exist_file_data as $ext_file) {
					if($ext_file->file_name != '') {
						if($exist_file_data['file_name'] != '') $exist_file_data['file_name'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
						$exist_file_data['file_name'] .= $ext_file->file_name;
					}
					if($ext_file->file_name_org != '') {
						if($exist_file_data['file_name_org'] != '') $exist_file_data['file_name_org'] .= CFG_UPLOAD_FILE_NAME_DELIMITER;
						$exist_file_data['file_name_org'] .= $ext_file->file_name_org;
					}
				}

				// 2-1-1.기존 파일이 변경되는 수정인 경우, 실제파일명으로 기존 파일 삭제
				$chg_real_file_name = isset($param['chg_real_file_name']) ? $param['chg_real_file_name'] : '';
				if($chg_real_file_name != '')  {
					
					// 파일 삭제 데이터 생성
					$append_proc_data = array(
						'file_name'=>$exist_file_data['file_name']
						,'file_name_org'=>$exist_file_data['file_name_org']
					);
					// 파일 삭제 및 기존 파일명에서 삭제하는 파일명 제거
//					echof($chg_real_file_name, '변경되는 파일명');
//					echof($append_proc_data, '추가되는 파일명');

					$rst_arr_del_ext_file_name = self::_processing_del_file($chg_real_file_name, $append_proc_data);

					// 기존 파일명이 하나라도 남아 있으면 새로 업로드된 파일과 합친다.
					if($rst_arr_del_ext_file_name['file_name'] != '' && $rst_arr_del_ext_file_name['file_name_org'] != '' ) {
//						echof($data['file_name'], '업로드된 파일명-real');
//						echof($data['file_name_org'], '업로드된 파일명-org');
//						echof($rst_arr_del_ext_file_name['file_name'], '삭제한후 파일명-real');
//						echof($rst_arr_del_ext_file_name['file_name_org'], '삭제한후 파일명-org');
						$data['file_name'] = $rst_arr_del_ext_file_name['file_name'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name'];
						$data['file_name_org'] = $rst_arr_del_ext_file_name['file_name_org'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name_org'];
					}
					// 기존 파일명이 모두 제거된 경우, 새로 업로드된 파일만 저장된다.

				}
				// 2-1-2.파일 변경없이 추가만 하는 수정인 경우
				else {
					// 기존 파일명에 합친다.
					if($exist_file_data['file_name'] != '') {
						$data['file_name'] = $exist_file_data['file_name'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name'];
					}
					if($exist_file_data['file_name_org'] != '') {
						$data['file_name_org'] = $exist_file_data['file_name_org'] . CFG_UPLOAD_FILE_NAME_DELIMITER . $data['file_name_org'];
					}
				}
			}
			// 2-2.파일 변경이 없는 경우, 바로 저장
			// DB 저장
			$rstRtn = self::_processing_data($data);
		}

		// 3.업로드가 없는 경우, db 저장
		else {
			$rstRtn = self::_processing_data($data);
		}

//		echof($data, 'data');
//		echof($param, 'param');
//		echof($rstRtn, 'result');
//		exit;
		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _insert_data
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_data($args) {
		$rstRtn['data']['rst'] = 'fail';
		$rstRtn['data']['msg'] = '';

		$kind = $args['kind'];
		unset($args['kind']);

		// db 저장
		if($kind == 'add') {
			$new_data = $this->manager->add_board($args);
			$new_id = $new_data['new_id'];

			if(!$new_id) {
				$rstRtn['data']['msg'] = 'DB저장 실패';
			}
			else {
				$rstRtn['data']['rst'] = 'succ';
			}
		}
		// edit
		else {
			 $this->manager->edit_board($args);
			$rstRtn['data']['rst'] = 'succ';
		}
		
		return $rstRtn;
	}




	//-----------------------------------------------------------------------------------------------------------------
	// _processing_del_file : "|" 문자로 합쳐진 문자열을 받아 file을 삭제한다.
	//-----------------------------------------------------------------------------------------------------------------
	// param : $args 삭제할 파일의 이름 문자열
	// param : $append_args DB에서 가져온 기존 파일명(원본명,저장명) 데이터로 삭제할 파일의 파일명을 제거한다.
	// return : 삭제한 파일의 파일명을 제외한 기존 파일명 문자열
	//-----------------------------------------------------------------------------------------------------------------
	function _processing_del_file($args, $append_args=NULL) {
		$rst_file_name = '';
		$rst_file_name_org = '';

		if(!is_null($append_args)) {
			$arr_file_name = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $append_args['file_name']);
			$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $append_args['file_name_org']);
		}

		// 삭제할 파일 데이터
		$arr_data = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $args);

		foreach($arr_data as $del_fname) {
			// 값이 있으면 실행 - delimiter가 없는 문자열을 자를 경우 빈 값이 하나 더 생긴다.
			if($del_fname != '') {
				// 파일 삭제
				@unlink(CFG_UPLOAD_PATH . $del_fname);
				clearstatcache();
				
				// DB에서 가져온 기존 파일명이 있는 경우
				if(!is_null($append_args)) {
					$index = 0;
					$index2 = 0;
					$arr_not_equal_fnm_real = array();
					$arr_not_equal_fnm_org = array();
					// 실제 파일명으로 비교한다. 원본파일명은 같은 이름이 있을 수 있기 때문..
					foreach($arr_file_name as $target_fname) {
						// 틀린 이름만 문자열로 생성
						if($del_fname != $target_fname) {
							$arr_not_equal_fnm_real[$index2] = $target_fname;
							$arr_not_equal_fnm_org[$index2] = $arr_file_name_org[$index];
							$index2++;
						}
						$index++;
					}
					// 틀린 파일명만 담은 배열로 기존 배열 갱신
					$arr_file_name = $arr_not_equal_fnm_real;
					$arr_file_name_org = $arr_not_equal_fnm_org;
				}
			}
		}

		$rstRtn['file_name'] = join(CFG_UPLOAD_FILE_NAME_DELIMITER, $arr_file_name);
		$rstRtn['file_name_org'] = join(CFG_UPLOAD_FILE_NAME_DELIMITER, $arr_file_name_org);

		return $rstRtn;
	}



	
	//-----------------------------------------------------------------------------------------------------------------
	// _join_array_in_delimeter : 입력받은 배열을 입력받은 문자로 붙여서 반환한다.
	//-----------------------------------------------------------------------------------------------------------------
	// param : $data 합칠 배열
	// param : $delimiter 합칠때 구분자로 사용할 문자
	//-----------------------------------------------------------------------------------------------------------------
	private function _join_array_in_delimeter($data, $delimiter) {
		$rstRtn = '';
		foreach($data as $item) {
			if($rstRtn != '') {
				$rstRtn .= $delimiter;
			}
			$rstRtn .= $item;
		}

		return $rstRtn;
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// del
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
//		self::_chk_session() ;

		$param = $this->input->post(NULL, TRUE);
		
		// board table name
		$table_name = self::_get_table_name($param['brd_id']);

		$data = array(
			'seq' => $param['seq']
			,'brd_id' => $table_name
		);
		$rst = $this->manager->del_board($data);
		
		// 파일 삭제
		if($rst['file_name']) {
			@unlink(CFG_UPLOAD_PATH . $rst['file_name']);
		}

		$rstRtn = self::board_list(TRUE);

		$rstRtn['data']['rst'] = $rst['rst'];
		$rstRtn['data']['msg'] = $rst['msg'];
		
		$this->load->view('json_view', $rstRtn);
	}
	


	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 여러 게시물 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		// board table name
		$table_name = self::_get_table_name($param['brd_id']);

		$arr_seq = explode(CFG_AUTH_CODE_DELIMITER,  $param['seq']);

		foreach($arr_seq as $id) {
			$data = array(
				'seq'=>$id
				,'brd_id'=>$table_name
			);
			$rst = $this->manager->del_board($data);

			// delete file
			if($rst['rst'] == 'succ' && $rst['file_name'] != '') {
				$path = CFG_UPLOAD_PATH . $rst['file_name'];
				if(file_exists($path)) {
					@unlink($path);
					clearstatcache();
				}
			}
		}
		
		// return a list data
		$rstRtn = self::board_list(TRUE);
		$rstRtn['data']['rst'] = 'succ';

		$this->load->view('json_view', $rstRtn);
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// board_list
	//-----------------------------------------------------------------------------------------------------------------
	public function board_list($rtn_data=NULL) {
//		self::_chk_session() ;

		$param = $this->input->post(NULL, TRUE);
		
		$table_name = self::_get_table_name($param['brd_id']);

		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// keyword
		$keyword = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';

		// target
		$target = isset($param['target']) && $param['target'] ? $param['target'] : 'all';
		
		// 상담 방법
		$csl_way = isset($param['csl_way']) && $param['csl_way'] ? base64_decode($param['csl_way']) : '';

		// search date
		$search_date_begin = isset($param['search_date_begin']) && $param['search_date_begin'] ? $param['search_date_begin'] : '';
		$search_date_end = isset($param['search_date_end']) && $param['search_date_end'] ? $param['search_date_end'] : '';
		
		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
			, 'brd_id' => $table_name
		);

		$where['target'] = $target;

		$where['keyword'] = $keyword;
		
		$where['csl_way'] = $csl_way;
		
		$where['search_date_begin'] = $search_date_begin;
		
		$where['search_date_end'] = $search_date_end;

		// 운영게시판은 공지글을 추가적으로 가져온다
		$rstRtn['data']['noti'] = array();
		$rst_noti = '';
		if($param['brd_id'] == '1') {
			$rst_noti = $this->manager->get_noti_articles(array('tbl_name' => $table_name));
			$rstRtn['data']['noti'] = $rst_noti['data'];
		}

		// 전체글
		$rst = $this->manager->get_board_list($where);

		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
// 		$rstRtn['data']['query'] = $rst['query'];
		
		// 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
		}
		else {
			$this->load->view('json_view', $rstRtn);
		}
	}



	//-----------------------------------------------------------------------------------------------------------------
	// check file exist
	//-----------------------------------------------------------------------------------------------------------------
	public function chk_download() {
		$param = $this->input->post(NULL, TRUE);
		$real_name = $param['real'];
		$org_name = $param['org'];

		$path = CFG_UPLOAD_PATH . $real_name;

		$rstRtn['data']['rst'] = 'fail';
		if(file_exists($path)) {
			clearstatcache();
			$rstRtn['data']['rst'] = 'succ';
		}

		$this->load->view('json_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// file download - 한글파일명인 경우 한글 깨지는 문제..
	//-----------------------------------------------------------------------------------------------------------------
	// public function download() {
	// 	$param = $this->input->get(NULL, TRUE);
	// 	$real_name =$param['real'];
	// 	$org_name =$param['org'];

	// 	$path = CFG_UPLOAD_PATH . $real_name;

		// if(file_exists($path)) {
		// 	$data = file_get_contents($path);
		// 	clearstatcache();
			
		// 	force_download($org_name, $data);
		// }
	// }




	//-----------------------------------------------------------------------------------------------------------------
	// file download
	//-----------------------------------------------------------------------------------------------------------------
	public function download() {
		
		$param = $this->input->get(NULL, TRUE);
		$real_name = $param['real'];
		$org_name = $param['org'];

		$path = CFG_UPLOAD_PATH . $real_name;
		
		if(file_exists($path)) {
			$data = file_get_contents($path);
			clearstatcache();
			
			force_download_v2($org_name, $data);
		}
	}



	//-----------------------------------------------------------------------------------------------------------------
	// file upload
	//-----------------------------------------------------------------------------------------------------------------
	private function _upload() {
		
		foreach ($_FILES as $index => $value){
			$rstRtn[$index]['file_name'] = '';
			$rstRtn[$index]['file_name_org'] = '';
			$rstRtn[$index]['error'] = '';
			$rstRtn[$index]['result'] = '';

			if ($value['name'] != ''){
				$this->load->library('upload');
				$this->upload->initialize($this->_set_upload_options());

				//upload the image
				if ( ! $this->upload->do_upload($index)){
					$rstRtn[$index]['error'] = $this->upload->display_errors();
					$rstRtn[$index]['result'] = 'fail';
				}
				else{
					$uploaded_data = $this->upload->data();
					$rstRtn[$index]['file_name'] = $uploaded_data['file_name'];
					$rstRtn[$index]['file_name_org'] = $uploaded_data['orig_name'];
					$rstRtn[$index]['result'] = 'succ';
				}
			}
		}
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// setting a file upload library
	//-----------------------------------------------------------------------------------------------------------------
	private function _set_upload_options() {
		$config = array();
		$config['upload_path'] = CFG_UPLOAD_PATH;
		$config['allowed_types'] = CFG_UPLOAD_ALL_ALLOW_EXT;
		$config['encrypt_name'] = TRUE;
		$config['overwrite'] = FALSE;
		$config['max_size'] = CFG_UPLOAD_MAX_FILE_SIZE;
		$config['max_width'] = 0; // no limit
		$config['max_height'] = 0; // no limit
		$config['max_filename'] = 0; // file name length, '0' is no limit
		$config['remove_spaces'] = TRUE;

		return $config;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_table_name : 입력된 테이블 index에 맞게 테이블 명 반환
	//-----------------------------------------------------------------------------------------------------------------
	function _get_table_name($args) {
		$table_name = 'brd_faq';
		if($args == '1') {
			$table_name = 'brd_operation';
		}
		else if($args == '2') {
			$table_name = 'brd_csl_exmp';
		}

		return $table_name;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		$admin = new Admin();
		$out = $admin->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				$rstRtn['data']['data'] = 'session_timeout';
				return $admin->logout();
			}
		}
		else {
			return $out;
		}
	}
}
