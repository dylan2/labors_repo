<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// Auth Class
require_once 'auth.php';




/******************************************************************************************************
 * 관리자 Class
 * Admin
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Admin_org extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

	//=================================================================================================================
	// construct
	//=================================================================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_manager', 'manager');

		// if(!isset($_SESSION)) {
		// 	ini_set("session.cache_expire", 1); // 세션 유효시간 : 분
		// 	ini_set("session.gc_maxlifetime", 3600); // 세션 가비지 컬렉션(로그인시 세션지속 시간) : 초

		// 	session_start();
		// }
		

		// TEST FORM PAGE - 세션체크하지 않도록 한다.
		$ref = @$_SERVER['HTTP_REFERER'];
		// if($ref && stripos($_SERVER['HTTP_REFERER'], '?c=main&m=test_form') !== FALSE) {
			// $this->is_local = TRUE;
		// }

		// $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		// $this->output->set_header("Pragma: no-cache");
	}


	
	
	
	//#################################################################################################################
	// Admin
	//#################################################################################################################

	//=================================================================================================================
	// index
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {

		// ssl 접속 처리
		if( !is_local() ) self::force_ssl();

		if( ! $this->session->userdata(CFG_SESSION_ADMIN_ID)) {
		// if( ! $_SESSION[CFG_SESSION_ADMIN_ID] ) {
			$this->load->view('adm_login_view');
		}
		else {
			// 첫화면 화면 id - 로그인 후 처음 노출되는 화면이다
			// $data['menu'] = 'lnb02_menu01_1'; // defualt menu, *표 앞은 대메뉴 id , 뒤는 소메뉴 index
			$data['menu'] = 'lnb01_menu01_1'; // defualt menu, *표 앞은 대메뉴 id , 뒤는 소메뉴 index
			$data['m_code'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_FIRST_CD); // 서브메뉴 code, 권한있는 최초 페이지 로딩을 위한 코드
			$this->redirect_list($data);
		}
	}
	

	
	//=================================================================================================================
	// Login
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// login
	//-----------------------------------------------------------------------------------------------------------------
	public function login() {

		$param = $this->input->post(NULL, TRUE);

		// 추가 : dylan 2017.09.12, danvistory.com
		// CSRF 위변조, URL파라메터 위변조, xpath 방지용 보안처리 토큰 체크
		if( is_bad_token($param) ) {
			$rstRtn['data']['rst'] = 'fail';
			$rstRtn['data']['msg'] = throw_error_return(array(
					'level' => "ERROR"
					,'log_msg' => "로그인 폼 위변조 시도"
					,'page_msg' => "err_login_03"
			), TRUE);
		}
		else {
		
			// 조회
			$where = array(
				'oper_id'=>$param['adminId']
				, 'oper_pwd'=>$param['adminPwd']
			);
			$tmp = $this->manager->get_login($where);
			$rstRtn['data'] = $tmp;
	
			if($rstRtn['data']['rst'] == 'succ') {
	
				// 세션 토큰 초기화
				$this->session->set_userdata(CFG_SECURE_FORM_TOKEN, "");
	
				// 권리구제 뷰,엑셀다운로드 권한 체크
				$is_auth_lh_view = stripos($tmp[CFG_SESSION_ADMIN_AUTH_CODES], base64_encode(CFG_EXCEPTION_CODE_LAW_HELP_CONTENT_VIEW_AUTH)) !== FALSE ? 1 : 0;
				$is_auth_lh_excel_download = stripos($tmp[CFG_SESSION_ADMIN_AUTH_CODES], base64_encode(CFG_EXCEPTION_CODE_LAW_HELP_LIST_EXCEL_DOWNLOAD_AUTH)) !== FALSE ? 1 : 0;
	
				// session 처리
				$data = array(
					CFG_SESSION_ADMIN_ID => $tmp[CFG_SESSION_ADMIN_ID]
					, CFG_SESSION_ADMIN_NAME => $tmp[CFG_SESSION_ADMIN_NAME]
					, CFG_SESSION_ADMIN_KIND_CODE => $tmp[CFG_SESSION_ADMIN_KIND_CODE]
					, CFG_SESSION_ADMIN_KIND_CODE_SUB => $tmp[CFG_SESSION_ADMIN_KIND_CODE_SUB]
					, CFG_SESSION_ADMIN_AUTH_GRP_ID => $tmp[CFG_SESSION_ADMIN_AUTH_GRP_ID]
					, CFG_SESSION_ADMIN_AUTH_CODES => $tmp[CFG_SESSION_ADMIN_AUTH_CODES]
					, CFG_SESSION_ADMIN_AUTH_ASSO_CD => $tmp[CFG_SESSION_ADMIN_AUTH_ASSO_CD]
					, CFG_SESSION_ADMIN_AUTH_ASSO_NM => $tmp[CFG_SESSION_ADMIN_AUTH_ASSO_NM]
					, CFG_SESSION_ADMIN_AUTH_CSL_VIEW => 0
					, CFG_SESSION_ADMIN_AUTH_IS_MASTER => 0
					, CFG_SESSION_ADMIN_AUTH_FIRST_CD => $tmp[CFG_SESSION_ADMIN_AUTH_FIRST_CD]
					, CFG_SESSION_ADMIN_AUTH_LAWHELP_VIEW => $is_auth_lh_view //권리구제 뷰 권한여부
					, CFG_SESSION_ADMIN_AUTH_LAWHELP_EXCEL_DOWNLOAD => $is_auth_lh_excel_download //권리구제 목록>엑셀 다운로드 권한여부
				);
				// master 계정 체크
				if(base64_decode($tmp['oper_auth_grp_id']) == CFG_AUTH_CODE_ADM_MASTER) {
					$data[CFG_SESSION_ADMIN_AUTH_IS_MASTER] = 1;
				}
				
				$this->session->set_userdata($data);
			}
			else {
				$rstRtn = array();
				$rstRtn['data']['rst'] = 'fail';
				$rstRtn['data']['msg'] = 'err_login_01';
	
				$this->_sess_destroy();
			}
		}

		$this->load->view('json_view', $rstRtn);

	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// logOut
	//-----------------------------------------------------------------------------------------------------------------
	public function logout($err_code=NULL) {

		$this->_sess_destroy();

		$rstRtn['data']['err_code'] = is_null($err_code) ? '' : $err_code;

		$this->load->view('adm_login_view', $rstRtn);
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _sess_destroy
	//-----------------------------------------------------------------------------------------------------------------
	protected function _sess_destroy() {
		// 로그인 유지 기능 처리
		// $is_suspend_logout = $this->session->userdata(CFG_SESSION_ADMIN_SUSPEND_LOGOUT) ? 
		// 	$this->session->userdata(CFG_SESSION_ADMIN_SUSPEND_LOGOUT) : 0;
		// $is_suspend_logout = $_SESSION[CFG_SESSION_ADMIN_SUSPEND_LOGOUT] ? $_SESSION[CFG_SESSION_ADMIN_SUSPEND_LOGOUT] : 0;

		// $this->session->sess_destroy();

		$data = array(
			CFG_SESSION_ADMIN_AUTH_IS_MASTER => 0
			, CFG_SESSION_ADMIN_ID => ''
			, CFG_SESSION_ADMIN_NAME => ''
			, CFG_SESSION_ADMIN_KIND_CODE => CFG_OPERATOR_KIND_CODE_OK1
			, CFG_SESSION_ADMIN_KIND_CODE_SUB => ''
			, CFG_SESSION_ADMIN_AUTH_GRP_ID => ''
			, CFG_SESSION_ADMIN_AUTH_CODES => ''
			, CFG_SESSION_ADMIN_AUTH_ASSO_CD => ''
			, CFG_SESSION_ADMIN_AUTH_ASSO_NM => ''
			, CFG_SESSION_ADMIN_AUTH_CSL_VIEW => 0
			, CFG_SESSION_ADMIN_AUTH_LAWHELP_EXCEL_DOWNLOAD => 0
			, CFG_SESSION_ADMIN_AUTH_LAWHELP_VIEW => 0
			// , CFG_SESSION_ADMIN_SUSPEND_LOGOUT => $is_suspend_logout // 로그인 유지 기능값만 남긴다.
		);
		$this->session->set_userdata($data);
		// $_SESSION = $data;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// chg_pwd : 운영자 비밀번호 변경
	//-----------------------------------------------------------------------------------------------------------------
	public function chg_pwd() {
		$param = $this->input->post(NULL, TRUE);
		
		$data = array(
			'oper_id' => $this->session->userdata(CFG_SESSION_ADMIN_ID)
			,'oper_pwd' => $param['curr_pwd']
			,'new_pwd' => $param['new_pwd']
		);
		
		$rst = $this->manager->change_pwd($data);

		$rstRtn['data']['msg'] = '';

		// 틀릴경우
		if($rst['rst'] == 'fail') {
			$rstRtn['data']['msg'] = 'not_match';
		}

		$rstRtn['data']['rst'] = $rst['rst'];

		$this->load->view('json_view', $rstRtn);
	}
	


	//-----------------------------------------------------------------------------------------------------------------
	// chg_pwd_view : 운영자 비번을 관리자가 변경할 화면 : 추가 2016.04.15 23:31:00
	//-----------------------------------------------------------------------------------------------------------------
	public function chg_pwd_view() {

		$params = $this->input->get(NULL, TRUE);

		$rstRtn = '';

		$page = 'adm_chg_pwd_popup_view';

		// 운영자 비번을 관리자가 변경할 경우 - adm_oper_edit_view 에서 사용 : 추가 2016.04.15 23:31:00
		if(isset($params['ref'])) {
			$page  = 'adm_chg_oper_pwd_popup_view';	
			$rstRtn['tid'] = $params['tid']; // 변경할 운영자 아이디
		}

		$this->load->view($page, $rstRtn);
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// chg_oper_pwd - 마스터 관리자가 운영자 비번 변경 처리 : 추가 2016.04.15 23:31:00
	//-----------------------------------------------------------------------------------------------------------------
	public function chg_oper_pwd() {
		$param = $this->input->post(NULL, TRUE);
		
		$data = array(
			'oper_id' => $param['tid']
			,'new_pwd' => $param['new_pwd']
		);
		
		$rst = $this->manager->change_oper_pwd($data);

		$rstRtn['data']['msg'] = '';

		// 틀릴경우
		if($rst['rst'] == 'fail') {
			$rstRtn['data']['msg'] = 'not_match';
		}

		$rstRtn['data']['rst'] = $rst['rst'];

		$this->load->view('json_view', $rstRtn);
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// redirect adm_main_view page
	//-----------------------------------------------------------------------------------------------------------------
	public function redirect_list($args=NULL) {
		
		// echof(base64_decode($this->session->userdata(CFG_SESSION_ADMIN_AUTH_GRP_ID)));
		// echof($this->session->userdata(CFG_SESSION_ADMIN_AUTH_LAWHELP_EXCEL_DOWNLOAD));
		// echof($this->session->userdata(CFG_SESSION_ADMIN_AUTH_LAWHELP_VIEW));

		// 선택된 메뉴 id
		$selected_menu = '';
		
		// 선택된 LNB 메뉴의 서브메뉴의 메뉴 code
		$m_code = '';
		
		$rstRtn = array();
		$rstRtn['data'] = array();
		$rstRtn['res'] = array();
		$rstRtn['edit'] = array();

		// 통계 외 메뉴에서 넘어온 경우는 검색 세션 데이터 제거한다. 2017.02.03 dylan
		if(isset($args['kind']) && $args['kind'] == 'menu') {
			$this->session->set_userdata(CFG_SESSION_SEARCH_DATA, '');
		}

		// 메뉴별 view 페이지 설정
		if(!is_null($args)) {
			$selected_menu = $args['menu'];
			$m_code = $args['m_code'];
			
			// LNB 메뉴 코드에서 서브메뉴 index를 추출한다.
			$sel_sub_menu_index = substr($selected_menu, -1, 1);
			
			$rstRtn = array();
			$rstRtn['res'] = array();

			//*********************************************************
			// 상담
			//*********************************************************
			if( stripos($selected_menu, 'lnb01') !== false) {
				require_once 'counsel.php';
				$counsel = new Counsel();

				// 입력
				if($sel_sub_menu_index == 1) {
					// 권한등록 - 수정/보기 링크 예외 처리
					$data['kind'] = isset($args['kind']) ? $args['kind'] : 'add';
					$data['id'] = isset($args['id']) ? $args['id'] : '';
					
					// 각종 select를 위한 기본 코드 데이터
					$tmp = $counsel->get_edit_view_data($data);
					$rstRtn['res'] = $tmp['data'];
					$rstRtn['edit'] = $tmp['edit'];
					
					// 접속자명
					$rstRtn['res'][CFG_SESSION_ADMIN_NAME] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
					// $rstRtn['res'][CFG_SESSION_ADMIN_NAME] = $_SESSION[CFG_SESSION_ADMIN_NAME];

					// 접속자의 소속코드
					$rstRtn['res'][CFG_SESSION_ADMIN_AUTH_ASSO_CD] = base64_encode($this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD));
					// $rstRtn['res'][CFG_SESSION_ADMIN_AUTH_ASSO_CD] = base64_encode($_SESSION[CFG_SESSION_ADMIN_AUTH_ASSO_CD]);

					// 접속자의 소속코드명
					$rstRtn['res'][CFG_SESSION_ADMIN_AUTH_ASSO_NM] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_NM);
					// $rstRtn['res'][CFG_SESSION_ADMIN_AUTH_ASSO_NM] = $_SESSION[CFG_SESSION_ADMIN_AUTH_ASSO_NM];
					
					// 검색항목 데이터
					// - 처리결과 코드
					$rstRtn['res']['csl_proc_rst_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);

					// 공지 중 팝업공지 확인
					$rstRtn['res']['popup_seq'] = $this->manager->get_popup_notice();

					$view_page = 'adm_counsel_edit_view';

				}
				// 조회(list)
				else {
					// 검색항목 데이터
					// - 직종 코드
					$rstRtn['res']['csl_work_kind_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
					// - 업종 코드
					$rstRtn['res']['csl_comp_kind_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
					// - 처리결과 코드
					$rstRtn['res']['csl_proc_rst_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);
					// - 검색 : 연도 데이터
					$rstRtn['res']['search_year'] = $this->manager->get_counsel_year();

		    	// 검색항목 출력
		    	$rstRtn[CFG_SESSION_SEARCH_DATA] = '';
		    	if($this->session->userdata(CFG_SESSION_SEARCH_DATA)) {
						foreach ($this->session->userdata(CFG_SESSION_SEARCH_DATA) as $key => $value) {
							$rstRtn[CFG_SESSION_SEARCH_DATA][$key] = $value;
						}
					}

					$view_page = 'adm_counsel_view';

				}
				unset($counsel);

				// add,edit 공통 data
				// 1.검색항목 : 상담방법 코드
				$rstRtn['res']['csl_way'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				// 공통코드 - 주제어 코드
				$rstRtn['res']['csl_keyword'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD);

			}

			//*********************************************************
			// 권리구제지원 상담
			//*********************************************************
			else if( stripos($selected_menu, 'lnb07') !== false) {
				require_once 'lawhelp.php';
				$lawhelp = new Lawhelp();

				// add,edit 공통 data
				// 1.검색항목 : 지원종류 코드
				$rstRtn['res']['code_sprt_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_LAWHELP_SUPPORT_KIND);

				// 2.검색항목 : 지원결과 코드
				$rstRtn['res']['code_sprt_rst'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_LAWHELP_SUPPORT_RESULT);

				// 입력
				if($sel_sub_menu_index == 1) {
					// 권한등록 - 수정/보기 링크 예외 처리
					$data['kind'] = isset($args['kind']) ? $args['kind'] : 'add';
					$data['id'] = isset($args['id']) ? $args['id'] : '';
					
					// 각종 select를 위한 기본 코드 데이터
					$tmp = $lawhelp->get_data($data);
					$rstRtn['res'] = $tmp['data'];
					$rstRtn['edit'] = $tmp['edit'];
					
					// 접속자명
					$rstRtn['res'][CFG_SESSION_ADMIN_NAME] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);

					// 접속자의 소속코드
					$rstRtn['res'][CFG_SESSION_ADMIN_AUTH_ASSO_CD] = base64_encode($this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD));

					// 접속자의 소속코드명
					$rstRtn['res'][CFG_SESSION_ADMIN_AUTH_ASSO_NM] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_NM);
					
					// 권리구제항목 : 지원결과 코드 - 추가 2018.07.05
					$rstRtn['res']['code_lh_kind'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_LAWLELP_KIND);

					$view_page = 'adm_lawhelp_edit_view';

				}
				// 조회(list)
				else {
					// 검색항목 데이터
					// - 검색 : 연도 데이터
					$rstRtn['res']['search_year'] = $this->manager->get_lawhelp_year();
					// - 검색 : 연령
					$rstRtn['res']['search_ages'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);

			    	// 검색항목 출력
			    	$rstRtn[CFG_SESSION_SEARCH_DATA] = '';
			    	if($this->session->userdata(CFG_SESSION_SEARCH_DATA)) {
						foreach ($this->session->userdata(CFG_SESSION_SEARCH_DATA) as $key => $value) {
							$rstRtn[CFG_SESSION_SEARCH_DATA][$key] = $value;
						}
					}
					$view_page = 'adm_lawhelp_view';

				}
				unset($lawhelp);

			}

			//*********************************************************
			// 통계
			//*********************************************************
			else if(stripos($selected_menu, 'lnb02') !== false) {
				// 소속 코드 data
				$rstRtn['res']['asso_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);
				// 상담 방법 코드 data
				$rstRtn['res']['s_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				// 상담 방법 코드 data
				$rstRtn['res']['csl_proc_rst'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);

				// 상담사례 - 상담보기 버튼 권한
				$rstRtn['res']['auth_csl_view'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_CSL_VIEW);
				// $rstRtn['res']['auth_csl_view'] = $_SESSION[CFG_SESSION_ADMIN_AUTH_CSL_VIEW];
				
				$kind = isset($args['kind']) ? $args['kind'] : 1;

				// view page name
				$vp_index = $kind;
				if(strlen($kind) < 2) {
					$vp_index = '0'. $kind;
				}
				$view_page = 'adm_statistic'. $vp_index .'_view';
				

				// 1번 상담사례는 구조상 view page에 하드코딩 처리
				$sttt_code = '';

				// 상담방법
				if($kind == '2') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				}
				// 상담유형
				else if($kind == '3') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);
				}
				// 거주지:4, 회사소재지:5
				if($kind == '4' || $kind == '5') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				}
				// 고용형태
				else if($kind == '6') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
				}
				// 업종
				else if($kind == '7') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
				}
				// 직종
				else if($kind == '8') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
				}
				// 사용주체
				else if($kind == '9') {
					$sttt_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND);
				}
				// 기타
				else if($kind == '10') {
					// 근로자수
					$temp[0] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
					// 4대보험
					$temp[1] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
					// 근로계약서
					$temp[2] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
					$sttt_code = array_merge($temp[0], $temp[1], $temp[2]);

					$rstRtn['res']['cnt_0' ] = count($temp[0]);
					$rstRtn['res']['cnt_1'] = count($temp[1]);
					$rstRtn['res']['cnt_2'] = count($temp[2]);

					$temp[0] = $this->manager->get_code_nm_by_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
					$temp[1] = $this->manager->get_code_nm_by_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
					$temp[2] = $this->manager->get_code_nm_by_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
					$rstRtn['res']['m_cd_nm'] = array_merge($temp[0], $temp[1], $temp[2]);
				}
				//교차통계
				else if($kind == '11') {
					$rstRtn['res']['code_0'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
					$rstRtn['res']['code_1'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);
					$rstRtn['res']['code_2'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);
					$rstRtn['res']['code_3'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
					$rstRtn['res']['code_4'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
					$rstRtn['res']['code_5'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
					$rstRtn['res']['code_6'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
					$rstRtn['res']['code_7'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
					$rstRtn['res']['code_8'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
					$rstRtn['res']['code_9'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
					$rstRtn['res']['code_10'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
					$rstRtn['res']['code_11'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);
					$rstRtn['res']['code_12'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE);

					$rstRtn['res']['cnt_0'] = count($rstRtn['res']['code_0']);
					$rstRtn['res']['cnt_1'] = count($rstRtn['res']['code_1']);
					$rstRtn['res']['cnt_2'] = count($rstRtn['res']['code_2']);
					$rstRtn['res']['cnt_3'] = count($rstRtn['res']['code_3']);
					$rstRtn['res']['cnt_4'] = count($rstRtn['res']['code_4']);
					$rstRtn['res']['cnt_5'] = count($rstRtn['res']['code_5']);
					$rstRtn['res']['cnt_6'] = count($rstRtn['res']['code_6']);
					$rstRtn['res']['cnt_7'] = count($rstRtn['res']['code_7']);
					$rstRtn['res']['cnt_8'] = count($rstRtn['res']['code_8']);
					$rstRtn['res']['cnt_9'] = count($rstRtn['res']['code_9']);
					$rstRtn['res']['cnt_10'] = count($rstRtn['res']['code_10']);
					$rstRtn['res']['cnt_11'] = count($rstRtn['res']['code_11']);
					$rstRtn['res']['cnt_12'] = count($rstRtn['res']['code_12']);
				}
				else if($kind == '12') {
					$sttt_code = '';
				}
				else if($kind == '13') {
					$rstRtn['res']['code_0'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
					$rstRtn['res']['code_1'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);
					$rstRtn['res']['code_2'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);
					$rstRtn['res']['code_3'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);
					$rstRtn['res']['code_4'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
					$rstRtn['res']['code_5'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
					$rstRtn['res']['code_6'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
					$rstRtn['res']['code_7'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
					$rstRtn['res']['code_8'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
					$rstRtn['res']['code_9'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
					$rstRtn['res']['code_10'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
					$rstRtn['res']['code_11'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
					$rstRtn['res']['code_12'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);
					$rstRtn['res']['code_13'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);
					$rstRtn['res']['code_14'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE);

					$rstRtn['res']['cnt_0'] = count($rstRtn['res']['code_0']);
					$rstRtn['res']['cnt_1'] = count($rstRtn['res']['code_1']);
					$rstRtn['res']['cnt_2'] = count($rstRtn['res']['code_2']);
					$rstRtn['res']['cnt_3'] = count($rstRtn['res']['code_3']);
					$rstRtn['res']['cnt_4'] = count($rstRtn['res']['code_4']);
					$rstRtn['res']['cnt_5'] = count($rstRtn['res']['code_5']);
					$rstRtn['res']['cnt_6'] = count($rstRtn['res']['code_6']);
					$rstRtn['res']['cnt_7'] = count($rstRtn['res']['code_7']);
					$rstRtn['res']['cnt_8'] = count($rstRtn['res']['code_8']);
					$rstRtn['res']['cnt_9'] = count($rstRtn['res']['code_9']);
					$rstRtn['res']['cnt_10'] = count($rstRtn['res']['code_10']);
					$rstRtn['res']['cnt_11'] = count($rstRtn['res']['code_11']);
					$rstRtn['res']['cnt_12'] = count($rstRtn['res']['code_12']);
					$rstRtn['res']['cnt_13'] = count($rstRtn['res']['code_13']);
					$rstRtn['res']['cnt_14'] = count($rstRtn['res']['code_14']);
				}

				if($kind != '11' || $kind != '13'){
					$rstRtn['res']['code'] = $sttt_code;
				}


				$rstRtn['data']['asso_code'] = '';

				$asso_code = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);
				foreach ($asso_code as $value) {
					if($rstRtn['data']['asso_code'] != '') $rstRtn['data']['asso_code'] .= ', ';
					$rstRtn['data']['asso_code'] .= "'" . $value->s_code . "'";
				}
				$rstRtn['date']['begin'] = isset($args['begin']) ? $args['begin'] : '';
				$rstRtn['date']['end'] = isset($args['end']) ? $args['end'] : '';
				$rstRtn['date']['sch_asso_code'] = isset($args['sch_asso_code']) ? $args['sch_asso_code'] : '';
				$rstRtn['date']['sch_s_code'] = isset($args['sch_s_code']) ? $args['sch_s_code'] : '';
				$rstRtn['date']['sch_csl_proc_rst'] = isset($args['sch_csl_proc_rst']) ? $args['sch_csl_proc_rst'] : '';
				$rstRtn['date']['txt_asso_code'] = isset($args['txt_asso_code']) ? $args['txt_asso_code'] : '';
				$rstRtn['date']['txt_s_code'] = isset($args['txt_s_code']) ? $args['txt_s_code'] : '';
				$rstRtn['date']['txt_csl_proc_rst'] = isset($args['txt_csl_proc_rst']) ? $args['txt_csl_proc_rst'] : '';
			}

			//*********************************************************
			// 게시판
			//*********************************************************
			else if( stripos($selected_menu, 'lnb03') !== false) {
				
				$kind = isset($args['kind']) ? $args['kind'] : 'list';
				$data['kind'] = $rstRtn['kind'] = $kind;
				// echof($args['id'], 'seq');

				// 게시판 id
				$rstRtn['id'] = isset($args['id']) && $args['id'] ? $args['id'] : '';
				$data['id'] = $rstRtn['id'];

				// 게시판 seq
				$rstRtn['seq'] = $args['seq'];
				$data['seq'] = $rstRtn['seq'];

				$rstRtn['brd_id'] = $sel_sub_menu_index;

				// 뷰, 수정용 데이터 : board 컨트롤러에서 가져온다.
				if($kind != 'list') {
					require_once 'board.php';
					$board = new Board();
					$rstRtn['res'] = $board->get_edit_view_data($data);
					unset($board);
				}

				// 상담방법 코드 data
				if($kind != 'view') {
					$rstRtn['res']['csl_way'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				}

				// 공통 view page - 3개 게시판을 공통처리한다.
				if($kind == 'list') {
					// 주요상담사례 게시판 검색항목 데이터
					if($rstRtn['brd_id'] == 2) {

						// 검색항목 데이터
						// - 직종 코드
						$rstRtn['res']['csl_work_kind_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
						// - 업종 코드
						$rstRtn['res']['csl_comp_kind_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
						// - 처리결과 코드
						$rstRtn['res']['csl_proc_rst_code'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);
						// - 상담방법 코드
						$rstRtn['res']['csl_way'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
						// - 주제어 코드
						$rstRtn['res']['csl_keyword'] = $this->manager->get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD);
					}

					$view_page = 'adm_board_view';
				}
				else {
					$view_page = 'adm_board_edit_view';
				}
			}

			//*********************************************************
			// 운영자관리
			//*********************************************************
			else if( stripos($selected_menu, 'lnb04') !== false) {
				
				// oper 컨트롤러에서 데이터를 가져온다.
				require_once 'oper.php';
				$oper = new Oper();
				
				if($sel_sub_menu_index == 1) {
					// base code
					$rstRtn['res'] = $oper->get_base_code();
					
					$view_page = 'adm_oper_view';
				}
				else {
					// 권한등록 - 수정/보기 링크 예외 처리
					$data['kind'] = isset($args['kind']) ? $args['kind'] : '';
					$data['id'] = isset($args['id']) ? $args['id'] : '';
					
					$rstRtn['res'] = $oper->get_edit_view_data($data);
					
					$view_page = 'adm_oper_edit_view';
				}
				unset($oper);
			}

			//*********************************************************
			// 권한관리
			//*********************************************************
			else if( stripos($selected_menu, 'lnb05') !== false) {
				if($sel_sub_menu_index == 1) {
					$view_page = 'adm_auth_grp_view';
				}
				else {
					// 권한등록 - 수정/보기 링크 예외 처리
					$data['kind'] = isset($args['kind']) ? $args['kind'] : '';
					$data['id'] = isset($args['id']) ? $args['id'] : '';
					
					// auth 컨트롤러에서 데이터를 가져온다.
					require_once 'auth.php';
					$auth = new Auth();
					$rstRtn['res'] = $auth->get_edit_view_data($data);
					unset($auth);

					$view_page = 'adm_auth_grp_edit_view';
				}
			}

			//*********************************************************
			// 코드관리
			//*********************************************************
			else if( stripos($selected_menu, 'lnb06') !== false) {
				$view_page = 'adm_code_view';
				// 권리구제유형 코드 관리인 경우
				if(base64_decode($m_code) == CFG_SUB_CODE_OF_MANAGE_CODE_LAWLELP_KIND) {
					$view_page = 'adm_code_lawhelp_view';
				}
			}
		}
		
		$rstRtn['data']['selected_menu'] = $selected_menu;
		$rstRtn['data']['m_code'] = $m_code;
		
		// 현재 접속한 oper id, name
		$rstRtn['oper_id'] = $this->session->userdata(CFG_SESSION_ADMIN_ID);
		$rstRtn['oper_name'] = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
		$rstRtn['oper_asso_cd'] = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD);

		$rstRtn['data'][CFG_SESSION_ADMIN_NAME]  = $this->session->userdata(CFG_SESSION_ADMIN_NAME);
		
		// LNB 메뉴 생성용 데이터 추출
		$rstRtn['data']['lnb_code'] = $this->manager->get_lnb_menu_sub_code_by_m_code(CFG_MASTER_CODE_MANAGE_CODE);
		
		$this->load->view($view_page, $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// 관리자의 해당 메뉴로 이동
	// - 모든 페이지 전환은 이곳을 통해야 메뉴의 선택처리 및 관리자 명이 노출 된다.
	// * 수정
	// - require_once 코드 정리
	//-----------------------------------------------------------------------------------------------------------------
	public function main() {
		// AES encrypt library
		require_once './lib/aes.class.php';
		require_once './lib/aesctr.class.php'; 

		// 1.세션체크
		$out = $this->is_out_session();
		if($out === TRUE) {
			$rstRtn['data']['data'] = 'session_timeout';
			return self::logout();
		}
		
		$param = $this->input->get(NULL, TRUE);

		// 2.선택한 LNB 메뉴 코드 처리
		// - base64 encode된 상태
		if(isset($param['menu']) && $param['menu'] != '') {
			$enc_menu = $param['menu'];

			// 2-1.menu_id base64 decrypt 처리
			$dec_menu = base64_decode($enc_menu);

			// 2-2.menu_id AES decrypt 처리 - 좌측 메뉴 클릭시 inc_header.php 에서 인코딩 처리한다.
			$menu = AesCtr::decrypt($dec_menu, CFG_ENCRYPT_KEY, 256);
			$data['menu'] = $menu;

		}
		if(isset($param['txt_s_code']) && $param['txt_s_code'] != '') {
			$enc_txt_s_code = $param['txt_s_code'];

			$dec_txt_s_code = base64_decode($enc_txt_s_code);

			$txt_s_code = AesCtr::decrypt($dec_txt_s_code, CFG_ENCRYPT_KEY, 256);
			$data['txt_s_code'] = $txt_s_code;
		}
		if(isset($param['txt_csl_proc_rst']) && $param['txt_csl_proc_rst'] != '') {
			$enc_txt_csl_proc_rst = $param['txt_csl_proc_rst'];

			$dec_txt_csl_proc_rst = base64_decode($enc_txt_csl_proc_rst);

			$txt_csl_proc_rst = AesCtr::decrypt($dec_txt_csl_proc_rst, CFG_ENCRYPT_KEY, 256);
			$data['txt_csl_proc_rst'] = $txt_csl_proc_rst;
		}
		if(isset($param['txt_asso_code']) && $param['txt_asso_code'] != '') {
			$enc_txt_asso_code = $param['txt_asso_code'];

			$dec_txt_asso_code = base64_decode($enc_txt_asso_code);

			$txt_asso_code = AesCtr::decrypt($dec_txt_asso_code, CFG_ENCRYPT_KEY, 256);
			$data['txt_asso_code'] = $txt_asso_code;
		}

		// 3.선택된 LNB 메뉴의 서브메뉴의 메뉴 code 처리
		// - base64 encode된 상태
		$m_code = $param['code'];
		$data['m_code'] = $m_code;
// echof($m_code);
		// 게시판인 경우, seq가 넘어온다. 
		$data['seq'] = '';
		if(isset($param['seq']) && $param['seq']) {
			$data['seq'] = $param['seq'];
		}
		
		// 특정LNB메뉴(운영자관리,권한관리)의 화면에서 메뉴를 통하지 않고 페이지 전환하는 경우, 
		// 즉 "등록,보기,수정" 버튼들을 클릭한 경우 해당 컨트롤러에서 구분 처리를 위해 필요한 플래그 임
		if(isset($param['kind'])) $data['kind'] = $param['kind'];
		if(isset($param['id'])) $data['id'] = $param['id'];

		//통계의 검색설정을 유지하기 위한 처리
		if(isset($param['begin'])) $data['begin'] = $param['begin'];
		if(isset($param['end'])) $data['end'] = $param['end'];
		if(isset($param['sch_asso_code'])) $data['sch_asso_code'] = $param['sch_asso_code'];
		if(isset($param['sch_s_code'])) $data['sch_s_code'] = $param['sch_s_code'];
		if(isset($param['sch_csl_proc_rst'])) $data['sch_csl_proc_rst'] = $param['sch_csl_proc_rst'];
		//if(isset($param['txt_asso_code'])) $data['txt_asso_code'] = $param['txt_asso_code'];
		//if(isset($param['txt_s_code'])) $data['txt_s_code'] = $param['txt_s_code'];
		//if(isset($param['txt_csl_proc_rst'])) $data['txt_csl_proc_rst'] = $param['txt_csl_proc_rst'];
		// 뷰 페이지로 전환
		if($menu && $m_code) {
			self::redirect_list($data);
		}
		else {
			self::logout('info_auth_02');
		}
	}
	


	
	
	//=================================================================================================================
	// Common
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// check_session - 세션이 살아 있는지 확인한다. 레이어팝업에서 사용
	//-----------------------------------------------------------------------------------------------------------------
	public function check_session() {
		$out = $this->is_out_session();
		$rstRtn['data']['rst'] = 'succ';
		if($out === TRUE) {
			$rstRtn['data']['rst'] = 'fail';
		}
		
		$this->load->view('json_view', $rstRtn);
	}
		
	
	
	//#################################################################################################################
	// Helper
	//#################################################################################################################

	//=================================================================================================================
	// Admin
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// is_out_session - json 데이터 리턴함수에서 세션 체크함수
	//-----------------------------------------------------------------------------------------------------------------
	public function is_out_session() {
		if($this->is_local) {
			return FALSE;
		}
		else {
			if(!$this->session->userdata(CFG_SESSION_ADMIN_ID)) {
			// if(! isset($_SESSION[CFG_SESSION_ADMIN_ID]) || $_SESSION[CFG_SESSION_ADMIN_ID] == '') {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// sending_mail
	//-----------------------------------------------------------------------------------------------------------------
	private function sending_mail($args) {
		$rstRtn = 'fail';
		
		$config = Array(
			'mailtype' => 'html'
			, 'charset' => 'UTF-8'
			, 'wordwrap' => TRUE
		); 
		$this->load->library('email', $config);
		
		$this->email->from(CFG_MAIL_SYSTEM_EMAIL, CFG_MAIL_SYSTEM_NAME);
		$this->email->to($args['email']); 
		$this->email->subject(CFG_MAIL_SUBJECT_REPLY_FEEDBACK);
		
		$msg = self::get_mailform_feedback();
		$msg = str_ireplace(CFG_MAILFORM_TAG_RECEIVER_NAME, $args['lname'] .' '. $args['fname'], $msg); // name
		$msg = str_ireplace(CFG_MAILFORM_TAG_REPLY_FEEDBACK_TITLE, $args['title'], $msg); // title
		$msg = str_ireplace(CFG_MAILFORM_TAG_REPLY_FEEDBACK_CONTENTS, $args['contents'], $msg); // cotnents
		$this->email->message($msg);
		
		// sending
		$rst = $this->email->send();
		
		if($rst === TRUE) {
			$rstRtn = 'succ';
		}
		
		// echo $this->email->print_debugger();
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_mailform_feedback - 메일폼 파일을 읽어 텍스트를 리턴한다.
	//-----------------------------------------------------------------------------------------------------------------
	private function get_mailform_feedback() {
		$this->load->helper('file');
		$form_path = './mailform/mailform_fdk_reply.html';
		
		$rstRtn = '';
		
		$rst = read_file($form_path);
		if($rst) {
			$rstRtn = $rst;
		}
		
		return $rstRtn;
	}


	//-----------------------------------------------------------------------------------------------------------------
	// simple_mail
	//-----------------------------------------------------------------------------------------------------------------
	// 옴부즈만지원신청 상담이 접수된 경우 text 메일 발송용, 추가 2017.02.10 dylan
	//-----------------------------------------------------------------------------------------------------------------
	public function simple_mail($args) {
		$rstRtn = 'fail';
		
		$config = Array(
			'mailtype' => 'html'
			, 'charset' => 'UTF-8'
			, 'wordwrap' => TRUE
		); 
		$this->load->library('email', $config);
		
		$this->email->from($args['from'], $args['from_nm']);
		$this->email->to($args['to']);
		$this->email->subject($args['subject']);
		$this->email->message($args['contents']);
		
		// sending
		$rst = @$this->email->send();
		
		if($rst === TRUE) {
			$rstRtn = 'succ';
		}
		
		// echo $this->email->print_debugger();
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_img - 이미지 삭제 테스트
	//-----------------------------------------------------------------------------------------------------------------
	public function del_img() {
		$param = $this->input->post(NULL, TRUE);
		
		$rstRtn['data']['exist'] = file_exists(CFG_UPLOAD_PATH . $param['imgNm']);
		$rstRtn['data']['rst'] = unlink(CFG_UPLOAD_PATH . $param['imgNm']);
		clearstatcache();
		
		$rstRtn['data']['url'] = CFG_UPLOAD_PATH . $param['imgNm'];
		
		$this->load->view('json_view', $rstRtn);
	}
	
		
	/*
	// upload result
	Array ( 
		[file_name] => test_watermark_200x50.png 
		[file_type] => image/png 
		[file_path] => /home/www_api/upload/ 
		[full_path] => /home/www_api/upload/test_watermark_200x50.png 
		[raw_name] => test_watermark_200x50 
		[orig_name] => test_watermark_200x50.png 
		[client_name] => test_watermark_200x50.png 
		[file_ext] => .png 
		[file_size] => 14.83 
		[is_image] => 1 
		[image_width] => 200 
		[image_height] => 50 
		[image_type] => png 
		[image_size_str] => width="200" height="50" 
	)
	*/




	//-----------------------------------------------------------------------------------------------------------------
	// excel_data : 상담리스트, 권리구제리스트 Excel 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function excel_data($args) {

		$data = $this->session->userdata(CFG_SESSION_SEARCH_DATA);
		$data['is_excel'] = 1; // limit 제거용
		
		$rstRtn = '';

		// 상담리스트
		if($args['kind'] == 'counsel_list') {
			$rstRtn = $this->manager->get_counsel_list($data);
		}
		// 권리구제리스트
		else {
			$rstRtn = $this->manager->get_lawhelp_list($data);
		}

		return $rstRtn;
	}

		
	
	//-----------------------------------------------------------------------------------------------------------------
	// change_pwd_new_encrypt : 단반향 암호화로 기존 운영자 비빌번호 일괄 변경
	//-----------------------------------------------------------------------------------------------------------------
	public function change_pwd_new_encrypt() {

		// $this->manager->change_pwd_new_encrypt();

		// echof('ok');
	}
	


	
	//-----------------------------------------------------------------------------------------------------------------
	// force_ssl : https 로 강제 접속처리 함수
	//-----------------------------------------------------------------------------------------------------------------
	protected function force_ssl() {
		if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") {
			$url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			redirect($url);
			exit;
		}
	}


	
	
	
	//=================================================================================================================
	// TEST
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// test_form - DB 처리 테스트용 페이지
	//-----------------------------------------------------------------------------------------------------------------
	/*public function test_form() {
		$this->load->helper(array('form', 'url'));
		
		$data['data'] = array(
			CFG_SESSION_ADMIN_ID => $this->session->userdata(CFG_SESSION_ADMIN_ID)
		);
		$this->load->view('test_form', $data);
	}
	*/
}
