<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/******************************************************************************************************
 * Main Class
  * 작성자 : delee
 *
 *
 *****************************************************************************************************/

class Main extends CI_Controller {
	var $limit = CFG_MAIN_PAGINATION_ITEM_PER_PAGE;
	

    //========================================================================
    // construct
    //========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		$this->load->model('db_admin', 'manager');
	}

	
    //========================================================================
    // Main
    //========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// default
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
		
		$this->load->view('main_view');
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// notice_list - 메인페이지에 노출할 리스트
	//-----------------------------------------------------------------------------------------------------------------
	public function notice_list() {
		$rstRtn['data']['rst'] = 'succ';
		$rstRtn['data']['msg'] = 'no data';
		$rstRtn['data']['data'] = '';
		$rstRtn['data']['tot_cnt'] = 0;
		
		$param = $this->input->post(NULL, TRUE);
		
		$kind = 'main';
		if(isset($param['kind']) && $param['kind']) {
			$kind = $param['kind'];
		}
		
		$limit = isset($param['cnt']) ? (int)$param['cnt'] : $this->limit;
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $limit) - $limit;
		
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
		);
		$rst = $this->manager->get_notice_list($where, null, $kind);
		
		// 결과가 없는 경우는 rst가 succ로 넘긴다. 오류일 때만 fail로 처리한다.
		if(count($rst['data']) > 0) {
			$rstRtn['data']['msg'] = '';
			$rstRtn['data']['data'] = $rst['data'];
			$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
		}
		
		$this->load->view('json_view', $rstRtn);
	}
}
