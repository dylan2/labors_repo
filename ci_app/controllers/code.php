<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// Auth Class
require_once 'auth.php';
require_once 'admin.php';




/******************************************************************************************************
 * 관리자 Class
 * Code
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Code extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

	//========================================================================
	// construct
	//========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');
		
		// session
		// $this->load->library('session');
	}
	
	
	
	
	//########################################################################
	// Code
	//########################################################################

	//========================================================================
	// index
	//========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	public function index() {
	}
	
	
	
	//========================================================================
	// Code Manage
	//========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// code : 등록 뷰화면 호출
	//-----------------------------------------------------------------------------------------------------------------
	public function add_view() {
//		self::_chk_session() ;
		
		$param = $this->input->get(NULL, TRUE);

		$rstRtn['data']['kind'] = 'add';
		$rstRtn['data']['ref'] = isset($param['ref']) ? $param['ref'] : '';
		$rstRtn['data']['code'] = $param['code'];
		
		// 수정화면을 위한 변수 세팅
		$rstRtn['data']['dsp_order'] = '';
		$rstRtn['data']['dsp_code'] = '';
		$rstRtn['data']['code_name'] = '';
		$rstRtn['data']['use_yn'] = 1;
		$rstRtn['data']['code_desc_yn'] = 'N';
		$rstRtn['data']['code_desc'] = '';
		
		$page = 'adm_code_edit_view';
		if($rstRtn['data']['ref'] == 'lhmng') {
			$page = 'adm_code_lawhelp_edit_view';
		}
		
		$this->load->view($page, $rstRtn);
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	// code : 실제 DB 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function add() {
//		self::_chk_session() ;

		$param = $this->input->post(NULL, TRUE);
		$m_code = base64_decode($param['code']);
		$desc = isset($param['ref']) && $param['ref'] == 'lhmng' ? '코드관리-세코드-'. $param['desc'] : '코드관리-소코드-'. $param['desc'];
		
		$use = $param['rdoUse_pop'] == 2 ? 0 : 1;
		$data = array(
			'm_code' => $m_code
			, 'code_name' => $param['txtCodeName_pop']
			, 'dsp_order' => $param['txtIndex_pop']
			, 'use_yn' => $use
			, 'desc' => $desc
		);
		if(isset($param['dsp_code'])) {
			$data['dsp_code'] = $param['dsp_code'];
		}

		// 권리구제유형 - 소코드 경우
		if(isset($param['ref']) && $param['ref'] == 'lhmng') {
			$rst = $this->manager->add_code_lhkind($data);
		}
		// 기존 코드 - 나머지 코드관리
		else {
			$data['code_desc_yn'] = $param['code_desc_yn'];
			$data['code_desc'] = $this->input->post('code_desc');// wysiwyg 기능으로 html코드 제거되지 않게 처리
			
			$rst = $this->manager->add_code($data);
		}
		// 목록 조회
		$rstRtn = self::code_list(TRUE);

		$this->load->view('json_view', $rstRtn);
	}

	

	/**
	 * 권리구제 소코드 추가 - 권리구제 전용
	 */
	public function add_lhkind() {

		$param = $this->input->post(NULL, TRUE);

		$m_code = base64_decode($param['m_code']);
		$desc = '코드관리-소코드-'. $param['desc'];
		
		$use = $param['use_yn'] == 2 ? 0 : 1;
		$data = array(
			'm_code' => $m_code
			, 'code_name' => $param['code_name']
			, 'dsp_order' => $param['dsp_order']
			, 'use_yn' => $use
			, 'desc' => $desc
		);
		$rst = $this->manager->add_code_lhkind($data);

		$rstRtn = self::code_list_lh_ccode($data);

		$this->load->view('json_view', $rstRtn);
	}


	/**
	 * 권리구제 소코드 수정 - 권리구제 전용
	 */
	public function edit_lhkind() {

		$param = $this->input->post(NULL, TRUE);
		$s_code = base64_decode($param['m_code']);
		
		$use = $param['use_yn'] == 2 ? 0 : 1;
		$data = array(
			's_code' => $s_code
			, 'code_name' => $param['code_name']
			, 'dsp_order' => $param['dsp_order']
			, 'use_yn' => $use
		);
		$rst = $this->manager->edit_code($data);
		
		// 권리구제유형 관리인 경우
		$data['m_code'] = CFG_SUB_CODE_OF_MANAGE_CODE_LAWLELP_KIND;
		$rstRtn = self::code_list_lh_ccode($data);

		$this->load->view('json_view', $rstRtn);
	}


	/**
	 * 권리구제유형 코드 소코드(C) 조회
	 * 
	 * @param  array $args 
	 * @return array
	 */
	public function code_list_lh_ccode($args=NULL) {
		$is_callpage = false;

		// adm_code_lawhelp_view 페이지에서 호출한 경우
		if(is_null($args)) {
			$is_callpage = true;
			$param = $this->input->post(NULL, TRUE);
			$args['m_code'] = base64_decode($param['m_code']);
		}

		$where = array(
			'offset' => 0
			, 'limit' => 99999
			, 'use_yn' => 'all'
			, 'm_code' => $args['m_code']
		);
		$rst = $this->manager->get_code_list($where);

		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
		$rstRtn['data']['rst'] = 'succ';

		// 페이지에서 호출한 경우
		if($is_callpage) {
			$this->load->view('json_view', $rstRtn);
		}
		// add 메서드에서 호출한 경우
		else {
			return $rstRtn;
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// code : 코드 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
		
		$param = $this->input->post(NULL, TRUE);
		$s_code = base64_decode($param['s_code']);
		
		$data['s_code'] = $s_code;
		$used_code = $this->manager->del_code($data);
	
		// return a list data
		$rstRtn = self::code_list(TRUE);
		$rstRtn['data']['used_code'] = $used_code;
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// code : 코드 수정 뷰화면 호출
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_view() {
		self::_chk_session() ;
		
		$param = $this->input->get(NULL, TRUE);
		
		$data['s_code'] = base64_decode($param['code']);
		$rstRtn['data'] = $this->manager->get_code($data);

		$rstRtn['data']['kind'] = 'edit';
		$rstRtn['data']['ref'] = isset($param['ref']) ? $param['ref'] : '';
		$rstRtn['data']['code'] = $param['code']; // base64 encoded

		// 상위코드의 설명글 등록인 경우, 순번,코드명 제거
// 		$rstRtn['data']['dsp_order'] = $rstRtn['data']['ref'] == 'csl_mcode' ? '' : $rstRtn['data']['dsp_order'];
// 		$rstRtn['data']['code_name'] = $rstRtn['data']['ref'] == 'csl_mcode' ? '' : $rstRtn['data']['code_name'];

		$page = 'adm_code_edit_view';
		if($rstRtn['data']['ref'] == 'lhmng') {
			$page = 'adm_code_lawhelp_edit_view';
		}
		
		$this->load->view($page, $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// code : 실제 DB 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function edit() {

		// wysiwyg 기능으로 html코드 제거하지 않도록 따로 저장
		$code_desc = $this->input->post('code_desc');
		
		$param = $this->input->post(NULL, TRUE);
		$s_code = base64_decode($param['code']);
		
		// 귄리구제지원인 경우
		if($param['ref'] == 'lhmng') {
			$use = $param['rdoUse_pop'] == 2 ? 0 : 1;
			$data = array(
				's_code' => $s_code
				, 'code_name' => $param['txtCodeName_pop']
				, 'dsp_code' => $param['dsp_code']
				, 'dsp_order' => $param['txtIndex_pop']
				, 'use_yn' => $use
			);
		}
		else {
			// 상위코드 설명글 등록인 경우
			if($param['ref'] == 'csl_mcode') {
				$data = array(
					's_code' => $s_code
					, 'code_desc_yn' => $param['code_desc_yn']
					, 'code_desc' => $code_desc
				);
			}
			else {
				$use = $param['rdoUse_pop'] == 2 ? 0 : 1;
				$data = array(
					's_code' => $s_code
					, 'code_name' => $param['txtCodeName_pop']
					, 'dsp_code' => $param['dsp_code']
					, 'dsp_order' => $param['txtIndex_pop']
					, 'use_yn' => $use
					, 'code_desc_yn' => $param['code_desc_yn']
					, 'code_desc' => $code_desc
				);
			}
		}
		$rst = $this->manager->edit_code($data);
		
		$rstRtn = self::code_list(TRUE);

		$this->load->view('json_view', $rstRtn);
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// code : 코드LIST
	//-----------------------------------------------------------------------------------------------------------------
	public function code_list($rtn_data=FALSE) {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		$use_yn = $param['rdoUse'];
		if($use_yn == 2) $use_yn = 0;
		
		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// keyword
		$keyword = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';
		
		// m_code - base64_decode
		$m_code = base64_decode($param['m_code']);
		
		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
			, 'use_yn' => $use_yn
			, 'm_code' => $m_code
		);
		if($keyword) {
			$where['code_name'] = $keyword;
		}
		
		$rst = $this->manager->get_code_list($where);
		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
		$rstRtn['data']['query'] = isset($rst['query']) ? $rst['query'] : '';
		
		// 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
		}
		else {
			$this->load->view('json_view', $rstRtn);
		}
	}
	

	//-----------------------------------------------------------------------------------------------------------------
	// using : 권한그룹 사용처리
	//-----------------------------------------------------------------------------------------------------------------
	public function using() {
		self::_use_or_not_and_multi_del(1);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// not_using : 권한그룹 사용중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function not_using() {
		self::_use_or_not_and_multi_del(0);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 권한그룹 사용중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
		self::_use_or_not_and_multi_del(2);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _use_or_not_and_multi_del - 다중 사용,중지,삭제
	//-----------------------------------------------------------------------------------------------------------------
	private function _use_or_not_and_multi_del($args) {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);

		$s_code = $param['s_code'];
		if(strpos($s_code, CFG_AUTH_CODE_DELIMITER) !== FALSE) {
			$arr_code = explode(CFG_AUTH_CODE_DELIMITER, $s_code);
			
			$tmp_code = '';
			foreach($arr_code as $code) {
				if($tmp_code != '') $tmp_code .= CFG_AUTH_CODE_DELIMITER;
				$tmp_code .= base64_decode($code);
			}
			$s_code = $tmp_code;
		}
		else {
			$s_code = base64_decode($s_code);
		}
		
		// del
		$used_code = '';
		if($args == 2) {
			$rst_tmp = $this->manager->del_multi_code($s_code);
			$used_code = $rst_tmp;
		}
		// 사용,사용중지
		else {
			$data['use_yn'] = $args;
			$data['s_code'] = $s_code;
			$this->manager->use_or_not_use_code($data);
		}
		
		// return a list data
		$rstRtn = self::code_list(TRUE);
		$rstRtn['data']['used_code'] = $used_code;
		
		$this->load->view('json_view', $rstRtn);
	}
	

	/**
	 * 코드 설명글 가져오기
	 *
	 * @param string $code 가져올 코드의 상위코드
	 * @return array
	 */
	public function code_desc() {
	
		// 코드관리 하위코드중 설명글 사용하는 코드 조회
		$rstRtn['code_desc'] = $this->manager->get_code_desc($code);
	
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		$admin = new Admin();
		$out = $admin->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				$rstRtn['data']['data'] = 'session_timeout';
				return $admin->logout();
			}
		}
		else {
			return $out;
		}
	}

	//
	/**
	 * 권리구제 - 구제유형코드 조회
	 * 
	 * @return void
	 */
	public function lhkind_sub_cd() {
		$param = $this->input->post(NULL, TRUE);

		$data = array(
			'm_code' => base64_decode($param['code'])
		);
		$rstRtn['data']['data'] = $this->manager->get_lhkind_sub_cd($data);
		$rstRtn['data']['tot_cnt'] = count($rstRtn['data']['data']);
		
		$this->load->view('json_view', $rstRtn);
	}
}
