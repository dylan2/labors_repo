<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// admin Class
require_once 'admin.php';



/******************************************************************************************************
 * 관리자 Class
 * Auth
 * 작성자 : delee
 *
 *
 * #조회결과 리턴 규칙 : 
 * rst - 처리 결과, 정상처리 : succ(데이터 유무과 무관), 오류 : fail
 * msg - 오류 발생시 오류 메시지
 * data - 레코드 셋(json으로 리턴)
 * tot_cnt - 조회된 데이터의 전체 레코드 개수
 * eg) 
 * $rstRtn['rst'] = 'succ';
 * $rstRtn['msg'] = 'no data';
 * $rstRtn['data'] = $rst->result();
 * $rstRtn['tot_cnt'] = $rst->cnt;
 *
 *****************************************************************************************************/
class Auth extends CI_Controller {
	var $offset = 0;
	var $limit = CFG_BACK_PAGINATION_ITEM_PER_PAGE;
	var $is_local = FALSE;
	

    //========================================================================
    // construct
    //========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');
		
		// session
		// $this->load->library('session');
	}
	
	
	
	
    //########################################################################
    // Auth
    //########################################################################
	
	//========================================================================
    // index
    //========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------------------------------------------
	public function index() {
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// auth_list : 권한그룹 LIST
	//-----------------------------------------------------------------------------------------------------------------
	public function auth_list($rtn_data=FALSE) {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		$use_yn = $param['rdoUse'];
		if($use_yn == 2) $use_yn = 0;
		
		// page
		$page = isset($param['page']) ? (int)$param['page'] : 1;
		$offset = ($page * $this->limit) - $this->limit;
		$limit = $this->limit;
		
		// keyword
		$keyword = isset($param['keyword']) && $param['keyword'] ? $param['keyword'] : '';
		
		// where
		$where = array(
			'offset' => $offset
			, 'limit' => $limit
			, 'use_yn' => $use_yn
		);
		$where2 = NULL;
		if($keyword) {
			$where['oper_auth_name'] = $keyword;
		}
		
		$rst = $this->manager->get_auth_grp_list($where, $where2);
		$rstRtn['data']['data'] = $rst['data'];
		$rstRtn['data']['tot_cnt'] = $rst['tot_cnt'];
// 		$rstRtn['data']['query'] = $rst['query'];
		
		// 만약, 데이터만 리턴하는 경우 - class내부에서 호출시 사용
		if($rtn_data === TRUE) {
			return $rstRtn;
		}
		else {
			$this->load->view('json_view', $rstRtn);
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del : 권한그룹 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		$data['oper_auth_grp_id'] = base64_decode($param['grp_id']);
		
		$rst = $this->manager->del_auth_grp($data);
		
		if($rst['rst'] == 'exist') {
			$rstRtn['data']['rst'] = $rst['rst'];
			$this->load->view('json_view', $rstRtn);
		}
		else {
			// return a list data
			$rstRtn = self::auth_list(TRUE);
			$rstRtn['data']['rst'] = $rst['rst'];
			$this->load->view('json_view', $rstRtn);
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// using : 권한그룹 사용처리
	//-----------------------------------------------------------------------------------------------------------------
	public function using() {
		self::_use_or_not_and_multi_del(1);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// not_using : 권한그룹 사용중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function not_using() {
		self::_use_or_not_and_multi_del(0);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi : 권한그룹 사용중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi() {
		self::_use_or_not_and_multi_del(2);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _use_or_not_and_multi_del - 다중 사용,중지,삭제
	//-----------------------------------------------------------------------------------------------------------------
	private function _use_or_not_and_multi_del($args) {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		if(strpos($param['grp_id'], CFG_AUTH_CODE_DELIMITER) !== FALSE) {
			$arr_tmp = explode(CFG_AUTH_CODE_DELIMITER, $param['grp_id']);
			$tmp_id = '';
			foreach($arr_tmp as $tmp) {
				if($tmp_id != '') $tmp_id .= CFG_AUTH_CODE_DELIMITER;
				$tmp_id .= base64_decode($tmp);
			}
			$data['oper_auth_grp_id'] = $tmp_id;
		}
		else {
			$data['oper_auth_grp_id'] = base64_decode($param['grp_id']);
		}
		
		// del
		$len = 0;
		if($args == 2) {
			$rst_tmp1 = $this->manager->del_multi_auth_grp($data);
			$rst_tmp2 = explode(CFG_AUTH_CODE_DELIMITER, $rst_tmp1);
			$rst = 'exist';
			$len = count($rst_tmp2);
		}
		else {
			$data['use_yn'] = $args;
			$rst = $this->manager->use_or_not_use_auth_grp($data);
		}
		
		// return a list data
		$rstRtn = self::auth_list(TRUE);
		$rstRtn['data']['rst'] = $rst;
		$rstRtn['data']['len'] = $len;
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_edit_view_data : 등록/수정 뷰화면 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function get_edit_view_data($args=NULL) {
		// self::_chk_session() ;
		
		if(is_null($args)) {
			$param = $this->input->get(NULL, TRUE);
		}
		else {
			$param['kind'] = $args['kind'];
			$param['id'] = $args['id'];
		}
		
		$m_code = CFG_MASTER_CODE_COUNSEL_MANAGE .','. 
			CFG_MASTER_CODE_COUNSEL_STATISTIC .','. 
			CFG_MASTER_CODE_BOARD .','. 
			CFG_MASTER_CODE_OPERATOR .','. 
			CFG_MASTER_CODE_AUTH .','. 
			CFG_MASTER_CODE_MANAGE_CODE .','.
			CFG_MASTER_CODE_LAW_HELP .','.
			CFG_MASTER_CODE_BIZ_COUNSEL .','.
			CFG_MASTER_CODE_BIZ_COUNSEL_STATISTIC;
			
		$rstRtn['data']['base_code'] = $this->manager->get_sub_code_by_m_code_auth($m_code);

		// 처리 구분 : add,view,edit
		$rstRtn['data']['kind'] = $param['kind'];
		
		// setting
		$rstRtn['data']['oper_auth_grp_id'] = '';
		$rstRtn['data']['oper_auth_name'] = '';
		$rstRtn['data']['etc'] = '';
		
		$rstRtn['data']['arr_auth_code'] = [];
		
		// edit
		if($param['kind'] == 'edit' || $param['kind'] == 'view') {
			// 해당 권한그룹에 할당된 권한 코드값 변수
			$rstRtn['data']['auth_code'] = array();
			
			$data = array(
				'oper_auth_grp_id' => base64_decode($param['id'])
			);
			$rst = $this->manager->get_auth($data);
			// 해당 권한그룹에 할당된 권한 코드값
			$oper_auth_codes = $rst['oper_auth_codes'];
			if(strpos($oper_auth_codes, CFG_AUTH_CODE_DELIMITER) !== FALSE) {
				$rstRtn['data']['arr_auth_code'] = explode(CFG_AUTH_CODE_DELIMITER, $oper_auth_codes);
			}
			
			$rstRtn['data']['oper_auth_grp_id'] = base64_encode($rst['oper_auth_grp_id']);
			$rstRtn['data']['oper_auth_name'] = $rst['oper_auth_name'];
			$rstRtn['data']['etc'] = $rst['etc'];
		}
		
		if(is_null($args)) {
			$this->load->view('adm_auth_grp_edit_view', $rstRtn);
		}
		else {
			return $rstRtn['data'];
		}
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// auth : 실제 DB 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function add() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		$dec_code = self::_decode_grp_code( $param['grp_code']);
		
		$data = array(
			'oper_auth_codes' => $dec_code
			, 'oper_auth_name' => $param['grp_nm']
			, 'use_yn' => 1
			, 'etc' => $param['grp_etc']
		);
		$rst = $this->manager->add_auth_grp($data);

		// 통계>상담사례 보기 권한 체크
		// 주석처리: 권한변경시 재로긴해야 한다고 알려주기 때문에 굳이 세션 업데이트 불필요하다 판단하여 주석처리, dylan 2019.08.05
// 		self::_check_auth_csl_view($dec_code);

		$rstRtn['data']['rst'] = 'succ';
		
		$this->load->view('json_view', $rstRtn);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// auth : 실제 DB 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function edit() {
//		self::_chk_session() ;
		
		$param = $this->input->post(NULL, TRUE);
		
		$enc_code = $param['grp_code'];
		$dec_code = self::_decode_grp_code($enc_code);
		
		// update
		$data = array(
			'oper_auth_grp_id' => base64_decode($param['grp_id'])
			, 'oper_auth_name' => $param['grp_nm']
			, 'oper_auth_codes' => $dec_code
			, 'etc' => $param['grp_etc']
		);
		
		$rst = $this->manager->edit_auth_grp($data);
		
		// 세션정보 업데이트 - 수정한 권한그룹의 id가 로그인한 사람과 같을 경우
		// $admin = new Admin();
		// $owner_grp_id = $admin->get_session_by_key(CFG_SESSION_ADMIN_AUTH_GRP_ID);

		// if($owner_grp_id == $param['grp_id']) {
		// 	$admin->update_session(array(CFG_SESSION_ADMIN_AUTH_CODES=>$enc_code));
		// }
		
		// 통계>상담사례 보기 권한 체크
		// 주석처리: 권한변경시 재로긴해야 한다고 알려주기 때문에 굳이 세션 업데이트 불필요하다 판단하여 주석처리, dylan 2019.08.05
// 		self::_check_auth_csl_view($dec_code);
		
		$rstRtn['data']['rst'] = 'succ';
		
		$this->load->view('json_view', $rstRtn);
	}


	//-----------------------------------------------------------------------------------------------------------------
	// 통계>상담사례 보기 권한 세팅
	//-----------------------------------------------------------------------------------------------------------------
	private function _check_auth_csl_view($args) {
		$auth_csl_view = 0;
		if(strpos($args, CFG_EXCEPTION_CODE_COUNSEL_STATISTIC_VIEW_AUTH) != FALSE) {
			$auth_csl_view = 1;
		}
		$this->session->set_userdata(CFG_SESSION_ADMIN_AUTH_CSL_VIEW, $auth_csl_view);
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _decode_grp_code : 코드 decode 처리 
	//-----------------------------------------------------------------------------------------------------------------
	private function _decode_grp_code($enc_code) {
		$dec_code = '';
		// 코드 decode 처리 
		if(strpos($enc_code, CFG_AUTH_CODE_DELIMITER) !== FALSE) {
			$arr_tmp = explode(CFG_AUTH_CODE_DELIMITER, $enc_code);
			foreach($arr_tmp as $data) {
				if($dec_code != '') {
					$dec_code .= CFG_AUTH_CODE_DELIMITER;
				}
				if($data != '|') {
					$dec_code .= base64_decode($data);
				}
				else {
					$dec_code .= '|';
				}
			}
			// $dec_code = str_replace('|', '', $dec_code);
		}
		
		return $dec_code;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	// req_auth - check authorization
	//-----------------------------------------------------------------------------------------------------------------
	// $target_auth_code : 권한을 체크할 메뉴 권한 코드
	//-----------------------------------------------------------------------------------------------------------------
	public function req_auth($oper_auth_codes=NULL, $target_auth_code=NULL, $is_json=TRUE) {
		$is_sess_out = self::_chk_session(FALSE);

		// 세션이 종료된 경우
		if($is_sess_out == TRUE) {
			$rstRtn['data']['rst'] = 'session_timeout';

		}
		else {
			if(is_null($oper_auth_codes)) {
				// 접속한 운영자의 권한코드
				$oper_auth_codes = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_CODES);
				// 메뉴의 권한코드
				$target_auth_code = $this->input->post('c');
			}

			$rst_chk_code = 'fail';
			
			$arr_auth = explode(CFG_AUTH_CODE_DELIMITER, $oper_auth_codes);
			
			foreach($arr_auth as $data) {
				if($target_auth_code == $data) {
					$rst_chk_code = 'succ';
					break;
				}
			}
			// 권리구제의 뷰 권한이 별도로 있는지 체크 - 뷰 권한은 입력 권한과 별개로 입력권한 아랫단계다. 단순 뷰만 할 수 있다.
			// if($rst_chk_code == 'fail') {
			// 	$auth_lh_view_cd = base64_encode(CFG_EXCEPTION_CODE_LAW_HELP_CONTENT_VIEW_AUTH);
			// 	$admin_auth_code = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_CODES);
			// 	$auth_lh_view = stripos($admin_auth_code, $auth_lh_view_cd) !== FALSE ? 1 : 0;
			// 	if($auth_lh_view == 1) {
			// 		$rst_chk_code = 'succ';
			// 	}
			// }

			$rstRtn['data']['rst'] = $rst_chk_code;
		}
		
		if($is_json) {
			$this->load->view('json_view', $rstRtn);
		}
		else {
			return $rstRtn;
		}
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// _chk_session : 세션 상태 체크
	//-----------------------------------------------------------------------------------------------------------------
	private function _chk_session($is_logout=TRUE) {
		$admin = new Admin();
		$out = $admin->is_out_session();
		
		if($is_logout) {
			if($out === TRUE) {
				$rstRtn['data']['data'] = 'session_timeout';
				return $admin->logout();
			}
		}
		else {
			return $out;
		}
	}
}
