<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// AES encrypt library
// require_once './lib/aes.class.php';
// require_once './lib/aesctr.class.php'; 



/******************************************************************************************************
 * Excel 생성 파일
 * Excel
 * 작성자 : delee
 *
 *
 *
 *****************************************************************************************************/
class Excel extends CI_Controller {
	

	//========================================================================
	// construct
	//========================================================================
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
		
		// base model
		$this->load->model('db_admin', 'manager');

		$this->load->library('PHPExcel');
		
		$this->load->helper('excel_helper');
	}
	
	
	
	
	//########################################################################
	// Excel
	//########################################################################

	//========================================================================
	// index
	//========================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// index
	//-----------------------------------------------------------------------------------------------------------------
	public function index() {
	}

	

	//-----------------------------------------------------------------------------------------------------------------
	// download
	//-----------------------------------------------------------------------------------------------------------------
	public function download($args) {

		// 다운로드 구분
		$kind = $args['kind'];

		// PHPExcel 클래스 선언
		$objPHPExcel = new PHPExcel();
		 
		// 문서 속성 설정
		$objPHPExcel->getProperties()
			->setTitle('Labors Counsel')
			->setSubject('Labor\'s Counsel Management Statistics Document')
			->setCreator('labors.or.kr')
			->setLastModifiedBy('labors.or.kr')
			->setDescription('Labor\'s Counsel Management Statistics Document');
		 
		// Create the worksheet
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();		
		// rename a sheet
		$sheet->setTitle('상담내역통계');

		$excel_data = [];
		
		// 통계별 데이터 세팅
		// - 상담사례, 교차통계, 월별통계
		if($kind == 'down01' || $kind == 'down11' || $kind == 'down12'){
			$excel_data_tmp = $args['data']['data'];
			$excel_data_tot = array();
			// 총계 row
			if(isset($args['data']['data_tot'])) {
				$tmp = new stdClass;
				$tmp->T0 = '총계';
				$arr_tmp = array(0=>$tmp);
				$excel_data_tot = array(0=>(object)array_merge((array)$tmp, (array)$args['data']['data_tot'][0])); // 합계 row data
			}
			$excel_data = array_merge($excel_data_tmp, $excel_data_tot);
		}
		// - 임금근로시간, 소속별, 시계열 통계, 기본통계는 해당코드 블럭에서 처리
		else if($kind != 'down13') {
			$excel_data = $args['data']['data'];
		}
		
		// 통계유형코드 데이터(가로축)
		$header_data = [];
		$header_nm = '';
		$arr_header_nm = [];
		$cell_width = '';
		$arr_cell_width = [];
		
		// 
		$alphabet = 65;
		$alpha_char = '';
		$str = '';
		$view_url = 'https://db.labors.or.kr/?c=admin&m=main&menu=SEFLMFJZdzVobGNud1FLVmk0OGV3WEhRYWltOGx3PT0=&code=UzAwMg==&kind=edit&id=';

		// 1.상담사례
		if($kind == 'down01') {
			// header cell text
//			$header_nm = '상담일●상담자명●성별●거주지●직종●상담방법●업종●회사소재지●경기도/기타●근로자수●고용형태●유형●4대보험●근로계약서●월평균임금●근로시간●상담내용●답변';
			// 연령대기타 추가 2018.07.09 dylan
			$header_nm = '번호●상담일●기관●상담자●이름●상담방법●상담방법-상세●연령대●연령대-상세●성별●거주지●거주지-기타●직종●직종-상세●업종●업종-상세●회사소재지●회사소재지-기타●고용형태●고용형태-기타'
			// .'●사용주체' // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
			// 주제어 추가 2017.02.14 dylan
			// 근로자수기타 추가 2018.07.09 dylan
			.'●근로자수●근로자수-상세●근로계약서●4대보험●월평균임금●주당근로시간●상담유형1●상담유형2●상담유형3●처리결과●주제어●상담내용●답변';
			// cell width
			$cell_width = '7●13●20●20●13●13●13●13●13●13●13●13●13●13●13●13●13●13●13●13'
			// .'●10' // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
			.'●13●13●13●13●13●13●13●13●13●13●30●30●30';

			// cell에 데이터 할당
			$index = count($excel_data);
			$row = 2;
			foreach($excel_data as $rs) {
				$alpha_index = 1;
				$sheet->setCellValue('A' . $row, $index--);
				$sheet->setCellValue('B' . $row, $rs->csl_date);
				$sheet->setCellValue('C' . $row, $rs->asso_name);
				$sheet->setCellValue('D' . $row, $rs->oper_name);
				$sheet->setCellValue('E' . $row, $rs->csl_name);
				$sheet->setCellValue('F' . $row, $rs->csl_type);
				$sheet->setCellValue('G' . $row, $rs->s_code_etc);
				$sheet->setCellValue('H' . $row, $rs->ages);
				$sheet->setCellValue('I' . $row, $rs->ages_etc);
				$sheet->setCellValue('J' . $row, $rs->gender);
				$sheet->setCellValue('K' . $row, $rs->live_addr);
				$sheet->setCellValue('L' . $row, $rs->live_addr_etc);
				$sheet->setCellValue('M' . $row, $rs->work_kind);
				$sheet->setCellValue('N' . $row, $rs->work_kind_etc);
				$sheet->setCellValue('O' . $row, $rs->comp_kind);
				$sheet->setCellValue('P' . $row, $rs->comp_kind_etc);
				$sheet->setCellValue('Q' . $row, $rs->comp_addr);
				$sheet->setCellValue('R' . $row, $rs->comp_addr_etc);
				$sheet->setCellValue('S' . $row, $rs->emp_kind);
				$sheet->setCellValue('T' . $row, $rs->emp_kind_etc);

				// 상담일에 해당 상담내용 페이지 링크 추가
				$sheet->getCell('B' . $row)->getHyperlink()->setUrl($view_url. $rs->seq);

				// emp_use_kind 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				/*
				$sheet->setCellValue('N' . $row, $rs->emp_use_kind);
				$sheet->setCellValue('O' . $row, $rs->emp_cnt);
				$sheet->setCellValue('P' . $row, $rs->emp_paper_yn);
				$sheet->setCellValue('Q' . $row, $rs->emp_insured_yn);
				$sheet->setCellValue('R' . $row, $rs->ave_pay_month);
				$sheet->setCellValue('S' . $row, $rs->work_time_week);
				$sheet->setCellValue('T' . $row, $rs->csl_kind_1);
				$sheet->setCellValue('U' . $row, $rs->csl_kind_2);
				$sheet->setCellValue('V' . $row, $rs->csl_kind_3);
				$sheet->setCellValue('W' . $row, $rs->csl_proc_rst);
				$sheet->setCellValue('X' . $row, $rs->csl_content);
				$sheet->setCellValue('Y' . $row, $rs->csl_reply);
				*/
				$sheet->setCellValue('U' . $row, $rs->emp_cnt);
				$sheet->setCellValue('V' . $row, $rs->emp_cnt_etc);
				$sheet->setCellValue('W' . $row, $rs->emp_paper_yn);
				$sheet->setCellValue('X' . $row, $rs->emp_insured_yn);
				$sheet->setCellValue('Y' . $row, $rs->ave_pay_month);
				$sheet->setCellValue('Z' . $row, $rs->work_time_week);
				// 상담유형 3개 값 분리
				$arr_csl_kind_cd = explode(',', $rs->csl_kind);
				$sheet->setCellValue('AA' . $row, isset($arr_csl_kind_cd[0]) ? $arr_csl_kind_cd[0] : '');
				$sheet->setCellValue('AB' . $row, isset($arr_csl_kind_cd[1]) ? $arr_csl_kind_cd[1] : '');
				$sheet->setCellValue('AC' . $row, isset($arr_csl_kind_cd[2]) ? $arr_csl_kind_cd[2] : '');
				$sheet->setCellValue('AD' . $row, $rs->csl_proc_rst);
				$sheet->setCellValue('AE' . $row, $rs->csl_keywords);// 주제어 추가 2017.02.14 dylan
				$sheet->setCellValue('AF' . $row, $rs->csl_content);
				$sheet->setCellValue('AG' . $row, $rs->csl_reply);
				$row++;
			}
		}
		
		// 4.임금근로시간 통계
		else if($kind == 'down04') {
			// header cell text
			$header_nm = '구분●임금 (빈도)●근로시간 (빈도)●전체상담건수';
			// cell width
			$cell_width = '30●30●30●30';

			// 통계유형 구분(가로)에 해당하는 코드의 컬럼명 가져온다.
			$sel_csl_cd = $args['csl_code_idx'];
			$sel_col = $args['sel_col'];
			
			// cell에 데이터 할당
			$sum_pay = 0;
			$sum_work = 0;
			$sum_pay_cnt = 0; 
			$sum_work_cnt = 0;
			$dst_p_tot = 0; 
			$dst_w_tot = 0;
			$tot_title = '전체 (빈도)'. ($sel_col == 11?'(실건수)':'');
			// 4-1.단순 통계
			if($sel_csl_cd == "") {
				$row = 2;
				$txt_dst_pcnt_sum = ''; 
				$txt_dst_wcnt_sum = '';
				$txt_dst_tot = '';
				$target1_tot = $args['data']['target1_tot'];// 항목별 전체건수
				$sum_target1_tot = 0;// 항목별 전체건수 합계
				$dst_csl_tot = $args['data']['dst_csl_tot'];//상담유형인 경우 실건수
				foreach($excel_data as $i => $rs) {
					$sheet->setCellValue('A'. $row, $rs->subject);		
					$sheet->setCellValue('B'. $row, number_to_currency($rs->pay) .' ('. number_to_currency($rs->p_cnt) .')');
					$sheet->setCellValue('C'. $row, number_to_currency($rs->works) .' ('. number_to_currency($rs->w_cnt) .')');
					$sheet->setCellValue('D'. $row, number_to_currency($target1_tot[$i]->t_cnt));
					$sum_target1_tot += (int)$target1_tot[$i]->t_cnt;
					$sum_pay += (int)$rs->pay * (int)$rs->p_cnt;
					$sum_work += (int)$rs->works * (int)$rs->w_cnt;
					$sum_pay_cnt += (int)$rs->p_cnt;
					$sum_work_cnt += (int)$rs->w_cnt;
					if($sel_col == 11){//상담유형 - 실건수
						$txt_dst_pcnt_sum = '('. number_to_currency($rs->dst_p_tot) .')';
						$txt_dst_wcnt_sum = '('. number_to_currency($rs->dst_w_tot) .')';
						$txt_dst_tot = '('. number_to_currency($dst_csl_tot) .')';
					}
					$row++;
				}
				$sheet->setCellValue('A'. $row, $tot_title);
				$sheet->setCellValue('B'. $row, number_to_currency(round($sum_pay/$sum_pay_cnt,0)) .' ('. number_to_currency($sum_pay_cnt) .')'. $txt_dst_pcnt_sum);
				$sheet->setCellValue('C'. $row, number_to_currency(round($sum_work/$sum_work_cnt,0)) .' ('. number_to_currency($sum_work_cnt) .')' . $txt_dst_wcnt_sum);
				$sheet->setCellValue('D'. $row, number_to_currency($sum_target1_tot) . $txt_dst_tot);
				$row++;
			}
			// 4-2.임금근로시간 교차통계
			// - 통계화면 table의 데이터로 엑셀 생성한다.
			else {
				// 엑셀데이터 배열
				$arr_excel = [];
				$row = count($excel_data) + 1;
				if($sel_csl_cd == 12) {
					$sel_csl_cd = 14;
				}
				else if($sel_csl_cd == 13) {
					$sel_csl_cd = 0;
				}
				$header_data = self::get_cmm_code($args, $sel_csl_cd);

				$arr_header_nm = explode(CFG_AUTH_CODE_DELIMITER_2, $header_data['header_nm']);

				foreach($excel_data as $i => $items) {
					$arr_excel[$i] = explode('|', $items);
				}
				
				$sheet->fromArray($arr_excel);
			}
		}

		// 5.소속별 통계
		else if($kind == 'down05') {
			// 통계유형 구분(가로)에 해당하는 코드의 컬럼명 가져온다.
			$header_data = self::get_cmm_code($args, $args['sch_kind']);
			
			// header cell text
			$header_nm = "구분". CFG_AUTH_CODE_DELIMITER_2 . $header_data['header_nm'];
			if($args['sch_kind'] == 13) {// 임금근로시간
				$header_nm .= CFG_AUTH_CODE_DELIMITER_2 .'전체상담건수';
			}
			else {// 임금근로시간 외
				$header_nm .= CFG_AUTH_CODE_DELIMITER_2 .'총계'. ($args['sch_kind'] == 11 ? '(실건수)':'') . '(비율)';
			}
			// cell width
			$cell_width = "50". CFG_AUTH_CODE_DELIMITER_2 . $header_data['cell_width'] . CFG_AUTH_CODE_DELIMITER_2 ."30";
			
			$cell_cnt = count(explode(CFG_AUTH_CODE_DELIMITER_2, $header_data['cell_width']));
			// cell에 데이터 할당
			$row = 2;
			$tot_sum = [];
			$target1_tot = $args['data']['target1_tot'];// 항목별 전체건수
			$sum_target1_tot = 0;// 항목별 전체건수 합계
			foreach($excel_data as $idx => $rs1) {
				$rs = (array)$rs1;
				$sheet->setCellValue('A'. $row, $rs['subject']);
				$alpha_char = 66;//B
				//임금근로시간 인 경우
				if($args['sch_kind'] == 13) {
					$sheet->setCellValue(chr($alpha_char++) . $row, number_to_currency($rs['pay']) .' ('. number_to_currency($rs['p_cnt']) .')');
					$sheet->setCellValue(chr($alpha_char++) . $row, number_to_currency($rs['works']) .' ('. number_to_currency($rs['w_cnt']) .')');
					$sheet->setCellValue(chr($alpha_char++) . $row, number_to_currency($target1_tot[$idx]->t_cnt));
					$sum_target1_tot += (int)str_replace(',', '', $target1_tot[$idx]->t_cnt);
					$tot_sum[0] += $rs['pay'] * $rs['p_cnt'];
					$tot_sum[1] += $rs['p_cnt'];
					$tot_sum[2] += $rs['works'] * $rs['w_cnt'];;
					$tot_sum[3] += $rs['w_cnt'];
				}
				else {
					for($i=0; $i<$cell_cnt; $i++) {
						$sheet->setCellValue(chr($alpha_char++) . $row, mixedNumberToCurrency($rs['ck'. ($i+1)]));
					}
					$sheet->setCellValue(chr($alpha_char) . $row, mixedNumberToCurrency($rs['tot']));//총계
				}
				$row++;
			}
			//총계 - 임금근로시간 인 경우
			if($args['sch_kind'] == 13) {
				$sheet->setCellValue('A'. $row, '전체 (빈도)');
				$sheet->setCellValue('B'. $row, number_to_currency(round($tot_sum[0]/$tot_sum[1],0)) .' ('. number_to_currency($tot_sum[1]) .')');
				$sheet->setCellValue('C'. $row, number_to_currency(round($tot_sum[2]/$tot_sum[3],0)) .' ('. number_to_currency($tot_sum[3]) .')');
				$sheet->setCellValue('D'. $row, number_to_currency($sum_target1_tot));
			}
			else {
				$sheet->setCellValue('A'. $row, '총계 (비율)');
				$alpha_char = 66;//B
				// 마지막 row 총계
				$tmp = $args['data']['data_tot'];
				$rs = array_values((array)$tmp[0]);
				$tot = trim(explode('(', $rs[count($rs)-1])[0]);
				for($i=0; $i<=$cell_cnt; $i++) {
					$val = $rs[$i];
					$per = '';
					if($i < $cell_cnt) {// 제일 마지막 총계는 이미 있기 때문에 추가안함
						$per = '('. round(($val/$tot)*100, 1, PHP_ROUND_HALF_UP) .'%)';
					}
					$sheet->setCellValue(chr($alpha_char++) . $row, mixedNumberToCurrency($val) . $per);
				}
			}
			$row++;//총건수 영역을 센터정렬로 처리하기 위한조치
		}

		// 7-1.시계열통계 : 건수/비율
		else if($kind == 'down07_1') {
			
			// cell에 데이터 할당
			$row = 3;
			foreach($excel_data as $idx => $rs1) {
				$rs = (array)$rs1;
				$sheet->setCellValue('A'. $row, $rs['subject']);
				$alpha_char = 66;//B
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					$sheet->setCellValue(chr($alpha_char) . $row, number_to_currency($rs['ck_'. $y]));
					$sheet->setCellValue(chr($alpha_char+1) . $row, $rs['cr_'. $y] .'%');
					// 헤더 년도영역 셀 merge
					$sheet->mergeCells(chr($alpha_char) .'1:'. chr($alpha_char+1) .'1');
					$alpha_char += 2;
				}
				$row++;
			}
			$data_tot = (array)$args['data']['data_tot'];
			$sheet->setCellValue('A'. $row, '총계'. ($args['sch_kind']==9 ? '(실건수)' : ''));
			$alpha_char = 66;//B
			// 마지막 row 총계;
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$sheet->setCellValue(chr($alpha_char) . $row, mixedNumberToCurrency($data_tot['ck_'. $y]));// 건수
				$sheet->setCellValue(chr($alpha_char+1) . $row, $data_tot['cr_'. $y] .'%');// 비율
				$alpha_char += 2;
			}
			$row++;//총계 영역을 센터정렬로 처리하기 위한조치

		}
		// 7-2.시계열통계 : 임금근로시간
		else if($kind == 'down07_2') {

			// cell에 데이터 할당
			$row = 3;
			$tot_sum = [[],[]];
			foreach($excel_data as $idx => $rs1) {
				$rs = (array)$rs1;
				$sheet->setCellValue('A'. $row, $rs['subject']);
				$alpha_char = 66;//B
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					$sheet->setCellValue(chr($alpha_char) . $row, number_to_currency($rs['p_'. $y]));
					$sheet->setCellValue(chr($alpha_char+1) . $row, $rs['w_'. $y]);
					$v1 = trim(explode('(', $rs['p_'. $y])[0]);
					$v2 = trim(explode(')', explode('(', $rs['p_'. $y])[1])[0]);
					$v3 = trim(explode('(', $rs['w_'. $y])[0]);
					$v4 = trim(explode(')', explode('(', $rs['w_'. $y])[1])[0]);
					$tot_sum[$y-$args['begin']][0] += $v1 * $v2;
					$tot_sum[$y-$args['begin']][1] += $v2;
					$tot_sum[$y-$args['begin']][2] += $v3 * $v4;
					$tot_sum[$y-$args['begin']][3] += $v4;
					// 헤더 년도영역 셀 merge
					$sheet->mergeCells(chr($alpha_char) .'1:'. chr($alpha_char+1) .'1');
					$alpha_char += 2;
				}
				$row++;
			}
			$data_tot = (array)$args['data']['data_tot'];
			// 총계
			$sheet->setCellValue('A'. $row, '총계 (빈도)');
			$alpha_char = 66;//B
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$sheet->setCellValue(chr($alpha_char) . $row, mixedNumberToCurrency(round($tot_sum[$y-$args['begin']][0]/$tot_sum[$y-$args['begin']][1],0) .'( '. $tot_sum[$y-$args['begin']][1] .')'));
				$sheet->setCellValue(chr($alpha_char+1) . $row, mixedNumberToCurrency(round($tot_sum[$y-$args['begin']][2]/$tot_sum[$y-$args['begin']][3],1) .'( '. $tot_sum[$y-$args['begin']][3] .')'));
				$alpha_char += 2;
			}
			$row++;//총계 영역을 센터정렬로 처리하기 위한조치
			
		}
		
		// 11.교차통계
		else if($kind == 'down11') {
			// 통계유형 구분(가로)에 해당하는 코드의 컬럼명 가져온다.
			$k = $args['sel_col'];
			if($args['sel_col'] == 12) {
				$k = 14;
			}
			$header_data = self::get_cmm_code($args, $k);

			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 . $header_data['header_nm'] . 
				CFG_AUTH_CODE_DELIMITER_2 .'총계'. ($args['sel_col']==11?'(실건수)':''). '(비율)';
			// cell width
			$cell_width = '40●'. $header_data['cell_width'] .'●20';

			// cell에 데이터 할당
			$row = 2;
			$loop = 1;
			$arr_tot = array_splice($excel_data, count($excel_data)-1, 1);
			foreach($excel_data as $rs) {
				$loop = 1;
				foreach($rs as $item) {
					$str = getColFromNumber($loop++) . $row;
					$sheet->setCellValue($str, mixedNumberToCurrency($item));
				}
				$row++;
			}
			
			// 총계
			$tot = trim(explode("(", $arr_tot[0]->tot)[0]);
			$loop = 1;
			$str = getColFromNumber($loop) . $row;
			$sheet->setCellValue($str, '총계'. ($args['csl_code']==base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND)?'(실건수)':'') .'(비율)');
			foreach($arr_tot[0] as $i => $item) {
				if($item != '총계') {
					$val = trim(explode('(', $item)[0]);
					$per = '';
					if($i != 'tot') {
						$per = '('. round($item/$tot*100, 1, PHP_ROUND_HALF_UP) .'%)';
					}
					$str = getColFromNumber(++$loop) . $row;
					$sheet->setCellValue($str, mixedNumberToCurrency($item) . $per);
				}
			}
			$row++;
		}

		// 12.월별통계
		else if($kind == 'down12') {
			$half_year = $args['half_year'];
			if($half_year == 1) { 
				$header_nm = '1월●2월●3월●4월●5월●6월●7월●8월●9월●10월●11월●12월';
				$cell_width = '10●10●10●10●10●10●10●10●10●10●10●10';
			}
			else if($half_year == 2) { 
				$header_nm = '1월●2월●3월●4월●5월●6월';
				$cell_width = '10●10●10●10●10●10';
			}
			else if($half_year == 3) { 
				$header_nm = '7월●8월●8월●10월●11월●12월';
				$cell_width = '10●10●10●10●10●10';
			}

			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 . $header_nm . CFG_AUTH_CODE_DELIMITER_2 .'총계';
			// cell width
			$cell_width = '40●'. $cell_width .'●20';
			
			// cell에 데이터 할당
			$row = 2;
			foreach($excel_data as $rs) {
				$loop = 1;
				foreach($rs as $item) {
					$str = getColFromNumber($loop++) . $row;
					$sheet->setCellValue($str, mixedNumberToCurrency($item));
				}
				$row++;
			}
		}

		// 13.기본통계
		else if($kind == 'down13') {
			// header cell text
			$header_nm = '구분'. CFG_AUTH_CODE_DELIMITER_2 .'상담건수'. CFG_AUTH_CODE_DELIMITER_2 .'퍼센트';

			// cell width
			$cell_width = '40●40●40';
			// cell에 데이터 할당
			$row = 2;
			$cell_index = 0;
			$tot = 0;
			foreach($args['data']['data_tot'][0] as $key => $rs) {
				if($cell_index == 2){	
					$row++;
					$cell_index = 0;
				}				
				$dbl_alpha_char = '';
				$alpha_index = 0;
				
				$str = $dbl_alpha_char . chr($alphabet + $cell_index) . $row;
				$val = is_hangul($rs) ? ($rs=='총계'&&$args['sch_kind']==12?'총계(실건수)':$rs) : mixedNumberToCurrency($rs);
				$sheet->setCellValue($str, $val);
				if($cell_index == 1){
					$tot = round(($rs/$args['data']['data_tot'][0]->tot)*100, 2);
					$sheet->setCellValue('C'.$row, mixedNumberToCurrency($tot) .' %');
				}
				$cell_index++;
			}
			$row++;
		}
		
		$index = 0;
		$alpha_index = 0;
		$dbl_alpha_char = '';

		//=======================
		//
		// 헤더 텍스트 적용 및 스타일 설정
		//
		//=======================
		$header_row = 1;
		
		// 시계열통계
		if(strpos($kind, 'down07_') !== FALSE) {
			$header_row = 2;

			// header cell text, cell width
			$header_nm = ['구분',''];
			$cell_width = '30';
			// 건수/비율
			$cell_title1 = '건수';
			$cell_title2 = '비율';
			if($kind == 'down07_2') {// 임금근로시간
				$cell_title1 = '월평균임금 (빈도)';
				$cell_title2 = '주당근로시간 (빈도)';
			}
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$header_nm[0] .= CFG_AUTH_CODE_DELIMITER_2 . $y .'년'. CFG_AUTH_CODE_DELIMITER_2 . $y .'년';//아래 for문에서 동일하게 처리하기 위해 한번 더 추가함
				$header_nm[1] .= CFG_AUTH_CODE_DELIMITER_2 . $cell_title1 . CFG_AUTH_CODE_DELIMITER_2 . $cell_title2;
				$cell_width .= CFG_AUTH_CODE_DELIMITER_2 .'20'. CFG_AUTH_CODE_DELIMITER_2 .'20';
			}

			// header cell text
			$arr_header_nm = [[],[]];
			$arr_header_nm[0] = explode(CFG_AUTH_CODE_DELIMITER_2, $header_nm[0]);
			$arr_header_nm[1] = explode(CFG_AUTH_CODE_DELIMITER_2, $header_nm[1]);
			// cell width
			$arr_cell_width = explode(CFG_AUTH_CODE_DELIMITER_2, $cell_width);
			//
			for($i=0; $i<count($arr_cell_width); $i++) {
				// Z까지 갔다면 다시 앞에 A를 붙여 A부터 시작하게 한다.
				if($alphabet+$index > 90) {
					$index = 0;
					$dbl_alpha_char = chr($alphabet+$alpha_index++);
				}
				$alpha_char = $dbl_alpha_char . chr($alphabet + $index);
				$index++;
				// header cell text
				$sheet->setCellValue($alpha_char .'1', $arr_header_nm[0][$i]);
				$sheet->setCellValue($alpha_char .'2', $arr_header_nm[1][$i]);
				// cell width
				$sheet->getColumnDimension($alpha_char)->setWidth($arr_cell_width[$i]);
			}
			// 헤더 구분영역 셀 merge
			$sheet->mergeCells('A1:A2');
		}
		// 임금근로시간통계 - 교차통계인 경우
		else if($kind == 'down04' && $args['csl_code'] != '') {
			$header_row = 2;
			// 구분 셀 머지
			$sheet->mergeCells('A1:A2');
			$col_len = max( array_map('count', $arr_excel) ) - 1;
			// 헤더 교차컬럼 가로 2칸씩 머지
			for($i=2; $i<$col_len; $i+=2) {
				$char1 = getColFromNumber($i);
				$char2 = getColFromNumber($i+1);
				$sheet->mergeCells($char1 .'1:'. $char2 .'1');
			}
			// 전체 영역 제목 셀 머지
			$char1 = getColFromNumber($col_len+1);
			$sheet->mergeCells($char1 .'1:'. $char1 .'2');
			$alpha_char = $char1;
			// 전체상담건수 row 셀 머지
			$sel_csl_cd_cnt = count($arr_header_nm) * 2;
			for($i=2; $i<=$sel_csl_cd_cnt; $i+=2) {
				$char1 = getColFromNumber($i);
				$char2 = getColFromNumber($i+1);
				$sheet->mergeCells($char1 . ($row-1) .':'. $char2 . ($row-1));
			}
			// 셀넓이
			for($i=0; $i<=$col_len; $i++) {
				$char1 = getColFromNumber($i+1);
				$sheet->getColumnDimension($char1)->setWidth(25);//일괄적용
			}
			
		}
		// 시계열통계 외 나머지 통계
		else {
			// header cell text
			$arr_header_nm = explode(CFG_AUTH_CODE_DELIMITER_2, $header_nm);
			// cell width
			$arr_cell_width = explode(CFG_AUTH_CODE_DELIMITER_2, $cell_width);
			//
			for($i=0; $i<count($arr_header_nm); $i++) {
				// Z까지 갔다면 다시 앞에 A를 붙여 A부터 시작하게 한다.
				if($alphabet+$index > 90) {
					$index = 0;
					$dbl_alpha_char = chr($alphabet+$alpha_index++);
				}
				$alpha_char = $dbl_alpha_char . chr($alphabet + $index);
				$index++;
				// header cell text
				$sheet->setCellValue($alpha_char .'1', $arr_header_nm[$i]);
				// cell width
				$sheet->getColumnDimension($alpha_char)->setWidth($arr_cell_width[$i]);
			}
		}
		
		// 헤더에 Bold 처리
		$sheet->getStyle('A1:'. $alpha_char . $header_row)->getFont()->setBold(true);

		// 헤더에 스타일 설정
		$sheet->getStyle('A1:'. $alpha_char . $header_row)->applyFromArray(
			array('fill'=>array(
					'type'=> PHPExcel_Style_Fill::FILL_SOLID
					,'color'=> array('rgb' => 'c5d9f1')
				),
				'borders' =>array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('rgb' => 'aaaaaa')
					)
				)
				,'font' => array(
					'size' => 9
				)
			)
		);

		// cell 가로,세로 가운데 정렬
		$sheet->getStyle('A1:'. $alpha_char . ($row - 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle('A1:'. $alpha_char . ($row - 1))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		
		// 상담사례 인 경우 주제어,상담내용,답변 셀은 좌측 정렬처리
		if($kind == 'down01') {
			$sheet->getStyle('AE2:AG'. ($row-1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		}

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $args['path'] .'"');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '. get_date() .' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setPreCalculateFormulas(false); //수식셀 사용안함(속도개선)
		
		ob_end_clean();
		ob_start();
		$objWriter->save('php://output');
		
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
		exit;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// download2 - download2는 기존 download를 일부 개선한 코드로 download는 통계에서 계속 사용한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function download2($args) {

		$excel_data = $args['data'];
		$kind = $args['kind'];

		// excel form 로딩
		// excel file name
		$file_name = 'counsel_list.xlsx';
		if($kind == 'lawhelp_list') {
			$file_name = 'legalsupport_list.xlsx';
		}
		$objPHPExcel = PHPExcel_IOFactory::load('./ci_app/views/excel_form/'. $file_name);
		
		// 문서 속성 설정
		$objPHPExcel->getProperties()
			->setTitle('Labors Counsel')
			->setSubject('Labor\'s Counsel Management Statistics Document')
			->setCreator('labors.or.kr')
			->setLastModifiedBy('labors.or.kr')
			->setDescription('Labor\'s Counsel Management Statistics Document');
		 
		// work sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$sheet = $objPHPExcel->getActiveSheet();
		
		// 테두리 그리기 및 center 정렬 범위
		$set_style_range = '';

		// 다운로드할 파일 이름
		$download_filename = '';

		// # 상담내역
		if($kind == 'counsel_list') {
			$download_filename = 'Counsel_List';

			// 처음 시작하는 cell row 번호
			$row_num = 3;
			$cnt = count($excel_data);

			foreach ($excel_data as $key => $value) {
					
				$sheet->setCellValue('A'. $row_num, $cnt)
					->setCellValue('B'. $row_num, $value->csl_way)
					->setCellValue('C'. $row_num, $value->csl_title)
					->setCellValue('D'. $row_num, $value->oper_name)
					->setCellValue('E'. $row_num, $value->asso_name)
					->setCellValue('F'. $row_num, $value->csl_date)
					->setCellValue('G'. $row_num, $value->csl_content)
					->setCellValue('H'. $row_num, $value->csl_reply);
					// 추가 : 상담내용, 답변 2018.07.03 dylan
				$row_num++;
				$cnt--;
			}

			$set_style_range = 'A3:H'. ($row_num-1);
			
			// 번호,상담방법,상담자,소속,상담일 - 가운데 정렬
			$styleArray = array(
				'alignment' => array(
				 	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
			);
			$sheet->getStyle('A3:B'. ($row_num-1))->applyFromArray($styleArray);
			$sheet->getStyle('D3:F'. ($row_num-1))->applyFromArray($styleArray);

			// 상담내용,답변 좌측정렬
			$styleArray = array(
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
				)
			);
			$sheet->getStyle('G3:H'. ($row_num-1))->applyFromArray($styleArray);
		}
		// # 권리구제 상담내역
		else if($kind == 'lawhelp_list') {

			// 다운로드할 파일 이름
			$download_filename = 'Legalsupport_List';
				
			// 처음 시작하는 cell row 번호
			$row_num = 3;
			$cnt = count($excel_data);

			foreach ($excel_data as $key => $value) {
				
				$lh_labor_asso_nm = $value->lh_labor_nm .($value->lh_labor_asso_nm==''?'':'('. $value->lh_labor_asso_nm .')');

				$sheet->setCellValue('A'. $row_num, $value->lh_code)
					// ->setCellValue('B'. $row_num, AesCtr::decrypt($value->lh_apply_nm, CFG_ENCRYPT_KEY, 256))
					->setCellValue('B'. $row_num, $value->lh_apply_nm)
					->setCellValue('C'. $row_num, $value->ages_nm)
					->setCellValue('D'. $row_num, $value->ages_etc)
					->setCellValue('E'. $row_num, $value->gender_cd_nm)
					->setCellValue('F'. $row_num, $value->lh_apply_comp_nm)
					->setCellValue('G'. $row_num, $value->work_kind_nm)//직종
					->setCellValue('H'. $row_num, $value->work_kind_etc)
					->setCellValue('I'. $row_num, $value->comp_kind_nm)//업종
					->setCellValue('J'. $row_num, $value->comp_kind_etc)
					->setCellValue('K'. $row_num, $value->sprt_kind_cd_nm)//지원종류
					->setCellValue('L'. $row_num, $value->sprt_kind_cd_etc)//지원종류-기타
					->setCellValue('M'. $row_num, $value->lh_kind_cd_nm)//사건유형 1차유형
					->setCellValue('N'. $row_num, $value->lh_kind_sub_cd_nm)//세부유형
					->setCellValue('O'. $row_num, $value->apply_organ_cd_nm)
					->setCellValue('P'. $row_num, $lh_labor_asso_nm)
					->setCellValue('Q'. $row_num, $value->sprt_organ_cd_nm)
					->setCellValue('R'. $row_num, $value->sprt_organ_cd_etc) // 대상기관_기타
					->setCellValue('S'. $row_num, $value->lh_sprt_cfm_date=='0000-00-00' ? '' : $value->lh_sprt_cfm_date)
					->setCellValue('T'. $row_num, $value->lh_accept_date=='0000-00-00' ? '' : $value->lh_accept_date)
					->setCellValue('U'. $row_num, $value->lid_date)
					->setCellValue('V'. $row_num, $value->lh_case_end_date=='0000-00-00' ? '' : $value->lh_case_end_date)
					->setCellValue('W'. $row_num, $value->apply_rst_cd_nm)
					->setCellValue('X'. $row_num, $value->apply_rst_cd_etc)
					->setCellValue('Y'. $row_num, $value->lh_sprt_content)
					->setCellValue('Z'. $row_num, $value->lh_etc);
				$row_num++;
				$cnt--;
			}
			$set_style_range = 'A3:Z'. ($row_num-1);
		}

		// 셀 정렬, 폰트사이즈, 테두리 그리기
		if($set_style_range != '') {
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
				// ,'alignment' => array(
				// 	'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				// )
				,'font' => array(
					'size' => 9
				)
			);
			$sheet->getStyle($set_style_range)->applyFromArray($styleArray);
		}
		
		// 파일 이름에 YmdHms 붙이기
		$YmdHms = get_date('YmdHms');

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Labors_'. $download_filename .'_'. $YmdHms .'.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '. get_date() .' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setPreCalculateFormulas(false); //수식셀 사용안함(속도개선)
		
		ob_end_clean();
		ob_start();
		$objWriter->save('php://output');
		
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
		exit;

	}





	
	//-----------------------------------------------------------------------------------------------------------------
	// _gen_code_to_string : 서브코드를 받아 해당 코드의 이름을 문자열로 만들어 리턴한다.
	//-----------------------------------------------------------------------------------------------------------------
	private function _gen_code_to_string($code) {
		$header_nm = '';
		$cell_width = '';

		$sub_codes = $this->manager->get_sub_code_by_m_code($code);
		foreach($sub_codes as $code) {
			if($header_nm != '') $header_nm .= CFG_AUTH_CODE_DELIMITER_2;
			if($cell_width != '') $cell_width .= CFG_AUTH_CODE_DELIMITER_2;
			$header_nm .= $code->code_name;
			$cell_width .= '20';
		}
		
		return array('header_nm'=>$header_nm, 'cell_width'=>$cell_width);
	}
	
	
	/**
	 * 
	 * 교차통계의 가로컬럼 데이터 조회
	 * 
	 */
	private function get_cmm_code($args, $kind=0) {
		$tmp = [];
		
		//통계유형 구분
		if($kind == 0){
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); //상담방법
		}
		elseif($kind == 1) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER); //성별
		}
		elseif($kind == 2) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_AGES); //연령대
		}
		elseif($kind == 3) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); //거주지
		}
		elseif($kind == 4) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); //회사소재지
		}
		elseif($kind == 5) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND); //직종
		}
		elseif($kind == 6) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND); //업종
		}
		elseif($kind == 7) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND); //고용형태
		}
		elseif($kind == 8) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT); //근로자수
		}
		elseif($kind == 9) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN); //근로계약서
		}
		elseif($kind == 10) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN); //4대보험
		}
		elseif($kind == 11) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND); //상담유형
		}
		elseif($kind == 12) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST); //처리결과
		}
		elseif($kind == 13) {//임금근로시간
			$tmp = [
				'header_nm' => '임금 (빈도)'. CFG_AUTH_CODE_DELIMITER_2 .'근로시간 (빈도)'
				,'cell_width' => '30'. CFG_AUTH_CODE_DELIMITER_2 .'30'
			];
		}
		elseif($kind == 14) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE); //상담동기
		}
		elseif($kind == 15) {
			$tmp = self::_gen_code_to_string(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION); //소속
		}
		
		return $tmp;
	}
	
	

	
	
	function obj_sort(&$objArray, $indexFunction, $sort_flags=0) {
		$indices = array();
		foreach($objArray as $obj) {
			$indeces[] = $indexFunction($obj);
		}
		return array_multisort($indeces, $objArray, $sort_flags);
	}
	
	function sortByTitle($a, $b){
		return strcmp($a->title, $b->title);
	}
}
?>