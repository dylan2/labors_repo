<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| Custom
|--------------------------------------------------------------------------
*/



//-------------------------------------------------------------------------
// # Database 정보
//-------------------------------------------------------------------------
// Database 정보 - config/database.php

$host_name = 'localhost';
$user_id = 'root';
$user_pwd = 'autoset';
$db_name = 'dbsansoul2';
$db_driver = 'mysql';
$session_use_db = false;

// FOR LIVE server
// - 계정변경, 기존 root를 아래 labordbusr 유저로 변경함. dylan 2019.08.12
if($_SERVER["SERVER_ADDR"] == '103.60.126.159') {
	$host_name = 'localhost';
	$user_id = 'labordbusr';
	$user_pwd = 'rnjsdlr12#$db';
}

define('CFG_DB_HOST_NAME', $host_name); // hostname
define('CFG_DB_USER_NAME', $user_id); // 계정
define('CFG_DB_PASSWORD', $user_pwd); // 비번
define('CFG_DB_DATABASE', $db_name); // database name
define('CFG_DB_DBDRIVE', $db_driver); // database db drive type

define('CFG_SESSION_USE_DB', $session_use_db); // 세션 테이블 사용여부  -> config.php

//-------------------------------------------------------------------------




// # 공통 상수 정의
// - 엑셀 파일 다운로드시 파일명 prefix
define('CFG_CMM_PREFIX_OF_EXCEL_FILE', 'Labor_Counsel_Statistic_');

// - 암호화를 위한 key
define('CFG_ENCRYPT_KEY', '$3__labors_key_#7');

// - auth 코드 구분자
define('CFG_AUTH_CODE_ADM_MASTER', 'GRP000'); // 마스터 계정 코드 - 유일하게 존재하며 권한체크를 하지 않고 모든 권한을 부여한다.
define('CFG_AUTH_CODE_ADMIN', 'GRP004'); // 관리자 계정 코드
define('CFG_AUTH_CODE_DELIMITER', ','); // 문자열 구분자
define('CFG_AUTH_CODE_DELIMITER_2', '●'); // 문자열 구분자2


// # 파일업로드
// 파일업로드 : 이미지 업로드 허용 확장자
define('CFG_UPLOAD_ALLOW_IMG_EXT', 'gif|png|jpg|jpeg');
// 파일업로드 : 모든 파일
define('CFG_UPLOAD_ALL_ALLOW_EXT', '*');
// 파일업로드 : 업로드 허용 용량
define('CFG_UPLOAD_MAX_FILE_SIZE', 20495360); // 20M
// 파일업로드 : 파일명 구분 문자
define('CFG_UPLOAD_FILE_NAME_DELIMITER', '|');
// 업로드 최대 개수 설정
define('CFG_UPLOAD_MAX_COUNT', 5); // 개수를 늘리면 DB에 해당 컬럼의 길이이 적절한지 검토가 필요해야한다!!
// 업로드 폴더
define('CFG_UPLOAD_PATH', './upload/');

// 추가 - 2017.01.18 dylan
// - Naver SE2 Editor 경로
define('CFG_BOARD_SE2_EDITOR_PATH', './lib/');

// 추가 - 상담 수정/등록 유예기간일(해가 바뀔때 유예기간일이 지나면 등록/수정 못함), 2017.01.31 dylan
// 수정 - 매년 1월 20일까지만 수정가능하도록 요청에 의해 수정, 최진혁. 2019.02.07 dylan
define('CFG_COUNSEL_EDITABLE_DAY', 20);


// # admin
// - session
define('CFG_SESSION_ADMIN_ID', 'oper_id');
define('CFG_SESSION_ADMIN_NAME', 'oper_name');
define('CFG_SESSION_ADMIN_AUTH_GRP_ID', 'oper_auth_grp_id'); // 권한그룹id
define('CFG_SESSION_ADMIN_AUTH_CODES', 'oper_auth_codes'); // 권한 코드 값
define('CFG_SESSION_ADMIN_KIND_CODE', 'oper_kind'); // 운영자 구분 코드 값 - OK1:기존, OK2:서울시 OK3:옴부즈만 OK4:자치구공무원
define('CFG_SESSION_ADMIN_KIND_CODE_SUB', 'oper_kind_sub'); // 운영자 소속 자치구 코드 값 - 옴부즈만, 자치구공무원
define('CFG_SESSION_ADMIN_AUTH_FIRST_CD', 'oper_auth_first_cd'); // 권한 코드 값중 최초 코드, 이 코드는 로그인 후 권한있는 페이지로 이동시키기 위해 사용
define('CFG_SESSION_ADMIN_AUTH_ASSO_CD', 'asso_cd'); // 소속코드
define('CFG_SESSION_ADMIN_AUTH_ASSO_NM', 'asso_nm'); // 소속코드명
define('CFG_SESSION_ADMIN_AUTH_CSL_VIEW', 'auth_csl_view'); // 상담내역>통계>상담사례의 상담보기 권한 여부, 있으면 1 없으면 0
define('CFG_SESSION_ADMIN_AUTH_BIZ_CSL_VIEW', 'auth_biz_csl_view'); // 사용자상담>통계>상담사례의 상담보기 권한 여부, 있으면 1 없으면 0 - 추가 dylan 2019.08.05
define('CFG_SESSION_ADMIN_SUSPEND_LOGOUT', 'is_suspend_logout'); // 로그인 유지 플래그
define('CFG_SESSION_ADMIN_AUTH_IS_MASTER', 'is_master'); // 마스터 계정(최상위 관리자)인지 여부 값 1, 0
// - 추가 : 고도화
define('CFG_SESSION_ADMIN_AUTH_LAWHELP_EXCEL_DOWNLOAD', 'is_lh_exdown'); // 권리구제-목록>엑셀다운로드 버튼 권한 여부 값  1,0
define('CFG_SESSION_ADMIN_AUTH_LAWHELP_VIEW', 'is_lh_view'); // 권리구제-입력>내용보기(입력/수정 아님) 권한 여부 값  1,0
// 검색 데이터 보관용
define('CFG_SESSION_SEARCH_DATA', 'search_data');

// 운영자 구분 코드
define('CFG_OPERATOR_KIND_CODE_OK1', 'OK1'); // 기본-상담원
define('CFG_OPERATOR_KIND_CODE_OK2', 'OK2'); // 서울시 소속
define('CFG_OPERATOR_KIND_CODE_OK3', 'OK3'); // 옴부즈만
define('CFG_OPERATOR_KIND_CODE_OK4', 'OK4'); // 옴부즈만 소속 자치구 공무원



//-------------------------------------------------------------------------
// # 보안 처리 관련
//-------------------------------------------------------------------------
/**
 * CSRF 위변조, URL파라메터 위변조, xpath 방지 보안처리 용
 *
 * @var unknown
 */
define('CFG_SECURE_FORM_TOKEN', '_toKen_');



// # Board paging
// - 페이지당 아이템 개수 : main page
define('CFG_MAIN_PAGINATION_ITEM_PER_PAGE', 5);
// - 페이지당 아이템 개수 : board list
define('CFG_BOARD_PAGINATION_ITEM_PER_PAGE', 10);
// - 페이지당 아이템 개수 : backoffice
define('CFG_BACK_PAGINATION_ITEM_PER_PAGE', 10);
// - 통계(상담,사업자 공통):상담사례 목록 요청당 가져올 개수
define('CFG_CSL_STTT01_CMM_PAGINATION_ITEM_PER_REQ', 100);


// # 대메뉴 코드
// - 상담내역관리
define('CFG_MASTER_CODE_COUNSEL_MANAGE', 'M001');
// - 상담내역통계
define('CFG_MASTER_CODE_COUNSEL_STATISTIC', 'M002');
// - 게시판
define('CFG_MASTER_CODE_BOARD', 'M003');
// - 운영자관리
define('CFG_MASTER_CODE_OPERATOR', 'M004');
// - 권한관리
define('CFG_MASTER_CODE_AUTH', 'M005');
// - 코드관리
define('CFG_MASTER_CODE_MANAGE_CODE', 'M006');
// - 권리구제지원 관리
define('CFG_MASTER_CODE_LAW_HELP', 'M007');
	// - 추가 권한 코드 - 내용조회only(수정없음), 엑셀다운로드only 권한 상수 : 이 코드는 DB에서 관리하지 않고 별도로 상수로만 관리한다.
	define('CFG_EXCEPTION_CODE_LAW_HELP_CONTENT_VIEW_AUTH', 'L001');
	define('CFG_EXCEPTION_CODE_LAW_HELP_CONTENT_VIEW_AUTH_NM', '권리구제결과 조회');
	define('CFG_EXCEPTION_CODE_LAW_HELP_LIST_EXCEL_DOWNLOAD_AUTH', 'L002');
	define('CFG_EXCEPTION_CODE_LAW_HELP_LIST_EXCEL_DOWNLOAD_AUTH_NM', '권리구제내역 엑셀다운로드');

// - 상담내역통계 - 상담사례>상담내역보기 버튼 권한 상수 : 이 코드는 DB에서 관리하지 않고 별도로 상수로만 관리한다.
define('CFG_EXCEPTION_CODE_COUNSEL_STATISTIC_VIEW_AUTH', 'X001');
define('CFG_EXCEPTION_CODE_NAME_COUNSEL_STATISTIC_VIEW_AUTH', '상담사례>상담내역보기');

// 추가 2019.06.18
// - 사용자상담
define('CFG_MASTER_CODE_BIZ_COUNSEL', 'M008');
// - 사용자상담통계
define('CFG_MASTER_CODE_BIZ_COUNSEL_STATISTIC', 'M009');
// - 사용자상담통계 - 상담사례>상담내역보기 버튼 권한 상수 : 이 코드는 DB에서 관리하지 않고 별도로 상수로만 관리한다.
define('CFG_EXCEPTION_CODE_BIZ_COUNSEL_STATISTIC_VIEW_AUTH', 'X002');
define('CFG_EXCEPTION_CODE_NAME_BIZ_COUNSEL_STATISTIC_VIEW_AUTH', '상담사례>상담내역보기');


// - 코드관리 -> 중코드상수 정의
// - 상담방법
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY', 'S011');
// - 상담이용동기 - 최진혁 요청에 의해 추가 2016.08.02, dylan
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE', 'S035');
// - 성별
define('CFG_SUB_CODE_OF_MANAGE_CODE_GENDER', 'S012');
// - 연령대
define('CFG_SUB_CODE_OF_MANAGE_CODE_AGES', 'S013');
// - 직종
define('CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND', 'S014');
// - 업종
define('CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND', 'S015');
// - 고용형태
define('CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND', 'S016');
// - 근로자수
define('CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT', 'S017');
// - 근로계약서작성
define('CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN', 'S018');
// - 4대보험가입
define('CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN', 'S019');
// - 상담유형
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND', 'S020');
// - 주제어
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD', 'S021');
// - 상담내역공유
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_SHARE_YN', 'S022');
	// - 상담내용 공유여부 : 공유(C08)
	define('CFG_SUB_CODE_CSL_SHARE_YN', 'C078');
// - 소속
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION', 'S023');
// - 상담자 - 상담자는 상위 코드가 없어 임시로 추가 2016.08.09, hskim
define('CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR', 'operator');
// S024는 대코드 M003에서 사용
// - 거주지
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS', 'S025');
// - 소재지 : 거주지와 회사소재지는 같은 코드를 사용하나 통계에서 조회를 따로하기에 따로 상수 정의
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP', 'S026');
// - 사용주체
define('CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND', 'S027');
// - 상담 처리결과
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST', 'S028');
// - 옴부즈만 지원신청
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST_OMBUDSMAN', 'C148');
// - 임금,근로시간
// -- 추가, 2019.07.23 dylan
// -- 임금,근로시간은 코드화 대상이 아니어서 별도 상수로 정의
define('CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PAY_WORKTIME', 'pay_worktime');


// - 권리구제유형 : 추가 2018.07.03
define('CFG_SUB_CODE_OF_MANAGE_CODE_LAWLELP_KIND', 'S036');

// # 추가 : <권리구제지원> 
// - 지원종류
define('CFG_SUB_CODE_OF_LAWHELP_SUPPORT_KIND', 'S031');
// - 신청기관
define('CFG_SUB_CODE_OF_LAWHELP_APPLY_CHANNEL', 'S032');
// - 대상기관
define('CFG_SUB_CODE_OF_LAWHELP_TARGET_ORGANS', 'S033');
// - 지원결과
define('CFG_SUB_CODE_OF_LAWHELP_SUPPORT_RESULT', 'S034');

// # 추가 : <사용자상담> 2019.06.21
// - 운영기간
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD', 'S040');
// - 근로계약서 작성여부
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN', 'S041');
// - 4대보험 가입여부
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN', 'S042');
// - 취업규칙작성여부
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN', 'S043');
// - 상담유형 -- 상담내역과 항목이 다르다.
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND', 'S044');
// - 처리결과 -- 상담내역과 항목이 다르다.
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT', 'S045');
// - 주제어 -- 상담내역과 항목이 다르다.
define('CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD', 'S046');



// #통계
// - 기타통계 엑셀 양식 파일경로
define('CFG_STTC_EXCEL_FORM_FILE_PATH', './form/Labor_Statistic_Etc_Form.xls');









/*
|--------------------------------------------------------------------------
| System
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');



/* End of file constants.php */
/* Location: ./application/config/constants.php */