<?php
/**
 * 
 * 상담내역 교차통계 
 * 
 */
include_once "./inc/inc_header.php";
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="./js/counsel/common.js"></script>
<script type="text/javascript">
var chart_mode = 1;
$(document).ready(function() {

	//차트 보기
	$('#view_chart').click(function(e) {
		e.preventDefault();
		if (chart_mode == 0) { //보기->숨기기
			$('.chart').css('display', 'none');
			$('#view_chart').text('통계차트 보기');
			$('#downchart').css('display', 'none');
			$('#printchart').css('display', 'none');
			chart_mode = 1;
		} 
		else { //숨기기->보기
			if(changed_code == true) {
				alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
				return;
			}
			$('#loading').show();
			$('.chart').css('display', 'block');
			$('#view_chart').text('통계차트 숨기기');
			$('#downchart').css('display', 'inline');
			$('#printchart').css('display', 'inline');

			var position = $('#downchart').offset();
			$('html').animate({ scrollTop: position.top }, 1000, function() {
				// 처음 가져온 데이터를 사용한다. 굳이 다시 가져올 이유도 없고, table데이터와 달라질 수 있기 때문에 다시 가져오는게 잘못된 결과를 보여줄 수 있다. 
				draw_chart(sttt_data);
				chart_mode = 0;
				$('#loading').hide();
			});
		}
	});

	//위로 이동
	$('#view_top').click(function(e) {
		e.preventDefault();
		var position = $('#contents_body').offset();
		$('html, body').animate({ scrollTop: position.top }, 1000);
	});

	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#s_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=s_code_popup_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e) {
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#csl_proc_rst').val(data.s_code);
				}
				if (data.code_name) {
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=csl_proc_rst_popup_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e) {
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#asso_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=asso_code_popup_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e) {
		e.preventDefault();
		$('#asso_code').val($('#asso_code_all').val());
		$('#asso_code_name').text("전체");
	});

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for (var i = year; i >= 2015; i--) {
		$("#sel_year").append("<option value=" + i + ">" + i + "년</option>");
	}
	$('#sel_year').change(function() {
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates('', 2);
		if ($(this).val() == '') {
			$('#btnAllTerm').click();
		}
	});

	$('#sel_month').change(function() {
		fill_list_search_dates($(this).val());
	});

	// "csl_code" select option 최초 생성
	$('#csl_code_options option').map(function(i, o) {
		if (i == 0) return; // 최초 성별 제외
		$('#csl_code').append($(o).clone());
	});

	// 통계유형을 바꾼뒤 검색버튼 클릭하지 않고 차트,표,엑셀다운 버튼 클릭 방지를 위한 플래그
	var changed_code = false;

	// 교차대상 코드
	$('select[name=csl_code]').change(function(){
		changed_code = true;
	});
	
	//상담방법 선택에 따른 교차유형 select option 세팅
	$('#sel_col').change(function() {
		$('#csl_code').empty(); //option 제거
		var sel_idx = $(this).find('option:selected').index();
		$('#csl_code_options option').map(function(i, o) {
			if (i == sel_idx) return; // 선택한 유형 제외
			$('#csl_code').append($(o).clone());
		});
		changed_code = true;
	});

	//통계탭 이동
	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this)+1;
		
		if(index == 2) index = 13;
		else if(index == 3) index = 11;
		else if(index == 6) index = 12;
		
		var begin = $('#search_date_begin').val();
		var end = $('#search_date_end').val();
		var sch_asso_code = $("#asso_code").val();
		var sch_s_code = $("#s_code").val();
		var sch_csl_proc_rst = $("#csl_proc_rst").val();		
		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});

	// print a list
	$('#printlist').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		var txt_sel_col = $('#sel_col option:selected').text();
		var txt_csl_code = $('#csl_code option:selected').text();
		var popupWindow = window.open("", "_blank");
		var $div = $('#list').clone();
		$div.find('table').css('border-collapse', 'collapse').find('th, td').css({ 'border': 'solid 1px #eee', 'height': '30px' });
		$div.find('th').css('background-color', '#e8fcff');
		popupWindow.document.write("<body>");
		popupWindow.document.write("<h3>교차통계 " + txt_sel_col + "-" + txt_csl_code + "</h3>");
		popupWindow.document.write($div.html());
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("table").css("width", "100%");
		$(popupWindow.document).find("td").css("text-align", "center");
		popupWindow.print();
		popupWindow.close();
	});

	// print a chart images
	$('#printchart').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		var popupWindow = window.open("", "_blank");
		var div = $('#chart_div').html();
		popupWindow.document.write("<body>");
		popupWindow.document.write(div);
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("img").css("width", "100%");
		popupWindow.print();
		popupWindow.close();
	});

	// 차트 데이터 전송용 폼 추가
	var addChartForm = function(method) {
		if($('#imgForm').length == 0) {
			$("body").append('<form id="imgForm" method="POST" target="imgFrame" action="/?c=chart_download&m=download">'+
				'<input type="hidden" name="dataUrl" />'+
				'</form>');
		}
		$("#imgFrame").remove();
		$("body").append('<iframe name="imgFrame" id="imgFrame" style="display:none;"></iframe>');
	};

	// 차트 데이터 전송
	var capture = function(id){
		$('#chart_div').show();
		html2canvas(document.getElementById(id), {
			logging: false,
	    width: $("#"+ id +" .chart_div").eq(0).width(),
	    height: $("#"+ id).height(),
			onrendered: function(canvas) {
				var img = canvas.toDataURL();
				$('input[name="dataUrl"]').val(img);
				$("#imgForm").submit();
				$('#chart_div').hide();
				$('#loading').hide();
			}
		});
	};
	// 차트 다은로드
	$('#downchart').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		$('#loading').show();
		addChartForm();
		capture('chart_div');
	});
	
	// download a excel file
	$('#downExcel').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		if ($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var sel_col_index = 0;
		sel_col_index = $('#sel_col option:selected').val();
		var url = '/?c=statistic&m=open_down_view&kind=down11&sel_col=' + sel_col_index;
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
	});

	// 검색
	$("#btnSubmit").click(function(e) {
		e.preventDefault();
		//검색대상이 상담유형이면, 안내 레이블 출력
		if ($('#sel_col option:selected').val() == 11 || $('#csl_code option:selected').text() == '상담유형') $('#infoLabel').css('display', 'inline');
		else $('#infoLabel').css('display', 'none');
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val($('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
		
		get_list(_page);
		changed_code = false;
	});

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e) {
		var date_begin = '',
			date_end = get_today();
		if ($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		} else if ($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		} else if ($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		} else {
			date_begin = '2015-01-01';
		}
		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});

	// list
	get_list(_page, true);
});



function get_today() {
	var now = new Date();
	return now.getFullYear() + '-' + genTwoDigit(now.getMonth() + 1) + '-' + genTwoDigit(now.getDate());
}

//서버로부터 수신된 데이터
var sttt_data = [];

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page, is_first) {
	_page = page;

	var url = '/?c=statistic&m=st11';
	var rsc = $('#frmSearch').serialize() + '&not_seq=0';
	var fn_succes = function(data) {
		sttt_data = data;
		gen_list(sttt_data);
		if (chart_mode == 0) { //차트 보기 상태일 때
			draw_chart(sttt_data);
		}
		var rst = sttt_data.data_tot[0]['tot'];
		if (rst == '0 (0%)' || rst == ' (0%)' || rst == '0(0) (0%)') alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
	};
	var fn_error = function(data) {
		if (is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var sel_col = $('#sel_col').val();
	var is_tg2_csl_kind = $('#csl_code option:selected').text()=='상담유형';

	var html_b = '<table class="tList02" border=0>';
	html_b += '<colgroup>';
	html_b += '<col style="width:*">';
	if(sel_col == 0){<?php foreach($res['code_0'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담방법
	else if(sel_col == 1){<?php foreach($res['code_1'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//성별
	else if(sel_col == 2){<?php foreach($res['code_2'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//연령대
	else if(sel_col == 3){<?php foreach($res['code_3'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//거주지
	else if(sel_col == 4){<?php foreach($res['code_4'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//회사소재지
	else if(sel_col == 5){<?php foreach($res['code_5'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//직종
	else if(sel_col == 6){<?php foreach($res['code_6'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//업종
	else if(sel_col == 7){<?php foreach($res['code_7'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//고용형태
	else if(sel_col == 8){<?php foreach($res['code_8'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//근로자수
	else if(sel_col == 9){<?php foreach($res['code_9'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//근로계약서
	else if(sel_col == 10){<?php foreach($res['code_10'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//4대보험
	else if(sel_col == 11){<?php foreach($res['code_11'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담유형
	else if(sel_col == 12){<?php foreach($res['code_12'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담동기
	html_b += '<col style="width:*">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th style="line-height:15px;">구분</th>';
	if(sel_col == 0){<?php foreach($res['code_0'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (75/count($res['code_0'])) ."%\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 1){<?php foreach($res['code_1'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (65/count($res['code_1'])) ."%\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 2){<?php foreach($res['code_2'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 3){<?php foreach($res['code_3'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 4){<?php foreach($res['code_4'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 5){<?php foreach($res['code_5'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (80/count($res['code_5'])) ."%\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 6){<?php foreach($res['code_6'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (90/count($res['code_6'])) ."%\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 7){<?php foreach($res['code_7'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 8){<?php foreach($res['code_8'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (80/count($res['code_8'])) ."%\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 9){<?php foreach($res['code_9'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 10){<?php foreach($res['code_10'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 11){<?php foreach($res['code_11'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (82/count($res['code_11'])) ."%\">". $item->code_name ."</th>';"; }?>}
	else if(sel_col == 12){<?php foreach($res['code_12'] as $item) { echo "html_b += '<th style=\"line-height:15px;width:". (80/count($res['code_12'])) ."%\">". $item->code_name ."</th>';"; }?>}
	html_b += '<th style="line-height:15px;">총계'+(sel_col==11 ? '(실건수)' : '')+'(비율)</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		// list
		var begin = _page-1;
		var end = total_cnt;
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			html_b += '<tr>';
			html_b += '  <td>'+ data.data[index].subject +'</td>';
			if(sel_col == 0){<?php for($i=0; $i<count($res['code_0']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 1){<?php for($i=0; $i<count($res['code_1']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 2){<?php for($i=0; $i<count($res['code_2']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 3){<?php for($i=0; $i<count($res['code_3']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 4){<?php for($i=0; $i<count($res['code_4']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 5){<?php for($i=0; $i<count($res['code_5']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 6){<?php for($i=0; $i<count($res['code_6']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 7){<?php for($i=0; $i<count($res['code_7']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 8){<?php for($i=0; $i<count($res['code_8']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 9){<?php for($i=0; $i<count($res['code_9']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 10){<?php for($i=0; $i<count($res['code_10']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 11){<?php for($i=0; $i<count($res['code_11']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sel_col == 12){<?php for($i=0; $i<count($res['code_12']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			if(data.data[index].tot != 0){
				html_b += '  <td>'+ applyComma(data.data[index].tot) +'</td>';
			}
			else{
				html_b += '  <td>0(0.0%)</td>';
			}
			html_b += '</tr>';
			index++;
		}
		// 구분별 total
		html_b += '<tr><td style="background:#F1F1F1 !important;">총계'+ ( is_tg2_csl_kind ? '(실건수)' : '') +'(비율)</td>';
		for(var item in data.data_tot[0]) {
			html_b += '  <td style="background:#F1F1F1 !important;">'+ applyComma(data.data_tot[0][item]) +'</td>';
		}
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="30" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// list html
	$('#list').empty().html(html_b);
	$('#list td').css({whiteSpace:'nowrap'});

	// 세로 합계 퍼센트 추가
	var tot = +$('table.tList02 tr:last td:last').text().split('(')[0].trim().replace(',','');
	$('table.tList02 tr:last td').not(':first,:last').each(function(i,o){// 처음,마지막 제외
		var val = 1;
		if(is_tg2_csl_kind) {
			val = +$(o).text().split('(')[0].replace(',','').trim();
		}
		else {
			val = +$(o).text().replace(',','').trim();
		}
		$(o).text( $(o).text() +' ('+ (val/tot*100).toFixed(1) +'%)' );
	});	
}


/*막대그래프 만들기 시작*/
google.charts.load('current', {'packages':['corechart', 'corechart']});

function draw_chart(data) {
	  var txt_sel_col = $('#sel_col option:selected').text();
	  var txt_csl_code = $('#csl_code option:selected').text();
    var chart_html = '',
    	len = Object.keys(data.data_tot[0]).length;
    // 서브 차트(파이차트) 개수가 많은 통계는 2개씩 구성한다.
    if(txt_sel_col == '거주지' || txt_sel_col == '회사소재지') {
  	  chart_html = '<table style="width:805px;">';
  	  chart_html += '<tr><td colspan="2"><div id="chart_div0" class="chart_div"></div></td></tr>';//첫번째 column chart
  	  var idx = 1; 
  	  len = Math.ceil(len/2);
  	  for(var i=1; i<len; i++) {
  	  	chart_html += '<tr><td style="width:50%;"><div id="chart_div'+ idx++ +'" class="chart_div"></div></td>'+
  	  		'<td style="width:50%;"><div id="chart_div'+ idx++ +'" class="chart_div"></div></td></tr>';
  	  }
  	  chart_html += '</table>';
    }
    else {
	    for(var i=0; i<len; i++) {
	    	chart_html += '<div id="chart_div'+i+'" class="chart_div"></div>';
	    }
    }
    $('#chart_div').html(chart_html);
	  
    var index = 0;
    var sel_col = $('#sel_col').val();
    var subject = new Array();
    var value = new Array();
    var array_data = new Array();
    if(sel_col == 0){<?php foreach($res['code_0'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 1){<?php foreach($res['code_1'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 2){<?php foreach($res['code_2'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 3){<?php foreach($res['code_3'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 4){<?php foreach($res['code_4'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 5){<?php foreach($res['code_5'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 6){<?php foreach($res['code_6'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 7){<?php foreach($res['code_7'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 8){<?php foreach($res['code_8'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 9){<?php foreach($res['code_9'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 10){<?php foreach($res['code_10'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 11){<?php foreach($res['code_11'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
    else if(sel_col == 12){<?php foreach($res['code_12'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}

  	array_data[0] = ['구분', '상담건수', { role: 'annotation' }, { role: 'style' }];
  	var index = 1;
  	for (var i in data.data_tot[0]) {
  		if (i == 'tot') break;
  		if (txt_csl_code == '상담유형') {
  			var str_value = data.data_tot[0][i].split('(');
  			value = Number(str_value[0]);
  		} else {
  			value = Number(data.data_tot[0][i]);
  		}
  		array_data[index] = [subject[index - 1], value, value, '#3366CC'];

  		img_chart(subject[index - 1], index, data);
  		index++;
  	}
  	var chart_data = new google.visualization.arrayToDataTable(array_data);

  	var options = {
  		title: '[' + txt_sel_col + ']-[' + txt_csl_code + '] 전체 Chart',
  		width: 1620,
  		height: 700,
  		chartArea: {
  			left: 100,
  			right: 100,
  			top: 100,
  			bottom: 200,
  			width: '100%',
  			height: '400',
  		},
  		bar: {
  			groupWidth: "30%",
  		},
  		fontSize: 12,
  		axisTitlesPosition: 'in',
  		legend: { position: 'none' },
  	};

  	var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'));
  	var chart_div = document.getElementById('chart_div0');

  	google.visualization.events.addListener(chart, 'ready', function() {
  		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
  	});

  	chart.draw(chart_data, options);

  	donut_chart(1, array_data[1][0], data);

  	/*막대그래프, 셀렉트 이벤트핸들러 시작*/
  	google.visualization.events.addListener(chart, 'select', selectHandler);

  	function selectHandler() {
  		var selection = chart.getSelection();
  		var sel_row = '';
  		for (var i = 0; i < selection.length; i++) {
  			var item = selection[i];
  			sel_row = item.row + 1;
  		}
  		if (sel_row != '') {
  			var sel_kind = array_data[sel_row][0];
  			donut_chart(sel_row, sel_kind, data);
  		}
  	}
  	/*막대그래프, 셀렉트 이벤트핸들러 끝*/
  }
  /*막대그래프 만들기 끝*/

  /*도넛그래프 만드기 시작*/
  function donut_chart(sel_row, sel_kind, data) {
  	var txt_sel_col = $('#sel_col option:selected').text();
  	var txt_csl_code = $('#csl_code option:selected').text();
  	var str_row = 'ck' + String(sel_row);
  	var oper, asso = '';
  	var value = 0;
  	var str_value = '';
  	var index = 1;
  	var array_data = new Array();

  	array_data[0] = ['상담자', '상담건수'];

  	for (var i = 0; i < data.tot_cnt; i++) {
  		asso = data.data[i].subject;
  		str_value = data.data[i][str_row].split('(');
  		value = Number(str_value[0]);
  		array_data[index] = [asso, value];
  		index++;
  	}

  	var chart_data = google.visualization.arrayToDataTable(array_data);

  	var options = {
  		title: '[' + sel_kind + '] - [' + txt_csl_code + '] 상세 Chart',
  		width: 1620,
  		height: 700,
  		chartArea: {
  			left: 100,
  			right: 100,
  			top: 100,
  			bottom: 100,
  			width: '100%',
  			height: '400',
  		},
  		legend: {
  			position: 'right',
  			maxLines: 3,
  		},
  		tooltip: {
  			ignoreBounds: true,
  		},
  		fontSize: 12,
  		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
  		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
  	};

  	var chart = new google.visualization.PieChart(document.getElementById('donut_chart'));
  	chart.draw(chart_data, options);
  }


  function img_chart(sel_kind, sel_row, data) {
  	var txt_sel_col = $('#sel_col option:selected').text();
  	var txt_csl_code = $('#csl_code option:selected').text();
  	var str_row = 'ck' + String(sel_row);
  	var oper, asso = '';
  	var value = 0;
  	var str_value = '';
  	var index = 1;
  	var array_data = new Array();

  	array_data[0] = ['상담자', '상담건수'];
  	for (var i = 0; i < data.tot_cnt; i++) {
  		asso = data.data[i].subject;
  		str_value = data.data[i][str_row].split('(');
  		value = Number(str_value[0]);
  		array_data[index] = [asso, value];
  		index++;
  	}
  	var chart_data = google.visualization.arrayToDataTable(array_data);
  	var options = {
  		title: '[' + txt_sel_col + ']-[' + txt_csl_code + '] ' + sel_kind + ' 상세 Chart',
  		width: 1620,
  		height: 500,
  		chartArea: {
  			left: 50,
  			right: 50,
  			top: 50,
  			bottom: 100,
  			width: '90%',
  			height: '400',
  		},
  		is3D: true,
  		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
  		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
  	};

  	var chart_div = document.getElementById('chart_div' + sel_row);
  	var chart = new google.visualization.PieChart(chart_div);

  	google.visualization.events.addListener(chart, 'ready', function() {
  		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
  	});

  	chart.draw(chart_data, options);

  }
  /*도넛그래프 만드기 시작*/
  </script>
  <?php
  include_once './inc/inc_menu.php';
  ?>
  <!--- //lnb 메뉴 area ---->
  <div id="contents_body">
  	<div id="cont_head">
  		<h2 class="h2_tit">상담내역 통계</h2>
  		<span class="location">
  			홈 > 상담내역 통계 > <em>통계</em>
  		</span>
  		<input type="hidden" id="begin" value="<?php echo $date['begin']?>">
  		<input type="hidden" id="end" value="<?php echo $date['end']?>">
  		<input type="hidden" id="sch_asso_code" value="<?php echo $date['sch_asso_code']?>">
  		<input type="hidden" id="sch_s_code" value="<?php echo $date['sch_s_code']?>">
  		<input type="hidden" id="sch_csl_proc_rst" value="<?php echo $date['sch_csl_proc_rst']?>">
  	</div>
  	<?php
  include_once './inc/inc_statistic_info.php';
  ?>
  	<ul class="tab">
  		<li><a href="#" class="go_page">상담사례</a></li>
				<li><a href="#" class="go_page">기본통계</a></li>
  		<li><a href="#" class="go_page selected" style="text-decoration: none">교차통계</a></li>
				<li><a href="#" class="go_page">임금근로시간통계</a></li>
				<li><a href="#" class="go_page">소속별통계</a></li>
				<li><a href="#" class="go_page">월별통계</a></li>
				<li><a href="#" class="go_page">시계열통계</a></li>
  	</ul>
  	<div class="cont_area" style="margin-top: 0">
  		<form name="frmSearch" id="frmSearch" method="post">
  			<input type="hidden" id="sch_kind" name="sch_kind" value="0">
  			<div class="con_text">
  				<label for="search_date_begin" class="l_title">상담일</label>
  				<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" readonly="readonly" style="width: 90px; margin-left: -20px">
  				~
  				<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" readonly="readonly" style="width: 90px">
  				<label for="sel_year" class="l_title" style="margin-left: 10px;">연월별</label>
  				<select id="sel_year" name="sel_year" style="margin-left: -20px;">
  					<option value="">연도</option>
  				</select>
  				<select id="sel_month" name="sel_month">
  					<option value="">전체</option>
  					<option value="01">1월</option>
  					<option value="02">2월</option>
  					<option value="03">3월</option>
  					<option value="04">4월</option>
  					<option value="05">5월</option>
  					<option value="06">6월</option>
  					<option value="07">7월</option>
  					<option value="08">8월</option>
  					<option value="09">9월</option>
  					<option value="10">10월</option>
  					<option value="11">11월</option>
  					<option value="12">12월</option>
  				</select>
  				<label for="sel_year" class="l_title" style="margin-left: 10px;">기간별</label>
  				<button type="button" id="btnToday" class="buttonS bGray" style="margin-left: -20px;">오늘</button>
  				<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
  				<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
  				<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button>
  				<br>
  				<span class="divice"></span>
  				<div>
  					<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속</button>
  					<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
  					<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
  					<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input>
  					<!-- 디버깅: 초기화버튼 클릭시 세팅용 초기데이터 -->
  					<input type="hidden" name="asso_code_name"></input>
  					<span id="asso_code_name" style="margin-left: 27px;"></span>
  				</div>
  				<span class="divice"></span>
  				<div>
  					<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
  					<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
  					<input type="hidden" name="s_code" id="s_code"></input>
  					<input type="hidden" name="s_code_name"></input>
  					<span id="s_code_name" style="margin-left: 5px;"></span>
  				</div>
  				<span class="divice"></span>
  				<div>
  					<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
  					<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
  					<input type="hidden" name="csl_proc_rst" id="csl_proc_rst"></input>
  					<input type="hidden" name="csl_proc_rst_name"></input>
  					<span id="csl_proc_rst_name" style="margin-left: 5px;"></span>
  				</div>
  				<span class="divice"></span>
  				<div class="stab_button">
  					<label for="sel_col" class="stab_title">통계 유형</label>
  					<select id="sel_col" name="sel_col">
  						<option value=1>성별</option>
  						<option value=2>연령대</option>
  						<option value=3>거주지</option>
  						<option value=4>회사소재지</option>
  						<option value=5>직종</option>
  						<option value=6>업종</option>
  						<option value=7>고용형태</option>
  						<option value=8>근로자수</option>
  						<option value=9>근로계약서</option>
  						<option value=10>4대보험</option>
  						<option value=11>상담유형</option>
  						<option value=12>상담동기</option>
  						<option value=0>상담방법</option>
  						<!-- 상담방법 순서 변경 요청에 의해 가장 아래로.. -->
  					</select>
  					<select id="csl_code" name="csl_code">
  					</select>
  					<!-- csl_code select의 옵션 제어용 -->
  					<select id="csl_code_options" style="display: none;">
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);?>' data-idx="1">성별</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);?>' data-idx="2">연령대</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);?>' data-idx="3">거주지</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP);?>' data-idx="4">회사소재지</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);?>' data-idx="5">직종</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);?>' data-idx="6">업종</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);?>' data-idx="7">고용형태</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);?>' data-idx="8">근로자수</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);?>' data-idx="9">근로계약서</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);?>' data-idx="10">4대보험</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);?>' data-idx="11">상담유형</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE);?>' data-idx="12">상담동기</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);?>' data-idx="13">상담방법</option>
  					</select>
  				</div>
  				<div id="infoLabel" class="floatL marginT10" style="color: #f37000; display: none">
  					* 상담유형은 중복선택이 가능하여 데이터가 중복될 수 있습니다. 총계표기 : 중복데이터(실데이터)
  				</div>
  				<div class="textR marginT10">
  					<button type="button" id="btnSubmit" class="buttonS bBlack">
  						<span class="icon_search"></span>
  						검색
  					</button>
  				</div>
  			</div>
  		</form>
  		
  		<!-- //컨텐츠 -->
  		<div class="list_select" style="margin-bottom:10px;">
  			<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
  			<button type="button" id="printlist" class="buttonM bGray floatR">표 인쇄</button>
  			<button type="button" id="view_chart" class="buttonM bGray floatR">통계차트 보기</button>
  		</div>
  		
  		<!-- list -->
  		<div style="overflow: auto;">
  			<button type="button" id="downchart" class="buttonS bBlack" style="display: none">차트다운로드</button>
  			<button type="button" id="printchart" class="buttonS bBlack" style="display: none">차트인쇄</button>
  			<div id="column_chart" class="chart" style="width: 100%; height: 700px; display: none"></div>
  			<div id="donut_chart" class="chart" style="width: 100%; height: 700px; display: none"></div>
  			<div id="list" style="width: 100%;"></div>
  		</div>
  		
  		<div id="chart_div" style="display: none"></div>
  		<a href="#" id="go_to_top" title="Go to top"></a>
  	</div>
  	<!-- //컨텐츠 -->
  </div>
  <!-- //  contents_body  area -->
  <?php
  include_once './inc/inc_footer.php';
  ?>
