<script>
$(document).ready(function(e){
	// 닫기
	$('button.cssClosePopup').click(function(e){
		closeLayerPopup();
	});

	// 레이어팝업의 y scroll 제거
	$('div.lyr_modal').css('overflow-y', 'hidden');
});
</script>


    <div id="popup">
        <header id="pop_header">
			<h2 class="">상담내역 보기</h2>			
        </header>
		<div id="popup_contents">
			<div class="floatC marginT05 marginB20">
				<div class="floatL">상담일 : <?php echo $data->csl_date;?> <span class="fBold marginL10 fBlue"></span> <!--상담내역--></div>
				<div class="floatR">상담자 : <?php echo $data->oper_name;?></div>
			</div>
			<table class="tList02">
				<caption>상담내용 입니다.</caption>
				<colgroup>
					<col style="width:100%">
				</colgroup>
				<tr>
					<th class="textL">상담내용</th>
				</tr>
				<tr>
					<td>
						<div style="overflow-y:scroll;">
							<div style="width:100%;height:190px;text-align:left;" class="input_font_size14"><?php echo $data->csl_content;?></div>
						</div>
					</td>
				</tr>
				<tr>
					<th class="textL">답변</th>
				</tr>
				<tr>
					<td>
						<div style="overflow-y:scroll;">
							<div style="width:100%;height:190px;text-align:left;" class="input_font_size14"><?php echo $data->csl_reply;?></div>
						</div>
					</td>
				</tr>
			</table>			
		</div>
		<div class="btn_set" style="padding-top:0px;margin-top:0px;">
			<button type="button" class="buttonM bGray cssClosePopup">닫기</button>
		</div>
    </div>