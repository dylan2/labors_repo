<?php
/**
 * 
 * 상담내역 - 상담사례 통계
 * 
 */
include_once "./inc/inc_header.php";
?>

<script type="text/javascript" src="./js/counsel/common.js"></script>
<script type="text/javascript">

// 검색어 쿠키처리 스크립트에서사용하기 위한 변수. 상담사례 통계는 일자를 당월로 세팅해야 하는 예외처리용
var page_id = 1;

$(document).ready(function(){
	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#s_code').val(data.s_code);
				}
				if(data.code_name){
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=s_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e){
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#csl_proc_rst').val(data.s_code);
				}
				if(data.code_name){
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=csl_proc_rst_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e){
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
				$('#asso_code').val(data.s_code);
				}
				if(data.code_name){
				$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=asso_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e){
		e.preventDefault();
		$('#asso_code').val("");
		$('#asso_code_name').text("전체");
	});

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for(var i=year; i>=2015; i--){
		$("#sel_year").append("<option value="+i+">"+i+"년</option>");
	}

	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates('', 1);
	});

	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});


	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this)+1;
		
		if(index == 2) index = 13;
		else if(index == 3) index = 11;
		else if(index == 6) index = 12;
		
		var begin = $('#search_date_begin').val();
		var end = $('#search_date_end').val();
		var sch_asso_code = $("#asso_code").val();
		var sch_s_code = $("#s_code").val();
		var sch_csl_proc_rst = $("#csl_proc_rst").val();		
		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});

	// download a excel file
	$('#downExcel').click(function(){
		if($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var url = '/?c=statistic&m=open_down_view&kind=down01&csl_kind=1&not_seq=1';
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
	});

	// 검색
  $("#btnSubmit").click(function(e) {
		e.preventDefault();
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val($('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
		_page = 1;
		isLast = false;
		
		get_list(_page, true);
  });

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end = get_today();
		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_begin = '2015-01-01';
		}
		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});

	// list
	get_list(_page, true);
});


function get_today() {
	var now = new Date();
	return now.getFullYear() +'-'+ genTwoDigit(now.getMonth()+1) +'-'+ genTwoDigit(now.getDate());
}


/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page, isFirst) {
	_page = page;
	var url = '/?c=statistic&m=st01';
	var rsc = $('#frmSearch').serialize() +'&not_seq=0&page='+ _page;
	var fn_succes = function(data) {
		gen_list(data, isFirst);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);

}

// edit
function edit(seq) {
	// 권한체크: 보기 권한 컨트롤러에서 체크
	var kind = 'edit';
	check_auth_redirect($('#lnb01_menu01').find('li>a').first(), kind, seq);
}

// 마지막 요청했는지 여부 플래그
var isLast = false;

// [권한] 상담보기 버튼 권한 소유여부, 소유:y
var csl_view_auth = "<?php echo $data['csl_view_auth'];?>";

/**
 * list html 생성
 */
function gen_list(data, isFirst) { 	
 	var html_b = '';	
 	var total_cnt = 0;
 	var is_admin = "<?php echo $data['is_admin'];?>";
 	
 	if(data && typeof data.data == 'object' && data.data.length > 0) {
 		total_cnt = data.tot_cnt;
 		$('#total_cnt').text('전체: '+ numberToCurrency(total_cnt) +'건');
 		var begin = _page-1;
 		var end = data.data.length;
 		var no = total_cnt - (begin * "<?php echo CFG_CSL_STTT01_CMM_PAGINATION_ITEM_PER_REQ;?>");
 		var index = 0;
 		var csl_name = '', d = '';
 		for(var i=0; i<end; i++) {
 			d = data.data[index];
 			html_b += '<tr>';
 			html_b += '  <td>'+ numberToCurrency((no--)) +'</td>';
 			html_b += '  <td>'+ d.csl_date +'</td>';
 			html_b += '  <td>'+ d.asso_name +'</td>';
 			html_b += '  <td>'+ d.oper_name +'</td>';
 			html_b += '  <td>'+ d.csl_name +'</td>';
 			html_b += '  <td>'+ d.csl_type +'</td>';
 			html_b += '  <td>'+ d.ages +'</td>';
 			html_b += '  <td>'+ d.gender +'</td>';
 			html_b += '  <td>'+ d.live_addr +'</td>';
 			html_b += '  <td>'+ d.work_kind +'</td>';
 			html_b += '  <td>'+ d.comp_kind +'</td>';
 			html_b += '  <td>'+ d.comp_addr +'</td>';
 			html_b += '  <td>'+ d.emp_kind +'</td>';
 			html_b += '  <td>'+ d.emp_cnt +'</td>';
 			html_b += '  <td>'+ d.emp_paper_yn +'</td>';
 			html_b += '  <td>'+ d.emp_insured_yn +'</td>';
 			html_b += '  <td>'+ d.ave_pay_month +'</td>';
 			html_b += '  <td>'+ d.work_time_week +'</td>';
 			html_b += '  <td>'+ d.csl_kind +'</td>';
 			html_b += '  <td>'+ d.csl_proc_rst +'</td>';
 			html_b += '  <td>';
 			html_b += '    <button type="button" class="buttonS bGray" '+
 				(is_admin=='n' ?
 					(d.asso_code!='<?php echo $oper_asso_cd;?>' || '<?php echo $data['csl_view_auth'];?>'=='n' ? 'disabled="disabled"' : '')
 		 			:'') +
 				'onClick="javascript:edit(\''+ d.seq +'\');">보기</button>';
 			html_b += '  </td>';
 			html_b += '</tr>';
 			//
 			index++;
 		}
 		isLast = no <= 1; 
 	}
 	else if(isFirst) {
 		html_b += '<tr><td colspan="21" style="align:center">내용이 없습니다.</td></tr>';
 	}

 	// list html
 	if(html_b != '') {
 	 	if(isFirst) {
 	 		$('#list tr').not('.th').remove();
 	 	}
 		$('#list').append(html_b);
 	}
}

$(document).ready(function(){
	$(window).scroll(function() {
		if(isLast) {// 마지막요청한 경우 더이상 요청하지 않도록한다.
			return;
		}
	  if($('#list tr').not('.th').length > 0 && Math.ceil($(window).scrollTop()+$(window).height()) >= $(document).height()){
	  	get_list(++_page);
	  }
	});
});
</script>

<?php
include_once './inc/inc_menu.php';
?>


		<!-- //lnb 메뉴 area -->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">상담내역 통계</h2>
				<span class="location">홈 > 상담내역 통계 > <em>통계</em></span>
			</div>
<?php
include_once './inc/inc_statistic_info.php';
?>
			<ul class="tab">
				<li><a href="#" class="go_page selected" style="text-decoration:none">상담사례</a></li>
				<li><a href="#" class="go_page">기본통계</a></li>
				<li><a href="#" class="go_page">교차통계</a></li>
				<li><a href="#" class="go_page">임금근로시간통계</a></li>
				<li><a href="#" class="go_page">소속별통계</a></li>
				<li><a href="#" class="go_page">월별통계</a></li>
				<li><a href="#" class="go_page">시계열통계</a></li>
			</ul>
			<div class="cont_area" style="margin-top:0">
				<form name="frmSearch" id="frmSearch" method="post">
				<div class="con_text">					
					<label for="search_date_begin" class="l_title">상담일</label>
					<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" readonly="readonly" style="width:90px; margin-left: -20px"> ~ 
					<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" readonly="readonly" style="width:90px">
					<label for="sel_year" class="l_title" style="margin-left:10px;">연월별</label>
					<select id="sel_year" name="sel_year" style="margin-left:-20px;">
						<option value="">연도</option>
					</select>
					<select id="sel_month" name="sel_month">
						<option value="">전체</option>
						<option value="01">1월</option>
						<option value="02">2월</option>
						<option value="03">3월</option>
						<option value="04">4월</option>
						<option value="05">5월</option>
						<option value="06">6월</option>
						<option value="07">7월</option>
						<option value="08">8월</option>
						<option value="09">9월</option>
						<option value="10">10월</option>
						<option value="11">11월</option>
						<option value="12">12월</option>
					</select>
					<label for="sel_year" class="l_title" style="margin-left:10px;">기간별</label>
					<button type="button" id="btnToday" class="buttonS bGray"  style="margin-left:-20px;">오늘</button>
					<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
					<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
					<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button><br>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속 </button>
						<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
						<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input>
						<input type="hidden" name="asso_code_name"></input>
						<span id="asso_code_name" style="margin-left:27px;"></span>
					</div>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
						<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="s_code" id="s_code" value=""></input>
						<input type="hidden" name="s_code_name"></input>
						<span id="s_code_name" style="margin-left:5px;"></span>
					</div>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
						<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="csl_proc_rst" id="csl_proc_rst" value=""></input>
						<input type="hidden" name="csl_proc_rst_name"></input>
						<span id="csl_proc_rst_name" style="margin-left:5px;"></span>
					</div>
					<div class="textR marginT10">
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>
			<!-- //컨텐츠 -->

				<div class="list_select" style="margin-bottom:10px;">
					<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
				</div>
				
				<div id="total_cnt" style="margin:-28px 0 10px 0;">전체: 0건</div>
					
				<!-- list -->
				<div style="overflow:auto;">
					<table class="tList02" id="list">
						<caption>통계 목록입니다.</caption>
						<colgroup>
							<col style="width:*">
							<col style="width:7%">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
							<col style="width:*">
						</colgroup>
						<tr class="th">
							<th style="line-height:15px;">No</th>
							<th style="line-height:15px;">상담일</th>
							<th style="line-height:15px;">기관</th>
							<th style="line-height:15px;">상담자</th>
							<th style="line-height:15px;">이름</th>
							<th style="line-height:15px;">상담방법</th>
							<th style="line-height:15px;">연령대</th>
							<th style="line-height:15px;">성별</th>
							<th style="line-height:15px;">거주지</th>
							<th style="line-height:15px;">직종</th>
							<th style="line-height:15px;">업종</th>
							<th style="line-height:15px;">회사<br>소재지</th>
							<th style="line-height:15px;">고용형태</th>
							<th style="line-height:15px;">근로자수</th>
							<th style="line-height:15px;">근로<br>계약서</th>
							<th style="line-height:15px;">4대보험</th>
							<th style="line-height:15px;">월평균<br>임금</th>
							<th style="line-height:15px;">주당<br>근로<br>시간</th>
							<th style="line-height:15px;">상담유형</th>
							<th style="line-height:15px;">처리결과</th>
							<th style="line-height:15px;">상담내역</th>
						</tr>
					</table>
				</div>
				
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->		
		
		
<?php
include_once './inc/inc_footer.php';
?>
