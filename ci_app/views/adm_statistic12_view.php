<?php
/**
 * 
 * 
 * 상담내역 - 월별 통계
 * 
 * 
 * 
 */
include_once "./inc/inc_header.php";
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="./js/counsel/common.js"></script>
<script type="text/javascript">
var chart_mode = 1;
$(document).ready(function() {

	//차트 보기
	$('#view_chart').click(function(e) {
		e.preventDefault();
		if (chart_mode == 0) { //숨기기
			$('.chart').css('display', 'none');
			$('#printchart').css('display', 'none');
			$('#downchart').css('display', 'none');
			$('#sel_chart').css('display', 'none');
			$('#view_chart').text('통계차트 보기');

			chart_mode = 1;
		}
		else { //보기
			$('#view_chart').text('통계차트 숨기기');
			$('#downchart').css('display', 'inline');
			$('#printchart').css('display', 'inline');
			$('#sel_chart').css('display', 'inline');
			//
			$('#sel_chart').trigger('change');
			chart_mode = 0;
		}
	});

	$('#sel_chart').change(function() {
		var sel_chart_index = $('#sel_chart option:selected').val();

		// 구글차트를 위한 데이터를 가져옴
		var url = '/?c=statistic&m=st12';
		var rsc = $('#frmSearch').serialize();
		var fn_succes = function(data) {
			$('html').animate({ scrollTop: $('#downchart').offset().top }, 1000, function(){
				if (sel_chart_index == 0) {
					column_chart(data);
				}
				else if (sel_chart_index == 1) {
					column_chart2(data);
				}
				else if (sel_chart_index == 2) {
					bar_chart(data);
				}
				else if (sel_chart_index == 3) {
					donut_chart(data);
				}
				else if (sel_chart_index == 4) {
					pie_chart(data);
				}
				else if (sel_chart_index == 5) {
					line_chart(data);
				}
			});
			var rst = data.data_tot[0]['tot'];
			if (data.data_tot[0]['tot'] == '0' || data.tot_cnt == 0) alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
		};
		var fn_error = function(data) {
			if (is_local) objectPrint(data);
		};
		req_ajax(url, rsc, fn_succes, fn_error);

		$('.chart').css('display', 'none');
		if (sel_chart_index == 0) {
			$('#column_chart').css('display', 'block');
		}
		else if (sel_chart_index == 1) {
			$('#column_chart2').css('display', 'block');
		}
		else if (sel_chart_index == 2) {
			$('#bar_chart').css('display', 'block');
		}
		else if (sel_chart_index == 3) {
			$('#donut_chart').css('display', 'block');
		}
		else if (sel_chart_index == 4) {
			$('#pie_chart').css('display', 'block');
		}
		else if (sel_chart_index == 5) {
			$('#line_chart').css('display', 'block');
		}
	});

	//위로 이동
	$('#view_top').click(function(e) {
		e.preventDefault();
		var position = $('#contents_body').offset();
		$('html, body').animate({	scrollTop: position.top}, 1000);
	});

	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#s_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=s_code_popup_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e) {
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#csl_proc_rst').val(data.s_code);
				}
				if (data.code_name) {
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=csl_proc_rst_popup_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e) {
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#asso_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=asso_code_popup_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e) {
		e.preventDefault();
		$('#asso_code').val($('#asso_code_all').val());
		$('#asso_code_name').text("전체");
	});


	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for (var i = year; i >= 2015; i--) {
		$("#sel_year").append("<option value=" + i + ">" + i + "년</option>");
	}

	//통계탭 이동
	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this) + 1;

		if (index == 2) index = 13;
		else if (index == 3) index = 11;
		else if (index == 6) index = 12;

		var begin = $('#search_date_begin').val();
		var end = $('#search_date_end').val();
		var sch_asso_code = $("#asso_code").val();
		var sch_s_code = $("#s_code").val();
		var sch_csl_proc_rst = $("#csl_proc_rst").val();
		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});

	// print a list
	$('#printlist').click(function() {
		var year = $('#sel_year').val();
		var popupWindow = window.open("", "_blank");
		var $div = $('#list').clone();
		$div.find('table').css('border-collapse', 'collapse').find('th, td').css({
			'border': 'solid 1px #eee',
			'height': '30px'
		});
		$div.find('th').css('background-color', '#e8fcff');
		popupWindow.document.write("<body>");
		popupWindow.document.write("<h3>월별통계 " + year + "</h3>");
		popupWindow.document.write($div.html());
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("table").css("width", "100%");
		$(popupWindow.document).find("td").css("text-align", "center");
		popupWindow.print();
		popupWindow.close();
	});

	// print a chart images
	$('#printchart').click(function() {
		var sel_chart_index = $('#sel_chart option:selected').val();
		var popupWindow = window.open("", "_blank");
		var div = $('#chart_div' + sel_chart_index).html();
		popupWindow.document.write("<body>");
		popupWindow.document.write(div);
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("img").css("width", "100%");
		popupWindow.print();
		popupWindow.close();
	});

	// download a chart images
	$('#downchart').click(function() {
		$('#chart_div').css('display', 'block');
		var sel_chart_index = $('#sel_chart option:selected').val();
		var chart_div_id = "#chart_div" + sel_chart_index;
		html2canvas($(chart_div_id), {
			// html2canvas는 보이는 부분만 캡쳐하기 때문에 chart_div내 첫번째 차트 이미지의 가로 크기가 커지는 경우 
			// 캡쳐안되는 문제가 있어 첫번째 차트 이미지의 크기를 캡쳐영역으로 지정하도록 수정함
			width: $(chart_div_id).find('img').width(),
			height: $(chart_div_id).height(),
			onrendered: function(canvas) {
				var img = canvas.toDataURL();
				$("body").append('<form id="imgForm" method="POST" target="_blank" action="/?c=chart_download&m=download">' +
					'<input type="hidden" name="dataUrl" value="' + img + '">' +
					'</form>');
				$("#imgForm").submit();
				$("#imgForm").remove();
			}
		});
		$('#chart_div').css('display', 'none');
	});

	// download a excel file
	$('#downExcel').click(function() {
		if ($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var url = '/?c=statistic&m=open_down_view&kind=down12';
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
	});

	// 검색
	$("#btnSubmit").click(function(e) {
		e.preventDefault();
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val($('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
		get_list(_page);
	});


	// list
	get_list(_page, true);
});


/**
 * 서버로 리스트 데이터 요청
 */
function get_list(page, is_first) {

	// page
	_page = page;

	// request
	var url = '/?c=statistic&m=st12';
	var rsc = $('#frmSearch').serialize() + '&not_seq=0';
	var fn_succes = function(data) {
		gen_list(data);

		if (chart_mode == 0) { //차트 보기 상태일 때
			var sel_chart_index = $('#sel_chart option:selected').val();
			if (sel_chart_index == 0) {
				column_chart(data);
			}
			else if (sel_chart_index == 1) {
				column_chart2(data);
			}
			else if (sel_chart_index == 2) {
				bar_chart(data);
			}
			else if (sel_chart_index == 3) {
				donut_chart(data);
			}
			else if (sel_chart_index == 4) {
				pie_chart(data);
			}
			else if (sel_chart_index == 5) {
				line_chart(data);
			}
		}

		if (data.data_tot[0]['tot'] == '0' || data.tot_cnt == 0) alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
	};
	var fn_error = function(data) {
		if (is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * 통계 테이블 생성
 */
function gen_list(data) {
	var half_year = $('#half_year').val();

	var html_b = '<table class="tList02" border="0">';
	html_b += '<colgroup>';
	html_b += '<col style="width:15%;">';
	if (half_year == 1) {
		for (var h = 1; h <= 12; h++) {
			html_b += '<col style=\"width:6.5%;\">';
		}
	}
	else {
		for (var h = 1; h <= 6; h++) {
			html_b += '<col style=\"width:12.5%;\">';
		}
	}
	html_b += '<col style="width:10%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th style="line-height:15px;">구분</th>';
	if (half_year == 1) {
		for (var h = 1; h <= 12; h++) {
			html_b += '<th style=line-height:15px;>' + h + '월</th>';
		}
	}
	else if (half_year == 2) {
		for (var h = 1; h <= 6; h++) {
			html_b += '<th style=line-height:15px;>' + h + '월</th>';
		}
	}
	else if (half_year == 3) {
		for (var h = 7; h <= 12; h++) {
			html_b += '<th style=line-height:15px;>' + h + '월</th>';
		}
	}
	html_b += '<th style="line-height:15px;">총계</th>';
	html_b += '</tr>';

	var total_cnt = 0;
	if (data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		var begin = _page - 1;
		var end = total_cnt;

		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for (var i = begin; i < end; i++) {
			html_b += '<tr>';
			html_b += '  <td>' + data.data[index].subject + '</td>';
			if (half_year == 1) {
				<?php for($i=1; $i<=12; $i++) { echo "html_b += '  <td>'+ data.data[index].ck".($i) ." +'</td>';"; }?>
			}
			else {
				<?php for($i=1; $i<=6; $i++) { echo "html_b += '  <td>'+ data.data[index].ck".($i) ." +'</td>';"; }?>
			}
			html_b += '  <td>' + numberToCurrency(data.data[index].tot) + '</td>';
			html_b += '</tr>';
			index++;
		}
		// 구분별 total
		html_b += '<tr><td colspan="1" style="background:#F1F1F1 !important;">총계</td>';
		for (var item in data.data_tot[0]) {
			html_b += '  <td style="background:#F1F1F1 !important;">' + numberToCurrency(data.data_tot[0][item]) + '</td>';
		}
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="30" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';

	// list html
	$('#list').empty().html(html_b);
}



/*컬럼차트 만들기 시작*/
google.charts.load('current', {'packages': ['corechart', 'corechart']});

function column_chart(data) {
	var chart_html = '';
	for (var i = 0; i < 6; i++) {
		chart_html += '<div id="chart_div' + i + '" class="chart_div"></div>';
		$('#chart_div').html(chart_html);
	}
	var half_year = $('#half_year').val();
	var month_end = 12;
	if (half_year != 1) month_end = 6;
	var year = $('#sel_year').val();
	//var index = 1;
	var month = '';
	var str_month = '';
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	subject[0] = '월';
	for (var i = 0; i < data.tot_cnt; i++) {
		subject[i + 1] = data.data[i].subject;
		if (i == data.tot_cnt - 1) {
			subject[i + 2] = {
				role: 'annotation'
			};
		}
	}
	array_data[0] = subject;
	for (var m = 1; m <= month_end; m++) {
		value = new Array();
		if (half_year != 3) value[0] = m + '월';
		else value[0] = m + 6 + '월';
		str_month = 'ck' + String(m);

		for (var i = 0; i < data.tot_cnt; i++) {
			value[i + 1] = Number(data.data[i][str_month]);
			if (i == data.tot_cnt - 1) {
				value[i + 2] = data.data_tot[0]['sum(M.' + str_month + ')'];
			}
		}
		array_data[m] = value;
	}
	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + year + '년] Column Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 300,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '400',
		},
		fontSize: 12,
		axisTitlesPosition: 'in',
		bar: {
			groupWidth: "50%",
		},
		isStacked: true
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'));
	chart.draw(chart_data, options);

	img_chart(0, chart_data, options);
}

function column_chart2(data) {

	var half_year = $('#half_year').val();
	var month_end = 12;
	if (half_year != 1) month_end = 6;
	var year = $('#sel_year').val();
	//var index = 1;
	var month = '';
	var str_month = '';
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	subject[0] = '월';
	for (var i = 0; i < data.tot_cnt; i++) {
		subject[i + 1] = data.data[i].subject;
		if (i == data.tot_cnt - 1) {
			subject[i + 2] = {
				role: 'annotation'
			};
		}
	}
	array_data[0] = subject;
	for (var m = 1; m <= month_end; m++) {
		value = new Array();
		if (half_year != 3) value[0] = m + '월';
		else value[0] = m + 6 + '월';
		str_month = 'ck' + String(m);

		for (var i = 0; i < data.tot_cnt; i++) {
			value[i + 1] = Number(data.data[i][str_month]);
			if (i == data.tot_cnt - 1) {
				value[i + 2] = data.data_tot[0]['sum(M.' + str_month + ')'];
			}
		}
		array_data[m] = value;
	}

	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + year + '년] Column Chart2',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 300,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '400',
		},
		bar: {
			groupWidth: "50%",
		},
		fontSize: 12,
		axisTitlesPosition: 'in',
		isStacked: 'percent',
		vAxis: {
			minValue: 0,
			ticks: [0, .3, .6, .9, 1]
		}
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('column_chart2'));
	chart.draw(chart_data, options);

	img_chart(1, chart_data, options);
}
/*컬럼차트 만들기 끝*/

/*바차트 만들기 시작*/
function bar_chart(data) {
	var half_year = $('#half_year').val();
	var month_end = 12;
	if (half_year != 1) month_end = 6;
	var year = $('#sel_year').val();
	//var index = 1;
	var month = '';
	var str_month = '';
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	subject[0] = '월';
	for (var i = 0; i < data.tot_cnt; i++) {
		subject[i + 1] = data.data[i].subject;
		if (i == data.tot_cnt - 1) {
			subject[i + 2] = {
				role: 'annotation'
			};
		}
	}
	array_data[0] = subject;
	for (var m = 1; m <= month_end; m++) {
		value = new Array();
		if (half_year != 3) value[0] = m + '월';
		else value[0] = m + 6 + '월';
		str_month = 'ck' + String(m);

		for (var i = 0; i < data.tot_cnt; i++) {
			value[i + 1] = Number(data.data[i][str_month]);
			if (i == data.tot_cnt - 1) {
				value[i + 2] = data.data_tot[0]['sum(M.' + str_month + ')'];
			}
		}
		array_data[m] = value;
	}
	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + year + '년] Bar Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 300,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '400',
		},
		fontSize: 12,
		isStacked: true
	};

	var chart = new google.visualization.BarChart(document.getElementById('bar_chart'));
	chart.draw(chart_data, options);

	img_chart(2, chart_data, options);
}
/*바차트 만들기 끝*/

/*도넛그래프 만드기 시작*/
function donut_chart(data) {
	var half_year = $('#half_year').val();
	var month_end = 12;
	if (half_year != 1) month_end = 6;
	var year = $('#sel_year').val();
	//var index = 1;
	var month = '';
	var str_month = '';
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	subject[0] = '월';
	for (var i = 0; i < data.tot_cnt; i++) {
		subject[i + 1] = data.data[i].subject;
		if (i == data.tot_cnt - 1) {
			subject[i + 2] = {
				role: 'annotation'
			};
		}
	}
	array_data[0] = subject;
	array_data[0] = ['구분', '상담건수'];
	var index = 1;
	for (var item in data.data_tot[0]) {
		if (item == 'tot') break;
		if (half_year != 3) month = index + '월';
		else month = index + 6 + '월';
		value = Number(data.data_tot[0][item]);
		array_data[index] = [month, value];
		index++;
	}
	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + year + '년] Donut Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 100,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '100%',
		},
		legend: {
			position: 'right',
			maxLines: 3,
		},
		tooltip: {
			ignoreBounds: true,
		},
		fontSize: 12,
		pieHole: 0.4,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart = new google.visualization.PieChart(document.getElementById('donut_chart'));
	chart.draw(chart_data, options);

	img_chart(3, chart_data, options);
}
/*도넛차트 만들기 끝*/

/*파이차트 만드기 시작*/
function pie_chart(data) {
	var half_year = $('#half_year').val();
	var month_end = 12;
	if (half_year != 1) month_end = 6;
	var year = $('#sel_year').val();
	//var index = 1;
	var month = '';
	var str_month = '';
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	subject[0] = '월';
	for (var i = 0; i < data.tot_cnt; i++) {
		subject[i + 1] = data.data[i].subject;
		if (i == data.tot_cnt - 1) {
			subject[i + 2] = {
				role: 'annotation'
			};
		}
	}
	array_data[0] = subject;
	array_data[0] = ['구분', '상담건수'];
	var index = 1;
	for (var item in data.data_tot[0]) {
		if (item == 'tot') break;
		if (half_year != 3) month = index + '월';
		else month = index + 6 + '월';
		value = Number(data.data_tot[0][item]);
		array_data[index] = [month, value];
		index++;
	}
	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + year + '년] 3D Pie Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 100,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '100%',
		},
		legend: {
			position: 'right',
			maxLines: 3,
		},
		tooltip: {
			ignoreBounds: true,
		},
		fontSize: 12,
		is3D: true,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
	chart.draw(chart_data, options);

	img_chart(4, chart_data, options);
}
/*파이차트 만들기 끝*/

/*라인차트 만들기 시작*/
function line_chart(data) {
	var half_year = $('#half_year').val();
	var month_end = 12;
	if (half_year != 1) month_end = 6;
	var year = $('#sel_year').val();
	//var index = 1;
	var month = '';
	var str_month = '';
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	subject[0] = '월';
	for (var i = 0; i < data.tot_cnt; i++) {
		subject[i + 1] = data.data[i].subject;
	}
	subject[i + 1] = '총계';
	array_data[0] = subject;
	for (var m = 1; m <= month_end; m++) {
		value = new Array();
		if (half_year != 3) value[0] = m + '월';
		else value[0] = m + 6 + '월';
		str_month = 'ck' + String(m);

		for (var i = 0; i < data.tot_cnt; i++) {
			value[i + 1] = Number(data.data[i][str_month]);
		}
		value[i + 1] = Number(data.data_tot[0]['sum(M.' + str_month + ')']);

		array_data[m] = value;
	}
	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + year + '년] Line Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 300,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '400',
		},
		fontSize: 12,
		axisTitlesPosition: 'in',
		is3D: true,
	};

	var chart = new google.visualization.LineChart(document.getElementById('line_chart'));
	chart.draw(chart_data, options);

	img_chart(5, chart_data, options);
}
/*라인차트 만드기 끝*/

/*이미지 차트 만들기 시작*/
function img_chart(sel_row, chart_data, options) {

	var chart_div = document.getElementById('chart_div' + String(sel_row));

	if (sel_row == 0) {
		var chart = new google.visualization.ColumnChart(chart_div);
	}
	else if (sel_row == 1) {
		var chart = new google.visualization.ColumnChart(chart_div);
	}
	else if (sel_row == 2) {
		var chart = new google.visualization.BarChart(chart_div);
	}
	else if (sel_row == 3) {
		var chart = new google.visualization.PieChart(chart_div);
	}
	else if (sel_row == 4) {
		var chart = new google.visualization.PieChart(chart_div);
	}
	else if (sel_row == 5) {
		var chart = new google.visualization.LineChart(chart_div);
	}

	google.visualization.events.addListener(chart, 'ready', function() {
		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});

	chart.draw(chart_data, options);
}
/*이미지 차트 만들기 끝*/


</script>

<?php
include_once './inc/inc_menu.php';
?>


		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">상담내역 통계</h2>
				<span class="location">홈 > 상담내역 통계 > <em>통계</em></span>
				<input type="hidden" id="begin" value="<?php echo $date['begin']?>">
				<input type="hidden" id="end" value="<?php echo $date['end']?>">
				<input type="hidden" id="sch_asso_code" value="<?php echo $date['sch_asso_code']?>">
				<input type="hidden" id="sch_s_code" value="<?php echo $date['sch_s_code']?>">
				<input type="hidden" id="sch_csl_proc_rst" value="<?php echo $date['sch_csl_proc_rst']?>">
			</div>
<?php
include_once './inc/inc_statistic_info.php';
?>
			<ul class="tab">
  		<li><a href="#" class="go_page">상담사례</a></li>
				<li><a href="#" class="go_page">기본통계</a></li>
				<li><a href="#" class="go_page">교차통계</a></li>
				<li><a href="#" class="go_page">임금근로시간통계</a></li>
				<li><a href="#" class="go_page">소속별통계</a></li>
				<li><a href="#" class="go_page selected" style="text-decoration:none">월별통계</a></li>
				<li><a href="#" class="go_page">시계열통계</a></li>
			</ul>
			<div class="cont_area" style="margin-top:0">
				<form name="frmSearch" id="frmSearch" method="post">
				<div class="con_text">
				<label for="sel_year" class="l_title">상담연도</label>
					<select id="sel_year" name="sel_year">
					</select>
					<select id="half_year" name="half_year">
						<option value="1">연간</option>
						<option value="2">상반기</option>
						<option value="3">하반기</option>
					</select>
					<br>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속</button>
						<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
						<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input><!-- 디버깅: 초기화버튼 클릭시 세팅용 초기데이터 -->
           	<input type="hidden" name="asso_code_name"></input>
						<span id="asso_code_name" style="margin-left:27px;"></span>
					</div>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
						<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="s_code" id="s_code"></input>
           	<input type="hidden" name="s_code_name"></input>
						<span id="s_code_name" style="margin-left:5px;"></span>
					</div>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
						<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="csl_proc_rst" id="csl_proc_rst"></input>
           	<input type="hidden" name="csl_proc_rst_name"></input>
						<span id="csl_proc_rst_name" style="margin-left:5px;"></span>
					</div>
					<span class="divice"></span>
					<div class="textR marginT10">
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>
			<!-- //컨텐츠 -->

  		<div class="list_select" style="margin-bottom:10px;">
					<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
					<button type="button" id="printlist" class="buttonM bGray floatR">표 인쇄</button>
					<button type="button" id="view_chart" class="buttonM bGray floatR">통계차트 보기</button>
				</div>

				<!-- list -->
				<div style="overflow:auto;">
					<select id="sel_chart" name="sel_chart" style="display: none">
						<option value=0 selected="true">Column</option>
						<option value=1>Column2</option>
						<option value=2>Bar</option>
						<option value=3>Donut</option>
						<option value=4>Pie</option>
						<option value=5>Line</option>
					</select>
					<button type="button" id="downchart" class="buttonS bBlack" style="display: none">차트다운로드</button>
					<button type="button" id="printchart" class="buttonS bBlack" style="display: none">차트인쇄</button>
					<div id="column_chart" class="chart" style="width:100%; height: 700px; display: none;"></div>
					<div id="column_chart2" class="chart" style="width:100%; height: 700px; display: none;"></div>
					<div id="bar_chart" class="chart" style="width:100%; height: 700px; display: none;"></div>
					<div id="donut_chart" class="chart" style="width: 100%; height: 700px; display: none;"></div>
					<div id="pie_chart" class="chart" style="width: 100%; height: 700px; display: none;"></div>
					<div id="line_chart" class="chart" style="width: 100%; height: 700px; display: none;"></div>
					<div id="list" style="width:100%;"></div>
				</div>

				<div id="chart_div" style="display: none;"></div>

				<a href="#" id="go_to_top" title="Go to top"></a>

			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
<?php
include_once './inc/inc_footer.php';
?>
