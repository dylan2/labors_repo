<script>
$(document).ready(function(){
	//
	$('#curr_pwd').focus();

	// change button
	$('button.cssBtnChgPwdPop').click(function(e){
		e.preventDefault();
		// validation

		// - new pwd
		if($('#new_pwd').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_oper_03']);
			$('#new_pwd').focus();
			return;
		}

		//
		if(confirm(CFG_MSG[CFG_LOCALE]['info_oper_01'])) {
			// request
			$.ajax({
				url: '/?c=admin&m=chg_oper_pwd'
				,data: $('#frmFormPop').serialize() +'&tid=<?php echo $tid;?>'
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					if(data.rst == 'succ') {
						alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
						closeLayerPopup();
					}
					else {
						alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
					}
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
				}
			});
		}
	});

	// cancel button
	$('button.cssBtnCancelPop').click(function(e){
		e.preventDefault();
		closeLayerPopup();
	});
});
</script>


    <div id="popup">
        <header id="pop_header">
			<h2 class="">비밀번호변경</h2>			
        </header>
		<form name="frmFormPop" id="frmFormPop" method="post">
		<div id="popup_contents">			
			<table class="tInsert">
				<caption>운영자 비밀번호변경 입니다.</caption>
				<colgroup>
					<col style="width:40%">
					<col style="width:60%">
				</colgroup>
				<tr>
					<th colspan="2" style="height:70px;">새 비밀번호는 직접 전달하셔야 합니다.</td>
				</tr>
				<tr>
					<th class="textL">변경할 새 비밀번호</th>
					<td><input type="password" name="new_pwd" id="new_pwd"></td>
				</tr>
			</table>			
		</div>
		<div class="btn_set">
			<button type="button" class="buttonM bOrange cssBtnChgPwdPop">변경</button>
			<button type="button" class="buttonM bGray cssBtnCancelPop">취소</button>
		</div>
		</form>
    </div>
