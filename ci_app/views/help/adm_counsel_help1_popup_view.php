
<script>
$(document).ready(function(){
	// 닫기버튼
	$('#btnClosePop').click(function(e){
		closeLayerPopup();
	});
});
</script>


<div id="popup">
    <header id="pop_header">
		<h2 class="">직종</h2>			
    </header>
	<div id="popup_contents">
		<div class="lh20">
			직종은 통계청 한국표준직업분류를 기준으로 하고 있습니다. <br>
			직종별 분류의 검색과 상세내역은 통계분류포털의 분류검색과 분류내용보기(해설) 을 참고하세요.	<br><br>	
			
			<a href="http://kssc.kostat.go.kr/ksscNew_web/link.do?gubun=002" target="_blank" class="fBold">> 통계분류포털 한국표준직업분류 분류검색 [바로가기]</a>
		</div>
	</div>
	<div class="btn_set">
		<button type="button" id="btnClosePop" class="buttonM bGray">닫기</button>
	</div>
</div>
