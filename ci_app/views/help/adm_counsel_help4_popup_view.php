
<script>
$(document).ready(function(){
	// 닫기버튼
	$('#btnClosePop').click(function(e){
		closeLayerPopup();
	});
});
</script>

<div id="popup">
    <header id="pop_header">
		<h2 class="">사용주체</h2>			
    </header>
	<div id="popup_contents">
		<ul class="lh20">
			<li><b>- 직접고용</b>  : 고용과 사용이 분리되지 않는 경우</li>
			<li class="marginT10"><b>- 파견 </b> : 파견사업주에게 고용되어 파견계약에 따라 사용사업주의 지휘·명령을 받아 일하는 경우</li>
			<li class="marginT10"><b>- 용역 </b> : 용역업체에 고용되어 지휘·명령을 받으면서 용역계약을 맺은 다른 업체에서 일하는 경우</li>
		</ul>
	</div>
	<div class="btn_set">
		<button type="button" id="btnClosePop" class="buttonM bGray">닫기</button>
	</div>
</div>
