<?php
/**
 * 
 * 상담내역 - 시계열통계
 * 
 * 추가: 2019.07.22, dylan
 * 
 */
include_once "./inc/inc_header.php";
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="./js/counsel/common.js"></script>
<script type="text/javascript">
var chart_mode = 0;
$(document).ready(function() {

	//차트 보기
	$('#view_chart').click(function(e) {
		e.preventDefault();
		if (chart_mode == 1) { //보기->숨기기
			$('.chart').css('display', 'none');
			$('#view_chart').text('통계차트 보기');
			$('#downchart').css('display', 'none');
			$('#printchart').css('display', 'none');
			chart_mode = 0;
		}
		else { //숨기기->보기
			$('.chart').css('display', 'block');
			$('#view_chart').text('통계차트 숨기기');
			$('#downchart').css('display', 'inline');
			$('#printchart').css('display', 'inline');

			var position = $('#downchart').offset();
			$('html').animate({
				scrollTop: position.top
			}, 1000, function() {
				// 구글차트
				column_chart(sttt_data);
				chart_mode = 1;
			});
		}
	});

	//위로 이동
	$('#view_top').click(function(e) {
		e.preventDefault();
		var position = $('#contents_body').offset();
		$('html, body').animate({
			scrollTop: position.top
		}, 1000);
	});

	// 통계유형 탭 클릭 이벤트 핸들러
	$(".stab_btn").on("click", function(e) {
		e.preventDefault();
		var index = $(".stab_btn").index(this);
		$('#txt_sch_kind').val($(this).text());
		$('#sch_kind').val(index);
		$(".stab_btn").attr('class', 'stab_btn');
		$(this).attr('class', 'stab_btn active');
		//검색대상이 상담유형이면, 안내 레이블 출력
		if (index == 9) $('#infoLabel').show();
		else $('#infoLabel').hide();

		get_list(_page);
	});

	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#s_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=s_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e) {
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#csl_proc_rst').val(data.s_code);
				}
				if (data.code_name) {
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=csl_proc_rst_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e) {
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#asso_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=asso_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e) {
		e.preventDefault();
		$('#asso_code').val($('#asso_code_all').val());
		$('#asso_code_name').text("전체");
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
	});

	// 시작연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for (var i = year; i >= 2015; i--) {
		$("#begin").append("<option value=" + i + ">" + i + "년</option>");
		$("#end").append("<option value=" + i + ">" + i + "년</option>");
	}

	//통계탭 이동
	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this) + 1;

		if (index == 2) index = 13;
		else if (index == 3) index = 11;
		else if (index == 6) index = 12;

		var begin = $('#begin').val();
		var end = $('#end').val();
		var sch_asso_code = $("#asso_code").val();
		var sch_s_code = $("#s_code").val();
		var sch_csl_proc_rst = $("#csl_proc_rst").val();

		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});

	// 통계구분 변경시 처리
	$('input[name=sttt_type]').change(function(e) {
		e.preventDefault();
		$("#btnSubmit").click();
	});

	// print a list
	$('#printlist').click(function() {
		var begin = $('#begin').val();
		var end = $('#end').val();
		var popupWindow = window.open("", "_blank");
		var $div = $('#list').clone();
		$div.find('table').css('border-collapse', 'collapse').find('th, td').css({
			'border': 'solid 1px #eee',
			'height': '30px'
		});
		$div.find('th').css('background-color', '#e8fcff');
		popupWindow.document.write("<body>");
		popupWindow.document.write("<h3>시계열통계: " + $('#txt_sch_kind').val() + "(" + $('input[name=sttt_type]:checked').next().text().trim() + "), 상담연도: " + begin + '년~' + end + "년</h3>");
		popupWindow.document.write($div.html());
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("table").css("width", "100%");
		$(popupWindow.document).find("td").css("text-align", "center");
		popupWindow.print();
		popupWindow.close();
	});

	// print a chart images
	$('#printchart').click(function() {
		var popupWindow = window.open("", "_blank");
		var div = $('#chart_div').html();
		popupWindow.document.write("<body>");
		popupWindow.document.write(div);
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("img").css("width", "100%");
		popupWindow.print();
		popupWindow.close();
	});

	// download a chart images
	$('#downchart').click(function() {
		$('#chart_div').css('display', 'block');
		html2canvas($("#chart_div"), {
			// html2canvas는 보이는 부분만 캡쳐하기 때문에 chart_div내 첫번째 차트 이미지의 가로 크기가 커지는 경우 
			// 캡쳐안되는 문제가 있어 첫번째 차트 이미지의 크기를 캡쳐영역으로 지정하도록 수정함
			width: $("#chart_div").find('img').width(),
			height: $("#chart_div").height(),
			onrendered: function(canvas) {
				var img = canvas.toDataURL();
				$("body").append('<form id="imgForm" method="POST" target="_blank" action="/?c=chart_download&m=download">' +
					'<input type="hidden" name="dataUrl" value="' + img + '">' +
					'</form>');
				$("#imgForm").submit();
				$("#imgForm").remove();
			}
		});
		$('#chart_div').css('display', 'none');
	});

	// download a excel file
	$('#downExcel').click(function() {
		if ($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var url = '/?c=statistic&m=open_down_view&kind=down07_' + $('input[name=sttt_type]:checked').val();
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
	});

	// 검색
	$("#btnSubmit").click(function(e) {
		e.preventDefault();
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val($('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());

		get_list(_page);
	});

	// list
	get_list(_page, true);
});

// 서버로부터 수신된 데이터
var sttt_data = [];

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page, is_first) {
	// page
	_page = page;
	// request
	var url = '/?c=statistic&m=st07';
	var rsc = $('#frmSearch').serialize();
	var fn_succes = function(data) {
		sttt_data = data;

		gen_list(sttt_data);

		if (chart_mode == 1) { //차트 보기 상태일 때
			column_chart(sttt_data);
		}

		if (sttt_data.tot_cnt == 0) alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
	};
	var fn_error = function(data) {
		if (is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * list html block 생성
 */
function gen_list(data) {
	var sch_kind = $('#sch_kind').val();
	var begin_year = $('#begin').val();
	var end_year = $('#end').val();
	var th_cnt = (end_year - begin_year + 1) * 2 + 1;
	var sttt_type = $('input[name=sttt_type]:checked').val(); // 건수비율, 임금근로시간 구분

	var html_b = '<table class="tList02" border=0>';
	html_b += '<colgroup>';
	html_b += '<col style="width:20%;">';
	for (var y = begin; y <= end; y++) {
		html_b += '<col style=\"width:*;\">';
	}
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th rowspan="2" style="line-height:15px;border-right:1px solid #ddd;">구분</th>';
	for (var y = begin_year; y <= end_year; y++) {
		html_b += '<th colspan="2" style="line-height:15px;border-right:1px solid #ddd;">' + y + '년</th>';
	}
	html_b += '</tr>';
	html_b += '<tr>';
	var t1 = '건수',
		t2 = '비율';
	if (sttt_type == 2) { //임금,근로시간 
		t1 = '월평균임금 (빈도)', t2 = '주당근로시간 (빈도)';
	}
	for (var y = begin_year; y <= end_year; y++) {
		html_b += '<th style="line-height:15px;border-right:1px solid #ddd;">' + t1 + '</th>';
		html_b += '<th style="line-height:15px;border-right:1px solid #ddd;">' + t2 + '</th>';
	}
	html_b += '</tr>';

	var total_cnt = 0;
	if (data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;

		// list
		var begin = _page - 1;
		var end = total_cnt;

		var no = total_cnt - (begin * _itemPerPage);
		var index = 0,
			d = '',
			tot_sum = [];
		for (var i = begin; i < end; i++) {
			d = data.data[index];
			html_b += '<tr>';
			html_b += '  <td>' + d.subject + '</td>';
			for (var y = begin_year; y <= end_year; y++) {
				if (sttt_type == 1) { //건수비율
					html_b += '  <td>' + applyComma(d['ck_' + y]) + '</td>';
					html_b += '  <td>' + d['cr_' + y] + '%</td>';
				}
				else { //임금근로시간
					if (!tot_sum[y - begin_year]) tot_sum[y - begin_year] = {
						p: 0,
						w: 0,
						pcnt: 0,
						wcnt: 0
					};
					tot_sum[y - begin_year].p += +d['p_' + y].split('(')[0].trim() * +d['p_' + y].split('(')[1].split(')')[0].trim();
					tot_sum[y - begin_year].pcnt += +d['p_' + y].split('(')[1].split(')')[0].trim();
					tot_sum[y - begin_year].w += +d['w_' + y].split('(')[0].trim() * +d['w_' + y].split('(')[1].split(')')[0].trim();
					tot_sum[y - begin_year].wcnt += +d['w_' + y].split('(')[1].split(')')[0].trim();
					html_b += '  <td>' + applyComma(d['p_' + y]) + '</td>';
					html_b += '  <td>' + applyComma(d['w_' + y]) + '</td>';
				}
			}
			html_b += '</tr>';
			index++;
		}
		// 전체 (빈도)
		html_b += '<tr>';
		//-건수비율
		if (sttt_type == 1) {
			html_b += '<td style="background:#F1F1F1 !important;">총계'+ ($('#sch_kind').val() == 9 ? '(실건수)' : '') +'</td>';
			for (var y = begin_year; y <= end_year; y++) {
				var txt_tot = applyComma(data.data_tot['ck_' + y]);
				if (sch_kind == 9) { //상담유형
					var arr_tot = data.data_tot['ck_' + y].split('(');
					txt_tot = applyComma(arr_tot[0]) + '(' + applyComma(arr_tot[1].split(')')[0]) + ')';
				}
				html_b += '  <td style="background:#F1F1F1 !important;">' + txt_tot + '</td>';
				html_b += '  <td style="background:#F1F1F1 !important;">' + data.data_tot['cr_' + y] + '%</td>';
			}
		}
		//-임금근로시간
		else {
			html_b += '<td style="background:#F1F1F1">전체 (빈도)</td>';
			for (var y = begin_year; y <= end_year; y++) {
				var w = numberToCurrency((tot_sum[y - begin_year].w/tot_sum[y - begin_year].wcnt).toFixed(1));
				html_b += '  <td style="background:#F1F1F1">' + numberToCurrency((tot_sum[y - begin_year].p/tot_sum[y - begin_year].pcnt).toFixed()) + ' (' + numberToCurrency(tot_sum[y - begin_year].pcnt) + ')</td>';
				html_b += '  <td style="background:#F1F1F1">' + w + ' (' + numberToCurrency(tot_sum[y - begin_year].wcnt) + ')</td>';
			}
		}
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="' + th_cnt + '" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';

	// list html
	$('#list').empty().html(html_b);
	$('#list th,td').css({whiteSpace:'nowrap'});
}


/*막대그래프 만들기 시작*/
google.charts.load('current', {'packages': ['corechart', 'bar']});

function column_chart(data) {
	var sttt_type = $('input[name=sttt_type]:checked').val();
	var txt_sch_kind = $('#txt_sch_kind').val();
	var chart_html = '';
	var begin_year = $('select[name=begin] option:selected').val();
	var end_year = $('select[name=end] option:selected').val();	
	// 차트 데이터 생성
	var chart_data = new google.visualization.DataTable();
	var array_data = [];
	
	if (sttt_type == 1) { //건수비율
		var header = ['구분'],
			data_view = [0];
		for (var y=begin_year; y<=end_year; y++) {
			// 데이터 header
			header.push(y + '');
			// 뷰용 데이터 컬럼 descriptor
			data_view.push(y - (begin_year - 1));
			data_view.push({
				calc: "stringify",
				sourceColumn: y - (begin_year - 1),
				type: "string",
				role: "annotation"
			});
		}
		for (var idx in data.data) {
			var ann_data = [];
			ann_data.push(data.data[idx].subject);
			for (var y=begin_year; y<=end_year; y++) {
				ann_data.push(+data.data[idx]['ck_' + y]);
			}
			array_data[+idx + 1] = ann_data;
		}
		array_data[0] = header;
		data = new google.visualization.arrayToDataTable(array_data);
		var chart_data = new google.visualization.DataView(data);	
		chart_data.setColumns(data_view);

		var chart_title = "시계열통계 Chart: " + txt_sch_kind + "(" + $('input[name=sttt_type]:checked').next().text().trim() + ")";

		var options = {
			title: chart_title,
			width: 1620,
			height: 700,
			chartArea: {
				left: 100,
				right: 100,
				top: 100,
				bottom: 200,
				width: '100%',
				height: '400',
			},
			bar: {
				groupWidth: "30%",
			},
			fontSize: 12,
			axisTitlesPosition: 'in',
			colors: ['#3366CC', '#448932']
		};

		var chart = new google.visualization.ColumnChart(document.getElementById('column_chart0'));
		var chart_div = document.getElementById('chart_div');

		google.visualization.events.addListener(chart, 'ready', function() {
	 		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
		});

		chart.draw(chart_data, options);
	}
	//임금,근로시간
	else { 
		$('#chart_div').append('<div id="chart_div0" class="chart_div"></div>');
		drawChartPayWorks(0, data);
		// 2번째 차트
		for (var i=1; i<=end_year - begin_year; i++) {
			$('#chart_container').append('<div id="column_chart'+ i +'" class="chart" style="width: 100%; height: 700px;"></div>');
			// 인쇄, 다운로드용
			$('#chart_div').append('<div id="chart_div' + i + '" class="chart_div"></div>');
			drawChartPayWorks(i, data);
		}
	}
}


/**
 * 임금,근로시간 차트 전용
 * 조회년도 모두 차트 생성한다.
 */
function drawChartPayWorks(i, data) {
	// 차트 데이터 생성
	var chart_data = new google.visualization.DataTable();
	var array_data = [];
	chart_data.addColumn('string', '구분');
	chart_data.addColumn('number', '임금');
	chart_data.addColumn({
		type: 'string',
		role: 'annotation'
	});
	chart_data.addColumn({
		type: 'string',
		role: 'style'
	});
	chart_data.addColumn('number', '근로시간');
	chart_data.addColumn({
		type: 'string',
		role: 'annotation'
	});
	chart_data.addColumn({
		type: 'string',
		role: 'style'
	});
	var curr_year = +$('select[name=begin] option:selected').val() + i;
	for (var idx in data.data) {
		var d = data.data[idx];
		var pay = +d['p_' + curr_year].split('(')[0].trim();
		var work = +d['w_' + curr_year].split('(')[0].trim();
		array_data.push([d.subject, pay, numberToCurrency(pay), '#3366CC', work, numberToCurrency(work), '#448932']);
	}
	chart_data.addRows(array_data);

	var chart_title = "시계열통계 Chart: " + txt_sch_kind + "(" + $('input[name=sttt_type]:checked').next().text().trim() + ")"+
		'\n상담년도: ' + curr_year + '년';

	var options = {
		title: chart_title,
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 100,
			top: 100,
			bottom: 200,
			width: '100%',
			height: '400',
		},
		bar: {
			groupWidth: "30%",
		},
		fontSize: 12,
		axisTitlesPosition: 'in',
		series: {
			0: {
				targetAxisIndex: 0
			},
			1: {
				targetAxisIndex: 1
			}
		},
		vAxes: {
			0: {
				title: '임금'
			}, // Left y-axis.
			1: {
				title: '근로시간'
			} // Right y-axis.
		},
		colors: ['#3366CC', '#448932']
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'+ i));
	var chart_div = document.getElementById('chart_div'+ i);

	google.visualization.events.addListener(chart, 'ready', function() {
		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});

	chart.draw(chart_data, options);
}
/*막대그래프 만들기 끝*/
</script>

<?php
include_once './inc/inc_menu.php';
?>


<!--- //lnb 메뉴 area ---->
<div id="contents_body">
	<div id="cont_head">
		<h2 class="h2_tit">상담내역 통계</h2>
		<span class="location">홈 > 통계 > <em>상담내역 통계</em></span>
	</div>
<?php
include_once './inc/inc_statistic_info.php';
?>
	<ul class="tab">
		<li><a href="#" class="go_page">상담사례</a></li>
		<li><a href="#" class="go_page">기본통계</a></li>
		<li><a href="#" class="go_page">교차통계</a></li>
		<li><a href="#" class="go_page">임금근로시간통계</a></li>
		<li><a href="#" class="go_page">소속별통계</a></li>
		<li><a href="#" class="go_page">월별통계</a></li>
		<li><a href="#" class="go_page selected" style="text-decoration: none">시계열통계</a></li>
	</ul>
	<div class="cont_area" style="margin-top: 0">
	
		<form name="frmSearch" id="frmSearch" method="post">
			<input type="hidden" name="not_seq" value="0"></input>
			<input type="hidden" id="sch_kind" name="sch_kind" value="0"></input>
			
			<div class="con_text">
				<label for="begin" class="l_title">상담연도</label>
				<select id="begin" name="begin"></select> ~ <select id="end" name="end"></select>
				<br>
				<span class="divice"></span>
				<div>
					<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속</button>
					<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
					<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
					<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input>
					<!-- 디버깅: 초기화버튼 클릭시 세팅용 초기데이터 -->
					<input type="hidden" name="asso_code_name"></input>
					<span id="asso_code_name" style="margin-left: 27px;"></span>
				</div>
				<span class="divice"></span>
				<div>
					<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
					<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
					<input type="hidden" name="s_code" id="s_code"></input>
					<input type="hidden" name="s_code_name"></input>
					<span id="s_code_name" style="margin-left: 5px;"></span>
				</div>
				<span class="divice"></span>
				<div>
					<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
					<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
					<input type="hidden" name="csl_proc_rst" id="csl_proc_rst"></input>
					<input type="hidden" name="csl_proc_rst_name"></input>
					<span id="csl_proc_rst_name" style="margin-left: 5px;"></span>
				</div>
				<span class="divice"></span>
				<div class="stab_button">
					<label for="sch_kind" class="stab_title">통계 유형</label>
					<input type="hidden" id="txt_sch_kind" name="txt_sch_kind" value="상담방법">
					<button type="button" class="stab_btn active">상담방법</button>
					<button type="button" class="stab_btn">소속</button>
					<button type="button" class="stab_btn">성별</button>
					<button type="button" class="stab_btn">연령대</button>
					<button type="button" class="stab_btn">직종</button>
					<button type="button" class="stab_btn">업종</button>
					<button type="button" class="stab_btn">고용형태</button>
					<button type="button" class="stab_btn">근로계약서</button>
					<button type="button" class="stab_btn">4대보험</button>
					<button type="button" class="stab_btn">상담유형</button>
				</div>
				<div id="infoLabel" class="floatL marginT10" style="color: #f37000; display: none">
					* 상담유형은 중복선택이 가능하여 데이터가 중복될 수 있습니다. 총계표기 : 중복데이터(실데이터)
				</div>
				<span class="divice"></span>
				<div class="marginT30">
					<div class="textL">
						<label for="sch_kind" class="stab_title">통계 구분</label>
						<input type="radio" name="sttt_type" id="sttt_type1" class="imgM" value="1" checked><label for="sttt_type1"> 건수/비율</label>
						<input type="radio" name="sttt_type" id="sttt_type2" class="imgM" value="2"><label for="sttt_type2"> 임금/근로시간</label>
					</div>
					<div class="textR">
						<button type="button" id="btnSubmit" class="buttonS bBlack">
							<span class="icon_search"></span>검색
						</button>
					</div>
				</div>
			</div>
		</form>
		<!-- //컨텐츠 -->

		<div class="list_select" style="margin-bottom:10px;">
			<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
			<button type="button" id="printlist" class="buttonM bGray floatR">표 인쇄</button>
			<button type="button" id="view_chart" class="buttonM bGray floatR">통계차트 보기</button>
		</div>

		<!-- list -->
		<div style="overflow: auto;">
			<button type="button" id="downchart" class="buttonS bBlack" style="display: none">차트다운로드</button>
			<button type="button" id="printchart" class="buttonS bBlack" style="display: none">차트인쇄</button>
			<div id="chart_container">
				<div id="column_chart0" class="chart" style="display: none; width: 100%; height: 700px;"></div>
			</div>
			<div id="list" style="width: 100%;"></div>
		</div>

		<div id="chart_div" style="display: none;"></div>

		<a href="#" id="go_to_top" title="Go to top"></a>
		
	</div>
	<!-- //컨텐츠 -->
</div>
<!-- //  contents_body  area -->

<?php
include_once './inc/inc_footer.php';
?>
