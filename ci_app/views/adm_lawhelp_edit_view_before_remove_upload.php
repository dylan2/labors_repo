<?php
include_once './inc/inc_header.php';
?>

<script>

var kind = "<?php echo $res['kind']; ?>";
if(!kind) kind = 'add';
//
var is_owner = "<?php echo $res['is_owner']; ?>";

$(document).ready(function(){

	// 상담일 today 세팅
	if(kind == 'add') {
		$('#csl_date').val(get_today());

		// 거주지 주소 최초 선택 처리
		$('#live_addr option').each(function(i,ele){
			if($(this).text() == '무응답') {
				$('#live_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});
		// 소재지 주소 최초 선택 처리
		$('#comp_addr option').each(function(i,ele){
			if($(this).text() == '무응답') {
				$('#comp_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});
	}
	// edit
	else {
		// 거주지 주소
		if($('#live_addr option:selected').text() == '기타') {
			$('#live_addr_etc').css('display', 'inline');
		}
		// 소재지 주소
		if($('#comp_addr option:selected').text() == '기타') {
			$('#comp_addr_etc').css('display', 'inline');
		}
		// 출석일 버튼 활성화
		$('#btnAddLidDateContainer').show();
		$('#lib_desc').hide();
	}

	// 인쇄 버튼
	$('.cssPrint').click(function(e){
		$('#focusOuter').focus();
		if(kind == 'edit') {
			open_popup_center_dual('/?c=lawhelp&m=lawhelp_print&seq='+ $('#seq').val(), '인쇄', 1060, 700);
		}
	});

	// 출석일 버튼 - 저장
	$('input:button[id=btnAddLidDate]').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if($('#lid_date').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_13']);
			$('#lid_date').focus();
			return;
		}
		var url = '/?c=lawhelp&m=add_lid_date';
		var rsc = 'seq='+ $('#seq').val() +'&lid_date='+ $('#lid_date').val();
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				var new_html = '<span style="width:100px;">'+ $('#lid_date').val() +'<input type="button" class="buttonS bGray cssRemoveLidDate" value="삭제" data-lid_seq="'+ data.new_id +'" style="padding:2px 4px;margin:0 10px 0 3px;"></span>';
				$('#lidDataContainer').prepend(new_html);
			}
			$('#lid_date').val('');
		};
		var fn_error = function(data) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);
	});

	// 출석조사일 삭제 버튼
	$('#lidDataContainer').bind('click', '.cssRemoveLidDate', function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if($(e.target)[0].tagName !== 'INPUT') return;

		var btn_this = $(e.target);
		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_10'])) {
			var url = '/?c=lawhelp&m=del_lid_date';
			var rsc = 'lid_seq='+ btn_this.attr('data-lid_seq');
			var fn_succes = function() {
				btn_this.closest('span').remove();
			};
			var fn_error = function(data) {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
			};
			// request
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});

	// 전송버튼 클릭시 form onSubmit 이벤트 발생처리
	$('.cssAdd').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
			$('form').submit();
		}
	});
	
	// 등록 버튼 이벤트 핸들러
	$('form').submit(function(e) {

		// 수정 - 자신의 글인지 체크
		if(kind == 'edit' && is_master != 1 && is_owner == 0) {
			alert(CFG_MSG[CFG_LOCALE]['warn_board_02']);
			return false;
		}

		// validation
		// 필수체크 항목
		// - 접수번호(자동생성), -> 직접입력으로 변경 : 최진혁 20160617
		// - 지원종류, 지원승인일, 신청자, 거주지, 회사, 회사소재지, 신청기관, 대리인(소속,이름), 대상기관, 권리구제지원내용
		// 접수번호
		if($('input[name=lh_code]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_12']);
			$('#lh_code').focus();
			return false;
		}
		// 지원종류
		if(!$("input[name=sprt_kind_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_01']);
			$('#sprt_kind_cd_0').focus();
			return false;
		}
		// 지원승인일
		if($('input[name=lh_sprt_cfm_date]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_02']);
			$('input[name=lh_sprt_cfm_date]').focus();
			return false;
		}
		// 신청자
		if($('input[name=lh_apply_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_03']);
			$('input[name=lh_apply_nm]').focus();
			return false;
		}
		// 거주지
		if($('input[name=lh_apply_addr]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_04']);
			$('input[name=lh_apply_addr]').focus();
			return false;
		}
		// 회사
		if($('input[name=lh_apply_comp_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_05']);
			$('input[name=lh_apply_comp_nm]').focus();
			return false;
		}
		// 회사소재지
		if($('input[name=lh_comp_addr]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_06']);
			$('input[name=lh_comp_addr]').focus();
			return false;
		}
		// 신청기관
		if(!$("input[name=apply_organ_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_07']);
			$('#apply_organ_cd_0').focus();
			return false;
		}
		// 대리인-소속
		if($('input[name=lh_labor_asso_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_08']);
			$('input[name=lh_labor_asso_nm]').focus();
			return false;
		}
		// 대리인-이름
		if($('input[name=lh_labor_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_09']);
			$('input[name=lh_labor_nm]').focus();
			return false;
		}
		// 대상기관
		if(!$("input[name=sprt_organ_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_10']);
			$('#sprt_organ_cd_0').focus();
			return false;
		}
		// 권리구제지원내용
		if($('textarea[name=lh_sprt_content]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_11']);
			$('textarea[name=lh_sprt_content]').focus();
			return false;
		}


		// 전송
		// for HTML5 browsers
		if(window.FormData !== undefined)  {

			// ie 9 이하 외 브라우저 때문에 여기서 이벤트 흐름을 끊어준다.
			e.preventDefault();

			// 파일 업로드 : 파일 추가(변경)된 개수
			$('#chg_file_cnt').val(chg_file_cnt);
			// 변경된 실제 파일명들
			$('#chg_real_file_name').val(chg_file_name_real);
			//
			var formData = new FormData(this);

			$('#loading').show();
			setTimeout(function(){
				$.ajax({
					url: '/?c=lawhelp&m=processing'
					,type: 'POST'
					,data:  formData
					,mimeType:"multipart/form-data"
					,contentType: false
					,cache: false
					,processData:false
					,success: function(data, textStatus, jqXHR) {
						var obj = JSON.parse(data);
						if(obj.rst == 'succ') {
							alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
							check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
						}
						else {
							if(is_local) objectPrint(data);
							if(is_local) objectPrint(jqXHR);
							if(is_local) console.log('textStatus : '+ textStatus);
							alert(obj.msg);
						}
					}
					,error: function(jqXHR, textStatus, errorThrown) {
						if(is_local) objectPrint(jqXHR);
						if(is_local) objectPrint(textStatus);
						if(is_local) objectPrint(errorThrown);
					
						var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
						if(data && data.msg) msg += '[' + data.msg +']';
						alert(msg);
					}
					,beforeSend: function(xhr) {
						$('#loading').show();
	  				}
			  	})
				.done(function( data ) {
					$('#loading').hide();
				});
			}, 10);
		}
		// for olden browsers
		else {

	   		// <form> 태그에 action을 직업 기입한 경우 크롬에서 폼 전송 안되는 문제 발생.
			$(this).attr("action", "/?c=lawhelp&m=processing");
		    var formObj = $(this);//*
		 
			//generate a random id
			var iframeId = 'unique' + (new Date().getTime());
	 
			//create an empty iframe
			var iframe = $('<iframe src="javascript:false;" style="width:0px;height:0px;background-color:white;" name="'+iframeId+'" />');
	 
			//hide it
			iframe.hide();
	 
			//set form target to iframe
			formObj.attr('target', iframeId);
	 
			//Add iframe to body
			iframe.appendTo('body');
			iframe.load(function(e) {
				check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
			});
		}
	});

	// list 버튼 이벤트 핸들러
	$('.cssList').click(function(e){
		$('#focusOuter').focus();
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), '', '');
	});

	// 뒤로 버튼 이벤트 핸들러
	$('.cssBack').click(function(e){
		$('#focusOuter').focus();
		history.back();
		return false;
	});


	// 파일 삭제
	$("button.cssDelFile").click(function(e){
		if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_02'])) {
			var index = $(this).parents('.floatC').attr('data-role-index');
			var seq = $('#seq').val();
			var file_name_real = $('#filename_real_'+ index).val();
			var btn_this = this;
			// request
			$.ajax({
				url: '/?c=lawhelp&m=del_file'
				,data: {sq:seq, fnr:file_name_real}
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
					//
					$(btn_this).attr('disabled', 'true');
					$('#filename_'+ index).val('');
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
					
					var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
					if(data && data.msg) msg += '[' + data.msg +']';
					alert(msg);
					$('#txtIndex_pop').focus();
				}
			});
		}
	});

	// 파일 download
	$('button.cssDownload').click(function(){
		var real = $(this).attr('data-role-real');
		var org = $(this).attr('data-role-org');
		// request
		$.ajax({
			url: '/?c=board&m=chk_download'
			,data: {real:real, org:org}
			,cache: false
			,async: false
			,method: 'post'
			,dataType: 'json'
			,success: function(data) {
				if(data.rst == 'fail') {
					alert(CFG_MSG[CFG_LOCALE]['warn_board_01']);
				}
				else {
					window.location.href = "/?c=board&m=download&real="+ real +"&org="+ org;
				}
			}
			,error: function(data) {
				if(is_local) objectPrint(data);
				
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			}
		});
	});
});



// jQuery form submit helper
function getDoc(frame) {
	var doc = null;

	// IE8 cascading access check
	try {
		if (frame.contentWindow) {
			doc = frame.contentWindow.document;
		}
	} catch(err) {
	}

	if (doc) { // successful getting content
		return doc;
	}

	try { // simply checking may throw in ie8 under ssl or mismatched protocol
		doc = frame.contentDocument ? frame.contentDocument : frame.document;
	} catch(err) {
		// last attempt
		doc = frame.document;
	}
	return doc;
}



//==================================================================================================
// 첨부파일 관련
//==================================================================================================
$(document).ready(function(e){
	// 파일 추가 버튼 이벤트 핸들러
	var new_btn_index = $('.cssAddFile').length;
	var cfg_upload_max_count = '<?php echo CFG_UPLOAD_MAX_COUNT;?>';
	$('#file_wrapper').on('click', '.cssAddFile', function(e){
		if(new_btn_index >= cfg_upload_max_count) {
			var limit_msg = (CFG_MSG[CFG_LOCALE]['info_board_06']).replace('x', cfg_upload_max_count);
			alert(limit_msg);
			return false;
		}
		// 태그 생성
		append_file_tag_block(new_btn_index);
		new_btn_index++;
		e.preventDefault();
	});

	// 추가한 항목 제거 이벤트 핸들러
	$('#file_wrapper').on('click', '.btnRremove', function(e){
		$(this).parents('.floatC').remove();
		new_btn_index--;
		e.preventDefault();
	});
});

// 파일 변경 여부 플래그
var chg_file_cnt = 0;
// 변경된 실제 파일명 - 삭제할 대상
var chg_file_name_real = '';
// 파일 개당 업로드 최대 용량
var file_upload_max_size = '<?php echo CFG_UPLOAD_MAX_FILE_SIZE;?>';

// file 선택시 파일 처리
function chg_files($this, ele_name) {
//	alert($($this)[0].size);
	var obj_ele = document.getElementById(ele_name);
	var sel_file_size = $($this)[0].files[0].size;
	// 선택한 파일이 용량 초과한 경우
	if(sel_file_size >= file_upload_max_size) {
		// 
		var info_size = numberToCurrency(Math.floor(parseInt(sel_file_size)/1024768));
		var info_msg = (CFG_MSG[CFG_LOCALE]['info_board_07']).replace('x', info_size);
		alert(info_msg);
		
		// html을 제거한뒤
		var index = $($this).parents('.floatC').attr('data-role-index');
		$($this).parents('.floatC').remove();
		// 새로 생성
		append_file_tag_block(index);
		// 기존 값이 있으면 대입
		if(obj_ele.value != '') {
			$('#filename_'+ index).val( obj_ele.value );
		}
		// 제거 버튼 제거
		$('#filename_'+ index).parents('.floatC').find('div>.btnRremove').remove();

		return false;
	}

	var matches = '';
	// 수정일 경우 기존 파일명과 선택한 파일명이 같으면 업로드하지 않도록 한다.
	if(obj_ele.value) {
		matches = $this.value.match(eval('/'+ obj_ele.value +'/g'));
	}
	// 파일명이 같으면 업로드하지 않는다
	if(matches) {
		alert(CFG_MSG[CFG_LOCALE]['info_board_05']);
	}
	else {
		// 기존 파일이 있는 경우만(처음 업로드시 파일 선택후 다시 선택해도 change 이벤트가 일어난다)
		if(obj_ele.value != '') {
			chg_file_cnt++;
		}
		obj_ele.value = $this.value;

		// 파일이 변경되면, 기존 파일 삭제를 위해 파일이름을 조합한다.
		var real_file_name = ele_name.replace('_', '_real_');
		if(chg_file_name_real != '') chg_file_name_real += '<?php echo CFG_UPLOAD_FILE_NAME_DELIMITER; ?>';
		chg_file_name_real += document.getElementById(real_file_name).value;
	}
}

/**
  * 파일추가 html 태그 생성
  */
function append_file_tag_block(index) {
	$('#file_wrapper').append('<div class="floatC" style="line-height:35px;margin-top:3px;"><div class="floatL cssFileForm"><input type="hidden" id="filename_real_'+ index +'" ></hidden><input type="text" id="filename_'+ index +'" class="floatL" readonly="readonly" style="width:280px;background:none;border:none;"><div class="file_up" ><button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button><input type="file" id="userfile_'+ index +'" name="userfile_'+ index +'" style="width:100%" onchange="javascript:chg_files(this, \'filename_'+ index +'\');"></div></div><div class="floatL marginL05" style="margin-top:-3px;"><button type="button" class="buttonS bGray cssAddFile">추가</button>&nbsp;<button type="button" class="buttonS bGray btnRremove">제거</button></div></div>');
}



</script>



<?php
include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">권리구제내역 입력</h2>
				<span class="location">홈 > 권리구제내역 관리 > <em>권리구제내역 입력</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post">
				<input type="hidden" name="kind" value="<?php echo $res['kind']; ?>"></input>
				<input type="hidden" name="oper_id" id="oper_id" value="<?php echo $edit['oper_id']; ?>"></input>
				<input type="hidden" name="seq" id="seq" value="<?php echo $edit['seq']; ?>"></input>
				<h3 class="sub_stit">
					신청정보
					<div class="marginT10 textR" style="margin-top:-18px;">
						<button type="button" class="buttonM bSteelBlue cssPrint">인쇄</button>
						<button type="button" class="buttonM bOrange cssAdd">등록</button>
						<button type="button" class="buttonM bGray cssBack">뒤로이동</button>
						<button type="button" class="buttonM bDarkGray cssList">목록</button>
					</div>
				</h3>
				<table class="tInsert">
					<caption>
						신청정보 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>접수번호 *</th>
						<td>
							<input type="text" id="lh_code" name="lh_code" maxlength="9" value="<?php echo $edit['lh_code'];?>" style="width:80px"> * 9자리
						</td>
					</tr>
					<tr>
						<th>지원종류 *</th>
						<td>
						<div style="height:30px;padding-top:10px;">
						<?php
							$index = 0; //$index_etc = 0;
							// $etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['code_sprt_kind'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								// if($item->code_name == '기타') {
								// 	$etc_item['index'] = $index;
								// 	$etc_item['item'] = $item;
								// 	$index_etc = $index;
								// }
								// else {
									echo '<input type="radio" name="sprt_kind_cd" class="imgM" id="sprt_kind_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['sprt_kind_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="sprt_kind_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								// }
								$index++;
							}
							// 기타 항목
							// echo '<input type="radio" name="sprt_kind_cd" class="imgM" id="sprt_kind_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['sprt_kind_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="sprt_kind_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?></div>
						<input type="text" name="sprt_kind_cd_etc" id="sprt_kind_cd_etc" class="imgM" size="70" maxlength="100" value="<?php echo $edit['sprt_kind_cd_etc'];?>"> * 최대 100자
						</td>
					</tr>
					<tr>
						<th>지원승인일 *</th>
						<td>
							<input type="text" id="lh_sprt_cfm_date" name="lh_sprt_cfm_date" class="datepicker date" value="<?php echo $edit['lh_sprt_cfm_date'];?>" style="width:80px">
						</td>
					</tr>
					<tr>
						<th>신청자 *</th>
						<td>
							<input type="text" name="lh_apply_nm" id="lh_apply_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_apply_nm'];?>"> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>거주지 *</th>
						<td>
							<input type="text" name="lh_apply_addr" id="lh_apply_addr" class="imgM" size="40" maxlength="70" value="<?php echo $edit['lh_apply_addr'];?>">  * 최대 70자
						</td>										
					</tr>
					<tr>
						<th>회사 *</th>
						<td>
							<input type="text" name="lh_apply_comp_nm" id="lh_apply_comp_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_apply_comp_nm'];?>"> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>회사소재지 *</th>
						<td>
							<input type="text" name="lh_comp_addr" id="lh_comp_addr" class="imgM" size="40" maxlength="70" value="<?php echo $edit['lh_comp_addr'];?>"> * 최대 70자
						</td>
					</tr>
					<tr>
						<th>신청기관 *</th>
						<td>
						<?php
							$index = 0; //$index_etc = 0;
							// $etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['code_apply_organ'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								// if($item->code_name == '기타') {
								// 	$etc_item['index'] = $index;
								// 	$etc_item['item'] = $item;
								// 	$index_etc = $index;
								// }
								// else {
									echo '<input type="radio" name="apply_organ_cd" class="imgM" id="apply_organ_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['apply_organ_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="apply_organ_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								// }
								$index++;
							}
							// 기타 항목
							// echo '<input type="radio" name="apply_organ_cd" class="imgM" id="apply_organ_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['apply_organ_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="apply_organ_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
						</td>
					</tr>
					<tr>
						<th>대리인 *</th>
						<td><b>소속</b> : 
							<input type="text" name="lh_labor_asso_nm" id="lh_labor_asso_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_labor_asso_nm'];?>"> * 최대 30자&nbsp;&nbsp;
							<b>이름</b> : 
							<input type="text" name="lh_labor_nm" id="lh_labor_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_labor_nm'];?>"> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>대상기관(관할) *</th> <!-- 대상기관(관할) 5개중 하나만 선택, 추가 입력박스도 마찮가지 -->
						<td>
						<div style="height:30px;padding-top:10px;">
						<?php
							$index = 0;
							foreach($res['code_sprt_organ'] as $item) {
								echo '<input type="radio" name="sprt_organ_cd" class="imgM" id="sprt_organ_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['sprt_organ_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="sprt_organ_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'. PHP_EOL;
								$index++;
							}
						?></div>
						<input type="text" name="sprt_organ_cd_etc" id="sprt_organ_cd_etc" class="imgM" size="70" maxlength="100" value="<?php echo $edit['sprt_organ_cd_etc'];?>"> * 최대 100자
						</td>
					</tr>
				</table>
				<h3 class="sub_stit">절차 진행 상황</h3>
				<table class="tInsert">
					<caption>
						절차 진행 상황 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>사건접수일</th>
						<td>
							<input type="text" id="lh_accept_date" name="lh_accept_date" class="datepicker date" value="<?php echo $edit['lh_accept_date'];?>" style="width:80px">
						</td>
					</tr>
					<tr>
						<th>출석조사일</th>
						<td>
							<input type="text" id="lid_date" name="lid_date" class="datepicker date" value="<?php echo $edit['lid_date'];?>" style="width:80px">
							<div id="btnAddLidDateContainer" style="display:none;margin:-30px 0 0 100px;">
								<input type="button" class="buttonS bGray" id="btnAddLidDate" value="추가">
							</div> <span id="lib_desc">* 추가버튼은 수정할때만 활성화됩니다.</span>
							<div style="margin-top:10px;" id="lidDataContainer">
							<?php
								foreach($edit['lid_date_data'] as $item) {
									echo '<span style="width:100px;">'. $item->lid_date .'<input type="button" class="buttonS bGray cssRemoveLidDate" value="삭제" data-lid_seq="'. $item->lid_seq .'" style="padding:2px 4px;margin:0 10px 0 3px;"></span>'. PHP_EOL; 
								}
							?>
							</div>
						</td>						
					</tr>
					<tr>
						<th>사건종결일</th>
						<td>
							<input type="text" id="lh_case_end_date" name="lh_case_end_date" class="datepicker date" value="<?php echo $edit['lh_case_end_date'];?>" style="width:80px">
						</td>
					</tr>
				</table>
				<h3 class="sub_stit">지원내용 및 결과</h3>
				<table class="tInsert">
					<caption>
						지원내용 및 결과 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>권리구제지원내용 *</th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="lh_sprt_content" id="lh_sprt_content"><?php echo $edit['lh_sprt_content'];?></textarea></td>
					</tr>
					<tr>
						<th>지원 결과</th>
						<td>
						<div style="height:30px;padding-top:10px;">
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['code_apply_rst'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								if($item->code_name == '기타') {
									$etc_item['index'] = $index;
									$etc_item['item'] = $item;
									$index_etc = $index;
								}
								else {
									echo '<input type="radio" name="apply_rst_cd" class="imgM" id="apply_rst_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['apply_rst_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="apply_rst_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								}
								$index++;
							}
							// 기타 항목
							echo '<input type="radio" name="apply_rst_cd" class="imgM" id="apply_rst_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['apply_rst_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="apply_rst_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?></div>
						<input type="text" name="apply_rst_cd_etc" id="apply_rst_cd_etc" class="imgM" size="70" maxlength="100" value="<?php echo $edit['apply_rst_cd_etc'];?>"> * 최대 100자
						</td>
					</tr>
					<tr>
						<th>특이사항</th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="lh_etc" id="lh_etc"><?php echo $edit['lh_etc'];?></textarea></td>
					</tr>
					<!--
					<tr>
						<th>첨부파일</th>
						<td>
							<div id="info_files">* 최대 <?//php echo CFG_UPLOAD_MAX_COUNT;?>개, 개당 <?//php echo (CFG_UPLOAD_MAX_FILE_SIZE/1024768);?>MB까지 업로드 가능합니다.</div>
							<div id="file_wrapper">
							<?//php
								// $file_name = $edit['file_name'];
								// $file_name_org = $edit['file_name_org'];
								// if(strpos($file_name_org, CFG_UPLOAD_FILE_NAME_DELIMITER) !== FALSE) {
								// 	$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name_org);
								// 	$arr_file_name_real = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name);
								// }
								// else {
								// 	$arr_file_name_real = array($file_name);
								// 	$arr_file_name_org = array('file_name_org'=>$file_name_org);
								// }

								// $file_index = 0;

								// if($res['kind'] == 'view'):
								// else:
								
								// 	foreach($arr_file_name_org as $file_name) {
							?>
								<div class="floatC" style="line-height:35px;margin-top:3px;" data-role-index="<?//php echo $file_index;?>">
									<div class="floatL cssFileForm">
										<input type="hidden" id="filename_real_<?//php echo $file_index;?>" value="<?//php echo $arr_file_name_real[$file_index]; ?>"></hidden>
										<input type="text" id="filename_<?//php echo $file_index;?>" class="floatL" readonly="readonly" value="<?//php echo $file_name; ?>" style="width:280px;background:none;border:none;">
										<div class="file_up" title="<?//php echo $file_name; ?>">
											<button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button>
											<input type="file" id="userfile_<?//php echo $file_index;?>" name="userfile_<?//php echo $file_index;?>" style="width:100%" onchange="javascript:chg_files(this, 'filename_<?//php echo $file_index;?>');">
										</div>
									</div>
									<div class="floatL marginL05" style="margin-top:-3px;">
										<button type="button" class="buttonS bGray cssAddFile">추가</button> 
										<?//php
										// if($res['kind'] == 'edit') {
										// 	echo('<button type="button" class="buttonS bGray cssDelFile" '. ($arr_file_name_real[$file_index] == '' ? 'disabled="disabled"' : '') .'>파일삭제</button>'. PHP_EOL);
										// 	echo('<button type="button" class="buttonS bGray cssDownload" data-role-real="'. $arr_file_name_real[$file_index] .'" data-role-org="'. $file_name .'" '. ($arr_file_name_real[$file_index] == '' ? 'disabled="disabled"' : '') .'>다운로드</button>');
										// }
										?>
									</div>
								</div>
							<?//php
								// 		$file_index++;
								// 	}

								// endif;
							?>
							</div>
						</td>
					</tr>
					-->
				</table>

				<div class="marginT10 textR">
					<a href="#" id="focusOuter"></a>
					<button type="button" class="buttonM bSteelBlue cssPrint">인쇄</button>
					<button type="button" class="buttonM bOrange cssAdd">등록</button>
					<button type="button" class="buttonM bGray cssBack">뒤로이동</button>
					<button type="button" class="buttonM bDarkGray cssList">목록</button>
				</div>
				</form>

			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
<?php
include_once './inc/inc_footer.php';
?>
