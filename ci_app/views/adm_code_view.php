<?php
include_once "./inc/inc_header.php";
?>

<script>
// 선택한 대코드 이름
var sel_code_name = '';

$(document).ready(function(){
	
	// 새하위코드 등록 버튼 이벤트 핸들러
	$('#btnAdd').click(function(e){
		var url = '/?c=code&m=add_view&code='+ $('#m_code').val();
		var callback = function(){
			get_list(_page);
		};
		gLayerId = openLayerModalPopup(url, 770, 450, '', '', '', callback, 1, true);
	});

	/*
	// 상위코드 설명글 등록버튼
	$('#btnAddDesc').click(function(e){
		var url = '/?c=code&m=edit_view&ref=csl_mcode&code='+ $('#m_code').val();
		var callback = function(){
			get_list(_page);
		};
		gLayerId = openLayerModalPopup(url, 770, 450, '', '', '', callback, 1, true);
	});
	*/
	
	// 전송버튼
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		get_list(_page);
	});
	
	// 사용.사용중지, 삭제 버튼 이벤트 핸들러
	$('#btnUse,#btnNotUse,#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var s_code = '';
			$('input[name=chk_item]:checked').each(function(i){
				if(s_code != '') s_code +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				s_code += $(this).attr('data-role-id');
			});
			
			var kind = 'using';
			if($(this).text() == '중지') {
				kind = 'not_using';
			}
			else if($(this).text() == '삭제') {
				kind = 'del_multi';
				if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
					return false;
				}
			}
			req_code_manage(kind, s_code);
		}
	});
	
	// 검색 - 검색어 엔터 처리
	$("#txtKeyword").keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$("#btnSubmit").click();
		}
	});
	
	// 대코드 이름 노출
	$('a.cls_menu').each(function(index){
		if($(this).css('font-weight') == '700') { // bold
			sel_code_name = $(this).text();
			if(sel_code_name) {
				sel_code_name = sel_code_name.split(' ')[1];
				$('#code_text').text(sel_code_name);
// 				$('#btnAddDesc').text(sel_code_name + ' 설명글등록');//설명글등록 버튼 text에 대코드명 추가
				return false;
			}
		}
	});
	
	// 상담내역공유 코드는 안내글 보여준다.
	if($('input[name=m_code]').val() == "<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_SHARE_YN);?>") {
		$('#info_label').css('display', 'inline');
		// 등록,수정,삭제 버튼 비활성화
		$('#btnDelete').prop('disabled', true);
		$('#btnAdd').prop('disabled', true);
		$('button.ai_cmd').prop('disabled', true);
	}

	// 관리버튼 클릭 이벤트
	$('#list').on('click', 'button.ai_cmd', function(e){
		e.preventDefault();
		var s_code = $(this).attr('data-role-id');
		var cmd = $(this).text();
		if(cmd == '삭제') {
			if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				req_code_manage('del', s_code);
			}
		}
		else {// 수정
			var url = '/?c=code&m=edit_view&code='+ s_code;
			var callback = function(){
				get_list(_page);
			};
			gLayerId = openLayerModalPopup(url, 770, 450, '', '', '', callback, 1, true);
		}
	});
	
	// 체크박스 전체 선택
	$('#list').on('click', 'input.checkAll', function(e){
		var status = $(this).prop('checked');
		$("input[name=chk_item]:checkbox").each(function(i) {
			$(this).prop("checked", status);
		});
	});

	get_list(_page);
	
});


/**
 * 보기/수정/삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, code) {
	
	var url = '/?c=code&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&s_code='+ code;
	var fn_succes = function(data) {
		// 삭제, 다중삭제 시
		if(type == 'del' || type == 'del_multi') {
			if(data.used_code == 'y') {//삭제할 코드가 상담에 사용중이면
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_07']);
			}
		}
		else {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
		}
		// set config total count
		_cfg_pagination.total_item = data.tot_cnt;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// list
		gen_list(data);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
	
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_empty_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// page
	_page = page;
	
	var url = '/?c=code&m=code_list';
	var rsc = $('#frmSearch').serialize() +'&page='+_page;
	var fn_succes = function(data) {
		// set config total count
		_cfg_pagination.total_item = data.tot_cnt;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// list
		gen_list(data);

		// 좌측 코드목록 높이 조절
		setTimeout(function(){
		 	var h = $('#contents_body').outerHeight(true) + $('#lnb').offset().top - $('#lnb06_menu01').offset().top; 
		 	$('#lnb06_menu01').height(h);
		}, 1);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		gen_empty_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var html_b = '<table class="tList">';
	html_b += '<caption>코드관리 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:6%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:20%">';
	html_b += '<col style="width:10%">';
	html_b += '<col style="width:14%">';
	html_b += '<col style="width:14%">';
	html_b += '<col style="width:15%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th><input type="checkbox" class="imgM checkAll"></th>';
	html_b += '<th>번호</th>';
	html_b += '<th>순서</th>';
	html_b += '<th>고유코드</th>';
	html_b += '<th>구분</th>';
	html_b += '<th>사용여부</th>';
	html_b += '<th>설명글사용여부</th>';
	html_b += '<th>등록일</th>';
	html_b += '<th>관리</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			var d = data.data[index];
			html_b += '<tr>';
			html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ Base64.encode(d.s_code) +'"></td>';
			html_b += '  <td>'+ (no--) +'</td>';
			html_b += '  <td>'+ d.dsp_order +'</td>';
			html_b += '  <td>'+ d.dsp_code +'</td>';
			html_b += '  <td>'+ d.code_name +'</td>';
			var use_yn = '<span class="labelG">중지</span>';
			if(d.use_yn == 1) {
				use_yn = '<span class="labelB">사용</span>';
			}
			html_b += '  <td>'+ use_yn +'</td>';
			use_yn = '<span class="labelG">중지</span>';
			if(d.code_desc_yn == 'Y') {
				use_yn = '<span class="labelB">사용</span>';
			}
			else {
				use_yn = '<span class="labelG">중지</span>';
				if(d.code_desc == '') {
					use_yn = '<span class="labelDg">미등록</span>';
				}
			}
			html_b += '  <td>'+ use_yn +'</td>';
			html_b += '  <td>'+ d.reg_date +'</td>';	
			html_b += '  <td>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(d.s_code) +'">수정</button>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(d.s_code) +'">삭제</button>';
			html_b += '  </td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="9" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
}

</script>

<?php
include_once './inc/inc_menu.php';
?>
		
		
		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">코드관리</h2>
				<span class="location">홈 > 코드관리 > <em id="code_text"></em></span>
			</div>
			<div class="cont_area">
				<form name="frmSearch" id="frmSearch" method="post">
				<div class="con_text">	
					<label for="board_select" class="l_title">구분</label>
					<input type="text" id="txtKeyword" name="keyword" style="width:60%">
					<input type="hidden" name="m_code" id="m_code" value="<?php echo $data['m_code']; ?>">
					<span class="divice"></span>
					<label for="board_rows" class="l_title">사용여부</label>
					<span style="padding:8px 0;display:inline-block">
						<input type="radio" name="rdoUse" id="rdoUse0" value="all" class="imgM" checked><label for="rdoUse0">전체</label>
						<input type="radio" name="rdoUse" id="rdoUse1" value="1" class="imgM"><label for="rdoUse1">사용</label>
						<input type="radio" name="rdoUse" id="rdoUse2" value="2" class="imgM"><label for="rdoUse2">사용중지</label>
					</span>
					<span class="divice"></span>
					<div class="textR marginT10">
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>
				
				<div class="list_no"></div>	
				
				<!-- list -->
				<div id="list"></div>
		
				<div id="info_label" style="display:none;color:red;font-weight:bold;line-height:40px;">상담내역공유 코드는 코드값을 고정하여 사용하기 때문에 등록/수정/삭제시 문제가 발생되어 기능을 제한합니다.</div>

				<div class="floatC marginT10">
					<button type="button" class="buttonM bGray floatL" id="btnUse">사용</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnNotUse">중지</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>
					<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">새코드등록</button>
					<!-- 필요없다하여 주석처리 
					<button type="button" class="buttonM bDarkGray floatR" id="btnAddDesc" style="margin-right:3px;">설명글등록</button>
					-->
				</div>

				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>
				
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		
		
<?php
include_once './inc/inc_footer.php';
?>
