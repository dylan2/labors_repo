<?php
include_once "./inc/inc_header.php";
?>

<script>
var grp_id = '<?php echo $res['oper_auth_grp_id']; ?>';

var kind = '<?php echo $res['kind']; ?>';
if(!kind) kind = 'add';

$(document).ready(function(){
	if(kind && kind == 'view') {
		$('#btnSubmit').css('width', 0).css('display', 'none');
		if(kind != 'add') {
			$('#idChkAll, #lblChkAll').css('width', 0).css('display', 'none');
		}
	}
	
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		$('input:checkbox').not(this).prop('checked', this.checked);
		// 기본권한은 항상 체크
		$('input:checkbox').each(function(){
			if($(this).next().text().indexOf('상담내역조회') != -1 || $(this).next().text().indexOf('상담내역입력') != -1 ) {
				$(this).prop('checked', 'checked');
			}
		});
	});
	
	// 목록버튼
	$('#btnList').click(function(e){
		e.preventDefault();
		check_auth_redirect($('#'+ sel_menu).find('li>a').first());
	});
	
	// 등록버튼
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		// validation
		if($('#txtAuthGrpName').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_04']);
			$('#txtAuthGrpName').focus();
			return false;
		}
		// 체크 갯수 검사
		if($('input[type=checkbox]:checked').length == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_03']);
			$('#chkCounselView').focus();
			return false;
		}
		
		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
			// checkbox를 검사해서 unchecked된 녀석들은 checked 상태로 바꾼뒤 가비지값을 넣는다.
			var chkVal = '';
			$('input[type=checkbox]').each(function(i){
				if($(this).attr('id') != 'idChkAll') {
					if(chkVal != '') chkVal += '<?php echo CFG_AUTH_CODE_DELIMITER; ?>';
					if($(this).prop('checked')) {
						chkVal += $(this).attr('data-role-code');
					}
					else {
						chkVal += '|';
					}
				}
			});
			
			// request
			$.ajax({
				url: '/?c=auth&m='+ kind
				,data: {grp_id: grp_id, grp_nm: $('#txtAuthGrpName').val(), grp_etc: $('#txtEtc').val(), grp_code: chkVal}
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					if(kind != 'view') {
						alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
						alert(CFG_MSG[CFG_LOCALE]['info_auth_06']);
					}
					check_auth_redirect($('#'+ sel_menu).find('li>a').first());
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
					
					var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
					if(data && data.msg) msg += '[' + data.msg +']';
					alert(msg);
				}
			});
		}
	});
});
</script>


<?php
include_once './inc/inc_menu.php';
?>

		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">권한관리</h2>
				<span class="location">홈 > 권한관리 > <em>권한등록</em></span>
			</div>
			<form name="frmForm" id="frmForm" method="post">
			<div class="cont_area">
				<span class="sub_stit" style="font-weight:bold; line-height:40px;color:#f37000">권한이 변경된 사용자는 다시 "로그인" 하여야 적용됩니다.</span>
				<table class="tInsert">
					<caption>
						권한등록 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>권한그룹</th>
						<td><input type="text" name="txtAuthGrpName" id="txtAuthGrpName" value="<?php echo $res['oper_auth_name']; ?>" style="width:50%"></td>
					</tr>
					<tr>
						<th>메모</th>
						<td><textarea class="marginT05" name="txtEtc" id="txtEtc" style="width:90%"><?php echo $res['etc']; ?></textarea></td>
					</tr>
				</table>
				<h3 class="sub_stit"></h3>
				<span class="sub_stit" style="font-weight:bold; line-height:40px;">권한설정</span> &nbsp; <span id="spanChkAll"><input type="checkbox" class="checkAll" id="idChkAll"><label for="idChkAll" id="lblChkAll">전체선택/해제</label></span>
				<table class="tInsert">
					<caption>
						권한등록 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<?php
// 					echof($res['base_code'] );
					$user_auth_code = join(',', $res['arr_auth_code']);
					// exit;
					$index = 0;
					$curr_m_code = '';
					foreach($res['base_code'] as $item) {
						if($curr_m_code == '' || $curr_m_code != $item->m_code) {
							if($curr_m_code != $item->m_code) {
								echo '</td></tr>'.PHP_EOL;
							}
							echo '<tr><th>'. $item->mcode_name .'</th><td> ';
						}
						$enc_code = base64_encode($item->s_code);
						echo '<input type="checkbox" name="chkItem_'. $enc_code .'" id="chkItem_'. $enc_code .'"  class="imgM" data-role-code="'. $enc_code .'" ';
						// 할당된 auth_code가 있는지 체크
						// - 기존엔 할당된 코드위치에 따라 권한 유무를 체크했으나 할당된코드중 코드가 있는지 확인하는 방식으로 변경함. dylan 2019.08.05
						if(strpos($user_auth_code, $item->s_code) !== FALSE || 
							($item->scode_name == '상담내역조회' || $item->scode_name == '상담내역입력')) {// -- 기본권한
							echo 'checked';
							// 기본 권한이 통계 -> 상담입력 으로 변경됨
							// 입력이 기본권한이기 때문에 입력후 목록으로 이동할 수 있도록 목록도 기본권한으로 변경할 수 없도록 한다.
							if($item->scode_name == '상담내역조회' || $item->scode_name == '상담내역입력' ) {
								echo ' disabled';
							}
						}
						echo '><label for="chkItem_'. $enc_code .'">'. $item->scode_name .'</label> '.PHP_EOL;
						// 설명 문구
						if($item->scode_name == '상담내역조회' || $item->scode_name == '상담내역입력') {
							echo '<span>(공통권한)</span>'.PHP_EOL;
						}

						// # 예외코드 추가 
						// - 통계>상담내역통계>상담사례>상담내역 보기 버튼 권한 추가 - 다른 상담보는 권한과 무관하다. 오직 통계>상담내역 보기 버튼에 대한 권한만 임! 
						// - 이 버튼이 생긴 이유는 "서울시" 소속 공무원들 때문이라고 한다. 서울시 공무원을 통계만 보도록 해야 한다고 한다. 상담내역은 안됨(물론 권한주면 소속상담은 볼수 있겠지)
						if($item->m_code == CFG_MASTER_CODE_COUNSEL_STATISTIC) {
							$exp_code = base64_encode(CFG_EXCEPTION_CODE_COUNSEL_STATISTIC_VIEW_AUTH);
							echo '<input type="checkbox" name="chkItem_'. $exp_code .'" id="chkItem_'. $exp_code .'"  class="imgM" data-role-code="'. $exp_code .'" ';
							if(strpos($user_auth_code, CFG_EXCEPTION_CODE_COUNSEL_STATISTIC_VIEW_AUTH) !== FALSE) {
								echo 'checked';
							}
							echo '><label for="chkItem_'. $exp_code .'">'. CFG_EXCEPTION_CODE_NAME_COUNSEL_STATISTIC_VIEW_AUTH .'</label> '.PHP_EOL;
						}
						// - 통계>사용자상담통계>상담사례>상담내역 보기 버튼 권한 추가 - 다른 상담보는 권한과 무관하다. 오직 통계>상담내역 보기 버튼에 대한 권한만 임! 
						if($item->m_code == CFG_MASTER_CODE_BIZ_COUNSEL_STATISTIC) {
							$exp_code = base64_encode(CFG_EXCEPTION_CODE_BIZ_COUNSEL_STATISTIC_VIEW_AUTH);
							echo '<input type="checkbox" name="chkItem_'. $exp_code .'" id="chkItem_'. $exp_code .'"  class="imgM" data-role-code="'. $exp_code .'" ';
							if(strpos($user_auth_code, CFG_EXCEPTION_CODE_BIZ_COUNSEL_STATISTIC_VIEW_AUTH) !== FALSE) {
								echo 'checked';
							}
							echo '><label for="chkItem_'. $exp_code .'">'. CFG_EXCEPTION_CODE_NAME_BIZ_COUNSEL_STATISTIC_VIEW_AUTH .'</label> '.PHP_EOL;
						}

						$curr_m_code = $item->m_code;
						$index++;
					}
					// - 권리구제지원 관리
					if($item->m_code == CFG_MASTER_CODE_LAW_HELP) {
						// -- 권리구제내역 조회(내용보기만 가능, 등록,수정 안됨)
						$exp_code = base64_encode(CFG_EXCEPTION_CODE_LAW_HELP_CONTENT_VIEW_AUTH);
						echo '<input type="checkbox" name="chkItem_'. $exp_code .'" id="chkItem_'. $exp_code .'"  class="imgM" data-role-code="'. $exp_code .'" ';
						if(isset($res['arr_auth_code'][$index]) && $res['arr_auth_code'][$index] != '' && $res['arr_auth_code'][$index] != '|') {
							echo 'checked';
						}
						echo '><label for="chkItem_'. $exp_code .'">'. CFG_EXCEPTION_CODE_LAW_HELP_CONTENT_VIEW_AUTH_NM .'</label> '.PHP_EOL;

						// -- 권리구제내역 조회(내용보기만 가능, 등록,수정 안됨)
						$exp_code = base64_encode(CFG_EXCEPTION_CODE_LAW_HELP_LIST_EXCEL_DOWNLOAD_AUTH);
						echo '<input type="checkbox" name="chkItem_'. $exp_code .'" id="chkItem_'. $exp_code .'"  class="imgM" data-role-code="'. $exp_code .'" ';
						if(isset($res['arr_auth_code'][$index+1]) && $res['arr_auth_code'][$index+1] != '' && $res['arr_auth_code'][$index+1] != '|') {
							echo 'checked';
						}
						echo '><label for="chkItem_'. $exp_code .'">'. CFG_EXCEPTION_CODE_LAW_HELP_LIST_EXCEL_DOWNLOAD_AUTH_NM .'</label> '.PHP_EOL;
						echo '<div style="margin-top:5px;">* "권리구제결과 조회" 권한 부여시 "권리구제내역 조회" 권한도 같이 부여해야 조회할 수 있습니다.</div>'.PHP_EOL;
						echo '<div style="margin-top:5px;">* 권리구제내역 조회 : 목록조회, 권리구제결과 조회 : 상세보기</div>'.PHP_EOL;
					}
					?>
				</table>
				<div class="marginT10 textR">
					<button type="button" id="btnSubmit" class="buttonM bSteelBlue">등록</button>
					<button type="button" id="btnList" class="buttonM bGray">목록</button>
				</div>
			</div>
			</form>
			<!-- //컨텐츠 -->
		</div>	
		<!-- //  contents_body  area -->
		
<?php
include_once './inc/inc_footer.php';
?>
