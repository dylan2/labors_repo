<?php
/**
 * 
 * 상담내역 - 임금근로시간통계 
 * 
 */
include_once "./inc/inc_header.php";
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="./js/counsel/common.js"></script>
<script type="text/javascript">
var chart_mode = 1;
$(document).ready(function() {

	//차트 보기
	$('#view_chart').click(function(e) {
		e.preventDefault();
		// 교차통계는 차트 지원 안함
// 		if($('#csl_code').val() != '') {
// 			alert(CFG_MSG[CFG_LOCALE]['info_sttc_04']);
// 			return;
// 		}
		if (chart_mode == 0) { //보기->숨기기
			$('.chart').css('display', 'none');
			$('#view_chart').text('통계차트 보기');
			$('#downchart').css('display', 'none');
			$('#printchart').css('display', 'none');
			chart_mode = 1;
		} 
		else { //숨기기->보기
			if(changed_code == true) {
				alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
				return;
			}
    	if($('#csl_code').val() == '') {//교차차트 아닌 경우 차트1개만 사용
      	$('#column_chart').css('display', 'block');
      	$('#donut_chart').css('display', 'none');
      	$('#donut_chart').hide();
      	$('#list').css('margin-top', '19px'); //donut_chart 숨기면서 가려지는 문제 처리
    	}
    	else {
      	$('.chart').css('display', 'block');
    	}
			$('#view_chart').text('통계차트 숨기기');
			$('#downchart').css('display', 'inline');
			$('#printchart').css('display', 'inline');
			$('#loading').show();
			var position = $('#downchart').offset();
			$('html').animate({ scrollTop: position.top }, 1000, function() {
				// 처음 가져온 데이터를 사용한다. 굳이 다시 가져올 이유도 없고, table데이터와 달라질 수 있기 때문에 다시 가져오는게 잘못된 결과를 보여줄 수 있다. 
				column_chart(sttt_data);
				chart_mode = 0;
				$('#loading').hide();
			});
		}
	});

	//위로 이동
	$('#view_top').click(function(e) {
		e.preventDefault();
		var position = $('#contents_body').offset();
		$('html, body').animate({ scrollTop: position.top }, 1000);
	});

	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#s_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=s_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e) {
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#csl_proc_rst').val(data.s_code);
				}
				if (data.code_name) {
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=csl_proc_rst_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e) {
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e) {
		e.preventDefault();
		var closedCallback = function(data) {
			if (data) {
				if (data.s_code) {
					$('#asso_code').val(data.s_code);
				}
				if (data.code_name) {
					$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=asso_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e) {
		e.preventDefault();
		$('#asso_code').val($('#asso_code_all').val());
		$('#asso_code_name').text("전체");
	});

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for (var i = year; i >= 2015; i--) {
		$("#sel_year").append("<option value=" + i + ">" + i + "년</option>");
	}
	$('#sel_year').change(function() {
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates('', 2);
		if ($(this).val() == '') {
			$('#btnAllTerm').click();
		}
	});

	$('#sel_month').change(function() {
		fill_list_search_dates($(this).val());
	});

	// "csl_code" select option 최초 생성
	$('#csl_code_options option').map(function(i, o) {
		if (i == 1) return; // 최초 성별 제외
		$('#csl_code').append($(o).clone());
	});

	// 통계유형을 바꾼뒤 검색버튼 클릭하지 않고 차트,표,엑셀다운 버튼 클릭 방지를 위한 플래그
	var changed_code = false;
	
	// 교차대상 코드
	$('select[name=csl_code]').change(function(){
		var d = $(this).find('option:selected').attr('data-idx');
		$('input[name=csl_code_idx]').val( d );//통계에서 사용하는 데이터
		changed_code = true;
	});

	//상담방법 선택에 따른 교차유형 select option 세팅
	$('#sel_col').change(function() {
		$('#csl_code').empty(); //option 제거
		var sel_val = $('#sel_col option:selected').val() != '' ? +$('#sel_col option:selected').val() : '';
		$('#csl_code_options option').map(function(i, o) {
			var csl_cd_val = $(this).attr('data-idx') != '' ? +$(this).attr('data-idx') : '';
			if (csl_cd_val === sel_val) return; // 선택한 유형 제외
			$('#csl_code').append($(o).clone());
		});
		// 교차코드 index 초기화
		$('input[name=csl_code_idx]').val('');//통계에서 사용하는 데이터
		changed_code = true;
	});

	//통계탭 이동
	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this) + 1;

		if(index == 2) index = 13;
		else if(index == 3) index = 11;
		else if(index == 6) index = 12;
		
		var begin = $('#search_date_begin').val();
		var end = $('#search_date_end').val();
		var sch_asso_code = $("#asso_code").val();
		var sch_s_code = $("#s_code").val();
		var sch_csl_proc_rst = $("#csl_proc_rst").val();

		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});

	// print a list
	$('#printlist').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		var txt_sel_col = $('#sel_col option:selected').text();
		var txt_csl_code = $('#csl_code').val() == "" ? "" : " : "+ $('#csl_code option:selected').text();
		var popupWindow = window.open("", "_blank");
		var $div = $('#list').clone();
		var title_sub = txt_csl_code == "" ? " 통계" : " 교차통계";
		$div.find('table').css('border-collapse', 'collapse').find('th, td').css({ 'border': 'solid 1px #eee', 'height': '30px' });
		$div.find('th').css('background-color', '#e8fcff');
		popupWindow.document.write("<body>");
		popupWindow.document.write("<h3>임금근로시간"+ title_sub +" - "+ txt_sel_col + txt_csl_code + "</h3>");
		popupWindow.document.write("<p>상담기간: "+ $("#search_date_begin").val() +"~"+ $("#search_date_end").val() +"</p>");
		popupWindow.document.write($div.html());
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("table").css("width", "100%");
		$(popupWindow.document).find("td").css("text-align", "center");
		popupWindow.print();
		popupWindow.close();
	});

	// print a chart images
	$('#printchart').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		var popupWindow = window.open("", "_blank");
		var div = $('#chart_div').html();
		popupWindow.document.write("<body>");
		popupWindow.document.write(div);
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("img").css("width", "100%");
		popupWindow.print();
		popupWindow.close();
	});

	// 차트 데이터 전송용 폼 추가
	var addChartForm = function(method) {
		if($('#imgForm').length == 0) {
			$("body").append('<form id="imgForm" method="POST" target="imgFrame" action="/?c=chart_download&m=download">'+
				'<input type="hidden" name="dataUrl" />'+
				'</form>');
		}
		$("#imgFrame").remove();
		$("body").append('<iframe name="imgFrame" id="imgFrame" style="display:none;"></iframe>');
	};

	// 차트 데이터 전송
	var capture = function(id){
		$('#chart_div').show();
		html2canvas(document.getElementById(id), {
			logging: false,
	    width: $("#"+ id +" .chart_div").eq(0).width(),
	    height: $("#"+ id).height(),
			onrendered: function(canvas) {
				var img = canvas.toDataURL();
				$('input[name="dataUrl"]').val(img);
				$("#imgForm").submit();
				$('#chart_div').hide();
				$('#loading').hide();
			}
		});
	};
	// 차트 다은로드
	$('#downchart').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		$('#loading').show();
		addChartForm();
		capture('chart_div');
	});

	
	// download a excel file
	$('#downExcel').click(function() {
		if(changed_code == true) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_05']);
			return;
		}
		if ($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var url = '/?c=statistic&m=open_down_view&kind=down04';
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
		// 방식변경: 데이터 처리가 복잡하여 화면에 그려진 테이블의 데이터만 추출하여 폼에 세팅, 이후 팝업에서 폼전송한 뒤 엑셀다운로드에서 이 데이터로 처리하기로 변경함
		// colspan, rowspan은 같은 데이터를 합쳐진 모든 셀에 채운다.
		if($('#csl_code option:selected').val() != '') {//교차통계인 경우만
			var str = '', str2 = '', dmt1 = '●', dmt2 = '|';
			$('table.tList02 tr').each(function(row,tr){// tr
				if(str != '') {
					str += dmt1;
				}
				str2 = '';
				$(tr).find('th,td').each(function(i,o){// td
					if(str2 != '') {
						str2 += dmt2;
					}
					// 헤더 두번째 tr 첫번째 th 제목
					if(row == 1 && i == 0) {
						str2 += '구분'+ dmt2;
					}
					if($(o).attr('colspan') != undefined) {
						var tmp = '';
						for(var j=0; j<+$(o).attr('colspan');j++) {
							if(tmp != '') {
								tmp += dmt2;
							}
							tmp += $(o).text();
						}
						str2 += tmp;
					}
					else {
						str2 += $(o).text();
					}
					// 헤더 두번째 tr 마지막 th 제목
					if(row == 1 && i == $(tr).find('th').length - 1) {
						str2 += dmt2 + str.split(dmt2)[str.split(dmt2).length-1].split(dmt1)[0];
					}
				});
				str += str2;
			});
			$('#frmSearch textarea[name=tableData]').remove();
			$('#frmSearch').append('<textarea name="tableData" style="display:none;"></textarea>');
			$('textarea[name=tableData]').val(str);
// 	 		console.log( $('textarea[name=tableData]').val() );
		}
	});

	// 검색
	$("#btnSubmit").click(function(e) {
		e.preventDefault();
		// 교차통계이고 차트보이기 상태면 숨김
		if($('#csl_code option:selected').val() != '') {
			$('.chart').css('display', 'none');
			$('#view_chart').text('통계차트 보기');
			$('#downchart').css('display', 'none');
			$('#printchart').css('display', 'none');
			chart_mode = 1;
		}
		//검색대상이 상담유형이면, 안내 레이블 출력
		if ($('#sel_col option:selected').val() == 11 || $('#csl_code option:selected').text() == '상담유형') {
			$('#infoLabel').css('display', $('#sel_col option:selected').val() == 11 || $('#csl_code option:selected').text() == '상담유형' ? 'inline' : 'none');
		}
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val($('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
		//
		get_list(_page);
		
		changed_code = false;
	});

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e) {
		var date_begin = '',
			date_end = get_today();
		if ($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		} else if ($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		} else if ($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		} else {
			date_begin = '2015-01-01';
		}
		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});

	//====================================================
	// for test
// 	$('#csl_code option').eq(1).prop('selected', true);

	// list
	get_list(_page, true);
});



function get_today() {
	var now = new Date();
	return now.getFullYear() + '-' + genTwoDigit(now.getMonth() + 1) + '-' + genTwoDigit(now.getDate());
}

//서버로부터 수신된 데이터
var sttt_data = [];

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page, is_first) {
	_page = page;

	var url = '/?c=statistic&m=st04';
	var rsc = $('#frmSearch').serialize() + '&not_seq=0';
	var fn_succes = function(data) {
		sttt_data = data;
		// 교차통계 대상코드를 선택하지 않았으면 단순임금통계 표출
		if($('#csl_code').val() == '') {
			gen_list(sttt_data);
		}
		else {// 교차통계
			gen_list2(sttt_data);
		}
		if (chart_mode == 0) { //차트 보기 상태일 때
			column_chart(sttt_data);
		}
		if (+sttt_data.dst_csl_tot == 0) {
			alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
		}
	};
	var fn_error = function(data) {
		if (is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * 단순 임금근로시간용
 */
function gen_list(data) {
	var sel_col = $('#sel_col').val();

	var html_b = '<table class="tList02" border=0>';
	html_b += '<colgroup>';
	html_b += '<col style="width:25%">';
	html_b += '<col style="width:25%">';
	html_b += '<col style="width:25%">';
	html_b += '<col style="width:25%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th style="line-height:15px;">구분</th>';
	html_b += '<th style="line-height:15px;">임금 (빈도)</th>';
	html_b += '<th style="line-height:15px;">근로시간 (빈도)</th>';
	html_b += '<th style="line-height:15px;">전체상담건수</th>';
	html_b += '</tr>';

	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = +data.tot_cnt;
		var dst_csl_tot = data.data[0].dst_csl_tot;
		var tot_pay = 0, tot_pcnt = 0, tot_work = 0, tot_wcnt = 0, tot_tg_cnt = 0;
		var dst_p_tot = "", dst_w_tot = "", txt_tot_desc = "", txt_dst_tot = '';
		for(var i=0; i<total_cnt; i++) {
			var d = data.data[i];
			tot_tg_cnt += +data.target1_tot[i].t_cnt;
			html_b += '<tr>';
			html_b += '  <td>'+ d.subject +'</td>';
			html_b += '  <td>'+ numberToCurrency(d.pay) +' ('+ numberToCurrency(d.p_cnt) +')</td>';
			html_b += '  <td>'+ numberToCurrency(d.works) +' ('+ numberToCurrency(d.w_cnt) +')</td>';
			html_b += '  <td>'+ numberToCurrency(data.target1_tot[i].t_cnt) +'</td>';
			html_b += '</tr>';
			tot_pay += +d.pay * +d.p_cnt;
			tot_work += +d.works * +d.w_cnt;
			tot_pcnt += +d.p_cnt;
			tot_wcnt += +d.w_cnt;
			dst_p_tot = $('#sel_col').val() == 11 ? d.dst_p_tot : "";
			dst_w_tot = $('#sel_col').val() == 11 ? d.dst_w_tot : "";
		}
		// 구분별 total
		if($('#sel_col').val() == 11) {//상담유형
			dst_p_tot = " ("+ numberToCurrency(dst_p_tot) +")";
			dst_w_tot = " ("+ numberToCurrency(dst_w_tot) +")";
			txt_tot_desc = "(실건수)";
			txt_dst_tot = " ("+ numberToCurrency(data.dst_csl_tot) +")";
		}
		html_b += '<tr>';
		html_b += '  <td style="background:#F1F1F1 !important;">전체 (빈도)'+ txt_tot_desc +'</td>';
		html_b += '  <td style="background:#F1F1F1 !important;">'+ numberToCurrency((tot_pay/tot_pcnt).toFixed(0)) +' ('+ numberToCurrency(tot_pcnt) +')'+ dst_p_tot +'</td>';
		html_b += '  <td style="background:#F1F1F1 !important;">'+ numberToCurrency((tot_work/tot_wcnt).toFixed(0)) +' ('+ numberToCurrency(tot_wcnt) +')'+ dst_w_tot +'</td>';
		html_b += '  <td style="background:#F1F1F1 !important;">'+ numberToCurrency(tot_tg_cnt) + txt_dst_tot +'</td>';
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="4" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';

	// list html
	$('#list').empty().html(html_b);
}


var sel_csl_cd_cnt = 0;//교차대상코드(가로축) 개수

/**
 * 교차통계 용
 */
function gen_list2(data) {
	var sel_csl_cd = $('#csl_code option:selected').attr('data-idx');
	var sel_col = $('#sel_col option:selected').val();
	
	var html_b = '<table class="tList02" border="0">';
	html_b += '<colgroup>';
	html_b += '<col style="width:*">';
	if(sel_csl_cd == 1){sel_csl_cd_cnt="<?php echo count($res['code_1']);?>";<?php foreach($res['code_1'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//성별
	else if(sel_csl_cd == 2){sel_csl_cd_cnt="<?php echo count($res['code_2']);?>";<?php foreach($res['code_2'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//연령대
	else if(sel_csl_cd == 3){sel_csl_cd_cnt="<?php echo count($res['code_3']);?>";<?php foreach($res['code_3'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//거주지
	else if(sel_csl_cd == 4){sel_csl_cd_cnt="<?php echo count($res['code_4']);?>";<?php foreach($res['code_4'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//회사소재지
	else if(sel_csl_cd == 5){sel_csl_cd_cnt="<?php echo count($res['code_5']);?>";<?php foreach($res['code_5'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//직종
	else if(sel_csl_cd == 6){sel_csl_cd_cnt="<?php echo count($res['code_6']);?>";<?php foreach($res['code_6'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//업종
	else if(sel_csl_cd == 7){sel_csl_cd_cnt="<?php echo count($res['code_7']);?>";<?php foreach($res['code_7'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//고용형태
	else if(sel_csl_cd == 8){sel_csl_cd_cnt="<?php echo count($res['code_8']);?>";<?php foreach($res['code_8'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//근로자수
	else if(sel_csl_cd == 9){sel_csl_cd_cnt="<?php echo count($res['code_9']);?>";<?php foreach($res['code_9'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//근로계약서
	else if(sel_csl_cd == 10){sel_csl_cd_cnt="<?php echo count($res['code_10']);?>";<?php foreach($res['code_10'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//4대보험
	else if(sel_csl_cd == 11){sel_csl_cd_cnt="<?php echo count($res['code_11']);?>";<?php foreach($res['code_11'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담유형
	else if(sel_csl_cd == 12){sel_csl_cd_cnt="<?php echo count($res['code_12']);?>";<?php foreach($res['code_12'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담동기
	else if(sel_csl_cd == 13){sel_csl_cd_cnt="<?php echo count($res['code_13']);?>";<?php foreach($res['code_13'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//소속
	else if(sel_csl_cd == 0){sel_csl_cd_cnt="<?php echo count($res['code_0']);?>";<?php foreach($res['code_0'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담방법
	html_b += '<col style="width:*">';
	html_b += '<col style="width:*">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th style="line-height:15px;border-right:1px solid #ddd;" rowspan="2">구분</th>';
	if(sel_csl_cd == 1){<?php foreach($res['code_1'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 2){<?php foreach($res['code_2'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 3){<?php foreach($res['code_3'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 4){<?php foreach($res['code_4'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 5){<?php foreach($res['code_5'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 6){<?php foreach($res['code_6'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 7){<?php foreach($res['code_7'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 8){<?php foreach($res['code_8'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 9){<?php foreach($res['code_9'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 10){<?php foreach($res['code_10'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 11){<?php foreach($res['code_11'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 12){<?php foreach($res['code_12'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 13){<?php foreach($res['code_13'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	else if(sel_csl_cd == 0){<?php foreach($res['code_0'] as $item) { echo "html_b += '<th colspan=\"2\" style=\"line-height:15px;border-right:1px solid #ddd;\">". $item->code_name ."</th>';"; }?>}
	html_b += '<th style="line-height:15px;border-right:1px solid #ddd;" colspan="2">전체</th>';
	html_b += '<th style="line-height:15px;" rowspan="2">전체상담건수'+ (sel_csl_cd == 11 ? "<br>(실건수)" : "") +'</th>';
	html_b += '</tr>';

	// 2번째 table header
	html_b += '<tr>';
	for(var j=0; j<sel_csl_cd_cnt; j++) {
		html_b += '<th style="line-height:15px;border-right:1px solid #ddd;">임금(빈도)</th>';
		html_b += '<th style="line-height:15px;border-right:1px solid #ddd;">근로시간(빈도)</th>';
	}
	html_b += '<th style="line-height:15px;border-right:1px solid #ddd;">임금(빈도)</th>';
	html_b += '<th style="line-height:15px;border-right:1px solid #ddd;">근로시간(빈도)</th>';
	html_b += '</tr>';

// 	console.log(data.target1_tot);
// 	console.log(data.target2_tot);
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = +data.tot_cnt;
		var dst_csl_tot = data.data[0].dst_csl_tot, idx = 1;
		var arr_pcnt_sum = [], arr_wcnt_sum = [];
		var sum_pay_tot = 0, sum_pcnt_tot = 0, sum_work_tot = 0, sum_wcnt_tot = 0;
		var sum_pay_tot_h = [], sum_pcnt_tot_h = [], sum_work_tot_h = [], sum_wcnt_tot_h = [];//가로축 코드별 합계용
		var txt_dst_tot = '', dt_tot = 0, dt_dst_tot = 0;
		for(var i=0; i<total_cnt; i++) {
			var sum_pay = 0, sum_pcnt = 0, sum_work = 0, sum_wcnt = 0, sum_csl_cnt = 0;
			var d = data.data[i];
			var dt = data.target1_tot[i];//전체상담건수(세로축)
			html_b += '<tr>';
			html_b += '  <td style="border-right:1px solid #ddd;">'+ d.subject +'</td>';
			for(var j=0; j<sel_csl_cd_cnt; j++) {
				var j2 = j+1;
				html_b += '  <td style="border-right:1px solid #ddd;">'+ numberToCurrency(d['pay'+ j2]) +' ('+ numberToCurrency(d['p_cnt'+ j2]) +')</td>';//임금(빈도--건수)
				html_b += '  <td style="border-right:1px solid #ddd;">'+ numberToCurrency(d['work'+ j2]) +' ('+ numberToCurrency(d['w_cnt'+ j2]) +')</td>';//근로시간(빈도--건수)
				sum_pay += +d['pay'+ j2] * +d['p_cnt'+ j2];
				sum_pcnt += +d['p_cnt'+ j2];
				sum_work += +d['work'+ j2] * +d['w_cnt'+ j2];
				sum_wcnt += +d['w_cnt'+ j2];
				// 총계용
				sum_pay_tot += +d['pay'+ j2] * +d['p_cnt'+ j2];
				sum_pcnt_tot += +d['p_cnt'+ j2];
				sum_work_tot += +d['work'+ j2] * +d['w_cnt'+ j2];
				sum_wcnt_tot += +d['w_cnt'+ j2];
				if( ! sum_pay_tot_h[j]) sum_pay_tot_h[j] = 0;
				if( ! sum_work_tot_h[j]) sum_work_tot_h[j] = 0;
				sum_pay_tot_h[j] += +d['pay'+ j2] * +d['p_cnt'+ j2];
				sum_work_tot_h[j] += +d['work'+ j2] * +d['w_cnt'+ j2];
				// 세로 상담건수 합계
				if( ! arr_pcnt_sum[j]) arr_pcnt_sum[j] = 0;
				if( ! arr_wcnt_sum[j]) arr_wcnt_sum[j] = 0;
				arr_pcnt_sum[j] += +d['p_cnt'+ j2];// 임금 
				arr_wcnt_sum[j] += +d['w_cnt'+ j2];// 근로시간
			}
			// 세로축 - 전체 - 임금,근로시간 합계
			html_b += '  <td style="border-right:1px solid #ddd;">'+ numberToCurrency((sum_pay/sum_pcnt).toFixed(0)) +' ('+ numberToCurrency(sum_pcnt) +')</td>';
			html_b += '  <td style="border-right:1px solid #ddd;">'+ numberToCurrency((sum_work/sum_wcnt).toFixed(0)) +' ('+ numberToCurrency(sum_wcnt) +')</td>';
			// 세로축 - 전체상담건수
			txt_dst_tot = '';
			if(sel_csl_cd == 11) { //상담유형
				txt_dst_tot = ' ('+ numberToCurrency(data.dst_tot[i].cnt) +')';
				dt_dst_tot += +data.dst_tot[i].cnt;
			}
			html_b += '  <td>'+ numberToCurrency(dt['t_cnt']) + txt_dst_tot +'</td>';
			html_b += '</tr>';
			dt_tot += +dt['t_cnt'];
			idx++;
		}
		// 가로축 - 전체(빈도) - 임금근로시간
		html_b += '<tr>';
		html_b += '  <td style="background:#F1F1F1 !important;">전체(빈도)</td>';
		for(var j=0; j<sel_csl_cd_cnt; j++) {
			html_b += '  <td style="background:#F1F1F1 !important; style="border-right:1px solid #ddd;">'+ numberToCurrency((sum_pay_tot_h[j]/arr_pcnt_sum[j]).toFixed(0)) +' ('+ numberToCurrency(arr_pcnt_sum[j]) +')</td>';
			html_b += '  <td style="background:#F1F1F1 !important; style="border-right:1px solid #ddd;">'+ numberToCurrency((sum_work_tot_h[j]/arr_wcnt_sum[j]).toFixed(0)) +' ('+ numberToCurrency(arr_wcnt_sum[j]) +')</td>';
		}
		html_b += '  <td style="background:#F1F1F1 !important; style="border-right:1px solid #ddd;">'+ numberToCurrency((sum_pay_tot/sum_pcnt_tot).toFixed()) +' ('+ numberToCurrency(sum_pcnt_tot) +')</td>';
		html_b += '  <td style="background:#F1F1F1 !important; style="border-right:1px solid #ddd;">'+ numberToCurrency((sum_work_tot/sum_wcnt_tot).toFixed()) +' ('+ numberToCurrency(sum_wcnt_tot) +')</td>';
		html_b += '  <td style="background:#F1F1F1 !important;">&nbsp;</td>';
		html_b += '</tr>';
		// 전체상담건수 (교차대상별 총상담건수-임금,근로시가 입력 무관한 전체건수)
		html_b += '<tr>';
		html_b += '  <td style="background:#F1F1F1 !important;font-weight:bold;">전체상담건수'+ (sel_col == 11 ? "(실건수)" : "") +'</td>';
		var sum_dt2 = 0;
		for(var i=0; i<sel_csl_cd_cnt; i++) {
			txt_dst_tot = '';
			var dt2 = data.target2_tot[i];//전체상담건수(가로축)
			sum_dt2 += +dt2;
			if(sel_col == 11) { //상담유형
				txt_dst_tot = ' ('+ numberToCurrency(data.dst_tot[i].cnt) +')';
				dt_dst_tot += +data.dst_tot[i].cnt;
			}
			html_b += '  <td colspan="2" style="background:#F1F1F1 !important; style="border-right:1px solid #ddd;">'+ numberToCurrency(dt2['t_cnt']) + txt_dst_tot +'</td>';
		}
		html_b += '  <td colspan="2" style="background:#F1F1F1 !important; style="border-right:1px solid #ddd;">&nbsp;</td>';
		var txt_dt_dst_tot = '';
		if(sel_csl_cd == 11 || sel_col == 11) { //상담유형
			txt_dt_dst_tot = ' ('+ numberToCurrency(dt_dst_tot) +')';
		}
		html_b += '  <td style="background:#F1F1F1 !important;">'+ numberToCurrency(dt_tot) + txt_dt_dst_tot +'</td>';
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="'+ (sel_csl_cd_cnt+2)*2 +'" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';

	// list html
	$('#list').empty().html(html_b);
	$('#list th,td').css({whiteSpace:'nowrap'});	
}

/*막대그래프 만들기 시작*/
google.charts.load('current', {'packages':['corechart']});

// 기본 컬럼차트
function column_chart(data) {
  // 인쇄, 다운로드용
  var chart_html = '<table style="width:805px;">';
  chart_html += '<tr><td colspan="2"><div id="chart_div0" class="chart_div"></div></td></tr>';//첫번째 column chart
  var idx = 1;
  for(var i=1; i<=data.data.length; i++) {
  	chart_html += '<tr><td style="width:50%;"><div id="chart_div'+ idx++ +'" class="chart_div"></div></td>'+
  		'<td style="width:50%;"><div id="chart_div'+ idx++ +'" class="chart_div"></div></td></tr>';
  }
  chart_html += '</table>';
  $('#chart_div').html(chart_html);
  
	var chart_html = '';
	var sel_col = $('#sel_col').val();
	var txt_sel_col = $('#sel_col option:selected').text();
	var txt_csl_code = $('#csl_code option:selected').text();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();
	var chart_data = new google.visualization.DataTable();

	chart_data.addColumn('string', '구분');
	chart_data.addColumn('number', '임금');
	chart_data.addColumn({type:'string', role: 'annotation'});
	chart_data.addColumn({type:'string', role: 'style'});
	chart_data.addColumn('number', '근로시간');
	chart_data.addColumn({type:'string', role: 'annotation'});
	chart_data.addColumn({type:'string', role: 'style'});

	//<교차통계용> 기본차트 선택시 서브차트용 데이터 배열
	var chartDataPay = [], chartDataWk = [], tmpHeader = [];
	
	// 단순
	if( $('#csl_code').val() == '' ) {
		// 교차용 서브차트 숨김
		$('#donut_chart').hide();
    for(var idx in data.data) {
      var d = data.data[idx];
      console.log(idx, d);
      var pay = +d.pay;
      var works = +d.works;
			array_data.push([d.subject, pay, numberToCurrency(pay), '#3366CC', works, numberToCurrency(works), '#448932']);
    }
//     console.log(array_data);
    chart_data.addRows(array_data);
	}
	// 교차
	else {
    $('table.tList02 tr').eq(0).find('th').each(function(i,o){
      if($(this).text() != '구분' && $(this).text().indexOf('전체') == -1) {
      	tmpHeader[i-1] = $(this).text();
      }
    });
//     console.log( tmpHeader );
    
    for(var idx in data.data) {
      var d = data.data[idx];
//       console.log(idx, d);
			if( ! chartDataPay[idx]) chartDataPay[idx] = [];
			if( ! chartDataWk[idx]) chartDataWk[idx] = [];
      
      var pay = 0, p_cnt = 0, work = 0, p_work = 0;
      for(var j=1; j<=sel_csl_cd_cnt; j++) {
	      pay += +d['pay'+ j] * +d['p_cnt'+ j];
	      p_cnt += +d['p_cnt'+ j];
	      work += +d['work'+ j] * +d['w_cnt'+ j];
	      p_work += +d['w_cnt'+ j];
	      chartDataPay[idx][j-1] = [tmpHeader[j-1], +d['pay'+ j]];
	      chartDataWk[idx][j-1] = [tmpHeader[j-1], +d['work'+ j]];
      }
      chartDataPay[idx].unshift(['구분','데이터']);
      chartDataWk[idx].unshift(['구분','데이터']);
      var p = Math.round(pay/p_cnt);
      p = isNaN(p) ? 0 : p;
      var w = Math.round(work/p_work);
      w = isNaN(w) ? 0 : w;
  		array_data.push([d.subject, p, numberToCurrency(p), '#3366CC', w, numberToCurrency(w), '#448932']);
    }
//     console.log( chartDataPay );
//     console.log( chartDataWk );
//     console.log(array_data);
    chart_data.addRows(array_data);

    // 인쇄, 다운로드용
    var n = 1;//0번째는 기본(컬럼)차트가 사용
    for(var i=0; i<data.data.length; i++) {
  		img_chart(n++, data.data[i].subject, '임금', chartDataPay[i]);
  		img_chart(n++, data.data[i].subject, '근로시간', chartDataWk[i]);
    }
	}
	
	var chart_title = "임금근로시간 "+ ($('#csl_code').val() == '' ? "" : "교차통계") +" Chart\n"+ 
		"상담기간: "+ $("#search_date_begin").val() +"~"+ $("#search_date_end").val();
	
  var options = {
  	title: chart_title,
  	width: 1620,
  	height: 700,
  	chartArea: {
  		left: 100,
  		right: 100,
  		top: 100,
  		bottom: 200,
  		width: '100%',
  		height: '400',
  	},
  	bar: {
  		groupWidth: "30%",
  	},
  	fontSize: 12,
  	axisTitlesPosition: 'in',
    series: {
			0: {targetAxisIndex: 0},
			1: {targetAxisIndex: 1}
		},
		vAxes: {
			0:{title: '임금', minValue: 0}, // Left y-axis.
			1:{title: '근로시간', minValue: 0}, // Right y-axis.
		},
		colors: ['#3366CC', '#448932']
		,sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value' // 퍼센트 대신 값이 출력되게 함
  };
  
  var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'));
  var chart_div = document.getElementById('chart_div0');
  
  google.visualization.events.addListener(chart, 'ready', function() {
  	chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
  });

  chart.draw(chart_data, options);

  // 단순임금근로시간통계는 도넛차트 불필요( 교차에서만 사용 )
  if($('#csl_code').val() == '') {
    return;
  }

  pie_chart(data.data[0].subject, '임금', chartDataPay[0]);
  
  function selectHandler() {
  	var selection = chart.getSelection();
  	var sel_row = +selection[0].row;
		var d = +selection[0].column > 1 ? chartDataWk[sel_row] : chartDataPay[sel_row];
	  var subject = data.data[sel_row].subject;
	  var kind = +selection[0].column > 1 ? '근로시간' : '임금';
	  //
//     console.log('select', d);
	  pie_chart(subject, kind, d);
  }
  /*막대그래프, 셀렉트 이벤트핸들러 시작*/
  google.visualization.events.addListener(chart, 'select', selectHandler);	
}
/*막대그래프 만들기 끝*/

/*도넛그래프 만드기 시작*/
function pie_chart(subject, kind, data) {
// 	var txt_sel_col = $('#sel_col option:selected').text();
	var txt_csl_code = $('#csl_code option:selected').text();
	
	var chart_data = google.visualization.arrayToDataTable(data);
	
	var options = {
		title: '[' + subject + '] - [' + txt_csl_code + '] '+ kind +' 상세 Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 100,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '400',
		},
		legend: {
			position: 'right',
			maxLines: 3,
		},
		tooltip: {
			ignoreBounds: true,
		},
		fontSize: 12,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};
	
	var chart = new google.visualization.PieChart(document.getElementById('donut_chart'));
	chart.draw(chart_data, options);
}

function img_chart(idx, subject, kind, data) {
	var txt_csl_code = $('#csl_code option:selected').text();	
	var chart_data = google.visualization.arrayToDataTable(data);
	var options = {
		title: '[' + subject + '] - [' + txt_csl_code + '] '+ kind +' 상세 Chart',
		width: 1620,
		height: 500,
		chartArea: {
			left: 50,
			right: 50,
			top: 50,
			bottom: 100,
			width: '90%',
			height: '400',
		},
		is3D: true
		,sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart_div = document.getElementById('chart_div' + idx);
	var chart = new google.visualization.PieChart(chart_div);

	google.visualization.events.addListener(chart, 'ready', function() {
		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});

	chart.draw(chart_data, options);
}
/*도넛그래프 만드기 시작*/
</script>


  <?php
  include_once './inc/inc_menu.php';
  ?>
  <!--- //lnb 메뉴 area ---->
  <div id="contents_body">
  	<div id="cont_head">
  		<h2 class="h2_tit">상담내역 통계</h2>
  		<span class="location">
  			홈 > 상담내역 통계 > <em>통계</em>
  		</span>
  		<input type="hidden" id="begin" value="<?php echo $date['begin']?>">
  		<input type="hidden" id="end" value="<?php echo $date['end']?>">
  		<input type="hidden" id="sch_asso_code" value="<?php echo $date['sch_asso_code']?>">
  		<input type="hidden" id="sch_s_code" value="<?php echo $date['sch_s_code']?>">
  		<input type="hidden" id="sch_csl_proc_rst" value="<?php echo $date['sch_csl_proc_rst']?>">
  	</div>
  	<?php
  include_once './inc/inc_statistic_info.php';
  ?>
  	<ul class="tab">
			<li><a href="#" class="go_page">상담사례</a></li>
			<li><a href="#" class="go_page">기본통계</a></li>
			<li><a href="#" class="go_page">교차통계</a></li>
			<li><a href="#" class="go_page selected" style="text-decoration:none">임금근로시간통계</a></li>
			<li><a href="#" class="go_page">소속별통계</a></li>
			<li><a href="#" class="go_page">월별통계</a></li>
			<li><a href="#" class="go_page">시계열통계</a></li>
  	</ul>
  	<div class="cont_area" style="margin-top: 0">
  		<form name="frmSearch" id="frmSearch" method="post">
  			<input type="hidden" id="sch_kind" name="sch_kind" value="0">
  			<div class="con_text">
  				<label for="search_date_begin" class="l_title">상담일</label>
  				<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" readonly="readonly" style="width: 90px; margin-left: -20px">
  				~
  				<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" readonly="readonly" style="width: 90px">
  				<label for="sel_year" class="l_title" style="margin-left: 10px;">연월별</label>
  				<select id="sel_year" name="sel_year" style="margin-left: -20px;">
  					<option value="">연도</option>
  				</select>
  				<select id="sel_month" name="sel_month">
  					<option value="">전체</option>
  					<option value="01">1월</option>
  					<option value="02">2월</option>
  					<option value="03">3월</option>
  					<option value="04">4월</option>
  					<option value="05">5월</option>
  					<option value="06">6월</option>
  					<option value="07">7월</option>
  					<option value="08">8월</option>
  					<option value="09">9월</option>
  					<option value="10">10월</option>
  					<option value="11">11월</option>
  					<option value="12">12월</option>
  				</select>
  				<label for="sel_year" class="l_title" style="margin-left: 10px;">기간별</label>
  				<button type="button" id="btnToday" class="buttonS bGray" style="margin-left: -20px;">오늘</button>
  				<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
  				<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
  				<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button>
  				<br>
  				<span class="divice"></span>
  				<div>
  					<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속</button>
  					<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
  					<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
  					<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input>
  					<!-- 디버깅: 초기화버튼 클릭시 세팅용 초기데이터 -->
  					<input type="hidden" name="asso_code_name"></input>
  					<span id="asso_code_name" style="margin-left: 27px;"></span>
  				</div>
  				<span class="divice"></span>
  				<div>
  					<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
  					<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
  					<input type="hidden" name="s_code" id="s_code"></input>
  					<input type="hidden" name="s_code_name"></input>
  					<span id="s_code_name" style="margin-left: 5px;"></span>
  				</div>
  				<span class="divice"></span>
  				<div>
  					<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
  					<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
  					<input type="hidden" name="csl_proc_rst" id="csl_proc_rst"></input>
  					<input type="hidden" name="csl_proc_rst_name"></input>
  					<span id="csl_proc_rst_name" style="margin-left: 5px;"></span>
  				</div>
  				<span class="divice"></span>
  				<div class="stab_button">
  					<label for="sel_col" class="stab_title">통계 유형</label>
  					<select id="sel_col" name="sel_col">
  						<option value=1>성별</option>
  						<option value=2>연령대</option>
  						<option value=3>거주지</option>
  						<option value=4>회사소재지</option>
  						<option value=5>직종</option>
  						<option value=6>업종</option>
  						<option value=7>고용형태</option>
  						<option value=8>근로자수</option>
  						<option value=9>근로계약서</option>
  						<option value=10>4대보험</option>
  						<option value=11>상담유형</option>
  						<option value=12>상담동기</option>
  						<option value=13>소속</option>
  						<option value=0>상담방법</option>
  						<!-- 상담방법 순서 변경 요청에 의해 가장 아래로.. -->
  					</select>
  					<select id="csl_code" name="csl_code"></select>
  					<input type="hidden" name="csl_code_idx" value=""></input><!-- 통계용 -->
  					<!-- csl_code select의 옵션 제어용 -->
  					<select id="csl_code_options" style="display: none;">
  						<!-- 교차통계선택시 교차통계화면을 보여준다. 미선택시 임금근로시간 화면 노출 -->
  						<option value='' data-idx="">교차통계선택</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);?>' data-idx="1">성별</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);?>' data-idx="2">연령대</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);?>' data-idx="3">거주지</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP);?>' data-idx="4">회사소재지</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);?>' data-idx="5">직종</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);?>' data-idx="6">업종</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);?>' data-idx="7">고용형태</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);?>' data-idx="8">근로자수</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);?>' data-idx="9">근로계약서</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);?>' data-idx="10">4대보험</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);?>' data-idx="11">상담유형</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE);?>' data-idx="12">상담동기</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);?>' data-idx="13">소속</option>
  						<option value='<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);?>' data-idx="0">상담방법</option>
  					</select>
  				</div>
  				<div id="infoLabel" class="floatL marginT10" style="color: #f37000; display: none">
  					* 상담유형은 중복선택이 가능하여 데이터가 중복될 수 있습니다. 총계표기 : 중복데이터(실데이터)
  				</div>
  				<div class="textR marginT10">
  					<button type="button" id="btnSubmit" class="buttonS bBlack">
  						<span class="icon_search"></span>
  						검색
  					</button>
  				</div>
  			</div>
  		</form>
  		<!-- //컨텐츠 -->
  		<div class="list_select" style="margin-bottom:10px;">
  			<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
  			<button type="button" id="printlist" class="buttonM bGray floatR">표 인쇄</button>
  			<button type="button" id="view_chart" class="buttonM bGray floatR">통계차트 보기</button>
  		</div>
  		
  		<!-- list -->
  		<div style="overflow: auto;">
  			<button type="button" id="downchart" class="buttonS bBlack" style="display: none">차트다운로드</button>
  			<button type="button" id="printchart" class="buttonS bBlack" style="display: none">차트인쇄</button>
  			<div id="column_chart" class="chart" style="width: 100%; height: 700px; display: none"></div>
  			<div id="donut_chart" class="chart" style="width: 100%; height: 700px; display: none"></div>
  			<div id="list" style="width: 100%;"></div>
  		</div>
  		
  		<div id="chart_div" style="display: none"></div>
  		<a href="#" id="go_to_top" title="Go to top"></a>
  	</div>
  	<!-- //컨텐츠 -->
  </div>
  <!-- //  contents_body  area -->
  <?php
  include_once './inc/inc_footer.php';
  ?>
