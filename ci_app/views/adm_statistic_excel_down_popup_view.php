<script>
/**
 * 
 * 통계 전용
 *
 */
var url = '/?c=statistic&m=download';
var csl_kind = "<?php echo $csl_kind;?>";
if(csl_kind == 2) { // 사용자상담
	url = '/?c=biz_statistic&m=download';
}

$(document).ready(function(e){
	// 다운받기 버튼 포커스
	$('button.bOrange').focus();

	// 전송폼 action
	$("#frmSearch").attr('action', url);

	// 다운받기 클릭시 전송
	$('button.bOrange').click(function(e){
		submit_form(e);
	});

	// 다운받기 버튼 Enter key 눌러지면 전송
	$("button.bOrange").keypress(function(e) {
		if (e.keyCode == 13) {
			submit_form(e);
			closeLayerPopup();
		}
	});

	// 닫기
	$('button.cssClosePopup').click(function(e){
		closeLayerPopup();
	});
  
});

function submit_form(e) {
	e.preventDefault();
	$("#frmSearch").attr('action', url);
	$('#frmSearch input[name=kind]').remove();
	$('#frmSearch input[name=not_seq]').remove();
	$('#frmSearch').append('<input type="hidden" name="kind" value="<?php echo $kind;?>"></input>');
	$('#frmSearch').append('<input type="hidden" name="not_seq" value="<?php echo $not_seq;?>"></input>');
	$("#frmSearch").submit();
}
</script>


	<div id="popup">
		<header id="pop_header">
			<h2 class="">Excel Download</h2>			
		</header>
		<div id="popup_contents">			
			<div class="floatC marginT05 marginB20">
				<div class="floatL fRed">* 전체 다운로드는 서버 부하가 크므로 업무시간 외 하시기 바랍니다.</div>
				<div class="floatL">다운로드 하시겠습니까?</div>
			</div>
		<div class="btn_set">
			<button type="button" class="buttonM bOrange cssClosePopup">받기</button>
			<button type="button" class="buttonM bGray cssClosePopup">닫기</button>
		</div>
	</div>