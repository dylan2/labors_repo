<?php
include_once './inc/inc_header.php';
?>


<style type="text/css">
/* 가독성을 높이기 위해 추가 */
textarea {
	line-height: 1.4em;
}
</style>

<script>

var kind = "<?php echo $res['kind'] == 'menu' ? 'add' : $res['kind']; ?>";
if(!kind) kind = 'add';
if(kind == 'view') kind = 'edit'; // view 권한이 추가되어 view 권한은 kind=view로 넘어오기 때문에 바꿔줌

//
var is_owner = "<?php echo $res['is_owner']; ?>";

// 인쇄가능 여부 - 권익센터,자치구센터 소속자의 작성글은 해당 소속자는 인쇄가능하다.
var is_printable = "<?php echo $res['is_printable']; ?>";


$(document).ready(function(){

	// view 권한이 추가되어 view 권한은 kind=view로 넘어오기 때문에 바꾼 데이터 적용
	$('input[name=kind]').val(kind);

	// 상담일 today 세팅
	if(kind == 'add') {
		$('#csl_date').val(get_today());

		// 거주지 주소 최초 선택 처리
		$('#live_addr option').each(function(i,ele){
			if($(this).text() == '무응답') {
				$('#live_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});
		// 소재지 주소 최초 선택 처리
		$('#comp_addr option').each(function(i,ele){
			if($(this).text() == '무응답') {
				$('#comp_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});

		// 삭제 버튼 숨김
		$('.cssDel').hide();
	}
	// edit
	else {
		// 거주지 주소
		if($('#live_addr option:selected').text() == '기타') {
			$('#live_addr_etc').css('display', 'inline');
		}
		// 소재지 주소
		if($('#comp_addr option:selected').text() == '기타') {
			$('#comp_addr_etc').css('display', 'inline');
		}
		// 출석일 버튼 활성화
		$('#btnAddLidDateContainer').show();
		$('#lib_desc').hide();

		// 관리자 외 자신의 글 아니면 등록(수정기능),삭제,인쇄,출석조사일 버튼 숨김
		if(is_master != 1 && is_owner == 0) {
			$('.cssAdd').prop('disabled', true);
			$('.cssDel').prop('disabled', true);
			$('#btnAddLidDate').prop('disabled', true);
		}
		// 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우는 허용
		if(is_printable == 1) {
			$('.cssPrint').prop('disabled', false);
		}
	}

	// 인쇄 버튼
	$('.cssPrint').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		if(kind == 'edit') {
			open_popup_center_dual('/?c=lawhelp&m=lawhelp_print&seq='+ $('#seq').val(), '인쇄', 1060, 700);
		}
	});

	// 출석일 버튼 - 저장
	$('input:button[id=btnAddLidDate]').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if($('#lid_date').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_13']);
			$('#lid_date').focus();
			return;
		}
		var url = '/?c=lawhelp&m=add_lid_date';
		var rsc = 'seq='+ $('#seq').val() +'&lid_date='+ $('#lid_date').val();
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				var new_html = '<span style="width:100px;">'+ $('#lid_date').val() +'<input type="button" class="buttonS bGray cssRemoveLidDate" value="삭제" data-lid_seq="'+ data.new_id +'" style="padding:2px 4px;margin:0 10px 0 3px;"></span>';
				$('#lidDataContainer').prepend(new_html);
			}
			$('#lid_date').val('');
		};
		var fn_error = function(data) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);
	});

	// 출석조사일 삭제 버튼
	$('#lidDataContainer').bind('click', '.cssRemoveLidDate', function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if($(e.target)[0].tagName !== 'INPUT') return;

		var btn_this = $(e.target);
		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_10'])) {
			var url = '/?c=lawhelp&m=del_lid_date';
			var rsc = 'lid_seq='+ btn_this.attr('data-lid_seq');
			var fn_succes = function() {
				btn_this.closest('span').remove();
			};
			var fn_error = function(data) {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
			};
			// request
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});
	
	// 삭제버튼 클릭
	$('.cssDel').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
			
			var url = '/?c=lawhelp&m=del';
			var rsc = {seq: $('#seq').val()};
			var fn_succes = function(data) {
				if(data.rst == 'succ') {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
					check_auth_redirect($('#'+ sel_menu).find('li>a').last(), 'list');
				}
				else {
					if(is_local) objectPrint(data);
					alert(data.msg);
				}
			};
			var fn_error = function(data) {
				if(is_local) objectPrint(data);
			
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			};
			// request
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});

	// 전송버튼 클릭시 form onSubmit 이벤트 발생처리
	$('.cssAdd').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {

			// 접수번호 중복여부 체크
			if(kind == 'add') {
				var url = '/?c=lawhelp&m=chk_code';
				var rsc = {code:$('#lh_code').val()};
				var fn_succes = function(data) {
					if(data.rst == 'succ') {
						$('form').submit();
					}
					else {
						alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_14']);
						$('#lh_code').focus();
					}
				};
				var fn_error = function(data) {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
				};
				req_ajax(url, rsc, fn_succes, fn_error);
			}
			else {
				$('form').submit();
			}
		}
	});
	
	// 등록 버튼 이벤트 핸들러
	$('form').submit(function(e) {

		// 수정 - 자신의 글인지 체크
		if(kind == 'edit' && is_master != 1 && is_owner == 0) {
			alert(CFG_MSG[CFG_LOCALE]['warn_board_02']);
			return false;
		}

		// validation
		// 필수체크 항목
		// - 접수번호(자동생성), -> 직접입력으로 변경 : 최진혁 20160617
		// - 사건유형, 지원승인일, 신청자, 거주지, 회사, 회사소재지, 신청기관, 대리인(소속,이름), 대상기관, 권리구제지원내용
		// 접수번호
		if($('input[name=lh_code]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_12']);
			$('#lh_code').focus();
			return false;
		}
		// 사건유형
		if(!$("input[name=sprt_kind_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_01']);
			$('#sprt_kind_cd_0').focus();
			return false;
		}
		// 지원승인일
		if($('input[name=lh_sprt_cfm_date]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_02']);
			$('input[name=lh_sprt_cfm_date]').focus();
			return false;
		}
		// 권리구제 유형 - 추가 2018.07.05
		if($('select[name=lh_kind_cd] option:selected').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_16']);
			$('select[name=lh_kind_cd]').focus();
			return false;
		}
		// 권리구제 세부유형 - 추가 2018.07.05
		// -- 새부유형이 없는 유형도 있어 필수에서 제외 - 2018.07.16 dylan
		// if($('input:checkbox[name="lh_kind_sub_cd[]"]:checked').length == 0) {
		// 	alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_17']);
		// 	$('input:checkbox[name="lh_kind_sub_cd[]"]').eq(0).focus();
		// 	return false;
		// }
		// 신청자
		if($('input[name=lh_apply_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_03']);
			$('input[name=lh_apply_nm]').focus();
			return false;
		}
		// 성별 : 추가 2018.07.03 dylan
		if($('input[name=gender_cd]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_15']);
			$('input[name=gender_cd]').eq(0).focus();
			return false;
		}
		// 거주지
		if($('input[name=lh_apply_addr]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_04']);
			$('input[name=lh_apply_addr]').focus();
			return false;
		}
		// 회사
		if($('input[name=lh_apply_comp_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_05']);
			$('input[name=lh_apply_comp_nm]').focus();
			return false;
		}
		// 회사소재지
		if($('input[name=lh_comp_addr]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_06']);
			$('input[name=lh_comp_addr]').focus();
			return false;
		}
		// 연령대
		if(!$("input[name=ages_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_20']);
			$('#ages_cd_0').focus();
			return false;
		}
		// 신청기관
		if(!$("input[name=apply_organ_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_07']);
			$('#apply_organ_cd_0').focus();
			return false;
		}
		// 대리인-소속
		if($('input[name=lh_labor_asso_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_08']);
			$('input[name=lh_labor_asso_nm]').focus();
			return false;
		}
		// 대리인-이름
		if($('input[name=lh_labor_nm]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_09']);
			$('input[name=lh_labor_nm]').focus();
			return false;
		}
		// 대상기관
		if(!$("input[name=sprt_organ_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_10']);
			$('#sprt_organ_cd_0').focus();
			return false;
		}
		// 권리구제지원내용
		if($('textarea[name=lh_sprt_content]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_lawhelp_11']);
			$('textarea[name=lh_sprt_content]').focus();
			return false;
		}

		// 전송
		var url = '/?c=lawhelp&m=processing';
		var rsc = $('#frmForm').serialize();
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
				check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
				if(is_local) objectPrint(data);
			}
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
		
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);

		e.preventDefault();
	});

	// list 버튼 이벤트 핸들러
	$('.cssList').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), 'list', '');
	});


	$('#lh_code').focus();


	// focusOut 용
	$(window).scroll(function() {
		$('#focusOuter').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 0});    
	});

	// 안내 팝업
	$('.cssOpenPop').click(function(e){
		e.preventDefault();
		var url = '/?c=counsel&m=counsel_help&url=./help/adm_counsel_help'+ $(this).attr('data-kind') +'_popup_view';
		openPopup(url);
	});

	// 지원유형 select change handler - 추가 2018.07.05
	$('select[name=lh_kind_cd]').change(function(e){
		var fn_init_sub_div = function() {
			$('#subCdContainer').empty();
			$('#subCdContainer').append("유형을 선택해 주세요.");
		};
		var cd = $(this).find('option:selected').val();
		if( ! cd) {
			fn_init_sub_div();
		}
		else {
			var url = '/?c=code&m=lhkind_sub_cd';
			var rsc = "code="+ cd;
			var fn_succes = function(data) {
				if(data && data.tot_cnt > 0) {
					$('#subCdContainer').empty();
					var arr_data = $('#kindSubCdData').text().split("&");
					$.each(data.data, function(i, o){
						var chk = '';
						$.each(arr_data, function(i2, o2){
							chk = "<input type='checkbox' name='lh_kind_sub_cd[]' id='chk_"+ i +"' value='"+ Base64.encode(o.s_code) +"' "+ (o.s_code==o2?'checked':'') +"><label for='chk_"+ i +"' style='margin-right:20px;'>"+ o.code_name +"</label>";
							if(o.s_code == o2) {
								return false;
							}
						});
						$('#subCdContainer').append(chk);
					});
				}
				else {
					fn_init_sub_div();
				}
			};
			var fn_error = function(data) {
				if(is_local) objectPrint(data);
			};
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});

	// 권리구제 유형 데이터가 있으면 세팅
	if($('#kindSubCdData').text() != '') {
		$('select[name=lh_kind_cd]').change();
	}
	// -- 세부유형 선택안한 경우
	else {
		$('select[name=lh_kind_cd]').change();
	}
});


var openPopup = function(url) {
	gLayerId = openLayerModalPopup(url, 500, 300, '', '', '', '', 1, true);
}
</script>



<?php
include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">권리구제내역 입력</h2>
				<span class="location">홈 > 권리구제내역 관리 > <em>권리구제내역 입력</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post">
				<input type="hidden" name="kind"></input>
				<input type="hidden" name="oper_id" id="oper_id" value="<?php echo $edit['oper_id']; ?>"></input>
				<input type="hidden" name="seq" id="seq" value="<?php echo $edit['seq']; ?>"></input>
				<h3 class="sub_stit">
					신청정보
					<div class="marginT10 textR" style="margin-top:-18px;">
						<button type="button" class="buttonM bGray cssPrint">인쇄</button>
						<button type="button" class="buttonM bSteelBlue cssAdd">등록</button>
						<button type="button" class="buttonM bOrange cssDel">삭제</button>
						<!-- <button type="button" class="buttonM bGray cssBack">뒤로이동</button> -->
						<button type="button" class="buttonM bDarkGray cssList">목록</button>
					</div>
				</h3>
				<table class="tInsert">
					<caption>
						신청정보 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>접수번호 <span class="fRed">*</span></th>
						<td>
							<input type="text" id="lh_code" name="lh_code" maxlength="9" value="<?php echo $edit['lh_code'];?>" style="width:80px"> * 9자리
						</td>
					</tr>
					<tr>
						<th>지원종류 <span class="fRed">*</span></th>
						<td>
						<div style="height:30px;padding-top:10px;">
						<?php
							$index = 0; //$index_etc = 0;
							// $etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['code_sprt_kind'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								// if($item->code_name == '기타') {
								// 	$etc_item['index'] = $index;
								// 	$etc_item['item'] = $item;
								// 	$index_etc = $index;
								// }
								// else {
									echo '<input type="radio" name="sprt_kind_cd" class="imgM" id="sprt_kind_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['sprt_kind_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="sprt_kind_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								// }
								$index++;
							}
							// 기타 항목
							// echo '<input type="radio" name="sprt_kind_cd" class="imgM" id="sprt_kind_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['sprt_kind_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="sprt_kind_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?></div>
						<input type="text" name="sprt_kind_cd_etc" id="sprt_kind_cd_etc" class="imgM" size="70" maxlength="100" value="<?php echo $edit['sprt_kind_cd_etc'];?>"> * 최대 100자
						</td>
					</tr>
					<tr>
						<th>지원승인일 <span class="fRed">*</span></th>
						<td>
							<input type="text" id="lh_sprt_cfm_date" name="lh_sprt_cfm_date" class="datepicker date" value="<?php echo $edit['lh_sprt_cfm_date'];?>" style="width:80px">
						</td>
					</tr>
					<tr>
						<th>사건유형 <span class="fRed">*</span></th>
						<td>
							<table class="tInsert">
								<colgroup>
									<col style="width:10%">
									<col style="width:20%">
									<col style="width:10%">
									<col style="width:60%">
								</colgroup>
								<tr>
									<th>1차유형</th>
									<td>
										<select name="lh_kind_cd" style="width:130px;margin-right:30px;">
											<option value="">선택하세요.</option>
										<?php
											foreach($res['code_lh_kind'] as $index => $item) {
												echo '<option value="'. base64_encode($item->s_code) .'" '. ($edit['lh_kind_cd'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
											}
										?>
										</select>
									</td>
									<th>세부유형</th> 
									<td>
										<div id="subCdContainer">유형을 선택해 주세요.</div>
										<div id="kindSubCdData" style="display:none;"><?php
										$tmp = '';
										foreach ($edit['lh_kind_cd_data'] as $key => $value) {
											if($tmp != '') $tmp .= "&";
											$tmp .= $value->kind_sub_cd;
										}
										echo $tmp;
										?></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<th>신청자 <span class="fRed">*</span></th>
						<td>
							<input type="text" name="lh_apply_nm" id="lh_apply_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_apply_nm'];?>"> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>성별 <span class="fRed">*</span></th>
						<td>
						<?php
						foreach($res['gender_cd'] as $i => $item) {
							echo '<input type="radio" name="gender_cd" class="imgM" id="gender_cd_'. $i .'" value="'. base64_encode($item->s_code) .'" '. ($edit['gender_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="gender_cd_'. $i++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>
					</tr>
					<tr>
						<th>거주지 <span class="fRed">*</span></th>
						<td>
							<input type="text" name="lh_apply_addr" id="lh_apply_addr" class="imgM" size="40" maxlength="70" value="<?php echo $edit['lh_apply_addr'];?>">  * 최대 70자
						</td>										
					</tr>
					<tr>
						<th>회사 <span class="fRed">*</span></th>
						<td>
							<input type="text" name="lh_apply_comp_nm" id="lh_apply_comp_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_apply_comp_nm'];?>"> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>회사소재지 <span class="fRed">*</span></th>
						<td>
							<input type="text" name="lh_comp_addr" id="lh_comp_addr" class="imgM" size="40" maxlength="70" value="<?php echo $edit['lh_comp_addr'];?>"> * 최대 70자
						</td>
					</tr>
					<tr>
						<th>연령대 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['ages_cd'] as $item) {
							echo '<input type="radio" name="ages_cd" class="imgM" id="ages_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['ages_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="ages_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?><br>
							<input type="text" name="ages_etc" id="ages_etc" value="<?php echo $edit['ages_etc'];?>" style="width:100px" maxlength="30" placeholder=""> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>직종 <a href="#" data-kind="1" class="cssOpenPop"><img src="../images/common/icon_que.png" alt="직종이란" class="imgM"></a></th>
						<td>
						<?php
						$index = 0;
						foreach($res['work_kind'] as $item) {
							echo '<input type="radio" name="work_kind" class="imgM" id="work_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['work_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="work_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<br>추가내용 : <input name="work_kind_etc" id="work_kind_etc" style="width: 350px; display: inline;" type="text" maxlength="30" value="<?php echo $edit['work_kind_etc'];?>" placeholder="최대 30자">
						</td>	
					</tr>
					<tr>
						<th>업종 <a href="#" data-kind="2" class="cssOpenPop"><img src="../images/common/icon_que.png" alt="업종이란" class="imgM"></a></th>
						<td>
						<?php
						$index = 0;
						foreach($res['comp_kind'] as $item) {
							echo '<input type="radio" name="comp_kind" class="imgM" id="comp_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['comp_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="comp_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<br>추가내용 : <input name="comp_kind_etc" id="comp_kind_etc" style="width: 350px; display: inline;" type="text" maxlength="30" value="<?php echo $edit['comp_kind_etc'];?>" placeholder="최대 30자">
						</td>	
					</tr>	
					<tr>
						<th>신청기관 <span class="fRed">*</span></th>
						<td>
						<?php
							$index = 0; //$index_etc = 0;
							// $etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['code_apply_organ'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								// if($item->code_name == '기타') {
								// 	$etc_item['index'] = $index;
								// 	$etc_item['item'] = $item;
								// 	$index_etc = $index;
								// }
								// else {
									echo '<input type="radio" name="apply_organ_cd" class="imgM" id="apply_organ_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['apply_organ_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="apply_organ_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								// }
								$index++;
							}
							// 기타 항목
							// echo '<input type="radio" name="apply_organ_cd" class="imgM" id="apply_organ_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['apply_organ_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="apply_organ_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
						</td>
					</tr>
					<tr>
						<th>대리인 <span class="fRed">*</span></th>
						<td><b>소속</b> : 
							<input type="text" name="lh_labor_asso_nm" id="lh_labor_asso_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_labor_asso_nm'];?>"> * 최대 30자&nbsp;&nbsp;
							<b>이름</b> : 
							<input type="text" name="lh_labor_nm" id="lh_labor_nm" class="imgM" size="20" maxlength="30" value="<?php echo $edit['lh_labor_nm'];?>"> * 최대 30자
						</td>
					</tr>
					<tr>
						<th>대상기관(관할) <span class="fRed">*</span></th> <!-- 대상기관(관할) 5개중 하나만 선택, 추가 입력박스도 마찮가지 -->
						<td>
						<div style="height:30px;padding-top:10px;">
						<?php
							$index = 0;
							foreach($res['code_sprt_organ'] as $item) {
								echo '<input type="radio" name="sprt_organ_cd" class="imgM" id="sprt_organ_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['sprt_organ_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="sprt_organ_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'. PHP_EOL;
								$index++;
							}
						?></div>
						<input type="text" name="sprt_organ_cd_etc" id="sprt_organ_cd_etc" class="imgM" size="70" maxlength="100" value="<?php echo $edit['sprt_organ_cd_etc'];?>"> * 최대 100자
						</td>
					</tr>
				</table>
				<h3 class="sub_stit">절차 진행 상황</h3>
				<table class="tInsert">
					<caption>
						절차 진행 상황 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>사건접수일</th>
						<td>
							<input type="text" id="lh_accept_date" name="lh_accept_date" class="datepicker date" value="<?php echo $edit['lh_accept_date'];?>" style="width:80px">
						</td>
					</tr>
					<tr>
						<th>출석조사일</th>
						<td>
							<input type="text" id="lid_date" name="lid_date" class="datepicker date" value="<?php echo $edit['lid_date'];?>" style="width:80px">
							<div id="btnAddLidDateContainer" style="display:none;margin:-30px 0 0 100px;">
								<input type="button" class="buttonS bGray" id="btnAddLidDate" value="추가">
							</div> <span id="lib_desc">* 추가버튼은 수정할때만 활성화됩니다.</span>
							<div style="margin-top:10px;" id="lidDataContainer">
							<?php
								foreach($edit['lid_date_data'] as $item) {
									echo '<span style="width:100px;">'. $item->lid_date .'<input type="button" class="buttonS bGray cssRemoveLidDate" value="삭제" data-lid_seq="'. $item->lid_seq .'" style="padding:2px 4px;margin:0 10px 0 3px;"></span>'. PHP_EOL; 
								}
							?>
							</div>
						</td>						
					</tr>
					<tr>
						<th>사건종결일</th>
						<td>
							<input type="text" id="lh_case_end_date" name="lh_case_end_date" class="datepicker date" value="<?php echo $edit['lh_case_end_date'];?>" style="width:80px">
						</td>
					</tr>
				</table>
				<h3 class="sub_stit">지원내용 및 결과</h3>
				<table class="tInsert">
					<caption>
						지원내용 및 결과 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>권리구제지원내용 <span class="fRed">*</span></th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="lh_sprt_content" id="lh_sprt_content"><?php echo $edit['lh_sprt_content'];?></textarea></td>
					</tr>
					<tr>
						<th>지원 결과</th>
						<td>
						<div style="height:30px;padding-top:10px;">
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['code_apply_rst'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								if($item->code_name == '기타') {
									$etc_item['index'] = $index;
									$etc_item['item'] = $item;
									$index_etc = $index;
								}
								else {
									echo '<input type="radio" name="apply_rst_cd" class="imgM" id="apply_rst_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['apply_rst_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="apply_rst_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								}
								$index++;
							}
							// 기타 항목
							echo '<input type="radio" name="apply_rst_cd" class="imgM" id="apply_rst_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['apply_rst_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="apply_rst_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?></div>
						<input type="text" name="apply_rst_cd_etc" id="apply_rst_cd_etc" class="imgM" size="70" maxlength="100" value="<?php echo $edit['apply_rst_cd_etc'];?>"> * 최대 100자
						</td>
					</tr>
					<tr>
						<th>특이사항</th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="lh_etc" id="lh_etc"><?php echo $edit['lh_etc'];?></textarea></td>
					</tr>
					<!--
					<tr>
						<th>첨부파일</th>
						<td>
							<div id="info_files">* 최대 <?//php echo CFG_UPLOAD_MAX_COUNT;?>개, 개당 <?//php echo (CFG_UPLOAD_MAX_FILE_SIZE/1024768);?>MB까지 업로드 가능합니다.</div>
							<div id="file_wrapper">
							<?//php
								// $file_name = $edit['file_name'];
								// $file_name_org = $edit['file_name_org'];
								// if(strpos($file_name_org, CFG_UPLOAD_FILE_NAME_DELIMITER) !== FALSE) {
								// 	$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name_org);
								// 	$arr_file_name_real = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name);
								// }
								// else {
								// 	$arr_file_name_real = array($file_name);
								// 	$arr_file_name_org = array('file_name_org'=>$file_name_org);
								// }

								// $file_index = 0;

								// if($res['kind'] == 'view'):
								// else:
								
								// 	foreach($arr_file_name_org as $file_name) {
							?>
								<div class="floatC" style="line-height:35px;margin-top:3px;" data-role-index="<?//php echo $file_index;?>">
									<div class="floatL cssFileForm">
										<input type="hidden" id="filename_real_<?//php echo $file_index;?>" value="<?//php echo $arr_file_name_real[$file_index]; ?>"></hidden>
										<input type="text" id="filename_<?//php echo $file_index;?>" class="floatL" readonly="readonly" value="<?//php echo $file_name; ?>" style="width:280px;background:none;border:none;">
										<div class="file_up" title="<?//php echo $file_name; ?>">
											<button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button>
											<input type="file" id="userfile_<?//php echo $file_index;?>" name="userfile_<?//php echo $file_index;?>" style="width:100%" onchange="javascript:chg_files(this, 'filename_<?//php echo $file_index;?>');">
										</div>
									</div>
									<div class="floatL marginL05" style="margin-top:-3px;">
										<button type="button" class="buttonS bGray cssAddFile">추가</button> 
										<?//php
										// if($res['kind'] == 'edit') {
										// 	echo('<button type="button" class="buttonS bGray cssDelFile" '. ($arr_file_name_real[$file_index] == '' ? 'disabled="disabled"' : '') .'>파일삭제</button>'. PHP_EOL);
										// 	echo('<button type="button" class="buttonS bGray cssDownload" data-role-real="'. $arr_file_name_real[$file_index] .'" data-role-org="'. $file_name .'" '. ($arr_file_name_real[$file_index] == '' ? 'disabled="disabled"' : '') .'>다운로드</button>');
										// }
										?>
									</div>
								</div>
							<?//php
								// 		$file_index++;
								// 	}

								// endif;
							?>
							</div>
						</td>
					</tr>
					-->
				</table>

				<div class="marginT10 textR">
					<button type="button" class="buttonM bGray cssPrint">인쇄</button>
					<button type="button" class="buttonM bSteelBlue cssAdd">등록</button>
					<button type="button" class="buttonM bOrange cssDel">삭제</button>
					<!-- <button type="button" class="buttonM bGray cssBack">뒤로이동</button> -->
					<button type="button" class="buttonM bDarkGray cssList">목록</button>
				</div>
				</form>

			</div>
			<!-- //컨텐츠 -->

			<a href="#" id="focusOuter" style="position:absolute;left:0px;top:0px;"></a>
		</div>
		<!-- //  contents_body  area -->
		
<?php
include_once './inc/inc_footer.php';
?>
