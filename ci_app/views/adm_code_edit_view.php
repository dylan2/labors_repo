<?php

$title = $data['kind'] == 'add' ? '등록' : '수정';

?>

<!-- Naver SE2 Editor -->
<script type="text/javascript" src="<?php echo CFG_BOARD_SE2_EDITOR_PATH;?>js/HuskyEZCreator.js" charset="utf-8"></script>

<script>
// 등록 구분: add, edit
var kind = "<?php echo $data['kind']; ?>";

// 등록,수정 공통사용
// 등록:m_code, 수정:s_code
var code = "<?php echo $data['code']; ?>";
// 어떤 페이지에서 호출했는지 구분자 : 권리구제유형코드, 나머지코드관리
var ref = "<?php echo $data['ref']; ?>";

$(document).ready(function(){
	// 관리할 상위 코드명
	var p_code_name = desc = sel_code_name;
	$op = $('select[id=lhKindList] option:selected');
	if($op.val()) {
		p_code_name = sel_code_name +' > ' + $op.attr('title');
		desc = $op.attr('title');
	}

	// 대코드 추출 및 노출
	$('#pop_m_code').text(p_code_name).css('font-weight', 'bold');
	// 해당 코드 설명용 문구
	$('input[name=desc]').val(desc);
	
	// 저장
	$('#pop_btnSubmit').click(function(e){
		e.preventDefault();
		// check index value
		var p_index = $('#txtIndex_pop').val();
		if(p_index == '' || !isNumeric(p_index)) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_03']);
			$('#txtIndex_pop').focus();
			return false;
		}
		// check a code name
		if($('#txtCodeName_pop').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_04']);
			$('#txtCodeName_pop').focus();
			return false;
		}
		// 설명글 사용시 체크시 내용 체크
		if($('input[name=code_desc_yn]:checked').val() == 'Y') {
			// 내용
			var cont = wyEditor.getById["code_desc"].getIR();
			if(cont == '' || cont == '<p>&nbsp;</p>' || cont == '<p><br></p>') {
				alert(CFG_MSG[CFG_LOCALE]['info_board_04'].replace('내용을', '설명을'));
				wyEditor.getById["code_desc"].exec("FOCUS");
				return false;
			}
		}
		
		// 수정인 경우
		if(kind == 'edit') {
			if(!confirm(CFG_MSG[CFG_LOCALE]['info_cmm_05'])) {
				return false; 
			}
		}

		// 에디터의 불필요한 태그 제거
		removeGargageTag(wyEditor, 'code_desc');
		
		var url = '/?c=code&m='+ kind;
		var rsc = $('#frmPopup').serialize() +'&ref='+ ref 
			+'&code='+ code +'&m_code='+ $('#m_code').val()
			+'&rdoUse=all&page='+_page;
		var fn_succes = function(data) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
			// set config total count
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			// list
			gen_list(data);
			closeLayerPopup();
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
			$('#txtIndex_pop').focus();
		};
		req_ajax(url, rsc, fn_succes, fn_error);
	});
	
	// 취소
	$('#pop_btnClose').click(function(e){
		closeLayerPopup();
	});

	//--------------------------------------------------------------------
	// for Naver StartEditor2 - Start
	var wyEditor = [];
	
	// setup the naver se2 editor
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: wyEditor,
		elPlaceHolder: "code_desc",
		sSkinURI: "<?php echo CFG_BOARD_SE2_EDITOR_PATH;?>/SmartEditor2SkinCustomButtons.html",	
		hideBeforeLoad: true, // 에디터 숨김
		htParams : {
			bUseBlocker: true,
			bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : false,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload : function(){}
		}, //boolean
		fOnAppLoad : function(){
			// 에디터 크기 조절
			$('.lyr_modal iframe').css({'width':'100%','height':'265px'});
			$('.lyr_modal iframe').contents().find('#smart_editor2').css({"min-width":"100px"});
			$('.lyr_modal iframe').contents().find('.husky_seditor_editing_area_container').css({"height":"207px"});
			$('.lyr_modal iframe').contents().find('#se2_iframe').css({"height":"207px"});
			var code_desc_data = $('#code_desc_data').val();
			if(code_desc_data) {
				wyEditor.getById["code_desc"].exec("PASTE_HTML", [ code_desc_data ]);
			}
			// 에디터 커버
			if($('input[name=code_desc_yn]:checked').val() == 'Y') {
				$('#editorCover').hide();
			}
			else {
				$('#editorCover').show();
			}
			// se2에디터의 하단 안내툴팁 숨김
			setTimeout(function(){
				$('.lyr_modal iframe').contents().find('.husky_seditor_resize_notice').hide();
			}, 1);
		},
		fCreator: "createSEditor2"
	});
	// for Naver StartEditor2 - End
	//--------------------------------------------------------------------	
	
	//개인상담,사용자상담 코드 중 상위코드 설명글 등록으로 호출한 경우, 순번,코드명,사용여부를 비활성화한다.
	if(ref == 'csl_mcode') {
		$('#txtIndex_pop').prop('disabled', true).css('background-color','#f4f4f4');
		$('#txtCodeName_pop').prop('disabled', true).css('background-color','#f4f4f4');
		$('input[name=rdoUse_pop]').prop('disabled', true);
		$('input[name=dsp_code]').closest('tr').hide();
	}
	else {
		$('#txtCodeName_pop').focus();
	}

	// 에디터 커버
	$('input[name=code_desc_yn]').change(function(){
		if($(this).val() == 'Y') {
			$('#editorCover').hide();
		}
		else {
			$('#editorCover').show();
		}	
	});
});
</script>

<div id="popup">
	<header id="pop_header">
		<h2 class="">코드<?php echo $title;?></h2>			
	</header>
	<form id="frmPopup" method="post">
	<div id="popup_contents">
		<div class="">
			<table class="tInsert" style="height:278px;table-layout: fixed;">
				<caption>코드등록입니다.</caption>
				<colgroup>
					<col style="width:20%;">
					<col style="width:30%;">
					<col style="width:20%;">
					<col style="width:30%;">
				</colgroup>
				<tr>
					<th>구분</th>
					<td>
						<span id="pop_m_code"></span>
						<!--<select id="board_select" class="styled">
							<option value="" selected>선택</option>
							<option value="">서울노동권익센터</option>
						</select>-->
						<input type="hidden" name="desc"></input>
					</td>
					<td colspan="2" rowspan="6" style="padding-bottom:0px;vertical-align:top;">
						<textarea id="code_desc" name="code_desc" style="width:100%;height:207px;border:0px;margin-left:-10px;"></textarea>
						<textarea id="code_desc_data" style="width:0;height:0;display:none;"><?php echo $data['code_desc']; ?></textarea>
						<div id="editorCover" style="display:none;position:relative;width:100%;height:255px;left:0;top:0;right:0;bottom:0;background:#cccccc;opacity:0.3;filter:alpha(opacity=0.3);-ms-filter:alpha(opacity=0.3);margin-top:-264px;"></div>
					</td>
				</tr>
				<tr>
					<th>코드명</th>
					<td><input type="text" id="txtCodeName_pop" name="txtCodeName_pop" style="width:150px" value="<?php echo $data['code_name']; ?>"></td>
				</tr>
				<tr>
					<th>순번</th>
					<td><input type="text" id="txtIndex_pop" name="txtIndex_pop" style="width:150px" value="<?php echo $data['dsp_order']; ?>"></td>
				</tr>
				<tr>
					<th>통계용 고유코드</th>
					<td><input type="text" id="dsp_code" name="dsp_code" style="width:150px" value="<?php echo $data['dsp_code']; ?>"></td>
				</tr>
				<tr>
					<th>사용여부</th>
					<td>
						<input type="radio" name="rdoUse_pop" id="rdoUse_pop1" value="1" class="imgM" <?php echo ($data['use_yn'] == 1 ? 'checked' : ''); ?> ><label for="rdoUse_pop1">사용</label>
						<input type="radio" name="rdoUse_pop" id="rdoUse_pop2" value="2" class="imgM" <?php echo ($data['use_yn'] == 0 ? 'checked' : ''); ?> ><label for="rdoUse_pop2">사용중지</label>
					</td>
				</tr>
				<tr>
					<th>설명팝업 사용여부</th>
					<td>
						<input type="radio" name="code_desc_yn" id="code_desc_yn1" value="Y" class="imgM" <?php echo ($data['code_desc_yn'] == 'Y' ? 'checked' : ''); ?> ><label for="code_desc_yn1">사용</label>
						<input type="radio" name="code_desc_yn" id="code_desc_yn2" value="N" class="imgM" <?php echo ($data['code_desc_yn'] == 'N' ? 'checked' : ''); ?> ><label for="code_desc_yn2">사용중지</label>
					</td>
				</tr>
			</table>
		</div>
	</div>
	</form>
	<div class="btn_set">
		<button type="button" id="pop_btnSubmit" class="buttonM bSteelBlue">등록</button>
		<button type="button" id="pop_btnClose" class="buttonM bGray">취소</button>
	</div>
</div>

