<script>
$(document).ready(function(){
	// 닫기버튼
	$('#btnClosePop').click(function(e){
		closeLayerPopup();
	});

	// 선택버튼
	$('#btnSubmitPop').click(function(e){
		var s_code = '';
		var code_name = '';
		$("input[name=csl_proc_rst]:checked").each(function() {
			var strArray = $(this).val().split('|');
			if(s_code != '') s_code += ', ';
			s_code += "'" + strArray[0]+ "'";

			if(code_name != '') code_name += ', ';
			code_name += "<" + strArray[1] + ">";
		});
		var oData = {
			s_code: s_code
			,code_name: code_name
		};
		closeLayerPopup(oData);
	});

	//전체 체크
	$('#check_all').click(function(){
		var chk_all = $(this).is(":checked");
		if(chk_all) $(".checked").prop('checked', true);
		else $(".checked").prop('checked', false);
	});
});
</script>
    <div id="popup">
        <header id="pop_header">
			<h2 class="">처리결과 리스트</h2>			
        </header>
		<div id="popup_contents" style="overflow-y: auto;-ms-overflow-y: auto; height:380px;">
<!--			<input type="hidden" id="kind" name="kind" value="<?php //echo $kind;?>"></input>
			<input type="hidden" id="s_code" name="s_code" value=""></input> -->
			<table class="tList">
				<colgroup>
					<col style="width:20%">
					<col style="width:80%">
				</colgroup>
				<tr>
					<th><input type="checkbox" id="check_all"></th>
					<th>처리결과</th>
				</tr>
				<?php
				foreach($res['csl_proc_rst'] as $item) {
					echo '<tr><td><input type="checkbox" name="csl_proc_rst" id="csl_proc_rst" class="checked" value="'. $item->s_code .'|'. $item->code_name.'"></td>';
					echo '<td>'. $item->code_name.'</td></tr>';
				}
				?>
			</table>
		</div>
		<div class="btn_set">
			<button type="button" id="btnClosePop" class="buttonM bGray">닫기</button>
			<button type="button" id="btnSubmitPop" class="buttonM bOrange">선택</button>
		</div>
    </div>