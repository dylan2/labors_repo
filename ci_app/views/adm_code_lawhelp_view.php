<?php
//
// 권리구제 유형 관리 전용 페이지
// 
// 
include_once "./inc/inc_header.php";
?>

<script>
// 선택한 대코드 이름
var sel_code_name = '';
// <권리구제> 중코드
var lh_m_code = "<?php echo base64_encode(CFG_SUB_CODE_OF_MANAGE_CODE_LAWLELP_KIND);?>";

$(document).ready(function(){

	// <권리구제> 유형관리 소코드(c) 신규 등록
	$('#btnAddMCode').click(function(e){
		var m = 'add_lhkind';
		$op = $('select[id=lhKindList] option:selected');
		var m_code = lh_m_code; // 신규
		if($op.val()) { // 수정
			m_code = $op.val();
			m = 'edit_lhkind';
		}
		var dsp_order = $('input[name=dsp_order]').val();
		if(!isNumeric(dsp_order)) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_03']);
			$('input[name=dsp_order]').focus();
			return false;
		}
		if(dsp_order == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_code_02']);
			return false;
		}
		else if($('input[name=code_name]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_code_03']);
			return false;
		}
		else if($('input:radio[name=use_yn]:checked').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_code_04']);
			return false;
		}
		else if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
			var url = '/?c=code&m='+ m;
			var rsc = $('form[name=frmForm]').serialize() +"&ref=lhmng&m_code="+ m_code
				+"&desc="+sel_code_name;
			var fn_succes = function(data) {
				if(data.rst == 'succ') {
					genLhMCodeList(data);
				}
				else {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
					if(is_local) objectPrint(data);
				}
				$('#btnClearMCode').click();
			};
			var fn_error = function(data) {
				if(is_local) objectPrint(data);
			
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			};
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});

	// <권리구제> 유형관리 세코드(B) 저장
	$('#btnAdd').click(function(e){
		e.preventDefault();
		$op = $('select[id=lhKindList] option:selected');
		if($op.val()) {
			var url = '/?c=code&m=add_view&ref=lhmng&code='+ $op.val();
			gLayerId = openLayerModalPopup(url, 400, 330, '', '', '', '', 1, true);
		}
		else {
			alert(CFG_MSG[CFG_LOCALE]['info_code_01']);
		}
	});

	// <권리구제> 소코드(C) select 선택시 입력창, 하위 세코드(B) 목록 생성
	$('select[id=lhKindList]').click(function(e){
		$op = $(this).find('option:selected');
		if($op.val()) {
			$('input[name=dsp_order]').val($op.attr('data-dsp-order'));
			$('input[name=code_name]').val($op.attr('title'));
			$('input:radio[name=use_yn]').eq($op.attr('data-useyn')-1).prop('checked', true);
			$('#m_code').val($op.val());
			// 하위목록 생성
			get_list(1);
		}
	});

	// <권리구제> 유형명 리스트 생성
	var genLhMCodeList = function(data) {
		if(data.tot_cnt > 0) {
			$('select[id=lhKindList]').empty();
			$.each(data.data, function(i,o){
				var code_nm = (o.use_yn==1?'':'[중지] ')+ o.code_name;
				var op = "<option title='"+ o.code_name// +'/'+o.s_code
					+"' value='"+ Base64.encode(o.s_code)
					+"' data-dsp-order='"+ o.dsp_order 
					+"' data-useyn='"+ o.use_yn
					+"'>"+ code_nm +"</option>";
				$('select[id=lhKindList]').append(op);
			});
		}
		else {
			var op = "<option value=''>데이터가 없습니다.</option>";
			$('select[id=lhKindList]').empty();
			$('select[id=lhKindList]').append(op);
		}
	};

	// 초기화 버튼
	$('#btnClearMCode').click(function(e){
		e.preventDefault();
		$('form[name=frmForm]')[0].reset();
		$('select[id=lhKindList] option').prop('selected', false);
		$('#m_code').val("");
		gen_empty_list();
	});
	
	// 전송버튼
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		get_list(_page);
	});
	
	// 사용.사용중지, 삭제 버튼 이벤트 핸들러
	$('#btnUse,#btnNotUse,#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var s_code = '';
			$('input[name=chk_item]:checked').each(function(i){
				if(s_code != '') s_code +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				s_code += $(this).attr('data-role-id');
			});
			
			var kind = 'using';
			if($(this).text() == '중지') {
				kind = 'not_using';
			}
			else if($(this).text() == '삭제') {
				kind = 'del_multi';
				if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
					return false;
				}
			}
			req_code_manage(kind, s_code);
		}
	});
	
	// 검색 - 검색어 엔터 처리
	$("#txtKeyword").keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$("#btnSubmit").click();
		}
	});
		
	// 대코드 이름 노출
	$('a.cls_menu').each(function(index){
		if($(this).css('font-weight') == '700') { // bold
			sel_code_name = $(this).text();
			if(sel_code_name) {
				sel_code_name = sel_code_name.split(' ')[1];
				$('#code_text').text(sel_code_name);
				return false;
			}
		}
	});
	
	// <권리구제> 유형관리 소코드(c) 가져오기
	var getAllCode = function() {
		var url = '/?c=code&m=code_list_lh_ccode';
		var rsc = "m_code="+ lh_m_code;
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				genLhMCodeList(data);
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
				if(is_local) objectPrint(data);
			}
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
		
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
		};
		req_ajax(url, rsc, fn_succes, fn_error);
	};

	// 코드 조회
	getAllCode();
	gen_empty_list();
});



/**
 * 보기/수정/삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, code) {
	
	var url = '/?c=code&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&s_code='+ code;
	var fn_succes = function(data) {
		// 삭제, 다중삭제 시
		if(type == 'del' || type == 'del_multi') {
			if(data.used_code == 'y') {//삭제할 코드가 상담에 사용중이면
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_07']);
			}
		}
		else {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
		}
		// set config total count
		_cfg_pagination.total_item = data.tot_cnt;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// list
		gen_list(data);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
	
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_empty_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}

var gen_empty_list = function() {
	_cfg_pagination.total_item = 1;
	_cfg_pagination.currentPage = 1;
	_cfg_pagination.linkFunc = 'get_list';
	_pagination = new Pagination(_cfg_pagination);
	gen_list();
};

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// page
	_page = page;
	
	var url = '/?c=code&m=code_list';
	var rsc = $('#frmSearch').serialize() +'&page='+_page;
	var fn_succes = function(data) {
		if(data && data.tot_cnt > 0) {
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.itemPerPage = 10;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			gen_list(data);
		}
		else {
			gen_empty_list();
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
	
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_empty_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	if(!_pagination) {
		_pagination = new Pagination(_cfg_pagination);
	}

	var html_b = '<table class="tList">';
	html_b += '<caption>코드관리 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:6%">';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:24%">';
	html_b += '<col style="width:20%">';
	html_b += '<col style="width:14%">';
	html_b += '<col style="width:20%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th><input type="checkbox" class="imgM checkAll"></th>';
	html_b += '<th>번호</th>';
	html_b += '<th>순서</th>';
	html_b += '<th>구분</th>';
	html_b += '<th>사용여부</th>';
	html_b += '<th>등록일</th>';
	html_b += '<th>관리</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			html_b += '<tr>';
			html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ Base64.encode(data.data[index].s_code) +'"></td>';
			html_b += '  <td>'+ (no--) +'</td>';
			html_b += '  <td>'+ data.data[index].dsp_order +'</td>';
			html_b += '  <td>'+ data.data[index].code_name +'</td>';
			var use_yn = '<span class="labelG">중지</span>';
			if(data.data[index].use_yn == 1) {
				use_yn = '<span class="labelB">사용</span>';
			}
			html_b += '  <td>'+ use_yn +'</td>';
			html_b += '  <td>'+ data.data[index].reg_date +'</td>';	
			html_b += '  <td>';
			// html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(data.data[index].s_code) +'">보기</button>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(data.data[index].s_code) +'">수정</button>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(data.data[index].s_code) +'">삭제</button>';
			html_b += '  </td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="7" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// list html
	$('#list').empty().append('<div class="list_no"> 전체 '+ total_cnt + '개</div>').append(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
	
	// 관리버튼 클릭 이벤트
	$('#list').off('click', 'button.ai_cmd');
	$('#list').on('click', 'button.ai_cmd', function(e){
		e.preventDefault();
		var s_code = $(this).attr('data-role-id');
		var cmd = $(this).text();
		if(cmd == '삭제') {
			if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				req_code_manage('del', s_code);
			}
		}
		else {
			var url = '/?c=code&m=edit_view&ref=lhmng&code='+ s_code;
			gLayerId = openLayerModalPopup(url, 400, 330, '', '', '', '', 1, true);
		}
	});
	
	// 체크박스 전체 선택
	$('#list').on('click', 'input.checkAll', function(e){
		var status = $(this).prop('checked');
		$("input[name=chk_item]:checkbox").each(function(i) {
			$(this).prop("checked", status);
		});
	});
	
	// 버튼 컨테이너 show
	$('#btnContainer').show();
}

</script>
<style type="text/css">
.tblKindMng .tInsert th, .tInsert td {
	padding: 5px 10px;
}
</style>

<?php
include_once './inc/inc_menu.php';
?>
		
		
		<!--- //lnb 메뉴 area ---->		
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">코드관리</h2>
				<span class="location">홈 > 코드관리 > <em id="code_text"></em></span>
			</div>
			<div class="cont_area">
				<div class="code_info_area">
					<div class="left" id="mngContainer">
						<div class="con_text leftBox">
							<div style="float:left">
								<div class="l_title" style="padding-bottom:5px;">유형명</div>
								<div>
								<select id="lhKindList" multiple="multiple" style="width:150px;height:117px;">
								</select>
								</div>
							</div>
							<div style="float:left;margin:0 20px 0 28px;border-left:1px solid #ccc;height:100%;"></div>
							<div style="float:left;margin-left: 10px;">
								<div class="l_title" style="padding-bottom:5px;">유형관리</div>
								<div>
									<form name="frmForm" method="post">	
									<table class="tInsert tblKindMng">
										<colgroup>
											<col style="width:30%">
											<col style="width:70%">
										</colgroup>
										<tbody>
											<tr>
												<th>순번</th>
												<td>
													<input type="text" name="dsp_order" class="imgM" size="10" maxlength="30" value="">
												</td>
											</tr>
											<tr>
												<th>유형명</th>
												<td>
													<input type="text" name="code_name" class="imgM" size="10" maxlength="30" value="">
												</td>
											</tr>
											<tr>
												<th>사용여부</th>
												<td>
													<input type="radio" name="use_yn" id="use_yn1" value="1" class="imgM" checked><label for="use_yn1">사용</label>
													<input type="radio" name="use_yn" id="use_yn2" value="2" class="imgM"><label for="use_yn2">사용중지</label>
												</td>
											</tr>
										</tbody>
									</table>
									</form>
								</div>
							</div>
							<div style="float:left;margin:71px 0 0 15px;">
								<button type="button" class="buttonM bSteelBlue" id="btnAddMCode" style="margin-bottom:1px;">등록</button><br>
								<button type="button" class="buttonM bGray" id="btnClearMCode" style="padding:7px 9px;">초기화</button>
							</div>
						</div>
					</div>	
					<div class="right">
						<div class="con_text">
						<form name="frmSearch" id="frmSearch" method="post">
							<label for="board_select" class="l_title">구분</label>
							<input type="text" id="txtKeyword" name="keyword" style="width:60%">
							<input type="hidden" name="m_code" id="m_code" value="">
							<span class="divice"></span>
							<label for="board_rows" class="l_title">사용여부</label>
							<span style="padding:8px 0;display:inline-block">
								<input type="radio" name="rdoUse" id="rdoUse0" value="all" class="imgM" checked><label for="rdoUse0">전체</label>
								<input type="radio" name="rdoUse" id="rdoUse1" value="1" class="imgM"><label for="rdoUse1">사용</label>
								<input type="radio" name="rdoUse" id="rdoUse2" value="2" class="imgM"><label for="rdoUse2">사용중지</label>
							</span>
							<span class="divice"></span>
							<div class="textR marginT10">
								<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
							</div>
						</form>
						</div>
					</div>
				</div>
				
				<!-- list -->
				<div id="list"></div>
		
				<div id="info_label" style="display:none;color:red;font-weight:bold;line-height:40px;">상담내역공유 코드는 코드값을 고정하여 사용하기 때문에 등록/수정/삭제시 문제가 발생되어 기능을 제한합니다.</div>

				<div class="floatC marginT10" id="btnContainer" style="display:none;">
					<button type="button" class="buttonM bGray floatL" id="btnUse">사용</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnNotUse">중지</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>
					<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">등록</button>
				</div>

				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>
				
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		
		
<?php
include_once './inc/inc_footer.php';
?>
