<?php
/**
 * 
 * 귄리구제지원에서 사용하는 사건유형 등록/수정 전용 팝업 페이지
 * 
 */

$title = $data['kind'] == 'add' ? '등록' : '수정';

?>
<!doctype html>
<html lang="ko">
<head>
<script>
// 등록 구분: add, edit
var kind = "<?php echo $data['kind']; ?>";

// 등록,수정 공통사용
// 등록:m_code, 수정:s_code
var code = "<?php echo $data['code']; ?>";
// 어떤 페이지에서 호출했는지 구분자 : 권리구제유형코드, 나머지코드관리
var ref = "<?php echo $data['ref']; ?>";

$(document).ready(function(){
	// 관리할 상위 코드명
	var p_code_name = desc = sel_code_name;
	$op = $('select[id=lhKindList] option:selected');
	if($op.val()) {
		p_code_name = sel_code_name +' > ' + $op.attr('title');
		desc = $op.attr('title');
	}

	// 대코드 추출 및 노출
	$('#pop_m_code').text(p_code_name).css('font-weight', 'bold');
	// 해당 코드 설명용 문구
	$('input[name=desc]').val(desc);
	
	// 저장
	$('#pop_btnSubmit').click(function(e){
		e.preventDefault();
		// check index value
		var p_index = $('#txtIndex_pop').val();
		if(p_index == '' || !isNumeric(p_index)) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_03']);
			$('#txtIndex_pop').focus();
			return false;
		}
		// check a code name
		if($('#txtCodeName_pop').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_04']);
			$('#txtCodeName_pop').focus();
			return false;
		}
		
		// 수정인 경우
		if(kind == 'edit') {
			if(!confirm(CFG_MSG[CFG_LOCALE]['info_cmm_05'])) {
				return false; 
			}
		}
		
		var url = '/?c=code&m='+ kind;
		var rsc = $('#frmPopup').serialize() +'&ref='+ ref 
			+'&code='+ code +'&m_code='+ $('#m_code').val()
			+'&rdoUse=all&page='+_page;
		var fn_succes = function(data) {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
			// set config total count
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			// list
			gen_list(data);
			closeLayerPopup();
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
			$('#txtIndex_pop').focus();
		};
		req_ajax(url, rsc, fn_succes, fn_error);
	});
	
	// 취소
	$('#pop_btnClose').click(function(e){
		closeLayerPopup();
	});

	$('#txtIndex_pop').focus();
});
</script>
</head>

<body id="popup_body">   
  <div id="popup">
		<header id="pop_header">
			<h2 class="">코드<?php echo $title;?></h2>			
		</header>
		<form id="frmPopup" method="post">
		<div id="popup_contents">
			<div class="">
				<table class="tInsert">
					<caption>코드등록 목록입니다.</caption>
					<colgroup>
						<col style="width:30%">
						<col style="width:70%">
					</colgroup>
					<tr>
						<th>구분</th>
						<td>
							<span id="pop_m_code"></span>
							<!--<select id="board_select" class="styled">
								<option value="" selected>선택</option>
								<option value="">서울노동권익센터</option>
							</select>-->
							<input type="hidden" name="desc"></input>
						</td>
					</tr>
					<tr>
						<th>순번</th>
						<td><input type="text" id="txtIndex_pop" name="txtIndex_pop" style="width:150px" value="<?php echo $data['dsp_order']; ?>"></td>
					</tr>
					<tr>
						<th>코드명</th>
						<td><input type="text" id="txtCodeName_pop" name="txtCodeName_pop" style="width:150px" value="<?php echo $data['code_name']; ?>"></td>
					</tr>
					<tr>
						<th>사용여부</th>
						<td>
							<input type="radio" name="rdoUse_pop" id="rdoUse_pop1" value="1" class="imgM" <?php echo ($data['use_yn'] == 1 ? 'checked' : ''); ?> ><label for="rdoUse_pop1">사용</label>
							<input type="radio" name="rdoUse_pop" id="rdoUse_pop2" value="2" class="imgM" <?php echo ($data['use_yn'] == 0 ? 'checked' : ''); ?> ><label for="rdoUse_pop2">사용중지</label>
						</td>
					</tr>
				</table>
			</div>
		</div>
		</form>
		<div class="btn_set">
			<button type="button" id="pop_btnSubmit" class="buttonM bSteelBlue">등록</button>
			<button type="button" id="pop_btnClose" class="buttonM bGray">취소</button>
		</div>
    </div>
</body>
</html>
