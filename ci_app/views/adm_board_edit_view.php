<?php
include_once "./inc/inc_header.php";

//공유게시판
$brd_name = '운영';
if($brd_id == '2'):
	$brd_name = '주요상담사례';
elseif($brd_id == '3'):
	$brd_name = 'FAQ';
endif;
?>

<?php 
// 수정 : 운영게시판인 경우 wysiwyg 기능 추가
// 운영게시판인 경우
if($brd_id == 1):
?>
<!-- Naver SE2 Editor -->
<script type="text/javascript" src="<?php echo CFG_BOARD_SE2_EDITOR_PATH;?>js/HuskyEZCreator.js" charset="utf-8"></script>
<?php
endif;
?>



<script>
var brd_id = '<?php echo $brd_id; ?>';

var kind = '<?php echo $kind; ?>';
if(!kind) kind = 'add';


$('document').ready(function(){
	if(kind == 'view') {
		$('#btnSubmit').css('display', 'none');
		$('#info_files').css('display', 'none');
		$('#info_files_valid_type').css('display', 'none');

		// file download
		$('button.cssDownload').click(function(){
			var real = $(this).attr('data-role-real');
			var org = $(this).attr('data-role-org');
			// request
			$.ajax({
				url: '/?c=board&m=chk_download'
				,data: {real:real, org:org}
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					if(data.rst == 'fail') {
						alert(CFG_MSG[CFG_LOCALE]['warn_board_01']);
					}
					else {
						window.location.href = "/?c=board&m=download&real="+ real +"&org="+ org;
					}
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
					
					var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
					if(data && data.msg) msg += '[' + data.msg +']';
					alert(msg);
				}
			});
		});
	}
	else {
		// 업로드 가능한 파일 종류 안내
		$('#info_files_valid_type').text( "* 업로드 가능한 파일 종류 : "+ valid_file_types.join(", "));
	}

	// 공지글 체크박스 이벤트 핸들러
	$('#notice_yn').click(function(e){
		$('#notice_dt_begin').attr('disabled', !$(this).prop('checked'));
		$('#notice_dt_end').attr('disabled', !$(this).prop('checked'));
	});
	// 수정 - 공지글이 체크가 되어 있으면
	if($('input:checkbox[id=notice_yn]:checked')) {
		$('#notice_dt_begin').attr('disabled', false);
		$('#notice_dt_end').attr('disabled', false);
	}

	// 파일 추가 버튼 이벤트 핸들러
	var new_btn_index = $('.cssAddFile').length;
	var cfg_upload_max_count = '<?php echo CFG_UPLOAD_MAX_COUNT;?>';
	$('#file_wrapper').on('click', '.cssAddFile', function(e){
		if(new_btn_index >= cfg_upload_max_count) {
			var limit_msg = (CFG_MSG[CFG_LOCALE]['info_board_06']).replace('x', cfg_upload_max_count);
			alert(limit_msg);
			return false;
		}
		// 태그 생성
		append_file_tag_block(new_btn_index);
		new_btn_index++;
		e.preventDefault();
	});

	// 추가한 항목 제거 이벤트 핸들러
	$('#file_wrapper').on('click', '.btnRremove', function(e){
		if(new_btn_index <= 1) {
			e.preventDefault();
			return false;
		}
		
		$(this).parents('.floatC').remove();
		new_btn_index--;
		e.preventDefault();
	});

	// 공지글 여부
	if($("#notice_yn").prop('checked') == false) {
		$('#notice_dt_begin').val('');
		$('#notice_dt_end').val('');
	}

	// 파일 삭제
	$('#file_wrapper').on('click', '.cssDelFile', function(e){
		if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_02'])) {
			$this = $(e.target);
			var index = $this.parents('.floatC').attr('data-role-index');
			var seq = $('#seq').val();
			var file_name_real = $('#filename_real_'+ index).val();
			// request
			$.ajax({
				url: '/?c=board&m=del_file'
				,data: {sq:seq, bid:brd_id, fnr:file_name_real}
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
					//
					$(this).attr('disabled', 'true');
					$('#filename_'+ index).val('');
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
					
					var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
					if(data && data.msg) msg += '[' + data.msg +']';
					alert(msg);
					$('#txtIndex_pop').focus();
				}
			});
		}
	});

	// 리스트
	$('#btnList').click(function(e){
		e.preventDefault();
		check_auth_redirect($('#'+ sel_menu).find('li>a').eq('<?php echo ($brd_id-1);?>'), 'list', '');
	});

	// 전송버튼 클릭시 form onSubmit 이벤트 발생처리
	$('#btnSubmit').click(function(e){
		$('form').submit();
		e.preventDefault();
	});
	
	//
	// form 전송
	$('form').submit(function(e) {
		e.preventDefault();

		<?php
			// 운영게시판인 경우
			if($brd_id == 1):
		?>
		oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);	// 에디터의 내용이 textarea에 적용됩니다.
		<?php
			endif;
		?>
		
		// validation
		// 공지글 여부
		if($("#notice_yn").prop('checked')) {
			if($('#notice_dt_begin').val() == '' || $('#notice_dt_end').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_board_01']);
				if($('#notice_dt_begin').val() == '') {
					$('#notice_dt_begin').focus();
					return false;
				}
				if($('#notice_dt_end').val() == '') {
					$('#notice_dt_end').focus();
					return false;
				}
			}
		}
		// 상담방법
		if($("#csl_way option:selected").val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_board_02']);
			$('#csl_way').focus();
			return false;
		}
		// 제목
		if($('#title').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_board_03']);
			$('#title').focus();
			return false;
		}
		// 내용
		<?php

		// 운영게시판인 경우
		if($brd_id == 1):
		?>
		cont = oEditors.getById["content"].getIR();
		if(cont == '' || cont == '<p>&nbsp;</p>' || cont == '<p><br></p>') {
			alert(CFG_MSG[CFG_LOCALE]['info_board_04']);
			oEditors.getById["content"].exec("FOCUS");
			return false;
		}
		<?php
		else:
		?>
		if($('#content').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_board_04']);
			$('#content').focus();
			return false;
		}
		<?php
		endif;

		?>

		//
		var confirmMsg = 'info_cmm_05'; // edit
		if(kind == 'add') confirmMsg = 'info_cmm_06';

		// 전송
		if(confirm(CFG_MSG[CFG_LOCALE][confirmMsg])) {
			// for HTML5 browsers
			if(window.FormData !== undefined)  {
				// 파일 업로드 : 파일 추가(변경)된 개수
				$('#chg_file_cnt').val(chg_file_cnt);
				// 변경된 실제 파일명들
				$('#chg_real_file_name').val(chg_file_name_real);
				//
				var formData = new FormData(this);
				$.ajax({
					url: '/?c=board&m=processing',
					type: 'POST',
					data:  formData,
					mimeType:"multipart/form-data",
					contentType: false,
					cache: false,
					processData:false,
					success: function(data, textStatus, jqXHR) {
						var obj = JSON.parse(data);
						if(obj.rst == 'succ') {
							alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
							check_auth_redirect($('#'+ sel_menu).find('li>a').eq('<?php echo ($brd_id-1);?>'), 'list', '');
						}
						else {
							if(is_local) objectPrint(data);
							if(is_local) objectPrint(jqXHR);
							if(is_local) console.log('textStatus : '+ textStatus);
							alert(obj.msg);
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						if(is_local) objectPrint(jqXHR);
						if(is_local) objectPrint(textStatus);
						if(is_local) objectPrint(errorThrown);
					
						var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
						if(data && data.msg) msg += '[' + data.msg +']';
						alert(msg);
					}
			   });
		   }
		   // for olden browsers
		   else {
				//generate a random id
				var  iframeId = 'unique' + (new Date().getTime());
		 
				//create an empty iframe
				var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
		 
				//hide it
				iframe.hide();
		 
				//set form target to iframe
				formObj.attr('target',iframeId);
		 
				//Add iframe to body
				iframe.appendTo('body');
				iframe.load(function(e) {
					var doc = getDoc(iframe[0]);
					var docRoot = doc.body ? doc.body : doc.documentElement;
					var data = docRoot.innerHTML;
					if(is_local) objectPrint(data);
					//data is returned from server.
				});
			}
		}
	});


	<?php
		// 운영게시판인 경우
		if($brd_id == 1):
			$cont = str_replace("\r", "", str_replace("\n", "<br>", $res['content']));
	?>
	var tmp_content = $('#tmp_content').val();

	//--------------------------------------------------------------------
	// for Naver StartEditor2 - Start
	var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
		oAppRef: oEditors,
		elPlaceHolder: "content",
		sSkinURI: "<?php echo CFG_BOARD_SE2_EDITOR_PATH;?>SmartEditor2Skin.html",	
		htParams : {
			bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
			fOnBeforeUnload : function(){
				//alert("완료!");
			}
		}, //boolean
		fOnAppLoad : function(){
			if(tmp_content) {
				// se2 에디터의 기본 입력 내용인 '<p>&nbsp;</p>' 제거
				tmp_content = tmp_content.replace('<p>'+ String.fromCharCode(160) +'</p>', '').replace('<p>&nbsp;</p>', '');
				// console.log(tmp_content);
				oEditors.getById["content"].exec("PASTE_HTML", [tmp_content]);
			}
		},
		fCreator: "createSEditor2"
	});
	// for Naver StartEditor2 - End
	//--------------------------------------------------------------------
	<?php
		endif;
	?>
});

// jQuery form submit helper
function getDoc(frame) {
	var doc = null;

	// IE8 cascading access check
	try {
		if (frame.contentWindow) {
			doc = frame.contentWindow.document;
		}
	} catch(err) {
	}

	if (doc) { // successful getting content
		return doc;
	}

	try { // simply checking may throw in ie8 under ssl or mismatched protocol
		doc = frame.contentDocument ? frame.contentDocument : frame.document;
	} catch(err) {
		// last attempt
		doc = frame.document;
	}
	return doc;
}

// 파일 변경 여부 플래그
var chg_file_cnt = 0;
// 변경된 실제 파일명 - 삭제할 대상
var chg_file_name_real = '';
// 파일 개당 업로드 최대 용량
var file_upload_max_size = '<?php echo CFG_UPLOAD_MAX_FILE_SIZE;?>';
// 업로드 가능한 파일 종류
var valid_file_types = ["bmp", "gif", "png", "jpg", "jpeg", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "pdf", "zip"];

// 보안 강화를 위해 업로드 가능한 파일 체크 루틴 추가 - 2017.09.13 dylan, danvistory.com 
var is_valid_file = function(name) {
	var ext = name.substring(name.lastIndexOf(".") + 1, name.length).toLowerCase();
  var rst = false;
  for (var i = 0; i < valid_file_types.length; i++) {
		if (ext == valid_file_types[i]) {
			rst = true;
			break;
		}
	}
  return rst;
};

// 기존 file block html을 제거하고 새로 생성한다.
var rebuild_file_block = function($this, ele_name) {
	// html을 제거한뒤
	var index = $($this).parents('.floatC').attr('data-role-index');
	var real_name = $($this).parents('.cssFileForm').find('input:first').eq(0).val();
	var ele_name_val = document.getElementById(ele_name).value;
	$($this).parents('.floatC').remove();
	// 새로 생성
	append_file_tag_block(index, real_name);
	
	// 기존 값이 있으면 대입
	if(ele_name_val != '') {
		// 제거 버튼 제거
		$('#filename_'+ index).parents('.floatC').find('div>.btnRremove').remove();
		$('#filename_'+ index).val( ele_name_val );
		$('#filename_'+ index).parents('.floatC').find('div:last').append('<button type="button" class="buttonS bGray cssDelFile">파일삭제</button>');
	}
};


// file 선택시 파일 처리
function chg_files($this, ele_name) {
	// file type 체크
	if( ! is_valid_file($($this)[0].files[0].name)) {
		alert(CFG_MSG[CFG_LOCALE]['info_board_08']);
		
		rebuild_file_block($this, ele_name);
		
		return false;
	}

	// file size 체크(선택한 파일이 용량 초과)
	var obj_ele = document.getElementById(ele_name);
	var sel_file_size = $($this)[0].files[0].size;
	if(sel_file_size >= file_upload_max_size) {
		// 
		var info_size = numberToCurrency(Math.floor(parseInt(sel_file_size)/1024768));
		var info_msg = (CFG_MSG[CFG_LOCALE]['info_board_07']).replace('x', info_size);
		alert(info_msg);

		rebuild_file_block($this, ele_name);

		return false;
	}

	// 수정일 경우 - 기존 파일명과 선택한 파일명이 같은지 체크, 같으면 업로드하지 않도록 한다.
	var matches = '';
	if(obj_ele.value) {
		matches = $this.value.match(eval('/'+ obj_ele.value +'/g'));
	}
	// 파일명이 같으면 업로드하지 않는다
	if(matches) {
		alert(CFG_MSG[CFG_LOCALE]['info_board_05']);
		
		return false;
	}

	// 정상인 경우
	// 기존 파일이 있는 경우만(처음 업로드시 파일 선택후 다시 선택해도 change 이벤트가 일어난다)
	if(obj_ele.value != '') {
		chg_file_cnt++;
	}
	obj_ele.value = $this.value;

	// 파일이 변경되면, 기존 파일 삭제를 위해 파일이름을 조합한다.
	var real_file_name = ele_name.replace('_', '_real_');
	if(chg_file_name_real != '') chg_file_name_real += '<?php echo CFG_UPLOAD_FILE_NAME_DELIMITER; ?>';
	chg_file_name_real += document.getElementById(real_file_name).value;	
}


/**
  * 파일추가 html 태그 생성
  */
function append_file_tag_block(index, name) {
	var real_name = name ? name : '';
	$('#file_wrapper').append('<div class="floatC" data-role-index="'+ index +'" style="line-height:35px;margin-top:3px;"><div class="floatL cssFileForm"><input type="hidden" id="filename_real_'+ index +'" value="'+ real_name +'"></hidden><input type="text" id="filename_'+ index +'" class="floatL" readonly="readonly" style="width:280px;background:none;border:none;"><div class="file_up" ><button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button><input type="file" id="userfile_'+ index +'" name="userfile_'+ index +'" style="width:100%" onchange="javascript:chg_files(this, \'filename_'+ index +'\');"></div></div><div class="floatL marginL05" style="margin-top:-3px;"><button type="button" class="buttonS bGray cssAddFile">추가</button>&nbsp;<button type="button" class="buttonS bGray btnRremove">제거</button></div></div>');
}
</script>

<?php
include_once './inc/inc_menu.php';
?>

		<!--- //lnb 메뉴 area ---->		
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit"><?php echo $brd_name; ?> 게시판</h2>
				<span class="location">홈 > 게시판 > <em><?php echo $brd_name; ?> 게시판</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post" enctype="multipart/form-data">
				<input type="hidden" name="kind" value="<?php echo $res['kind']; ?>"></input>
				<input type="hidden" name="seq" id="seq" value="<?php echo $res['seq']; ?>"></input>
				<input type="hidden" name="brd_id" value="<?php echo $brd_id; ?>"></input>
				<input type="hidden" name="chg_file_cnt" id="chg_file_cnt" value="0"></input>
				<input type="hidden" name="chg_real_file_name" id="chg_real_file_name" value=""></input>
				<table class="tInsert">
					<caption>게시판 입력폼 입니다.</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>* 제목</th>
						<td>
						<?php
						if($kind == 'view'):
							echo $res['title'];
						else:
						?>
							<input type="text" id="title" name="title" style="width:90%" value="<?php echo $res['title']; ?>">
						<?php
						endif;
						?>
						</td>
					</tr>
					<?php
					if($brd_id == '1'):
					?>
					<tr>
						<th>공지기간</th>
						<td>
						<?php
						if($kind == 'view'):
							if($res['notice_yn'] == '1'):
								echo $res['notice_dt_begin'] .' ~ '. $res['notice_dt_end'];
							endif;

							if($res['popup_yn'] == '1') :
						?>
							<div style="margin:-20px 0 0 200px;">
								<span class="icon_check"></span>
								<span style="width:150px;margin-left:-8px;">팝업공지</span>
							</div>
						<?php
							endif;
						else:
						?>
							<input type="checkbox" id="notice_yn" name="notice_yn" class="imgM" <?php echo $res['notice_yn'] == '1' ? 'checked' : ''; ?> > 
							<input type="text" id="notice_dt_begin" name="notice_dt_begin" value="<?php echo $res['notice_dt_begin']; ?>" class="datepicker date" style="width:90px" disabled> ~ 
							<input type="text" id="notice_dt_end" name="notice_dt_end" value="<?php echo $res['notice_dt_end']; ?>" class="datepicker date" style="width:90px" disabled>
							<div style="margin:-26px 0 0 275px;">
								<input type="checkbox" id="popup_yn" name="popup_yn" class="imgM" <?php echo $res['popup_yn'] == '1' ? 'checked' : ''; ?> ><label for="popup_yn">팝업여부</label> <span style="color:#ec8025">(공지글로 체크시 공지 기간 동안 팝업으로 노출됩니다. 공지글이 아닌 경우는 기간제한이 없습니다.)</span>
							</div>
						<?php
						endif;
						?>
						</td>
					</tr>
					<?php
					else:
					?>
					<tr>
						<th>* 상담방법</th>
						<td>
						<?php
						if($kind == 'view'):
							echo $res['code_name'];
						else:
						?>
							<select name="csl_way" id="csl_way" class="styled" style="width:150px">
								<option value="" selected>선택</option>
								<?php
								foreach($res['csl_way'] as $rs) {
									echo '<option value="'. base64_encode($rs->s_code) .'" '. ($res['s_code'] == $rs->s_code ? 'selected' : '') .'>'. $rs->code_name .'</option>';
								}
								?>
							</select>
						<?php
						endif;
						?>
						</td>
					</tr>
					<?php
					endif;
					?>
					<tr>
						<th>작성자</th>
						<td>
							<?php
								echo $res['oper_name'] != '' ? $res['oper_name'] :  $oper_name;
							?>
							<input type="hidden" name="oper_id" id="oper_id" value="<?php echo $res['oper_id'] != '' ? $res['oper_id'] :  $oper_id; ?>"></input>
						</td>
					</tr>
					<tr>
						<th>첨부파일</th>
						<td>
							<div id="info_files">* 최대 <?php echo CFG_UPLOAD_MAX_COUNT;?>개, 개당 <?php echo (CFG_UPLOAD_MAX_FILE_SIZE/1024768);?>MB까지 업로드 가능합니다.</div>
							<div id="info_files_valid_type"></div>
							<div id="file_wrapper">
							<?php
								$file_name = $res['file_name'];
								$file_name_org = $res['file_name_org'];
								if(strpos($file_name_org, CFG_UPLOAD_FILE_NAME_DELIMITER) !== FALSE) {
									$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name_org);
									$arr_file_name_real = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name);
								}
								else {
									$arr_file_name_real = array($file_name);
									$arr_file_name_org = array('file_name_org'=>$file_name_org);
								}

								$file_index = 0;
								if($kind == 'view'):
									if($file_name_org != ''):
										foreach($arr_file_name_org as $file_name) {
								?>
								<div class="floatC" style="line-height:35px;">
									<button type="button" class="buttonS bGray cssDownload" data-role-real="<?php echo $arr_file_name_real[$file_index];?>" data-role-org="<?php echo $file_name; ?>">다운로드</button>
									<?php echo $file_name; ?>
								</div>
								
								<?php
											$file_index++;
										}
									endif;
								else:
								
									foreach($arr_file_name_org as $file_name) {
							?>
								<div class="floatC" style="line-height:35px;margin-top:3px;" data-role-index="<?php echo $file_index;?>">
									<div class="floatL cssFileForm">
										<input type="hidden" id="filename_real_<?php echo $file_index;?>" value="<?php echo $arr_file_name_real[$file_index]; ?>"></hidden>
										<input type="text" id="filename_<?php echo $file_index;?>" class="floatL" readonly="readonly" value="<?php echo $file_name; ?>" style="width:280px;background:none;border:none;">
										<div class="file_up" title="<?php echo $file_name; ?>">
											<button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button>
											<input type="file" id="userfile_<?php echo $file_index;?>" name="userfile_<?php echo $file_index;?>" style="width:100%" onchange="javascript:chg_files(this, 'filename_<?php echo $file_index;?>');">
										</div>
									</div>
									<div class="floatL marginL05" style="margin-top:-3px;">
										<button type="button" class="buttonS bGray cssAddFile">추가</button> 
										<?php
										if($kind == 'edit' && $file_name) {
											echo('<button type="button" class="buttonS bGray cssDelFile" '. ($arr_file_name_real[$file_index] == '' ? 'disabled' : '') .'>파일삭제</button>');
										}
										?>
									</div>
								</div>
							<?php
										$file_index++;
									}

								endif;
							?>
							</div>
						</td>
					</tr>
					<tr>
						<th>* 내용</th>
						<td>
						<?php

						if($kind == 'view'):
							echo '<div style="width:100%;height:200px;overflow-y:auto;">'. nl2br($res['content']) .'</div>';

						else:
							// 운영게시판 인 경우
							if($brd_id == 1):
								echo '<textarea id="content" name="content" style="width:100%;display:none;"></textarea>';
							else:
								echo '<textarea id="content" name="content" style="width:90%;height:150px">'. $res['content'] .'</textarea>';
							endif;

						endif;

						?>
						</td>
					</tr>
				</table>
				<div class="marginT10 textR">
					<button type="button" id="btnSubmit" class="buttonM bSteelBlue">등록</button>
					<button type="button" id="btnList" class="buttonM bGray">목록</button>
				</div>
				</form>
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		<textarea id="tmp_content" style="display:none;"><?php echo $cont;?></textarea>
		
		
<?php
include_once './inc/inc_footer.php';
?>
