<?php
/**
 * 관리자 페이지 로그인
 * 작성자 : dylan
 * 작성일 : 2015.06.19
 * 수정일 : 2016.06.21 고도화
 *  
 */
?>
<?php
include_once "./inc/inc_header_js.php";

// 추가 : dylan 2017.09.12, danvistory.com
$this->session->set_userdata(CFG_SECURE_FORM_TOKEN, get_token());
?>
	
<script>

$(document).ready(function() {

    // 아이디 저장
    if($.cookie('labors_uid')) {
        $("#txt_id").val($.cookie('labors_uid'));
        $('input[id=chk_save_id]').prop('checked', true);
    }

    //
    $("#txt_id").focus();
	
    $("#btn_submit").click(function(e) {
			e.preventDefault();
		
			if($("#txt_id").val() == '') {
				alert( CFG_MSG[CFG_LOCALE]['info_login_01'] );
				$("#txt_id").focus();
				return false;
			}

			if($("#txt_pwd").val() == '') {
				alert( CFG_MSG[CFG_LOCALE]['info_login_02'] );
				$("#txt_pwd").focus();
				return false;
			}

			// id 저장인 경우
			if( $('input[id=chk_save_id]').is(':checked')) {
				$.cookie('labors_uid', $("#txt_id").val(), {expires:36500});
			}
			else {
				$.removeCookie('labors_uid');
			}

			// 전송
			var url = '/?c=admin&m=login';
			var rsc = $("#frmForm").serialize();
			var fn_succes = function(data) {
				if(data.rst == 'succ') {
					document.location.href = '/?c=admin';
				}
				else {
					if(data && data.msg) {
						alert(CFG_MSG[CFG_LOCALE][data.msg]);
					}
				}
		};
		var fn_error = function(data) {
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data.msg) msg += '[' + data.msg +']';
			alert(msg);
			$("#txt_id").focus();
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);
	});

	$("#txt_id,#txt_pwd").keypress(function(e) {
		if (event.keyCode == 13) {
			e.preventDefault();
			$("#btn_submit").click();
		}
	});
});


</script>
</head>

<body>   

    <div id="login">
    <form name="frmForm" id="frmForm" autocomplete="off" enctype="multipart/form-data">
				<input type="hidden" name="<?php echo CFG_SECURE_FORM_TOKEN;?>" value="<?php echo $this->session->userdata(CFG_SECURE_FORM_TOKEN);?>">
        <div class="login_box">
            <ul class="login_form">
                <li><input type="text" id="txt_id" name="adminId" placeholder="아이디" style="width:240px;text-align:center;" tabindex="1"></li>
                <li><input type="password" id="txt_pwd" name="adminPwd" placeholder="비밀번호" style="width:240px;text-align:center;" tabindex="2"></li>
                <li class="login_btn">
                    <button type="text" class="btn_login" id="btn_submit" name="btn_submit" tabindex="3">로그인</button>
                </li>
            </ul>
            <ul>
                <li style="margin:10px 0 0 200px;">
                    <input type="checkbox" id="chk_save_id"><label for="chk_save_id">아이디 저장</label>
                </li>
            </ul>
        </div>
    </form>
    </div>

</body>
</html>
