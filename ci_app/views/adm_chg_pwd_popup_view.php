<script>
$(document).ready(function(){
	//
	$('#curr_pwd').focus();

	// change button
	$('button.cssBtnChgPwdPop').click(function(e){
		e.preventDefault();
		// validation
		// - old pwd
		if($('#curr_pwd').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_oper_02']);
			$('#curr_pwd').focus();
			return;
		}
		// - new pwd
		if($('#new_pwd').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_oper_03']);
			$('#new_pwd').focus();
			return;
		}
		// - check pwd
		if($('#check_pwd').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_oper_04']);
			$('#check_pwd').focus();
			return;
		}
		// 비번 동일 유무 확인
		if($('#new_pwd').val() != $('#check_pwd').val()) {
			alert(CFG_MSG[CFG_LOCALE]['info_subs_09']);
			$('#check_pwd').focus();
			return;
		}
		//
		if(confirm(CFG_MSG[CFG_LOCALE]['info_oper_01'])) {
			// request
			$.ajax({
				url: '/?c=admin&m=chg_pwd'
				,data: $('#frmFormPop').serialize()
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					if(data.rst == 'succ') {
						alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
						closeLayerPopup();
					}
					else {
						if(data.msg == 'not_match') {
							alert(CFG_MSG[CFG_LOCALE]['info_oper_05']);
							$('#curr_pwd').focus();
						}
						else {
							alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
						}
					}
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
				}
			});
		}
	});

	// cancel button
	$('button.cssBtnCancelPop').click(function(e){
		e.preventDefault();
		closeLayerPopup();
	});
});
</script>


    <div id="popup">
        <header id="pop_header">
			<h2 class="">비밀번호변경</h2>			
        </header>
		<form name="frmFormPop" id="frmFormPop" method="post">
		<div id="popup_contents">			
			<table class="tInsert">
				<caption>비밀번호변경 입니다.</caption>
				<colgroup>
					<col style="width:30%">
					<col style="width:70%">
				</colgroup>
				<tr>
					<th class="textL">현재 비밀번호</th>
					<td><input type="password" name="curr_pwd" id="curr_pwd"></td>
				</tr>
				<tr>
					<th class="textL">새 비밀번호</th>
					<td><input type="password" name="new_pwd" id="new_pwd"></td>
				</tr>
				<tr>
					<th class="textL">새 비밀번호확인</th>
					<td><input type="password" name="check_pwd" id="check_pwd"></td>
				</tr>
			</table>			
		</div>
		<div class="btn_set">
			<button type="button" class="buttonM bOrange cssBtnChgPwdPop">변경</button>
			<button type="button" class="buttonM bGray cssBtnCancelPop">취소</button>
		</div>
		</form>
    </div>
