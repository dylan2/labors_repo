<?php
include_once './inc/inc_header.php';
?>

<script>

var kind = "<?php echo $res['kind']; ?>";
if(!kind) kind = 'add';
//
var is_owner = "<?php echo $res['is_owner']; ?>";

// 공지 팝업 게시물 seq
var popup_noti_seq = "<?php echo $res['popup_seq'];?>";

$(document).ready(function(){

	// 상담일 today 세팅
	if(kind == 'add') {
		$('#csl_date').val(get_today());

		// 거주지 주소 최초 선택 처리
		$('#live_addr option').each(function(i,ele){
			if($(this).text() == '무응답') {
				$('#live_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});
		// 소재지 주소 최초 선택 처리
		$('#comp_addr option').each(function(i,ele){
			if($(this).text() == '무응답') {
				$('#comp_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});
	}
	// edit
	else {
		// 거주지 주소
		if($('#live_addr option:selected').text() == '기타') {
			$('#live_addr_etc').css('display', 'inline');
		}
		// 소재지 주소
		if($('#comp_addr option:selected').text() == '기타') {
			$('#comp_addr_etc').css('display', 'inline');
		}

	}

	// 인쇄 버튼
	$('.cssPrint').click(function(e){
		$('#focusOuter').focus();
		if(kind == 'edit') {
			open_popup_center_dual('/?c=counsel&m=counsel_print&seq='+ $('#seq').val(), '인쇄', 1060, 700);
		}
	});


	// 상담방법 - 선택된 값이 없으면 처음 radio를 checked한다. - 요청에 의해 수석처리 : 2015.08.21
	// if(!$("input[name=s_code]").is(":checked")) {
	// 	$('input:radio[name="s_code"]:nth(0)').attr('checked', true);
	// }

	// 성별 - 선택된 값이 없으면 처음 radio를 checked한다. - 요청에 의해 수석처리 : 2015.08.21
	// if(!$("input[name=gender]").is(":checked")) {
	// 	var index = find_default_item($('input:radio[name="gender"]'));
	// 	$('input:radio[name="gender"]:nth('+ index +')').attr('checked', true);
	// }

	// 연령대 - 선택된 값이 없으면 처음 radio를 checked한다. - 요청에 의해 수석처리 : 2015.08.21
	// if(!$("input[name=ages]").is(":checked")) {
	// 	var index = find_default_item($('input:radio[name="ages"]'));
	// 	$('input:radio[name="ages"]:nth('+ index +')').attr('checked', true);
	// }

	// 직종 - 선택된 값이 없으면 처음 radio를 checked한다.
	if(!$("input[name=work_kind]").is(":checked")) {
		var index = find_default_item($('input:radio[name="work_kind"]'));
		$('input:radio[name="work_kind"]:nth('+ index +')').attr('checked', true);
	}

	// 회사 업종 - 선택된 값이 없으면 처음 radio를 checked한다.
	if(!$("input[name=comp_kind]").is(":checked")) {
		var index = find_default_item($('input:radio[name="comp_kind"]'));
		$('input:radio[name="comp_kind"]:nth('+ index +')').attr('checked', true);
	}

	// 고용형태 - 선택된 값이 없으면 처음 checkbox를 checked한다.
	if(!$("input[name=emp_kind]").is(":checked")) {
		var index = find_default_item($('input:radio[name="emp_kind"]'));
		$('input:radio[name="emp_kind"]:nth('+ index +')').attr('checked', true);
	}

	// 사용주체 - 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
	// if(!$("input[name=emp_use_kind]").is(":checked")) {
	// 	var index = find_default_item($('input:radio[name="emp_use_kind"]'));
	// 	$('input:radio[name="emp_use_kind"]:nth('+ index +')').attr('checked', true);
	// }

	// 근로자수 - 선택된 값이 없으면 처음 radio를 checked한다.
	if(!$("input[name=emp_cnt]").is(":checked")) {
		var index = find_default_item($('input:radio[name="emp_cnt"]'));
		$('input:radio[name="emp_cnt"]:nth('+ index +')').attr('checked', true);
	}

	// 근로계약서 작성 여부 - 선택된 값이 없으면 처음 radio를 checked한다.
	if(!$("input[name=emp_paper_yn]").is(":checked")) {
		var index = find_default_item($('input:radio[name="emp_paper_yn"]'));
		$('input:radio[name="emp_paper_yn"]:nth('+ index +')').attr('checked', true);
	}

	// 4대보험가입 여부 - 선택된 값이 없으면 처음 radio를 checked한다.
	if(!$("input[name=emp_insured_yn]").is(":checked")) {
		var index = find_default_item($('input:radio[name="emp_insured_yn"]'));
		$('input:radio[name="emp_insured_yn"]:nth('+ index +')').attr('checked', true);
	}

	// 상담유형 - 선택된 값이 없으면 처음 checkbox를 checked한다. - 요청에 의해 수석처리 : 2015.08.21
	// if($("input[id^=csl_kind_]:checked").length == 0) {
	// 	$('input:checkbox[id="csl_kind_0"]').attr('checked', true);
	// }

	// 주제어 - 선택된 값이 없으면 처음 checkbox를 checked한다. - 요청에 의해 수석처리 : 2015.08.21
	// if($("input[name^=csl_keyword_]:checked").length == 0) {
	// 	$('input:checkbox[name="csl_keyword_0"]').attr('checked', true);
	// }

	// 상담내역공유 - 선택된 값이 없으면 '공유안함' radio를 checked한다.
	if(!$("input[name=csl_share_yn]").is(":checked")) {
		$('input:radio[name="csl_share_yn"]').each(function(){
			if($(this).next().text().indexOf('안') != -1) {
				$(this).attr('checked', true);
			}
		});
	}

	// 상담결과 - 선택된 값이 없으면 처음 radio를 checked한다. - 요청에 의해 수석처리 : 2015.08.21
	// if(!$("input[name=csl_proc_rst]").is(":checked")) {
	// 	$('input:radio[name="csl_proc_rst"]:nth(0)').attr('checked', true);
	// }

	// 전송버튼 클릭시 form onSubmit 이벤트 발생처리
	$('.cssAdd').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
			$('form').submit();
		}
	});
	

	// 등록 버튼 이벤트 핸들러
	$('form').submit(function(e) {

		// 수정 - 자신의 글인지 체크
		if(kind == 'edit' && is_master != 1 && is_owner == 0) {
			alert(CFG_MSG[CFG_LOCALE]['warn_board_02']);
			return false;
		}

		// validation
		// 필수체크 항목
		// - 성별,상담유형,상담내용,상담답변,처리결과 5개
		// - 상담방법, 성별, 연령대, 상담유형, 상담제목, 상담내용, 상담답변, 처리결과 - 8개 => 변경 2015.08.21
		// 상담방법
		if(!$("input[name=s_code]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_19']);
			$('#s_code_0').focus();
			return false;
		}
		// 성별
		if(!$("input[name=gender]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_17']);
			$('#gender_0').focus();
			return false;
		}
		// 연령대
		if(!$("input[name=ages]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_20']);
			$('#ages_0').focus();
			return false;
		}
		// 상담유형
		if(!$("input[id^=csl_kind_]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_14']);
			$('#csl_kind_0').focus();
			return false;
		}
		// 상담유형 - 3개까지 체크 가능
		var csl_kind_chk_len = $("input[id^=csl_kind_]:checked").length;
		if(csl_kind_chk_len > 3) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_16']);
			$('#csl_kind_0').focus();
			return false;
		}
		// 제목
		if($('#csl_title').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_03']);
			$('#csl_title').focus();
			return false;
		}
		// 상담내용
		if($('#csl_content').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_04']);
			$('#csl_content').focus();
			return false;
		}
		// 상담 답변
		if($('#csl_reply').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_05']);
			$('#csl_reply').focus();
			return false;
		}
		// 상담 처리결과
		if(!$("input[name=csl_proc_rst]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_06']);
			$('#csl_proc_rst_0').focus();
			return false;
		}

		/*
		// 상담일
		if($('#csl_date').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_02']);
			$('#csl_date').focus();
			return false;
		}
		// 내담자 이름
		if($('#csl_name').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_11']);
			$('#csl_name').focus();
			return false;
		}
		// 내담자 연락처
		if($('#csl_tel01').val() == '' || $('#csl_tel02').val() == '' || $('#csl_tel03').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_12']);
			if($('#csl_tel01').val() == '') { $('#csl_tel01').focus(); return false; }
			if($('#csl_tel02').val() == '') { $('#csl_tel02').focus(); return false; }
			if($('#csl_tel03').val() == '') { $('#csl_tel03').focus(); return false; }
		}
		// 거주지
		if($("#live_addr option:selected").val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_07']);
			$('#live_addr').focus();
			return false;
		}
		// 소재지
		if($("#comp_addr option:selected").val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_08']);
			$('#comp_addr').focus();
			return false;
		}
		// 고용형태
		if(!$("input[name^=emp_kind_]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_13']);
			$('#emp_kind_0').focus();
			return false;
		}
		// 상담제목
		if($('#csl_title').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_03']);
			$('#csl_title').focus();
			return false;
		}
		// 주제어
		if(!$("input[name^=csl_keyword_]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_15']);
			$('#csl_keyword_0').focus();
			return false;
		}
		*/
		var dlm = '<?php echo CFG_AUTH_CODE_DELIMITER;?>';
		// 고용형태,상담유형,주제어 데이터 조합
		// - 고용형태 -- 중복체크 가능하게 요청 뒤 원복 요청으로 주석처리
//		var emp_kind = '';
//		$("input[name^=emp_kind_]:checked").each(function(){
//			if(emp_kind != '') emp_kind += dlm;
//			emp_kind += $(this).val();
//		});
//		if(emp_kind.indexOf(dlm) == -1) {
//			emp_kind = dlm;
//		}
		// - 상담유형
		var csl_kind = '', 
			csl_kind_cnt = 0;
		$("input[id^=csl_kind_]:checked").each(function(){
			if(csl_kind != '') csl_kind += dlm;
			csl_kind += $(this).val();
			csl_kind_cnt++;
		});
		$('input[name="csl_kind"]').val(csl_kind);

		// -- 상담유형을 3개의 구조로 맞춘다. 이유는 통계쪽에서 엑셀다운로드시 3개의 컬럼으로 나눠 보여달라는 요청 때문임. 
		// -- 각 컬럼은 특정값에 고정되지 않기 때문에 3개를 선택 안한 경우 임의로 3개의 구조로 만든다.
//		if(csl_kind_cnt < 3) {
//			for(var i=0; i<3-csl_kind_cnt; i++) {
//				if(csl_kind != '') csl_kind += dlm;
//			}
//		}
		// - 주제어
		// var csl_keyword = '';
		// $("input[name^=csl_kewyord_]:checked").each(function(){
		// 	if(csl_keyword != '') csl_keyword += dlm;
		// 	csl_keyword += $(this).val();
		// });

		// 기타 input box값 체크 - 기타가 아닌 경우 input box 값 없앰
		// - 상담방법
		if($('input:radio[name=s_code]:checked').next().text() != '기타') {
			$('input[name=s_code_etc]').val('');
		}
		// - 고용형태
		if($('input:radio[name=emp_kind]:checked').next().text() != '기타') {
			$('input[name=emp_kind_etc]').val('');
		}
		// - 처리결과
		if($('input:radio[name=csl_proc_rst]:checked').next().text() != '기타') {
			$('input[name=csl_proc_rst_etc]').val('');
		}


		//
		//
		// 첨부파일의 보안문제를 이유로 첨부파일 기능 제거요청으로 재작업 - 20160620 최진혁
		//
		// 
		// 전송
		// for HTML5 browsers
		if(window.FormData !== undefined)  {

			// ie 9 이하 외 브라우저 때문에 여기서 이벤트 흐름을 끊어준다.
			e.preventDefault();

			// 파일 업로드 : 파일 추가(변경)된 개수
			$('#chg_file_cnt').val(chg_file_cnt);
			// 변경된 실제 파일명들
			$('#chg_real_file_name').val(chg_file_name_real);
			//
			var formData = new FormData(this);

			$('#loading').show();
			setTimeout(function(){
				$.ajax({
					url: '/?c=counsel&m=processing'
					,type: 'POST'
					,data:  formData
					//,data: formData +'&csl_kind='+ csl_kind +'&csl_keyword='+ csl_keyword
					,mimeType:"multipart/form-data"
					,contentType: false
					,cache: false
					,processData:false
					,success: function(data, textStatus, jqXHR) {
						var obj = JSON.parse(data);
						if(obj.rst == 'succ') {
							alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
							check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
						}
						else {
							if(is_local) objectPrint(data);
							if(is_local) objectPrint(jqXHR);
							if(is_local) console.log('textStatus : '+ textStatus);
							alert(obj.msg);
						}
					}
					,error: function(jqXHR, textStatus, errorThrown) {
						if(is_local) objectPrint(jqXHR);
						if(is_local) objectPrint(textStatus);
						if(is_local) objectPrint(errorThrown);
					
						var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
						if(data && data.msg) msg += '[' + data.msg +']';
						alert(msg);
					}
					,beforeSend: function(xhr) {
						$('#loading').show();
	  				}
			  	})
				.done(function( data ) {
					$('#loading').hide();
				});
			}, 10);
	   }
	   // for olden browsers
	   else {

	   		// <form> 태그에 action을 직업 기입한 경우 크롬에서 폼 전송 안되는 문제 발생.
			$(this).attr("action", "/?c=counsel&m=processing");
		    var formObj = $(this);//*
		 
			//generate a random id
			var iframeId = 'unique' + (new Date().getTime());
	 
			//create an empty iframe
			var iframe = $('<iframe src="javascript:false;" style="width:0px;height:0px;background-color:white;" name="'+iframeId+'" />');
	 
			//hide it
			iframe.hide();
	 
			//set form target to iframe
			formObj.attr('target', iframeId);
	 
			//Add iframe to body
			iframe.appendTo('body');
			iframe.load(function(e) {
				check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
			});
		}		
	});


	// list 버튼 이벤트 핸들러
	$('.cssList').click(function(e){
		$('#focusOuter').focus();
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), '', '');
	});

	// 뒤로 버튼 이벤트 핸들러
	$('.cssBack').click(function(e){
		$('#focusOuter').focus();
		history.back();
		return false;
	});

	//내담자정보->관련상담 버튼 클릭 핸들러
	$('#btnRelViewCounsel').click(function(e){
		// 내담자 이름으로만 검색, 전번은 제외 요청에 따라 주석처리 2015.08.06 delee
//		if($('#csl_name').val() == '') || $('#csl_tel01').val() == '' || $('#csl_tel02').val() == '' || $('#csl_tel03').val() == '') {
			if($('#csl_name').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_csl_11']);
				$('#csl_name').focus();
				return false;
			}
//			else {
//				alert(CFG_MSG[CFG_LOCALE]['info_csl_12']);
//			}
//			if($('#csl_tel01').val() == '') { $('#csl_tel01').focus(); return false; }
//			if($('#csl_tel02').val() == '') { $('#csl_tel02').focus(); return false; }
//			if($('#csl_tel03').val() == '') { $('#csl_tel03').focus(); return false; }
//		}

		$('#loading').show();
		var closedCallback = function(data) {
			if(data) {
				if(data.seq) $('#csl_ref_seq').val(data.seq);
				// if(data.title) $('#csl_title').val(data.title);
				// if(data.content) $('#csl_content').val(data.content);
				// if(data.reply) $('#csl_reply').val(data.reply);
				//
				if(data.csl_name) $('#csl_name').val(data.csl_name);
				if(data.csl_tel) {
					var arr_csl_tel = data.csl_tel.split('-');
					$('#csl_tel01').val(arr_csl_tel[0]);
					$('#csl_tel02').val(arr_csl_tel[1]);
					$('#csl_tel03').val(arr_csl_tel[2]);
				}
				if(data.gender) $('input:radio[name=gender]:input[value="'+ Base64.encode(data.gender) +'"]').prop('checked', true);
				if(data.ages) $('input:radio[name=ages]:input[value="'+ Base64.encode(data.ages) +'"]').prop('checked', true);
				// 거주지
				if(data.live_addr) {
					$('select[name=live_addr] option[value="'+ Base64.encode(data.live_addr) +'"]').prop('selected', true);
					// 기타인 경우
					if($('select[name=live_addr] option:selected').text() == '기타') {
						$('input[name=live_addr_etc]').show();
						$('input[name=live_addr_etc]').val(data.live_addr_etc);
					}
				}
				// 직종
				if(data.work_kind) {
					$('select[name=work_kind] option[value="'+ Base64.encode(data.work_kind) +'"]').prop('selected', true);
					// 기타입력 항목
					$('input[name=work_kind_etc]').val(data.work_kind_etc);
				}
				if(data.comp_kind) {
					$('input:radio[name=comp_kind]:input[value="'+ Base64.encode(data.comp_kind) +'"]').prop('checked', true);// 기타입력 항목
					$('input[name=comp_kind_etc]').val(data.comp_kind_etc);
				}
				// 소재지
				if(data.comp_addr) {
					$('select[name=comp_addr] option[value="'+ Base64.encode(data.comp_addr) +'"]').prop('selected', true);
					// 기타인 경우
					if($('select[name=comp_addr] option:selected').text() == '기타') {
						$('input[name=comp_addr_etc]').show();
						$('input[name=comp_addr_etc]').val(data.comp_addr_etc);
					}
				}
				// 고용형태
				if(data.emp_kind) {
					$('input:radio[name=emp_kind]:input[value="'+ Base64.encode(data.emp_kind) +'"]').prop('checked', true);
					// 기타인 경우
					if($('input:radio[name=emp_kind]:checked').next().text() == '기타') {
						$('#emp_kind_etc_holder').show();
						$('input[name=emp_kind_etc]').val(data.emp_kind_etc);
					}
				}
				if(data.emp_cnt) $('input:radio[name=emp_cnt]:input[value="'+ Base64.encode(data.emp_cnt) +'"]').prop('checked', true);
				if(data.emp_paper_yn) $('input:radio[name=emp_paper_yn]:input[value="'+ Base64.encode(data.emp_paper_yn) +'"]').prop('checked', true);
				if(data.emp_insured_yn) $('input:radio[name=emp_insured_yn]:input[value="'+ Base64.encode(data.emp_insured_yn) +'"]').prop('checked', true);
				if(data.ave_pay_month) $('input[name=ave_pay_month]').val(data.ave_pay_month);
				if(data.work_time_week) $('input[name=work_time_week]').val(data.work_time_week);
			}
		};
		var data = encodeURI('&nm='+ $('#csl_name').val());// + '&tel='+ $('#csl_tel01').val() + '-' + $('#csl_tel02').val() + '-' + $('#csl_tel03').val());
		var url = '/?c=counsel&m=counsel_rel_view&seq='+ $('input[name=seq]').val() + data;
		gLayerId = openLayerModalPopup(url, 800, 830, '', '', '', closedCallback, 1, true);
	});

	// 상담내용->불러오기 버튼 이벤트 핸들러
	$('#btnGetExistCounsel').click(function(e){
		$('#loading').show();
		var closedCallback = function(data) {
			if(data) {
				// if(data.seq) $('#csl_ref_seq').val(data.seq);
				if(data.title) $('#csl_title').val(data.title);
				if(data.content) $('#csl_content').val(data.content);
				if(data.reply) $('#csl_reply').val(data.reply);
			}
			$('#csl_title').focus();
		};
		var url = '/?c=counsel&m=counsel_list_popup_view&seq='+ $('input[name=seq]').val();
		gLayerId = openLayerModalPopup(url, 800, 850, '', '', '', closedCallback, 1, true);
	});

	// 거주지/소재지 변경 이벤트 핸들러
	$('#live_addr,#comp_addr').change(function(){
		var selopTxt;
		var isLive = $(this).attr('id') == 'live_addr';
		if(isLive) {
			selopTxt = $("#live_addr option:selected").text();
		}
		else {
			selopTxt = $("#comp_addr option:selected").text();
		}
		//
		if(selopTxt == '경기도' || selopTxt == '기타') {
			if(isLive) {
				$('#live_addr_etc').css('display', 'inline').focus();
			}
			else {
				$('#comp_addr_etc').css('display', 'inline').focus();
			}
		}
		else {
			if(isLive) {
				$('#live_addr_etc').css('display', 'none');
				$('#live_addr_etc').val('');
			}
			else {
				$('#comp_addr_etc').css('display', 'none');
				$('#comp_addr_etc').val('');
			}
		}
	});

	// 상담방법 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_s_code = "<?php echo base64_encode($edit['s_code']);?>";
	$("input:radio[name=s_code]").each(function(i){
		if($(this).val() == csl_s_code) {
			if($(this).next().text() == '기타') {
				$('#s_code_etc').css('display', 'inline');
				var s_code_etc = "<?php echo $edit['s_code_etc'];?>";
				$('#s_code_etc').val( s_code_etc );
			}
			else {
				$('#s_code_etc').val('');
			}
		}
	});	
	// 상담방법 - 기타 선택한 경우, input box 활성화
	$('input:radio[name=s_code]').click(function(e){
		var display = 'none';
		var len = $('#s_code_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).attr('id') == 's_code_'+len ) display = 'inline';
		$('#s_code_etc_holder').css('display', display);
		if($(this).attr('id') == 's_code_'+len ) $('#s_code_etc').focus();
	});
	if($('#s_code_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#s_code_etc_holder').css('display', 'none');
	}

	// 고용형태 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var emp_kind = "<?php echo base64_encode($edit['emp_kind']);?>";
	$("input:radio[name=emp_kind]").each(function(i){
		if($(this).val() == emp_kind) {
			if($(this).next().text() == '기타') {
				$('#emp_kind_etc').css('display', 'inline');
				var emp_kind_etc = "<?php echo $edit['emp_kind_etc'];?>";
				$('#emp_kind_etc').val( emp_kind_etc );
			}
			else {
				$('#emp_kind_etc').val('');
			}
		}
	});
	// 고용형태 - 기타 선택한 경우, input box 활성화
	$('input:radio[name=emp_kind]').click(function(e){
		var display = 'none';
		var len = $('#emp_kind_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).attr('id') == 'emp_kind_'+len ) display = 'inline';
		$('#emp_kind_etc_holder').css('display', display);
		if($(this).attr('id') == 'emp_kind_'+len ) $('#emp_kind_etc').focus();
	});
	if($('#emp_kind_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#emp_kind_etc_holder').css('display', 'none');
	}

	// 상담결과 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_proc_rst = "<?php echo base64_encode($edit['csl_proc_rst']);?>";
	$("input:radio[name=csl_proc_rst]").each(function(i){
		if($(this).val() == csl_proc_rst) {
			if($(this).next().text() == '기타') {
				$('#csl_proc_rst_etc').css('display', 'inline');
				var csl_proc_rst_etc = "<?php echo $edit['csl_proc_rst_etc'];?>";
				$('#csl_proc_rst_etc').val( csl_proc_rst_etc );
			}
			else {
				$('#csl_proc_rst_etc').val('');
			}
		}
	});
	// 상담결과 - 기타 선택한 경우, input box 활성화
	$('input[name=csl_proc_rst]').click(function(e){
		// console.log( $(this).next().text() );

		var display = 'none';
		var len = $('#csl_proc_rst_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).attr('id') == 'csl_proc_rst_'+len ) display = 'inline';
		$('#csl_proc_rst_etc_holder').css('display', display);
		if($(this).attr('id') == 'csl_proc_rst_'+len ) $('#csl_proc_rst_etc').focus();
	});
	if($('#csl_proc_rst_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#csl_proc_rst_etc_holder').css('display', 'none');
	}


	// 파일 삭제
	$("button.cssDelFile").click(function(e){
		if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_02'])) {
			var index = $(this).parents('.floatC').attr('data-role-index');
			var seq = $('#seq').val();
			var file_name_real = $('#filename_real_'+ index).val();
			var btn_this = this;
			// request
			$.ajax({
				url: '/?c=counsel&m=del_file'
				,data: {sq:seq, fnr:file_name_real}
				,cache: false
				,async: false
				,method: 'post'
				,dataType: 'json'
				,success: function(data) {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
					//
					$(btn_this).attr('disabled', 'true');
					$(btn_this).next().attr('disabled', 'true');
					$('#filename_'+ index).val('');
				}
				,error: function(data) {
					if(is_local) objectPrint(data);
					
					var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
					if(data && data.msg) msg += '[' + data.msg +']';
					alert(msg);
					$('#txtIndex_pop').focus();
				}
			});
		}
	});

	// 파일 download
	$('button.cssDownload').click(function(){
		var real = $(this).attr('data-role-real');
		var org = $(this).attr('data-role-org');
		// request
		$.ajax({
			url: '/?c=board&m=chk_download'
			,data: {real:real, org:org}
			,cache: false
			,async: false
			,method: 'post'
			,dataType: 'json'
			,success: function(data) {
				if(data.rst == 'fail') {
					alert(CFG_MSG[CFG_LOCALE]['warn_board_01']);
				}
				else {
					window.location.href = "/?c=board&m=download&real="+ real +"&org="+ org;
				}
			}
			,error: function(data) {
				if(is_local) objectPrint(data);
				
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			}
		});
	});

	// focus
	$('#csl_name').focus();
});

/**
  * 입력받은 jquery 객체 중 각 앨리먼트 다음에 위치한 태그의 값이 "무응답"을 찾아 index 리턴
  */
function find_default_item(jq) {
	var rst  = 0;
	jq.each(function(i, ele){
		if($(this).next().text() == '무응답') {
			rst = i;
		}
	});
	return rst;
}

/**
  * 안내 팝업
  */
function open_help(index) {
	var url = '/?c=counsel&m=counsel_help&url=./help/adm_counsel_help'+ index +'_popup_view';
	var h = 300, w = 500;
	if(index == 3) {
		h = 570;
		w = 750;
	}
	gLayerId = openLayerModalPopup(url, w, h, '', '', '', '', 1, true);
}



// jQuery form submit helper
function getDoc(frame) {
	var doc = null;

	// IE8 cascading access check
	try {
		if (frame.contentWindow) {
			doc = frame.contentWindow.document;
		}
	} catch(err) {
	}

	if (doc) { // successful getting content
		return doc;
	}

	try { // simply checking may throw in ie8 under ssl or mismatched protocol
		doc = frame.contentDocument ? frame.contentDocument : frame.document;
	} catch(err) {
		// last attempt
		doc = frame.document;
	}
	return doc;
}


//==================================================================================================
// 첨부파일 관련
//==================================================================================================
$(document).ready(function(e){
	// 파일 추가 버튼 이벤트 핸들러
	var new_btn_index = $('.cssAddFile').length;
	var cfg_upload_max_count = '<?php echo CFG_UPLOAD_MAX_COUNT;?>';
	$('#file_wrapper').on('click', '.cssAddFile', function(e){
		if(new_btn_index >= cfg_upload_max_count) {
			var limit_msg = (CFG_MSG[CFG_LOCALE]['info_board_06']).replace('x', cfg_upload_max_count);
			alert(limit_msg);
			return false;
		}
		// 태그 생성
		append_file_tag_block(new_btn_index);
		new_btn_index++;
		e.preventDefault();
	});

	// 추가한 항목 제거 이벤트 핸들러
	$('#file_wrapper').on('click', '.btnRremove', function(e){
		$(this).parents('.floatC').remove();
		new_btn_index--;
		e.preventDefault();
	});
});

// 파일 변경 여부 플래그
var chg_file_cnt = 0;
// 변경된 실제 파일명 - 삭제할 대상
var chg_file_name_real = '';
// 파일 개당 업로드 최대 용량
var file_upload_max_size = '<?php echo CFG_UPLOAD_MAX_FILE_SIZE;?>';

// file 선택시 파일 처리
function chg_files($this, ele_name) {
//	alert($($this)[0].size);
	var obj_ele = document.getElementById(ele_name);
	var sel_file_size = $($this)[0].files[0].size;
	// 선택한 파일이 용량 초과한 경우
	if(sel_file_size >= file_upload_max_size) {
		// 
		var info_size = numberToCurrency(Math.floor(parseInt(sel_file_size)/1024768));
		var info_msg = (CFG_MSG[CFG_LOCALE]['info_board_07']).replace('x', info_size);
		alert(info_msg);
		
		// html을 제거한뒤
		var index = $($this).parents('.floatC').attr('data-role-index');
		$($this).parents('.floatC').remove();
		// 새로 생성
		append_file_tag_block(index);
		// 기존 값이 있으면 대입
		if(obj_ele.value != '') {
			$('#filename_'+ index).val( obj_ele.value );
		}
		// 제거 버튼 제거
		$('#filename_'+ index).parents('.floatC').find('div>.btnRremove').remove();

		return false;
	}

	var matches = '';
	// 수정일 경우 기존 파일명과 선택한 파일명이 같으면 업로드하지 않도록 한다.
	if(obj_ele.value) {
		matches = $this.value.match(eval('/'+ obj_ele.value +'/g'));
	}
	// 파일명이 같으면 업로드하지 않는다
	if(matches) {
		alert(CFG_MSG[CFG_LOCALE]['info_board_05']);
	}
	else {
		// 기존 파일이 있는 경우만(처음 업로드시 파일 선택후 다시 선택해도 change 이벤트가 일어난다)
		if(obj_ele.value != '') {
			chg_file_cnt++;
		}
		obj_ele.value = $this.value;

		// 파일이 변경되면, 기존 파일 삭제를 위해 파일이름을 조합한다.
		var real_file_name = ele_name.replace('_', '_real_');
		if(chg_file_name_real != '') chg_file_name_real += '<?php echo CFG_UPLOAD_FILE_NAME_DELIMITER; ?>';
		chg_file_name_real += document.getElementById(real_file_name).value;
	}
}

/**
  * 파일추가 html 태그 생성
  */
function append_file_tag_block(index) {
	$('#file_wrapper').append('<div class="floatC" style="line-height:35px;margin-top:3px;"><div class="floatL cssFileForm"><input type="hidden" id="filename_real_'+ index +'" ></hidden><input type="text" id="filename_'+ index +'" class="floatL" readonly="readonly" style="width:280px;background:none;border:none;"><div class="file_up" ><button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button><input type="file" id="userfile_'+ index +'" name="userfile_'+ index +'" style="width:100%" onchange="javascript:chg_files(this, \'filename_'+ index +'\');"></div></div><div class="floatL marginL05" style="margin-top:-3px;"><button type="button" class="buttonS bGray cssAddFile">추가</button>&nbsp;<button type="button" class="buttonS bGray btnRremove">제거</button></div></div>');
}


$(document).ready(function(e){
	
	var arr_func = [];

	// 팝업 공지
	var arr_popup_noti_seq = popup_noti_seq.split(',');
	var func = null;
	for(var i=0; i<arr_popup_noti_seq.length; i++) {
		if(arr_popup_noti_seq[i] && ! $.cookie('labors_hide_popup_'+ arr_popup_noti_seq[i])) {
			var url = '"/?c=board&m=open_notice_popup&seq='+ arr_popup_noti_seq[i] +'"';
			func = function(url) {
				openLayerModalPopup(url, 500, 500,'','','','',1,true,null,true,true);
			};
			arr_func.push('func('+url+')');
		}
	}

	arr_func.forEach(function(item, index, array){
		if(index == 0) {
			eval(item);
		}
		else {
			// 첫번째 이후 팝업 생성과 컨텐츠 적용이 순차적으로 정확히 적용되도록 딜레이를 준다.
			setTimeout(function(){eval(item);},100);
		}
	});


});

</script>



<?php
include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">상담내역입력</h2>
				<span class="location">홈 > 상담내역관리 > <em>상담내역입력</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post">
				<input type="hidden" name="kind" value="<?php echo $res['kind']; ?>"></input>
				<input type="hidden" name="oper_id" id="oper_id" value="<?php echo $edit['oper_id']; ?>"></input>
				<input type="hidden" name="csl_ref_seq" id="csl_ref_seq" value="<?php echo $edit['csl_ref_seq']; ?>"></input>
				<input type="hidden" name="seq" id="seq" value="<?php echo $edit['seq']; ?>"></input>
				<h3 class="sub_stit">
					상담경로
					<div class="marginT10 textR" style="margin-top:-18px;">
						<button type="button" class="buttonM bSteelBlue cssPrint">인쇄</button>
						<button type="button" class="buttonM bOrange cssAdd">등록</button>
						<button type="button" class="buttonM bGray cssBack">뒤로이동</button>
						<button type="button" class="buttonM bDarkGray cssList">목록</button>
					</div>
				</h3>
				<table class="tInsert">
					<caption>
						상담경로 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>소속 *</th>
						<td><?php echo $edit['asso_name']; ?>
							<input type="hidden" id="asso_code" name="asso_code" value="<?php echo base64_encode($edit['asso_code']); ?>"></input>
						</td>
					</tr>
					<tr>
						<th>상담자 *</th>
						<td><?php echo $edit['oper_name']; ?></td>
					</tr>
					<tr>
						<th>상담일 *</th>
						<td><input type="text" id="csl_date" name="csl_date" class="datepicker date" value="<?php echo $edit['csl_date'];?>" style="width:80px"></td>
					</tr>
					<tr>
						<th>상담방법 *</th>
						<td>
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['s_code'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								if($item->code_name == '기타') {
									$etc_item['index'] = $index;
									$etc_item['item'] = $item;
									$index_etc = $index;
								}
								else {
									echo '<input type="radio" name="s_code" class="imgM" id="s_code_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['s_code'] == $item->s_code ? 'checked="checked"' : '') .'><label for="s_code_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								}
								$index++;
							}
							// 기타 항목
							echo '<input type="radio" name="s_code" class="imgM" id="s_code_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['s_code'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="s_code_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="s_code_etc_holder" data-role-etc_index="<?php echo $index_etc;?>">
								<input type="text" name="s_code_etc" id="s_code_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>	
					</tr>
				</table>	
				<h3 class="sub_stit">내담자 정보</h3>
				<table class="tInsert">
					<caption>
						내담자 정보 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>성명</th>
						<td><input type="text" name="csl_name" id="csl_name" class="imgM" size="10" maxlength="10" value="<?php echo $edit['csl_name'];?>">&nbsp;
							<button type="button" class="buttonS bGray marginT03" id="btnRelViewCounsel">관련상담</button>
							<span style="marginL10">* 재상담, 지속상담의 경우 성명 입력후 "관련상담" 버튼을 클릭하시면 관련상담을 찾아볼 수 있습니다.</span></td>
					</tr>
					<tr>
						<th>연락처</th>
						<td>
							<input type="text" name="csl_tel01" id="csl_tel01" class="imgM" size="4" maxlength="4" value="<?php echo $edit['csl_tel01'];?>"> - 
							<input type="text" name="csl_tel02" id="csl_tel02" class="imgM" size="4" maxlength="4" value="<?php echo $edit['csl_tel02'];?>"> - 
							<input type="text" name="csl_tel03" id="csl_tel03" class="imgM" size="4" maxlength="4" value="<?php echo $edit['csl_tel03'];?>">
						</td>
					</tr>
					<tr>
						<th>성별 *</th>
						<td>
						<?php
						$index = 0;
						foreach($res['gender'] as $item) {
							echo '<input type="radio" name="gender" class="imgM" id="gender_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['gender'] == $item->s_code ? 'checked="checked"' : '') .'><label for="gender_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>	
					<tr>
						<th>연령대 *</th>
						<td>
						<?php
						$index = 0;
						foreach($res['ages'] as $item) {
							echo '<input type="radio" name="ages" class="imgM" id="ages_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['ages'] == $item->s_code ? 'checked="checked"' : '') .'><label for="ages_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>	
					<tr>
						<th>거주지 *<br>(구단위)</th>
						<td>
							<select class="styled" id="live_addr" name="live_addr">
								<?php
								$index = 0;
								foreach($res['csl_addr'] as $item) {
									echo '<option value="'. base64_encode($item->s_code) .'" '. ($edit['live_addr'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
								}
								?>
							</select>
							<input type="text" class="" id="live_addr_etc" name="live_addr_etc" style="width:350px;display:none" value="<?php echo $edit['live_addr_etc'];?>" maxlength="30" placeholder="최대 30자"> 
						</td>										
					</tr>
					<tr>
						<th>직종 <a href="javascript:open_help(1)"><img src="../images/common/icon_que.png" alt="직종이란" class="imgM"></a></th>
						<td>
						<?php
						$index = 0;
						foreach($res['work_kind'] as $item) {
							echo '<input type="radio" name="work_kind" class="imgM" id="work_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['work_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="work_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<br>추가내용 : <input name="work_kind_etc" id="work_kind_etc" style="width: 350px; display: inline;" type="text" maxlength="30" value="<?php echo $edit['work_kind_etc'];?>" placeholder="최대 30자">
						</td>	
					</tr>
				</table>
				<h3 class="sub_stit">사업장 근로조건</h3>
				<table class="tInsert">
					<caption>
						사업장 근로조건 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>업종 <a href="javascript:open_help(2)"><img src="../images/common/icon_que.png" alt="업종이란" class="imgM"></a></th>
						<td>
						<?php
						$index = 0;
						foreach($res['comp_kind'] as $item) {
							echo '<input type="radio" name="comp_kind" class="imgM" id="comp_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['comp_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="comp_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<br>추가내용 : <input name="comp_kind_etc" id="comp_kind_etc" style="width: 350px; display: inline;" type="text" maxlength="30" value="<?php echo $edit['comp_kind_etc'];?>" placeholder="최대 30자">
						</td>	
					</tr>	
					<tr>
						<th>소재지 *<br>(구단위)</th>
						<td>
							<select class="styled" id="comp_addr" name="comp_addr">
								<?php
								$index = 0;
								foreach($res['csl_addr'] as $item) {
									echo '<option value="'. base64_encode($item->s_code) .'" '. ($edit['comp_addr'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
								}
								?>
							</select>
							<input type="text" class="" id="comp_addr_etc" name="comp_addr_etc" style="width:350px;display:none" value="<?php echo $edit['comp_addr_etc'];?>" maxlength="30" placeholder="최대 30자"> 
						</td>										
					</tr>
					<tr>
						<th>고용형태 * <a href="javascript:open_help(3)"><img src="../images/common/icon_que.png" alt="고용형태란" class="imgM"></a></th>
						<td>
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['emp_kind'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								if($item->code_name == '기타') {
									$etc_item['index'] = $index;
									$etc_item['item'] = $item;
									$index_etc = $index;
								}
								else {
									echo '<input type="radio" name="emp_kind" class="imgM" id="emp_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_kind_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								}
								$index++;
							}
							// 기타 항목
							echo '<input type="radio" name="emp_kind" class="imgM" id="emp_kind_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['emp_kind'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="emp_kind_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="emp_kind_etc_holder" data-role-etc_index="<?php echo $index_etc;?>">
								<input type="text" name="emp_kind_etc" id="emp_kind_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>
					</tr>
					<!-- // 사용주체 항목 주석처리 - 2015.11.23 요청에 의한 처리 delee
					<tr>
						<th>사용주체 * <a href="javascript:open_help(4)"><img src="../images/common/icon_que.png" alt="사용주체란" class="imgM"></a></th>
						<td>
						<?php
						// $index = 0;
						// foreach($res['emp_use_kind'] as $item) {
						// 	echo '<input type="radio" name="emp_use_kind" class="imgM" id="emp_use_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_use_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_use_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						// }
						?>
						</td>
					</tr>
					-->
					<tr>
						<th>근로자수</th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_cnt'] as $item) {
							echo '<input type="radio" name="emp_cnt" class="imgM" id="emp_cnt_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_cnt'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_cnt_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>
					</tr>
					<tr>
						<th>근로계약서 작성</th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_paper_yn'] as $item) {
							echo '<input type="radio" name="emp_paper_yn" class="imgM" id="emp_paper_yn_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_paper_yn'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_paper_yn_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>
					<tr>
						<th>4대보험가입</th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_insured_yn'] as $item) {
							echo '<input type="radio" name="emp_insured_yn" class="imgM" id="emp_insured_yn_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_insured_yn'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_insured_yn_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>
					<tr>
						<th>월평균임금</th>
						<td>
							<input type="text" id="ave_pay_month" name="ave_pay_month" maxlength="30" value="<?php echo $edit['ave_pay_month'];?>">
						</td>						
					</tr>
					<tr>
						<th>주당 근로시간</th>
						<td>
							<input type="text" id="work_time_week" name="work_time_week" maxlength="30" value="<?php echo $edit['work_time_week'];?>">
						</td>						
					</tr>
				</table>
				<h3 class="sub_stit">상담유형 및 처리결과</h3>
				<table class="tInsert">
					<caption>
						상담유형 및 처리결과 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>상담유형 *</th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_kind'] as $item) {
							echo '<input type="checkbox" id="csl_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. (strpos($edit['csl_sub_code'], $item->s_code)!==FALSE ? 'checked="checked"' : '') .' class="imgM"><label for="csl_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<input type="hidden" name="csl_kind"></input>
						</td>
					</tr>
					<tr>
						<th>상담내용 *</th>
						<td>
							<button type="button" class="buttonS bGray marginT03" id="btnGetExistCounsel">불러오기</button>
							<input type="text" name="csl_title" id="csl_title" style="width:83%;" maxlength="200" class="input_font_size14" value="<?php echo $edit['csl_title'];?>">
							<textarea class="marginT05 input_font_size14" style="width:90%;height:150px;" name="csl_content" id="csl_content"><?php echo $edit['csl_content'];?></textarea>
						</td>						
					</tr>
					<tr>
						<th>답변 *</th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="csl_reply" id="csl_reply"><?php echo $edit['csl_reply'];?></textarea></td>
					</tr>
					<tr>
						<th>처리결과 *</th>
						<td>
						<?php
						$index = 0; $index_etc = 0;
						$etc_item = array('index'=>0, 'item'=>new stdClass());
						foreach($res['csl_proc_rst'] as $item) {
							// 기타 항목은 항상 끝에 배치한다.
							if($item->code_name == '기타') {
								$etc_item['index'] = $index;
								$etc_item['item'] = $item;
								$index_etc = $index;
							}
							else {
								echo '<input type="radio" name="csl_proc_rst" class="imgM" id="csl_proc_rst_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['csl_proc_rst'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_proc_rst_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
							}
							$index++;
						}
						// 기타 항목
						echo '<input type="radio" name="csl_proc_rst" class="imgM" id="csl_proc_rst_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['csl_proc_rst'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="csl_proc_rst_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="csl_proc_rst_etc_holder" data-role-etc_index="<?php echo $index_etc;?>">
								<input type="text" name="csl_proc_rst_etc" id="csl_proc_rst_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>
					</tr>
					<!--
					<tr>
						<th>주제어</th>
						<td>
						<?php
						// $index = 0;
						// foreach($res['csl_keyword'] as $item) {
						// 	echo '<input type="checkbox" name="csl_keyword_'. $index .'" class="imgM" id="csl_keyword_'. $index .'" value="'. base64_encode($item->s_code) .'" '. (strpos($edit['csl_sub_code'], $item->s_code)!==FALSE ? 'checked="checked"' : '') .'><label for="csl_keyword_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						// }
						?>
						</td>
					</tr>	
					-->
					<tr>
						<th>상담사례 공유</th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_share_yn'] as $item) {
							echo '<input type="radio" name="csl_share_yn" class="imgM" id="csl_share_yn_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['csl_share_yn'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_share_yn_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
							<div class="f11 marginT05 marginL10">* "공유함" 선택시 상담내용 불러오기, 주요상담사례 게시판에 보여지게 됩니다.</div>
						</td>						
					</tr>
					<tr>
						<th>첨부파일</th>
						<td>
							<div id="info_files">* 최대 <?php echo CFG_UPLOAD_MAX_COUNT;?>개, 개당 <?php echo (CFG_UPLOAD_MAX_FILE_SIZE/1024768);?>MB까지 업로드 가능합니다.</div>
							<div id="file_wrapper">
							<?php
								$file_name = $edit['file_name'];
								$file_name_org = $edit['file_name_org'];
								if(strpos($file_name_org, CFG_UPLOAD_FILE_NAME_DELIMITER) !== FALSE) {
									$arr_file_name_org = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name_org);
									$arr_file_name_real = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $file_name);
								}
								else {
									$arr_file_name_real = array($file_name);
									$arr_file_name_org = array('file_name_org'=>$file_name_org);
								}

								$file_index = 0;

								if($res['kind'] == 'view'):
								else:
								
									foreach($arr_file_name_org as $file_name) {
							?>
								<div class="floatC" style="line-height:35px;margin-top:3px;" data-role-index="<?php echo $file_index;?>">
									<div class="floatL cssFileForm">
										<input type="hidden" id="filename_real_<?php echo $file_index;?>" value="<?php echo $arr_file_name_real[$file_index]; ?>"></hidden>
										<input type="text" id="filename_<?php echo $file_index;?>" class="floatL" readonly="readonly" value="<?php echo $file_name; ?>" style="width:280px;background:none;border:none;">
										<div class="file_up" title="<?php echo $file_name; ?>">
											<button type="button" class="buttonS bWhite btn_file" id="btnOpenFile">파일찾기</button>
											<input type="file" id="userfile_<?php echo $file_index;?>" name="userfile_<?php echo $file_index;?>" style="width:100%" onchange="javascript:chg_files(this, 'filename_<?php echo $file_index;?>');">
										</div>
									</div>
									<div class="floatL marginL05" style="margin-top:-3px;">
										<button type="button" class="buttonS bGray cssAddFile">추가</button> 
										<?php
										if($res['kind'] == 'edit') {
											echo('<button type="button" class="buttonS bGray cssDelFile" '. ($arr_file_name_real[$file_index] == '' ? 'disabled="disabled"' : '') .'>파일삭제</button>'. PHP_EOL);
											echo('<button type="button" class="buttonS bGray cssDownload" data-role-real="'. $arr_file_name_real[$file_index] .'" data-role-org="'. $file_name .'" '. ($arr_file_name_real[$file_index] == '' ? 'disabled="disabled"' : '') .'>다운로드</button>');
										}
										?>
									</div>
								</div>
							<?php
										$file_index++;
									}

								endif;
							?>
							</div>
						</td>
					</tr>
				</table>

				<div class="marginT10 textR">
					<a href="#" id="focusOuter"></a>
					<button type="button" class="buttonM bSteelBlue cssPrint">인쇄</button>
					<button type="button" class="buttonM bOrange cssAdd">등록</button>
					<button type="button" class="buttonM bGray cssBack">뒤로이동</button>
					<button type="button" class="buttonM bDarkGray cssList">목록</button>
				</div>
				</form>

			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		<?php
		// 상담내용 불러오기 팝업에서 사용할 검색 항목데이터
		$data_csl_proc_rst_code = '';
		foreach($res['csl_proc_rst_code'] as $rs) {
			if($data_csl_proc_rst_code !='') {
				$data_csl_proc_rst_code .= '|';
			}
			$data_csl_proc_rst_code .= base64_encode($rs->s_code) .'*'. $rs->code_name;
		}
		?>
		<span id="csl_proc_rst_code" style="display:none;width:0px;"><?php echo $data_csl_proc_rst_code;?></span>
<?php
include_once './inc/inc_footer.php';
?>
