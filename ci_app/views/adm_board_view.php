<?php
/**
 * 
 * 게시판 공통
 * 
 * 목록페이지
 * 
 * 
 */
include_once "./inc/inc_header.php";

//공유게시판
$brd_name = '운영';
$colspan = 8;
if($brd_id == '1') {
} 
// 주요상담사례
else if($brd_id == '2') {
	$brd_name = '주요상담사례';
	$colspan = 7;
} 
// 주요상담사례-사용자상담
else if($brd_id == '3') {
	$brd_name = '사용자 상담사례';
	$colspan = 7;
} 
// FAQ
else {
	$brd_name = 'FAQ';
	$colspan = 9;
}
?>



<script>
// board index, 보드명 아닌 LNB메뉴의 서브메뉴 index임
var brd_id = '<?php echo $brd_id; ?>';



$(document).ready(function(){
	
	// 등록 버튼 이벤트 핸들러 -- 운영게시판, FAQ
	$('#btnAdd').click(function(e){
		check_auth_redirect($('#'+ sel_menu).find('li>a').eq('<?php echo ($brd_id-1);?>'), 'add', '');
	});
	
	// 전송버튼 이벤트 핸들러
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		_page = 1;
		get_list(_page);
	});
	
	// 초기화 이벤트 핸들러
	$('#btnClear').click(function(e){
		e.preventDefault();
		_page = 1;
		$('form')[0].reset();
		$('#keyword').val('').css('display', 'inline');
		$('#target_keyword').css('display', 'none'); // 주제어
		$('#target_work_kind').css('display', 'none'); // 직종
		$('#target_comp_kind').css('display', 'none'); // 업종
		$('#target_csl_proc_rst').css('display', 'none'); // 처리결과
	});
	

	// 검색 - 검색어 변경시 처리
	$('#target').change(function(e){
		var sel = $('#target option:selected').val();
		var display1 = display2 = display3 = display4 = 'none';
		display1 = sel == 'counsel_keyword' ? 'inline' : 'none';
		display2 = sel == 'counsel_work_kind' ? 'inline' : 'none';
		display3 = sel == 'counsel_comp_kind' ? 'inline' : 'none';
		display4 = sel == 'counsel_proc_rst' ? 'inline' : 'none';

		var keyword_val = '';
		var kwyword_display = 'inline';
		$('#keyword').css('display', 'inline');
		if(sel == 'counsel_keyword' || sel == 'counsel_work_kind' || sel == 'counsel_comp_kind' || sel == 'counsel_proc_rst') {
			keyword_val = '';
			// 직종,업종은 keyword를 항상 노출한다.
			if(sel == 'counsel_keyword' || sel == 'counsel_proc_rst') {
				kwyword_display = 'none';
			}
		}
		$('#target_keyword').css('display', display1); // 주제어
		$('#target_work_kind').css('display', display2); // 직종
		$('#target_comp_kind').css('display', display3); // 업종
		$('#target_csl_proc_rst').css('display', display4); // 처리결과
		$('#keyword').val(keyword_val).css('display', kwyword_display);
	});

	// 검색 - 주제어 변경시, 주제어는 기타가 입력이 아닌 코드임
	// $('#target_keyword').change(function(e){
		// var selVal = $('#target_keyword option:selected').val();
		// // keyword에 선택한 option의 data를 저장해서 넘긴다.
		// if(selVal != '') selVal = Base64.decode(selVal);
		// $('#keyword').val(selVal);
	// });
	
	// 검색 - 처리결과 변경시
	$('#target_csl_proc_rst').change(function(e){
		// var etc_flag = 0;
		// 기타 선택시 - 입력박스 show
		var selText = $('#target_csl_proc_rst option:selected').text();
		var display = 'none';
		if(selText == '기타') {
			display = 'inline';
			// $('#keyword').val("<?//php echo isset($search_data['keyword']) ? $search_data['keyword'] : '';?>");
			// $('#keyword').val("");
			// etc_flag = 1;
		}
		// else {
		// 	var selVal = $('#target_csl_proc_rst option:selected').val();
		// 	// keyword에 선택한 option의 data를 저장해서 넘긴다.
		// 	if(selVal != '') selVal = Base64.decode(selVal);
		// 	$('#keyword').val(selVal);
		// }
		$('#keyword').val("").css('display', display);
	});
	
	// 검색 - 검색어 엔터 처리
	$("#keyword").keypress(function(e) {
		if (e.keyCode == 13) {
			$("#btnSubmit").click();
		}
	});

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();

		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_end = '';
		}

		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});	
	
	// 삭제 버튼 이벤트 핸들러
	$('#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var oper_id = '', seqs = '';
			$('input[name=chk_item]:checked').each(function(i){
				if(seqs != '') seqs +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				seqs += $(this).attr('data-role-id');
			});
			
			kind = 'del_multi';
			if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				return false;
			}
			req_code_manage(kind, seqs);
		}
	});

	
	// *** 상담일만 적용해 달라는 요청 2016.06.15 최진혁
	// 검색어 채워넣기
	// - 상담방법
	// var csl_way = Base64.encode("<?//php echo isset($search_data['csl_way']) ? $search_data['csl_way'] : '';?>");
	// $('select[name=csl_way] option[value="'+ csl_way +'"]').prop('selected', true);
	// - 상담일
	$('input[name=search_date_begin]').val("<?php echo isset($search_data['search_date_begin']) ? $search_data['search_date_begin'] : '';?>");
	$('input[name=search_date_end]').val("<?php echo isset($search_data['search_date_end']) ? $search_data['search_date_end'] : '';?>");
	// - 검색대상
	// $('select[name=target] option[value="<?//php echo isset($search_data['target']) ? $search_data['target'] : '';?>"]').prop('selected', true);
	// $('select[name=target]').change();
	// - 검색어
	// $('input[name=keyword]').val("<?//php echo isset($search_data['keyword']) ? $search_data['keyword'] : '';?>");
	
	// - 검색대상 : 처리결과인 경우
	// var target_proc_rst = "<?//php echo isset($search_data['target_proc_rst']) && $search_data['target_proc_rst']!='' ? $search_data['target_proc_rst'] : '';?>";
	// if(target_proc_rst != '') {
	// 	target_proc_rst = Base64.encode(target_proc_rst);
	// 	$('select[name=target_csl_proc_rst] option[value="'+ target_proc_rst +'"]').prop('selected', true);
	// 	$('select[name=target_csl_proc_rst]').change();
	// }

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for(var i=year; i>=2015; i--){
		$("#sel_year").append("<option value="+i+">"+i+"년</option>");
	}

	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates();
	});

	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});


	// list
	get_list(_page);

	$('table tbody tr').hover(function() {
		$(this).css( "background-color", "#efefef" );
	}, function() {  
		$(this).css( "background-color", "transparent" );
	});

});



/**
 * 리스트를 동적생성하면 이벤트를 다시 걸어줘야 한다.
 */
function addManagerEventListener() {
	// 관리버튼 클릭 이벤트
	$('button.ai_cmd, a.ai_cmd').click(function(e){
		e.preventDefault();
		var cmd = $(this).text();

		// 자신의 글인지 체크
		var isowner = $(this).attr('data-role-isowner');
		if(is_master != 1 && isowner == 0) {
			var msg = CFG_MSG[CFG_LOCALE]['warn_board_02'];
			msg = msg.replace('수정', cmd);
			alert(msg);
			return false;
		}

		var seq = $(this).attr('data-role-id');
		if(cmd == '삭제') {
			if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				req_code_manage('del', seq);
			}
		}
		else {
			var kind = 'view';
			if(cmd == '수정') kind = 'edit';
			check_auth_redirect($('#'+ sel_menu).find('li>a').eq('<?php echo $brd_id-1;?>'), kind, brd_id, seq);
		}
	});
	
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		$('input:checkbox').not(this).prop('checked', this.checked);
	});
	
	// 상담내용보기 버튼
	$('button.ai_cmd_view').click(function(e) {
		var auth = $(this).attr('data-role-auth') == 'true' ? 1 : 0;
		var ctrl = brd_id == 2 ? 'counsel' : 'biz_counsel';  
		var url = '/?c='+ ctrl +'&m=open_counsel_at&seq='+ $(this).attr('data-role-id') +'&auth='+ auth;
		gLayerId = openLayerModalPopup(url, 800, 700, '', '', '', '', 1, true);
	});
}


/**
 * 삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, tid) {
	// 권한체크
	var jqObj = $('#'+ sel_menu).find('li>a').eq('<?php echo $brd_id-1;?>');
	var p = jqObj.parents('ul').attr('id');
	var menu_id = Base64.encode(Aes.Ctr.encrypt(p +'_'+ jqObj.attr('data-role-index'), "<?php echo CFG_ENCRYPT_KEY; ?>", 256));
	var code = jqObj.attr('data-role-code');

	// master 제외
	if(is_master == 1) {
		_req_process(type, tid);
	}
	else {
		var url = '/?c=auth&m=req_auth';
		var rsc = { c: code };
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				_req_process(type, tid);
			}
			else if(data.rst == 'session_timeout') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_09']);
				location.href = '/?c=admin';
			}
			else {
				var msg = CFG_MSG[CFG_LOCALE]['info_auth_01'];
				if(data.msg) msg += '[' + data.msg +']';
				alert(msg);
			}
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
		
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
			gen_list();
		};
		req_ajax(url, rsc, fn_succes, fn_error);
	}
}


/**
 * 서버로 데이터 요청
 */
function _req_process(type, tid) {
	var url = '/?c=board&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&seq='+ tid +'&brd_id='+ brd_id;
	var fn_succes = function(data) {
		if(type != 'view') {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
		}
		if(data) {
			gen_list(data);
		}
		else {
			gen_list();
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * 서버에서 받은 데이터로 리스트, 페이징 html 생성
 */
function get_list(page) {
	// search date
	var date_b = $('#search_date_begin').val();
	var date_e = $('#search_date_end').val();
	if((date_b && date_e == '' ) || (date_b == '' && date_e)) {
		var now = new Date();
		var tmp_date = now.getFullYear() +'-'+ genTwoDigit(now.getMonth()+1) +'-'+ genTwoDigit(now.getDate());
		if(date_b == '') $('#search_date_begin').val(tmp_date);
		if(date_e == '') $('#search_date_end').val(tmp_date);
	}

	// page
	_page = page;

	var data = '&is_master='+ is_master;
	var url = '/?c=';
	// 상담내역
	if(brd_id == 2) {
		url += 'counsel&m=counsel_list';
		data += '&isbrd=1';
	}
	// 사용자상담
	else if(brd_id == 3) {
		url += 'biz_counsel&m=counsel_list';
		data += '&isbrd=1';
	}
	else {
		url += 'board&m=board_list';
		data += '&brd_id='+ brd_id;
	}
	
	var rsc = $('#frmSearch').serialize() + '&page='+_page + data;
	var fn_succes = function(data) {
		if(data && data.tot_cnt > 0) {
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.itemPerPage = 10;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			gen_list(data);
		}
		else {
			gen_list();
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	if(!_pagination) {
		_pagination = new Pagination(_cfg_pagination);
	}

	var headerHtml1 = '';
	var headerHtml2 = '';
	
	if(brd_id == 1) {
		$('div.cssCmdBtn').show();
		// 공유게시판
		headerHtml1 += '<col style="width:4%">';
		headerHtml1 += '<col style="width:6%">';
		headerHtml1 += '<col style="width:4%">';
		headerHtml1 += '<col style="width:*">';
		headerHtml1 += '<col style="width:7%">';
		headerHtml1 += '<col style="width:5%">';
		headerHtml1 += '<col style="width:13%">';
		headerHtml1 += '<col style="width:11%">';
		//
		headerHtml2 += '<th><input type="checkbox" class="imgM checkAll"></th>';
		headerHtml2 += '<th>번호</th>';
		headerHtml2 += '<th>파일</th>';
		headerHtml2 += '<th>제목</th>';
		headerHtml2 += '<th>작성자</th>';
		headerHtml2 += '<th>조회</th>';
		headerHtml2 += '<th>등록일</th>';
		headerHtml2 += '<th>관리</th>';
	}
	// 주요상담사례
	else if(brd_id == 2) {
		headerHtml1 += '<col style="width:6%">';
		headerHtml1 += '<col style="width:8%">';
		headerHtml1 += '<col style="width:*">';
		headerHtml1 += '<col style="width:11%">';
		headerHtml1 += '<col style="width:12%">';
		headerHtml1 += '<col style="width:10%">';
		headerHtml1 += '<col style="width:7%">';
		//
		headerHtml2 += '<th>번호</th>';
		headerHtml2 += '<th>상담방법</th>';
		headerHtml2 += '<th>제목</th>';
		headerHtml2 += '<th>상담자</th>';
		headerHtml2 += '<th>소속</th>';
		headerHtml2 += '<th>상담일</th>';
		headerHtml2 += '<th>상담내역</th>';
	}
	// 사용자 상담사례
	else if(brd_id == 3) {
		headerHtml1 += '<col style="width:6%">';
		headerHtml1 += '<col style="width:8%">';
		headerHtml1 += '<col style="width:*">';
		headerHtml1 += '<col style="width:11%">';
		headerHtml1 += '<col style="width:12%">';
		headerHtml1 += '<col style="width:10%">';
		headerHtml1 += '<col style="width:7%">';
		//
		headerHtml2 += '<th>번호</th>';
		headerHtml2 += '<th>상담방법</th>';
		headerHtml2 += '<th>제목</th>';
		headerHtml2 += '<th>상담자</th>';
		headerHtml2 += '<th>소속</th>';
		headerHtml2 += '<th>상담일</th>';
		headerHtml2 += '<th>상담내역</th>';
	}
	else {
		$('div.cssCmdBtn').show();
		// FAQ
		headerHtml1 += '<col style="width:4%">';
		headerHtml1 += '<col style="width:6%">';
		headerHtml1 += '<col style="width:11%">';
		headerHtml1 += '<col style="width:4%">';
		headerHtml1 += '<col style="width:*">';
		headerHtml1 += '<col style="width:7%">';
		headerHtml1 += '<col style="width:5%">';
		headerHtml1 += '<col style="width:13%">';
		headerHtml1 += '<col style="width:11%">';
		//
		headerHtml2 += '<th><input type="checkbox" class="imgM checkAll"></th>';
		headerHtml2 += '<th>번호</th>';
		headerHtml2 += '<th>상담방법</th>';
		headerHtml2 += '<th>파일</th>';
		headerHtml2 += '<th>제목</th>';
		headerHtml2 += '<th>작성자</th>';
		headerHtml2 += '<th>조회</th>';
		headerHtml2 += '<th>등록일</th>';
		headerHtml2 += '<th>관리</th>';
	}
	
	// file icon
	var arr_file_names = '', file_names = '', html_file_icon = '&nbsp;';

	var html_b = '<table class="tList">';
	html_b += '<caption>게시판 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += headerHtml1;
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += headerHtml2;
	html_b += '</tr>';
	var reg_date = "";
	
	// 운영게시판 공지글
	if(data && typeof data.noti == 'object' && data.noti.length > 0) {
		for(var i=0; i<data.noti.length; i++) {
			// file icon
			html_file_icon = '&nbsp;';
			if(data.noti[i].file_name != '') {
				arr_file_names = data.noti[i].file_name_org.split('<?php echo CFG_UPLOAD_FILE_NAME_DELIMITER;?>');
				for(var x=0; x<arr_file_names.length; x++) {
					if(file_names != '') file_names += '\n';
					file_names += arr_file_names[x];
				}
				html_file_icon = '<img src="./images/icons/icon_file.png" width="11" height="13" align="absmiddle" alt="file_image" title="'+ file_names +'" />';
			}
			// yyyy-mm-dd
			reg_date = data.noti[i].reg_date.split(" ")[0];
			html_b += '<tr>';
			html_b += '  <td style="background-color:#fff4c7;text-align:left;" colspan="2">[공지]</td>';
			html_b += '  <td style="background-color:#fff4c7">'+ html_file_icon +'</td>';
			html_b += '  <td style="background-color:#fff4c7;text-align:left;"><a href="#" class="ai_cmd" data-role-id="'+ data.noti[i].seq +'" class="marginL05">'+ data.noti[i].title +'</a></td>';
			html_b += '  <td style="background-color:#fff4c7">'+ data.noti[i].oper_name +'</td>';
			html_b += '  <td style="background-color:#fff4c7">'+ data.noti[i].view_cnt  +'</td>';
			html_b += '  <td style="background-color:#fff4c7">'+ reg_date +'</td>';
			html_b += '  <td style="background-color:#fff4c7">-</td>';
			html_b += '</tr>';
		}
	}

	// 사용자 상담사례
	if(brd_id == 3) {
		if(data && typeof data.csl == 'object' && data.tot_cnt > 0) {
			data.data = data.csl;
		}
		else {
			data = {data:undefined};
		}
	}
	// 일반글
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		var isowner = 0;

		// list 생성
		for(var i=begin; i<end; i++) {
			html_b += '<tr>';
			
			// 2: 주요상담사례, 3: 사용자 상담사례
			if(brd_id == 2 || brd_id == 3) {

				// yyyy-mm-dd
				reg_date = data.data[index].csl_date.split(" ")[0];
				
				html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ data.data[index].seq +'" style="display:none">'+ (no--) +'</td>';
				html_b += '  <td>'+ data.data[index].csl_way +'</td>';
				html_b += '  <td style="text-align:left;" class="marginL05">'+ data.data[index].csl_title +'</td>';
				html_b += '  <td>'+ data.data[index].oper_name +'</td>';
				html_b += '  <td>'+ data.data[index].asso_name +'</td>';
				html_b += '  <td>'+ reg_date +'</td>';
				html_b += '  <td>';
				html_b += '    <button type="button" class="buttonS bGray ai_cmd_view" data-role-id="'+ data.data[index].seq +'" data-role-auth="'+ (data.data[index].oper_id == '<?php echo $oper_id;?>') +'">보기</button>';
				html_b += '  </td>';
			}
			// 운영,FAQ 게시판
			else {
				html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ data.data[index].seq +'"></td>';
				html_b += '  <td>'+ (no--) +'</td>';
				// file icon
				html_file_icon = '&nbsp;', file_names = '';
				if(data.data[index].file_name != '') {
					arr_file_names = data.data[index].file_name_org.split('<?php echo CFG_UPLOAD_FILE_NAME_DELIMITER;?>');
					for(var x=0; x<arr_file_names.length; x++) {
						if(file_names != '') file_names += '\n';
						file_names += arr_file_names[x];
					}
					html_file_icon = '<img src="./images/icons/icon_file.png" width="11" height="13" align="absmiddle" alt="file_image" title="'+ file_names +'" />';
				}

				// 자신의 글 여부 1, 0
				isowner = data.data[index].oper_id == '<?php echo $oper_id;?>' ? 1 : 0;

				// FAQ, 운영게시판만
				if(brd_id == 4) {
					html_b += '  <td>'+ data.data[index].code_name +'</td>';
				}
				// yyyy-mm-dd
				reg_date = data.data[index].reg_date.split(" ")[0];

				html_b += '  <td>'+ html_file_icon +'</td>';
				html_b += '  <td style="text-align:left;"><a href="#" class="ai_cmd" data-role-id="'+ data.data[index].seq +'" class="marginL05">'+ data.data[index].title +'</a></td>';
				html_b += '  <td>'+ data.data[index].oper_name +'</td>';
				html_b += '  <td>'+ data.data[index].view_cnt  +'</td>';
				html_b += '  <td>'+ reg_date +'</td>';
				html_b += '  <td>';
				html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ data.data[index].seq +'" data-role-isowner="'+ isowner +'">수정</button>';
				html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ data.data[index].seq +'" data-role-isowner="'+ isowner +'">삭제</button>';
				html_b += '  </td>';
			}
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="<?php echo $colspan;?>" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
//	alert(html_b);
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
	
	// 관리버튼 이벤트 등록
	addManagerEventListener();
}
</script>

<?php
include_once './inc/inc_menu.php';
?>



		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit"><?php echo $brd_name; ?> 게시판</h2>
				<span class="location">홈 > 게시판 > <em><?php echo $brd_name; ?></em></span>
			</div>
			<div class="cont_area">
				<form name="frmSearch" id="frmSearch" method="post">
				<div class="con_text">	
				<?php
					$date_title = '등록일';
					if($brd_id != '1'):
				?>
					<label for="csl_way" class="l_title">상담방법</label>
					<select name="csl_way" id="csl_way" class="styled" style="width:20%">
						<option value="">전체</option>
						<?php
						foreach($res['csl_way'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<span class="divice"></span>
				<?php
					elseif($brd_id == 2):
						$date_title = '상담일';

					endif;
				?>
					<label for="board_select" class="l_title"><?php echo $date_title;?></label>
					<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" style="width:90px" readonly="readonly"> ~ 
					<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" style="width:90px" readonly="readonly">
					<label for="sel_year" class="l_title" style="margin-left:10px;">연월별</label>
					<select id="sel_year" name="sel_year" style="margin-left:-20px;">
					<option value="">연도</option>
					</select>
					<select id="sel_month" name="sel_month">
						<option value="">전체</option>
						<option value="01">1월</option>
						<option value="02">2월</option>
						<option value="03">3월</option>
						<option value="04">4월</option>
						<option value="05">5월</option>
						<option value="06">6월</option>
						<option value="07">7월</option>
						<option value="08">8월</option>
						<option value="09">9월</option>
						<option value="10">10월</option>
						<option value="11">11월</option>
						<option value="12">12월</option>
					</select>
					<label for="sel_year" class="l_title" style="margin-left:10px;">기간별</label>
					<button type="button" id="btnToday" class="buttonS bGray"  style="margin-left:-20px;">오늘</button>
					<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
					<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
					<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button>
					<br>
					<span class="divice"></span>
					<label for="search_target" class="l_title">검색어</label>
					<select id="target" name="target" class="styled">
						<option value="" selected>전체</option>	
				<?php
					// 주요상담사례 게시판 || 사용자상담
					if($brd_id == '2' || $brd_id == '3'):
					
						if($brd_id == '2'):
				?>
						<option value="Q1.csl_title">제목</option>
						<option value="Q1.csl_content">상담 내용</option>
						<option value="Q1.csl_reply">답변</option>
						<option value="counsel_work_kind">직종</option>
						<option value="counsel_comp_kind">업종</option>
						<option value="counsel_keyword">주제어</option>
						<option value="counsel_proc_rst">처리결과</option>
						<!-- 상담자,내담자 항목 추가 - 2015.11.23 요청에 의한 추가 delee -->
						<option value="counsel_oper_id">상담자ID</option>
						<option value="counsel_oper_name">상담자</option>
						<option value="Q1.csl_name">내담자</option>
				<?php
						// 사용자상담사례 게시판
						else:
				?>
						<option value="Q1.csl_title">제목</option>
						<option value="Q1.csl_content">상담 내용</option>
						<option value="Q1.csl_reply">답변</option>
						<option value="counsel_comp_kind">업종</option>
						<option value="counsel_keyword">주제어</option>
						<option value="counsel_proc_rst">처리결과</option>
						<option value="counsel_oper_id">상담자ID</option>
						<option value="counsel_oper_name">상담자</option>
						<option value="Q1.csl_cmp_nm">사업장명</option>
						<option value="Q1.csl_name">대표자명</option>
						<option value="counsel_csl_tel">연락처 뒤4자리</option>
				<?php 
						endif;
				?>
					</select>
					<!-- 주제어 항목 -->
					<select id="target_keyword" class="styled" name="target_keyword" style="display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_keyword'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<!-- 직종 항목 -->
					<select id="target_work_kind" class="styled" name="target_work_kind" style="display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_work_kind_code'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<!-- 업종 항목 -->
					<select id="target_comp_kind" class="styled" name="target_comp_kind" style="display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_comp_kind_code'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<!-- 처리결과 항목 -->
					<select id="target_csl_proc_rst" class="styled" name="target_csl_proc_rst" style="display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_proc_rst_code'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<input type="text" style="width:50%" id="keyword" name="keyword">
					<div>전체 검색은 제목,상담 내용,답변,직종/업종의 기타,상담자,내담자를 대상으로 검색됩니다.</div>
					<span class="divice"></span>
				<?php
					else:
				?>
						<option value="B.title">제목</option>
						<option value="B.content">내용</option>
						<option value="O.oper_id">아이디</option>
						<option value="O.oper_name">성명</option>
					</select>
					<input type="text" id="keyword" name="keyword" style="width:60%">
					<span class="divice"></span>
				<?php
					endif;
				?>
					<div class="textR" style="margin-top:-35px;">
						<button type="button" id="btnClear" class="buttonS bGray"><span class="icon_all"></span>초기화</button>
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>

				<div class="list_no"></div>	
				
				<!-- list -->
				<div id="list"></div>
		
				<div class="floatC marginT10 cssCmdBtn" style="display:none;">
					<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>
					<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">등록</button>
				</div>
				
				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>
				
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		
		
<?php
include_once './inc/inc_footer.php';
?>