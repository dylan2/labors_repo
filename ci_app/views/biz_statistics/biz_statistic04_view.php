<?php
/**
 * 
 * 사용자상담 - 소속별 통계
 * 
 */
include_once "./inc/inc_header.php";
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="./js/counsel/common.js"></script>
<script>
var chart_mode = 1;

$(document).ready(function(){

	//차트 보기
	$('#view_chart').click(function(e){
    e.preventDefault();
    if(chart_mode == 0){ //숨기기
      $('.chart').css('display', 'none');
      $('#view_chart').text('통계차트 보기');
      $('#downchart').css('display', 'none');
      $('#printchart').css('display', 'none');
    	chart_mode = 1;
    }
    else{
      $('.chart').css('display', 'block');
      $('#view_chart').text('통계차트 숨기기');
      $('#downchart').css('display', 'inline');
      $('#printchart').css('display', 'inline');
			chart_mode = 0;
    	var position = $('#downchart').offset();		//스크롤이동
			$('html, body').animate({scrollTop : position.top}, 1000);
			
      // 구글차트를 위한 데이터를 가져옴
      var url = '/?c=biz_statistic&m=st04';
      var rsc = $('#frmSearch').serialize();
      var fn_succes = function(data) {
          column_chart(data);
          var rst = data.data_tot[0]['tot'];
          if(rst == '0 (0%)' || rst == ' (0%)' || rst == '0(0) (0%)') {
            alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
          }
      };
      var fn_error = function(data) {
          if(is_local) objectPrint(data);
      };
      // request
      req_ajax(url, rsc, fn_succes, fn_error);
    }
  });

	//위로 이동
	$('#view_top').click(function(e){
    e.preventDefault();
    var position = $('#contents_body').offset();
    $('html, body').animate({scrollTop : position.top}, 1000);
  });

	// 통계유형 탭 클릭 이벤트 핸들러
	$(".stab_btn").on("click", function(e) {
		e.preventDefault();
		var index = $(".stab_btn").index(this);
		$('#txt_sch_kind').val($(this).text());
		$('#sch_kind').val(index);
		$(".stab_btn").attr('class','stab_btn');
		$(this).attr('class','stab_btn active');
		//검색대상이 상담유형이면, 안내 레이블 출력
		if(index == 8) $('#infoLabel').show();
    else $('#infoLabel').hide();

		get_list(_page);
	});

	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#s_code').val(data.s_code);
				}
				if(data.code_name){
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=biz_statistic&m=s_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e){
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#csl_proc_rst').val(data.s_code);
				}
				if(data.code_name){
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=biz_statistic&m=csl_proc_rst_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e){
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#asso_code').val(data.s_code);
				}
				if(data.code_name){
					$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=biz_statistic&m=asso_code_popup_view';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e){
		e.preventDefault();
		$('#asso_code').val( $('#asso_code_all').val() );
		$('#asso_code_name').text("전체");
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
	});

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for(var i=year; i>=2015; i--){
		$("#sel_year").append("<option value="+i+">"+i+"년</option>");
	}

	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates('', 2);
		if($(this).val() == '') {
			$('#btnAllTerm').click();
		}
	});

	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});

	//통계탭 이동
	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this)+1;
		var begin = $('#search_date_begin').val();
		var end = $('#search_date_end').val();
		var sch_asso_code = $("#asso_code").val();//소속코드
		var sch_s_code = $("#s_code").val();//상담방법
		var sch_csl_proc_rst = $("#csl_proc_rst").val();//처리결과
		
		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});

	// print a list
	$('#printlist').click(function(){
		var txt_sch_kind = $('#txt_sch_kind').val();
		var popupWindow = window.open("", "_blank");      
    var $div = $('#list').clone();
    $div.find('table').css('border-collapse','collapse').find('th, td').css({'border':'solid 1px #eee', 'height':'30px'});
    $div.find('th').css('background-color','#e8fcff');
		popupWindow.document.write("<body>");
		popupWindow.document.write("<h3>기본통계 "+txt_sch_kind+"</h3>");
		popupWindow.document.write($div.html());
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("table").css("width", "100%");
		$(popupWindow.document).find("td").css("text-align", "center");
		popupWindow.print();
		popupWindow.close();
	});

	// print a chart images
	$('#printchart').click(function(){
		var popupWindow = window.open("", "_blank");      
		var div = $('#chart_div').html();
		popupWindow.document.write("<body>");
		popupWindow.document.write(div);
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("img").css("width", "100%");
		popupWindow.print();
		popupWindow.close();
	});

	// download a chart images
	$('#downchart').click(function(){
		$('#chart_div').css('display','block');
		html2canvas($("#chart_div"), {
			// html2canvas는 보이는 부분만 캡쳐하기 때문에 chart_div내 첫번째 차트 이미지의 가로 크기가 커지는 경우 
			// 캡쳐안되는 문제가 있어 첫번째 차트 이미지의 크기를 캡쳐영역으로 지정하도록 수정함
	    width: $('#chart_div0').find('img').width(),
	    height: $('#chart_div').height(),
			onrendered: function(canvas) {
				var img = canvas.toDataURL();
				$("body").append('<form id="imgForm" method="POST" target="_blank" action="/?c=chart_download&m=download">' +
						'<input type="hidden" name="dataUrl" value="' + img + '">' +
						'</form>');
				$("#imgForm").submit();
				$("#imgForm").remove();
			}
		});
		$('#chart_div').css('display','none');
	});
    
	// download a excel file
	$('#downExcel').click(function(){
		if($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var sch_kind_index = 0;
		sch_kind_index = $('#sch_kind').val();
		
		var url = '/?c=biz_statistic&m=open_down_view&kind=down04&sel_col='+sch_kind_index;
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
	});

	// 검색
  $("#btnSubmit").click(function(e) {
		e.preventDefault();
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val( $('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());

		get_list(_page);
  });

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();
		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_begin = '2015-01-01';
		}
		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});
	
	// list
	get_list(_page, true);
});


function get_today() {
	var now = new Date();
	return now.getFullYear() +'-'+ genTwoDigit(now.getMonth()+1) +'-'+ genTwoDigit(now.getDate());
}

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	_page = page;

	var url = '/?c=biz_statistic&m=st04';
	var rsc = $('#frmSearch').serialize();
	var fn_succes = function(data) {
		gen_list(data);
		//차트 보기 상태일 때
		if(chart_mode == 0){	
			column_chart(data);
		}
		var rst = data.data_tot[0]['tot'];
    if(rst == '0 (0%)' || rst == ' (0%)' || rst == '0(0) (0%)') {
    	alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
    } 
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var sch_kind = $('#sch_kind').val();
	var index = 0;
	var total = data.data_tot[0]['tot'];

	var html_b = '<table class="tList02" border=0>';
	html_b += '<colgroup>';
	html_b += '<col style="width:*">';
	if(sch_kind == 0){<?php foreach($res['code_0'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담방법
	else if(sch_kind == 1){<?php foreach($res['code_1'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//소재지
	else if(sch_kind == 2){<?php foreach($res['code_2'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//근로자수
	else if(sch_kind == 3){<?php foreach($res['code_3'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//업종
	else if(sch_kind == 4){<?php foreach($res['code_4'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//운영기간
	else if(sch_kind == 5){<?php foreach($res['code_5'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//근로계약서
	else if(sch_kind == 6){<?php foreach($res['code_6'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//4대보험
	else if(sch_kind == 7){<?php foreach($res['code_7'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//취업규칙
	else if(sch_kind == 8){<?php foreach($res['code_8'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//상담유형
	else if(sch_kind == 9){<?php foreach($res['code_9'] as $item) { echo "html_b += '<col style=\"width:*\">';"; }?>}//처리결과
	html_b += '<col style="width:*">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th style="line-height:15px;">구분</th>';
	var txt_sch_kind8 = '';
	if(sch_kind == 0){<?php foreach($res['code_0'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 1){<?php foreach($res['code_1'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 2){<?php foreach($res['code_2'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 3){<?php foreach($res['code_3'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 4){<?php foreach($res['code_4'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 5){<?php foreach($res['code_5'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 6){<?php foreach($res['code_6'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 7){<?php foreach($res['code_7'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	else if(sch_kind == 8){
		txt_sch_kind8 = '(실건수) ';
		<?php foreach($res['code_8'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>
	}
	else if(sch_kind == 9){<?php foreach($res['code_9'] as $item) { echo "html_b += '<th style=\"line-height:15px;\">". $item->code_name ."</th>';"; }?>}
	html_b += '<th style="line-height:15px;">총계 '+ txt_sch_kind8 +' (비율)</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = total_cnt;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			html_b += '<tr>';
			html_b += '  <td>'+ data.data[index].subject +'</td>';
			if(sch_kind == 0){<?php for($i=0; $i<count($res['code_0']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 1){<?php for($i=0; $i<count($res['code_1']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 2){<?php for($i=0; $i<count($res['code_2']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 3){<?php for($i=0; $i<count($res['code_3']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 4){<?php for($i=0; $i<count($res['code_4']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 5){<?php for($i=0; $i<count($res['code_5']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 6){<?php for($i=0; $i<count($res['code_6']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 7){<?php for($i=0; $i<count($res['code_7']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 8){<?php for($i=0; $i<count($res['code_8']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			else if(sch_kind == 9){<?php for($i=0; $i<count($res['code_9']); $i++) { echo "html_b += '  <td>'+ data.data[index].ck". ($i+1) ." +'</td>';"; }?>}
			if(data.data[index].tot != 0){
				html_b += '  <td>'+ data.data[index].tot +'</td>';
			}
			else{
				html_b += '  <td>0(0.0%)</td>';
			}
			html_b += '</tr>';
			index++;
		}
		// 구분별 total
		html_b += '<tr><td style="background:#F1F1F1 !important;">총계 (비율)</td>';
		for(var item in data.data_tot[0]) {
			html_b += '  <td style="background:#F1F1F1 !important;">'+ data.data_tot[0][item] +'</td>';
		}
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="30" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// list html
	$('#list').empty().html(html_b);
	$('#list th,td').css({whiteSpace:'nowrap'});

	// 세로 합계 퍼센트 추가
	var arr_per = [], tmp_arr = [];
	$('table.tList02 tr:gt(0)').each(function(i,o){
		if($(o).find('td').eq(0).text().indexOf('총계') == -1) {
			$(o).find('td').each(function(i2,o2){
				var str = $(o2).text();
				if(i2 > 0) {
					var val = +str.split('(')[1].split('%)')[0];
					if(!arr_per[i2-1]) {
						arr_per[i2-1] = 0;
					}
					arr_per[i2-1] += +val.toFixed(1);
				}
			});
		}
	});
	$('table.tList02 tr:last td').not(':last').each(function(i,o){
		if($(o).text().indexOf('총계') == -1) {
			$(o).text( $(o).text() +' ('+ arr_per[i-1].toFixed(1) +'%)' );
		}
	});
	
	// 상담기간
	var sch_date = $('#search_date_begin').val() +' ~ '+ $('#search_date_end').val();
	if(sch_date == ' ~ ') sch_date = '전체';
	$('#csl_date').html(sch_date);

}

/*막대그래프 만들기 시작*/
google.charts.load('current', {'packages':['corechart', 'corechart']});

function column_chart(data) {
	var chart_html = '';
	for (var i = 0; i < Object.keys(data.data_tot[0]).length; i++) {
		chart_html += '<div id="chart_div' + i + '" class="chart_div"></div>';
		$('#chart_div').html(chart_html);
	}

	var txt_sel_col = $('#txt_sch_kind').val();
	var index = 0;
	var sch_kind = $('#sch_kind').val();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

  if(sch_kind == 0){<?php foreach($res['code_0'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 1){<?php foreach($res['code_1'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 2){<?php foreach($res['code_2'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 3){<?php foreach($res['code_3'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 4){<?php foreach($res['code_4'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 5){<?php foreach($res['code_5'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 6){<?php foreach($res['code_6'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 7){<?php foreach($res['code_7'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 8){<?php foreach($res['code_8'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  else if(sch_kind == 9){<?php foreach($res['code_9'] as $key => $item) { echo "subject[".$key."] = '". $item->code_name ."';"; }?>}
  
  array_data[0] = ['구분', '상담건수', { role: 'annotation'}, {role:'style'}];
	var index = 1;
	for (var item in data.data_tot[0]) {
		if (item == 'tot') break;
		if (sch_kind == 8) { //상담유형
			var str_value = data.data_tot[0][item].split('(');
			value = Number(str_value[0]);
		}
		else {
			value = Number(data.data_tot[0][item]);
		}
		array_data[index] = [subject[index - 1], value, value, '#3366CC'];

		img_chart(subject[index - 1], index, data);
		index++;
	}
	var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + txt_sel_col + '] 전체 Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 100,
			top: 100,
			bottom: 200,
			width: '100%',
			height: '400',
		},
		bar: {
			groupWidth: "30%",
		},
		fontSize: 12,
		axisTitlesPosition: 'in',
		legend: {
			position: 'none'
		},
	};

	var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'));
	var chart_div = document.getElementById('chart_div0');

	google.visualization.events.addListener(chart, 'ready', function() {
		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});

	chart.draw(chart_data, options);

	donut_chart(1, array_data[1][0], data);

	/*막대그래프, 셀렉트 이벤트핸들러 시작*/
	google.visualization.events.addListener(chart, 'select', selectHandler);

	function selectHandler() {
		var selection = chart.getSelection();
		var sel_row = '';
		for (var i = 0; i < selection.length; i++) {
			var item = selection[i];
			sel_row = item.row + 1;
		}

		if (sel_row != '') {
			var sel_kind = array_data[sel_row][0];
			donut_chart(sel_row, sel_kind, data);
		}
	}
	/*막대그래프, 셀렉트 이벤트핸들러 끝*/
}
/*막대그래프 만들기 끝*/

/*도넛그래프 만드기 시작*/
function donut_chart(sel_row, sel_kind, data) {
	var txt_sel_col = $('#txt_sch_kind').val();
	var str_row = 'ck' + String(sel_row);
	var oper, asso = '';
	var value = 0;
	var str_value = '';
	var index = 1;
	var array_data = new Array();

	array_data[0] = ['상담자', '상담건수'];

	for (var i = 0; i < data.tot_cnt; i++) {
		asso = data.data[i].subject;
		str_value = data.data[i][str_row].split('(');
		value = Number(str_value[0]);
		array_data[index] = [asso, value];
		index++;
	}

	var chart_data = google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '[' + sel_kind + '] 상세 Chart',
		width: 1620,
		height: 700,
		chartArea: {
			left: 100,
			right: 100,
			top: 100,
			bottom: 100,
			width: '100%',
			height: '400',
		},
		legend: {
			position: 'right',
			maxLines: 3,
		},
		tooltip: {
			ignoreBounds: true,
		},
		fontSize: 12,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart = new google.visualization.PieChart(document.getElementById('donut_chart'));
	chart.draw(chart_data, options);
}

function img_chart(sel_kind, sel_row, data) {
	var txt_sel_col = $('#txt_sch_kind').val();
	var str_row = 'ck' + String(sel_row);
	var oper, asso = '';
	var value = 0;
	var str_value = '';
	var index = 1;
	var array_data = new Array();

	array_data[0] = ['상담자', '상담건수'];
	for (var i = 0; i < data.tot_cnt; i++) {
		asso = data.data[i].subject;
		str_value = data.data[i][str_row].split('(');
		value = Number(str_value[0]);
		array_data[index] = [asso, value];
		index++;
	}
	var chart_data = google.visualization.arrayToDataTable(array_data);
	var options = {
		title: '[' + txt_sel_col + '] ' + sel_kind + ' 상세 Chart',
		width: 1620,
		height: 500,
		chartArea: {
			left: 50,
			right: 50,
			top: 50,
			bottom: 100,
			width: '90%',
			height: '400',
		},
		is3D: true,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart_div = document.getElementById('chart_div' + sel_row);
	var chart = new google.visualization.PieChart(chart_div);

	google.visualization.events.addListener(chart, 'ready', function() {
		chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
	});

	chart.draw(chart_data, options);
}
/*도넛그래프 만드기 끝*/

</script>

<?php
include_once './inc/inc_menu.php';
?>


<!-- //lnb 메뉴 area -->
<div id="contents_body">
	<div id="cont_head">
		<h2 class="h2_tit">사용자상담 통계</h2>
		<span class="location"> 홈 > 사용자상담 통계 > <em>통계</em>
		</span>
	</div>
<?php
include_once './inc/inc_statistic_info.php';
?>
	<ul class="tab">
		<li><a href="#" class="go_page">상담사례</a></li>
		<li><a href="#" class="go_page">기본통계</a></li>
		<li><a href="#" class="go_page">교차통계</a></li>
		<li><a href="#" class="go_page selected" style="text-decoration: none">소속별통계</a></li>
		<li><a href="#" class="go_page">월별통계</a></li>
		<li><a href="#" class="go_page">시계열통계</a></li>
	</ul>
	<div class="cont_area" style="margin-top: 0">

		<form name="frmSearch" id="frmSearch" method="post">
			<input type="hidden" name="not_seq" value="0"></input>
			<input type="hidden" id="sch_kind" name="sch_kind" value="0"></input><!--통계유형-->

			<div class="con_text">
				<label for="search_date_begin" class="l_title">상담일</label>
				<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" readonly="readonly" style="width: 90px; margin-left: -20px">
				~
				<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" readonly="readonly" style="width: 90px">
				<label for="sel_year" class="l_title" style="margin-left: 10px;">연월별</label>
				<select id="sel_year" name="sel_year" style="margin-left: -20px;">
					<option value="">연도</option>
				</select>
				<select id="sel_month" name="sel_month">
					<option value="">전체</option>
					<option value="01">1월</option>
					<option value="02">2월</option>
					<option value="03">3월</option>
					<option value="04">4월</option>
					<option value="05">5월</option>
					<option value="06">6월</option>
					<option value="07">7월</option>
					<option value="08">8월</option>
					<option value="09">9월</option>
					<option value="10">10월</option>
					<option value="11">11월</option>
					<option value="12">12월</option>
				</select>
				<label for="sel_year" class="l_title" style="margin-left: 10px;">기간별</label>
				<button type="button" id="btnToday" class="buttonS bGray" style="margin-left: -20px;">오늘</button>
				<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
				<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
				<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button>
				<br>
				<span class="divice"></span>
				<div>
					<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속</button>
					<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
					<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
					<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input>
					<!-- 초기화버튼 클릭시 세팅용 초기데이터 -->
					<input type="hidden" name="asso_code_name"></input>
					<span id="asso_code_name" style="margin-left: 27px;"></span>
				</div>
				<span class="divice"></span>
				<div>
					<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
					<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
					<input type="hidden" name="s_code" id="s_code" value=""></input>
					<input type="hidden" name="s_code_name"></input>
					<span id="s_code_name" style="margin-left: 5px;"></span>
				</div>
				<span class="divice"></span>
				<div>
					<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
					<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
					<input type="hidden" name="csl_proc_rst" id="csl_proc_rst" value=""></input>
					<input type="hidden" name="csl_proc_rst_name"></input>
					<span id="csl_proc_rst_name" style="margin-left: 5px;"></span>
				</div>
				<span class="divice"></span>
				<div class="stab_button">
					<label for="sch_kind" class="stab_title">통계 유형</label>
					<input type="hidden" id="txt_sch_kind" name="txt_sch_kind" value="상담방법">
					<button type="button" class="stab_btn active">상담방법</button>
<!-- 					<button type="button" class="stab_btn">소속</button> -->
					<button type="button" class="stab_btn">소재지</button>
					<button type="button" class="stab_btn">근로자수</button>
					<button type="button" class="stab_btn">업종</button>
					<button type="button" class="stab_btn">운영기간</button>
					<button type="button" class="stab_btn">근로계약서</button>
					<button type="button" class="stab_btn">4대보험</button>
					<button type="button" class="stab_btn">취업규칙</button>
					<button type="button" class="stab_btn">상담유형</button>
					<button type="button" class="stab_btn">처리결과</button>
				</div>
				<div id="infoLabel" class="floatL marginT10" style="color: #f37000; display: none">
					* 상담유형은 중복선택이 가능하여 데이터가 중복될 수 있습니다. 총계표기 : 중복데이터(실데이터)
				</div>
				<div class="textR marginT10">
					<button type="button" id="btnSubmit" class="buttonS bBlack">
						<span class="icon_search"></span> 검색
					</button>
				</div>
			</div>
		</form>
		<!-- //컨텐츠 -->

		<!-- list -->
		<div class="list_select" style="margin-bottom: 10px;">
			<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
			<button type="button" id="printlist" class="buttonM bGray floatR">표 인쇄</button>
			<button type="button" id="view_chart" class="buttonM bGray floatR">통계차트 보기</button>
		</div>
		
		<div style="overflow: auto;">
			<button type="button" id="downchart" class="buttonS bBlack" style="display: none">차트다운로드</button>
			<button type="button" id="printchart" class="buttonS bBlack" style="display: none">차트인쇄</button>
			<div id="column_chart" class="chart" style="width: 100%; height: 700px; display: none"></div>
			<div id="donut_chart" class="chart" style="width: 100%; height: 700px; display: none"></div>
			<div id="list" style="width: 100%;"></div>
		</div>

		<div id="chart_div" style="display: none"></div>

		<a href="#" id="go_to_top" title="Go to top"></a>

	</div>
	<!-- //컨텐츠 -->
</div>
<!-- //  contents_body  area -->



<?php
include_once './inc/inc_footer.php';
?>
