<?php
include_once './inc/inc_header.php';


// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
$is_gu_officer = 0;
if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
	$is_gu_officer = 1;
}


?>


<style type="text/css">
/* 가독성을 높이기 위해 추가 */
textarea {
	line-height: 1.4em;
	resize: vertical;
}
/* 태그 비활성화 클래스 */
.disabled {
	background-color:#eee;
}
</style>

<script type="text/javascript" src="./js/jquery.base64encode.js"></script>
<script>

// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
var is_gu_officer = "<?php echo $is_gu_officer;?>";

//[권한] 체크 - 통계 상담사례>상담보기, 엑셀 다운로드 링크를 타고 접근하는 경우의 처리
var is_auth = "<?php echo $res['is_auth']; ?>";
if(is_auth == 0) {
	alert(CFG_MSG[CFG_LOCALE]['info_csl_22']);
	history.back();
}

// 등록/수정 가능 기간 - 추가 2017.01.31 dylan
var editable_day = "<?php echo CFG_COUNSEL_EDITABLE_DAY;?>";
var is_editable_period = true;



var kind = "<?php echo $res['kind'] == 'menu' ? 'add' : $res['kind']; ?>";
if(!kind) kind = 'add';

// 자신의 글인지 여부
var is_owner = "<?php echo $res['is_owner']; ?>";

// 수정/삭제 가능 여부 - 서울시 계정만 해당됨(옴부즈만 상대)
var is_editable = "<?php echo $res['is_editable']; ?>";

// 인쇄가능 여부 - 권익센터,자치구센터 소속자의 작성글은 해당 소속자는 인쇄가능하다.
var is_printable = "<?php echo $res['is_printable']; ?>";

// 공지 팝업 게시물 seq
var popup_noti_seq = "<?php echo $res['popup_seq'];?>";



$(document).ready(function(){
	// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
	if(is_gu_officer == 1) {
		$('.cssAdd').prop('disabled', true);
	}

	var today = new Date();

	// 주당근로시간 입력박스 - 숫자만 입력되게 제한처리
	$('#ave_pay_month').currencify();
	
	// 년도가 바뀐 경우 - 수정가능일 이전만 이전년도 상담입력 가능, 이후는 당해년도만 입력 가능하게 수정
	// - (주) 수정 가능일이 한달이 넘을 경우 아래 코드 수정해야 한다.
	if(today.getMonth() >= 0) { // 1월
		var op = {date:'#csl_date', option:{yearRange: 'c:c'}}
		// 1월 이고 editable_day 경과 전인 경우
		//if(today.getMonth() == 0 && today.getDate() <= editable_day) {
		//	op = {date:'#csl_date', option:{yearRange: 'c-1:c'}}
		//}
		setup_datepicker(op);
	}

	// 등록
	if(kind == 'add') {
		// 상담일 today 세팅
		$('#csl_date').val(get_today());

		// 거주지 주소 최초 선택 처리
		$('#live_addr option').each(function(i,o){
			if($(this).text() == '무응답') {
				$('#live_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});
		// 소재지 주소 최초 선택 처리
		$('#comp_addr option').each(function(i,o){
			if($(this).text() == '무응답') {
				$('#comp_addr option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});

		// 삭제 버튼 숨김
		$('.cssDel').hide();
		
	}
	// edit
	else {
		// 거주지 주소
		if($('#live_addr option:selected').text() == '기타') {
			$('#live_addr_etc').css('display', 'inline');
		}
		// 소재지 주소
		if($('#comp_addr option:selected').text() == '기타') {
			$('#comp_addr_etc').css('display', 'inline');
		}

		// 추가
		// - 당해년도 이전 상담은 수정시 통계가 수정전과 바뀌는 문제로 수정못하도록 요청에 의해 수정, 2017.01.18 dylan
		// - 년도가 바뀐 경우, 수정 가능일이 지나면 달력 선택을 막는다. 
		// - (주) 수정 가능일이 한달이 넘을 경우 아래 코드 수정해야 한다.
		// 추가 2019.02.07
		// - 관리자계정 외 전년도 말일까지 상담건은 수정기간(1/20) 이후 모두 막는다.
		// - 체크일을 상담일이 아닌 등록일 기준으로 변경(상담일은 등록일과 다르게 지정할 수 있기 때문)
		// var csl_date = "<?php //echo $edit['csl_date'];?>";
		var reg_date = "<?php echo $edit['reg_date'];?>";
		if(new Date(reg_date).getFullYear() < today.getFullYear()) { 
			// 1월 이고 editable_day 경과 또는 1월 이후
			if((today.getMonth() == 0 && today.getDate() > editable_day) || today.getMonth() > 0) {
				is_editable_period = false;
			}
		}
		//월평균임금 적용
		$('#ave_pay_month').focus();
		$('#ave_pay_month').blur();// 콤마적용을 위한 처리
		$('#ave_pay_month').currencify({execShowHangul:true});//한글통화 표기수행
			
		// 주당근로시간 적용
		var work_time_week = $('#work_time_week').val();
		if(work_time_week) {
			applyWTWDataToUI(work_time_week);
		}
		
		// 관리자 외 자신의 글 아니면 등록,삭제 버튼 숨김
		// 추가 - 관리자외 모든계정은 매년 1월 20일까지만 수정가능하도록 요청에 의해 수정, 최진혁. 2019.02.07 dylan
		if(is_master != 1 && (is_editable_period == false || (is_owner == 0 && is_editable == 0))) {
			$('.cssAdd').prop('disabled', true);
			$('.cssDel').prop('disabled', true);
		}
				
		// 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우는 허용
		if(is_printable == 1) {
			$('.cssPrint').prop('disabled', false);
		}
	}// end of edit

	// 인쇄 버튼
	$('.cssPrint').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		if(kind == 'edit') {
			open_popup_center_dual('/?c=counsel&m=counsel_print&seq='+ $('#seq').val(), '인쇄', 1060, 700);
		}
	});

	// 직종 - 선택된 값이 없으면 무응답을 checked한다.
	if(!$("input[name=work_kind]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="work_kind"]'));
		$('input:radio[name="work_kind"]:nth('+ index +')').attr('checked', true);
	}

	// 회사 업종 - 선택된 값이 없으면 무응답을 checked한다.
	if(!$("input[name=comp_kind]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="comp_kind"]'));
		$('input:radio[name="comp_kind"]:nth('+ index +')').attr('checked', true);
	}

	// 고용형태 - 선택된 값이 없으면 무응답을 checked한다.
	if(!$("input[name=emp_kind]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_kind"]'));
		$('input:radio[name="emp_kind"]:nth('+ index +')').attr('checked', true);
	}

	// 근로자수 - 선택된 값이 없으면 무응답을 checked한다.
	if(!$("input[name=emp_cnt]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_cnt"]'));
		$('input:radio[name="emp_cnt"]:nth('+ index +')').attr('checked', true);
	}

	// 근로계약서 작성 여부 - 선택된 값이 없으면 무응답을 checked한다.
	if(!$("input[name=emp_paper_yn]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_paper_yn"]'));
		$('input:radio[name="emp_paper_yn"]:nth('+ index +')').attr('checked', true);
	}

	// 4대보험가입 여부 - 선택된 값이 없으면 무응답을 checked한다.
	if(!$("input[name=emp_insured_yn]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_insured_yn"]'));
		$('input:radio[name="emp_insured_yn"]:nth('+ index +')').attr('checked', true);
	}

	// 상담내역공유 - 선택된 값이 없으면 '공유안함' radio를 checked한다.
	if(!$("input[name=csl_share_yn]").is(":checked")) {
		$('input:radio[name="csl_share_yn"]').each(function(){
			if($(this).next().text().indexOf('공유안함') != -1) {
				$(this).attr('checked', true);
			}
		});
	}

	// 전송버튼 클릭시 form onSubmit 이벤트 발생처리
	$('.cssAdd').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
			$('form').submit();
		}
	});
	
	// 삭제버튼 클릭
	$('.cssDel').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
			
			var url = '/?c=counsel&m=del';
			var rsc = {seq: $('#seq').val()};
			var fn_succes = function(data) {
				if(data.rst == 'succ') {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
					check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
				}
				else {
					if(is_local) objectPrint(data);
					alert(data.msg);
				}
			};
			var fn_error = function(data) {
				if(is_local) objectPrint(data);
			
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			};
			// request
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});

	// 추가: 2019.06.18
	// 주당 근로시간 : 일단위 값 선택시 십단위 select 활성화
	$('#work_time_week_03').change(function(){
		if($(this).val() != '' && $(this).val() > 0) {
			$('#work_time_week_02').removeClass('disabled').prop('disabled', false);
			$('#work_time_week_02_info').removeClass('disabled');
		}
		else {
			$('#work_time_week_02').addClass('disabled').prop('disabled', true);
			$('#work_time_week_02_info').addClass('disabled');
		}
	});
	// 주당 근로시간 : 십단위 값 선택시 백단위 select 활성화
	$('#work_time_week_02').change(function(){
		if($(this).val() != '' && $(this).val() > 0) {
			$('#work_time_week_01').removeClass('disabled').prop('disabled', false);
			$('#work_time_week_01_info').removeClass('disabled');
		}
		else {
			$('#work_time_week_01').addClass('disabled').prop('disabled', true);
			$('#work_time_week_01_info').addClass('disabled');
		}
	});
	
	// 등록 버튼 이벤트 핸들러
	$('form').submit(function(e) {

		// 수정 - 자신의 글인지 체크
		if(kind == 'edit' && is_master != 1 && (is_owner == 0 && is_editable == 0)) {
			alert(CFG_MSG[CFG_LOCALE]['warn_board_02']);
			return false;
		}

		// validation
		// 필수체크 항목
		// - 성별,상담유형,상담내용,상담답변,처리결과 5개
		// - 상담방법, 성별, 연령대, 상담유형, 상담제목, 상담내용, 상담답변, 처리결과 - 8개 => 변경 2015.08.21
		// 상담방법
		if(!$("input[name=s_code]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_19']);
			$('#s_code_0').focus();
			return false;
		}
		// 상담이용동기
		// - 교차통계에서 사용하기 때문에 필수로 수정 2019.06.29 dylan -- 필수아니라고 해서 주석처리 2019.07.29 dylan
// 		if(!$("input[name=csl_motive_cd]").is(":checked")) {
// 			alert(CFG_MSG[CFG_LOCALE]['info_csl_23']);
// 			$('#csl_motive_cd_0').focus();
// 			return false;
// 		}
		// 성별
		if(!$("input[name=gender]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_17']);
			$('#gender_0').focus();
			return false;
		}
		// 연령대
		if(!$("input[name=ages]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_20']);
			$('#ages_0').focus();
			return false;
		}
		// 상담유형
		if(!$("input[id^=csl_kind_]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_14']);
			$('#csl_kind_0').focus();
			return false;
		}
		// 상담유형 - 3개까지 체크 가능
		var csl_kind_chk_len = $("input[id^=csl_kind_]:checked").length;
		if(csl_kind_chk_len > 3) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_16']);
			$('#csl_kind_0').focus();
			return false;
		}
		// 제목
		if($('#csl_title').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_03']);
			$('#csl_title').focus();
			return false;
		}
		// 상담내용
		if($('#csl_content').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_04']);
			$('#csl_content').focus();
			return false;
		}
		// 상담 답변
		if($('#csl_reply').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_05']);
			$('#csl_reply').focus();
			return false;
		}
		// 상담 처리결과
		if(!$("input[name=csl_proc_rst]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_06']);
			$('#csl_proc_rst_0').focus();
			return false;
		}

		var dlm = '<?php echo CFG_AUTH_CODE_DELIMITER;?>';
		// 고용형태,상담유형,주제어 데이터 조합
		// - 고용형태 -- 중복체크 가능하게 요청 뒤 원복 요청으로 주석처리
//		var emp_kind = '';
//		$("input[name^=emp_kind_]:checked").each(function(){
//			if(emp_kind != '') emp_kind += dlm;
//			emp_kind += $(this).val();
//		});
//		if(emp_kind.indexOf(dlm) == -1) {
//			emp_kind = dlm;
//		}
		// - 상담유형
		// var csl_kind = '', 
		// 	csl_kind_cnt = 0;
		// $("input[id^=csl_kind_]:checked").each(function(){
		// 	if(csl_kind != '') csl_kind += dlm;
		// 	csl_kind += $(this).val();
		// 	csl_kind_cnt++;
		// });
		// $('input[name="csl_kind"]').val(csl_kind);

		// -- 상담유형을 3개의 구조로 맞춘다. 이유는 통계쪽에서 엑셀다운로드시 3개의 컬럼으로 나눠 보여달라는 요청 때문임. 
		// -- 각 컬럼은 특정값에 고정되지 않기 때문에 3개를 선택 안한 경우 임의로 3개의 구조로 만든다.
//		if(csl_kind_cnt < 3) {
//			for(var i=0; i<3-csl_kind_cnt; i++) {
//				if(csl_kind != '') csl_kind += dlm;
//			}
//		}
		// - 주제어
		// var csl_keyword = '';
		// $("input[name^=csl_kewyord_]:checked").each(function(){
		// 	if(csl_keyword != '') csl_keyword += dlm;
		// 	csl_keyword += $(this).val();
		// });

		// 기타 input box값 체크 - 기타가 아닌 경우 input box 값 없앰
		// - 상담방법
		if($('input:radio[name=s_code]:checked').next().text() != '기타') {
			$('input[name=s_code_etc]').val('');
		}
		// - 고용형태
		if($('input:radio[name=emp_kind]:checked').next().text() != '기타') {
			$('input[name=emp_kind_etc]').val('');
		}
		// - 처리결과
		if($('input:radio[name=csl_proc_rst]:checked').next().text() != '기타') {
			$('input[name=csl_proc_rst_etc]').val('');
		}

		// 월평균임금 - 숫자,콤마 외 입력 제한 처리
// 		tmp_ave_pay_month = $('#ave_pay_month').val().replace(/ /g,"");
// 		if(tmp_ave_pay_month !="" && !is_money(tmp_ave_pay_month) ) {
// 			alert(CFG_MSG[CFG_LOCALE]['info_csl_21']);
// 			$('#ave_pay_month').focus();
// 			return false;
// 		}
		// 콤마제거
		$('#ave_pay_month').val($('#ave_pay_month').val().replace(/,/g, "").trim());

		// 주당 근로시간 - 숫자,콤마 외 입력 제한 처리
		tmp_work_time_week = $('#work_time_week_01').val() + $('#work_time_week_02').val() + $('#work_time_week_03').val() +'.'+ $('#work_time_week_04').val() + $('#work_time_week_05').val();
// 		if($('#work_time_week_01').val() + $('#work_time_week_02').val() + $('#work_time_week_03').val() < 1) {
// 			alert(CFG_MSG[CFG_LOCALE]['info_csl_24']);
// 			return false;
// 		}
		tmp_work_time_week = tmp_work_time_week.substr(0, 1) == 0 ? tmp_work_time_week.substr(1, tmp_work_time_week.length-1) : tmp_work_time_week;
		$('#work_time_week').val(tmp_work_time_week == '.00' ? '' : tmp_work_time_week);

		//
		//
		// 첨부파일의 보안문제를 이유로 첨부파일 기능 제거요청으로 재작업 - 20160620 최진혁
		//
		// 
		// 전송
		var url = '/?c=counsel&m=processing';
		var rsc = $('#frmForm').serialize();
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
				check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
				if(is_local) objectPrint(data);
			}
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
		
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);

		e.preventDefault();
	});




	// list 버튼 이벤트 핸들러
	$('.cssList').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), 'list', '');
	});

	//내담자정보->관련상담 버튼 클릭 핸들러
	$('#btnRelViewCounsel').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		// 내담자 이름으로만 검색, 전번은 제외 요청에 따라 주석처리 2015.08.06 delee
//		if($('#csl_name').val() == '') || $('#csl_tel01').val() == '' || $('#csl_tel02').val() == '' || $('#csl_tel03').val() == '') {
			if($('#csl_name').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_csl_11']);
				$('#csl_name').focus();
				return false;
			}
//			else {
//				alert(CFG_MSG[CFG_LOCALE]['info_csl_12']);
//			}
//			if($('#csl_tel01').val() == '') { $('#csl_tel01').focus(); return false; }
//			if($('#csl_tel02').val() == '') { $('#csl_tel02').focus(); return false; }
//			if($('#csl_tel03').val() == '') { $('#csl_tel03').focus(); return false; }
//		}

		$('#loading').show();
		var closedCallback = function(data) {
			if(data) {
// 				console.log(data);
				// 추가, 2017.02.01 dylan
				// - 원글key가 있는 상담인 경우 작성하는 상담의 원글key를 최초 원글key로 세팅한다.
				if(data.csl_ref_seq && data.csl_ref_seq > 0) {
					$('#csl_ref_seq').val(data.csl_ref_seq);
				}
				// "관련상담"이 "관련상담"으로 작성한 상담이 아닌 경우
				else {
					if(data.seq) $('#csl_ref_seq').val(data.seq);
				}
				// if(data.title) $('#csl_title').val(data.title);
				// if(data.content) $('#csl_content').val(data.content);
				// if(data.reply) $('#csl_reply').val(data.reply);
				//
				if(is_valid(data.csl_name)) $('#csl_name').val(data.csl_name);
// 				console.log(data.csl_tel, typeof data.csl_tel, data.csl_tel, is_valid(data.csl_tel));
				if(is_valid(data.csl_tel)) {
					var arr_csl_tel = data.csl_tel.split('-');
					$('#csl_tel01').val(arr_csl_tel[0]);
					$('#csl_tel02').val(arr_csl_tel[1]);
					$('#csl_tel03').val(arr_csl_tel[2]);
				}
				if(is_valid(data.gender)) $('input:radio[name=gender]:input[value="'+ Base64.encode(data.gender) +'"]').prop('checked', true);
				if(is_valid(data.ages)) $('input:radio[name=ages]:input[value="'+ Base64.encode(data.ages) +'"]').prop('checked', true);
				// 거주지
				if(is_valid(data.live_addr)) {
					$('select[name=live_addr] option[value="'+ Base64.encode(data.live_addr) +'"]').prop('selected', true);
					// 기타인 경우
					if($('select[name=live_addr] option:selected').text() == '기타') {
						$('input[name=live_addr_etc]').show();
						$('input[name=live_addr_etc]').val(data.live_addr_etc);
					}
				}
				// 직종
				if(is_valid(data.work_kind)) {
					$('input:radio[name=work_kind]:input[value="'+ Base64.encode(data.work_kind) +'"]').prop('checked', true);
					// 기타입력 항목
					$('input[name=work_kind_etc]').val(data.work_kind_etc);
				}
				// 회사 업종 
				if(is_valid(data.comp_kind)) {
					$('input:radio[name=comp_kind]:input[value="'+ Base64.encode(data.comp_kind) +'"]').prop('checked', true);// 기타입력 항목
					$('input[name=comp_kind_etc]').val(data.comp_kind_etc);
				}
				// 소재지
				if(is_valid(data.comp_addr)) {
					$('select[name=comp_addr] option[value="'+ Base64.encode(data.comp_addr) +'"]').prop('selected', true);
					// 기타인 경우
					if($('select[name=comp_addr] option:selected').text() == '기타') {
						$('input[name=comp_addr_etc]').show();
						$('input[name=comp_addr_etc]').val(data.comp_addr_etc);
					}
				}
				// 고용형태
				if(is_valid(data.emp_kind)) {
					$('input:radio[name=emp_kind]:input[value="'+ Base64.encode(data.emp_kind) +'"]').prop('checked', true);
					// 기타인 경우
					if($('input:radio[name=emp_kind]:checked').next().text() == '기타') {
						$('#emp_kind_etc_holder').show();
						$('input[name=emp_kind_etc]').val(data.emp_kind_etc);
					}
				}
				if(is_valid(data.emp_cnt)) $('input:radio[name=emp_cnt]:input[value="'+ Base64.encode(data.emp_cnt) +'"]').prop('checked', true);
				if(is_valid(data.emp_paper_yn)) $('input:radio[name=emp_paper_yn]:input[value="'+ Base64.encode(data.emp_paper_yn) +'"]').prop('checked', true);
				if(is_valid(data.emp_insured_yn)) $('input:radio[name=emp_insured_yn]:input[value="'+ Base64.encode(data.emp_insured_yn) +'"]').prop('checked', true);
				if(is_valid(data.ave_pay_month)) { 
					$('input[name=ave_pay_month]').val(data.ave_pay_month);
					$('#ave_pay_month').focus();
					$('#ave_pay_month').blur();// 콤마적용을 위한 처리
					$('#ave_pay_month').currencify({execShowHangul:true});//한글통화 표기수행
				}
				if(is_valid(data.work_time_week)) {
					$('input[name=work_time_week]').val(data.work_time_week);
					applyWTWDataToUI(data.work_time_week);
				}
				$('#csl_name').focus();
			}
		};
		var data = encodeURI('&nm='+ $('#csl_name').val());// + '&tel='+ $('#csl_tel01').val() + '-' + $('#csl_tel02').val() + '-' + $('#csl_tel03').val());
		var url = '/?c=counsel&m=counsel_rel_view&seq='+ $('input[name=seq]').val() + data;
		gLayerId = openLayerModalPopup(url, 800, 635, '', '', '', closedCallback, 1, true);
	});

	// 상담내용->불러오기 버튼 이벤트 핸들러
	$('#btnGetExistCounsel').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		$('#loading').show();
		var closedCallback = function(data) {
			if(data) {
				if(data.title) $('#csl_title').val(data.title);
				if(data.content) $('#csl_content').val(data.content);
				if(data.reply) $('#csl_reply').val(data.reply);
			}
			$('#csl_title').focus();
		};
		var url = '/?c=counsel&m=counsel_list_popup_view&seq='+ $('input[name=seq]').val();
		gLayerId = openLayerModalPopup(url, 800, 710, '', '', '', closedCallback, 1, true);

		// jquery tooltip - 태그에 title 에 지정된 텍스트가 대상임
		$('#'+gLayerId).tooltip({
			track: true
			,position: {
				my: "center bottom-10",
				at: "center top",
				using: function( position, feedback ) {					
					// 툴팁의 top, bottom이 화면에 가려질 경우 처리
					if(position.top < 0 || $('#list').offset().top + position.top + $(this).height() > window.screen.height) {
						position.top = 10;
						$(this).css({
							width: 1000
							,minWidth: 1000
						});
					}
					$(this).css(position).css({
						background: '#fff'
						,zIndex: 999999
					});//.css('box-shadow', '0 0 3px black');
				}
			}
	  });
	});

	// 거주지/소재지 변경 이벤트 핸들러
	$('#live_addr,#comp_addr').change(function(){
		var selopTxt;
		var isLive = $(this).attr('id') == 'live_addr';
		if(isLive) {
			selopTxt = $("#live_addr option:selected").text();
		}
		else {
			selopTxt = $("#comp_addr option:selected").text();
		}
		//
		if(selopTxt == '경기도' || selopTxt == '기타') {
			if(isLive) {
				$('#live_addr_etc').css('display', 'inline').focus();
			}
			else {
				$('#comp_addr_etc').css('display', 'inline').focus();
			}
		}
		else {
			if(isLive) {
				$('#live_addr_etc').css('display', 'none');
				$('#live_addr_etc').val('');
			}
			else {
				$('#comp_addr_etc').css('display', 'none');
				$('#comp_addr_etc').val('');
			}
		}
	});

	// 상담방법 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_s_code = "<?php echo base64_encode($edit['s_code']);?>";
	$("input:radio[name=s_code]").each(function(i){
		if($(this).val() == csl_s_code) {
			if($(this).next().text() == '기타') {
				$('#s_code_etc').css('display', 'inline');
				var s_code_etc = "<?php echo $edit['s_code_etc'];?>";
				$('#s_code_etc').val( s_code_etc );
			}
			else {
				$('#s_code_etc').val('');
			}
		}
	});	
	// 상담방법 - 기타 선택한 경우, input box 활성화
	$('input:radio[name=s_code]').click(function(e){
		var display = 'none';
// 		var len = $('#s_code_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).next().text() == '기타') display = 'inline';
// 		if($(this).attr('id') == 's_code_'+len ) display = 'inline';
		$('#s_code_etc_holder').css('display', display);
// 		if($(this).attr('id') == 's_code_'+len ) $('#s_code_etc').focus();
	});
	if($('#s_code_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#s_code_etc_holder').css('display', 'none');
	}

	// 상담이용동기 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_motive_cd = "<?php echo base64_encode($edit['csl_motive_cd']);?>";
	$("input:radio[name=csl_motive_cd]").each(function(i){
		if($(this).val() == csl_motive_cd) {
			if($(this).next().text() == '기타') {
				$('#csl_motive_etc').css('display', 'inline');
				var csl_motive_etc = "<?php echo $edit['csl_motive_etc'];?>";
				$('#csl_motive_etc').val( csl_motive_etc );
			}
			else {
				$('#csl_motive_etc').val('');
			}
		}
	});	
	// 상담이용동기 - 기타 선택한 경우, input box 활성화
	$('input:radio[name=csl_motive_cd]').click(function(e){
		var display = 'none';
// 		var len = $('#csl_motive_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).next().text() == '기타') display = 'inline';
// 		if($(this).attr('id') == 'csl_motive_cd_'+len ) display = 'inline';
		$('#csl_motive_cd_etc_holder').css('display', display);
// 		if($(this).attr('id') == 'csl_motive_cd_'+len ) $('#csl_motive_etc').focus();
	});
	if($('#csl_motive_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#csl_motive_cd_etc_holder').css('display', 'none');
	}
	
	// RADIO 선택취소 버튼
	$('.cssInitRadio').click(function(e){
		e.preventDefault();
		var t = $(this).attr('target');
		if(t) {
			$('input[name='+ t +']').prop('checked', false);
			$('#'+ t +'_etc_holder').css('display', 'none');
		}
	});

	// 고용형태 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var emp_kind = "<?php echo base64_encode($edit['emp_kind']);?>";
	$("input:radio[name=emp_kind]").each(function(i){
		if($(this).val() == emp_kind) {
			if($(this).next().text() == '기타') {
				$('#emp_kind_etc').css('display', 'inline');
				var emp_kind_etc = "<?php echo $edit['emp_kind_etc'];?>";
				$('#emp_kind_etc').val( emp_kind_etc );
			}
			else {
				$('#emp_kind_etc').val('');
			}
		}
	});
	// 고용형태 - 기타 선택한 경우, input box 활성화
	$('input:radio[name=emp_kind]').click(function(e){
		var display = 'none';
// 		var len = $('#emp_kind_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).next().text() == '기타') display = 'inline';
// 		if($(this).attr('id') == 'emp_kind_'+len ) display = 'inline';
		$('#emp_kind_etc_holder').css('display', display);
// 		if($(this).attr('id') == 'emp_kind_'+len ) $('#emp_kind_etc').focus();
	});
	if($('#emp_kind_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#emp_kind_etc_holder').css('display', 'none');
	}

	// 상담결과 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_proc_rst = "<?php echo base64_encode($edit['csl_proc_rst']);?>";
	$("input:radio[name=csl_proc_rst]").each(function(i){
		if($(this).val() == csl_proc_rst) {
			if($(this).next().text() == '기타') {
				$('#csl_proc_rst_etc').css('display', 'inline');
				var csl_proc_rst_etc = "<?php echo $edit['csl_proc_rst_etc'];?>";
				$('#csl_proc_rst_etc').val( csl_proc_rst_etc );
			}
			else {
				$('#csl_proc_rst_etc').val('');
			}
		}
	});
	// 상담결과 - 기타 선택한 경우, input box 활성화
	$('input[name=csl_proc_rst]').click(function(e){
		// console.log( $(this).next().text() );

		var display = 'none';
// 		var len = $('#csl_proc_rst_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).next().text() == '기타') display = 'inline';
// 		if($(this).attr('id') == 'csl_proc_rst_'+len ) display = 'inline';
		$('#csl_proc_rst_etc_holder').css('display', display);
// 		if($(this).attr('id') == 'csl_proc_rst_'+len ) $('#csl_proc_rst_etc').focus();
	});
	if($('#csl_proc_rst_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#csl_proc_rst_etc_holder').css('display', 'none');
	}


	// focus
	$('#csl_name').focus()
	.keypress(function(e) {
		if (e.keyCode == 13) {
			$("#btnRelViewCounsel").click();
		}
	});


	// focusOut 용
	$(window).scroll(function() {
		$('#focusOuter').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 0});    
	});

	// 코드 설명글(상위코드) 적용 - 추가 2019.07.08 dylan -- 사용안한다고해서 주석처리, 2019.07.29 dylan
	//var arrCodeDesc = eval('<?php //echo json_encode($res['mcode_desc']);?>');
// 	$('table th').each(function(i,o){
// 		for(var i=0; i<arrCodeDesc.length; i++){
// 			if($(o).text().indexOf(arrCodeDesc[i].code_name) != -1) {
// 				$(o).html('<a class="openCodeDescPopup" style="cursor:help;"><textarea style="display:none;">'+ arrCodeDesc[i].code_desc +'</textarea>'+ $(o).html() +'&nbsp;<img src="../images/common/icon_que2.png" alt="'+ arrCodeDesc[i].code_name +'이란" class="imgM"></a>');
// 				break;
// 			}
// 		}
// 	});
	// 코드 설명글(하위코드) 적용
	var arrCodeDesc = eval('<?php echo json_encode($res['scode_desc']);?>');
	$('table td label').each(function(i,o){
		for(var i=0; i<arrCodeDesc.length; i++){
			if($(o).prev()[0].tagName == 'INPUT' && $(o).prev().val() == $.base64.encode(arrCodeDesc[i].s_code)) {
// 				$(o).append('&nbsp;<a class="openCodeDescPopup" style="cursor:help;text-decoration:underline;"><img src="../images/common/icon_que2.png" alt="'+ arrCodeDesc[i].code_name +'(이)란" class="imgM" align="bottom"><textarea style="display:none;">'+ arrCodeDesc[i].code_desc +'</textarea></a>');
				$(o).addClass('openCodeDescPopup').text($(o).text());
				$(o).append('<textarea style="display:none;">'+ arrCodeDesc[i].code_desc +'</textarea>');
				break;
			}
		}
	});
	// help 내용 적용시 테이블의 스타일이 사라지는 문제로 테이블 숨긴 후 다 적용한 후 보여주도록 한다.
	$('table th, td').css('font-size', '12px');
	$('table').show();
});

/**
 * 근로시간 입력UI 컨트롤 함수
 */
function applyWTWDataToUI(wtw) {
	wtw = wtw.indexOf('.') != -1 ? wtw.split('.') : wtw;
	var arr_wtw = wtw[0].split('').reverse(),
		len = arr_wtw.length < 3 ? 3 : arr_wtw.length,
		idx = 1;
	for(var i=len-1; i>=0; i--) {
		var d = arr_wtw[i] ? arr_wtw[i] : '';
		$('#work_time_week_0'+ (idx++) +' option[value="'+ d +'"]').prop('selected', true);
	}
	// 소숫점이하 2자리 적용
	if(wtw[1]) {
		var arr_wtw2 = wtw[1].split('');
		$('#work_time_week_04 option[value="'+ arr_wtw2[0] +'"]').prop('selected', true);
		$('#work_time_week_05 option[value="'+ arr_wtw2[1] +'"]').prop('selected', true);
	}
}

/**
  * 입력받은 jquery 객체 중 각 앨리먼트 다음에 위치한 태그의 값이 "무응답"을 찾아 index 리턴
  */
function find_text_in_elms(jq) {
	var ft = typeof find_text === 'undefined' ? '무응답' : find_text;
	var rst  = 0;
	jq.each(function(i, ele){
		if($(this).next().text() == ft) {
			rst = i;
		}
	});
	return rst;
}

/**
  * 안내 팝업
  */
function open_help(index) {
	var url = '/?c=counsel&m=counsel_help&url=./help/adm_counsel_help'+ index +'_popup_view';
	var h = 300, w = 500;
	if(index == 3) {
		h = 570;
		w = 750;
	}
	gLayerId = openLayerModalPopup(url, w, h, '', '', '', '', 1, true);
}



$(document).ready(function(e){
	
	var arr_func = [];

	// 팝업 공지
	var arr_popup_noti_seq = popup_noti_seq.split(',');
	var func = null;
	for(var i=0; i<arr_popup_noti_seq.length; i++) {
		if(arr_popup_noti_seq[i] && ! $.cookie('labors_hide_popup_'+ arr_popup_noti_seq[i])) {
			var url = '"/?c=board&m=open_notice_popup&seq='+ arr_popup_noti_seq[i] +'"';
			func = function(url) {
				openLayerModalPopup(url, 500, 500,'','','','',1,true,null,true,true);
			};
			arr_func.push('func('+url+')');
		}
	}

	arr_func.forEach(function(item, index, array){
		if(index == 0) {
			eval(item);
		}
		else {
			// 첫번째 이후 팝업 생성과 컨텐츠 적용이 순차적으로 정확히 적용되도록 딜레이를 준다.
			setTimeout(function(){eval(item);},100);
		}
	});

	//월평균임금 금액버튼 이벤트핸들러
	$('.btnAddPay').click(function(){
		addAvePayMonth( +$(this).attr('data-pay') );
	});
	var addAvePayMonth = function(data){
		var apm = $('#ave_pay_month').val() ? +$('#ave_pay_month').val().replace(/,/g, "").trim() : 0;
		var rst = apm + data;
		if(rst <= 0) return;
		$('#ave_pay_month').val(rst.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
		$('#ave_pay_month').currencify({execShowHangul:true});
	};
});

(function($){
	// 통화로 표기하는 플러그인
	// : 숫자만 입력되며 숫자 입력시 한글통화로 표기되고 focusout되면 콤마 표기함  
	$.fn.currencify = function(op){
		var setting = $.extend({
			showHangul: true
			,execShowHangul: false
		}, op);
		this.on('focus', function(){
			$(this).val( $(this).val().replace(/,/g, "").trim() );
		});
		this.on('focusout', function(){
			$(this).val( $(this).val().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );
		});
		this.on('keyup', function(){
			$(this).val( $(this).val().replace(/[^0-9]/g,"") );
			// 첫번째 숫자가 0이면 제거
			if($(this).val().substr(0, 1) == 0) {
				$(this).val( $(this).val().substr(1, $(this).val().length-1) );
			}
			// 입력된 숫자 한글로 표기
			if(setting.showHangul) {
				$('#hgCurrency').text('('+ convertHangulCurrency( $(this).val() ) +'원)');
			}
		});
		// 금액을 한글로 표기할 태그 추가
		if(setting.showHangul && $('#hgCurrency').length == 0) {
			this.parent().append('<span id="hgCurrency"></span>');
		}
		// 입력된 숫자를 한글표기로 변환하여 리턴
		var convertHangulCurrency = function(m){
			if(!m) return '';
			m = m.replace(/,/g, "").trim();
			const arr = [["","일","이","삼","사","오","육","칠","팔","구"], ["","십","백","천"], ["","만","억","조","경"]];
			var rst = '',
				len = m.split('').length; 
			for(var i=len-1; i>=0; i--){
				rst += arr[0][parseInt(m.substring(len-i-1, len-i))];
				if(parseInt(m.substring(len-i-1, len-i)) > 0) {
					rst += arr[1][i%4];
				}
				if(i%4 == 0) {
					rst += arr[2][i/4];
				}
			}
			return rst;
		};
		// 한글표기만 수행할 경우 처리
		if(setting.execShowHangul) {
			var cc = $(this).val().replace(/,/g, "").trim();
			$('#hgCurrency').text('('+ convertHangulCurrency(cc) +'원)');
		}
	};
}($)); 

function is_valid(d) {
// 	console.log('is_valid: ', d, "typeof:", typeof d);
// 	console.log(typeof d == 'undefined');
// 	console.log("d == 'null'", d == 'null');
// 	console.log("d == null", d == null);
// 	console.log("d == undefined", d == undefined);
	if(typeof d == 'undefined' || d == 'null' || d == null || !d || d == undefined || d == "undefined") {
		return false;
	}
	return true;
}
</script>



<?php
include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">상담내역입력</h2>
				<span class="location">홈 > 상담내역관리 > <em>상담내역입력</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post">
				<input type="hidden" name="kind" value="<?php echo $res['kind'] == 'menu' ? 'add' : $res['kind']; ?>"></input>
				<input type="hidden" name="csl_ref_seq" id="csl_ref_seq" value="<?php echo $edit['csl_ref_seq']; ?>"></input>
				<input type="hidden" name="seq" id="seq" value="<?php echo $edit['seq']; ?>"></input>
				<h3 class="sub_stit">
					상담경로
					<div class="marginT10 textR" style="margin-top:-18px;">
						<button type="button" class="buttonM bGray cssShowCodeDescPopup" style="background:#eaeaea;">코드설명팝업보기</button>
						<button type="button" class="buttonM bGray cssPrint">인쇄</button>
						<button type="button" class="buttonM bSteelBlue cssAdd">등록</button>
						<button type="button" class="buttonM bOrange cssDel">삭제</button>
						<!-- <button type="button" class="buttonM bGray cssBack">뒤로이동</button> -->
						<button type="button" class="buttonM bDarkGray cssList">목록</button>
					</div>
				</h3>
				<table class="tInsert" style="display:none;"> 
					<caption>
						상담경로 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>소속 <span class="fRed">*</span></th>
						<td><?php echo $edit['asso_name']; ?>
							<input type="hidden" id="asso_code" name="asso_code" value="<?php echo base64_encode($edit['asso_code']); ?>"></input>
						</td>
					</tr>
					<tr>
						<th>상담자 <span class="fRed">*</span></th>
						<td><?php echo $edit['oper_name']; ?></td>
					</tr>
					<tr>
						<th>상담일 <span class="fRed">*</span></th>
						<td><input type="text" id="csl_date" name="csl_date" class="datepicker date" value="<?php echo $edit['csl_date'];?>" readonly="readonly" style="width:80px"></td>
					</tr>
					<tr style="height:51px;">
						<th>상담방법 <span class="fRed">*</span></th>
						<td>
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['s_code'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
// 								if($item->code_name == '기타') {
// 									$etc_item['index'] = $index;
// 									$etc_item['item'] = $item;
// 									$index_etc = $index;
// 								}
// 								else {
									echo '<input type="radio" name="s_code" class="imgM" id="s_code_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['s_code'] == $item->s_code ? 'checked="checked"' : '') .'><label for="s_code_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
// 								}
								$index++;
							}
							// 기타 항목
// 							echo '<input type="radio" name="s_code" class="imgM" id="s_code_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['s_code'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="s_code_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="s_code_etc_holder"><!-- data-role-etc_index="<?php //echo $index_etc;?>">-->
								<input type="text" name="s_code_etc" id="s_code_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>	
					</tr>
					<tr style="height:51px;">
						<th>상담이용동기</th>
						<td>
							<button type="button" class="buttonS bGray cssInitRadio" target="csl_motive_cd">선택취소</button>
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['csl_motive_cd'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
// 								if($item->code_name == '기타') {
// 									$etc_item['index'] = $index;
// 									$etc_item['item'] = $item;
// 									$index_etc = $index;
// 								}
// 								else {
									echo '<input type="radio" name="csl_motive_cd" class="imgM" id="csl_motive_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['csl_motive_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_motive_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
// 								}
								$index++;
							}
							// 기타 항목
// 							echo '<input type="radio" name="csl_motive_cd" class="imgM" id="csl_motive_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['csl_motive_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="csl_motive_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="csl_motive_cd_etc_holder"><!-- data-role-etc_index="<?php //echo $index_etc;?>">-->
								<input type="text" name="csl_motive_etc" id="csl_motive_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>	
					</tr>
				</table>	
				<h3 class="sub_stit">내담자 정보</h3>
				<table class="tInsert" style="display:none;">
					<caption>
						내담자 정보 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>성명</th>
						<td><input type="text" name="csl_name" id="csl_name" class="imgM" size="10" maxlength="10" value="<?php echo $edit['csl_name'];?>">&nbsp;
							<button type="button" class="buttonS bGray marginT03" id="btnRelViewCounsel">관련상담</button>
							<span style="marginL10">* 재상담, 지속상담의 경우 성명 입력후 "관련상담" 버튼을 클릭하시면 관련상담을 찾아볼 수 있습니다.</span></td>
					</tr>
					<tr>
						<th>연락처</th>
						<td>
							<input type="text" name="csl_tel01" id="csl_tel01" class="imgM" size="4" maxlength="4" value="<?php echo $edit['csl_tel01'];?>"> - 
							<input type="text" name="csl_tel02" id="csl_tel02" class="imgM" size="4" maxlength="4" value="<?php echo $edit['csl_tel02'];?>"> - 
							<input type="text" name="csl_tel03" id="csl_tel03" class="imgM" size="4" maxlength="4" value="<?php echo $edit['csl_tel03'];?>">
						</td>
					</tr>
					<tr>
						<th>성별 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['gender'] as $item) {
							echo '<input type="radio" name="gender" class="imgM" id="gender_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['gender'] == $item->s_code ? 'checked="checked"' : '') .'><label for="gender_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>	
					<tr>
						<th>연령대 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['ages'] as $item) {
							echo '<input type="radio" name="ages" class="imgM" id="ages_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['ages'] == $item->s_code ? 'checked="checked"' : '') .'><label for="ages_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?><br>
						<input type="text" name="ages_etc" style="width:100px;" value="<?php echo $edit['ages_etc'];?>" maxlength="30" placeholder=""> 
						</td>
					</tr>
					<tr>
						<th>거주지 <span class="fRed">*</span><br>(구단위)</th>
						<td>
							<select class="styled" id="live_addr" name="live_addr">
								<?php
								$index = 0;
								foreach($res['csl_addr'] as $item) {
									echo '<option value="'. base64_encode($item->s_code) .'" '. ($edit['live_addr'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
								}
								?>
							</select>
							<input type="text" class="" id="live_addr_etc" name="live_addr_etc" style="width:350px;display:none" value="<?php echo $edit['live_addr_etc'];?>" maxlength="30" placeholder="최대 30자"> 
						</td>										
					</tr>
					<tr>
						<th>직종 <span class="fRed">*</span> <a href="javascript:open_help(1)"><img src="../images/common/icon_que.png" alt="직종이란" class="imgM"></a></th>
						<td>
						<?php
						$index = 0;
						foreach($res['work_kind'] as $item) {
							echo '<input type="radio" name="work_kind" class="imgM" id="work_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['work_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="work_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<br>추가내용 : <input name="work_kind_etc" id="work_kind_etc" style="width: 350px; display: inline;" type="text" maxlength="30" value="<?php echo $edit['work_kind_etc'];?>" placeholder="최대 30자">
						</td>	
					</tr>
				</table>
				<h3 class="sub_stit">사업장 근로조건</h3>
				<table class="tInsert" style="display:none;">
					<caption>
						사업장 근로조건 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>업종 <span class="fRed">*</span> <a href="javascript:open_help(2)"><img src="../images/common/icon_que.png" alt="업종이란" class="imgM"></a></th>
						<td>
						<?php
						$index = 0;
						foreach($res['comp_kind'] as $item) {
							echo '<input type="radio" name="comp_kind" class="imgM" id="comp_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['comp_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="comp_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<br>추가내용 : <input name="comp_kind_etc" id="comp_kind_etc" style="width: 350px; display: inline;" type="text" maxlength="30" value="<?php echo $edit['comp_kind_etc'];?>" placeholder="최대 30자">
						</td>	
					</tr>	
					<tr>
						<th>소재지 <span class="fRed">*</span><br>(구단위)</th>
						<td>
							<select class="styled" id="comp_addr" name="comp_addr">
								<?php
								$index = 0;
								foreach($res['csl_addr'] as $item) {
									echo '<option value="'. base64_encode($item->s_code) .'" '. ($edit['comp_addr'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
								}
								?>
							</select>
							<input type="text" class="" id="comp_addr_etc" name="comp_addr_etc" style="width:350px;display:none" value="<?php echo $edit['comp_addr_etc'];?>" maxlength="30" placeholder="최대 30자"> 
						</td>										
					</tr>
					<tr>
						<th>고용형태 <span class="fRed">*</span> <a href="javascript:open_help(3)"><img src="../images/common/icon_que.png" alt="고용형태란" class="imgM"></a></th>
						<td>
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['emp_kind'] as $item) {
								// 기타 항목은 항상 끝에 배치한다. -- 코드관리에서 정하도록 변경
// 								if($item->code_name == '기타') {
// 									$etc_item['index'] = $index;
// 									$etc_item['item'] = $item;
// 									$index_etc = $index;
// 								}
// 								else {
									echo '<input type="radio" name="emp_kind" class="imgM" id="emp_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_kind_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
// 								}
								$index++;
							}
							// 기타 항목
// 							echo '<input type="radio" name="emp_kind" class="imgM" id="emp_kind_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['emp_kind'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="emp_kind_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="emp_kind_etc_holder"> <!-- data-role-etc_index="<?php //echo $index_etc;?>">-->
								<input type="text" name="emp_kind_etc" id="emp_kind_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>
					</tr>
					<!-- // 사용주체 항목 주석처리 - 2015.11.23 요청에 의한 처리 delee
					<tr>
						<th>사용주체 * <a href="javascript:open_help(4)"><img src="../images/common/icon_que.png" alt="사용주체란" class="imgM"></a></th>
						<td>
						<?php
						// $index = 0;
						// foreach($res['emp_use_kind'] as $item) {
						// 	echo '<input type="radio" name="emp_use_kind" class="imgM" id="emp_use_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_use_kind'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_use_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						// }
						?>
						</td>
					</tr>
					-->
					<tr>
						<th>근로자수</th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_cnt'] as $item) {
							echo '<input type="radio" name="emp_cnt" class="imgM" id="emp_cnt_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_cnt'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_cnt_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?><br>
						<input type="text" name="emp_cnt_etc" style="width:100px;" value="<?php echo $edit['emp_cnt_etc'];?>" maxlength="30" placeholder=""> 
						</td>
					</tr>
					<tr>
						<th>근로계약서 작성</th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_paper_yn'] as $item) {
							echo '<input type="radio" name="emp_paper_yn" class="imgM" id="emp_paper_yn_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_paper_yn'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_paper_yn_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>
					<tr>
						<th>4대보험가입</th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_insured_yn'] as $item) {
							echo '<input type="radio" name="emp_insured_yn" class="imgM" id="emp_insured_yn_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['emp_insured_yn'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_insured_yn_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>
					<tr>
						<th>월평균임금</th>
						<td>
							<p style="margin-bottom:5px;">* 숫자만 입력가능합니다.</p>
							<input type="text" id="ave_pay_month" name="ave_pay_month" maxlength="30" value="<?php echo $edit['ave_pay_month'];?>">
							<span id="hgCurrency"></span>
							<p>
								<button type="button" class="buttonS bGray marginT03 btnAddPay" data-pay="10000">+1만원</button>
								<button type="button" class="buttonS bGray marginT03 btnAddPay" data-pay="100000">+10만원</button>
								<button type="button" class="buttonS bGray marginT03 btnAddPay" data-pay="1000000">+100만원</button>
								<button type="button" class="buttonS bGray marginT03 btnAddPay" data-pay="-10000" style="margin-left:10px;background-color:#fff7f7;">-1만원</button>
								<button type="button" class="buttonS bGray marginT03 btnAddPay" data-pay="-100000" style="background-color:#fff7f7;">-10만원</button>
								<button type="button" class="buttonS bGray marginT03 btnAddPay" data-pay="-1000000" style="background-color:#fff7f7;">-100만원</button>
							</p>
						</td>						
					</tr>
					<tr>
						<th>주당 근로시간</th>
						<td>
							<p style="margin-bottom:5px;">* 백단위, 십단위, 일단위 각각 선택해 주세요.</p>
							<div>
								<span id="work_time_week_01_info" class="disabled" style="border: 1px solid #ccc;padding: 6px;">백단위</span>
								<span id="work_time_week_02_info" style="margin-right:1px;border: 1px solid #ccc;padding: 6px;">십단위</span>
								<span id="work_time_week_03_info" style="border: 1px solid #ccc;padding: 6px;">일단위</span>
								<span id="work_time_week_04_info" style="border: 1px solid #ccc;padding: 2px 0 0px 0px;margin: 0px 0 0 10px;width: 102px;display: inline-block;text-align: center;">소숫점</span>
							</div>
							<select id="work_time_week_01" style="width:50px;" disabled="disabled" class="disabled">
								<option value=""></option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
							<select id="work_time_week_02" style="width:50px;">
								<option value=""></option>
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
							<select id="work_time_week_03" style="width:50px;">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
							<b>.</b>
							<select id="work_time_week_04" style="width:50px;">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select>
							<select id="work_time_week_05" style="width:50px;">
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
							</select> 시간
							<input type="hidden" id="work_time_week" name="work_time_week" value="<?php echo $edit['work_time_week'];?>">
						</td>						
					</tr>
				</table>
				<h3 class="sub_stit">상담유형 및 처리결과</h3>
				<table class="tInsert" style="display:none;">
					<caption>
						상담유형 및 처리결과 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>상담유형 <span class="fRed">*</span></th>
						<td>
							<p style="margin-bottom:5px;">* 3개까지 선택 가능합니다.</p>
						<?php
						$index = 0;
						foreach($res['csl_kind'] as $item) {
							echo '<input type="checkbox" name="csl_kind[]" id="csl_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. (strpos($edit['csl_sub_code'], $item->s_code)!==FALSE ? 'checked="checked"' : '') .' class="imgM"><label for="csl_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						<!-- <input type="hidden" name="csl_kind"></input> -->
						</td>
					</tr>
					<tr>
						<th>상담내용 <span class="fRed">*</span></th>
						<td>
							<button type="button" class="buttonS bGray marginT03" id="btnGetExistCounsel">불러오기</button>
							<input type="text" name="csl_title" id="csl_title" style="width:83%;" maxlength="200" class="input_font_size14" value="<?php echo $edit['csl_title'];?>">
							<textarea class="marginT05 input_font_size14" style="width:90%;height:150px;" name="csl_content" id="csl_content"><?php echo $edit['csl_content'];?></textarea>
						</td>						
					</tr>
					<tr>
						<th>답변 <span class="fRed">*</span></th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="csl_reply" id="csl_reply"><?php echo $edit['csl_reply'];?></textarea></td>
					</tr>
					<tr>
						<th>처리결과 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0; $index_etc = 0;
						$etc_item = array('index'=>0, 'item'=>new stdClass());
						foreach($res['csl_proc_rst'] as $item) {
							// 기타 항목은 항상 끝에 배치한다.
// 							if($item->code_name == '기타') {
// 								$etc_item['index'] = $index;
// 								$etc_item['item'] = $item;
// 								$index_etc = $index;
// 							}
// 							else {
								echo '<input type="radio" name="csl_proc_rst" class="imgM" id="csl_proc_rst_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['csl_proc_rst'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_proc_rst_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
// 							}
							$index++;
						}
						// 기타 항목
// 						echo '<input type="radio" name="csl_proc_rst" class="imgM" id="csl_proc_rst_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($edit['csl_proc_rst'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="csl_proc_rst_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="csl_proc_rst_etc_holder"> <!-- data-role-etc_index="<?php //echo $index_etc;?>">-->
								<input type="text" name="csl_proc_rst_etc" id="csl_proc_rst_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>
					</tr>
					<tr>
						<th>주제어</th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_keyword'] as $item) {
							echo '<input type="checkbox" name="csl_keyword[]" id="csl_keyword_'. $index .'" value="'. base64_encode($item->s_code) .'" '. (strpos($edit['csl_sub_code'], $item->s_code)!==FALSE ? 'checked="checked"' : '') .' class="imgM"><label for="csl_keyword_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>
					</tr>	
					<tr>
						<th>상담사례 공유 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_share_yn'] as $item) {
							echo '<input type="radio" name="csl_share_yn" class="imgM" id="csl_share_yn_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($edit['csl_share_yn'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_share_yn_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
							<div class="f11 marginT05 marginL10">* "공유함" 선택시 상담내용 불러오기, 주요상담사례 게시판에 보여지게 됩니다.</div>
						</td>						
					</tr>
				</table>

				<div class="marginT10 textR">
					<button type="button" class="buttonM bGray cssPrint">인쇄</button>
					<button type="button" class="buttonM bSteelBlue cssAdd">등록</button>
					<button type="button" class="buttonM bOrange cssDel">삭제</button>
					<!-- <button type="button" class="buttonM bGray cssBack">뒤로이동</button> -->
					<button type="button" class="buttonM bDarkGray cssList">목록</button>
				</div>
				</form>

			</div>
			<!-- //컨텐츠 -->

			<a href="#" id="focusOuter" style="position:absolute;left:0px;top:0px;"></a>
		</div>
		<!-- //  contents_body  area -->
		
		<?php
		// 상담내용 불러오기 팝업에서 사용할 검색 항목데이터
		$data_csl_proc_rst_code = '';
		foreach($res['csl_proc_rst_code'] as $rs) {
			if($data_csl_proc_rst_code !='') {
				$data_csl_proc_rst_code .= '|';
			}
			$data_csl_proc_rst_code .= base64_encode($rs->s_code) .'*'. $rs->code_name;
		}
		?>
		<span id="csl_proc_rst_code" style="display:none;width:0px;"><?php echo $data_csl_proc_rst_code;?></span>


<?php
include_once './inc/inc_code_desc.php';
include_once './inc/inc_footer.php';
?>
