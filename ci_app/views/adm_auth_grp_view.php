<?php
include_once "./inc/inc_header.php";
?>

<script>
var edit_view_id = "<?php echo base64_encode('S010'); ?>";

$(document).ready(function(){
	// 등록 버튼 이벤트 핸들러
	$('#btnAdd').click(function(e){
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), 'add', '');
	});
	
	// 전송버튼 이벤트 핸들러
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		get_list(_page);
	});
	
	// 사용.사용중지, 삭제 버튼 이벤트 핸들러
	$('#btnUse,#btnNotUse,#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var grp_id = '';
			$('input[name=chk_item]:checked').each(function(i){
				if(grp_id != '') grp_id +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				grp_id += $(this).attr('data-role-id');
			});
			
			var kind = 'using';
			if($(this).text() == '중지') {
				kind = 'not_using';
			}
			else if($(this).text() == '삭제') {
				kind = 'del_multi';
				if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
					return false;
				}
			}
			req_code_manage(kind, grp_id);
		}
	});
	
	// 검색 - 검색어 엔터 처리
	$("#txtKeyword").keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$("#btnSubmit").click();
		}
	});
	
	// list
	get_list(_page);
});


/**
 * 리스트를 동적생성하면 이벤트를 다시 걸어줘야 한다.
 */
function addManagerEventListener() {
	// 관리버튼 클릭 이벤트
	$('button.ai_cmd').click(function(e){
		e.preventDefault();
		var grp_id = $(this).attr('data-role-id');
		var cmd = $(this).text();
		if(cmd == '삭제') {
			if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				req_code_manage('del', grp_id);
			}
		}
		else {
			var kind = 'view';
			if(cmd == '수정') kind = 'edit';
			check_auth_redirect($('#'+ sel_menu).find('li>a').last(), kind, grp_id);
		}
	});
	
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		$('input:checkbox').not(this).prop('checked', this.checked);
	});
}


/**
 * 삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, tid) {
	var url = '/?c=auth&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&grp_id='+ tid;
	var fn_succes = function(data) {
// 		console.log(data);
		// 삭제시 이미 사용된 코드 삭제인 경우 메시지 처리
		if(type.indexOf('del') != -1 && data.rst == 'exist') {
			if(data.len && data.len > 0) {
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_08'].replace('x', data.len);
				alert(msg);
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_07']);
			}
		}
		else {
			if(type != 'view') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
			}
		}
		if(data.tot_cnt && data.tot_cnt > 0) {
			// set config total count
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			// list
			gen_list(data);
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// page
	_page = page;
	
	var url = '/?c=auth&m=auth_list';
	var rsc = $('#frmSearch').serialize() +'&page='+_page;
	var fn_succes = function(data) {
		if(data) {
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.itemPerPage = 10;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			gen_list(data);
		}
		else {
			gen_list();
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	if(!_pagination) {
		_pagination = new Pagination(_cfg_pagination);
	}
	var html_b = '<table class="tList">';
	html_b += '<caption>권한관리 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:5%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:*">';
	html_b += '<col style="width:10%">';
	html_b += '<col style="width:14%">';
	html_b += '<col style="width:20%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th><input type="checkbox" class="imgM checkAll"></th>';
	html_b += '<th>번호</th>';
	html_b += '<th>권한그룹</th>';
	html_b += '<th>사용여부</th>';
	html_b += '<th>등록일</th>';
	html_b += '<th>관리</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			html_b += '<tr>';
			html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ Base64.encode(data.data[index].oper_auth_grp_id) +'"></td>';
			html_b += '  <td>'+  (no--) +'</td>';
			html_b += '  <td>'+ data.data[index].oper_auth_name +'</td>';
			var use_yn = '<span class="labelG">중지</span>';
			if(data.data[index].use_yn == 1) {
				use_yn = '<span class="labelB">사용</span>';
			}
			html_b += '  <td>'+ use_yn +'</td>';
			html_b += '  <td>'+ data.data[index].reg_date +'</td>';
			html_b += '  <td>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(data.data[index].oper_auth_grp_id) +'">권한보기</button>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(data.data[index].oper_auth_grp_id) +'">수정</button>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ Base64.encode(data.data[index].oper_auth_grp_id) +'">삭제</button>';
			html_b += '  </td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="6" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
	
	// 관리버튼 이벤트 등록
	addManagerEventListener();
}

</script>

<?php
include_once './inc/inc_menu.php';
?>

		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">권한조회</h2>
				<span class="location">홈 > 권한관리 > <em>권한조회</em></span>
			</div>
			<div class="cont_area">
				<form name="frmSearch" id="frmSearch" method="post">
				<div class="con_text">	
					<label for="board_select" class="l_title">권한그룹</label>
					<input type="text" id="txtKeyword" name="keyword" style="width:60%">
					<input type="hidden" name="m_code" id="m_code" value="<?php echo $data['m_code']; ?>">
					<span class="divice"></span>
					<label for="board_rows" class="l_title">사용여부</label>
					<span style="padding:8px 0;display:inline-block">
						<input type="radio" name="rdoUse" id="rdoUse0" value="all" class="imgM" checked><label for="rdoUse0">전체</label>
						<input type="radio" name="rdoUse" id="rdoUse1" value="1" class="imgM"><label for="rdoUse1">사용</label>
						<input type="radio" name="rdoUse" id="rdoUse2" value="2" class="imgM"><label for="rdoUse2">사용중지</label>
					</span>
					<span class="divice"></span>
					<div class="textR marginT10">
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>

				<div class="list_no"></div>	
				
				<!-- list -->
				<div id="list"></div>
		
				<div class="floatC marginT10">
					<button type="button" class="buttonM bGray floatL" id="btnUse">사용</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnNotUse">중지</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>
					<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">등록</button>
				</div>
				
				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>
				
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		
		
<?php
include_once './inc/inc_footer.php';
?>
