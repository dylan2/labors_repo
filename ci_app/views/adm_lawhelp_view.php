<?php
include_once './inc/inc_header.php';

// 권한그룹 id
$grp_id = base64_decode($this->session->userdata(CFG_SESSION_ADMIN_AUTH_GRP_ID));

// 조회권한 여부
$is_auth_view = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_LAWHELP_VIEW);

?>


<script>
$(document).ready(function(){
	// 등록 버튼 이벤트 핸들러
	$('#btnAdd').click(function(e){
		check_auth_redirect($('#'+ sel_menu).find('li>a').first(), 'add', '');
	});
	
	// 전송버튼 이벤트 핸들러
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		_page = 1;
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.27 
		$.cookie("laborsSearchDataLawhelp", $('form[name=frmSearch]').serialize());
		get_list(_page);
	});
	
	// 초기화 이벤트 핸들러
	$('#btnClear').click(function(e){
		e.preventDefault();
		_page = 1;
		$('form')[0].reset();
		$('#keyword').val('').css('display', 'inline');
		$('#target_sprt_kind_cd').css('display', 'none'); // 지원종류
		$('#target_apply_rst_cd').css('display', 'none'); // 지원결과
	});
	
	// 다중 삭제 버튼 이벤트 핸들러
	$('#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var csl_seqs = '';
			$('input[name=chk_item]:checked').each(function(i){
				if(csl_seqs != '') csl_seqs +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				csl_seqs += $(this).attr('data-role-id');
			});
			
			var kind = 'del_multi';
			if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				return false;
			}

			req_code_manage(kind, csl_seqs);
		}
	});

	// Excel - button
	$('#btnExcel').click(function(e){
		e.preventDefault();
		// 엑셀 다운로드할 내용 유무 체크
		if($('.tList tr>td').length <= 1 && $('.tList tr>td').text().indexOf('없습니다') != -1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var url = '/?c=lawhelp&m=vdownload_excel&kind=lawhelp_list';
		gLayerId = openLayerModalPopup(url, 400, 200, '', '', '', '', 1, true, '');
	});

	// 검색 - 검색어 변경시 처리
	$('#target').change(function(e){
		var display = 'none', width = 60;
		var sel = $(this).val();
		// 지원종류
		if(sel == 'sprt_kind_cd') {
			width = 50;
			$('#target_sprt_kind_cd').css('display', 'inline');
			$('#target_apply_rst_cd').css('display', 'none');
		}
		// 지원결과
		else if(sel == 'apply_rst_cd') {
			width = 50;
			$('#target_apply_rst_cd').css('display', 'inline');
			$('#target_sprt_kind_cd').css('display', 'none');
		}
		else {
			$('#target_apply_rst_cd').css('display', 'none');
			$('#target_sprt_kind_cd').css('display', 'none');	
		}
		$('#keyword').css('width', width +'%');
	});
	
	// 검색 - 검색어 엔터 처리
	$("#keyword").keypress(function(e) {
		if (e.keyCode == 13) {
			$("#btnSubmit").click();
		}
	});

   //연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for(var i=year; i>=2015; i--){
		$("#sel_year").append("<option value="+i+">"+i+"년</option>");
	}

	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates();
	});

	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});


	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();

		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_end = '';
		}

		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});	

	
	// *** 상담일만 적용해 달라는 요청 2016.06.15 최진혁
	// 검색어 채워넣기
	// -- 검색 후 history.back 버튼 클릭시 검색어 채워넣기 안되는 문제 제기로 클라이언트 쿠키에 저장하는 방식으로 변경함. dylan, 2018.08.27 
	var schData = $.cookie("laborsSearchDataLawhelp");
	// *** 키워드 검색시 검색어 유지 요청으로 수정 2017.09.12 서재란
	// - 일자검색
	var schDateTarget = getValueInGetStringByKey(schData, "search_date_target");
	$('select[name=search_date_target] option[value="'+ schDateTarget +'"]').prop('selected', true);
	// - 일자
	var schDateBegin = getValueInGetStringByKey(schData, "search_date_begin");
	$('input[name=search_date_begin]').val(schDateBegin);
	var schDateEnd = getValueInGetStringByKey(schData, "search_date_end");
	$('input[name=search_date_end]').val(schDateEnd);
	// - 년월 selectbox
	var schSelYear = getValueInGetStringByKey(schData, "sel_year");
	$('select[name=sel_year] option[value="'+ schSelYear +'"]').prop('selected', true);
	var schSelMonth = getValueInGetStringByKey(schData, "sel_month");
	$('select[name=sel_month] option[value="'+ schSelMonth +'"]').prop('selected', true);
	// - 연령
	var schAges = getValueInGetStringByKey(schData, "search_ages");
	$('select[name=search_ages] option[text="'+ schAges +'"]').prop('selected', true);
	// - 검색대상
	var schTarget = getValueInGetStringByKey(schData, "target");
	$('select[name=target] option[value="'+ schTarget +'"]').prop('selected', true);
	$('select[name=target]').change();
	// - 검색어
	var schKeyword = getValueInGetStringByKey(schData, "keyword");
	if(schKeyword) {
		schKeyword = decodeURIComponent(schKeyword)
	}
	$('input[name=keyword]').val(schKeyword);

	// - 지원결과
	// var target_apply_rst_cd = "<?//php echo isset($search_data['target_apply_rst_cd']) && $search_data['target_apply_rst_cd']!='' ? $search_data['target_apply_rst_cd'] : '';?>";
	// if(target_apply_rst_cd != '') {
	// 	target_apply_rst_cd = Base64.encode(target_apply_rst_cd);
	// 	$('select[name=target_apply_rst_cd] option[value="'+ target_apply_rst_cd +'"]').prop('selected', true);
	// 	$('select[name=target_apply_rst_cd]').change();
	// }
	
	
	// list
	// 요청에 의한 수정 : dylan 2017.09.12, danvistory.com
	get_list("<?php echo isset($search_data['page']) ? $search_data['page'] : 1;?>");
	
	$('table tbody tr').hover(function() {
		$(this).css( "background-color", "#efefef" );
	}, function() {  
		$(this).css( "background-color", "transparent" );
	});
});


/**
 * 리스트를 동적생성하면 이벤트를 다시 걸어줘야 한다.
 */
function addManagerEventListener() {
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		var status = $(this).prop('checked');
		$("input[name=chk_item]:checkbox").each(function(i) {
			$(this).prop("checked", status);
		});
	});
}


/**
 * 삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, tid) {
	var url = '/?c=lawhelp&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&seqs='+ tid +'&page='+ _page;
	var fn_succes = function(data) {
		alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);

		// set config total count
		_cfg_pagination.total_item = data.tot_cnt;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// list
		gen_list(data);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		// empty list
		_cfg_pagination.total_item = 1;
		_cfg_pagination.currentPage = 1;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// search date
	var date_b = $('#search_date_begin').val();
	var date_e = $('#search_date_end').val();
	if((date_b && date_e == '' ) || (date_b == '' && date_e)) {
		var now = new Date();
		var today = get_today();
		if(date_b == '') $('#search_date_begin').val(today);
		// if(date_e == '') $('#search_date_end').val(today);
	}

	// page
	_page = page;
	
	var url = '/?c=lawhelp&m=lawhelp_list';
	var rsc = $('#frmSearch').serialize() +'&page='+_page;
	var fn_succes = function(data) {
		// set config total count
		_cfg_pagination.total_item = data.tot_cnt == 0 ? 1 : data.tot_cnt;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// build the list
		gen_list(data);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		// empty list
		_cfg_pagination.total_item = 1;
		_cfg_pagination.currentPage = 1;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

// 권리구제 내용 뷰 권한 여부
var is_auth_view = '<?php echo $is_auth_view;?>';
var login_oper_id = "<?php echo $oper_id; ?>";
// edit
function edit(seq, oper_id) {
	var kind = 'edit';

	// 권리구제 내용 뷰 권한이 있으면 예외처리
	if(is_master == 1 || is_master != 1 && (is_auth_view == 1 || oper_id == login_oper_id)) {
		kind = 'view';
	}
	// 상세내용 조회권한이 없는 경우
	// 내용조회only(수정없음), 엑셀다운로드only 권한 상수 : 이 코드는 DB에서 관리하지 않고 별도로 상수로만 관리한다.
	else {
		alert(CFG_MSG[CFG_LOCALE]['info_auth_01']);
		return false;
	}

	check_auth_redirect($('#'+ sel_menu).find('li>a').first(), kind, seq);
	
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var html_b = '<table class="tList" border="0">';
	html_b += '<caption>권리구제지원조회 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:2%">';
	html_b += '<col style="width:4%">';
	html_b += '<col style="width:4%">';
	html_b += '<col style="width:4%">';
	html_b += '<col style="width:4%">';
	html_b += '<col style="width:5%">';
	html_b += '<col style="width:5%">';
	html_b += '<col style="width:5%">';
	html_b += '<col style="width:5%">';
	html_b += '<col style="width:10%">';
	html_b += '<col style="width:12%">';
	html_b += '<col style="width:6%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:6%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th><input type="checkbox" class="imgM checkAll"></th>';
	html_b += '<th>번호</th>';
	html_b += '<th>접수<br>번호</th>';
	html_b += '<th>신청자</th>';
	html_b += '<th>연령대</th>';
	// 추가개발로 수정 2018.07.09 dylan
	// html_b += '<th>회사</th>';
	// html_b += '<th>지원종류</th>';
	html_b += '<th>성별</th>';
	html_b += '<th>직종<br>(상세)</th>';
	html_b += '<th>업종</th>';
	html_b += '<th>사건<br>유형<br>(상세)</th>';
	//--//
	html_b += '<th>신청기관</th>';
	html_b += '<th>대리인</th>';
	html_b += '<th>대상기관</th>';
	html_b += '<th>지원승인일</th>';
	html_b += '<th>사건접수일</th>';
	html_b += '<th>출석일</th>';
	html_b += '<th>사건종결일</th>';
	html_b += '<th>지원결과</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	var tr_bgcolor = '';
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		var lh_apply_nm;
		var txt_lh_apply_comp_nm;

		for(var i=begin; i<end; i++) {
			lh_apply_nm = data.data[index].lh_apply_nm;
			lh_apply_nm = data.data[index].lh_apply_nm;
			lh_apply_nm = lh_apply_nm == '' ? lh_apply_nm : lh_apply_nm.substr(0, 1) + 'OO';
			/*lh_apply_comp_nm = data.data[index].lh_apply_comp_nm;
			if(lh_apply_comp_nm != '') {
				left3 = lh_apply_comp_nm.substr(0, 5);
				if(left3.indexOf('(주)') != -1 || left3.indexOf('( 주)') != -1 || left3.indexOf('(주 )') != -1 || left3.indexOf('( 주 )') != -1){
					txt_lh_apply_comp_nm = '(주)';
				}
				else {
					txt_lh_apply_comp_nm = lh_apply_comp_nm.substr(0, 1);
				}
				txt_lh_apply_comp_nm += 'OOO..';
			}
			*/
			// 직종(상세)
			var tmp_work_kind_nm = remove_null(data.data[index].work_kind_nm);
			var txt_work_kind_nm = remove_null(data.data[index].work_kind_etc) ? tmp_work_kind_nm + '<br>('+ data.data[index].work_kind_etc +')' : tmp_work_kind_nm;
			// 사건유형(상세)
			var tmp_lh_kind_cd_nm = remove_null(data.data[index].lh_kind_cd_nm);
			var txt_lh_kind_cd_nm = remove_null(data.data[index].lh_kind_sub_cd_nm) ? tmp_lh_kind_cd_nm + '<br>('+ data.data[index].lh_kind_sub_cd_nm +')' : tmp_lh_kind_cd_nm;

			html_b += '<tr>';
			html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ data.data[index].seq +'"></td>';
			html_b += '  <td>'+ no-- +'</td>';
			html_b += '  <td onclick="javascript:edit(\''+ data.data[index].seq +'\',\''+ data.data[index].reg_oper_id +'\');" style="cursor:pointer;text-decoration:underline;">'+ data.data[index].lh_code +'</td>';
			html_b += '  <td>'+ lh_apply_nm +'</td>';
			html_b += '  <td>'+ data.data[index].ages_nm +'</td>';	//연령대
			// html_b += '  <td>'+ txt_lh_apply_comp_nm +'</td>';
			// html_b += '  <td>'+ data.data[index].sprt_kind_cd_nm +'</td>';
			html_b += '  <td>'+ remove_null(data.data[index].gender_cd_nm) +'</td>';
			html_b += '  <td>'+ txt_work_kind_nm +'</td>';
			html_b += '  <td>'+ remove_null(data.data[index].comp_kind_nm) +'</td>';
			html_b += '  <td>'+ txt_lh_kind_cd_nm +'</td>';
			//
			html_b += '  <td>'+ data.data[index].apply_organ_cd_nm  +'</td>';
			html_b += '  <td>'+ data.data[index].lh_labor_nm +'('+ data.data[index].lh_labor_asso_nm +')</td>';
			html_b += '  <td>'+ data.data[index].sprt_organ_cd_nm +'</td>';
			html_b += '  <td>'+ data.data[index].lh_sprt_cfm_date +'</td>';
			html_b += '  <td>'+ remove_null(data.data[index].lh_accept_date) +'</td>';
			html_b += '  <td>'+ remove_null(data.data[index].lid_date).replace(/\,/g, '<br>') +'</td>';
			html_b += '  <td>'+ remove_null(data.data[index].lh_case_end_date) +'</td>';
			html_b += '  <td>'+ remove_null(data.data[index].apply_rst_cd_nm) +'</td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="17" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
	
	// 체크박스 전체선택 이벤트 등록
	addManagerEventListener();

	// 버튼 블럭 show
	$('.cssBtnBlock').show();
}

function remove_null(data) {
	return data == null ? '' : data == '0000-00-00' ? '' : data;
}
</script>
<style type="text/css">
.tList th {
	line-height: 13px;
}
.tList td {
	font-size: 11px;
}
</style>

<?php
include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">권리구제내역 조회</h2>
				<span class="location">홈 > 권리구제내역 관리 > <em>권리구제내역 조회</em></span>
			</div>
			<div class="cont_area">
				<form name="frmSearch" id="frmSearch" method="post">
				<input type="hidden" name="is_master" value="<?php echo $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);?>">
				<div class="con_text">
					<label for="search_date_target" class="l_title" style="width:70px">일자검색</label>
					<select id="search_date_target" name="search_date_target" class="styled">
						<option value="lh_sprt_cfm_date">지원승인일</option>
						<option value="lh_accept_date">사건접수일</option>
						<option value="lh_case_end_date">사건종결일</option>
					</select>
					<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" style="width:90px" readonly="readonly"> ~ 
					<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" style="width:90px" readonly="readonly">
					<label for="sel_year" class="l_title" style="margin-left:10px;">연월별</label>
					<select id="sel_year" name="sel_year" style="margin-left:-20px;">
						<option value="">연도</option>
					</select>
					<select id="sel_month" name="sel_month">
						<option value="">전체</option>
						<option value="01">1월</option>
						<option value="02">2월</option>
						<option value="03">3월</option>
						<option value="04">4월</option>
						<option value="05">5월</option>
						<option value="06">6월</option>
						<option value="07">7월</option>
						<option value="08">8월</option>
						<option value="09">9월</option>
						<option value="10">10월</option>
						<option value="11">11월</option>
						<option value="12">12월</option>
					</select>
					<label for="sel_year" class="l_title" style="margin-left:10px;">기간별</label>
					<button type="button" id="btnToday" class="buttonS bGray"  style="margin-left:-20px;">오늘</button>
					<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
					<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
					<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button>
					* 지원승인일 대상으로 검색됩니다.
					<br>
					<span class="divice"></span>
					<label for="search_ages" class="l_title">연령</label>
					<select id="search_ages" name="search_ages" class="styled" style="margin-left:10px;">
						<option value="all">전체</option>
						<?php
						foreach($res['search_ages'] as $rs) {
							echo '<option value="'. $rs->s_code .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<br>
					<span class="divice"></span>
					<label for="target" class="l_title" style="width:70px">검색어</label>
					<select id="target" name="target" class="styled">
						<option value="all" selected>전체</option>
						<option value="lh_code">접수번호</option>
						<option value="sprt_kind_cd">지원종류</option>
						<option value="lh_sprt_content">권리구제지원내용</option>
						<option value="apply_rst_cd">지원결과</option>
						<option value="lh_apply_nm">신청자</option>
						<option value="lawhelp_lh_labor">대리인</option>
						<option value="apply_organ_cd">신청기관</option>
						<option value="sprt_organ_cd">대상기관</option>
					</select>
					<!--지원종류 선택시 코드 노출 -->
					<select id="target_sprt_kind_cd" name="target_sprt_kind_cd" class="styled" style="width:120px;display:none;">
						<option value="all">전체</option>
						<?php
						foreach($res['code_sprt_kind'] as $item) {
							echo '<option value="'. base64_encode($item->s_code) .'">'. $item->code_name  .'</option>';
						}
						?>
					</select>
					<!--지원결과 선택시 코드 노출 -->
					<select id="target_apply_rst_cd" name="target_apply_rst_cd" class="styled" style="width:120px;display:none;">
						<option value="" selected>전체</option>
						<?php
						foreach($res['code_sprt_rst'] as $item) {
							echo '<option value="'. base64_encode($item->s_code) .'">'. $item->code_name  .'</option>';
						}
						?>
					</select>
					<!--검색대상:직종인 경우 검색어의 내용을 직종 추가 입력 항목에 대해서 검색한다. 전체 검색일 경우도 마찬가지. -->
					<input type="text" style="width:60%;" id="keyword" name="keyword">
					<span class="divice"></span>
					<div id="infoLabel" class="floatL" style="height:40px;"> * 전체 검색시 지원종류,지원결과는 제외됩니다.</div>
					<div class="textR" style="margin-top:-10px;">
						<button type="button" id="btnClear" class="buttonS bGray"><span class="icon_all"></span>초기화</button>
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>

				<div class="list_no"></div>
				
				<!-- list -->
				<div id="list"></div>
		
				<div class="floatC marginT10 cssBtnBlock" style="display:none;">
				<?php
					// 관리자 그룹인 경우만 다중삭제 버튼 노출
					if($this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER) == 1 || $grp_id == CFG_AUTH_CODE_ADMIN) {
						echo '<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>'. PHP_EOL;
					}
				?>
					<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">등록</button>
				<?php
					// 엑셀 다운로드 권한 여부
					if($grp_id == CFG_AUTH_CODE_ADMIN || $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER) == 1 || $this->session->userdata(CFG_SESSION_ADMIN_AUTH_LAWHELP_EXCEL_DOWNLOAD) == 1) {
						echo '<button type="button" class="buttonM bGray floatR" id="btnExcel" style="margin-right:5px;">엑셀 다운</button>'. PHP_EOL;
					}
				?>
				</div>
				
				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>

			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
	
<?php
include_once './inc/inc_footer.php';
?>
