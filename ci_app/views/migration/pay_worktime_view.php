<?php
include_once './inc/inc_header.php';


?>

<script>

$(document).ready(function(){

	var is_master = "<?php echo $data['is_master'];?>";
	if( ! is_master) {
		alert('잘못된 접속입니다.');
		location.href = '/';
	}

	// 전송버튼 이벤트 핸들러
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		if(confirm("수정한 내용을 저장하시겠습니까?")) {
			var url = '/?c=migration&m=pay_edit';
			var rsc = $('#frmSearch').serialize();
			var fn_succes = function(data) {
				if(data.rst == 'succ') {
					alert("처리되었습니다.");
				}
				else {
					alert("장애가 발생하였습니다.");
				}
			};
			var fn_error = function(data) {
				if(is_local) objectPrint(data);
			
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				alert(msg);
			};
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});
	
	// list
	get_list();
	
});


/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// page
	_page = page;
	
	var url = '/?c=migration&m=pay_lists';
	var rsc = $('#frmSearch').serialize() +'&page='+_page;
	var fn_succes = function(data) {
		// set config total count
		_cfg_pagination.total_item = data.tot_cnt == 0 ? 1 : data.tot_cnt;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// build the list
		gen_list(data);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		// empty list
		_cfg_pagination.total_item = 1;
		_cfg_pagination.currentPage = 1;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var html_b = '<table class="tList" border="0">';
	html_b += '<caption>목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:3%">';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:30%">';
	html_b += '<col style="width:30%">';
	html_b += '<col style="width:7%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th>번호</th>';
	html_b += '<th>상담자명</th>';
	html_b += '<th>소속명</th>';
	html_b += '<th>평균임금</th>';
	html_b += '<th>근로시간</th>';
	html_b += '<th>상담내용</th>';
	html_b += '<th>답변내용</th>';
	html_b += '<th>상담일</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	var tr_bgcolor = '';
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = +data.tot_cnt;
		
		for(var i=0; i<total_cnt; i++) {
			var d = data.data[i];
			html_b += '<tr>';
			html_b += '  <td><input type="hidden" name="seq[]" value="'+ d.seq +'"></input>'+ (i+1) +'</td>'
			html_b += '  <td>'+ d.oper_name +'</td>';
			html_b += '  <td>'+ d.code_name +'</td>';
			html_b += '  <td>원본: '+ d.ave_pay_month_org +'<p style="width:100%;padding:10px 0;margin-bottom:10px;border-bottom:1px #ddd  solid;"></p>수정: <input type="text" name="ave_pay_month[]" style="width:50px;" value="'+ d.ave_pay_month +'"></td>';
			html_b += '  <td>원본: '+ d.work_time_week_org +'<p style="width:100%;padding:10px 0;margin-bottom:10px;border-bottom:1px #ddd solid;"></p>수정: <input type="text" name="work_time_week[]" style="width:50px;" value="'+ d.work_time_week +'"></td>';
			html_b += '  <td>'+ d.csl_content  +'</td>';
			html_b += '  <td>'+ d.csl_reply +'</td>';
			html_b += '  <td>'+ d.csl_date +'</td>';
			html_b += '</tr>';
		}
	}
	else {
		html_b += '<tr><td colspan="8" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b);
}
</script>

<?php
// include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body" style="width:100%;">
			<div id="cont_head">
				<h2 class="h2_tit">임금,근로시간 데이터 수정</h2>
				<span class="location">홈 > 임금,근로시간 데이터 수정</em></span>
			</div>
			<div class="cont_area">
				<form name="frmSearch" id="frmSearch" method="post">

				<div class="list_no"></div>
				
				<!-- list -->
				<div id="list"></div>
		
				<div class="floatC marginT10">
					<button type="button" class="buttonM bSteelBlue floatR" id="btnSubmit">전송</button>
				</div>
				
				</form>
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
	
<?php
include_once './inc/inc_footer.php';
?>
