<?php
/**
 * 
 * 
 * 상담내역 - 기본통계
 * 
 * 
 * 
 */
include_once "./inc/inc_header.php";
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="./js/counsel/common.js"></script>
<script type="text/javascript">
var chart_mode = 1;
$(document).ready(function(){

	//차트 보기
	$('#view_chart').click(function(e){
    e.preventDefault();
    if(chart_mode == 0){ //숨기기
    	$('.chart').css('display', 'none');
    	$('#printchart').css('display', 'none');
    	$('#downchart').css('display', 'none');
    	$('#sel_chart').css('display', 'none');
    	$('#view_chart').text('통계차트 보기');

    	chart_mode = 1;
    }
    else{	//보기
    	$('#view_chart').text('통계차트 숨기기');
    	$('#downchart').css('display', 'inline');
    	$('#printchart').css('display', 'inline');
    	$('#sel_chart').css('display', 'inline');
    	$('#sel_chart').trigger('change');

    	var position = $('#downchart').offset();		//스크롤이동
        $('html, body').animate({scrollTop : position.top}, 1000);
        chart_mode = 0;
    }
  });

	//차트
	$('#sel_chart').change(function(){
		var sel_chart_index = $('#sel_chart option:selected').val();
		// 구글차트를 위한 데이터를 가져옴
		var url = '/?c=statistic&m=st13';
		var rsc = $('#frmSearch').serialize() +'&not_seq=0';
		var fn_succes = function(data) {  		
			if(sel_chart_index == 0){
				column_chart(data);
			}
			else if(sel_chart_index == 1){
				bar_chart(data);
			}
			else if(sel_chart_index == 2){
				donut_chart(data);
			}
			else if(sel_chart_index == 3){
				pie_chart(data);
			}
			else if(sel_chart_index == 4){
				line_chart(data);
			}
			var rst = data.data_tot[0]['tot'];
			if(rst == '0(0)' || rst == '0') {
				alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
			}
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);

		$('.chart').css('display', 'none');

		if(sel_chart_index == 0){
			$('#column_chart').css('display', 'block');
		}
		else if(sel_chart_index == 1){
			$('#bar_chart').css('display', 'block');
		}
		else if(sel_chart_index == 2){
			$('#donut_chart').css('display', 'block');
		}
		else if(sel_chart_index == 3){
			$('#pie_chart').css('display', 'block');
		}
		else if(sel_chart_index == 4){
			$('#line_chart').css('display', 'block');
		}
	});

	//위로 이동
	$('#view_top').click(function(e){
    e.preventDefault();
    var position = $('#contents_body').offset();
    $('html, body').animate({scrollTop : position.top}, 1000);
  });

	$(".stab_btn").on("click", function(e) {
		e.preventDefault();
		var index = $(".stab_btn").index(this);
		$('#txt_sch_kind').val($(this).text());
		$('#sch_kind').val(index);
		$(".stab_btn").attr('class','stab_btn');
		$(this).attr('class','stab_btn active');

		//검색대상이 상담유형이면, 안내 레이블 출력
		if(index == 12) $('#infoLabel').css('display', 'inline');
    else $('#infoLabel').css('display', 'none');

		get_list(_page);
	});

	// 상담방법 조회 버튼
	$('#btn_s_code').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#s_code').val(data.s_code);
				}
				if(data.code_name){
					$('#s_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=s_code_popup_view&kind=down13';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 상담방법 초기화 버튼
	$('#btnInitSCode').click(function(e){
		e.preventDefault();
		$('#s_code').val("");
		$('#s_code_name').text("전체");
	});

	// 처리결과 조회 버튼
	$('#btn_csl_proc_rst').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#csl_proc_rst').val(data.s_code);
				}
				if(data.code_name){
					$('#csl_proc_rst_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=csl_proc_rst_popup_view&kind=down13';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 처리결과 초기화 버튼
	$('#btnInitCslProcRst').click(function(e){
		e.preventDefault();
		$('#csl_proc_rst').val("");
		$('#csl_proc_rst_name').text("전체");
	});

	// 소속 조회 버튼
	$('#btn_asso_code').click(function(e){
		e.preventDefault();
		var closedCallback = function(data) {
			if(data) {
				if(data.s_code){
					$('#asso_code').val(data.s_code);
				}
				if(data.code_name){
					$('#asso_code_name').text(data.code_name);
				}
			}
		};
		var url = '/?c=statistic&m=asso_code_popup_view&kind=down13';
		gLayerId = openLayerModalPopup(url, 600, 600, '', '', '', closedCallback, 1, true);
	});
	// 소속 초기화 버튼
	$('#btnInitAssoCode').click(function(e){
		e.preventDefault();
		$('#asso_code').val( $('#asso_code_all').val() );
		$('#asso_code_name').text("전체");
	});

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for(var i=year; i>=2015; i--){
		$("#sel_year").append("<option value="+i+">"+i+"년</option>");
	}

	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates('', 2);
		if($(this).val() == '') {
			$('#btnAllTerm').click();
		}
	});

	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});


	//통계탭 이동
	$(".go_page").on("click", function(e) {
		e.preventDefault();
		var index = +$(".go_page").index(this)+1;
		
		if(index == 2) index = 13;
		else if(index == 3) index = 11;
		else if(index == 6) index = 12;
		
		var begin = $('#search_date_begin').val();
		var end = $('#search_date_end').val();
		var sch_asso_code = $("#asso_code").val();
		var sch_s_code = $("#s_code").val();
		var sch_csl_proc_rst = $("#csl_proc_rst").val();		
		go_page(index, begin, end, sch_asso_code, sch_s_code, sch_csl_proc_rst);
	});


	// print a list
	$('#printlist').click(function(){
		var txt_sch_kind = $('#txt_sch_kind').val();
		var popupWindow = window.open("", "_blank");      
    var $div = $('#list').clone();
    $div.find('table').css('border-collapse','collapse').find('th, td').css({'border':'solid 1px #eee', 'height':'30px'});
    $div.find('th').css('background-color','#e8fcff');  
		popupWindow.document.write("<body>");
		popupWindow.document.write("<h3>기본통계 "+txt_sch_kind+"</h3>");
		popupWindow.document.write($div.html());
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("table").css("width", "100%");
		$(popupWindow.document).find("td").css("text-align", "center");
		popupWindow.print();
		popupWindow.close();
	});

	// print a chart images
	$('#printchart').click(function(){
		var sel_chart_index = $('#sel_chart option:selected').val();
		var popupWindow = window.open("", "_blank");      
		var div = $('#chart_div'+sel_chart_index).html();
		popupWindow.document.write("<body>");
		popupWindow.document.write(div);
		popupWindow.document.write("</body>");
		popupWindow.document.close();
		$(popupWindow.document).find("img").css("width", "100%");
		popupWindow.print();
		popupWindow.close();
	});


	// download a chart images
	$('#downchart').click(function(){
		$('#chart_div').css('display','block');
    var sel_chart_index = $('#sel_chart option:selected').val();
    var chart_div_id = "#chart_div"+ sel_chart_index;
		html2canvas($(chart_div_id), {
			// html2canvas는 보이는 부분만 캡쳐하기 때문에 chart_div내 첫번째 차트 이미지의 가로 크기가 커지는 경우 
			// 캡쳐안되는 문제가 있어 첫번째 차트 이미지의 크기를 캡쳐영역으로 지정하도록 수정함
	    width: $(chart_div_id).find('img').width(),
	    height: $(chart_div_id).height(),
			onrendered: function(canvas) {
				var img = canvas.toDataURL();
				$("body").append('<form id="imgForm" method="POST" target="_blank" action="/?c=chart_download&m=download">' + 
				'<input type="hidden" name="dataUrl" value="' + img + '">' +
				'</form>');
				$("#imgForm").submit();
				$("#imgForm").remove();
			}
		});
		$('#chart_div').css('display','none');
	});

	// download a excel file
	$('#downExcel').click(function(){
		if($('#list').find('tr>td').length == 1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var sch_kind_index = 0;
		sch_kind_index = $('#sch_kind').val();
		var url = '/?c=statistic&m=open_down_view&kind=down13&sel_col='+sch_kind_index;
		gLayerId = openLayerModalPopup(url, 400, 220, '', '', '', '', 1, true);
	});

	// 검색
  $("#btnSubmit").click(function(e) {
		e.preventDefault();
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.29
		$('input[name=asso_code_name]').val( $('#asso_code_name').text());
		$('input[name=s_code_name]').val($('#s_code_name').text());
		$('input[name=csl_proc_rst_name]').val($('#csl_proc_rst_name').text());
		$.cookie("laborsSearchDataStatis", $('form[name=frmSearch]').serialize());
		get_list(_page);
  });

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();

		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_begin = '2015-01-01';
		}
		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});
	
	// list
	get_list(_page, true);
});


function get_today() {
	var now = new Date();
	return now.getFullYear() +'-'+ genTwoDigit(now.getMonth()+1) +'-'+ genTwoDigit(now.getDate());
}

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page, is_first) {

	// page
	_page = page;

	var url = '/?c=statistic&m=st13';
	var rsc = $('#frmSearch').serialize() +'&not_seq=0';
	var fn_succes = function(data) {
		gen_list(data);

		if(chart_mode == 0){	//차트 보기 상태일 때
			var sel_chart_index = $('#sel_chart option:selected').val();
			if(sel_chart_index == 0){
				column_chart(data);
			}
			else if(sel_chart_index == 1){
				bar_chart(data);
			}
			else if(sel_chart_index == 2){
				donut_chart(data);
			}
			else if(sel_chart_index == 3){
				pie_chart(data);
			}
			else if(sel_chart_index == 4){
				line_chart(data);
			}
		}

		var rst = data.data_tot[0]['tot'];
        if(rst == '0(0)' || rst == '0') alert(CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03']);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);

}


/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {

	var sch_kind = $('#sch_kind').val();
	var index = 0;
	var total = data.data_tot[0]['tot'];

	var html_b = '<table class="tList02" border=0>';
	html_b += '<colgroup>';
	html_b += '<col style="width:33%">';
	html_b += '<col style="width:33%">';
	html_b += '<col style="width:33%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th style="line-height:15px;">구분</th>';
	html_b += '<th style="line-height:15px;">상담건수</th>';
	html_b += '<th style="line-height:15px;">퍼센트</th>';
	html_b += '</tr>';
	
	if(data.tot_cnt > 0) {
		html_b += '<tr>';
		for(var item in data.data_tot[0]) {
			if(index %2 ==0) html_b += '</tr><tr>';	
			html_b += '  <td '+ (data.data_tot[0][item]=='총계' || item=='tot' ? ' style="background:#F1F1F1 !important;"' : '') +'>'+ applyComma(data.data_tot[0][item]) + (data.data_tot[0][item]=='총계' && $('#sch_kind').val()==12 ? '(실건수)' : '') +'</td>';
			if(item != 'tot' && sch_kind != 12 && index %2 == 1){
				if(data.data_tot[0]['tot'] == 0){
					html_b += '<td>0 %</td>';
				}
				else{
					html_b += '<td>'+ ((data.data_tot[0][item]) / total * 100).toFixed(1) +' %</td>';
				}
			}
			else if(item != 'tot' && sch_kind == 12 && index %2 == 1){
				var strArray = total.split('(');
				html_b += '<td>'+ ((data.data_tot[0][item]) / Number(strArray[0]) * 100).toFixed(1) +' %</td>';
			}
			else if(item == 'tot'){
				html_b += '<td style="background:#F1F1F1 !important;">100 %</td>';
			}
			index++;
		}
		html_b += '</tr>';
	}
	else {
		html_b += '<tr><td colspan="30" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// list html
	$('#list').empty().html(html_b);
}


/*컬럼차트 만들기 시작*/
google.charts.load('current', {'packages':['corechart', 'corechart']});

function column_chart(data) {
	var chart_html = '';
    for(var i=0; i<5; i++) {
        chart_html += '<div id="chart_div'+i+'" class="chart_div"></div>';
        $('#chart_div').html(chart_html);
    }

	var txt_sch_kind = $('#txt_sch_kind').val();
	var sch_kind = $('#sch_kind').val();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	array_data[0] = ['구분', '상담건수', {role:'annotation'} , {role:'style'}];
	var length = Object.keys(data.data_tot[0]).length;
	length = (length/2)-1;
	for(var i = 1; i<=length;  i++) {
		value = Number(data.data_tot[0]['t'+i]);
		subject = data.data_tot[0]['s'+i];
		/*if(sch_kind == 6 || sch_kind == 7){
			subject = subject.substring(0,7);
			subject += '..';
		}	*/
		if(subject == '총계') break;
		array_data[i] = [subject, value, value, '#3366CC'];
	}
    var chart_data = new google.visualization.arrayToDataTable(array_data);

    var options = {
    	title: '['+txt_sch_kind+'] Column Chart',
    	width:1620,
    	height:700,
    	chartArea:{
		    left:100,
		    right:100,
		    top: 100,
		    bottom:100,
		    width: '100%',
		    height: '400',
		},
		bar: {
			groupWidth: "30%",
		},
		fontSize:12,
		axisTitlesPosition:'out',
    	legend: { position: 'none'},
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('column_chart'));
    chart.draw(chart_data, options);

    img_chart(0, chart_data, options);
}
/*컬럼차트 만들기 끝*/
/*바차트 만들기 시작*/
function bar_chart(data) {
	var txt_sch_kind = $('#txt_sch_kind').val();
	var sch_kind = $('#sch_kind').val();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	array_data[0] = ['구분', '상담건수', {role:'annotation'} , {role:'style'}];
	var length = Object.keys(data.data_tot[0]).length;
	length = (length/2)-1;
	for(var i = 1; i<=length;  i++) {
		value = Number(data.data_tot[0]['t'+i]);
		subject = data.data_tot[0]['s'+i];	
		if(subject == '총계') break;
		array_data[i] = [subject, value, value, 'gold'];
	}
    var chart_data = new google.visualization.arrayToDataTable(array_data);
    var options = {
    	title: '['+txt_sch_kind+'] Bar Chart',
    	width:1620,
    	height:700,
    	chartArea:{
		    left:250,
		    right:50,
		    top: 100,
		    bottom:100,
		    width: '100%',
		    height: '400',
		},
		fontSize:12,
		axisTitlesPosition:'in',
    	legend: { position: 'none'},
   		isStacked: true
    };

    var chart = new google.visualization.BarChart(document.getElementById('bar_chart'));
    chart.draw(chart_data, options);

    img_chart(1, chart_data, options);
}
/*바차트 만들기 끝*/
/*도넛차트 만드기 시작*/
function donut_chart(data){
	var txt_sch_kind = $('#txt_sch_kind').val();
	var sch_kind = $('#sch_kind').val();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	array_data[0] = ['구분', '상담건수'];
	var length = Object.keys(data.data_tot[0]).length;
	length = (length/2)-1;
	for(var i = 1; i<=length;  i++) {
		value = Number(data.data_tot[0]['t'+i]);
		subject = data.data_tot[0]['s'+i];	
		if(subject == '총계') break;
		array_data[i] = [subject, value];
	}
    var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '['+txt_sch_kind+'] Donut Chart',
		width:1620,
    	height:700,
    	chartArea:{
		    left:100,
		    right:100,
		    top: 50,
		    bottom:50,
		    width: '100%',
		    height: '100%',
		},
		legend:{
			position:'right',
			maxLines:3,
		},
		tooltip:{
			ignoreBounds:true,
		},
		fontSize:12,
		pieHole: 0.4,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart = new google.visualization.PieChart(document.getElementById('donut_chart'));
	chart.draw(chart_data, options);

	img_chart(2, chart_data, options);
}
/*도넛차트 만들기 끝*/
/*파이차트 만드기 시작*/
function pie_chart(data){
	var txt_sch_kind = $('#txt_sch_kind').val();
	var sch_kind = $('#sch_kind').val();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	array_data[0] = ['구분', '상담건수'];
	var length = Object.keys(data.data_tot[0]).length;
	length = (length/2)-1;
	for(var i = 1; i<=length;  i++) {
		value = Number(data.data_tot[0]['t'+i]);
		subject = data.data_tot[0]['s'+i];	
		if(subject == '총계') break;
		array_data[i] = [subject, value];
	}
    var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '['+txt_sch_kind+'] 3D Pie Chart',
		width:1620,
    	height:700,
    	chartArea:{
		    left:100,
		    right:100,
		    top: 50,
		    bottom:50,
		    width: '100%',
		    height: '100%',
		},
		legend:{
			position:'right',
			maxLines:3,
		},
		tooltip:{
			ignoreBounds:true,
		},
		fontSize:12,
		is3D:true,
		sliceVisibilityThreshold: 0 //대비 값 차이가 크거나 값이 0인 경우 차트에서 숨겨짐 방지
		,pieSliceText: 'value-and-percentage' // 퍼센트 대신 값이 출력되게 함
	};

	var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
	chart.draw(chart_data, options);

	img_chart(3, chart_data, options);
}
/*파이차트 만들기 끝*/
/*라인차트 만들기 시작*/
function line_chart(data){
	var txt_sch_kind = $('#txt_sch_kind').val();
	var sch_kind = $('#sch_kind').val();
	var subject = new Array();
	var value = new Array();
	var array_data = new Array();

	array_data[0] = ['구분', '상담건수', {role:'annotation'} , {role:'style'}];
	var length = Object.keys(data.data_tot[0]).length;
	length = (length/2)-1;
	for(var i = 1; i<=length;  i++) {
		value = Number(data.data_tot[0]['t'+i]);
		subject = data.data_tot[0]['s'+i];
		/*if(sch_kind == 6 || sch_kind == 7){
			subject = subject.substring(0,7);
			subject += '..';
		}	*/
		if(subject == '총계') break;
		array_data[i] = [subject, value, value, 'red'];
	}
    var chart_data = new google.visualization.arrayToDataTable(array_data);

	var options = {
		title: '['+txt_sch_kind+'] Line Chart',
		width:1620,
    	height:700,
    	chartArea:{
		    left:100,
		    right:100,
		    top: 100,
		    bottom:100,
		    width: '100%',
		    height: '400',
		},
		fontSize:12,
		axisTitlesPosition:'in',
		legend: { position: 'none'},
		is3D:true,
	};

	var chart = new google.visualization.LineChart(document.getElementById('line_chart'));
	chart.draw(chart_data, options);

	img_chart(4, chart_data, options);
}
/*라인차트 만드기 끝*/
/*이미지 차트 만들기 시작*/
function img_chart(sel_row, chart_data, options){

    var chart_div = document.getElementById('chart_div'+sel_row);
    if(sel_row == 0){var chart = new google.visualization.ColumnChart(chart_div);}
    else if(sel_row == 1){var chart = new google.visualization.BarChart(chart_div);}
    else if(sel_row == 2){var chart = new google.visualization.PieChart(chart_div);}
    else if(sel_row == 3){var chart = new google.visualization.PieChart(chart_div);}
    else if(sel_row == 4){var chart = new google.visualization.LineChart(chart_div);}

    google.visualization.events.addListener(chart, 'ready', function () {
      chart_div.innerHTML = '<img src="' + chart.getImageURI() + '">';
    });

    chart.draw(chart_data, options);
}
/*이미지 차트 만들기 끝*/

</script>

<?php
include_once './inc/inc_menu.php';
?>


		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">상담내역 통계</h2>
				<span class="location">홈 > 상담내역 통계 > <em>통계</em></span>
				<input type="hidden" id="begin" value="<?php echo $date['begin']?>">
				<input type="hidden" id="end" value="<?php echo $date['end']?>">
				<input type="hidden" id="sch_asso_code" value="<?php echo $date['sch_asso_code']?>">
				<input type="hidden" id="sch_s_code" value="<?php echo $date['sch_s_code']?>">
				<input type="hidden" id="sch_csl_proc_rst" value="<?php echo $date['sch_csl_proc_rst']?>">
			</div>
<?php
include_once './inc/inc_statistic_info.php';
?>
			<ul class="tab">
  			<li><a href="#" class="go_page">상담사례</a></li>
				<li><a href="#" class="go_page selected" style="text-decoration:none">기본통계</a></li>
				<li><a href="#" class="go_page">교차통계</a></li>
				<li><a href="#" class="go_page">임금근로시간통계</a></li>
				<li><a href="#" class="go_page">소속별통계</a></li>
				<li><a href="#" class="go_page">월별통계</a></li>
				<li><a href="#" class="go_page">시계열통계</a></li>
			</ul>
			<div class="cont_area" style="margin-top:0">
				<form name="frmSearch" id="frmSearch" method="post">
				<input type="hidden" id="sch_kind" name="sch_kind" value="0">
				<div class="con_text">					
					<label for="search_date_begin" class="l_title">상담일</label>
					<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" readonly="readonly" style="width:90px; margin-left: -20px"> ~ 
					<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" readonly="readonly" style="width:90px">
					<label for="sel_year" class="l_title" style="margin-left:10px;">연월별</label>
					<select id="sel_year" name="sel_year" style="margin-left:-20px;">
					<option value="">연도</option>
					</select>
					<select id="sel_month" name="sel_month">
						<option value="">전체</option>
						<option value="01">1월</option>
						<option value="02">2월</option>
						<option value="03">3월</option>
						<option value="04">4월</option>
						<option value="05">5월</option>
						<option value="06">6월</option>
						<option value="07">7월</option>
						<option value="08">8월</option>
						<option value="09">9월</option>
						<option value="10">10월</option>
						<option value="11">11월</option>
						<option value="12">12월</option>
					</select>
					<label for="sel_year" class="l_title" style="margin-left:10px;">기간별</label>
					<button type="button" id="btnToday" class="buttonS bGray"  style="margin-left:-20px;">오늘</button>
					<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
					<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
					<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button><br>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_asso_code" class="buttonS bGray marginT03">소속</button>
						<button type="button" id="btnInitAssoCode" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="asso_code" id="asso_code" value="<?php echo $data['asso_code']?>"></input>
						<input type="hidden" id="asso_code_all" value="<?php echo $data['asso_code']?>"></input><!-- 디버깅: 초기화버튼 클릭시 세팅용 초기데이터 -->
						<input type="hidden" name="asso_code_name"></input>
						<span id="asso_code_name" style="margin-left:27px;"></span>
					</div>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_s_code" class="buttonS bGray marginT03">상담방법</button>
						<button type="button" id="btnInitSCode" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="s_code" id="s_code"></input>
						<input type="hidden" name="s_code_name"></input>
						<span id="s_code_name" style="margin-left:5px;"></span>
					</div>
					<span class="divice"></span>
					<div>
						<button type="button" id="btn_csl_proc_rst" class="buttonS bGray marginT03">처리결과</button>
						<button type="button" id="btnInitCslProcRst" class="buttonS bWhite marginT03">초기화</button>
						<input type="hidden" name="csl_proc_rst" id="csl_proc_rst"></input>
						<input type="hidden" name="csl_proc_rst_name"></input>
						<span id="csl_proc_rst_name" style="margin-left:5px;"></span>
					</div>
					<span class="divice"></span>
					<div class="stab_button">
						<label for="sch_kind" class="stab_title">통계 유형</label>
						<input type="hidden" id="txt_sch_kind" name="txt_sch_kind" value="상담방법">
						<button type="button" id="btn_kind_0" class="stab_btn active">상담방법</button>
						<button type="button" id="btn_kind_1" class="stab_btn">소속</button>
						<button type="button" id="btn_kind_2" class="stab_btn">성별</button>
						<button type="button" id="btn_kind_3" class="stab_btn">연령대</button>
						<button type="button" id="btn_kind_4" class="stab_btn">거주지</button>
						<button type="button" id="btn_kind_5" class="stab_btn">회사소재지</button>
						<button type="button" id="btn_kind_6" class="stab_btn">직종</button>
						<button type="button" id="btn_kind_7" class="stab_btn">업종</button>
						<button type="button" id="btn_kind_8" class="stab_btn">고용형태</button>
						<button type="button" id="btn_kind_9" class="stab_btn">근로자수</button>
						<button type="button" id="btn_kind_10" class="stab_btn">근로계약서</button>
						<button type="button" id="btn_kind_11" class="stab_btn">4대보험</button>
						<button type="button" id="btn_kind_12" class="stab_btn">상담유형</button>
						<button type="button" id="btn_kind_13" class="stab_btn">처리결과</button>
						<button type="button" id="btn_kind_14" class="stab_btn">상담동기</button>
					</div>
					<div id="infoLabel" class="floatL marginT10" style="color:#f37000; display: none">
					* 상담유형은 중복선택이 가능하여 데이터가 중복될 수 있습니다. 총계표기 : 중복데이터(실데이터)</div>
					<div class="textR marginT10">
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>
			<!-- //컨텐츠 -->

  		<div class="list_select" style="margin-bottom:10px;">
					<button type="button" id="downExcel" class="buttonM bGray floatR">엑셀다운로드</button>
					<button type="button" id="printlist" class="buttonM bGray floatR">표 인쇄</button>
					<button type="button" id="view_chart" class="buttonM bGray floatR">통계차트 보기</button>
				</div>

				<!-- list -->
				<div style="overflow:auto;">
					<select id="sel_chart" name="sel_chart" style="display: none">
						<option value=0>Column</option>
						<option value=1>Bar</option>
						<option value=2>Donut</option>
						<option value=3>Pie</option>
						<option value=4>Line</option>
					</select>
					<button type="button" id="downchart" class="buttonS bBlack" style="display: none;">차트다운로드</button>
					<button type="button" id="printchart" class="buttonS bBlack" style="display: none">차트인쇄</button>
					<div id="column_chart" class="chart" style="width:100%; height: 700px; display: none;"></div>
					<div id="bar_chart" class="chart" style="width:100%; height: 700px; display: none;"></div>
					<div id="donut_chart" class="chart" style="width: 100%; height: 700px; display: none;"></div>
					<div id="pie_chart" class="chart" style="width: 100%; height: 700px; display: none;"></div>
					<div id="line_chart" class="chart" style="width: 100%; height: 700px; display: none;"></div>
					<div id="list" style="width:100%;"></div>
				</div>
				<div id="chart_div" style="display: none"></div>

				<a href="#" id="go_to_top" title="Go to top"></a>

			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->		
		
<?php
include_once './inc/inc_footer.php';
?>
