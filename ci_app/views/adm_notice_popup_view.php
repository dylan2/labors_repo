<script>

var seq = "<?php echo $data['seq'];?>";

$(document).ready(function(){
	var this_id = 'layer_'+ ($('.css_popup').length-1);

	if("<?php echo $data['seq'];?>") {
		
		$('#'+ this_id +' .css_popup').hide();

		var url = '/?c=board&m=get_notice_popup_data';
		var rsc = 'seq=<?php echo $data['seq'];?>';
		var fnSuccess = function(data) {
			// console.log(data[0]);
			if(data && data[0]) {
				var data = data[0];
				$('#'+ this_id +' .css_pop_header>h2').text(data.title);
				$('#'+ this_id +' .css_notice_duration').text(data.notice_dt_begin.split(' ')[0] +' ~ '+ data.notice_dt_end.split(' ')[0]);
				$('#'+ this_id +' .css_notice_writer').text(data.oper_name);
				var str_file_names_org = '';
				var str_file_names_real = data.file_name;
				if(str_file_names_org.indexOf(',') != -1) {
					var arr_file_name_org = str_file_names_org.split(',');
					var arr_file_name_real = str_file_names_real.split(',');
					for(var j=0; j<arr_file_name_org.length; j++) {
						if(str_file_names_org != '') str_file_names_org += '<br>';
						str_file_names_org += '<a href="#" class="cssDownload" '+
							'data-role-real="'+ arr_file_name_real[j] +'" '+
							'data-role-org="'+ arr_file_name_org[j] +'">'+ arr_file_name_org[j] +'</a>';
					}
				}
				else {
					str_file_names_org = '<a href="#" class="cssDownload" style="text-decoration:underline;color:#787878;" '+
						'data-role-real="'+ data.file_name +'" '+
						'data-role-org="'+ data.file_name_org +'">'+ data.file_name_org +'</a>';
				}
				$('#'+ this_id +' .css_notice_files').html(str_file_names_org);
				$('#'+ this_id +' .css_notice_contents').html(data.content.replace(/\n/g, "<br/>"));
			}
			$('#'+ this_id +' .css_popup').show();
		};
		var fnFailure = function(data) {
			$('#'+ this_id +' .css_popup').show();
		};
		req_ajax(url, rsc, fnSuccess, fnFailure);
	}
	else {
		closeLayerPopup();
	}


	// file download
	$('#'+ this_id +' .css_notice_files').bind('click', function(e){
		e.preventDefault();
		if(e.target.tagName !== 'A') {
			return;
		}

		var real = $(e.target).attr('data-role-real');
		var org = $(e.target).attr('data-role-org');
		// request
		$.ajax({
			url: '/?c=board&m=chk_download'
			,data: {real:real, org:org}
			,cache: false
			,async: false
			,method: 'post'
			,dataType: 'json'
			,success: function(data) {
				if(data.rst == 'fail') {
					alert(CFG_MSG[CFG_LOCALE]['warn_board_01']);
				}
				else {
					window.location.href = "/?c=board&m=download&real="+ real +"&org="+ org;
				}
			}
			,error: function(data) {
				if(is_local) objectPrint(data);
				
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			}
		});
	});

	// 팝업 닫기
	$('#'+ this_id +' .cssCloseNotiPop').click(function(e){
		// 하루동안 숨기기 체크한 경우
		if($('#'+ this_id +' .cssHideOneDay').prop('checked')) {
			$.cookie('labors_hide_popup_<?php echo $data['seq'];?>', 1, {expires:1}); // other arguments - domain, secure : if https set true
		}
		else {
			$.removeCookie('labors_hide_popup_<?php echo $data['seq'];?>');
		}
		closeLayerPopup();
	});

});

</script>


    <div id="popup" class="css_popup">
        <header id="pop_header" class="css_pop_header">
			<h2 style="width:85%;"></h2>			
        </header>
		<form name="frmFormPop" method="post">
		<div id="popup_contents">
			<div style="height:356px;overflow-y:auto;overflow-x:hidden;">
				<table class="tInsert">
					<caption>팝업공지 내용입니다.</caption>
					<colgroup>
						<col style="width:20%">
						<col style="width:80%">
					</colgroup>
					<tr>
						<th>공지기간</th>
						<td><div class="css_notice_duration"></div></td>
					</tr>
					<tr>
						<th>작성자</th>
						<td><div class="css_notice_writer"></div></td>
					</tr>
					<tr>
						<th>첨부파일</th>
						<td><div class="css_notice_files"></div></td>
					</tr>
					<tr>
						<th>내용</th>
						<td style="line-height:inherit;"><div class="css_notice_contents"></div></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="btn_set" style="width:100%;background-color:#006d76;position:absolute;bottom:0px;">
			<input type="checkbox" class="cssHideOneDay" id="hideOneDay_<?php echo $data['seq'];?>"><label for="hideOneDay_<?php echo $data['seq'];?>" style="color:white;">하루동안 숨기기</label>
			<button type="button" class="buttonS bGray cssCloseNotiPop" style="margin:0 11px 0 -59px;float:right;">닫기</button>
		</div>
		</form>
    </div>
