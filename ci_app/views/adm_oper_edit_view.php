<?php
include_once "./inc/inc_header.php";
?>


<script>
var oper_id = '<?php echo $res['oper_id']; ?>';

var kind = '<?php echo $res['kind']; ?>';
if(!kind) kind = 'add';

var is_id_check = false;
	
$('document').ready(function(){

	// 운영자 구분 : 옴부즈만, 자치구공무원
	var oper_kind_gu = {k3 : "<?php echo CFG_OPERATOR_KIND_CODE_OK3;?>", k4 : "<?php echo CFG_OPERATOR_KIND_CODE_OK4;?>" };


	// oper_kind
	var oper_kind = '<?php echo $res['oper_kind']; ?>';
	if(oper_kind) {
		$('select[name=oper_kind] option[value="'+ oper_kind +'"]').prop('selected', true);
	}

	// 운영자구분 : 옴부즈만, 자치구공무원인 경우 추가 데이터 select 생성
	create_select_oper_kind_sub();
	//
	if(oper_kind != oper_kind_gu.k3 && oper_kind != oper_kind_gu.k4) {
		$('select[name=oper_kind_sub]').hide();
	}


	// 등록, 리스트 버튼 이벤트 핸들러
	$('button.btnClass').click(function(e){
		e.preventDefault();
		// 등록
		if($(this).attr('id') == 'btnSubmit') {
			// validation
			// 소속
			if($("#sltS_Code option:selected").val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_auth_05']);
				$('#sltS_Code').focus();
				return false;
			}
			// 권한
			if($("#sltGrpId option:selected").val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_auth_03']);
				$('#sltGrpId').focus();
				return false;
			}
			// 이름
			if($('#txtOperName').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_subs_01']);
				$('#txtOperName').focus();
				return false;
			}
			// 운영자구분 - 소속자치구
			if($('select[name=oper_kind] option:selected').val() == oper_kind_gu.k3 
				|| $('select[name=oper_kind] option:selected').val() == oper_kind_gu.k4) {
				if($('select[name=oper_kind_sub] option:selected').val() == '') {
					alert(CFG_MSG[CFG_LOCALE]['info_subs_10']);
					$('select[name=oper_kind_sub]').focus();
					return false;
				}
			}
			// 아이디
			if($('#txtId').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_login_01']);
				$('#txtId').focus();
				return false;
			}
			// 등록인 경우
			if(kind == 'add') {
				// 아이디 중복 확인 여부
				if(!is_id_check || confirm_id != $('#txtId').val()) {
					alert(CFG_MSG[CFG_LOCALE]['info_subs_02']);
					$('#txtId').focus();
					is_id_check = false;
					return false;
				}
			}
			// 비번
			if(kind == 'add' || is_master != 1) {
				if($('#txtPwd').val() == '') {
					alert(CFG_MSG[CFG_LOCALE]['info_login_02']);
					$('#txtPwd').focus();
					return false;
				}
			}
			// 등록인 경우
			if(kind == 'add') {
				// 비번확인
				if($('#txtPwdChk').val() == '') {
					alert(CFG_MSG[CFG_LOCALE]['info_subs_03']);
					$('#txtPwdChk').focus();
					return false;
				}
				// 비번 일치 확인
				else {
					if($('#txtPwd').val() !== $('#txtPwdChk').val()) {
						alert(CFG_MSG[CFG_LOCALE]['info_subs_09']);
						$('#txtPwdChk').focus();
						return false;
					}
				}
			}
			// 휴대폰
			if($('#sltHP02').val() == '' || $('#sltHP03').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_subs_04']);
				if($('#sltHP02').val() == '') {
					$('#sltHP02').focus();
				}
				else {
					$('#sltHP03').focus();
				}
				return false;
			}
			// 연락처
			if($('#sltTel02').val() == '' || $('#sltTel03').val() == '') {
				alert(CFG_MSG[CFG_LOCALE]['info_subs_05']);
				if($('#sltTel02').val() == '') {
					$('#sltTel02').focus();
				}
				else {
					$('#sltTel03').focus();
				}
				return false;
			}
			// 비고 - 입력 길이 : 100자까지
			if($('textarea[name=txaEtc]').val() != '' && $('textarea[name=txaEtc]').val().length > 100) {
				alert(CFG_MSG[CFG_LOCALE]['info_subs_11']);
				$('textarea[name=txaEtc]').focus();
				return false;
			}
			
			if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
				// request
				$.ajax({
					url: '/?c=oper&m='+ kind
					,data: $('#frmForm').serialize()
					,cache: false
					,async: false
					,method: 'post'
					,dataType: 'json'
					,success: function(data) {
						if(data.rst == 'succ') {
							alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
							check_auth_redirect($('#'+ sel_menu).find('li>a').first());
						}
						else {
							if(data.msg == 'not_match_password') {
								alert(CFG_MSG[CFG_LOCALE]['info_subs_08']);
								$('#txtPwd').focus();
							}
							else {
								alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
							}
						}
					}
					,error: function(data) {
						if(is_local) objectPrint(data);
						
						var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
						if(data && data.msg) msg += '[' + data.msg +']';
						alert(msg);
					}
				});
			}
		}
		// 리스트
		else {
			check_auth_redirect($('#'+ sel_menu).find('li>a').first());
		}
	});
	
	// 수정인 경우, 아이디, 비번확인을 숨긴다.
	if(kind == 'edit') {
		$('#txtId').css('width', 0).css('display', 'none');
		$('#edit_node_id').text($('#txtId').val());
		$('#chkExistId').css('width', 0).css('display', 'none');
		// 비번확인
		$('#pwd_chk_label, #txtPwdChk').css('width', 0).css('display', 'none');
		//
		$('select[name=oper_kind_sub] option[value="<?php echo $res['oper_kind_sub'];?>"]').prop('selected', true);
		// 아이디 길이 안내 문구
		$('#chkExistIdInfo').hide();
	}
	
	// id중복확인
	var confirm_id = '';
	$('#chkExistId').click(function(e){
		e.preventDefault();
		var user_id = $('#txtId').val();
		if(user_id == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_login_01']);
			$('#txtId').focus();
			return false;
		}
		is_id_check = true;
		// request
		$.ajax({
			url: '/?c=oper&m=id_check'
			,data: {oid: user_id }
			,cache: false
			,async: false
			,method: 'post'
			,dataType: 'json'
			,success: function(data) {
				if(data.rst == 'succ') {
					confirm_id = user_id;
					alert(CFG_MSG[CFG_LOCALE]['info_subs_07']);
				}
				else {
					alert(CFG_MSG[CFG_LOCALE]['info_subs_06']);
				}
			}
			,error: function(data) {
				if(is_local) objectPrint(data);
				
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			}
		});
	});

	// master grp
	var grp_id = "<?php echo $res['oper_auth_grp_id'];?>";
	var mst_id = "<?php echo CFG_AUTH_CODE_ADM_MASTER;?>";
	if(grp_id == mst_id) {
		$('#sltGrpId').attr('disabled', true).css('background-color', '#efefef');
		$('input[name=rdoUse]').attr('disabled', true);
		// input tag 생성
		$('#frmForm').append('<input type="hidden" name="sltGrpId" value="'+ mst_id +'"></input>');
		$('#frmForm').append('<input type="hidden" name="rdoUse" value=1></input>');
	}

	// 
	/*
	$('#sltGrpId').change(function(){
		if($(this).val() == mst_id) {
			alert();
		}
	});
	*/

	// form value 적용

	// hp
	var hp = '<?php echo $res['hp']; ?>';
	if(hp) {
		var arr_hp = hp.split('-');
		$("#sltHP01 > option[value="+ arr_hp[0] +"]").prop("selected", true);
		// 해당 값이 없으면 추가
		if(!$("#sltHP01 option:selected").val()) {
			$("#sltHP01").append("<option value='"+ arr_hp[0] +"'>"+ arr_hp[0] +"</option>");
		}
		$("#sltHP02").val(arr_hp[1]);
		$("#sltHP03").val(arr_hp[2]);
	}
	
	// tel
	var tel = '<?php echo $res['tel']; ?>';
	if(tel) {
		var arr_tel = tel.split('-');
		$("#sltTel01 > option[value="+ arr_tel[0] +"]").prop("selected", true);
		$("#sltTel02").val(arr_tel[1]);
		$("#sltTel03").val(arr_tel[2]);
	}


	// 운영자구분 : 옴부즈만 선택시 추가 데이터 표시(자치구별 공무원 계정)
	$('select[name=oper_kind]').change(function(){
		$('select[name=oper_kind_sub] option[value=""]').prop('selected', true);
		if($(this).val() == oper_kind_gu.k3 || $(this).val() == oper_kind_gu.k4) {
			$('select[name=oper_kind_sub]').show();
		}
		else {
			$('select[name=oper_kind_sub]').hide();
		}
	});

});


// 운영자구분 : 옴부즈만, 자치구공무원인 경우 추가 데이터 select 생성
function create_select_oper_kind_sub() {
	var _gu = "강남구|G01,강동구|G02,강북구|G03,강서구|G04,관악구|G05,광진구|G06,구로구|G07,금천구|G08,노원구|G09,도봉구|G10,동대문구|G11,동작구|G12,마포구|G13,서대문구|G14,서초구|G15,성동구|G16,성북구|G17,송파구|G18,양천구|G19,영등포구|G20,용산구|G21,은평구|G22,종로구|G23,중구|G24,중랑구|G25";
	var arr_gu = _gu.split(',');
	$('select[name=oper_kind_sub]').append('<option value="">소속 자치구 선택</option>');
	$.each(arr_gu, function(i, d){
		data = d.split('|');
		$('select[name=oper_kind_sub]').append('<option value="'+ data[1] +'">'+ data[0] +'</option>');
	});
}
</script>

<?php
include_once './inc/inc_menu.php';
?>

		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">운영자등록</h2>
				<span class="location">홈 > 운영자관리 > <em>운영자등록</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post">
				<input type="hidden" name="is_master" value="<?php echo $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER)?>"></input>
				<table class="tInsert">
					<caption>
						운영자등록 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:36%">
						<col style="width:14%">
						<col style="width:36%">
					</colgroup>
					<tr>
						<th>소속 <span class="fOrange">*</span></th>
						<td>
							<select id="sltS_Code" name="sltS_Code" class="styled" style="width:50%">
								<option value="">선택</option>
								<?php
								foreach($res['asso_code']['data'] as $rs) {
									echo '<option value="'. $rs->s_code .'" '. ($res['s_code'] == $rs->s_code ? 'selected' : '') .'>'. $rs->code_name .'</option>';
								}
								?>
							</select>
						</td>	
						<th>권한 <span class="fOrange">*</span></th>
						<td>
							<select id="sltGrpId" name="sltGrpId" class="styled" style="width:50%">
								<option value="">선택</option>
								<?php
								foreach($res['grp_data']['data'] as $rs) {
									// 신규등록인 경우, 마스터권한 제거
									if($res['kind'] == 'add') {
										if($rs->oper_auth_grp_id != CFG_AUTH_CODE_ADM_MASTER) {
											echo '<option value="'. $rs->oper_auth_grp_id .'" '. ($res['oper_auth_grp_id'] == $rs->oper_auth_grp_id ? 'selected' : '') .'>'. $rs->oper_auth_name .'</option>';
										}
									}
									else {
										if($res['oper_auth_grp_id'] != CFG_AUTH_CODE_ADM_MASTER) {
											if($rs->oper_auth_grp_id != CFG_AUTH_CODE_ADM_MASTER) {
												echo '<option value="'. $rs->oper_auth_grp_id .'" '. ($res['oper_auth_grp_id'] == $rs->oper_auth_grp_id ? 'selected' : '') .'>'. $rs->oper_auth_name .'</option>';
											}
										}
									}
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th>성명 <span class="fOrange">*</span></th>
						<td><input type="text" name="txtOperName" id="txtOperName" value="<?php echo ($res['oper_name_1']); ?>" maxlength="20"> *20자리까지 입력 가능</td>
						<th>운영자구분 <span class="fOrange">*</span></th>
						<td>
							<select name="oper_kind">
								<option value="<?php echo CFG_OPERATOR_KIND_CODE_OK1;?>">기본운영자</option>
								<option value="<?php echo CFG_OPERATOR_KIND_CODE_OK2;?>">서울시</option>
								<option value="<?php echo CFG_OPERATOR_KIND_CODE_OK3;?>">옴부즈만</option>
								<option value="<?php echo CFG_OPERATOR_KIND_CODE_OK4;?>">자치구공무원</option>
							</select>
							<select name="oper_kind_sub"></select>
						</td>
					</tr>
					<tr>
						<th>아이디 <span class="fOrange">*</span></th>
						<td colspan="3"><input type="text" name="txtId" id="txtId" value="<?php echo ($res['oper_id']); ?>" maxlength="15"><span id="edit_node_id"></span>
							<input type="button" id="chkExistId" class="buttonM bGray btnClass" value="중복확인"> <span id="chkExistIdInfo">*15자리까지 입력 가능</span>
						</td>
					</tr>
					<tr>
						<th>비밀번호 <span class="fOrange">*</span></th>
						<td>
							<input type="password" name="txtPwd" id="txtPwd" maxlength="20">
							<?php

							if($this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER) == 1) {
								echo "<button class='buttonS bRed' id='btnChgOperPwd' data-tid='". $res['oper_id'] ."'>비밀번호변경</button>";
							}

							?> *20자리까지 입력 가능
						</td>	
						<th><span  id="pwd_chk_label">비밀번호확인 <span class="fOrange">*</span></span></th>
						<td><input type="password" name="txtPwdChk" id="txtPwdChk"></td>	
					</tr>
					<tr>
						<th>휴대폰 <span class="fOrange">*</span></th>
						<td>
							<select name="sltHP01" id="sltHP01" class="styled">
								<option value="010" selected>010</option>
								<option value="011">011</option>
								<option value="017">017</option>
							</select> - <input type="text" name="sltHP02" id="sltHP02" style="width:40px" maxlength="4"> - 
							<input type="text" name="sltHP03" id="sltHP03" style="width:40px" maxlength="4">
						</td>	
						<th>연락처 <span class="fOrange">*</span></th>
						<td>
							<select name="sltTel01" id="sltTel01" class="styled">
								<option value="02" selected>02</option>
								<option value="031">031</option>
								<option value="032">032</option>
								<option value="033">033</option>
								<option value="042">042</option>
								<option value="043">043</option>
								<option value="051">051</option>
								<option value="052">052</option>
								<option value="053">053</option>
								<option value="054">054</option>
								<option value="055">055</option>
								<option value="061">061</option>
								<option value="062">062</option>
								<option value="063">063</option>
								<option value="070">070</option>
							</select> - <input type="text" name="sltTel02" id="sltTel02" style="width:40px" maxlength="4"> - 
							<input type="text" name="sltTel03" id="sltTel03" style="width:40px" maxlength="4">
						</td>
					</tr>
					<tr>
						<th>이메일</th>
						<td colspan="3"><input type="text" name="txtEmail" id="txtEmail" value="<?php echo ($res['email']); ?>" style="width:30%" maxlength="50"></td>						
					</tr>	
					<tr>
						<th>사용여부 <span class="fOrange">*</span></th>
						<td colspan="3">							
							<input type="radio" name="rdoUse" id="rdoUse" value="1" <?php echo ($res['use_yn'] == 1 ? 'checked' : ''); ?> checked ><label for="rdoUse">사용</label>
							<input type="radio" name="rdoUse" id="rdoNotUse" value="0" <?php echo ($res['use_yn'] == 0 ? 'checked' : ''); ?> ><label for="rdoNotUse">사용중지</label>
						</td>										
					</tr>
					<tr>
						<th>비고</th>
						<td colspan="3">
							<textarea name="txaEtc" id="txaEtc" style="width:90%;height:80px"><?php echo ($res['etc']); ?></textarea>
							<br>*100자까지 입력 가능
						</td>						
					</tr>
				</table>
				<div class="marginT10 textR">
					<button type="button" id="btnSubmit" class="buttonM bSteelBlue btnClass">등록</button>
					<button type="button" id="btnList" class="buttonM bGray btnClass">목록</button>
				</div>
				</form>
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		
		
<?php
include_once './inc/inc_footer.php';
?>
