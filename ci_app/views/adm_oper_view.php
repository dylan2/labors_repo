<?php
include_once "./inc/inc_header.php";
?>

<script>
$(document).ready(function(){
	// 등록 버튼 이벤트 핸들러
	$('#btnAdd').click(function(e){
		$.removeCookie("laborsSearchDataOper");
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), 'add', '');
	});
	
	// 전송버튼 이벤트 핸들러
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		$.cookie("laborsSearchDataOper", $('form[name=frmSearch]').serialize());
		get_list(_page);
	});
	
	// 검색 - 검색어 엔터 처리
	$("#keyword").keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			$("#btnSubmit").click();
		}
	});
	
	// 사용.사용중지, 삭제 버튼 이벤트 핸들러
	$('#btnUse,#btnNotUse,#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var oper_id = '',
				oper_ids = '', 
				is_inc_self = false;
			$('input[name=chk_item]:checked').each(function(i){
				if(oper_ids != '') oper_ids +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				oper_id = $(this).attr('data-role-id');
				oper_ids += oper_id;
				if(oper_id == '<?php echo $oper_id;?>') {
					is_inc_self = true;
				}
			});

			// 자신의 계정이 포함되어 있으면 return
			if(is_inc_self) {
				alert(CFG_MSG[CFG_LOCALE]['err_oper_02']);
				return false;
			}
			
			var kind = 'using';
			if($(this).text() == '중지') {
				kind = 'not_using';
			}
			else if($(this).text() == '삭제') {
				kind = 'del_multi';
				if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
					return false;
				}
			}
			req_code_manage(kind, oper_ids);
		}
	});

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();

		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_end = '';
		}

		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});

	//연도 셀렉트박스
	var now = new Date();
	var year = now.getFullYear();
	for(var i=year; i>=2015; i--){
		$("#sel_year").append("<option value="+i+">"+i+"년</option>");
	}
	$('#sel_year').change(function(){
		/*var sel_year = $('#sel_year option:selected').text();
		sel_year =  sel_year.substring(0,4);*/
		$('#sel_month').val('all').prop('selected', true);

		selectEvent(sel_month);

	});

	// 검색어 채워넣기
	var schData = $.cookie("laborsSearchDataOper");
	// - 소속
	var sltAssoCd = getValueInGetStringByKey(schData, "sltS_Code");
	$('select[name=sltS_Code] option[value="'+ sltAssoCd +'"]').prop('selected', true);
	// - 권한
	var sltGrpId = getValueInGetStringByKey(schData, "sltGrpId");
	$('select[name=sltGrpId] option[value="'+ sltGrpId +'"]').prop('selected', true);
	// - 등록일
	var schDateBegin = getValueInGetStringByKey(schData, "search_date_begin");
	$('input[name=search_date_begin]').val(schDateBegin);
	var schDateEnd = getValueInGetStringByKey(schData, "search_date_end");
	$('input[name=search_date_end]').val(schDateEnd);
	// - 등록일:년월 selectbox
	var schSelYear = getValueInGetStringByKey(schData, "sel_year");
	$('select[name=sel_year] option[value="'+ schSelYear +'"]').prop('selected', true);
	var schSelMonth = getValueInGetStringByKey(schData, "sel_month");
	$('select[name=sel_month] option[value="'+ schSelMonth +'"]').prop('selected', true);
	// - 사용여부
	var rdoUse = getValueInGetStringByKey(schData, "rdoUse");
	$('input:radio[name=rdoUse][value="'+ rdoUse +'"]').prop('checked', true);
	// - 검색대상
	var schTarget = getValueInGetStringByKey(schData, "search_target");
	$('select[name=search_target] option[value="'+ schTarget +'"]').prop('selected', true);
	$('select[name=search_target]').change();
	// - 검색어
	var schKeyword = getValueInGetStringByKey(schData, "keyword");
	$('input[name=keyword]').val(schKeyword);
	
	// list
	get_list(_page);
});

//검색 - 월별 조회
function selectEvent(select_month){
	var year = $('#sel_year').val();
	var now = new Date();
	if(year == '') year = now.getFullYear();
	var date_begin = '';
	var date_end = '';
	if(select_month.value == 'all' || select_month == 'all'){
		date_begin = year + "-01-01";
		date_end = year + "-12-31";
	}
	else{
		date_begin = year + "-" + select_month.value + "-01";
		if(select_month.value == "04" || select_month.value == "06" || select_month.value == "09" || select_month.value == "11"){
			date_end = year + "-"+ select_month.value+"-30";
		}
		else if(select_month.value == "02"){
			if(year % 4 == 0){
				date_end = year + "-"+ select_month.value + "-29";
			}
			else{
				date_end = year + "-"+ select_month.value+"-28";
			}
		}
		else{
			date_end = year + "-"+ select_month.value+"-31";
		}
	}
	$('#search_date_begin').val(date_begin);
	$('#search_date_end').val(date_end);
}

/**
 * 리스트를 동적생성하면 이벤트를 다시 걸어줘야 한다.
 */
function addManagerEventListener() {
	// 관리버튼 클릭 이벤트
	$('button.ai_cmd').click(function(e){
		e.preventDefault();
		var oper_id = $(this).attr('data-role-id');
		var cmd = $(this).text();
		if(cmd == '삭제') {
			if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				req_code_manage('del', oper_id);
			}
		}
		else {
			var kind = 'view';
			if(cmd == '수정') kind = 'edit';
			check_auth_redirect($('#'+ sel_menu).find('li>a').last(), kind, oper_id);
		}
	});
	
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		var status = $(this).prop('checked');
		$("input[name=chk_item]:checkbox").each(function(i) {
			$(this).prop("checked", status);
		});
	});

	// 자신의 계정은 삭제버튼 비활성화
	$('button.ai_cmd_del').each(function(){
		if($(this).attr('data-role-id') == '<?php echo $oper_id;?>') {
			$(this).attr('disabled', true).css('background-color', '#e0e0e0');
		}
	});
}


/**
 * 삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, tid) {
	var url = '/?c=oper&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&oper_id='+ tid +'&is_master='+ is_master;
	var fn_succes = function(data) {
		// 등록한 게시물이 있는  운영자 삭제인 경우 메시지 처리
		if(type.indexOf('del') != -1) {
			if(data.rst == 'exist' && data.len && data.len > 0) {
				alert(CFG_MSG[CFG_LOCALE]['err_oper_01']);
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
				gen_list(data);
			}
		}
		else {
			if(type != 'view') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
				gen_list(data);
			}
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_list(data);
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}


/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// search date
	var date_b = $('#search_date_begin').val();
	var date_e = $('#search_date_end').val();
	if((date_b && date_e == '' ) || (date_b == '' && date_e)) {
		var now = new Date();
		var tmp_date = now.getFullYear() +'-'+ genTwoDigit(now.getMonth()+1) +'-'+ genTwoDigit(now.getDate());
		if(date_b == '') $('#search_date_begin').val(tmp_date);
		if(date_e == '') $('#search_date_end').val(tmp_date);
	}

	// page
	_page = page;
	
	var url = '/?c=oper&m=oper_list';
	var rsc = $('#frmSearch').serialize() +'&page='+_page +'&is_master='+ is_master;
	var fn_succes = function(data) {
		if(data) {
			_cfg_pagination.total_item = data.tot_cnt;
			_cfg_pagination.itemPerPage = 10;
			_cfg_pagination.currentPage = _page;
			_cfg_pagination.linkFunc = 'get_list';
			_pagination = new Pagination(_cfg_pagination);
			gen_list(data);
		}
		else {
			gen_list();
		}
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		gen_list();
	};
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	if(!_pagination) {
		_pagination = new Pagination(_cfg_pagination);
	}
	var html_b = '<table class="tList">';
	html_b += '<caption>운영자관리 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:4%">';
	html_b += '<col style="width:5%">';
	html_b += '<col style="width:15%">';
	html_b += '<col style="width:*">';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:11%">';
	html_b += '<col style="width:13%">';
	html_b += '<col style="width:7%">';
	html_b += '<col style="width:11%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th><input type="checkbox" class="imgM checkAll"></th>';
	html_b += '<th>번호</th>';
	html_b += '<th>소속</th>';
	html_b += '<th>권한</th>';
	html_b += '<th>아이디</th>';
	html_b += '<th>성명</th>';
	html_b += '<th>등록일</th>';
	html_b += '<th>사용여부</th>';
	html_b += '<th>관리</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			html_b += '<tr>';
			html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ data.data[index].oper_id +'"></td>';
			html_b += '  <td>'+ (no--) +'</td>';
			html_b += '  <td>'+ data.data[index].code_name +'</td>';
			html_b += '  <td>'+ data.data[index].oper_auth_name +'</td>';
			html_b += '  <td>'+ data.data[index].oper_id +'</td>';
			html_b += '  <td>'+ data.data[index].oper_name  +'</td>';
			html_b += '  <td>'+ data.data[index].reg_date +'</td>';
			var use_yn = '<span class="labelG">중지</span>';
			if(data.data[index].use_yn == 1) {
				use_yn = '<span class="labelB">사용</span>';
			}
			html_b += '  <td>'+ use_yn +'</td>';
			html_b += '  <td>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd" data-role-id="'+ data.data[index].oper_id +'">수정</button>';
			html_b += '    <button type="button" class="buttonS bGray ai_cmd ai_cmd_del" data-role-id="'+ data.data[index].oper_id +'">삭제</button>';
			html_b += '  </td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="9" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
	
	// 관리버튼 이벤트 등록
	addManagerEventListener();
}
</script>

<?php
include_once './inc/inc_menu.php';
?>

		<!--- //lnb 메뉴 area ---->			
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">운영자조회</h2>
				<span class="location">홈 > 운영자관리 > <em>운영자조회</em></span>
			</div>
			<div class="cont_area">
				<form name="frmSearch" id="frmSearch" method="post">
				<div class="con_text">	
					<label for="sltS_Code" class="l_title">소속</label>
					<select name="sltS_Code" id="sltS_Code" class="styled" style="width:20%">
						<option value="">전체</option>
						<?php
						foreach($res['asso_code']['data'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<label for="sltGrpId" class="l_title marginL20">권한</label>
					<select name="sltGrpId" id="sltGrpId" class="styled" style="width:20%">
						<option value="">전체</option>
						<?php
						foreach($res['grp_data']['data'] as $rs) {
							echo '<option value="'. base64_encode($rs->oper_auth_grp_id) .'" >'. $rs->oper_auth_name .'</option>';
						}
						?>
					</select>
					<span class="divice"></span>
					<label for="search_date_begin" class="l_title">등록일</label>
					<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" style="width:90px"> ~ 
					<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" style="width:90px">
					<label for="sel_year" class="l_title" style="margin-left:10px;">연월별</label>
					<select id="sel_year" name="sel_year" style="margin-left:-20px;">
					<option value="">연도</option>
					</select>
					<select id="sel_month" name="sel_month" onchange="javascript:selectEvent(this);">
					<option value="all">전체</option>
					<option value="01">1월</option>
					<option value="02">2월</option>
					<option value="03">3월</option>
					<option value="04">4월</option>
					<option value="05">5월</option>
					<option value="06">6월</option>
					<option value="07">7월</option>
					<option value="08">8월</option>
					<option value="09">9월</option>
					<option value="10">10월</option>
					<option value="11">11월</option>
					<option value="12">12월</option>
					<!-- <option value="all">전체</option> -->
					</select>
					<label for="sel_year" class="l_title" style="margin-left:10px;">기간별</label>
					<button type="button" id="btnToday" class="buttonS bGray"  style="margin-left:-20px;">오늘</button>
					<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
					<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
					<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button><br>
					<span class="divice"></span>
					<label for="board_rows" class="l_title">사용여부</label>
					<span style="padding:8px 0;display:inline-block">
						<input type="radio" name="rdoUse" id="rdoUse0" value="all" class="imgM" checked><label for="rdoUse0">전체</label>
						<input type="radio" name="rdoUse" id="rdoUse1" value="1" class="imgM"><label for="rdoUse1">사용</label>
						<input type="radio" name="rdoUse" id="rdoUse2" value="2" class="imgM"><label for="rdoUse2">사용중지</label>
					</span>
					<span class="divice"></span>
					<label for="search_target" class="l_title">검색어</label>
					<select id="search_target" name="search_target" class="styled">
						<option value="" selected>전체</option>
						<option value="S.code_name">소속</option>
						<option value="A.oper_auth_name">권한</option>
						<option value="O.oper_id">아이디</option>
						<option value="O.oper_name">성명</option>
					</select>
					<input type="text" id="keyword" name="keyword" style="width:60%">
					<span class="divice"></span>
					<div class="textR marginT10">
						<button type="button" id="btnSubmit" class="buttonS bBlack"><span class="icon_search"></span>검색</button>
					</div>
				</div>
				</form>

				<div class="list_no"></div>	
				
				<!-- list -->
				<div id="list"></div>
		
				<div class="floatC marginT10">					
					<button type="button" class="buttonM bGray floatL" id="btnUse">사용</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnNotUse">중지</button>
					<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>
					<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">등록</button>
				</div>
				
				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>
				
			</div>
			<!-- //컨텐츠 -->
		</div>
		<!-- //  contents_body  area -->
		
		
		
<?php
include_once './inc/inc_footer.php';
?>
