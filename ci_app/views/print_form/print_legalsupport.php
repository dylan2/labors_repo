<!doctype html>
<html lang="ko">
<head>
    <meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow" />
    <title>서울시통합노동상담관리시스템 관리자</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <link href="./css/admin-style.css" rel="stylesheet" type="text/css">

    <link href="./css/jquery-ui-1.11.2.min.css" rel="stylesheet" type="text/css">
	<link href="./css/dv_loading.css" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
        <script type="text/javascript" src="./js/html5shiv.js"></script>
        <script type="text/javascript" src="./js/html5shiv.printshiv.js"></script>
    <![endif]-->
    <script type="text/javascript" src="./js/jquery.1.9.1.min.js"></script>
    <script type="text/javascript" src="./js/jquery-ui.1.9.2.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(e){
    	window.print();
    });
    </script>

    <style>
	    body,table,div { font-size:20px !important; }
	    /*.tInsert th, .tInsert td { line-height:28px !important; padding:15px 10px !important; }*/
    </style>
</head>

<body style="width:650px;margin:0px 10px 0">				
	<div style="font-size:27px;padding-top:10px;padding-bottom:4px;border-bottom:1px solid #ddd">
		<b>권리구제지원 상담내용</b>
		<div style="margin:-20px 0 0 800px;"><?php echo get_date();?></div>
	</div>
	<div class="cont_area">
		<h3 class="sub_stit" style="font-size:20px;">신청정보</h3>
		<table class="tInsert">
			<caption>
				신청정보 입력 테이블 입니다.
			</caption>
			<colgroup>
				<col style="width:20%">
				<col style="width:30%">
				<col style="width:20%">
				<col style="width:30%">
			</colgroup>
			<tr>
				<th>접수번호</th>
				<td colspan="3"><?php echo $data['lh_code']; ?></td>
			</tr>
			<tr>
				<th>지원종류</th>
				<td colspan="3"><?php echo $data['sprt_kind_cd_nm'] .' ('. $data['sprt_kind_cd_etc'] .')'; ?></td>
			</tr>
			<tr>
				<th>지원승인일</th>
				<td colspan="3"><?php echo $data['lh_sprt_cfm_date'];?></td>
			</tr>
			<tr>
				<th>사건 1차유형</th>
				<td><?php echo $data['lh_kind_cd_nm'];?></td>
				<th>세부유형</th>
				<td><?php echo $data['lh_kind_sub_cd_nm'];?></td>
			</tr>
			<tr>
				<th>신청자</th>
				<!-- <td colspan="3"><?//php echo AesCtr::decrypt($data['lh_apply_nm'], CFG_ENCRYPT_KEY, 256);?></td> -->
				<td><?php echo $data['lh_apply_nm'];?></td>
				<th>성별</th>
				<td><?php echo $data['gender_cd_nm'];?></td>
			</tr>
			<tr>
				<th>연령대</th>
				<td><?php echo $data['ages'];?></td>
				<th>연령대 상세</th>
				<td><?php echo $data['ages_etc'];?></td>
			</tr>
			<tr>
				<th>거주지</th>
				<td colspan="3"><?php echo $data['lh_apply_addr'];?></td>
			</tr>
			<tr>
				<th>직종</th>
				<td><?php echo $data['work_kind_nm'];?></td>
				<th>직종 상세</th>
				<td><?php echo $data['work_kind_etc'];?></td>
			</tr>
			<tr>
				<th>업종</th>
				<td><?php echo $data['comp_kind_nm'];?></td>
				<th>업종 상세</th>
				<td><?php echo $data['comp_kind_etc'];?></td>
			</tr>
			<tr>
				<th>회사</th>
				<td colspan="3"><?php echo $data['lh_apply_comp_nm'];?></td>
			</tr>
			<tr>
				<th>회사소재지</th>
				<td colspan="3"><?php echo $data['lh_comp_addr'];?></td>
			</tr>
			<tr>
				<th>신청기관</th>
				<td><?php echo $data['apply_organ_cd_nm'];?></td>
				<th>대상기관(관할)</th>
				<td><?php echo $data['sprt_organ_cd_nm'] .' ('. $data['sprt_organ_cd_etc'] .')';?></td>
			</tr>
			<tr>
				<th>대리인</th>
				<td colspan="3"><b>소속</b> : <?php echo $data['lh_labor_asso_nm'] .', <b>이름</b> : '. $data['lh_labor_nm'];?></td>
			</tr>
		</table>	
		<h3 class="sub_stit" style="font-size:20px;">절차 진행 상황</h3>
		<table class="tInsert">
			<caption>
				절차 진행 상황 테이블 입니다.
			</caption>
			<colgroup>
				<col style="width:20%">
				<col style="width:30%">
				<col style="width:20%">
				<col style="width:30%">
			</colgroup>
			<tr>
				<th>사건접수일</th>
				<td><?php echo $data['lh_accept_date']=='0000-00-00'?'':$data['lh_accept_date'];?></td>
				<th>출석조사일</th>
				<td><?php echo str_replace(',', '<br>', $data['lid_date']);?></td>
			</tr>	
			<tr>
				<th>사건종결일</th>
				<td colspan="3"><?php echo $data['lh_case_end_date']=='0000-00-00'?'':$data['lh_case_end_date'];?></td>
			</tr>
		</table>	
		<h3 class="sub_stit" style="font-size:20px;">지원내용 및 결과</h3>
		<table class="tInsert">
			<caption>
				지원내용 및 결과 테이블 입니다.
			</caption>
			<colgroup>
				<col style="width:20%">
				<col style="width:30%">
				<col style="width:20%">
				<col style="width:30%">
			</colgroup>
			<tr>
				<th>권리구제지원내용</th>
				<td colspan="3" style="line-height:30px;padding:15px 10px;">
					<?php echo nl2br($data['lh_sprt_content']);?>
				</td>
			</tr>
			<tr>
				<th>지원 결과</th>
				<td colspan="3" style="line-height:30px;padding:15px 10px;">
					<?php echo $data['apply_rst_cd_nm'] .' '. ($data['apply_rst_cd_etc'] != '' ? '<br>('.$data['apply_rst_cd_etc'].')' : '');?>
				</td>
			</tr>
			<tr>
				<th>특이사항</th>
				<td colspan="3" style="line-height:30px;padding:15px 10px;">
					<?php echo nl2br($data['lh_etc']);?>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
