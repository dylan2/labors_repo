<!doctype html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow" />
	<title>서울시통합노동상담관리시스템 관리자</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<link href="./css/admin-style.css" rel="stylesheet" type="text/css">
	<link href="./css/jquery-ui-1.11.2.min.css" rel="stylesheet" type="text/css">
	<link href="./css/dv_loading.css" rel="stylesheet" type="text/css">
	<!--[if lt IE 9]>
	<script type="text/javascript" src="./js/html5shiv.js"></script>
	<script type="text/javascript" src="./js/html5shiv.printshiv.js"></script>
	<![endif]-->
	<script type="text/javascript" src="./js/jquery.1.9.1.min.js"></script>
	<script type="text/javascript" src="./js/jquery-ui.1.9.2.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(e){
		window.print();
	});
	</script>
	<style>
		body,table,div { font-size:20px; }
	</style>
</head>

<body style="width:650px;margin:0px 10px 0">				
	<div style="font-size:27px;padding-top:10px;padding-bottom:4px;border-bottom:1px solid #ddd">
		<b>상담내역</b>
		<div style="margin:-20px 0 0 800px;"><?php echo get_date();?></div>
	</div>
	<div class="cont_area">
		<h3 class="sub_stit" style="font-size:20px;">상담경로</h3>
		<table class="tInsert">
			<caption>
				상담경로 입력 테이블 입니다.
			</caption>
			<colgroup>
				<col style="width:20%">
				<col style="width:30%">
				<col style="width:20%">
				<col style="width:30%">
			</colgroup>
			<tr>
				<th>소속</th>
				<td><?php echo $data['asso_name'];?></td>
				<th>상담자</th>
				<td><?php echo $data['oper_name'];?></td>
			</tr>
			<tr>
				<th>상담일</th>
				<td><?php echo $data['csl_date'];?></td>
				<th>상담방법</th>
				<td><?php echo $data['csl_method'] .' '. $data['s_code_etc'];?></td>
			</tr>
		</table>	
		<h3 class="sub_stit" style="font-size:20px;">사업장 현황</h3>
		<table class="tInsert">
			<caption>
				내담자 정보 입력 테이블 입니다.
			</caption>
			<colgroup>
				<col style="width:20%">
				<col style="width:30%">
				<col style="width:20%">
				<col style="width:30%">
			</colgroup>
			<!-- 요청에의해 성명,연락처 제거 20160621 -->
			<tr>
				<th>사업장명</th>
				<td colspan="3"><?php echo $data['csl_cmp_nm'];?></td>
			</tr>	
			<tr>
				<th>대표자명</th>
				<td colspan="3"><?php echo $data['csl_name'];?></td>
			</tr>	
			<tr>
				<th>연락처</th>
				<td colspan="3"><?php echo $data['csl_tel'];?></td>
			</tr>	
			<tr>
				<th>소재지 </th>
				<td colspan="3"><?php echo $data['comp_addr_cd_nm'] .' '. ($data['comp_addr_etc']==''?'':' ('.$data['comp_addr_etc'].')');?></td>
			</tr>
			<tr>
				<th>근로자수 </th>
				<td colspan="3"><?php echo $data['emp_cnt_cd_nm'] .' '. ($data['emp_cnt_etc']==''?'':' ('.$data['emp_cnt_etc'].')');?></td>
			</tr>
			<tr>
				<th>업종 </th>
				<td colspan="3"><?php echo $data['comp_kind_cd_nm'] .' '. ($data['comp_kind_etc']==''?'':' ('.$data['comp_kind_etc'].')');?></td>
			</tr>
			<tr>
				<th>운영기간</th>
				<td colspan="3"><?php echo $data['oper_period_cd_nm'];?></td>
			</tr>
			<tr>
				<th style="font-size:19px;">근로계약서작성여부</th>
				<td colspan="3"><?php echo $data['emp_paper_yn_cd_nm'];?></td>
			</tr>
			<tr>
				<th>4대보험가입여부</th>
				<td colspan="3"><?php echo $data['emp_insured_yn_cd_nm'];?></td>
			</tr>
			<tr>
				<th>취업규칙작성여부</th>
				<td colspan="3"><?php echo $data['emp_rules_yn_cd_nm'];?></td>
			</tr>
		</table>
		<h3 class="sub_stit" style="font-size:20px;">상담유형 및 처리결과</h3>
		<table class="tInsert">
			<caption>
				상담유형 및 처리결과 입력 테이블 입니다.
			</caption>
			<colgroup>
				<col style="width:18%">
				<col style="width:82%">
			</colgroup>
			<tr>
				<th>상담유형</th>
				<td><?php echo $data['csl_kind'];?></td>						
			</tr>
			<tr>
				<th>상담내용</th>
				<td style="line-height:30px;padding:15px 10px;">
					<?php echo nl2br($data['csl_content']);?>
				</td>
			</tr>
			<tr>
				<th>답변</th>
				<td style="line-height:30px;padding:15px 10px;">
					<?php echo nl2br($data['csl_reply']);?>
				</td>	
			</tr>
			<tr>
				<th>처리결과</th>
				<td style="line-height:30px;padding:15px 10px;">
					<?php echo $data['csl_proc_rst_cd_nm'] .' '. $data['csl_proc_rst_etc'];?>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
