<?php
include_once './inc/inc_header.php';

// 권한그룹 id
$grp_id = base64_decode($this->session->userdata(CFG_SESSION_ADMIN_AUTH_GRP_ID));

// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
$is_gu_officer = 0;
if ($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
	$is_gu_officer = 1;
}

?>

<script>

var is_gu_officer = "<?php echo $is_gu_officer;?>";

$(document).ready(function(){

	// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
	if(is_gu_officer == 1) {
		$('#btnAdd').css('display', 'none');
		$('#btnExcel').css('display', 'none');
		$('#btnDelete').css('display', 'none');
	}


	// 등록 버튼 이벤트 핸들러
	$('#btnAdd').click(function(e){
		check_auth_redirect($('#'+ sel_menu).find('li>a').first(), 'add', '');
	});

	// 전송버튼 이벤트 핸들러
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		_page = 1;
		// 검색어 보관
		$.cookie("laborsSearchDataCounselBiz", $('form[name=frmSearch]').serialize());
		get_list(_page);
	});
	/*
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		// validation - 업종, 처리결과, 키워드 외 검색어 미입력 체크
		var sel = $('select[name=target] option:selected').val();
		if(sel != '' && (sel != 'counsel_comp_kind' && sel != 'counsel_keyword' && sel != 'counsel_proc_rst') && $('input[name=keyword]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_11']);
			return;
		} 
		_page = 1;
		$('#loading').show();
		// 검색 후 검색어가 내용보기에서 뒤로가기버튼 클릭시 사라지는 문제 해결을 위해 쿠키 저장방식으로 변경, 2018.08.27 
		$.cookie("laborsSearchDataCounselBiz", $('form[name=frmSearch]').serialize());
		$('input[name=page]').val(_page);
		$('#frmSearch').attr("action", "/?c=biz_counsel&m=counsel_list").submit();
	});
	*/
	
	// 초기화 이벤트 핸들러
	$('#btnClear').click(function(e){
		e.preventDefault();
		_page = 1;
		$('form')[0].reset();
		$('#keyword').val('').css('display', 'inline');
		$('#target_keyword').css('display', 'none'); // 주제어
		$('#target_work_kind').css('display', 'none'); // 직종
		$('#target_comp_kind').css('display', 'none'); // 업종
		$('#target_csl_proc_rst').css('display', 'none'); // 처리결과
		// 쿠키에 저장한 검색데이터 삭제, 2018.08.27
		$.removeCookie("laborsSearchDataCounselBiz");
	});
	
	// 다중 삭제 버튼 이벤트 핸들러
	$('#btnDelete').click(function(e){
		e.preventDefault();
		var len = $('input[name=chk_item]:checked').length;
		if(len == 0) {
			alert(CFG_MSG[CFG_LOCALE]['info_auth_04']);
		}
		else {
			var csl_seqs = '';
			$('input[name=chk_item]:checked').each(function(i){
				if(csl_seqs != '') csl_seqs +='<?php echo CFG_AUTH_CODE_DELIMITER;?>';
				csl_seqs += $(this).attr('data-role-id');
			});
			
			var kind = 'del_multi';
			if( ! confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
				return false;
			}
			req_code_manage(kind, csl_seqs);
		}
	});

	// Excel - button
	$('#btnExcel').click(function(e){
		e.preventDefault();
		// 엑셀 다운로드할 내용 유무 체크
		if($('table tr>td').length <= 1 && $('table tr>td').text().indexOf('없습니다') != -1) {
			alert(CFG_MSG[CFG_LOCALE]['info_sttc_01']);
			return;
		}
		var url = '/?c=biz_counsel&m=vdownload_excel&kind=biz_counsel_list';
		gLayerId = openLayerModalPopup(url, 400, 200, '', '', '', '', 1, true, '');
	});

	// 검색 - 검색어 변경시 처리
	$('#target').change(function(e){
		var sel = $('#target option:selected').val();
		var display1 = display2 = display3 = display4 = 'none';
		display1 = sel == 'counsel_keyword' ? 'inline' : 'none';
		display3 = sel == 'counsel_comp_kind' ? 'inline' : 'none';
		display4 = sel == 'counsel_proc_rst' ? 'inline' : 'none';

		var keyword_val = '';
		var kwyword_display = 'inline';
		$('#keyword').css('display', 'inline').width('60%');
		if(sel == 'counsel_keyword' || sel == 'counsel_comp_kind' || sel == 'counsel_proc_rst') {
			keyword_val = '';
			// 직종,업종은 keyword를 항상 노출한다.
			if(sel == 'counsel_keyword' || sel == 'counsel_proc_rst') {
				kwyword_display = 'none';
			}
			$('#keyword').width('40%');
		}
		$('#target_keyword').css('display', display1); // 주제어
		$('#target_comp_kind').css('display', display3); // 업종
		$('#target_csl_proc_rst').css('display', display4); // 처리결과
		$('#keyword').val(keyword_val).css('display', kwyword_display);
	});

	// 검색 - 처리결과 변경시
	$('#target_csl_proc_rst').change(function(e){
		// var etc_flag = 0;
		// 기타 선택시 - 입력박스 show
		var selText = $('#target_csl_proc_rst option:selected').text();
		var display = 'none';
		if(selText == '기타') {
			display = 'inline';
		}
		$('#keyword').val("").css('display', display);
	});
	
	// 검색 - 검색어 엔터 처리
	$("#keyword").keypress(function(e) {
		if (e.keyCode == 13) {
			$("#btnSubmit").click();
		}
	});

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();

		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_end = '';
		}

		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});	


	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates();
	});
	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});

	// *** 상담일만 적용해 달라는 요청 2016.06.15 최진혁
	// 검색어 채워넣기
	// -- 검색 후 history.back 버튼 클릭시 검색어 채워넣기 안되는 문제 제기로 클라이언트 쿠키에 저장하는 방식으로 변경함. dylan, 2018.08.27 
	var schData = $.cookie("laborsSearchDataCounselBiz");
	// *** 키워드 검색시 검색어 유지 요청으로 수정 2017.09.12 서재란
	// - 상담일
	var schDateBegin = getValueInGetStringByKey(schData, "search_date_begin");
	$('input[name=search_date_begin]').val(schDateBegin);
	var schDateEnd = getValueInGetStringByKey(schData, "search_date_end");
	$('input[name=search_date_end]').val(schDateEnd);
	// - 검색대상
	var schTarget = getValueInGetStringByKey(schData, "target");
	$('select[name=target] option[value="'+ schTarget +'"]').prop('selected', true);
	$('select[name=target]').change();
	// - 검색어
	var schKeyword = getValueInGetStringByKey(schData, "keyword");
	if(schKeyword) {
		schKeyword = decodeURIComponent(schKeyword)
	}
	$('input[name=keyword]').val(schKeyword);
	// - 년월 selectbox
	var schSelYear = getValueInGetStringByKey(schData, "sel_year");
	$('select[name=sel_year] option[value="'+ schSelYear +'"]').prop('selected', true);
	var schSelMonth = getValueInGetStringByKey(schData, "sel_month");
	$('select[name=sel_month] option[value="'+ schSelMonth +'"]').prop('selected', true);
	// - 상담방법
	var schCslWay = getValueInGetStringByKey(schData, "csl_way");
	$('select[name=csl_way] option[value="'+ schCslWay +'"]').prop('selected', true);

	// list
	// 요청에 의한 수정 : dylan 2017.09.12, danvistory.com
	get_list("<?php echo isset($search_data['page']) ? $search_data['page'] : 1;?>");
	
	$('table tbody tr').hover(function() {
		$(this).css( "background-color", "#efefef" );
	}, function() {  
		$(this).css( "background-color", "transparent" );
	});

	// page
	_page = +"<?php echo $page;?>";

	// set config of Pagination and creation
	//_cfg_pagination.total_item = +"<?php //echo $tot_cnt;?>";
// 	_cfg_pagination.currentPage = _page;
// 	_cfg_pagination.linkFunc = 'pgLinkFunc';
// 	_pagination = new Pagination(_cfg_pagination);
// 	$('ul.pages').html(_pagination.toString());
});

/**
 * pagination에서 페이지번호 클릭시 호출할 함수
 */
var pgLinkFunc = function(page){
	$('input[name=page]').val(_page = page);
	$('#frmSearch').submit();
};

/**
 * 리스트를 동적생성하면 이벤트를 다시 걸어줘야 한다.
 */
function addManagerEventListener() {
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		var status = $(this).prop('checked');
		$("input[name=chk_item]:checkbox").each(function(i) {
			$(this).prop("checked", status);
		});
	});
}


/**
 * 삭제 처리 요청 및 리스트, 페이징 생성
 */
function req_code_manage(type, tid) {
	var url = '/?c=biz_counsel&m='+ type;
	var rsc = $('#frmSearch').serialize() + '&seqs='+ tid;
	var fn_succes = function(data) {
		// 삭제시 이미 사용된 코드 삭제인 경우 메시지 처리
		if(type.indexOf('del') != -1 && data.rst == 'exist') {
			if(data.len && data.len > 0) {
				var msg = CFG_MSG[CFG_LOCALE]['info_csl_10'].replace('x', data.len);
				alert(msg);
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
			}
		}
		else {
			if(type != 'view') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
			}
		}
		location.reload();
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		
		var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
		if(data && data.msg) msg += '[' + data.msg +']';
		alert(msg);
		location.reload();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list(page) {
	// search date
	var date_b = $('#search_date_begin').val();
	var date_e = $('#search_date_end').val();
	if((date_b && date_e == '' ) || (date_b == '' && date_e)) {
		var now = new Date();
		var today = get_today();
		if(date_b == '') $('#search_date_begin').val(today);
		// if(date_e == '') $('#search_date_end').val(today);
	}

	// page
	_page = page;

	var url = '/?c=biz_counsel&m=counsel_list';
	var rsc = $('#frmSearch').serialize() +'&page='+_page;
	var fn_succes = function(data) {
		// set config total count
		_cfg_pagination.total_item = data.tot_cnt == 0 ? 1 : data.tot_cnt;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		// build the list
		gen_list(data);
	};
	var fn_error = function(data) {
		if(is_local) objectPrint(data);
		// empty list
		_cfg_pagination.total_item = 1;
		_cfg_pagination.currentPage = 1;
		_cfg_pagination.linkFunc = 'get_list';
		_pagination = new Pagination(_cfg_pagination);
		gen_list();
	};
	// request
	req_ajax(url, rsc, fn_succes, fn_error);
}

// edit
function edit(seq) {
	check_auth_redirect($('#'+ sel_menu).find('li>a').first(), 'edit', seq, '', _page);
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
// 	var html_b = '<table class="tList" border="0">';
// 	html_b += '<caption>상담내역조회 목록입니다.</caption>';
// 	html_b += '<colgroup>';
// 	html_b += '<col style="width:3%">';
// 	html_b += '<col style="width:6%">';
// 	html_b += '<col style="width:12%">';
// 	html_b += '<col style="width:*">';
// 	html_b += '<col style="width:12%">';
// 	html_b += '<col style="width:12%">';
// 	html_b += '<col style="width:10%">';
// 	html_b += '</colgroup>';
// 	html_b += '<tr>';
// 	html_b += '<th><input type="checkbox" class="imgM checkAll"></th>';
// 	html_b += '<th>번호</th>';
// 	html_b += '<th>상담방법</th>';
// 	html_b += '<th>제목</th>';
// 	html_b += '<th>상담자</th>';
// 	html_b += '<th>소속</th>';
// 	html_b += '<th>상담일</th>';
// 	html_b += '</tr>';
	
	var html_b = '';
	var total_cnt = 0;
	var tr_bgcolor = '';
	if(data && typeof data.csl == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.csl.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;

		var csl_proc_rst = '';
		for(var i=begin; i<end; i++) {
			var d = data.csl;
			html_b += '<tr>';
			html_b += '  <td><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="'+ d[index].seq +'"></td>';
			html_b += '  <td>'+ (no--) +'</td>';
			html_b += '  <td>'+ d[index].csl_way +'</td>';
			html_b += '  <td onclick="javascript:edit(\''+ d[index].seq +'\');"><div style="cursor:pointer">'+ d[index].csl_title +'</div></td>';
			html_b += '  <td>'+ d[index].oper_name +'</td>';
			html_b += '  <td>'+ d[index].asso_name  +'</td>';
			html_b += '  <td>'+ d[index].csl_date +'</td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="7" style="align:center">내용이 없습니다.</td></tr>';
	}
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list tr').not('.th').remove();
	$('#list').append(html_b);
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());
	
	// 체크박스 전체선택 이벤트 등록
	addManagerEventListener();

	// 버튼 블럭 show
	$('.cssBtnBlock').show();

	$('#loading').hide();
}
</script>

<?php
include_once './inc/inc_menu.php';
?>

<!-- //  contents_body  area -->
<div id="contents_body">
	<div id="cont_head">
		<h2 class="h2_tit">사용자상담조회</h2>
		<span class="location">홈 > 사용자상담관리 > <em>사용자상담조회</em></span>
	</div>
	<div class="cont_area">
		<form name="frmSearch" id="frmSearch" method="post">
			<input type="hidden" name="selected_menu" value="<?php echo $data['selected_menu'];?>"> 
			<input type="hidden" name="m_code" value="<?php echo $data['m_code'];?>"> 
			<input type="hidden" name="page">
			<div class="con_text">
				<label for="csl_way" class="l_title">상담방법</label> 
				<select id="csl_way" name="csl_way" class="styled" style="width: 150px">
					<option value="" selected>전체</option>
						<?php
						foreach ($res['csl_way'] as $item) {
							echo '<option value="' . base64_encode($item->s_code) . '">' . $item->code_name . '</option>';
						}
						?>
				</select> 
				<br>
				<span class="divice"></span> 
				<label for="board_select" class="l_title">상담일</label> 
				<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" style="width: 90px" readonly="readonly"> ~
				<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" style="width: 90px" readonly="readonly"> 
				<label for="sel_year" class="l_title" style="margin-left: 10px;">연월별</label>
				<select id="sel_year" name="sel_year" style="margin-left: -20px;">
					<option value="">연도</option>
						<?php
						foreach ($res['search_year'] as $item) {
							echo '<option value="' . $item->year . '">' . $item->year . '</option>';
						}
						?>
					</select> <select id="sel_month" name="sel_month">
					<option value="">전체</option>
					<option value="01">1월</option>
					<option value="02">2월</option>
					<option value="03">3월</option>
					<option value="04">4월</option>
					<option value="05">5월</option>
					<option value="06">6월</option>
					<option value="07">7월</option>
					<option value="08">8월</option>
					<option value="09">9월</option>
					<option value="10">10월</option>
					<option value="11">11월</option>
					<option value="12">12월</option>
				</select> 
				<label for="sel_year" class="l_title" style="margin-left: 10px;">기간별</label>
				<button type="button" id="btnToday" class="buttonS bGray" style="margin-left: -20px;">오늘</button>
				<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
				<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
				<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button>
				<br> 
				<span class="divice"></span>
				<label for="target" class="l_title">검색어</label> 
				<select id="target" class="styled" name="target">
					<option value="" selected>전체</option>
					<option value="csl_title">제목</option>
					<option value="csl_content">상담 내용</option>
					<option value="csl_reply">답변</option>
					<option value="counsel_comp_kind">업종</option>
					<option value="counsel_keyword">주제어</option>
					<option value="counsel_proc_rst">처리결과</option>
					<!-- 상담자,내담자 항목 추가 - 2015.11.23 요청에 의한 추가 delee -->
					<option value="counsel_oper_id">상담자ID</option>
					<option value="counsel_oper_name">상담자</option>
					<option value="csl_cmp_nm">사업장명</option>
					<option value="csl_name">대표자명</option>
					<option value="counsel_csl_tel">연락처 뒤4자리</option>
				</select>
				<!-- 주제어 항목 -->
				<select id="target_keyword" class="styled" name="target_keyword" style="display: none">
					<option value="" selected>전체</option>
						<?php
						foreach ($res['csl_keyword'] as $rs) {
							echo '<option value="' . base64_encode($rs->s_code) . '" >' . $rs->code_name . '</option>';
						}
						?>
				</select>
				<!-- 업종 항목 -->
				<select id="target_comp_kind" class="styled" name="target_comp_kind" style="display: none">
					<option value="" selected>전체</option>
						<?php
						foreach ($res['csl_comp_kind_code'] as $rs) {
							echo '<option value="' . base64_encode($rs->s_code) . '" >' . $rs->code_name . '</option>';
						}
						?>
				</select>
				<!-- 처리결과 항목 -->
				<select id="target_csl_proc_rst" class="styled" name="target_csl_proc_rst" style="display: none">
					<option value="" selected>전체</option>
						<?php
						foreach ($res['csl_proc_rst_code'] as $rs) {
							echo '<option value="' . base64_encode($rs->s_code) . '" >' . $rs->code_name . '</option>';
						}
						?>
				</select>
				<input type="text" style="width:60%" id="keyword" name="keyword">
				<span class="divice"></span>
				<div>전체 검색은 사업장명,대표자명,전화번호 뒤 4자리,제목,상담 내용,답변,상담자id/이름,처리결과,업종의 기타를 대상으로 검색됩니다.</div>
				<span class="divice"></span>
				<div class="textR" style="margin-top:-27px;">
					<button type="button" id="btnClear" class="buttonS bGray">
						<span class="icon_all"></span>초기화
					</button>
					<button type="button" id="btnSubmit" class="buttonS bBlack">
						<span class="icon_search"></span>검색
					</button>
				</div>
			</div>
		</form>

		<!--  
		<div style="margin: 10px 0 5px 0;">건수 : <?php //echo number_to_currency($tot_cnt);?></div>
		-->

		<div class="list_no"></div>
				
		<table class="tList" border="0" id="list">
			<caption>상담내역조회 목록입니다.</caption>
			<colgroup>
				<col style="width: 3%">
				<col style="width: 6%">
				<col style="width: 12%">
				<col style="width: *">
				<col style="width: 12%">
				<col style="width: 12%">
				<col style="width: 10%">
			</colgroup>
			<tr class="th">
				<th><input type="checkbox" class="imgM checkAll"></th>
				<th>번호</th>
				<th>상담방법</th>
				<th>제목</th>
				<th>상담자</th>
				<th>소속</th>
				<th>상담일</th>
			</tr>
					<?php
// 					if (empty($csl)) {
// 						echo '<tr><td colspan="7" style="align:center">내용이 없습니다.</td></tr>';
// 					}
// 					else {
// 						foreach ($csl as $key => $item) {
// 							// 상담처리결과 가 "옴부즈만지원신청" 인 경우 - 배경색 처리
// 							$tr_bgcolor = '';
// 							if ($item->csl_proc_rst_cd == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST_OMBUDSMAN) {
// 								$tr_bgcolor = ' style="background-color:#DDE9FF;font-weight:bold;"';
// 							}
// 							echo '<tr>';
// 							echo '  <td' . $tr_bgcolor . '><input type="checkbox" name="chk_item" class="ai_chkbox" data-role-id="' . $item->seq . '"></td>';
// 							echo '  <td' . $tr_bgcolor . '>' . $loop -- . '</td>';
// 							echo '  <td' . $tr_bgcolor . '>' . $item->csl_method_nm . '</td>';
// 							echo '  <td onclick="javascript:edit(\'' . $item->seq . '\');"' . $tr_bgcolor . '><div style="cursor:pointer">' . $item->csl_title . '</div></td>';
// 							echo '  <td' . $tr_bgcolor . '>' . $item->oper_name . '</td>';
// 							echo '  <td' . $tr_bgcolor . '>' . $item->asso_code_nm . '</td>';
// 							echo '  <td' . $tr_bgcolor . '>' . $item->csl_date . '</td>';
// 							echo '</tr>';
// 						}
// 					}
					?>
				</table>

		<div class="floatC marginT10 cssBtnBlock" style="display:none;">
				<?php
				// 관리자 그룹인 경우만 다중삭제 버튼 노출
				if ($this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER) == 1 || $grp_id == CFG_AUTH_CODE_ADMIN) {
					echo '<button type="button" class="buttonM bGray floatL marginL05" id="btnDelete">삭제</button>' . PHP_EOL;
				}
				?>
			<button type="button" class="buttonM bSteelBlue floatR" id="btnAdd">등록</button>
			<button type="button" class="buttonM bGray floatR" id="btnExcel" style="margin-right: 5px;">엑셀 다운</button>
		</div>

		<!-- pagination-->
		<div class="tPages">
			<ul class="pages"></ul>
		</div>

	</div>
	<!-- //컨텐츠 -->
</div>
<!-- //  contents_body  area -->


<?php
include_once './inc/inc_footer.php';
?>
