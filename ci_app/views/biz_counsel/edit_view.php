<?php
include_once './inc/inc_header.php';


// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
$is_gu_officer = 0;
if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK4) {
	$is_gu_officer = 1;
}


?>


<style type="text/css">
/* 가독성을 높이기 위해 추가 */
textarea {
	line-height: 1.4em;
	resize: vertical;
}
/* 태그 비활성화 클래스 */
.disabled {
	background-color:#eee;
}
</style>

<script type="text/javascript" src="./js/jquery.base64encode.js"></script>
<script>

// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
var is_gu_officer = "<?php echo $is_gu_officer;?>";

// [권한] 체크 - 통계 상담사례>상담보기, 엑셀 다운로드 링크를 타고 접근하는 경우의 처리
var is_auth = "<?php echo $res['is_auth']; ?>";
if(is_auth == 0) {
	alert(CFG_MSG[CFG_LOCALE]['info_csl_22']);
	history.back();
}

// 등록/수정 가능 기간 - 추가 2017.01.31 dylan
var editable_day = "<?php echo CFG_COUNSEL_EDITABLE_DAY;?>";
var is_editable_period = true;



var kind = "<?php echo $res['kind'] == 'menu' ? 'add' : $res['kind']; ?>";
if(!kind) kind = 'add';

// 자신의 글인지 여부
var is_owner = "<?php echo $res['is_owner']; ?>";

// 수정/삭제 가능 여부 - 서울시 계정만 해당됨(옴부즈만 상대)
var is_editable = "<?php echo $res['is_editable']; ?>";

// 인쇄가능 여부 - 권익센터,자치구센터 소속자의 작성글은 해당 소속자는 인쇄가능하다.
var is_printable = "<?php echo $res['is_printable']; ?>";

// 공지 팝업 게시물 seq
var popup_noti_seq = "<?php echo $res['popup_seq'];?>";



$(document).ready(function(){
	
	// [권한] 자치구공무원인 경우 : 같은 소속의 옴부즈만이 상담한 글에 대해서만 조회,인쇄 권한 있기 때문에 글등록 버튼을 숨긴다.
	if(is_gu_officer == 1) {
		$('.cssAdd').prop('disabled', true);
	}

	// 년도가 바뀐 경우 - 수정가능일 이전만 이전년도 상담입력 가능, 이후는 당해년도만 입력 가능하게 수정
	// - (주) 수정 가능일이 한달이 넘을 경우 아래 코드 수정해야 한다.
	var today = new Date();
	if(today.getMonth() >= 0) { // 1월
		var op = {date:'#csl_date', option:{yearRange: 'c:c'}}
		// 1월 이고 editable_day 경과 전인 경우
		//if(today.getMonth() == 0 && today.getDate() <= editable_day) {
		//	op = {date:'#csl_date', option:{yearRange: 'c-1:c'}}
		//}
		setup_datepicker(op);
	}

	// 등록
	if(kind == 'add') {
		// 상담일 today 세팅
		$('#csl_date').val(get_today());

		// 소재지 주소 최초 선택 처리
		$('#comp_addr_cd option').each(function(i,o){
			if($(this).text() == '무응답') {
				$('#comp_addr_cd option:eq("'+ i +'")').attr('selected', 'selected');
			}
		});

		// 삭제 버튼 숨김
		$('.cssDel').hide();
	}
	// edit
	else {
		// 소재지 주소
		if($('#comp_addr_cd option:selected').text() == '기타') {
			$('#comp_addr_etc').css('display', 'inline');
		}

		// 추가
		// - 당해년도 이전 상담은 수정시 통계가 수정전과 바뀌는 문제로 수정못하도록 요청에 의해 수정, 2017.01.18 dylan
		// - 년도가 바뀐 경우, 수정 가능일이 지나면 달력 선택을 막는다. 
		// - (주) 수정 가능일이 한달이 넘을 경우 아래 코드 수정해야 한다.
		// 추가 2019.02.07
		// - 관리자계정 외 전년도 말일까지 상담건은 수정기간(1/20) 이후 모두 막는다.
		// - 체크일을 상담일이 아닌 등록일 기준으로 변경(상담일은 등록일과 다르게 지정할 수 있기 때문)
		// var csl_date = "<?php //echo $csl['csl_date'];?>";
		var reg_date = "<?php echo $csl['reg_date'];?>";
		if(new Date(reg_date).getFullYear() < today.getFullYear()) { 
			// 1월 이고 editable_day 경과 또는 1월 이후
			if((today.getMonth() == 0 && today.getDate() > editable_day) || today.getMonth() > 0) {
				is_editable_period = false;
			}
		}
		
		// 관리자 외 자신의 글 아니면 등록,삭제 버튼 숨김
		// 추가 - 관리자외 모든계정은 매년 1월 20일까지만 수정가능하도록 요청에 의해 수정, 최진혁. 2019.02.07 dylan
		if(is_master != 1 && (is_editable_period == false || (is_owner == 0 && is_editable == 0))) {
			$('.cssAdd').prop('disabled', true);
			$('.cssDel').prop('disabled', true);
		}
				
		// 서울권익센터,자치구센터 계정 중 작성자와 같은 소속인 경우는 허용
		if(is_printable == 1) {
			$('.cssPrint').prop('disabled', false);
		}
	}// end of edit

	
	// 인쇄 버튼
	$('.cssPrint').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		if(kind == 'edit') {
			open_popup_center_dual('/?c=biz_counsel&m=counsel_print&seq='+ $('#seq').val(), '인쇄', 1060, 700);
		}
	});

	// 소재지 - 선택된 값이 없으면 "무응답" 항목을 checked한다.
	if( ! $("select[name=comp_addr_cd]").is(":selected")) {
		$('select[name=comp_addr_cd] option').each(function(i,o){
			if($(o).text() == '무응답') {
				$(o).prop('selected', true);
			}
		});
	}
	// 근로자수 - 선택된 값이 없으면 "무응답" 항목을 checked한다.
	if( ! $("input[name=emp_cnt_cd]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_cnt_cd"]'));
		$('input:radio[name="emp_cnt_cd"]:nth('+ index +')').attr('checked', true);
	}
	// 업종 - 선택된 값이 없으면 무응답을 checked한다.
	if( ! $("input[name=comp_kind_cd]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="comp_kind_cd"]'));
		$('input:radio[name="comp_kind_cd"]:nth('+ index +')').attr('checked', true);
	}
	// 운영기간 - 선택된 값이 없으면 무응답을 selected한다.
	if( ! $("select[name=oper_period_cd]").is(":selected")) {
		$('select[name=oper_period_cd] option').each(function(i,o){
			if($(o).text() == '무응답') {
				$(o).prop('selected', true);
			}
		});
	}
	// 근로계약서 작성 여부 - 선택된 값이 없으면 무응답을 checked한다.
	if( ! $("input[name=emp_paper_yn_cd]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_paper_yn_cd"]'));
		$('input:radio[name="emp_paper_yn_cd"]:nth('+ index +')').attr('checked', true);
	}
	// 4대보험가입 여부 - 선택된 값이 없으면 무응답을 checked한다.
	if( ! $("input[name=emp_insured_yn_cd]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_insured_yn_cd"]'));
		$('input:radio[name="emp_insured_yn_cd"]:nth('+ index +')').attr('checked', true);
	}
	// 취업규칙 - 선택된 값이 없으면 무응답을 checked한다.
	if( ! $("input[name=emp_rules_yn_cd]").is(":checked")) {
		var index = find_text_in_elms($('input:radio[name="emp_rules_yn_cd"]'));
		$('input:radio[name="emp_rules_yn_cd"]:nth('+ index +')').attr('checked', true);
	}
	// 상담사례공유 - 선택된 값이 없으면 '공유안함' radio를 checked한다.
	if( ! $("input:radio[name=csl_share_yn_cd]").is(":checked")) {
		$('input:radio[name="csl_share_yn_cd"]').each(function(){
			if($(this).next().text().indexOf('공유안함') != -1) {
				$(this).attr('checked', true);
			}
		});
	}

	// 전송버튼 클릭시 form onSubmit 이벤트 발생처리
	$('.cssAdd').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['info_cmm_06'])) {
			$('form').submit();
		}
	});
	
	// 삭제버튼 클릭
	$('.cssDel').click(function(e){
		$('#focusOuter').focus();
		e.preventDefault();

		if(confirm(CFG_MSG[CFG_LOCALE]['warn_cmm_01'])) {
			
			var url = '/?c=biz_counsel&m=del';
			var rsc = {seq: $('#seq').val()};
			var fn_succes = function(data) {
				if(data.rst == 'succ') {
					alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
					check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
				}
				else {
					if(is_local) objectPrint(data);
					alert(data.msg);
				}
			};
			var fn_error = function(data) {
				if(is_local) objectPrint(data);
			
				var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
				if(data && data.msg) msg += '[' + data.msg +']';
				alert(msg);
			};
			// request
			req_ajax(url, rsc, fn_succes, fn_error);
		}
	});

	// 등록 버튼 이벤트 핸들러
	$('form').submit(function(e) {

		// 수정 - 자신의 글인지 체크
		if(kind == 'edit' && is_master != 1 && (is_owner == 0 && is_editable == 0)) {
			alert(CFG_MSG[CFG_LOCALE]['warn_board_02']);
			return false;
		}

		// validation
		// 필수체크 항목 : 상담방법, 근로자수, 업종, 운영기간, 근로계약서 작성여부, 
		// -상담방법
		if(!$("input[name=s_code]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_19']);
			$("input[name=s_code]").focus();
			return false;
		}
		// -상담유형
		if(!$("input[id^=csl_kind_]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_14']);
			$('#csl_kind_0').focus();
			return false;
		}
		// -상담유형 - 3개까지 체크 가능
		var csl_kind_chk_len = $("input[id^=csl_kind_]:checked").length;
		if(csl_kind_chk_len > 3) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_16']);
			$('#csl_kind_0').focus();
			return false;
		}
		// -제목
		if($('#csl_title').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_03']);
			$('#csl_title').focus();
			return false;
		}
		// -상담내용
		if($('#csl_content').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_04']);
			$('#csl_content').focus();
			return false;
		}
		// -상담 답변
		if($('#csl_reply').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_05']);
			$('#csl_reply').focus();
			return false;
		}
		// -상담 처리결과
		if(!$("input[name=csl_proc_rst_cd]").is(":checked")) {
			alert(CFG_MSG[CFG_LOCALE]['info_csl_06']);
			$('#csl_proc_rst_cd_0').focus();
			return false;
		}

		// 기타 input box값 체크 - 기타가 아닌 경우 input box 값 없앰
		// - 상담방법
		if($('input:radio[name=s_code]:checked').next().text() != '기타') {
			$('input[name=s_code_etc]').val('');
		}
		// - 처리결과
		if($('input:radio[name=csl_proc_rst_cd]:checked').next().text() != '기타') {
			$('input[name=csl_proc_rst_etc]').val('');
		}

		// 전송
		var url = '/?c=biz_counsel&m=processing';
		var rsc = $('#frmForm').serialize();
		var fn_succes = function(data) {
			if(data.rst == 'succ') {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_01']);
				check_auth_redirect($('#'+ sel_menu).find('li>a').last() );
			}
			else {
				alert(CFG_MSG[CFG_LOCALE]['info_cmm_02']);
				if(is_local) objectPrint(data);
			}
		};
		var fn_error = function(data) {
			if(is_local) objectPrint(data);
		
			var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
			if(data && data.msg) msg += '[' + data.msg +']';
			alert(msg);
		};
		// request
		req_ajax(url, rsc, fn_succes, fn_error);

		e.preventDefault();
	});


	// list 버튼 이벤트 핸들러
	$('.cssList').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		var page = "<?php echo $page;?>";
		check_auth_redirect($('#'+ sel_menu).find('li>a').last(), 'list', '', '', page);
	});

	//내담자정보->관련상담 버튼 클릭 핸들러
	$('#btnRelViewCounsel').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		if($('#csl_cmp_nm').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_biz_csl_01']);
			$('#csl_cmp_nm').focus();
			return false;
		}

		$('#loading').show();
		var closedCallback = function(data) {
			if(data) {
				// 원글key가 있는 상담인 경우 작성하는 상담의 원글key를 최초 원글key로 세팅한다.
				if(data.csl_ref_seq && data.csl_ref_seq > 0) {
					$('#csl_ref_seq').val(data.csl_ref_seq);
				}
				// "관련상담"이 "관련상담"으로 작성한 상담이 아닌 경우
				else {
					if(data.seq) $('#csl_ref_seq').val(data.seq);
				}
				//
				if(is_valid(data.csl_name)) $('#csl_name').val(data.csl_name);
				if(is_valid(data.csl_tel)) {
					var arr_csl_tel = data.csl_tel.split('-');
					$('#csl_tel01').val(arr_csl_tel[0]);
					$('#csl_tel02').val(arr_csl_tel[1]);
					$('#csl_tel03').val(arr_csl_tel[2]);
				}
				// 회사 업종 
				if(is_valid(data.comp_kind_cd)) {
					$('input:radio[name=comp_kind_cd][value="'+ Base64.encode(data.comp_kind_cd) +'"]').prop('checked', true);// 기타입력 항목
					$('input[name=comp_kind_etc]').val(data.comp_kind_etc);
				}
				// 소재지
				if(is_valid(data.comp_addr_cd)) {
					$('select[name=comp_addr_cd] option[value="'+ Base64.encode(data.comp_addr_cd) +'"]').prop('selected', true);
					// 기타인 경우
					if($('select[name=comp_addr_cd] option:selected').text() == '기타') {
						$('input[name=comp_addr_etc]').show();
						$('input[name=comp_addr_etc]').val(data.comp_addr_etc);
					}
				}
				// 근로자수
				if(is_valid(data.emp_cnt_cd)) $('input:radio[name=emp_cnt_cd][value="'+ Base64.encode(data.emp_cnt_cd) +'"]').prop('checked', true);
				// 운영기간 
				if(is_valid(data.oper_period_cd)) $('input:radio[name=oper_period_cd][value="'+ Base64.encode(data.oper_period_cd) +'"]').prop('checked', true);
				// 근로계약서 작성여부 
				if(is_valid(data.emp_paper_yn_cd)) $('input:radio[name=emp_paper_yn_cd][value="'+ Base64.encode(data.emp_paper_yn_cd) +'"]').prop('checked', true);
				// 4대보험 가입여부 
				if(is_valid(data.emp_insured_yn_cd)) $('input:radio[name=emp_insured_yn_cd][value="'+ Base64.encode(data.emp_insured_yn_cd) +'"]').prop('checked', true);
				// 취업규칙 작성여부
				if(is_valid(data.emp_rules_yn_cd)) $('input:radio[name=emp_rules_yn_cd][value="'+ Base64.encode(data.emp_rules_yn_cd) +'"]').prop('checked', true);
				$('#csl_cmp_nm').focus();
			}
		};
		var data = encodeURI('&nm='+ $('#csl_cmp_nm').val());
		var url = '/?c=biz_counsel&m=counsel_rel_view&seq='+ $('input[name=seq]').val() + data;
		gLayerId = openLayerModalPopup(url, 800, 635, '', '', '', closedCallback, 1, true);
	});

	// 상담내용->불러오기 버튼 이벤트 핸들러
	$('#btnGetExistCounsel').click(function(e){
		e.preventDefault();
		$('#focusOuter').focus();
		$('#loading').show();
		var closedCallback = function(data) {
			if(data) {
				if(data.title) $('#csl_title').val(data.title);
				if(data.content) $('#csl_content').val(data.content);
				if(data.reply) $('#csl_reply').val(data.reply);
			}
			$('#csl_title').focus();
		};
		var url = '/?c=biz_counsel&m=counsel_list_popup_view&seq='+ $('input[name=seq]').val();
		gLayerId = openLayerModalPopup(url, 800, 710, '', '', '', closedCallback, 1, true);

		// jquery tooltip - 태그에 title 에 지정된 텍스트가 대상임
		$('#'+gLayerId).tooltip({
			track: true
			,position: {
				my: "center bottom-10",
				at: "center top",
				using: function( position, feedback ) {					
					// 툴팁의 top, bottom이 화면에 가려질 경우 처리
					if(position.top < 0 || $('#list').offset().top + position.top + $(this).height() > window.screen.height) {
						position.top = 10;
						$(this).css({
							width: 1000
							,minWidth: 1000
						});
					}
					$(this).css(position).css({
						background: '#fff'
						,zIndex: 999999
					});//.css('box-shadow', '0 0 3px black');
				}
			}
	  });
	});

	// 거주지/소재지 변경 이벤트 핸들러
	$('#comp_addr_cd').change(function(){
		var selopTxt = $("#comp_addr_cd option:selected").text();
		//
		if(selopTxt == '경기도' || selopTxt == '기타') {
			$('#comp_addr_etc').css('display', 'inline').focus();
		}
		else {
			$('#comp_addr_etc').css('display', 'none');
			$('#comp_addr_etc').val('');
		}
	});

	// 상담방법 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_s_code = "<?php echo base64_encode($csl['s_code']);?>";
	$("input:radio[name=s_code]").each(function(i){
		if($(this).val() == csl_s_code) {
			if($(this).next().text() == '기타') {
				$('#s_code_etc').css('display', 'inline');
				var s_code_etc = "<?php echo $csl['s_code_etc'];?>";
				$('#s_code_etc').val( s_code_etc );
			}
			else {
				$('#s_code_etc').val('');
			}
		}
	});	
	// 상담방법 - 기타 선택한 경우, input box 활성화
	$('input:radio[name=s_code]').click(function(e){
		var display = 'none';
		var len = $('#s_code_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).attr('id') == 's_code_'+len ) display = 'inline';
		$('#s_code_etc_holder').css('display', display);
		if($(this).attr('id') == 's_code_'+len ) $('#s_code_etc').focus();
	});
	if($('#s_code_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#s_code_etc_holder').css('display', 'none');
	}

	// 상담결과 - 기타가 선택되어 있는 경우 입력박스 show/hide - 수정에서 사용
	var csl_proc_rst_cd = "<?php echo base64_encode($csl['csl_proc_rst_cd']);?>";
	$("input:radio[name=csl_proc_rst_cd]").each(function(i){
		if($(this).val() == csl_proc_rst_cd) {
			if($(this).next().text() == '기타') {
				$('#csl_proc_rst_etc').css('display', 'inline');
				var csl_proc_rst_etc = "<?php echo $csl['csl_proc_rst_etc'];?>";
				$('#csl_proc_rst_etc').val( csl_proc_rst_etc );
			}
			else {
				$('#csl_proc_rst_etc').val('');
			}
		}
	});
	// 상담결과 - 기타 선택한 경우, input box 활성화
	$('input[name=csl_proc_rst_cd]').click(function(e){
		// console.log( $(this).next().text() );

		var display = 'none';
		var len = $('#csl_proc_rst_etc_holder').attr('data-role-etc_index'); // 처리결과항목 개수
		// 기타 항목 선택인 경우, 입력박스 노출
		if($(this).attr('id') == 'csl_proc_rst_cd_'+len ) display = 'inline';
		$('#csl_proc_rst_etc_holder').css('display', display);
		if($(this).attr('id') == 'csl_proc_rst_cd_'+len ) $('#csl_proc_rst_etc').focus();
	});
	if($('#csl_proc_rst_etc').val() == '') { // 기타항목에 값이 없으면 hidden 처리
		$('#csl_proc_rst_etc_holder').css('display', 'none');
	}


	// focus
	$('#csl_cmp_nm').focus()
	.keypress(function(e) {
		if (e.keyCode == 13) {
			$("#btnRelViewCounsel").click();
		}
	});

	// focusOut 용
	$(window).scroll(function() {
		$('#focusOuter').animate({top:$(window).scrollTop()+"px" },{queue: false, duration: 0});    
	});

	// 코드 설명글(상위코드) 적용 - 추가 2019.07.08 dylan
	//var arrCodeDesc = eval('<?php //echo json_encode($res['mcode_desc']);?>');
// 	$('table th').each(function(i,o){
// 		for(var i=0; i<arrCodeDesc.length; i++){
// 			if($(o).text().indexOf(arrCodeDesc[i].code_name) != -1) {
// 				$(o).html('<a class="openCodeDescPopup" style="cursor:help;"><textarea style="display:none;">'+ arrCodeDesc[i].code_desc +'</textarea>'+ $(o).html() +'&nbsp;<img src="../images/common/icon_que2.png" alt="'+ arrCodeDesc[i].code_name +'이란" class="imgM"></a>');
// 				break;
// 			}
// 		}
// 	});
	// 코드 설명글(하위코드) 적용
	var arrCodeDesc = eval('<?php echo json_encode($res['scode_desc']);?>');
	$('table td label').each(function(i,o){
		for(var i=0; i<arrCodeDesc.length; i++){
			if($(o).prev()[0].tagName == 'INPUT' && $(o).prev().val() == $.base64.encode(arrCodeDesc[i].s_code)) {
// 				$(o).append('&nbsp;<a class="openCodeDescPopup" style="cursor:help;text-decoration:underline;"><img src="../images/common/icon_que2.png" alt="'+ arrCodeDesc[i].code_name +'(이)란" class="imgM" align="bottom"><textarea style="display:none;">'+ arrCodeDesc[i].code_desc +'</textarea></a>');
				$(o).addClass('openCodeDescPopup').text($(o).text());
				$(o).append('<textarea style="display:none;">'+ arrCodeDesc[i].code_desc +'</textarea>');
				break;
			}
		}
	});
	// help 내용 적용시 테이블의 스타일이 사라지는 문제로 테이블 숨긴 후 다 적용한 후 보여주도록 한다.
	$('table th, td').css('font-size', '12px');
	$('table').show();
});

/**
  * 입력받은 jquery 객체 중 각 앨리먼트 다음에 위치한 태그의 값이 "무응답"을 찾아 index 리턴
  */
function find_text_in_elms(jq, find_text) {
	var ft = typeof find_text === 'undefined' ? '무응답' : find_text;
	var rst  = 0;
	jq.each(function(i, ele){
		if($(this).next().text() == ft) {
			rst = i;
		}
	});
	return rst;
}

/**
  * 안내 팝업
  */
function open_help(index) {
	var url = '/?c=biz_counsel&m=counsel_help&url=./help/adm_counsel_help'+ index +'_popup_view';
	var h = 300, w = 500;
	if(index == 3) {
		h = 570;
		w = 750;
	}
	gLayerId = openLayerModalPopup(url, w, h, '', '', '', '', 1, true);
}



$(document).ready(function(e){
	
	var arr_func = [];

	// 팝업 공지
	var arr_popup_noti_seq = popup_noti_seq.split(',');
	var func = null;
	for(var i=0; i<arr_popup_noti_seq.length; i++) {
		if(arr_popup_noti_seq[i] && ! $.cookie('labors_hide_popup_'+ arr_popup_noti_seq[i])) {
			var url = '"/?c=board&m=open_notice_popup&seq='+ arr_popup_noti_seq[i] +'"';
			func = function(url) {
				openLayerModalPopup(url, 500, 500,'','','','',1,true,null,true,true);
			};
			arr_func.push('func('+url+')');
		}
	}

	arr_func.forEach(function(item, index, array){
		if(index == 0) {
			eval(item);
		}
		else {
			// 첫번째 이후 팝업 생성과 컨텐츠 적용이 순차적으로 정확히 적용되도록 딜레이를 준다.
			setTimeout(function(){eval(item);},100);
		}
	});
});

function is_valid(d) {
// 	console.log('is_valid: ', d, "typeof:", typeof d);
// 	console.log(typeof d == 'undefined');
// 	console.log("d == 'null'", d == 'null');
// 	console.log("d == null", d == null);
// 	console.log("d == undefined", d == undefined);
	if(typeof d == 'undefined' || d == 'null' || d == null || !d || d == undefined || d == "undefined") {
		return false;
	}
	return true;
}
</script>



<?php
include_once './inc/inc_menu.php';
?>	
		
		
		<!-- //  contents_body  area -->
		<div id="contents_body">
			<div id="cont_head">
				<h2 class="h2_tit">사용자상담 입력</h2>
				<span class="location">홈 > 사용자상담관리 > <em>사용자상담입력</em></span>
			</div>
			<div class="cont_area">
				<form name="frmForm" id="frmForm" method="post">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>"></input>
				<input type="hidden" name="kind" value="<?php echo $res['kind'] == 'menu' ? 'add' : $res['kind']; ?>"></input>
				<input type="hidden" name="csl_ref_seq" id="csl_ref_seq" value="<?php echo $csl['csl_ref_seq']; ?>"></input>
				<input type="hidden" name="seq" id="seq" value="<?php echo $csl['seq']; ?>"></input>
				<h3 class="sub_stit">
					상담경로
					<div class="marginT10 textR" style="margin-top:-18px;">
						<button type="button" class="buttonM bGray cssShowCodeDescPopup" style="background:#eaeaea;">코드설명팝업보기</button>
						<button type="button" class="buttonM bGray cssPrint">인쇄</button>
						<button type="button" class="buttonM bSteelBlue cssAdd">등록</button>
						<button type="button" class="buttonM bOrange cssDel">삭제</button>
						<button type="button" class="buttonM bDarkGray cssList">목록</button>
					</div>
				</h3>
				<table class="tInsert" style="display:none;"> 
					<caption>
						상담경로 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>소속 <span class="fRed">*</span></th>
						<td><?php echo $csl['asso_name']; ?>
							<input type="hidden" id="asso_code" name="asso_code" value="<?php echo base64_encode($csl['asso_code']); ?>"></input>
						</td>
					</tr>
					<tr>
						<th>상담자 <span class="fRed">*</span></th>
						<td><?php echo $csl['oper_name']; ?></td>
					</tr>
					<tr>
						<th>상담일 <span class="fRed">*</span></th>
						<td><input type="text" id="csl_date" name="csl_date" class="datepicker date" value="<?php echo $csl['csl_date'];?>" readonly="readonly" style="width:80px"></td>
					</tr>
					<tr style="height:51px;">
						<th>상담방법 <span class="fRed">*</span></th>
						<td>
						<?php
							$index = 0; $index_etc = 0;
							$etc_item = array('index'=>0, 'item'=>new stdClass());
							foreach($res['s_code'] as $item) {
								// 기타 항목은 항상 끝에 배치한다.
								if($item->code_name == '기타') {
									$etc_item['index'] = $index;
									$etc_item['item'] = $item;
									$index_etc = $index;
								}
								else {
									echo '<input type="radio" name="s_code" class="imgM" id="s_code_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['s_code'] == $item->s_code ? 'checked="checked"' : '') .'><label for="s_code_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
								}
								$index++;
							}
							// 기타 항목
							echo '<input type="radio" name="s_code" class="imgM" id="s_code_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($csl['s_code'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="s_code_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="s_code_etc_holder" data-role-etc_index="<?php echo $index_etc;?>">
								<input type="text" name="s_code_etc" id="s_code_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>	
					</tr>
				</table>	
				<h3 class="sub_stit">사업장 현황</h3>
				<table class="tInsert" style="display:none;"> 
					<caption>
						내담자 정보 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>사업장명</th>
						<td><input type="text" name="csl_cmp_nm" id="csl_cmp_nm" class="imgM" size="10" maxlength="100" value="<?php echo $csl['csl_cmp_nm'];?>">&nbsp;
							<button type="button" class="buttonS bGray marginT03" id="btnRelViewCounsel">관련상담</button>
							<span style="marginL10">* 재상담, 지속상담의 경우 성명 입력후 "관련상담" 버튼을 클릭하시면 관련상담을 찾아볼 수 있습니다.</span></td>
					</tr>
					<tr>
						<th>대표자명</th>
						<td>
							<input type="text" name="csl_name" id="csl_name" class="imgM" size="10" maxlength="10" value="<?php echo $csl['csl_name'];?>">
					</tr>
					<tr>
						<th>연락처</th>
						<td>
							<input type="text" name="csl_tel01" id="csl_tel01" class="imgM" size="4" maxlength="4" value="<?php echo $csl['csl_tel01'];?>"> - 
							<input type="text" name="csl_tel02" id="csl_tel02" class="imgM" size="4" maxlength="4" value="<?php echo $csl['csl_tel02'];?>"> - 
							<input type="text" name="csl_tel03" id="csl_tel03" class="imgM" size="4" maxlength="4" value="<?php echo $csl['csl_tel03'];?>">
						</td>
					</tr>
					<tr>
						<th>소재지 <span class="fRed">*</span><br>(구단위)</th>
						<td>
							<select class="styled" id="comp_addr_cd" name="comp_addr_cd">
								<?php
								$index = 0;
								foreach($res['comp_addr_cd'] as $item) {
									echo '<option value="'. base64_encode($item->s_code) .'" '. ($csl['comp_addr_cd'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
								}
								?>
							</select>
							<input type="text" class="" id="comp_addr_etc" name="comp_addr_etc" style="width:350px;display:none" value="<?php echo $csl['comp_addr_etc'];?>" maxlength="30" placeholder="최대 30자"> 
						</td>										
					</tr>
					<tr>
						<th>근로자수 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_cnt_cd'] as $item) {
							echo '<input type="radio" name="emp_cnt_cd" class="imgM" id="emp_cnt_cd_'. $index .'" value="'. base64_encode($item->s_code) .'"><label for="emp_cnt_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?><br>
						<input type="text" name="emp_cnt_etc" style="width:100px;" value="<?php echo $csl['emp_cnt_etc'];?>" maxlength="30" placeholder=""> 
						</td>	
					</tr>
					<tr>
						<th>업종 <span class="fRed">*</span> <a href="javascript:open_help(2)"><img src="../images/common/icon_que.png" alt="업종이란" class="imgM"></a></th></th>
						<td>
						<?php
						$index = 0;
						foreach($res['comp_kind_cd'] as $item) {
							echo '<input type="radio" name="comp_kind_cd" class="imgM" id="comp_kind_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['comp_kind_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="comp_kind_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?><br>
						추가내용: <input type="text" name="comp_kind_etc" style="width:100px;" value="<?php echo $csl['comp_kind_etc'];?>" maxlength="30" placeholder=""> 
						</td>	
					</tr>	
					<tr>
						<th>운영기간 <span class="fRed">*</span></th>
						<td>
							<select class="styled" id="oper_period_cd" name="oper_period_cd">
								<?php
								$index = 0;
								foreach($res['oper_period_cd'] as $item) {
									echo '<option value="'. base64_encode($item->s_code) .'" '. ($csl['oper_period_cd'] == $item->s_code ? 'selected' : '') .'>'. $item->code_name .'</option>'; 
								}
								?>
							</select>
						</td>						
					</tr>
					<tr>
						<th>근로계약서 작성여부 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_paper_yn_cd'] as $item) {
							echo '<input type="radio" name="emp_paper_yn_cd" class="imgM" id="emp_paper_yn_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['emp_paper_yn_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_paper_yn_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>
					</tr>
					<tr>
						<th>4대보험 가입여부 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_insured_yn_cd'] as $item) {
							echo '<input type="radio" name="emp_insured_yn_cd" class="imgM" id="emp_insured_yn_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['emp_insured_yn_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_insured_yn_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>
					<tr>
						<th>취업규칙 작성여부 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['emp_rules_yn_cd'] as $item) {
							echo '<input type="radio" name="emp_rules_yn_cd" class="imgM" id="emp_rules_yn_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['emp_rules_yn_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="emp_rules_yn_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>						
					</tr>
				</table>
				
				<h3 class="sub_stit">상담유형 및 처리결과</h3>
				<table class="tInsert" style="display:none;"> 
					<caption>
						상담유형 및 처리결과 입력 테이블 입니다.
					</caption>
					<colgroup>
						<col style="width:14%">
						<col style="width:86%">
					</colgroup>
					<tr>
						<th>상담유형 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_kind_cd'] as $item) {
							echo '<input type="checkbox" name="csl_kind[]" id="csl_kind_'. $index .'" value="'. base64_encode($item->s_code) .'" '. (strpos($csl['csl_sub_code'], $item->s_code)!==FALSE ? 'checked="checked"' : '') .' class="imgM"><label for="csl_kind_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>
					</tr>
					<tr>
						<th>상담내용 <span class="fRed">*</span></th>
						<td>
							<button type="button" class="buttonS bGray marginT03" id="btnGetExistCounsel">불러오기</button>
							<input type="text" name="csl_title" id="csl_title" style="width:83%;" maxlength="200" class="input_font_size14" value="<?php echo $csl['csl_title'];?>">
							<textarea class="marginT05 input_font_size14" style="width:90%;height:150px;" name="csl_content" id="csl_content"><?php echo $csl['csl_content'];?></textarea>
						</td>						
					</tr>
					<tr>
						<th>답변 <span class="fRed">*</span></th>
						<td><textarea class="input_font_size14" style="width:90%;height:150px;" name="csl_reply" id="csl_reply"><?php echo $csl['csl_reply'];?></textarea></td>
					</tr>
					<tr>
						<th>처리결과 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0; $index_etc = 0;
						$etc_item = array('index'=>0, 'item'=>new stdClass());
						foreach($res['csl_proc_rst_cd'] as $item) {
							// 기타 항목은 항상 끝에 배치한다.
							if($item->code_name == '기타') {
								$etc_item['index'] = $index;
								$etc_item['item'] = $item;
								$index_etc = $index;
							}
							else {
								echo '<input type="radio" name="csl_proc_rst_cd" class="imgM" id="csl_proc_rst_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['csl_proc_rst_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_proc_rst_cd_'. $index .'">'. $item->code_name .'</label>&nbsp;'; 
							}
							$index++;
						}
						// 기타 항목
						echo '<input type="radio" name="csl_proc_rst_cd" class="imgM" id="csl_proc_rst_cd_'. $etc_item['index'] .'" value="'. base64_encode($etc_item['item']->s_code) .'" '. ($csl['csl_proc_rst_cd'] == $etc_item['item']->s_code ? 'checked="checked"' : '') .'><label for="csl_proc_rst_cd_'. $etc_item['index'] .'">'. $etc_item['item']->code_name .'</label>&nbsp;'; 
						?>
							<span id="csl_proc_rst_etc_holder" data-role-etc_index="<?php echo $index_etc;?>">
								<input type="text" name="csl_proc_rst_etc" id="csl_proc_rst_etc" style="width:550px" maxlength="50" placeholder="최대 50자">
							</span>
						</td>
					</tr>
					<tr>
						<th>주제어</th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_keyword_cd'] as $item) {
							echo '<input type="checkbox" name="csl_keyword[]" id="csl_keyword_'. $index .'" value="'. base64_encode($item->s_code) .'" '. (strpos($csl['csl_sub_code'], $item->s_code)!==FALSE ? 'checked="checked"' : '') .' class="imgM"><label for="csl_keyword_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
						</td>
					</tr>	
					<tr>
						<th>상담사례 공유 <span class="fRed">*</span></th>
						<td>
						<?php
						$index = 0;
						foreach($res['csl_share_yn_cd'] as $item) {
							echo '<input type="radio" name="csl_share_yn_cd" class="imgM" id="csl_share_yn_cd_'. $index .'" value="'. base64_encode($item->s_code) .'" '. ($csl['csl_share_yn_cd'] == $item->s_code ? 'checked="checked"' : '') .'><label for="csl_share_yn_cd_'. $index++ .'">'. $item->code_name .'</label>&nbsp;'; 
						}
						?>
							<div class="f11 marginT05 marginL10">* "공유함" 선택시 상담내용 불러오기, 주요 사용자 상담사례 게시판에 보여지게 됩니다.</div>
						</td>						
					</tr>
				</table>

				<div class="marginT10 textR">
					<button type="button" class="buttonM bGray cssPrint">인쇄</button>
					<button type="button" class="buttonM bSteelBlue cssAdd">등록</button>
					<button type="button" class="buttonM bOrange cssDel">삭제</button>
					<button type="button" class="buttonM bDarkGray cssList">목록</button>
				</div>
				</form>

			</div>
			<!-- //컨텐츠 -->

			<a href="#" id="focusOuter" style="position:absolute;left:0px;top:0px;"></a>
		</div>
		<!-- //  contents_body  area -->
		
		<?php
		// 상담내용 불러오기 팝업에서 사용할 검색 항목데이터
		$data_csl_proc_rst_code = '';
		foreach($res['csl_proc_rst_cd'] as $rs) {
			if($data_csl_proc_rst_code !='') {
				$data_csl_proc_rst_code .= '|';
			}
			$data_csl_proc_rst_code .= base64_encode($rs->s_code) .'*'. $rs->code_name;
		}
		?>
		<span id="csl_proc_rst_code" style="display:none;width:0px;"><?php echo $data_csl_proc_rst_code;?></span>


<?php
include_once './inc/inc_code_desc.php';
include_once './inc/inc_footer.php';
?>
