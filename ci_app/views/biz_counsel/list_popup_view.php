<?php

// 상담내용 불러오기 팝업

?>

<script>
var seq = "<?php echo $res['seq'];?>"; // 자신의 글은 가져오지 않도록 한다.

$(document).ready(function(){
	// 검색박스 간격조정
	$('.con_text').css({paddingTop:10,paddingBottom:10});

	// 페이지 초기화 : 팝업이 2개라 영향을 준다.
	_page = 1;

	// date setup
	setup_datepicker({date:'search_date_begin'});
	setup_datepicker({date:'search_date_end'});

	// 전송버튼
	$('#btnSubmitPop').click(function(e){
		_page = 1;
		e.preventDefault();
		// validation - 업종, 처리결과, 키워드 외 검색어 미입력 체크
		var sel = $('select[name=target] option:selected').val();
		if(sel != '' && (sel != 'counsel_comp_kind' && sel != 'counsel_keyword' && sel != 'counsel_proc_rst') && $('input[name=keyword]').val() == '') {
			alert(CFG_MSG[CFG_LOCALE]['info_cmm_11']);
			return;
		} 
		get_list_pop(_page);
	});
	
	// 초기화 이벤트 핸들러
	$('#btnClear').click(function(e){
		e.preventDefault();
		_page = 1;
		$('form[name=frmSearchPop]')[0].reset();
		$('#keyword').val('').css('display', 'inline');
		$('#target_keyword').css('display', 'none'); // 주제어
		$('#target_comp_kind').css('display', 'none'); // 업종
		$('#target_csl_proc_rst').css('display', 'none'); // 처리결과
	});
		

	// 검색 - 검색어 변경시 처리
	$('#target').change(function(e){
		var sel = $('#target option:selected').val();
		var display1 = display3 = display4 = 'none';
		display1 = sel == 'counsel_keyword' ? 'inline' : 'none';
		display3 = sel == 'counsel_comp_kind' ? 'inline' : 'none';
		display4 = sel == 'counsel_proc_rst' ? 'inline' : 'none';

		var keyword_val = '';
		var kwyword_display = 'inline';
		$('#keyword').css('display', 'inline');
		if(sel == 'counsel_keyword' || sel == 'counsel_comp_kind' || sel == 'counsel_proc_rst') {
			keyword_val = '';
			// 직종,업종은 keyword를 항상 노출한다.
			if(sel == 'counsel_keyword' || sel == 'counsel_proc_rst') {
				kwyword_display = 'none';
			}
		}
		$('#target_keyword').css('display', display1); // 주제어
		$('#target_comp_kind').css('display', display3); // 업종
		$('#target_csl_proc_rst').css('display', display4); // 처리결과
		$('#keyword').val(keyword_val).css('display', kwyword_display);
	});

	// 검색 - 처리결과 변경시
	$('#target_csl_proc_rst').change(function(e){
		// 기타 선택시 - 입력박스 show
		var selText = $('#target_csl_proc_rst option:selected').text();
		var display = 'none';
		if(selText == '기타') {
			display = 'inline';
		}
		$('#keyword').val("").css('display', display);
	});

	// 검색 - 검색어 엔터 처리
	$("#keyword").keypress(function(e) {
		if (e.keyCode == 13) {
			$("#btnSubmitPop").click();
		}
	});
	
	// 닫기버튼
	$('#btnClosePop').click(function(e){
		closeLayerPopup({});
	});

	// 검색 - 오늘,일주일,한달,전체
	$('#btnToday,#btnWeek,#btnMonth,#btnAllTerm').click(function(e){
		var date_begin='', date_end=get_today();

		if($(this).attr('id') == 'btnToday') {
			date_begin = get_today();
		}
		else if($(this).attr('id') == 'btnWeek') {
			date_begin = get_week_ago();
		}
		else if($(this).attr('id') == 'btnMonth') {
			date_begin = get_xmonth_ago(1);
		}
		else {
			date_end = '';
		}

		$('#search_date_begin').val(date_begin);
		$('#search_date_end').val(date_end);
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	});	

	$('#sel_year').change(function(){
		$('#sel_month').val('').prop('selected', true);
		fill_list_search_dates();
	});

	$('#sel_month').change(function(){
		fill_list_search_dates($(this).val());
	});


	get_list_pop(_page);
});


/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list_pop(page) {
	// page
	_page = page;
	
	// request
	var url = '/?c=biz_counsel&m=counsel_list_popup';
	var data = $('#frmSearchPop').serialize() +'&seq='+ seq +'&page='+_page;
	var fnSuccess = function(data) {
		_cfg_pagination.total_item = data.tot_cnt == 0 ? 1 : data.tot_cnt;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list_pop';
		_pagination = new Pagination(_cfg_pagination);
		// list
		gen_list(data);
	};
	var fnFailure = function(data) {
		if(is_local) objectPrint(data);
		
		// empty list
		_cfg_pagination.total_item = 1;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = 1;
		_cfg_pagination.linkFunc = 'get_list_pop';
		_pagination = new Pagination(_cfg_pagination);
		gen_list();
	};
	req_ajax(url, data, fnSuccess, fnFailure);
}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var html_b = '<table class="tList">';
	html_b += '<caption>상담내역조회 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:17%">';
	html_b += '<col>';
	html_b += '<col style="width:15%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th>번호</th>';
	html_b += '<th>유형</th>';
	html_b += '<th>제목</th>';
	html_b += '<th>적용</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		var txt_csl_share_yn = '';
		for(var i=begin; i<end; i++) {
			// " 제거
			csl_title = data.data[index].csl_title.replaceAll('"', '');
			csl_content = data.data[index].csl_content.replaceAll('"', '');
			csl_reply = data.data[index].csl_reply.replaceAll('"', '');

			// 주요상담사례(공유되는 상담) 여부
			txt_csl_share_yn = data.data[index].csl_share_yn == "<?php echo CFG_SUB_CODE_CSL_SHARE_YN?>" ? '<span style="color:blue">[주요상담사례]</span> ' : '';
			//
			html_b += '<tr>';
			html_b += '  <td>'+ (no--) +'</td>';
			// 상담유형 아이콘화 색상
			var iconCss = 'labelB';
			if(data.data[index].s_code == 'C002') iconCss = 'labelGr';
			if(data.data[index].s_code == 'C003') iconCss = 'labelO';
			if(data.data[index].s_code == 'C004') iconCss = 'labelP';
			if(data.data[index].s_code == 'C005') iconCss = 'labelG';
			html_b += '  <td><span class="'+ iconCss +'">'+ data.data[index].csl_method_nm +'</span></td>';
			// 수정 : 상담내용 tooltip 추가, 2017.01.18 by dylan
			html_b += '  <td class="textL"><span title="<b>[답변]</b> '+ csl_reply +'">'+ txt_csl_share_yn + csl_title +'</span></td>';	
			html_b += '  <td><span '+
				'data-seq="'+ data.data[index].seq +'" '+
				'data-title="'+ csl_title +'" '+
				'data-cts="'+ csl_content +'" '+
				'data-rpy="'+ csl_reply +'"></span><button type="button" class="buttonS bGray cssApplyPop">적용</button></td>';
			html_b += '</tr>';
			
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="4" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// 전체 100개 / 검색 <span class="fBold">50 개</span>
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b).find('td').css({paddingTop:5,paddingBottom:5}).end().find('th').css({lineHeight:'30px',height:30});
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());

	// 적용버튼 클릭 핸들러
	$('button.cssApplyPop').click(function(e){
		$this = $(this).prev();
		var oData = {
			seq: $this.attr('data-seq')
			,title: $this.attr('data-title')
			,content: $this.attr('data-cts')
			,reply: $this.attr('data-rpy')
		};
		closeLayerPopup(oData);
	});
}

</script>



    <div id="popup">
        <header id="pop_header">
			<h2 class="">상담내역 불러오기</h2>			
        </header>
		<div id="popup_contents">
			<div class="">
				<form name="frmSearchPop" id="frmSearchPop" method="post">
				<div class="con_text">
					<label for="csl_way" class="l_title">상담방법</label>
					<select id="csl_way" name="csl_way" class="styled" style="width:80px">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_way'] as $item) {
							echo '<option value="'. base64_encode($item->s_code) .'">'. $item->code_name  .'</option>';
						}
						?>
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<label for="board_select" class="l_title">상담일</label>
					<input type="text" id="search_date_begin" name="search_date_begin" class="datepicker date" style="width:80px" readonly="readonly"> ~ 
					<input type="text" id="search_date_end" name="search_date_end" class="datepicker date" style="width:80px" readonly="readonly">
					<button type="button" id="btnToday" class="buttonS bGray">오늘</button>
					<button type="button" id="btnWeek" class="buttonS bGray">일주일</button>
					<button type="button" id="btnMonth" class="buttonS bGray">한달</button>
					<button type="button" id="btnAllTerm" class="buttonS bGray">전체</button><br>
					<span class="divice"></span>
					<label for="target" class="l_title">검색어</label>
					<select id="target" class="styled" name="target">
						<option value="" selected>전체</option>
						<option value="Q1.csl_title">제목</option>
						<option value="Q1.csl_content">상담 내용</option>
						<option value="Q1.csl_reply">답변</option>
						<option value="counsel_comp_kind">업종</option>
						<option value="counsel_keyword">주제어</option>
						<option value="counsel_proc_rst">처리결과</option>
						<option value="counsel_oper_id">상담자ID</option>
						<option value="counsel_oper_name">상담자</option>
						<option value="counsel_cmp_nm">사업장명</option>
						<option value="counsel_csl_name">대표자명</option>
						<option value="counsel_csl_tel">연락처 뒤4자리</option>
					</select>
					<!-- 주제어 항목 -->
					<select id="target_keyword" class="styled" name="target_keyword" style="width:100px;display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_keyword'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<!-- 업종 항목 -->
					<select id="target_comp_kind" class="styled" name="target_comp_kind" style="width:100px;display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_comp_kind_code'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<!-- 처리결과 항목 -->
					<select id="target_csl_proc_rst" class="styled" name="target_csl_proc_rst" style="width:100px;display:none">
						<option value="" selected>전체</option>
						<?php
						foreach($res['csl_proc_rst_code'] as $rs) {
							echo '<option value="'. base64_encode($rs->s_code) .'" >'. $rs->code_name .'</option>';
						}
						?>
					</select>
					<!--주제어,처리결과 중 기타 외 선택시 해당 코드값 넘기는 용도도 겸함 -->
					<!--검색대상:직종인 경우 검색어의 내용을 직종 추가 입력 항목에 대해서 검색한다. 전체 검색일 경우도 마찬가지. -->
					<input type="text" style="width:177px;" id="keyword" name="keyword">
					<select id="sel_year" name="sel_year" style="margin-left:0px;">
						<option value="">연도</option>
						<?php
						foreach($res['search_year'] as $item) {
							echo '<option value="'. $item->year .'">'. $item->year .'</option>';
						}
						?>
					</select>
					<select id="sel_month" name="sel_month">
						<option value="">전체</option>
						<option value="01">1월</option>
						<option value="02">2월</option>
						<option value="03">3월</option>
						<option value="04">4월</option>
						<option value="05">5월</option>
						<option value="06">6월</option>
						<option value="07">7월</option>
						<option value="08">8월</option>
						<option value="09">9월</option>
						<option value="10">10월</option>
						<option value="11">11월</option>
						<option value="12">12월</option>
					</select>
					<span class="divice"></span>
					<div class="textR marginT10" style="margin:-38px -10px 0 0;">
						<button type="button" id="btnClear" class="buttonS bGray">초기화</button>
						<button type="button" id="btnSubmitPop" class="buttonS bBlack">검색</button>
					</div>
				</div>
				</form>
				
				<div class="list_no" style="margin-top:12px;"></div>	
				
				<!-- list -->
				<div id="list"></div>
				
				<!-- pagination-->
				<div class="tPages">
					<ul class="pages"></ul>
				</div>
				
			</div>
		</div>
		<div class="btn_set" style="padding:0px;margin-top:-19px;">
			<button type="button" id="btnClosePop" class="buttonM bGray">닫기</button>
		</div>
    </div>
