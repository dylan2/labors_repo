<script>
var nm = "<?php echo $data['csl_name'];?>";
var seq = "<?php echo $data['seq'];?>"; // 자신의 글은 가져오지 않도록 한다.

$(document).ready(function(){
	// 상단 간격조정
	$('#popup_contents').css({paddingTop:0,paddingBottom:10});

	// 페이지 초기화 : 팝업이 2개라 영향을 준다.
	_page = 1;

	// 닫기버튼
	$('#btnClosePop').click(function(e){
		closeLayerPopup();
	});

	get_list_pop(_page);	
});


/**
 * 서버로 리스트 데이터 요청 및 생성, 페이징 생성
 */
function get_list_pop(page) {
	// page
	_page = page;
	
	// request
	var url = '/?c=biz_counsel&m=counsel_list_popup';
	var data = {'nm':nm, 'page':_page, 'seq':seq };
	var fnSuccess = function(data) {
		_cfg_pagination.total_item = data.tot_cnt == 0 ? 1 : data.tot_cnt;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = _page;
		_cfg_pagination.linkFunc = 'get_list_pop';
		_pagination = new Pagination(_cfg_pagination);
		// list
		gen_list(data);
	};
	var fnFailure = function(data) {
		if(is_local) objectPrint(data);
		
		// empty list
		_cfg_pagination.total_item = 1;
		_cfg_pagination.itemPerPage = 10;
		_cfg_pagination.currentPage = 1;
		_cfg_pagination.linkFunc = 'get_list_pop';
		_pagination = new Pagination(_cfg_pagination);
		gen_list();
	};
	req_ajax(url, data, fnSuccess, fnFailure);

}

/**
 * (주) html이 li태그가 아닌 table 태그를 사용하였다.
 * 서버에서 받아온 데이터를 list html block으로 생성
 * 메뉴별 이벤트 등록
 */
function gen_list(data) {
	var html_b = '<table class="tList">';
	html_b += '<caption>상담내역조회 목록입니다.</caption>';
	html_b += '<colgroup>';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:17%">';
	html_b += '<col style="width:44%">';
	html_b += '<col style="width:15%">';
	html_b += '<col style="width:8%">';
	html_b += '<col style="width:8%">';
	html_b += '</colgroup>';
	html_b += '<tr>';
	html_b += '<th>번호</th>';
	html_b += '<th>유형</th>';
	html_b += '<th>제목</th>';
	html_b += '<th>상담일</th>';
	html_b += '<th>상담</th>';
	html_b += '<th>적용</th>';
	html_b += '</tr>';
	
	var total_cnt = 0;
	if(data && typeof data.data == 'object' && data.tot_cnt > 0) {
		total_cnt = data.tot_cnt;
		_cfg_pagination.total_item = data.tot_cnt;
		
		// list
		var begin = _page-1;
		var end = begin + data.data.length;
		
		var no = total_cnt - (begin * _itemPerPage);
		var index = 0;
		for(var i=begin; i<end; i++) {
			html_b += '<tr data-id="'+ i +'" class="data_tr">';
			html_b += '  <td>'+ (no--) +'</td>';
			// 상담유형 아이콘화 색상
			var iconCss = 'labelB';
			if(data.data[index].s_code == 'C002') iconCss = 'labelGr';//전화
			if(data.data[index].s_code == 'C003') iconCss = 'labelO';//온라인
			if(data.data[index].s_code == 'C004') iconCss = 'labelP';//찾아가는 노동상담
			if(data.data[index].s_code == 'C005') iconCss = 'labelG';//기타
			if(data.data[index].s_code == 'C194') iconCss = 'labelY';//SNS
			html_b += '  <td><span class="'+ iconCss +'">'+  data.data[index].csl_method_nm +'</span></td>';
			html_b += '  <td class="textL">'+ data.data[index].csl_title +'</td>';
			html_b += '  <td>'+ data.data[index].csl_date +'</td>';	
			html_b += '  <td><button type="button" class="buttonS bGray cssViewCounselPop">보기</button></td>';	
			html_b += '  <td><button type="button" class="buttonS bGray cssApplyPop">적용</button>';
			html_b += '<span style="display:none;" '+
				'data-seq="'+ data.data[index].seq +'" '+
				'data-title="'+ data.data[index].csl_title +'" '+
				'data-cts="'+ data.data[index].csl_content.replaceAll('"', '') +'" '+
				'data-rpy="'+ data.data[index].csl_reply +'" '+
				'data-csl_cmp_nm="'+ data.data[index].csl_cmp_nm +'" '+
				'data-csl_name="'+ data.data[index].csl_name +'" '+
				'data-csl_tel="'+ data.data[index].csl_tel +'" '+
				'data-comp_kind_cd="'+ data.data[index].comp_kind_cd +'" '+
				'data-comp_kind_etc="'+ data.data[index].comp_kind_etc +'" '+
				'data-comp_addr_cd="'+ data.data[index].comp_addr_cd +'" '+
				'data-comp_addr_etc="'+ data.data[index].comp_addr_etc +'" '+
				'data-emp_cnt_cd="'+ data.data[index].emp_cnt_cd +'" '+
				'data-emp_cnt_etc="'+ data.data[index].emp_cnt_etc +'" '+
				'data-oper_period_cd="'+ data.data[index].oper_period_cd +'" '+
				'data-emp_paper_yn_cd="'+ data.data[index].emp_paper_yn_cd +'" '+
				'data-emp_insured_yn_cd="'+ data.data[index].emp_insured_yn_cd +'" '+
				'data-emp_rules_yn_cd="'+ data.data[index].emp_rules_yn_cd +'" '+
				'data-csl_ref_seq="'+ data.data[index].csl_ref_seq +'">'+ // 원글 key
				'</span></td>';
			html_b += '</tr>';
			// 숨겨진 상담내용,답변
			html_b += '<tr style="display:none;">';
			html_b += '  <td colspan="6" style="width:100%">';
			html_b += '      <div><h3 class="sub_stit" style="height: 20px;margin: 3px 0px 0px 10px;text-align:left;">상담내용</h3><textarea style="width:97%;height:130px;border:1px solid #eee;overflow-y: auto;-ms-overflow-y: auto;padding: 10px;margin: 0px 20px 0px 0px;text-align:left;" readonly="readonly">'+ data.data[index].csl_content +'</textarea>';
			html_b += '      <h3 class="sub_stit" style="height: 20px;margin: 9px 0px 0px 10px;text-align:left;">답변</h3><textarea style="width:97%;height:130px;border:1px solid #eee;overflow-y: auto;-ms-overflow-y: auto;padding: 10px;margin: 0px 20px 0px 0px;text-align:left;" readonly="readonly">'+ data.data[index].csl_reply +'</textarea></div>';
			html_b += '  </td>';
			html_b += '</tr>';
			index++;
		}
	}
	else {
		html_b += '<tr><td colspan="6" style="align:center">내용이 없습니다.</td></tr>';
	}
	html_b += '</table>';
	
	// total count
	$('div.list_no').html(' 전체 '+ total_cnt + '개');
	
	// list html
	$('#list').empty().html(html_b).find('td').css({paddingTop:5,paddingBottom:5}).end().find('th').css({lineHeight:'30px',height:30});
	
	// pagination block 생성
	$('ul.pages').html(_pagination.toString());

	// 적용버튼 클릭 핸들러
	$('button.cssApplyPop').click(function(e){
		$this = $(this).next();
		var oData = {
			seq: $this.attr('data-seq')
			,title: $this.attr('data-title')
			,content: $this.attr('data-cts')
			,reply: $this.attr('data-rpy')
			,csl_cmp_nm: $this.attr('data-csl_cmp_nm') //
			,csl_name: $this.attr('data-csl_name') //
			,csl_tel: $this.attr('data-csl_tel')
			,comp_kind_cd: $this.attr('data-comp_kind_cd')
			,comp_kind_etc: $this.attr('data-comp_kind_etc')
			,comp_addr_cd: $this.attr('data-comp_addr_cd')
			,comp_addr_etc: $this.attr('data-comp_addr_etc')
			,emp_cnt_cd: $this.attr('data-emp_cnt_cd')
			,emp_cnt_etc: $this.attr('data-emp_cnt_etc')
			,oper_period_cd: $this.attr('data-oper_period_cd')
			,emp_paper_yn_cd: $this.attr('data-emp_paper_yn_cd')
			,emp_insured_yn_cd: $this.attr('data-emp_insured_yn_cd')
			,emp_rules_yn_cd: $this.attr('data-emp_rules_yn_cd')
			,csl_ref_seq: $this.attr('data-csl_ref_seq')
		};
		closeLayerPopup(oData);
	});

	// 제목 클릭 이벤트 핸들러
	$('.cssViewCounselPop').click(function(e){
		e.preventDefault();
		var display = 'table-row';
		var target = $(this).parents('tr');
		if(target.next().css('display') == 'table-row') {
			display = 'none';
		}
		target.next().css('display', display);
		// 다른tr 모두 닫기
		$p = target;
		$('.data_tr').each(function(i, ele){
			if($p.attr('data-id') !== $(ele).attr('data-id')) {
				$(ele).next().css('display', 'none');
			}
		});
	});
}

</script>



	<div id="popup">
		<header id="pop_header">
			<h2 class="">관련상담 리스트</h2>			
		</header>
		<div id="popup_contents" style="overflow-y: auto;-ms-overflow-y: auto;">
			<div class="list_no"></div>	
			<!-- list -->
			<div id="list" style="height:400px;"></div>
		</div>

		<!-- pagination-->
		<div class="tPages">
			<ul class="pages"></ul>
		</div>

		<div class="btn_set">
			<button type="button" id="btnClosePop" class="buttonM bGray">닫기</button>
		</div>
	</div>
