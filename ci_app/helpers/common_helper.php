<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * 공통 - echof
 * @$_prefix : 출력할 데이터 앞쪽에 출력할 문자열
 * @$_obj : 출력할 데이터
 */
if ( ! function_exists('echof') ) {
	function echof($_obj=null, $_prefix=null) {
		// 개발자 모드일때만 출력
// 		if( is_local() ) {
			echo '[echoF] '.$_prefix .' : ';
			if(gettype($_obj)=='array' || gettype($_obj)=='object') {
				echo ' [type:'.gettype($_obj) .'] ';
				print_r($_obj);
			}
			else {
				echo ' '. $_obj;
			}
			echo '<br/>';
// 		}
	}
}



/**
 * 공통 - alert
 * @$_str : 출력할 문자열
 * @$_is_back : 이전페이지로 되돌려 보낼지 여부
 */
if ( ! function_exists('alert') ) {
	function alert($_str, $_is_back=FALSE) {
		$tmp = '<script>alert("'. $_str .'")</script>';
		if($_is_back) {
			$tmp .= '<script>history.back()</script>';
		}
		echo($tmp);
		exit;
	}
}



if ( ! function_exists('go_index_page') ) {
	function go_index_page($bad_access) {
		$tmp = '';
		if($bad_access) {
			$tmp .= '<script>alert(\'잘못된 접근입니다.\')</script>';
		}
		$tmp .= '<script>location.href="'. DOC_ROOT .'";</script>';
		
		echo($tmp);
	}
}



if ( ! function_exists('redirect_js') ) {
	function redirect_js($msg) {
		alert($msg);
		echo '<script type="text/javascript">top.window.location="/"</script>';
		die();
	}
}


/**
 * 공통 - number_to_currency
 * @$_num : 통화형태로 표현하기 위한 입력값(3자리 단위 콤마 찍기)
 */
if ( ! function_exists('number_to_currency') ) {
	function number_to_currency($_num) {
		return @number_format($_num);
	}
}


/**
 * 공통 - create_thumb
 * 입력된 이미지의 썸네일 이미지를 만든다.
 * 생성하는 곳은 해당 이미지와 같은 경로에 생성한다.
 * param $path_img : 이미지 full 경로
 * param $_thumb_width : 생성한 썸네일 가로 크기, 가로 기준으로 정비율 생성한다.
 * param $_lock_ratio : 정비율로 생성할지 여부, defalt : 1
 * param $_prefix : 생성한 썸네일 이름 구분자. 원본 이름 뒤에 붙는다, defalt : '_thumb'
 * return : 생성된 썸네일 이미지 이름
 */
if ( ! function_exists('create_thumb') ) {
	function create_thumb($path_img, $_thumb_width, $_lock_ratio=1, $_prefix='_thumb' )  {
		$rst_name = 'default_thumb.gif';
		
		// 확장자 포함 파일명
		$org_full_name = basename($path_img);
		// 확장자만 추출
		$org_ext = end(explode('.', $org_full_name));
		// 확장자 제외 이름
		$org_nm = basename($path_img, '.'. $org_ext);
		// 넘어온 이미지 경로에서 이름 제외한 경로 - 썸네일 저장 경로가 된다
		$org_path = str_replace($org_full_name, '', $path_img);
		// 최종 저장할 경로 + 파일명
		$save_path = $org_path . $org_nm . $_prefix .'.'. $org_ext;
		/*
		echo ('넘어온 이미지 확장자 포함 파일명 : '. $org_full_name .'<br>');
		echo ('넘어온 이미지확장자 : '. $org_ext .'<br>');
		echo ('넘어온 이미지확장자 제외 이름 : '. $org_nm .'<br>');
		echo ('넘어온 이미지 경로에서 이름 제외한 경로 : '. $org_path .'<br>');
		echo ('최종 저장할 경로 + 파일명 : '. $save_path .'<br>');
		exit;
		*/
		if(file_exists($path_img) === true) {
			$info_image = getimagesize($path_img);
			$width = $info_image[0];
			$height = $info_image[1];
			// echo ('넘어온 이미지 mine type : '. $info_image['mime'] .'<br>');

			switch($info_image['mime']) {
				case "image/gif";
					$new_image = imagecreatefromgif($path_img);
					break;
				case "image/jpeg";
					$new_image = imagecreatefromjpeg($path_img);
					break;
				case "image/png";
					$new_image = imagecreatefrompng($path_img);
					break;
				case "image/bmp";
					$new_image = imagecreatefromwbmp($path_img);
					break;
			}
			
			if ( $new_image ) {
				// calculate thumbnail size
				$new_width = $_thumb_width;
				$new_height = $_lock_ratio == 1 ? floor( $height * ( $_thumb_width / $width ) ) : $_thumb_width;

				// create a new temporary image
				$canvas_img = imagecreatetruecolor( $new_width, $new_height );

				// copy and resize old image into new image 
				imagecopyresampled( $canvas_img, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

				// save thumbnail into a file
				switch($info_image['mime']) {
					case "image/gif";
						// $org_ext = 'gif';
						$rst = imagegif( $canvas_img, $save_path );
						break;
					case "image/jpeg";
						// $org_ext = 'jpeg';
						$rst = imagejpeg( $canvas_img, $save_path );
						break;
					case "image/png";
						// $org_ext = 'png';
						$rst = imagepng( $canvas_img, $save_path );
						break;
					case "image/bmp";
						// $org_ext = 'bmp';
						$rst = imagewbmp( $canvas_img, $save_path );
						break;
				}
				
				// 성공한 경우, 썸네일 이름 조합
				if($rst === true) {
					$rst_name = $save_path;
				}
			}
		}
		// 파일이 없으면 기본이미지 이름 리턴
		
		return $rst_name;
	}
}


/**
 * 공통 - split_file_name
 * 파일 이름에서 확장자를 제거한 이름만 또는 확장자만 리턴한다(경로를 포함한 경우 포함).
 * param $file_name : 파일 이름(경로 있어도 처리)
 * param $ext : 확장자 문자열, 파이프라인(|)으로 구분
 * param $get_type : 요청 구분(0: 입력된 문자열 그대로 리턴, 1:이름만 리턴, 2: 확장자만 리턴)
 * return : 파일이름에 확장자 제외한 문자열 리턴
 */
if ( ! function_exists('split_file_name') ) {
	function split_file_name($file_name, $ext='jpg|png|jpeg|bmp', $get_type=2) {
	   if($file_name && !is_null($file_name)) { 
	      $matches = null;
	      preg_match('/(\w+)\.($ext)$/i', $file_org_name, $matches);
	      // print_r($matches);
	      // -> Array ( [0] => test_watermark_200x50.png [1] => test_watermark_200x50 [2] => png )
	      if(count($matches) > 0) {
	         return $matches[$get_type];
	      }
	   }
	   else {
	      return $file_name;
	   }
	}
}


/**
 * 공통 - is_local
 * 로컬상태여부 체크 함수
 */
if ( ! function_exists('is_local') ) {
	function is_local() {
		return $_SERVER["SERVER_ADDR"] === '127.0.0.1';
	}
}


/**
 *공통 - dv_redirect
 * js로 입력된 url 이동
 */
if ( ! function_exists('dv_redirect') ) {
	function dv_redirect($url='/',$msg='') {
		$tmp = '';
	 
		if($msg) $tmp .= '<script>alert("'. $msg .'");</script>';
	 
		$tmp .= '<script>window.location.href="'. $url .'";</script>';
	 
		$CI =& get_instance();
		echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=".$CI->config->item('charset')."\">";
		echo $tmp;
	 
		exit;
	}
}


/**
 * 공통 - ci_log
 * CI:system>core>Common.php 파일내 log 기록함수 wrapping 함수이다.
 * (주)로그를 기록하지 않으려면 config.php -> $config['log_threshold'] = 0 으로 수정한다.
 * param $level : 로그파일에 기록될 구분, info, debug, error, ..
 * param $message : message
 * param $php_error : native PHP error를 로깅할인지 플래그
 */
if ( ! function_exists('ci_log') ) {
	function ci_log($level = 'error', $message, $php_error = FALSE) {
		log_message($level, $message, $php_error);
	}
}


/**
 * 공통 - get_hash
 * 단반향 암호화, 입력된 데이터를 sha256 hash 처리해서 리턴한다.
 * $params $data : hash할 데이터
 * $params $key : hash에 사용할 key
 */
if ( ! function_exists('dv_hash') ) {
	function dv_hash($data, $key=CFG_ENCRYPT_KEY) {
		// $key에 datetime stamp를 추가하자
		return hash('sha256', $data . $key);
	}
}



/**
 * @desc 		입력된 문자열의 캐릭터셋 utf-8 을 euc-kr 로 바꾸어 리턴한다.
 * @author		dylan
 * @created		2016.03.25
 * @param		string $data			캐릭터셋 변환할 문자열
 *				string $old_charset		변환할 캐릭터셋 
 *				string $new_charset		변환될 캐릭터겟
 * @return		string
 */
if ( ! function_exists('dv_conv') ) {
	function dv_iconv($data, $old_charset='utf-8', $new_charset='euc-kr') {
		
		$tmp = $data;
		if(!is_local()) {
			$tmp = iconv($old_charset, $new_charset, $data);
		}

		return $tmp;

	}
}



/**
 * @desc 		입력된 문자열의 캐릭터셋 euc-kr 을 utf-8 로 바꾸어 리턴한다.
 * @author		dylan
 * @created		2016.03.25
 * @param		string $data			캐릭터셋 변환할 문자열
 *				string $old_charset		변환할 캐릭터셋 
 *				string $new_charset		변환될 캐릭터겟
 * @return		string
 */
if ( ! function_exists('dv_oconv') ) {
	function dv_oconv($data, $old_charset='euc-kr', $new_charset='utf-8') {
		
		$tmp = $data;
		if(!is_local()) {
			$tmp = iconv($old_charset, $new_charset, $data);
		}

		return $tmp;

	}
}



/**
 * @desc 		현재일에서 년,월,일,시,분,초 중 한 요소를 리턴한다.
 * @author		dylan
 * @created		2016.04.23
 * @param		string $id	년월일시분초 중 반환할 요소 id
 * @return		string 		Y = yyyy, y = yy, M = mm, d = dd, h = hh, m = minute, s = second, null인 경우 yyyy-mm-dd hh:mm:ss 를 리턴
 */
if ( ! function_exists('get_in_date') ) {
	function get_in_date($id=NULL) {

		$date = get_date();
		if(is_null($id)) return $date;

		$a_tmp = explode(' ', $date);
		$ymd = explode('-', $a_tmp[0]);
		list($y,$M,$d) = $ymd;
		$y = strlen($y) == 1 ? '0'.$y : $y;
		$M = strlen($M) == 1 ? '0'.$M : $M;
		$d = strlen($d) == 1 ? '0'.$d : $d;
		
		$hms = explode(':', $a_tmp[1]);
		list($h,$m,$s) = $hms;
		$h = strlen($h) == 1 ? '0'.$h : $h;
		$m = strlen($m) == 1 ? '0'.$m : $m;
		$s = strlen($s) == 1 ? '0'.$s : $s;

		$rstRtn = '';
		switch ($id) {
			case 'y':  $rstRtn = substr($y, 2, 2); break;
			case 'M': $rstRtn = $M; break;
			case 'd': $rstRtn = $d; break;
			case 'h': $rstRtn = $h; break;
			case 'm': $rstRtn = $m; break;
			case 's': $rstRtn = $s; break;
			default:
				$rstRtn = $y; // case 'Y'
				break;
		}
		
		return $rstRtn;
	}
}



/**
 * @desc 		현재일을 리턴한다.
 * @author		dylan
 * @created		2016.04.23
 * @param		
 * @return		string 		yyyy-mm-dd hh:mm:ss
 */
if ( ! function_exists('get_date') ) {
	function get_date($fmt=NULL) {
		if( ! ini_get('date.timezone') ) {
			date_default_timezone_set('Asia/Seoul');
		}
		
		if($fmt) {
			return date($fmt);
		}
		else {
			return date("Y-m-d H:i:s");
		}
	}
}



/**
 * @desc 		숫자 0인 경우 공백 처리
 * @author		dylan
 * @created		2016.04.25
 * @param		
 * @return		string 		yyyy-mm-dd hh:mm:ss
 */
if ( ! function_exists('remove_zero') ) {
	function remove_zero($data) {
		$rst = $data == 0 ? '' : $data;
		return $rst;
	}
}



/**
 * @desc 		stdClass를 array로 변환한다.
 * @author		dylan
 * @created		2016.04.25
 * @param		
 * @return		array
 */
if ( ! function_exists('gen_stdobj_to_array') ) {
	function gen_stdobj_to_array($args) {
		$rstRtn = array();

		foreach ($args as $key => $item) {
			if(gettype($item) != 'object') {
				$rstRtn[$key] = $item;
			}
			else {
				gen_stdobj_to_array($item);
			}
		}

		return $rstRtn;
	}
}



/**
 * @desc 		입력받은 두 날짜간 개월수 차이를 리턴한다., eg) 2015-12-01, 2016-01-01 결과: 2
 * @author		dylan
 * @created		2016.05.07
 * @param		
 * @return		int
 */
if ( ! function_exists('get_interval_in_month') ) {
	function get_interval_in_month($from, $to) {
	    $month_in_year = 12;
	    $date_from = getdate(strtotime($from));
	    $date_to = getdate(strtotime($to));
	    return ($date_to['year'] - $date_from['year']) * $month_in_year -
	        ($month_in_year - $date_to['mon']) +
	        ($month_in_year - $date_from['mon']) + 1;
	}
}



/**
 * @desc 		입력받은 숫자가 1자리인 경우 왼쪽을 0으로 채워 2자리로 리턴
 * @author		dylan
 * @created		2016.05.08
 * @param		
 * @return		int
 */
if ( ! function_exists('gen_two_digit') ) {
	function gen_two_digit($data) {
		// return str_pad($data,2,'0',STR_PAD_LEFT);
		if(!$data || !is_numeric($data))
			return $data;
		else
			return sprintf("%02d", $data);
	}
}



/**
 * @desc 		입력받은 숫자를 한글로 리턴
 * @author		anonymous
 * @created		2016.05.10
 * @param		
 * @return		string
 */
if ( ! function_exists('numbersToCharacters') ) {

	function convert_num_to_kor($number)
	{
		$number_character_set = '일,이,삼,사,오,육,칠,팔,구';
		$number_character_pos = ',십,백,천,만,십,백,천,억,십,백,천,조,십,백,천,경';

		if( !is_numeric($number) ) return '';

		$ac = explode(',', $number_character_set);
		$ap = explode(',', $number_character_pos);

		$char = '';
		$cl = strlen($number);


		for($i = 0; $i < $cl; $i++)
		{
			$c = substr($number, $i,1);
			if(!is_numeric($c)) {
				$char = $c.$char;
				continue;
			}
			if($c<1 && ((($cl-$i-1)%4) || $is_skip)) continue;

			$is_skip = (($cl-$i-1)%4) ? false : true;
			$str_ac = $c-1 < 0 ? '' : $ac[$c-1];
			$char .= $str_ac.$ap[$cl-$i-1];
		}

		return $char;
	}
}



/**
 * @desc 		입력받은 데이터에서 숫자만 리턴
 * @author		anonymous
 * @created		2016.05.13
 * @param		
 * @return		mixed
 */
if ( ! function_exists('get_only_number') ) {

	function get_only_number($data) {

		return preg_replace('/[^0-9-]|(?<=.)-/', '', $data);

	}

}


/**
 * @desc 		입력받은 데이터에서 숫자만 리턴
 * @author		anonymous
 * @created		2016.05.13
 * @param		
 * @return		mixed
 */
if ( ! function_exists('convert_money') ) {

	function convert_money($data) {

		if(is_numeric($data)) {
			return $data;
		}
		else {
			if(is_null($data)) {
				return 0;
			}
			else {
				return get_only_number($data);
			}
		}
	}

}


/**
 * CI의 Force Download를 일부 수정한 버전. 
 * 수정내용 :
 * 1.ie11 체크가 안되는 문제 수정
 * 2.한글파일 다운로드시 깨지는 문제 수정
 *
 * Generates headers that force a download to happen
 *
 * @access	public
 * @param	string	filename
 * @param	mixed	the data to be downloaded
 * @return	void
 */
if ( ! function_exists('force_download_v2'))
{
	function force_download_v2($filename = '', $data = '')
	{
		if ($filename == '' OR $data == '')
		{
			return FALSE;
		}

		// Try to determine if the filename includes a file extension.
		// We need it in order to set the MIME type
		if (FALSE === strpos($filename, '.'))
		{
			return FALSE;
		}

		// Grab the file extension
		$x = explode('.', $filename);
		$extension = end($x);

		// Load the mime types
		if (defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/mimes.php'))
		{
			include(APPPATH.'config/'.ENVIRONMENT.'/mimes.php');
		}
		elseif (is_file(APPPATH.'config/mimes.php'))
		{
			include(APPPATH.'config/mimes.php');
		}

		// Set a default mime if we can't find it
		if ( ! isset($mimes[$extension]))
		{
			$mime = 'application/octet-stream';
		}
		else
		{
			$mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
		}

		header("Content-Type: text/html; charset=cp949");
		
		// Generate the server headers
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE 
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== FALSE
			|| strpos($_SERVER['HTTP_USER_AGENT'], 'Edge') !== FALSE)
		{
			$filename_ie = iconv("UTF-8", "EUC-KR", $filename);
			header('Content-Type: '.$mime);
			header('Content-Disposition: attachment; filename="'.$filename_ie.'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header("Content-Transfer-Encoding: binary");
			header('Pragma: public');
			header("Content-Length: ".strlen($data));
		}
		else
		{
			header('Content-Type: '.$mime);
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary");
			header('Expires: 0');
			header('Pragma: no-cache');
			header("Content-Length: ".strlen($data));
		}

		exit($data);
	}
}



/**
 * 공통 - dv_encrypt
 * 단방향 암호화 함수, decrypt는 제공하지 않는다.
 * param $data : 암호화할 데이터
 * param $key : 암호화할 추가  key 데이터
 */
if ( ! function_exists('dv_encrypt'))
{
	function dv_encrypt($data, $key) {
		return base64_encode(crypt(md5($data), md5($key)));
	}
}



/**
 * 공통 - goIndexPage
 * 권익센터 전용 함수+++++++++++++++++++++++++++++++++++++++++++++++
 * param $bad_access js alert 출력 여부
 */
if ( ! function_exists('goIndexPage'))
{
	function goIndexPage($bad_access) {
		$tmp = '';
		if($bad_access) {
			$tmp .= '<script>alert(\'잘못된 접근입니다.\')</script>';
		}
		$tmp .= '<script>location.href="'. DOC_ROOT .'";</script>';
		
		echo($tmp);
	}
}



/**
 * 추가 : dylan 2017.09.12<br>
 * 보안관련 함수들 추가
 * @author danvistory.com
 */
if (! function_exists('get_rand_str')) {
	
	/**
	 * CFSR, URL파라메터 변조 방지용 랜덤문자열 생성 함수
	 *
	 * @param $length int 생성할 문자열 개수, default 5
	 * @return string
	 */
	function get_rand_str($length=10) {
		$str = '';
		$base = str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
		$max = mb_strlen($base, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $base[rand(0, $max)];
		}
	
		return $str;
	}
}


if (! function_exists('get_token')) {
	
	/**
	 * 토큰 생성 함수<br>
	 * 토큰이 미생성 상태거나 세션이 종료된(또는 로그아웃, 미로그인) 경우 단순 토큰 생성해 리턴<br> 
	 * 로그인 한 상태인 경우 사용자id를 붙여서 리턴(xpath 방지처리)<br>
	 * 
	 * @param void
	 * @return boolean
	 */
	function get_token() {
		// 랜덤 문자열 생성
		$token = get_rand_str();
		
		$CI =& get_instance();
		
		// 로그인 한 경우, user_id 추가
		if( $CI->session->userdata(CFG_SESSION_ADMIN_ID) != '' ) {
			$token .= $CI->session->userdata(CFG_SESSION_ADMIN_ID);
		}
	
		return $token;
	}
}


if (! function_exists('is_bad_token')) {
	
	/**
	 * 토큰 체크 함수
	 * 
	 * 잘못된 토큰, 세션, 수신된 토큰과 세션 토큰이 다르면 TRUE 리턴, 정상이면 FALSE
	 * 
	 * @param array $form $_GET or $_POST 데이터
	 * @return boolean
	 */
	function is_bad_token(&$form) {
		$rst = TRUE;
		$CI =& get_instance();

		if( $CI->session->userdata(CFG_SECURE_FORM_TOKEN) == ''
				|| ! isset($form[CFG_SECURE_FORM_TOKEN]) 
				|| $form[CFG_SECURE_FORM_TOKEN] == ''
				|| $CI->session->userdata(CFG_SECURE_FORM_TOKEN) != $form[CFG_SECURE_FORM_TOKEN] ) {
					
			$rst = TRUE;
		}
		else {
			$rst = FALSE;
			// 세션 토큰 초기화 -> 로그인 성공시 초기화 하도록 변경 (admin::login 메서드 내로 이동시킴)
			//$CI->session->set_userdata(CFG_SECURE_FORM_TOKEN, "");
			// 폼 데이터에서 토큰 제거
			unset($form[CFG_SECURE_FORM_TOKEN]);
		}

		return $rst;
	}
}


if (! function_exists('throw_error_return')) {

	/**
	 * 오류 처리를 위해 아래 기능을 수행한다.<br>
	 * 1.로깅<br>
	 * 2.오류 내용 리턴
	 *
	 * @param array $args 로깅, 애러 메시지용 배열<br>
	 * string $args['level'] 로깅 레벨, CI 내장함수 log_message()로 보낼 레벨 - DEBUG, ERROR<br>
	 * string $args['log_msg'] 로깅 메시지<br>
	 * string $args['page_msg'] 커스텀 애러 페이지에 출력할 메시지
	 * @param boolean $is_log_form 폼 데이터(POST)를 로깅할지 여부, default false
	 * @return mixed
	 */
	function throw_error_return($args, $is_log_form=FALSE) {
		$form_data = "";
		if($is_log_form === TRUE) {
			$form_data = ", form : ". print_r($_POST, TRUE);
		}
		// logging
		log_message($args['level'], $args['log_msg'] .", URI : ". $_SERVER['REQUEST_URI'] . $form_data);
		// show error page
		return $args['page_msg'];
	}
}
/**
 * 추가 끝<br>
 * 보안관련 함수들 추가
 * @author danvistory.com
 */


/**
 * 공통 - dv_encrypt
 * 단방향 암호화 함수, decrypt는 제공하지 않는다.
 * param $data : 암호화할 데이터
 * param $key : 암호화할 추가  key 데이터
 */
if ( ! function_exists('dv_encrypt') ) {
	function dv_encrypt($data, $key) {
		return base64_encode(crypt(md5($data), md5($key)));
	}
}


/**
 * 비대칭 알고리듬인 RSA를 사용하여 문자열 암호화 <br>
 * 
 * PHP 5.2 이상, openssl 모듈 필요<br>
 * RSA 개인키, 공개키 조합을 생성<br>
 * 키 생성에는 시간이 많이 걸리므로, 한 번만 생성하여 저장해 두고 사용<br>
 * 단, 비밀번호는 개인키를 사용할 때마다 필요하다.<br>
 */
function rsa_generate_keys($password, $bits=2048, $digest_algorithm='sha256')
{
	if(empty($password)) {
		throw new ErrorException("Check your password, please..");
		return false;
	}
	
	$res = openssl_pkey_new(array(
			'digest_alg' => $digest_algorithm,
			'private_key_bits' => $bits,
			'private_key_type' => OPENSSL_KEYTYPE_RSA
	));

	if (false === openssl_pkey_export($res, $private_key, $password)) {
		throw new CryptoException('Could not export private key', OpenSslUtil::getErrors());
	}

	$public_key = openssl_pkey_get_details($res);
	$public_key = $public_key['key'];

	return array(
			'private_key' => $private_key,
			'public_key' => $public_key,
	);
}

/**
 * RSA 공개키를 사용한 문자열 암호화 함수<br>
 * 
 * 암호화할 때는 비밀번호가 필요하지 않다<br>
 * 오류가 발생할 경우 false를 반환한다.<br>
 */
function rsa_encrypt($plaintext, $public_key)
{
	// 용량 절감과 보안 향상을 위해 평문을 압축한다.
	$plaintext = gzcompress($plaintext);

	// 공개키를 사용하여 암호화한다.
	$pubkey_decoded = @openssl_pkey_get_public($public_key);
	if ($pubkey_decoded === false) return false;

	$ciphertext = false;
	$status = @openssl_public_encrypt($plaintext, $ciphertext, $pubkey_decoded);
	if (!$status || $ciphertext === false) return false;

	// 암호문을 base64로 인코딩하여 반환한다.
	return base64_encode($ciphertext);
}

/**
 * RSA 개인키를 사용하여 암호화된 문자열 복호화 함수 <br>
 * 
 * 복호화할 때는 비밀번호가 필요하다.<br>
 * 오류가 발생할 경우 false를 반환한다.
 */
function rsa_decrypt($ciphertext, $private_key, $password)
{
	// 암호문을 base64로 디코딩한다.
	$ciphertext = @base64_decode($ciphertext, true);
	if ($ciphertext === false) return false;

	// 개인키를 사용하여 복호화한다.
	$privkey_decoded = @openssl_pkey_get_private($private_key, $password);
	if ($privkey_decoded === false) return false;

	$plaintext = false;
	$status = @openssl_private_decrypt($ciphertext, $plaintext, $privkey_decoded);
	@openssl_pkey_free($privkey_decoded);
	if (!$status || $plaintext === false) return false;

	// 압축을 해제하여 평문을 얻는다.
	$plaintext = @gzuncompress($plaintext);
	if ($plaintext === false) return false;

	// 이상이 없는 경우 평문을 반환한다.
	return $plaintext;
}

?>