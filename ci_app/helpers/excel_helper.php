<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * 아래 유형의 문자열에 통화형으로 변환하여 리턴
 * 
 * eg) 3234(1321) -> 3,234(1,321) <br>
 * 3234(100%) -> 3,234(100%) <br>
 * 3234(1321)(100%) -> 3,234(1,321)(100%)<br>
 * 
 * 엑셀에서만 사용
 * 
 * @param string $data
 * @param string $half_up_digit 반올림할 자릿수, 기본 1
 * @return string
 */
if ( ! function_exists('mixedNumberToCurrency') ) {
	function mixedNumberToCurrency($data, $half_up_digit=1) {
		// 첫번째 문자가 숫자가 아니면 바로 리턴
		if( ! preg_match('/^[0-9]/', $data)) {
			return $data;
		}
		$data = str_replace(',', '', str_replace(' ', '', $data));
		$rstRtn = '';
		$lastChar = '';
		if (strrpos($data, ')') !== FALSE) { // 마지막 괄호가 있으면 리턴전에 붙여준다.
			$lastChar = ')';
		}
		$arr = preg_split('/[()]/', $data);
		if(count($arr) == 1 && ! is_numeric($data)) {// 숫자아닌 단순 문자 또는 숫자를 포함한 문자인 경우, 리턴
			return $data;
		}
		$bk_cnt = substr_count($data, '(');
		$arr_bk = array('');
		for ($i = 1; $i <= $bk_cnt; $i ++) {
			$arr_bk[] = '(';
		}
		$loop = 0;
		$exist_bk = 0;
		foreach ($arr as $idx => $val) {
			if ($val != '') {
				if ($exist_bk == 1) { // 직전에 괄호를 붙였으면 뒤에 닫는다.
					$rstRtn .= ')';
				}
				$exist_bk = 0;
				if ($rstRtn != '') {
					$rstRtn .= $arr_bk[$loop];
					$exist_bk = 1;
				}
				if (strpos($val, '.') !== FALSE) {
					$rstRtn .= number_format((double) $val, $half_up_digit);
					if (strpos($val, '%') !== FALSE) {
						$rstRtn .= '%';
					}
				}
				else if (strpos($val, '%') === FALSE) {
					$rstRtn .= number_format((double) $val);
				}
				else if (strpos($val, '%') !== FALSE) {
					$rstRtn .= number_format((double) $val) . '%';
				}
				$loop ++;
			}
		}
		return $rstRtn . $lastChar;
	}
}



/**
 * 가로,세로 숫자값에 해당하는 문자열 셀좌표
 *
 * eg) addressbyColRow(2,10) -> return B10
 *
 * @param int $col
 * @param int $row
 */
if ( ! function_exists('addressbyColRow') ) {
	function addressbyColRow($col, $row) {
		return getColFromNumber($col).$row;
	}
}

/**
 * 입력 숫자에 해당하는 문자 리턴(엑셀 가로 문자)
 *
 * eg) addressbyColRow(2) -> return B
 *
 * @param int $col
 * @param int $row
 */
if ( ! function_exists('getColFromNumber') ) {
	function getColFromNumber($num) {
		$numeric = ($num - 1) % 26;
		$letter = chr(65 + $numeric);
		$num2 = intval(($num - 1) / 26);
		if ($num2 > 0) {
			return getColFromNumber($num2) . $letter;
		} else {
			return $letter;
		}
	}
}


/**
 * 입력된 문자열이 한글인지 체크한다. <br>
 *
 * @param string $ch
 * @return boolean
 */
if ( ! function_exists('is_hangul') ) {
	function is_hangul($str) {
		$rst = '';
		$len = mb_strlen($str, 'utf-8');
		if($len <= 0) return false;
		for($i=0;$i<$len;$i++) {
			$rst .= (int)is_hangul_char(mb_substr($str, $i, 1, 'utf-8'));
		}
		return strpos($rst, '1') !== FALSE;
	}
}
	
/**
 * 입력된 문자가 한글인지 체크한다. <br>
 * (주)입력된 문자가 문자열인 경우 앞1자리만 체크한다.
 * 
 * @param char $ch
 * @return boolean
 */
if ( ! function_exists('is_hangul_char') ) {
	function is_hangul_char($ch) {
		$c = utf8_ord($ch);
		if( 0x1100<=$c && $c<=0x11FF ) return true;
		if( 0x3130<=$c && $c<=0x318F ) return true;
		if( 0xAC00<=$c && $c<=0xD7A3 ) return true;
		return false;
	}
}

if ( ! function_exists('utf8_ord') ) {
	function utf8_ord($ch) {
		$len = strlen($ch);
		if($len <= 0) return false;
		$h = ord($ch{0}); // 1개만 체크
		if ($h <= 0x7F) return $h;
		if ($h < 0xC2) return false;
		if ($h<=0xDF && $len>1) return ($h & 0x1F) << 6 | (ord($ch{1}) & 0x3F);
		if ($h<=0xEF && $len>2) return ($h & 0x0F) << 12 | (ord($ch{1}) & 0x3F) << 6 | (ord($ch{2}) & 0x3F);
		if ($h<=0xF4 && $len>3) return ($h & 0x0F) << 18 | (ord($ch{1}) & 0x3F) << 12 | (ord($ch{2}) & 0x3F) << 6 | (ord($ch{3}) & 0x3F);
		return false;
	}
}