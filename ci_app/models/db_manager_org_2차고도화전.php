<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/******************************************************************************************************
 * DB 관리 Class
 * DB_manager
 * 작성자 : delee
 *
 *
 *****************************************************************************************************/
class Db_manager extends CI_Model {
	var $offset = 0;
	var $limit = CFG_MAIN_PAGINATION_ITEM_PER_PAGE;
	
	var $tbl_oper = 'operator';
	var $tbl_sub_code = 'sub_code';
	var $tbl_master_code = 'master_code';
	var $tbl_auth_grp = 'oper_auth_grp';
	var $tbl_counsel = 'counsel';
	var $tbl_counsel_sub = 'counsel_sub';
	var $tbl_brd_faq = 'brd_faq';
	var $tbl_brd_oper = 'brd_operation';

	// 권리구제지원
	var $tbl_lawhelp = 'lawhelp';
	var $tbl_lawhelp_invtgt_date = 'lawhelp_invtgt_date';
	var $tbl_lawhelp_kind_sub_cd = 'lawhelp_kind_sub_cd';
	
	
	//=================================================================================================================
	// construct
	//=================================================================================================================
	function __construct() {
		parent::__construct();
		
		// session
		// $this->load->library('session');

    }
	
	
	
	
	
	
	//=================================================================================================================
	// Admin
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_login
	//-----------------------------------------------------------------------------------------------------------------
	public function get_login($args) {
		$rstRtn['rst'] = '';
		$rstRtn['msg'] = '';
		$rstRtn['oper_id'] = '';
		$rstRtn['oper_name'] = '';
		$rstRtn['oper_auth_grp_id'] = '';
		$rstRtn['oper_auth_codes'] = '';


		$fields ='OP.oper_kind, OP.oper_kind_sub, OP.oper_id, OP.oper_auth_grp_id, OP.oper_name, OP.use_yn, OP.s_code as asso_cd'
			.', GP.oper_auth_codes, S.code_name as asso_nm';
		$rs = $this->db->query('SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_oper .' OP '
			.'INNER JOIN '. $this->tbl_auth_grp .' GP ON OP.oper_auth_grp_id=GP.oper_auth_grp_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON OP.s_code=S.s_code '
			.'WHERE OP.oper_id = "'. $args['oper_id'] .'" AND OP.oper_pwd="'. dv_hash($args['oper_pwd'], CFG_ENCRYPT_KEY) .'"');
		$rst = $rs->result();

		if(count($rst) >= 1) {
			if($rst[0]->use_yn == 0) {
				$rstRtn['msg'] = 'err_login_02';
			}
			else {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = '';
				$rstRtn['oper_id'] = $rst[0]->oper_id;
				$rstRtn['oper_name'] = $rst[0]->oper_name;
				$rstRtn['oper_kind'] = $rst[0]->oper_kind;
				$rstRtn['oper_kind_sub'] = $rst[0]->oper_kind_sub;
				$rstRtn['oper_auth_grp_id'] = base64_encode($rst[0]->oper_auth_grp_id);
				$rstRtn[CFG_SESSION_ADMIN_AUTH_ASSO_CD] = $rst[0]->asso_cd;
				$rstRtn[CFG_SESSION_ADMIN_AUTH_ASSO_NM] = $rst[0]->asso_nm;
				
				// 권한코드 첫번째 코드
				$first_enc_code = '';

				// 암호화 하기 전 구분자로 나눈뒤 각 코드를 개별암호화하여 구분자로 붙여 보낸다.
				$codes = $rst[0]->oper_auth_codes;
				if($codes) {
					$tmp = explode(CFG_AUTH_CODE_DELIMITER, $codes);
					$tmp2 = '';
					foreach($tmp as $data) {
						if($tmp2 != '') {
							$tmp2 .= CFG_AUTH_CODE_DELIMITER;
						}
						$tmp2 .= base64_encode($data);
						//
						// 로그인 후 첫번째 로딩 페이지 용 권한코드(=서브메뉴 코드)
						if($first_enc_code == '') {
							$first_enc_code = base64_encode($data);
						}

						// 통계>상담사례 보기 권한 세팅
						$auth_csl_view = 0;
						if($data == CFG_EXCEPTION_CODE_COUNSEL_STATISTIC_VIEW_AUTH) {
							$auth_csl_view = 1;
						}
						$this->session->set_userdata(CFG_SESSION_ADMIN_AUTH_CSL_VIEW, $auth_csl_view);
					}
					$enc_codes = $tmp2;
				}
				else {
					$enc_codes = '';
				}
				$rstRtn['oper_auth_codes'] = $enc_codes;
				$rstRtn[CFG_SESSION_ADMIN_AUTH_FIRST_CD] = $first_enc_code;
			}
		}
		else {
			$rstRtn['rst'] = 'fail';
			$rstRtn['msg'] = 'err_login_01';
		}
		
		return $rstRtn;
	}

	
	
	//#################################################################################################################
	// Statistic
	//#################################################################################################################
	
	//=================================================================================================================
	// Statistic
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_sttc_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_sttc_list($args) {
		$where = '1=1 ';

		//상담자 검색을 위한 조인문
		$oper_join = ' INNER JOIN operator O ON O.oper_id=C.oper_id ';
		// csl_date
		$where1 = '';
		if(isset($args['csl_date']) && $args['csl_date'] != '') {
			$where1 .= 'AND '. $args['csl_date'] .' ';
		}
		// asso_code 소속
		$where2 = '';
		if(isset($args['asso_code'][0]) && $args['asso_code'][0] != '') {
			$where2 = 'AND (C.asso_code in (' .$args['asso_code'][0]. '))';
		}

		// s_code 상담방법
		$where3 = '';
		if(isset($args['s_code'][0]) && $args['s_code'][0] != '') {
			$where3 = 'AND (C.s_code in (' .$args['s_code'][0]. '))';
		}

		// 상담자
		/*$where4 = '';
		if(isset($args['oper_id'][0]) && $args['oper_id'][0] != '') {
			$where4 = 'AND (O.oper_id in (' .$args['oper_id'][0]. '))';
		}*/

		// csl_proc_rst 처리결과
		$where5 = '';
		if(isset($args['csl_proc_rst'][0]) && $args['csl_proc_rst'][0] != '') {
			$where5 = 'AND (C.csl_proc_rst in (' .$args['csl_proc_rst'][0]. '))';
		}

		// master 관리자 여부
		$is_master = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);
		// $is_master = $_SESSION[CFG_SESSION_ADMIN_AUTH_IS_MASTER];

		// 각 쿼리별 합계 - 고용형태,상담유형 인 별도 코드 사용
		$total_counting = 'count(C.seq)';

		// 각 쿼리별 합계 쿼리
		$query_tot = '';


		//---------------------------------------------------------
		// # 구분 1 : 상담사례
		//---------------------------------------------------------
		if($args['kind'] == 'down01') {
			
			// 고용형태 코드 - 중복체크 가능하게 요청 뒤 원복 요청으로 주석처리
			// $code['emp_kind'] = self::get_sub_code_by_m_code_v2(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
			// $emp_kind_code = '"'. str_replace(',', '","', $code['emp_kind'][0]->s_code) .'"';

			// 상담유형 코드
			$code['csl_kind'] = self::get_sub_code_by_m_code_v2(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);
			$csl_kind_code = '"'. str_replace(',', '","', $code['csl_kind'][0]->s_code) .'"';
			
			$where .= $where1 . $where2 . $where3 . $where5;
			//
			$select = 'SELECT ';
			// 통계-상담사례 엑셀 다운로드시  seq을 사용하기 위해 주석처리
			// if($args['exclude_seq'] != 1) { 
				$select .= 'C.seq, ';
			// }
			$select .= 'C.csl_date, S14.code_name as asso_name, O.oper_name, C.csl_name, '
				.'S4.dsp_order as csl_type, C.s_code_etc, S12.dsp_order as ages, S1.dsp_order as gender, '
				.'C.ages_etc,C.emp_cnt_etc,' // 디버깅, 2019.07.11
				.'S2.dsp_order as live_addr, C.live_addr_etc, S3.dsp_order as work_kind, C.work_kind_etc, S5.dsp_order as comp_kind, C.comp_kind_etc, '
				.'S6.dsp_order as comp_addr, C.comp_addr_etc, S8.dsp_order as emp_kind, C.emp_kind_etc, '
				// ,'S13.dsp_order as emp_use_kind, '// 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				.'S7.dsp_order as emp_cnt, S10.dsp_order as emp_paper_yn, '
				.'S9.dsp_order as emp_insured_yn, C.ave_pay_month, C.work_time_week, '
				.'C.asso_code, '; // 상담사례 목록에서 상담내용보기 버튼 권한 체크용
			// excel 다운로드 용 컬럼 추가
			if($args['exclude_seq'] == 1) {
				$select .= 'SUBSTRING_INDEX(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)), ",", 1) as csl_kind_1, '
					.'CASE(LENGTH(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order))) - LENGTH(REPLACE(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)), ",", "")) ) '
					.'WHEN 0 THEN "" '
					.'WHEN 1 THEN SUBSTRING_INDEX(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)), ",", -1) '
					.'ELSE SUBSTRING_INDEX(SUBSTRING_INDEX(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)), ",", -2), ",", 1) '
					.'END as csl_kind_2, '
					.'CASE( LENGTH(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order))) - LENGTH(REPLACE(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)), ",", "")) ) '
					.'WHEN 2 THEN SUBSTRING_INDEX(CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)), ",", -1) '
					.'ELSE "" '
					.'END as csl_kind_3, '
					.'S15.dsp_order as csl_proc_rst, ';
				$select .= '(SELECT GROUP_CONCAT(K2.code_name) FROM '. $this->tbl_counsel_sub .' K1 INNER JOIN '. $this->tbl_sub_code .' K2 ON K1.s_code=K2.s_code WHERE K1.csl_seq=C.seq AND K2.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'") as csl_keywords, ';
					
				// 상담사례 상담보기 권한 여부
				// master OR 타 기관 상담이라도 상담보기 권한이 있으면 상담내용,결과를 노출한다.
				$auth_csl_view = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_CSL_VIEW);
				// $auth_csl_view = $_SESSION[CFG_SESSION_ADMIN_AUTH_CSL_VIEW];
				if($is_master == 1 || $auth_csl_view == 1) {
					$select .= 'C.csl_content, C.csl_reply ';
				}
				else {
					$select .= '(SELECT EC.csl_content FROM '. $this->tbl_counsel .' EC WHERE C.seq=EC.seq AND C.asso_code="'. $args['oper_asso_cd'] .'") as csl_content, ';
					$select .= '(SELECT EC.csl_reply FROM '. $this->tbl_counsel .' EC WHERE C.seq=EC.seq AND C.asso_code="'. $args['oper_asso_cd'] .'") as csl_reply ';
				}
			}
			else {
				$select .= 'CONCAT(GROUP_CONCAT(DISTINCT S11.dsp_order ORDER BY S11.dsp_order)) as csl_kind, '
					.'S15.dsp_order as csl_proc_rst ';
			}

			$q = 'FROM '. $this->tbl_counsel .' C '
				.'INNER JOIN operator O ON O.oper_id=C.oper_id '
				.'LEFT JOIN sub_code S1 ON S1.s_code=C.gender '
				.'LEFT JOIN sub_code S12 ON S12.s_code=C.ages '
				.'LEFT JOIN sub_code S2 ON S2.s_code=C.live_addr '
				.'LEFT JOIN sub_code S3 ON S3.s_code=C.work_kind '
				.'LEFT JOIN sub_code S4 ON S4.s_code=C.s_code '
				.'LEFT JOIN sub_code S5 ON S5.s_code=C.comp_kind '
				.'LEFT JOIN sub_code S6 ON S6.s_code=C.comp_addr '
				.'LEFT JOIN sub_code S7 ON S7.s_code=C.emp_cnt '
				.'LEFT JOIN sub_code S8 ON S8.s_code=C.emp_kind ' // 단일항목 체크
				// .'INNER JOIN counsel_sub CS ON C.seq = CS.csl_seq AND CS.s_code IN ('. $emp_kind_code .') '
				// .'INNER JOIN sub_code S8 ON CS.s_code = S8.s_code '
				.'LEFT JOIN sub_code S9 ON C.emp_insured_yn=S9.s_code '
				.'LEFT JOIN sub_code S10 ON C.emp_paper_yn=S10.s_code '
				// .'LEFT JOIN sub_code S13 ON S13.s_code = C.emp_use_kind ' // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				.'LEFT JOIN sub_code S14 ON S14.s_code = C.asso_code '
				.'LEFT JOIN sub_code S15 ON S15.s_code = C.csl_proc_rst '
				.'INNER JOIN counsel_sub CS2 ON C.seq = CS2.csl_seq AND CS2.s_code IN ('. $csl_kind_code .') '
				.'INNER JOIN sub_code S11 ON CS2.s_code = S11.s_code '
				.'WHERE '. $where .' '
				.'GROUP BY ';
			// 수정: 조회, 엑셀다운 쿼리를 동일하지 않아 다른 결과가 나오는 문제 수정 20151228 dylan
			// if($args['exclude_seq'] != 1) { 
				$q .= 'C.seq, ';
			// }
			$q .= 'C.csl_date, C.asso_code, O.oper_name, C.csl_name, '
				.'S1.dsp_order, S12.dsp_order, S2.dsp_order, S3.dsp_order, '
				.'S4.dsp_order, S5.dsp_order, S6.dsp_order, S7.dsp_order, S8.dsp_order, '
				.'S9.dsp_order , S10.dsp_order'
				// .', S13.dsp_order' // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				.', C.ave_pay_month, C.work_time_week '
				.'ORDER BY C.csl_date DESC, C.seq DESC, C.asso_code ASC';

			$query = $select . $q;
			// echof($query);
			// # 쿼리 실행
			if(!empty($query)) {
				$rst = $this->db->query($query);
				$rstRtn['data'] = $rst->result();
			}

			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data']);

			// $rstRtn['query'] = $query .'# '. $query_tot;
		}
		//---------------------------------------------------------
		// 구분 2 : 상담방법 : S002
		//---------------------------------------------------------
		else if($args['kind'] == 'down02') {

			// 상담방법 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); 
			
			// 상담일자 외 - 소속/성별/직종/업종/거주지+회사소재지/고용형태/근로자수/근로계약서/4대보험/연령대
			if($args['csl_code'] != 'csl_date') {
				// where - 상담일, 상담자 기본 검색대상
				$where .= $where1 . $where2 . $where3 . $where5;

				$datas = self::get_sub_code_by_m_code($args['csl_code']);
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP){ //소재지의 코드가 거주지 코드와 같기 때문..
					$datas = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				}
				$q = '';

				foreach($datas as $data) {
					$q3 = '';
					$code = $data->s_code;
					
					//
					$target_field = '';

					// 검색 대상 컬럼명
					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION) {// 소속
						$target_field = 'C.asso_code';
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) {// 성별
						$target_field = 'C.gender'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
						$target_field = 'C.work_kind'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
						$target_field = 'C.comp_kind'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS) { // 거주지/회사소재지
						$target_field = 'C.live_addr'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP) { // 거주지/회사소재지
						$target_field = 'C.comp_addr'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND) {// 고용형태
						$target_field = 'C.emp_kind'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) { // 근로자수
						$target_field = 'C.emp_cnt'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN) { // 근로계약서
						$target_field = 'C.emp_paper_yn'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN) { // 4대보험
						$target_field = 'C.emp_insured_yn'; 
					}
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) { // 연령대
						$target_field = 'C.ages'; 
					}
					// else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND) { // 사용주체
					// 	$target_field = 'C.emp_use_kind'; 
					// } // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
					else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST) { // 처리결과
						$target_field = 'C.csl_proc_rst'; 
					}

					// 쿼리 조합
					$target_field .= '="'. $code .'"'; 

					if($target_field != '') {
						$target_field = ' AND '. $target_field;
					}

					if($q != '') {
						$q .= ' UNION ';
					}
					
					$q1 = 'SELECT DD.code_name as subject,';
					$q2 = 'FROM ('
						.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
					
					$q3_cds = '';
					$q3_index = 1;
					$q4_t = '';

					// 상담방법 code
					foreach($datas2 as $data2) {
						$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
						$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .' WHERE '. $where .' AND C.s_code = "'. $data2->s_code .'" '. $target_field .' ) A'. $q3_index .', ';
						if($q3_cds != '') $q3_cds .= ',';
						$q3_cds .= $data2->s_code;
						// 구분별 총계 쿼리
						if($q4_t != '') $q4_t .= ',';
						$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .' WHERE '. $where .' AND C.s_code = "'. $data2->s_code .'") T'. $q3_index;
						//
						$q3_index++;
					}
					
					$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
					$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
					$q .= '(SELECT '. $total_counting .' as ck'. $q3_index .' FROM counsel C '. $oper_join .' WHERE '. $where .' AND C.s_code IN ('. $q3_cds .') '. $target_field .' ) T ) ';
					$q4_t .= ', (SELECT count(C.seq) FROM counsel C '. $oper_join .' WHERE '. $where .' AND C.s_code IN ('. $q3_cds .')) T'. $q3_index;
				}
			}
			// 상담일자
			else {

				// 검색일자 - 전체인 경우 빈값이다.
				if(isset($args['csl_date']) && $args['csl_date'] != '') {
					$sch_date = $args['csl_date'];
				}
				else {
					$sch_date = 'C.csl_date BETWEEN "1900-01-01 00:00:00" AND "'. get_date() .'" ';
				}
				//
				$q = 'SELECT DISTINCT C.csl_date FROM counsel C '. $oper_join .' WHERE '. $sch_date .' ORDER BY C.csl_date DESC';
				$rstTmp = $this->db->query($q);
				$datas = $rstTmp->result();
				$q = '';
				// for total
				$dates = '';

				foreach($datas as $data) {
					$q3 = '';
					$date = $data->csl_date;
					if($q != '') {
						$q .= ' UNION ALL ';
					}
					// 상담방법 별 일자 총계 쿼리의 일자 조합
					if(!empty($dates)) $dates .= ',';
					$dates .= $date;

					$where_t = $where . ' AND C.csl_date = "'. $date .'" '. $where2 . $where3 . $where5;
					
					$q1 = 'SELECT "'. $date .'" as subject,';
					$q2 = 'FROM (';
					
					$q3_cds = '';
					$q3_index = 1;
					$q4_t = '';
					foreach($datas2 as $data2) {
						$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
						$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .' WHERE '. $where_t .' AND C.s_code = "'. $data2->s_code .'" ) A'. $q3_index .', ';
						if($q3_cds != '') $q3_cds .= ',';
						$q3_cds .= $data2->s_code;
						// 구분별 총계 쿼리
						if($q4_t != '') $q4_t .= ',';
						$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .' WHERE '. $where . $where2. $where3. $where5 .' AND C.csl_date IN ("'. str_replace(',', '","', $dates) .'") AND C.s_code = "'. $data2->s_code .'") T'. $q3_index;
						//
						$q3_index++;
					}
					
					$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
					$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
					$q .= '(SELECT '. $total_counting .' as ck'. $q3_index .' FROM counsel C '. $oper_join .' WHERE '. $where_t .' AND C.s_code IN ('. $q3_cds .') ) T ) ';
					// 상담방법 모든 종류의 전체일자에 대한 총계
					$q4_t .= ', (SELECT count(C.seq) FROM counsel C '. $oper_join .' WHERE '. $where . $where2. $where3. $where5 .' AND C.csl_date IN ("'. str_replace(',', '","', $dates) .'") AND C.s_code IN ('. $q3_cds .')) T'. $q3_index;
				}
			}

			// 상담일자인 경우, 데이터가 없으면 $q가 공백이라 오류가 발생하는 문제 처리
			if(empty($q)) {
				$query = 'SELECT seq FROM counsel WHERE 1=0';
				$query_tot = $query;
			}
			else {
				$query = $q;
				$query_tot = 'SELECT '. $q4_t;
			}
		// echof($query);
		}
		//---------------------------------------------------------
		// 구분 3 : 상담유형 : S020
		//---------------------------------------------------------
		else if($args['kind'] == 'down03') {
			
			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3. $where5;

			$datas = self::get_sub_code_by_m_code($args['csl_code']);
			$q = '';
			// for total
			$codes = '';

			// 상담유형
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND); 
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 초기화
				$target_field = '';
				$target_field_qry = '';
				$target_field_tot = '';

				// 검색 대상 컬럼명
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) { // 근로자수
					$target_field = 'emp_cnt'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
					$target_field = 'work_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
					$target_field = 'comp_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND) {// 고용형태
					$target_field = 'emp_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY) { // 상담방법
					$target_field = 's_code'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
					$target_field = 'gender'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) { // 연령대
					$target_field = 'ages'; 
				}
				// else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND) { // 사용주체
				// 	$target_field = 'emp_use_kind'; 
				// }

				// 쿼리 조합
				if($target_field != '') {
					$target_field_qry = ' AND C.'. $target_field .'="'. $code .'"';
				}

				if($q != '') {
					$q .= ' UNION ';
				}

				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				$q3_cds = '';
				$q3_index = 1;

				// 구분 데이터 - 상담유형
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .' INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq AND CS.s_code="'. $data2->s_code .'" WHERE '. $where .' '. $target_field_qry .' ) A'. $q3_index .', ';
					if($q3_cds != '') $q3_cds .= ',';
					$q3_cds .= $data2->s_code;
					$q3_index++;
				}
				
				$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
				$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
				$q .= '(SELECT CONCAT(count(C.seq), "(", count(DISTINCT C.seq), ")") as ck'. $q3_index .' FROM counsel C '. $oper_join .' INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq AND CS.s_code IN ('. $q3_cds .') WHERE '. $where .' '. $target_field_qry .' ) T ) ';
			}
			
			// 상담유형별 총계 쿼리 생성
			$q3_index = 1;
			$q4_t = '';
			foreach($datas2 as $data2) {
				if($q4_t != '') $q4_t .= ', ';
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .' INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq AND CS.s_code="'. $data2->s_code .'" WHERE '. $where .' AND C.'. $target_field .' IN ("'. str_replace(',', '","', $codes) .'") ) T'. $q3_index++;
			}
			$q4_t .= ', (SELECT CONCAT(count(C.seq), "(", count(DISTINCT C.seq), ")") FROM counsel C '. $oper_join .' INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq AND CS.s_code IN ('. $q3_cds .') WHERE '. $where .' AND C.'. $target_field .' IN ("'. str_replace(',', '","', $codes) .'") ) TT';
			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;

		}
		//---------------------------------------------------------
		// 구분 4 : 거주지 : S025
		//---------------------------------------------------------
		// 구분 5 : 회사소재지 : S025 - 회사는 거주지와 같은 s_code 사용한다.
		//---------------------------------------------------------
		else if($args['kind'] == 'down04' || $args['kind'] == 'down05') {
			
			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3 . $where5;

			$datas = self::get_sub_code_by_m_code($args['csl_code']);
			$q = '';
			// for total
			$codes = ''; 

			// $args['kind'] == 'down05'
			$addr_field = 'C.live_addr';
			if($args['kind'] == 'down05') {
				$addr_field = 'C.comp_addr';
			}

			// 거주지/회사소재지 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 초기화
				$inner_join_sub = '';
				$inner_join_sub_tot = '';
				$target_field = '';
				$target_field_qry = '';
				$target_field_tot_qry = '';

				// 검색 대상 컬럼명
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
					$inner_join_sub = 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq AND CS2.s_code="'. $code .'" ';
					$inner_join_sub_tot = 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq AND CS2.s_code IN (?) ';
					$total_counting = 'CONCAT(count(C.seq), "(", count(DISTINCT C.seq), ")")';
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND) {// 고용형태
					$target_field = 'emp_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
					$target_field = 'work_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
					$target_field = 'comp_kind'; 
				}
				// 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				// else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND) { // 사용주체
				// 	$target_field = 'emp_use_kind'; 
				// }

				// 쿼리 조합 - 상담유형 외 나머지
				if($args['csl_code'] != CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) {
					$target_field_qry = ' AND C.'. $target_field .'="'. $code .'"'; 
					$target_field_tot_qry = ' AND C.'. $target_field .' IN (?) ';
				}

				if($q != '') {
					$q .= ' UNION ';
				}
				
				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				
				$q3_cds = '';
				$q3_index = 1;

				// 거주지/회사소재지 code
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join . $inner_join_sub .' WHERE '. $where . ' AND '. $addr_field .' = "'. $data2->s_code .'" '. $target_field_qry .' ) A'. $q3_index .', ';
					if($q3_cds != '') $q3_cds .= ',';
					$q3_cds .= $data2->s_code;
					$q3_index++;
				}
				
				$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
				$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
				// 상담유형별 합계(실데이터)는 중복제거가 되지 않는다. 왜냐하면 counsel_sub 테이블에 존재하여 join하기 때문. 상담유형 tab(down03)은 counsel_sub 테이블과 join을 똑같이 하지만 counsel 데이터를 기준으로 중복제거하기 때문에 제거 되는 것임 
				// $q .= '(SELECT '. $total_counting .' as ck'. $q3_index .' FROM counsel C '. $inner_join_sub .' WHERE '. $where .' AND '. $addr_field .' IN ('. $q3_cds .') '. $target_field_qry .' ) T ) ';
				$q .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join . $inner_join_sub .' WHERE '. $where .' AND '. $addr_field .' IN ('. $q3_cds .') '. $target_field_qry .' ) T ) ';
			}
			
			// 거주지/회사소재지 별 총계 쿼리 생성
			$q3_index = 1;
			$q4_t = '';
			$codes = '"'. str_replace(',', '","', $codes) .'"';
			foreach($datas2 as $data2) {
				if($q4_t != '') $q4_t .= ', ';
				if(!empty($inner_join_sub_tot)) {
					$inner_join_sub_tot = str_replace('?', $codes, $inner_join_sub_tot);
				}
				if(!empty($target_field_tot_qry)) {
					$target_field_tot_qry = str_replace('?', $codes, $target_field_tot_qry);
				}
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join . $inner_join_sub_tot .' WHERE '. $where .' AND '. $addr_field .' = "'. $data2->s_code .'" '. $target_field_tot_qry .' ) T'. $q3_index++;
			}
			$q4_t .= ', (SELECT '. $total_counting .' FROM counsel C '. $oper_join . $inner_join_sub_tot .' WHERE '. $where . $target_field_tot_qry .' AND '. $addr_field . ' IN ('. $q3_cds .') ) TT';
			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;
		}
		//---------------------------------------------------------
		// 구분 6 : 고용형태 - S016
		//---------------------------------------------------------
		else if($args['kind'] == 'down06') {

			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3 . $where5;

			$datas = self::get_sub_code_by_m_code($args['csl_code']);
			$q = '';
			// for total
			$codes = '';

			// 고용형태 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 초기화
				$target_field = '';
				
				// 검색 대상 컬럼명
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) {// 연령대
					$target_field = 'ages'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) {// 근로자수
					$target_field = 'emp_cnt'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
					$target_field = 'gender'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
					$target_field = 'work_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
					$target_field = 'comp_kind'; 
				}
				

				// 쿼리 조합
				$target_field_qry = 'AND C.'. $target_field .'="'. $code .'"'; 

				if($q != '') {
					$q .= ' UNION ';
				}
				
				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				
				$q3_cds = '';
				$q3_index = 1;
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_kind = "'. $data2->s_code .'" '. $target_field_qry .' ) A'. $q3_index .', ';
					if($q3_cds != '') $q3_cds .= ',';
					$q3_cds .= $data2->s_code;
					$q3_index++;
				}
				
				$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
				$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
				$q .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_kind IN ('. $q3_cds .') '. $target_field_qry .' ) T ) ';
			}
			
			// 고용형태 별 총계 쿼리 생성
			$q3_index = 1;
			$q4_t = '';
			$codes = '"'. str_replace(',', '","', $codes) .'"';
			foreach($datas2 as $data2) {
				if($q4_t != '') $q4_t .= ', ';
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_kind = "'. $data2->s_code .'" AND C.'. $target_field .' IN ('. $codes .') ) T'. $q3_index++;
			}
			$q4_t .= ', (SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_kind IN ('. $q3_cds .') AND C.'. $target_field .' IN ('. $codes .') ) TT';
			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;
		}
		//---------------------------------------------------------
		// 구분 7 : 업종 - S015
		//---------------------------------------------------------
		else if($args['kind'] == 'down07') {

			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3 . $where5;

			$datas = self::get_sub_code_by_m_code($args['csl_code']);
			$q = '';
			// for total
			$codes = '';

			// 업종 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 검색 대상 컬럼명
				$target_field = 'ages'; // 연령대
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
					$target_field = 'gender'; 
				}

				// 쿼리 조합
				$target_field_qry = 'C.'. $target_field .'="'. $code .'"';

				if($q != '') {
					$q .= ' UNION ';
				}
				
				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				
				$q3_cds = '';
				$q3_index = 1;
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.comp_kind = "'. $data2->s_code .'" AND '. $target_field_qry .' ) A'. $q3_index .', ';
					if($q3_cds != '') $q3_cds .= ',';
					$q3_cds .= $data2->s_code;
					$q3_index++;
				}
				
				$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
				$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
				$q .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.comp_kind IN ('. $q3_cds .') AND '. $target_field_qry .' ) T ) ';
			}
			
			// 업종 별 총계 쿼리 생성
			$q3_index = 1;
			$q4_t = '';
			$codes = '"'. str_replace(',', '","', $codes) .'"';
			foreach($datas2 as $data2) {
				if($q4_t != '') $q4_t .= ', ';
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.comp_kind = "'. $data2->s_code .'" AND '. $target_field .' IN ('. $codes .') ) T'. $q3_index++;
			}
			$q4_t .= ', (SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.comp_kind IN ('. $q3_cds .') AND '. $target_field .' IN ('. $codes .') ) TT';
			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;
		}
		//---------------------------------------------------------
		// 구분 8 : 직종 - S014
		//---------------------------------------------------------
		else if($args['kind'] == 'down08') {

			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3 . $where5;

			$datas = self::get_sub_code_by_m_code($args['csl_code']);
			$q = '';
			// for total
			$codes = '';

			// 직종 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 검색 대상 컬럼명
				$target_field = 'ages'; // 연령대
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
					$target_field = 'gender'; 
				}

				// 쿼리 조합
				$target_field_qry = 'C.'. $target_field .'="'. $code .'"'; 

				if($q != '') {
					$q .= ' UNION ';
				}
				
				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				
				$q3_cds = '';
				$q3_index = 1;
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.work_kind = "'. $data2->s_code .'" AND '. $target_field_qry .' ) A'. $q3_index .', ';
					if($q3_cds != '') $q3_cds .= ',';
					$q3_cds .= $data2->s_code;
					$q3_index++;
				}
				
				$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
				$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
				$q .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.work_kind IN ('. $q3_cds .') AND '. $target_field_qry .' ) T ) ';
			}
			
			// 직종 별 총계 쿼리 생성
			$q3_index = 1;
			$q4_t = '';
			$codes = '"'. str_replace(',', '","', $codes) .'"';
			foreach($datas2 as $data2) {
				if($q4_t != '') $q4_t .= ', ';
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.work_kind = "'. $data2->s_code .'" AND '. $target_field .' IN ('. $codes .') ) T'. $q3_index++;
			}
			$q4_t .= ', (SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.work_kind IN ('. $q3_cds .') AND '. $target_field .' IN ('. $codes .') ) TT';
			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;

		}
		//---------------------------------------------------------
		// 구분 9 : 사용주체 - S027
		//---------------------------------------------------------
		else if($args['kind'] == 'down09') {

			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3 . $where5;

			$datas = self::get_sub_code_by_m_code($args['csl_code']);
			$q = '';
			// for total
			$codes = '';

			// 사용주체 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND);
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 초기화
				$target_field = '';
				
				// 검색 대상 컬럼명
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) {// 연령대
					$target_field = 'ages'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) {// 근로자수
					$target_field = 'emp_cnt'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
					$target_field = 'gender'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
					$target_field = 'work_kind'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
					$target_field = 'comp_kind'; 
				}

				// 쿼리 조합
				$target_field_qry = 'AND C.'. $target_field .'="'. $code .'"'; 

				if($q != '') {
					$q .= ' UNION ';
				}
				
				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				
				$q3_cds = '';
				$q3_index = 1;
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_use_kind = "'. $data2->s_code .'" '. $target_field_qry .' ) A'. $q3_index .', ';
					if($q3_cds != '') $q3_cds .= ',';
					$q3_cds .= $data2->s_code;
					$q3_index++;
				}
				
				$q .= $q1 .'T.ck'. $q3_index .' as tot '. $q2 . $q3;
				$q3_cds = '"'. str_replace(',', '","', $q3_cds) .'"';
				$q .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_use_kind IN ('. $q3_cds .') '. $target_field_qry .' ) T ) ';
			}
			
			// 사용주체 별 총계 쿼리 생성
			$q3_index = 1;
			$q4_t = '';
			$codes = '"'. str_replace(',', '","', $codes) .'"';
			foreach($datas2 as $data2) {
				if($q4_t != '') $q4_t .= ', ';
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_use_kind = "'. $data2->s_code .'" AND C.'. $target_field .' IN ('. $codes .') ) T'. $q3_index++;
			}
			$q4_t .= ', (SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_use_kind IN ('. $q3_cds .') AND C.'. $target_field .' IN ('. $codes .') ) TT';
			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;

		}
		//---------------------------------------------------------
		// 구분 10 : 기타(마지막)
		//---------------------------------------------------------
		else if($args['kind'] == 'down10') {
			$csl_code = $args['csl_code'];
			$csl_code_org = $csl_code;
			
			// 거주지와 회사소재지는 같은 코드를 사용하나 통계에서 조회를 따로하기에 따로 상수를 정의했다.
			// 그래서 회사소재지로 검색할 때 주거지 코드로 바꿔준다.
			if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP) { // 거주지
				$csl_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
			}

			// where - 상담일, 상담자 기본 검색대상
			$where .= $where1 . $where2 . $where3 . $where5;

			$datas = self::get_sub_code_by_m_code($csl_code);
			$q = '';
			// for total
			$codes = '';

			// 근로자수 code
			$datas2 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
			
			// 4대보험 code
			$datas3 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
			
			// 근로계약서 code
			$datas4 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
			
			foreach($datas as $data) {
				$q3 = '';
				$code = $data->s_code;

				// for total
				if(!empty($codes)) $codes .= ',';
				$codes .= $code;
				
				// 초기화
				$target_field = '';
				
				// 검색 대상 컬럼명
				if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
					$target_field = 'work_kind'; 
				}
				else if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
					$target_field = 'comp_kind'; 
				}
				else if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND) {// 고용형태
					$target_field = 'emp_kind'; 
				}
				else if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) {// 성별
					$target_field = 'gender'; 
				}
				// 회사 소재지를 거주지보다 먼저 체크해야 한다.
				else if($csl_code_org == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP) { // 회사소재지
					$target_field = 'comp_addr'; 
				}
				else if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS) { // 거주지
					$target_field = 'live_addr'; 
				}
				else if($csl_code == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) { // 연령대
					$target_field = 'ages'; 
				}
				else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_USE_KIND) { // 사용주체
					$target_field = 'emp_use_kind'; 
				}

				// 쿼리 조합
				$target_field_qry = 'AND C.'. $target_field .'="'. $code .'"'; 

				if($q != '') {
					$q .= ' UNION ';
				}
				
				$q1 = 'SELECT DD.code_name as subject,';
				$q2 = 'FROM ('
					.'(SELECT code_name FROM sub_code WHERE s_code="'. $code .'") DD, ';
				
				$q3_index = 1;
				$q4 = '';

				// 근로자수 code
				foreach($datas2 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_cnt = "'. $data2->s_code .'" '. $target_field_qry .' ) A'. $q3_index .', ';
					$q3_index++;
				}
				
				// 4대보험 code
				foreach($datas3 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					$q3 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_insured_yn = "'. $data2->s_code .'" '. $target_field_qry .' ) A'. $q3_index .', ';
					$q3_index++;
				}
				
				// 근로계약서 code
				foreach($datas4 as $data2) {
					$q1 .= 'A'. $q3_index .'.ck'. $q3_index .', ';
					if($q4 != '' ) $q4 .= ', ';
					$q4 .= '(SELECT count(C.seq) as ck'. $q3_index .' FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_paper_yn = "'. $data2->s_code .'" '. $target_field_qry .' ) A'. $q3_index;
					$q3_index++;
				}

				// 제일끝에 ',' 제거
				$q1 = substr($q1, 0, -2) .' ';
				
				$q .= $q1 . $q2 . $q3 . $q4 .')';
			}	

			// 총계 쿼리 생성
			// - 근로자수 별 
			$q3_index = 1;
			$q4_t = '';
			$codes = '"'. str_replace(',', '","', $codes) .'"';
			foreach($datas2 as $data2) {
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_cnt = "'. $data2->s_code .'" AND C.'. $target_field .' IN ('. $codes .') ) T'. $q3_index++ .', ';
			}

			// - 4대보험 별 
			foreach($datas3 as $data2) {
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_insured_yn = "'. $data2->s_code .'" AND C.'. $target_field .' IN ('. $codes .') ) T'. $q3_index++ .', ';
			}

			// - 근로계약서 별 
			foreach($datas4 as $data2) {
				$q4_t .= '(SELECT count(C.seq) FROM counsel C '. $oper_join .'WHERE '. $where .' AND C.emp_paper_yn = "'. $data2->s_code .'" AND C.'. $target_field .' IN ('. $codes .') ) T'. $q3_index++ .', ';
			}
			// 제일끝에 ',' 제거
			$q4_t = substr($q4_t, 0, -2) .' ';

			$query_tot = 'SELECT '. $q4_t;

			// main query
			$query = $q;
		}

		//---------------------------------------------------------
		// 구분 11 : 교차통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down11') {

			$target_field1 = '';
			$csl_kind = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$where_omb = ''; //옴부즈만 검색
			$where_asso = '';

			//교차 가로축 검색
			if($args['sel_col'] == 0){
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); //상담방법
				$target_field1 = 'C.s_code';
			}
			elseif($args['sel_col'] == 1) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER); //성별
				$target_field1 = 'C.gender';
			}
			elseif($args['sel_col'] == 2) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES); //연령대
				$target_field1 = 'C.ages';
			}
			elseif($args['sel_col'] == 3) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); //거주지
				$target_field1 = 'C.live_addr';
			}
			elseif($args['sel_col'] == 4) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); //회사소재지
				$target_field1 = 'C.comp_addr';
			}
			elseif($args['sel_col'] == 5) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND); //직종
				$target_field1 = 'C.work_kind';
			}
			elseif($args['sel_col'] == 6) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND); //업종
				$target_field1 = 'C.comp_kind';
			}
			elseif($args['sel_col'] == 7) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND); //고용형태
				$target_field1 = 'C.emp_kind';
			}
			elseif($args['sel_col'] == 8) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT); //근로자수
				$target_field1 = 'C.emp_cnt';
			}
			elseif($args['sel_col'] == 9) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN); //근로계약서
				$target_field1 = 'C.emp_paper_yn';
			}
			elseif($args['sel_col'] == 10) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN); //4대보험
				$target_field1 = 'C.emp_insured_yn';
			}
			elseif($args['sel_col'] == 11) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND); //상담유형
				$target_field1 = 'CS2.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sel_col'] == 12) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE); //상담동기
				$target_field1 = 'C.csl_motive_cd';
			}

			//교차 세로축 검색
			if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION) {// 소속
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
				$target_field2 = 'C.asso_code';
				if(isset($args['asso_code'][0]) && $args['asso_code'][0] != '') {
					$where_asso = 'AND (SC.s_code in (' .$args['asso_code'][0]. '))';
				}
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {// 상담자
				$target_field2 = 'C.oper_id';
				$where_omb = 'AND O.oper_auth_grp_id = "GRP013" ';
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) {// 상담유형
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND;
				$target_field2 = 'CS2.s_code';
				$target_field3 = 'DT.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND;
				$target_field2 = 'C.work_kind'; 
				$target_field3 = 'DT.work_kind'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
				$target_field2 = 'C.comp_kind'; 
				$target_field3 = 'DT.comp_kind'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS) { // 거주지
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field2 = 'C.live_addr'; 
				$target_field3 = 'DT.live_addr'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP) { // 회사소재지
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field2 = 'C.comp_addr'; 
				$target_field3 = 'DT.comp_addr'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND) {// 고용형태
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND;
				$target_field2 = 'C.emp_kind'; 
				$target_field3 = 'DT.emp_kind'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) { // 근로자수
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT;
				$target_field2 = 'C.emp_cnt'; 
				$target_field3 = 'DT.emp_cnt'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN) { // 근로계약서
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN;
				$target_field2 = 'C.emp_paper_yn';
				$target_field3 = 'DT.emp_paper_yn'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN) { // 4대보험
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN;
				$target_field2 = 'C.emp_insured_yn'; 
				$target_field3 = 'DT.emp_insured_yn'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) { // 연령대
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_AGES;
				$target_field2 = 'C.ages';
				$target_field3 = 'DT.ages'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_GENDER;
				$target_field2 = 'C.gender';
				$target_field3 = 'DT.gender'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE) {// 상담동기
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE;
				$target_field2 = 'C.csl_motive_cd'; 
				$target_field3 = 'DT.csl_motive_cd'; 
			}

			// 상담일자 외 - 소속/성별/직종/업종/거주지+회사소재지/고용형태/근로자수/근로계약서/4대보험/연령대
			if($args['csl_code'] != 'csl_date') {

				// where - 상담일, 상담방법, 처리결과 검색
				$where .= $where1 . $where2 . $where3 . $where5 . $where_omb;
				//$where .= $where1 . $where3 . $where5;

				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'select S.subject ';
				$t3 = '';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';

					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
						$t1 .= 'CONCAT(IFNULL(SUM(M.ck'.$index.'), 0), "(", IFNULL(M.distinct_ck'.$index.', 0), ")")';
					}
					else{
						$t1 .= 'SUM(M.ck'.$index.')';
					}
					if($t_tot != '') $t_tot .= ' + ';

					$t_tot .= 'SUM(M.ck'.$index.')';
			
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;
					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
						$t2 .= ', S.distinct_ck'.$index;
					}
					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
						if($t3 != '') $t3 .= ', ';
						$t3 .= '(SELECT count(*) as distinct_ck'.$index .' FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
							.'WHERE '.$where .' AND '.$target_field1.' = "'.$data->s_code.'" ) AS distinct_ck'.$index.' ';
					}

					$index++;
				}
				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ")")';
				}
				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 . $csl_kind2 .' FROM ( SELECT DD.code_name AS subject, A.ck1, A.target ';
				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t .= ', (SELECT count(*) as distinct_tot FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
					     .'WHERE '.$where.' ) AS distinct_tot ';
				}
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t .= ', '.$t3;
				}
				if($args['csl_code'] != CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {// 상담자 외
					$t .= 'FROM (SELECT code_name, s_code, dsp_order FROM sub_code ';
					$t .=' WHERE m_code = "'.$datas2.'" ) DD ';
				}
				elseif($args['csl_code'] == 'operator'){
					$t .= 'FROM (SELECT oper_name as code_name, oper_id as s_code, s_code as dsp_order FROM operator ) DD ';
				}
				$t .='LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_field2.' as target2, C.oper_id, '.$target_field1.' as target FROM counsel C '
					.'INNER JOIN operator O ON O.oper_id = C.oper_id ';

				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', '.$target_field2.' ) A ON A.target2 = DD.s_code '
					.') S ) M ';

				//총계 쿼리
				$query_tot = $t;
				// echof($t);
				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
					
						$total = explode('(', $total);
						$total = $total[0];
					
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
				$csl_kind2 = '';
				$csl_kind3 = '';
			}
				$q = '';
				$index = 1;
				$q1 = 'select M.subject ';
				$q_tot = '';
				$q2 = 'select S.subject ';
				foreach($datas1 as $data) {

						$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
						$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") as ck'.$index;

						if($q_tot != '') $q_tot .= ' + ';

						$q_tot .= 'sum(M.ck'.$index.')';
						$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

						$index++;
				}

				
				if($total != 0){	
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					if($args['sel_col'] == 11){
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ") (", IFNULL('.$per.', 0),"%)")';
					}
					else{
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
					}
				}

				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.', S.dsp_order '.$csl_kind2.' from ( select DD.code_name AS subject, A.ck1, A.target, DD.dsp_order '.$csl_kind3;
					if($args['csl_code'] != CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {// 상담자 외
						$q .= ' FROM (SELECT SC.code_name, SC.s_code, SC.dsp_order '.$csl_kind4.' FROM sub_code SC ';
						if($args['sel_col'] == 11){
							$q .= 'LEFT JOIN (SELECT COUNT(*) as distinct_tot, '.$target_field2.' FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
							.'WHERE '.$where.' GROUP BY '.$target_field2.') DT ON SC.s_code = '.$target_field3.' ';
						}
						$q .= 'WHERE m_code = "'.$datas2.'" '.$where_asso.') DD ';
					}	
					else{	//상담자
						$q .= 'FROM (SELECT O.oper_name as code_name, O.oper_id as s_code, O.s_code as dsp_order FROM operator O WHERE 1=1 '.$where_omb.') DD ';
					}
				$q .= 'LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_field2.' as target2, '.$target_field1.' as target FROM counsel C '
					.'INNER JOIN operator O ON O.oper_id = C.oper_id ';

				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){ //상담유형
					$q .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', '.$target_field2.' ) A ON A.target2 = DD.s_code '
					.') S ) M group by M.subject ORDER BY M.dsp_order ASC ';
				
				$query = $q;
			  	 //echof($query);

				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}

				$rstRtn['tot_cnt'] = count($rstRtn['data']);

				// $rstRtn['query'] = $query .'# '. $query_tot;
			}
			// 상담일자
			else {

				// 검색일자 - 전체인 경우 빈값이다.
				if(isset($args['csl_date']) && $args['csl_date'] != '') {
					$sch_date = $args['csl_date'];
				}
				else {
					$sch_date = 'C.csl_date BETWEEN "1900-01-01 00:00:00" AND "'. get_date() .'" ';
				}

				// where - 상담일, 상담방법, 처리결과 검색
				$where .= $where1 . $where2 . $where3 . $where5;
				//$where .= $where1 . $where3 . $where5;

				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'select S.subject ';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';
					$t1 .= 'SUM(M.ck'.$index.')';
					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'SUM(M.ck'.$index.')';
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;

					$index++;
				}

				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 .' FROM ( SELECT DD.csl_date AS subject, A.ck1, A.target '
				.'FROM (SELECT DISTINCT C.csl_date FROM counsel C '
				. 'WHERE 1=1 '.$where1.') DD '
				.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.csl_date AS target2, C.oper_id, '.$target_field1.' AS target FROM counsel C '
				.'INNER JOIN operator O ON O.oper_id = C.oper_id '
				.'WHERE ' .$where.' ' 
				.'GROUP BY '.$target_field1.', C.csl_date) A ON A.target2 = DD.csl_date '
				.') S ) M ';

				//총계 쿼리
				$query_tot = $t;
				// echof($t);
				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
					
					$total = explode('(', $total);
					$total = $total[0];
					
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}

				$q = '';
				$index = 1;
				$q1 = 'select M.subject ';
				$q_tot = '';
				$q2 = 'select S.subject ';
				foreach($datas1 as $data) {
					if(isset($total) && $total != 0){	
						$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
						$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") AS ck'.$index;
					}
					else{
						$q1 .= ', IFNULL(sum(M.ck'.$index.'), 0) AS ck'.$index;
					}

					if($q_tot != '') $q_tot .= ' + ';

					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;

					$index++;
				}

				
				if(isset($total) && $total != 0){	
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
				}
				else{
					$q_tot = 'IFNULL('.$q_tot.', 0)';
				}

				$q = $q1 .', '. $q_tot .' AS tot FROM ('.$q2.' FROM ( SELECT DD.csl_date AS subject, A.ck1, A.target '
				.'FROM (SELECT DISTINCT C.csl_date FROM counsel C '
				. 'WHERE 1=1 '.$where1.') DD '
				.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.csl_date AS target2, '.$target_field1.' AS target FROM counsel C '
					.'INNER JOIN operator O ON O.oper_id = C.oper_id '
				. 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', C.csl_date ) A ON A.target2 = DD.csl_date '
					.') S ) M group by M.subject ORDER BY M.subject DESC ';
				
				$query = $q;


				// 상담일자인 경우, 데이터가 없으면 $q가 공백이라 오류가 발생하는 문제 처리
				if(empty($q)) {
					$query = 'SELECT seq FROM counsel WHERE 1=0';
					$query_tot = $query;
				}
				else {
					$query = $q;
					$query_tot = $t;
				}
			  // echof($query);
				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}
			}
			
			$rstRtn['tot_cnt'] = count($rstRtn['data']);

			// $rstRtn['query'] = $query .'# '. $query_tot;
		}

		//---------------------------------------------------------
		// 구분 12 : 월별 통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down12') {

			$current_year = $args['sel_year'];
			$half_year = $args['half_year'];
			if($half_year == 1){
				$first = 1;
				$last = 12;
			}
			elseif($half_year == 2){
				$first = 1;
				$last = 6;
			}
			elseif($half_year == 3){
				$first = 7;
				$last = 12;
			}

			// where - 상담방법, 처리결과 검색
			$where .= $where3 . $where5;

			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = 'select S.subject '; // S.subject2
			for($month=$first; $month<=$last; $month++) {
				if($month < 10) $month = '0'.$month;

				if($t1 != '') $t1 .= ', ';
				$t1 .= 'sum(M.ck'.$index.')';

				if($t_tot != '') $t_tot .= ' + ';

				$t_tot .= 'sum(M.ck'.$index.')';
				$t2 .= ', IF(S.target LIKE "%'.$current_year.'-'.$month.'%", S.ck1, 0) as ck'.$index;

				$index++;
			}
			if($args['asso_code'][0] == "'C131'"){
				$t = 'select '.$t1 .', '. $t_tot .' as tot FROM ('.$t2.' from ( select DD2.oper_name AS subject, A.ck1, A.target ' //DD2.oper_name AS subject2
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"][0].')) DD '
					.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id ="GRP013") DD2 ON DD.s_code = DD2.s_code '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					//.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code ' //  C.oper_id,
					.') S ) M ';
			}
			else{		
				$t = 'select '.$t1 .', '. $t_tot .' as tot FROM ('.$t2.' from ( select DD.code_name AS subject, A.ck1, A.target ' //DD2.oper_name AS subject2
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"][0].')) DD '
					/*.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_id in ('.$args['oper_id'][0].')) DD2 ON DD.s_code = DD2.s_code '*/
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';

				$t .= 'WHERE ' .$where.' ' 
					//.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code ' //  C.oper_id,
					.') S ) M ';
			}

			//총계 쿼리
			$query_tot = $t;
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
			}

			$q = '';
			$index = 1;
			$q1 = 'select M.subject '; // M.subject2
			$q_tot = '';
			$q2 = 'select S.subject '; // S.subject2
			for($month=$first; $month<=$last; $month++) {
				if($month < 10) $month = '0'.$month;

				$q1 .= ', sum(M.ck'.$index.') as ck'.$index;

				if($q_tot != '') $q_tot .= ' + ';

				$q_tot .= 'sum(M.ck'.$index.')';
				$q2 .= ', IF(S.target LIKE "%'.$current_year.'-'.$month.'%", S.ck1, 0) as ck'.$index;

				$index++;
			}
			if($args['asso_code'][0] == "'C131'"){
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.' from ( select DD2.oper_name AS subject, A.ck1, A.target ' // DD2.oper_name AS subject2
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"][0].')) DD '
					.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id = "GRP013") DD2 ON DD.s_code = DD2.s_code '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';
					$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					//.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code ' //
					.') S ) M group by M.subject '; // M.subject2
			}
			else{
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.' from ( select DD.code_name AS subject, A.ck1, A.target ' // DD2.oper_name AS subject2
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"][0].')) DD '
					//.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id = "GRP013" DD2 ON DD.s_code = DD2.s_code '*/
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';
				$q .= 'WHERE ' .$where.' ' 
					//.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code ' //  C.oper_id,
					.') S ) M group by M.subject '; // M.subject2
			}

			$query = $q;
		// echof($query);

			// # 쿼리 실행
			if(!empty($query)) {
				$rst = $this->db->query($query);
				$rstRtn['data'] = $rst->result();
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data']);

			// $rstRtn['query'] = $query .'# '. $query_tot;
			
		}

		//---------------------------------------------------------
		// 구분 13 : 기본통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down13') {

			$target_field1 = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$data_suj = '';

			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = ''; //, S.subject2 ';

			if($args['sch_kind'] == 0) {// 0.상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 1.소속
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);
				$target_field1 = 'C.asso_code';
				if($args['asso_code'][0] == "'C131'"){
					$datas1 = $datas1 = self::get_oper_id_by_s_code('C131');
					$target_field1 = 'C.asso_code';
				}
			}
			elseif($args['sch_kind'] == 2) {// 2.성별
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER);
				$target_field1 = 'C.gender';
			}
			elseif($args['sch_kind'] == 3) { // 3.연령대
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES);
				$target_field1 = 'C.ages'; 
			}
			elseif($args['sch_kind'] == 4) { // 4.거주지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				$target_field1 = 'C.live_addr'; 
			}
			elseif($args['sch_kind'] == 5) { // 5.회사소재지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				$target_field1 = 'C.comp_addr'; 
			}
			elseif($args['sch_kind'] == 6) {// 6.직종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND);
				$target_field1 = 'C.work_kind'; 
			}
			elseif($args['sch_kind'] == 7) {// 7.업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
				$target_field1 = 'C.comp_kind'; 
			}
			elseif($args['sch_kind'] == 8) {// 8.고용형태
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND);
				$target_field1 = 'C.emp_kind'; 
			}
			elseif($args['sch_kind'] == 9) { // 9.근로자수
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
				$target_field1 = 'C.emp_cnt'; 
			}
			elseif($args['sch_kind'] == 10) { // 10.근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN);
				$target_field1 = 'C.emp_paper_yn'; 
			}
			elseif($args['sch_kind'] == 11) { // 11.4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN);
				$target_field1 = 'C.emp_insured_yn'; 
			}
			elseif($args['sch_kind'] == 12) {// 12.상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND);
				$target_field1 = 'CS2.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD2.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sch_kind'] == 13) {// 13.처리결과
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST);
				$target_field1 = 'C.csl_proc_rst'; 
			}
			elseif($args['sch_kind'] == 14) {// 14.상담동기
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE);
				$target_field1 = 'C.csl_motive_cd'; 
			}
			// where - 상담일, 상담방법, 처리결과 검색
			$where .= $where1 . $where3 . $where5;

			foreach($datas1 as $data) {
				if($args['sch_kind'] == 1) {	//소속 유형 검색 시, 선택한 소속만 데이터를 가져오도록 처리
					if($args['asso_code'][0] == "'C131'"){
						if(strpos($data->oper_name, '옴부즈만')){
							if($t1 != '') $t1 .= ', ';
							$t1 .= '"'.$data->oper_name. '" AS s'.$index;
							$t1 .= ', sum(M.ck'.$index.') AS t'.$index;

							if($t_tot != '') $t_tot .= ' + ';
							$t_tot .= 'sum(M.ck'.$index.')';

							if($t2 != '') $t2 .= ', ';
							$t2 .= ' IF(S.target = "'.$data->oper_id.'", S.ck1, 0) as ck'.$index;

							$index++;
						}
					}
					else{
						if(strpos($args['asso_code'][0], $data->s_code)){
							if($t1 != '') $t1 .= ', ';
							$t1 .= '"'.$data->code_name. '" AS s'.$index;
							$t1 .= ', sum(M.ck'.$index.') AS t'.$index;

							if($t_tot != '') $t_tot .= ' + ';
							$t_tot .= 'sum(M.ck'.$index.')';

							if($t2 != '') $t2 .= ', ';
							$t2 .= ' IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

							$index++;
						}
					}
				}
				else{
					if($t1 != '') $t1 .= ', ';
					$t1 .= '"'.$data->code_name. '" AS s'.$index;
					$t1 .= ', sum(M.ck'.$index.') AS t'.$index;

					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'sum(M.ck'.$index.')';

					if($t2 != '') $t2 .= ', ';
					$t2 .= ' IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

					$index++;
				}
			}
			$t1 .= ', "총계" AS s'.$index;
			if($args['sch_kind'] == 12){
				$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ")")';
			}
			if($args['sch_kind'] == 1 && $args['asso_code'][0] == "'C131'"){
				$t = 'SELECT '.$t1 .', '. $t_tot .' as tot FROM ( SELECT'.$t2. ' FROM ( select A.ck1, A.target '
					.' FROM (SELECT code_name, s_code FROM sub_code ';
				if(isset($args['asso_code'][0]) && $args['asso_code'][0] != '') {
					$t .= 'WHERE s_code in (' .$args['asso_code'][0]. '))';
				}
				$t .= 'DD LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id as target FROM counsel C '
					.'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id ) A ON A.asso_code = DD.s_code '
					.') S ) M ';
					//echof($t);
			}
			else{
				$t = 'SELECT '.$t1 .', '. $t_tot .' as tot FROM ( SELECT'.$t2. $csl_kind2 .' FROM ( select A.ck1, A.target ';
					if($args['sch_kind'] == 12){
						$t .= ', (SELECT COUNT(*) as distinct_tot FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
						.'WHERE '.$where. $where2.') AS distinct_tot ';
					}
					$t .=' FROM (SELECT code_name, s_code FROM sub_code ';
					if(isset($args['asso_code'][0]) && $args['asso_code'][0] != '') {
						$t .= 'WHERE s_code in (' .$args['asso_code'][0]. '))';
					}
					$t .= 'DD LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, '.$target_field1.' as target FROM counsel C ';

				if($args['sch_kind'] == 12){
					$t .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, '.$target_field1.' ) A ON A.asso_code = DD.s_code '
					.') S ) M ';
			}
			//총계 쿼리
			$query_tot = $t;
			// echof($t);
			$total = 1;
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
				$total = $rstRtn['data_tot'][0]->tot;
				
				$total = explode('(', $total);
				$total = $total[0];
				
				/*if($total == 0){
					$rstRtn['data_tot'][0]->tot .= ' (0%)';
				}
				else{
					$rstRtn['data_tot'][0]->tot .= ' (100%)';
				}*/
			}

/*			$q = '';
			$index = 1;
			$q1 = 'select M.subject '; //, M.subject2 ';
			$q_tot = '';
			$q2 = 'select S.subject '; //, S.subject2 ';
			foreach($datas1 as $data) {
					
					$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
					$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), "(", IFNULL('.$per.', 0), "%)") as ck'.$index;

					if($q_tot != '') $q_tot .= ' + ';

					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

					$index++;
			}

			if($total != 0){	
				$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
				if($args['sch_kind'] == 12){
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ") (", IFNULL('.$per.', 0),"%)")';
					}
					else{
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
					}
			}

			$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2 . $csl_kind2.' from ( select DD.code_name AS subject, A.ck1, A.target ' .$csl_kind4
				.' FROM (SELECT code_name, s_code FROM sub_code '
				.' WHERE s_code in ('.$args["asso_code"][0].')) DD ';
				//.'LEFT JOIN (SELECT SC.oper_name, SC.s_code, SC.oper_id '.$csl_kind4.' FROM operator SC ';
				if($args['sch_kind'] == 12){
					$q .= 'LEFT JOIN (SELECT COUNT(*) as distinct_tot, C.asso_code FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
					.'WHERE '.$where. $where2.' GROUP BY C.oper_id) DT ON DD.s_code = DT.asso_code ';
				}
				//$q .= 'WHERE SC.oper_id in ('.$args['oper_id'][0].')) DD2 ON DD.s_code = DD2.s_code ';
				$q .='LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, '.$target_field1.' as target FROM counsel C ';

			if($args['sch_kind'] == 12){ //상담유형
				$q .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
			}

			$q .= 'WHERE ' .$where.' ' 
				.'GROUP BY C.asso_code, C.oper_id, '.$target_field1.' ) A ON A.asso_code = DD.s_code ' // AND DD2.oper_id = A.oper_id '
				.') S ) M GROUP BY M.subject '; //, M.subject2 ';
			
			$query = $q;
		 // echof($query);

			// # 쿼리 실행
			if(!empty($query)) {
				$rst = $this->db->query($query);
				$rstRtn['data'] = $rst->result();
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data_tot'][0]);

			// $rstRtn['query'] = $query .'# '. $query_tot;*/
			$rstRtn['tot_cnt'] = count($rstRtn['data_tot'][0]);
		}

		return $rstRtn;
	}

	
	
		
	//#################################################################################################################
	// Board
	//#################################################################################################################
	
	//=================================================================================================================
	// Board
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_board_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_board_list($args) {
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		$where = 'WHERE 1=1 ';

		// target
		// - 전체 검색
		if($args['target'] == 'all') {
			// keyword
			if($args['keyword']) {
				$where .= ' AND (B.title LIKE "%'. $args['keyword'] .'%" OR '
					.'B.content LIKE "%'. $args['keyword'] .'%" OR '
					.'O.oper_id LIKE "%'. $args['keyword'] .'%" OR '
					.'O.oper_name LIKE "%'. $args['keyword'] .'%") ';
			}
		}
		else {
			// keyword
			if($args['keyword']) {
				$where .= ' AND '. $args['target'] .' LIKE "%'. $args['keyword'] .'%" ';
			}
		}
		
		// 상담 방법 코드 : s_code
		if($args['csl_way']) {
			$where .= ' AND B.s_code = "'. $args['csl_way'] .'" ';
		}

		// 상담일 : search_date_begin, search_date_end
		if($args['search_date_begin']) {
			$where .= ' AND B.reg_date BETWEEN "'. $args['search_date_begin'] .' 00:00:00" AND "'. $args['search_date_end'] .' 23:59:59" ';
		}
		
		$brd_id = $args['brd_id'];

		// fields
		$fields = 'B.* ';
		

		$query1 = 'SELECT '. $fields .', O.oper_name ';
		$query2 = 'FROM '. $brd_id .' B '
			.'INNER JOIN '. $this->tbl_oper .' O ON B.oper_id = O.oper_id ';
			
		// 운영게시판 외 join 추가
		if($args['brd_id'] != $this->tbl_brd_oper) {
			$query1 .= ', S.code_name ';
			$query2 .= 'INNER JOIN '. $this->tbl_sub_code .' S ON B.s_code = S.s_code ';
		}
		$query2 .= ' '. $where .' ';

		$query3 = 'ORDER BY B.seq DESC '
			.'LIMIT '. $offset .', '. $limit;

		$q = $query1 . $query2 . $query3;
		// echof($q);exit;
		
		// query
		$rs = $this->db->query($q);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$query1 =  'SELECT COUNT(B.seq) as cnt ';
		$rs = $this->db->query($query1 . $query2);
		$tmp = $rs->result();

		// $rstRtn['query'] = $q;

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}

	

	//-----------------------------------------------------------------------------------------------------------------
	// get_noti_articles : 해당 게시판의 공지글만 가져온다
	//-----------------------------------------------------------------------------------------------------------------
	public function get_noti_articles($args) {
		$table_name = $args['tbl_name'];

		$query = 'SELECT B.*, O.oper_name, "notice_article" '
			.'FROM '. $table_name .' B '
			.'INNER JOIN '. $this->tbl_oper .' O ON B.oper_id = O.oper_id '
			.'WHERE NOW() BETWEEN B.notice_dt_begin AND B.notice_dt_end '
			.'ORDER BY B.notice_dt_end ASC';
		$rs = $this->db->query($query);
		$rstRtn['data'] = $rs->result();

		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_notice_popup_data - 운영자게시판의 공지 중 팝업 공지 데이터 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function get_notice_popup_data($args) {

		$seq = $args['seq'];
		$where = array(
			'seq' => $seq
		);
		$rs = $this->db->select($this->tbl_brd_oper.'.*,' . $this->tbl_oper.'.oper_name')
			->join($this->tbl_oper, $this->tbl_oper.'.oper_id='.$this->tbl_brd_oper.'.oper_id')
			->get_where($this->tbl_brd_oper, $where);
		$rstRtn = $rs->result();

		return $rstRtn;

	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_exist_file_data_board : 해당 게시판의 글 수정시 해당 게시물의 첨부파일의 이름을 리턴한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function get_exist_file_data_board($args) {
		$rst = $this->db->select('file_name,file_name_org')->get_where($args['table_name'], array('seq'=> $args['seq']) );
		$rstRtn = $rst->result();

		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// add_board : 등록
	//-----------------------------------------------------------------------------------------------------------------
	public function add_board($args) {
		$args['reg_date'] = get_date();
		
		$table_name = $args['table_name'];
		unset($args['table_name']);
		unset($args['seq']);
		
		$this->db->insert($table_name, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'error-fail insert';
		$rstRtn['new_id'] = '';
		
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = '';
			$rstRtn['new_id'] = $this->db->insert_id();
		}
		
		return $rstRtn;
	}	
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_board
	//-----------------------------------------------------------------------------------------------------------------
	// - return var :
	//  $rst - 처리결과, 항상 1 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_board($args) {
		$where = array(
			'seq' => $args['seq']
		);
		
		$args['mod_date'] = get_date();

		$table_name = $args['table_name'];

		// 불필요한 데이터 삭제
		unset($args['table_name']);
		unset($args['seq']);
		
		// 항상 1 리턴, 
		// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
		$rst = $this->db->where($where)->update($table_name, $args);
		// $this->get_last_query();
		
		return $rst;
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_board
	//-----------------------------------------------------------------------------------------------------------------
	// - return var :
	//  $rstRtn['rst'] - 처리결과, succ/fail
	//  $rstRtn['img_nm'] - 이미지 이름
	//-----------------------------------------------------------------------------------------------------------------
	public function del_board($args) {
		$where = array(
			'seq' => $args['seq']
		);

		// 이미지 이름 반환
		$rs = $this->db->select('file_name')->get_where($args['brd_id'], $where);
		$rst = $rs->result();

		$rstRtn['file_name'] = '';
		if(count($rst) > 0) {
			$rstRtn['file_name'] = $rst[0]->file_name;
		}

		// 삭제
		$this->db->delete($args['brd_id'], $where);
		
		$affected_rows = $this->db->affected_rows();
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($affected_rows > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_board : 게시물 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_board($args) {

		// view인 경우, counting
		if(isset($args['kind']) && $args['kind'] == 'view') {
			$query = 'UPDATE '. $args['brd_id'] .' SET view_cnt = view_cnt+1 WHERE seq = "'. $args['seq'] .'" ';
			$this->db->query($query);
		}

		$where = 'B.seq = "'. $args['seq'] .'" ';
		
		$fields = 'B.*, O.oper_name  ';
		// 공유게시판 외 상담유형코드 추가
		if($args['brd_id'] != $this->tbl_brd_oper) {
			$fields .= ', S.code_name ';
		}
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $args['brd_id'] .' B '
			.'INNER JOIN '. $this->tbl_oper.' O ON O.oper_id = B.oper_id ';
		
			// 공유게시판 외 join 추가
			if($args['brd_id'] != $this->tbl_brd_oper) {
				$query .= 'INNER JOIN '. $this->tbl_sub_code .' S ON B.s_code = S.s_code ';
			}

			$query .= 'WHERE '. $where;
			
		// $rstRtn['query'] = $query;
		// echof($query);


		// query
		$rs = $this->db->query($query);
		
		$rstRtn= $rs->result();
				
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// 기존 이미지 파일명 가져오기
	//-----------------------------------------------------------------------------------------------------------------
	// - return : image name
	//-----------------------------------------------------------------------------------------------------------------
	public function get_exist_img_name($args) {
		$where = array(
			'seq' => $args['seq']
		);
		$rst = $this->db->select('file_name')->get_where($args['brd_id'], $where);
		
		$file_nm = '';
		if(count($rst) > 0) {
			$rstRtn = $rst->result();
			$file_nm = $rstRtn[0]->file_name;
		}
		
		return $img_nm;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_popup_notice : 운영자게시판 공지게시물 중 팝업 게시물 seq 리턴
	//-----------------------------------------------------------------------------------------------------------------
	public function get_popup_notice() {

		$query = 'SELECT GROUP_CONCAT(seq) as seq '
			.'FROM '. $this->tbl_brd_oper .' '
			.'WHERE notice_yn=1 AND popup_yn=1 AND "'. get_date() .'" BETWEEN notice_dt_begin AND notice_dt_end';
		$rs = $this->db->query($query);

		$rst = $rs->result();
		$rstRtn = $rst[0]->seq;

		return $rstRtn;
	}



	
	//#################################################################################################################
	// Counsel
	//#################################################################################################################
	
	//=================================================================================================================
	// Counsel
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_counsel_list
	//-----------------------------------------------------------------------------------------------------------------
	// [권한]
	// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
	// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
	// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
	// - 자치구 공무원 : 소속 자치구의 옴부즈만 상담만 조회,인쇄 가능 - 2016.08.09 추가
	//-----------------------------------------------------------------------------------------------------------------
	public function get_counsel_list($args) {
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// 소속코드 : asso_code - 자신이 속한 소속의 상담만 가져온다.
		// 고도화 - 위 조건 변경됨
		// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
		// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
		// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
		$where = 'WHERE 1=1 ';


		// 주요상담사례 게시판에서 호출한 경우 - 공유된 전체 상담만 가져옴(권한과 무관)
		if($args['is_board'] == 1) {
			$where .= 'AND C.csl_share_yn = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
		}
		// master 제외 모든 상담자 대상
		else if($args['is_master'] != 1) {

			// 권한 부분 쿼리
			$where .= self::_get_counsel_comm_query();
		}


		// 검색 시작 ------------------------------------------------------
		$search = 'WHERE 1=1 ';
		$inner_join_counsel_keyword = '';
		$keyword = $args['keyword'];
		
		// 상담방법 : csl_way
		if($args['csl_way']) {
			$search .= 'AND Q1.s_code = "'. $args['csl_way'] .'" ';
		}

		// 상담일 : search_date_begin, search_date_end
		// - 양쪽 다 있는 경우
		if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
			$search .= 'AND Q1.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
			$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND Q1.csl_date LIKE "'. $begin_year[0] .'%" ';
		}

		// 검색
		// 수정 : 직종,업종 처리 추가 2017.02.10 dylan
		// 직종
		if($args['target'] == 'counsel_work_kind') {
			if($args['target_work_kind'] != '') {
				$search .= 'AND Q1.work_kind = "'. $args['target_work_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.work_kind_etc LIKE "%'. $keyword .'%" ';
			}

		}
		// 업종
		else if($args['target'] == 'counsel_comp_kind') {
			if($args['target_comp_kind'] != '') {
				$search .= 'AND Q1.comp_kind = "'. $args['target_comp_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.comp_kind_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 주제어 
		else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') { 
				$inner_join_counsel_keyword = 'INNER JOIN '. $this->tbl_counsel_sub .' CS ON C.seq=CS.csl_seq AND CS.s_code="'. $args['target_keyword'] .'" '
					.'INNER JOIN '. $this->tbl_sub_code .' CS1 ON CS.s_code = CS1.s_code AND CS1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'"';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$inner_join_counsel_keyword = 'INNER JOIN (SELECT TCS.csl_seq FROM '. $this->tbl_counsel_sub .' TCS INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'" GROUP BY TCS.csl_seq) CS1 ON C.seq=CS1.csl_seq ';
			}
		}
		// 처리결과
		else if($args['target'] == 'counsel_proc_rst') {
			if($args['target_csl_proc_rst'] != '') {
				$search .= 'AND Q1.csl_proc_rst = "'. $args['target_csl_proc_rst'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 상담자 id - 2015.11.23 요청에 의한 추가 delee
		else if($args['target'] == 'counsel_oper_id') {
			if($keyword != '') {
				$search .= 'AND Q1.oper_id LIKE "%'. $keyword .'%" ';
			}
		}
		// 상담자명 
		else if($args['target'] == 'counsel_oper_name') {
			if($keyword != '') {
				$search .= 'AND Q1.oper_name LIKE "%'. $keyword .'%" ';
			}
		}
		// 추가 : dylan 2017.09.12, danvistory.com
		// 내담자 전화번호 뒤 4자리 검색
		else if($args['target'] == 'counsel_csl_tel') {
			if($keyword != '') {
				$search .= 'AND Q1.csl_tel LIKE "%'. $keyword .'%" ';
			}
		}
		// 전체검색
		else if($args['target'] == '') {
			if($keyword != '') {
				$search .= ' AND ( Q1.csl_title LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_content LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_reply LIKE "%'. $keyword .'%" '
					.'OR Q1.oper_name LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_name LIKE "%'. $keyword .'%" '
					.'OR Q1.work_kind_etc LIKE "%'. $keyword .'%" ' // 업종 기타
					.'OR Q1.comp_kind_etc LIKE "%'. $keyword .'%" ' // 직종 기타
					.'OR Q1.csl_tel LIKE "%'. $keyword .'%" ' // 내담자 전화번호 뒤 4자리 - 추가 : dylan 2017.09.12, danvistory.com
					.') ';
			}
		}
		else {
			if($keyword != '') {
				$search .= 'AND '. $args['target'] .' LIKE "%'. $keyword .'%" ';
			}
		}
		
		// 검색 끝 ------------------------------------------------------


		// fields
		// 필드명 변경, reg_date->csl_date - 2015.11.23 요청에 의한 변경 delee
		// 필드명 추가 : 원글 key 2017.02.01 dylan
		$fields2 = 'Q1.seq,Q1.csl_title,Q1.csl_content,Q1.csl_date,Q1.oper_name,Q1.csl_way'
			.',Q1.oper_id,Q1.oper_kind,Q1.csl_proc_rst,Q1.csl_name,Q1.csl_tel,Q1.gender,Q1.ages,Q1.ages_etc,Q1.live_addr,Q1.live_addr_etc,Q1.work_kind'
			.',Q1.work_kind_etc,Q1.comp_kind,Q1.comp_addr,Q1.comp_addr_etc,Q1.emp_kind,Q1.emp_kind_etc,Q1.emp_cnt,Q1.emp_cnt_etc,Q1.emp_paper_yn'
			.',Q1.emp_insured_yn,Q1.ave_pay_month,Q1.work_time_week,Q1.csl_reply,Q1.csl_share_yn,Q1.s_code,Q1.csl_ref_seq,Q1.S2code_name,Q1.S3code_name '
			.',Q1.csl_proc_rst_etc,Q1.S4code_name,Q1.comp_kind_etc,SC.code_name as asso_name ';
		$fields1 = 'C.seq,C.csl_title,C.csl_content,C.csl_date,O.oper_name,O.s_code as asso_code,S.code_name as csl_way'
			.',O.oper_id,O.oper_kind,C.csl_proc_rst,C.csl_name,C.csl_tel,C.gender,C.ages,C.ages_etc,C.live_addr,C.live_addr_etc,C.work_kind,C.work_kind_etc'
			.',C.comp_kind,C.comp_addr,C.comp_addr_etc,C.emp_kind,C.emp_kind_etc,C.emp_cnt,C.emp_cnt_etc,C.emp_paper_yn,C.emp_insured_yn'
			.',C.ave_pay_month,C.work_time_week,C.csl_reply,C.csl_share_yn,S.s_code,C.csl_ref_seq,S2.code_name as S2code_name,S3.code_name as S3code_name '
			.',C.csl_proc_rst_etc,S4.code_name as S4code_name,C.comp_kind_etc ';

		$query1 = 'SELECT '. $fields2 .' ';
		$query2 = 'FROM (SELECT '. $fields1 .' ' 
			.'FROM '. $this->tbl_counsel .' C '
			.$inner_join_counsel_keyword
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
			.'INNER JOIN '. $this->tbl_sub_code .' S3 ON C.work_kind = S3.s_code ' // 직종
			.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.$where .') Q1 INNER JOIN '. $this->tbl_sub_code .' SC ON Q1.asso_code=SC.s_code ';
		
		// 엑셀 다운로드 일 경우 limit 제외
		$orderby = ' ORDER BY Q1.csl_date DESC, Q1.seq DESC ';
		if(! isset($args['is_excel'])) {
			$orderby .= 'LIMIT '. $offset .', '. $limit;
		}

		$q = $query1 . $query2 . $search . $orderby ;
		// $rstRtn['query'] = $q;
// 		echof($q);

		// query
		$rs = $this->db->query($q);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$q =  $query1 . $query2 . $search;
		$rs = $this->db->query($q);
		$tmp = $rs->result();
		// $rstRtn['query2'] = $q;

		$rstRtn['tot_cnt'] = count($tmp);
		
		return $rstRtn;
	}

	
	/**
	 * 상담목록, 관련상담/불러오기 팝업 목록에서 사용하는 권한에 따른 공통 퀴리 생성 함수
	 *
	 */
	private function _get_counsel_comm_query() {
		$query = '';
		// 기본 - 권익센터,OO센터 직원 : 소속 상담만 노출
		if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK1) {
			$query .= 'AND O.s_code = "'. $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) .'" ';
		}
		// 서울시 : 자신의 글과 옴부즈만 상담만 노출(참고 : 서울시는 상담등록을 하지 않는다고 한다.)
		else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2) {
			$query .= 'AND ('
					.'O.oper_kind = "'. CFG_OPERATOR_KIND_CODE_OK3 .'" OR C.oper_id = "'. $this->session->userdata(CFG_SESSION_ADMIN_ID) .'"'
				.') ';
		}
		// 옴부즈만 : 자신의 상담만 노출
		else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK3) {
			$query .= 'AND C.oper_id = "'. $this->session->userdata(CFG_SESSION_ADMIN_ID) .'" ';
		}
		// 자치구 공무원 : 소속 자치구의 옴부즈만 상담만 조회,인쇄 가능 - 2016.08.09 추가
		else {
			$query .= 'AND ('
					.'O.oper_kind = "'. CFG_OPERATOR_KIND_CODE_OK3 .'" AND O.oper_kind_sub = "'. $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE_SUB) .'"'
				.') ';
		}

		return $query;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_counsel_list_pop
	// 사용하는 view : adm_counsel_list_popup_view, adm_counsel_rel_list_popup_view
	//-----------------------------------------------------------------------------------------------------------------
	// [권한]
	// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
	// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
	// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
	// * 수정 
	// - 공유하기 팝업 호출인 경우 공유하기 인 상담만을 소속과 무관하게 보여준다. 2017.01.18 dylan
	// * <관련상담> 수정인 경우 팝업 목록,검색시 보고있는 상담은 대상에서 제외한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function get_counsel_list_pop($args) {
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// 기본적으로 자신이 속한 기관의 상담만 가져온다.
		$where = 'WHERE 1=1 ';

		// master 제외
		if(isset($args['is_master']) && $args['is_master'] != 1) {

			// 권한 부분 쿼리 (상담목록 쿼리와 동일)
			$where .= self::_get_counsel_comm_query();

		}

		// <관련상담> 수정인 경우 팝업 목록,검색시 보고있는 상담은 대상에서 제외한다.
		if($args['seq'] != '') {
			$where .= ' AND C.seq<>'. $args['seq'] .' ';
		}

		// 관련상담 팝업인 경우 - 내담자의 이름과 동일한 상담을 가져온다.
		if(isset($args['csl_name']) && trim($args['csl_name']) != '') {
			$where .= ' AND C.csl_name LIKE "%'. $args['csl_name'] .'%" ';
		}

		// <불러오기> 팝업인 경우
		// - 관련상담 조건 + 공유하기 인 상담
		if(isset($args['csl_share_yn'])) {
			$where .= ' OR C.csl_share_yn = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
		}


		// 검색 시작 ------------------------------------------------------
		$search = 'WHERE 1=1 ';
		$inner_join_counsel_keyword = '';
		$keyword = $args['keyword'];
		
		// 상담방법 : csl_way
		if($args['csl_way']) {
			$search .= 'AND Q1.s_code = "'. $args['csl_way'] .'" ';
		}

		// 상담일 : search_date_begin, search_date_end
		// - 양쪽 다 있는 경우
		if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
			$search .= 'AND Q1.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
			$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND Q1.csl_date LIKE "'. $begin_year[0] .'%" ';
		}

		// 검색
		// 수정 : 직종,업종 처리 추가 2017.02.10 dylan
		// 직종
		if($args['target'] == 'counsel_work_kind') {
			if($args['target_work_kind'] != '') {
				$search .= 'AND Q1.work_kind = "'. $args['target_work_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.work_kind_etc LIKE "%'. $keyword .'%" ';
			}

		}
		// 업종
		else if($args['target'] == 'counsel_comp_kind') {
			if($args['target_comp_kind'] != '') {
				$search .= 'AND Q1.comp_kind = "'. $args['target_comp_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.comp_kind_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 주제어 
		else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') { 
				$inner_join_counsel_keyword = 'INNER JOIN '. $this->tbl_counsel_sub .' CS ON C.seq=CS.csl_seq AND CS.s_code="'. $args['target_keyword'] .'" '
					.'INNER JOIN '. $this->tbl_sub_code .' CS1 ON CS.s_code = CS1.s_code AND CS1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'"';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$inner_join_counsel_keyword = 'INNER JOIN (SELECT TCS.csl_seq FROM '. $this->tbl_counsel_sub .' TCS INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'" GROUP BY TCS.csl_seq) CS1 ON C.seq=CS1.csl_seq ';
			}
		}
		// 처리결과
		else if($args['target'] == 'counsel_proc_rst') {
			if($args['target_csl_proc_rst'] != '') {
				$search .= 'AND Q1.csl_proc_rst = "'. $args['target_csl_proc_rst'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 상담자 id - 2015.11.23 요청에 의한 추가 delee
		else if($args['target'] == 'counsel_oper_id') {
			if($keyword != '') {
				$search .= 'AND Q1.oper_id LIKE "%'. $keyword .'%" ';
			}
		}
		// 상담자명 
		else if($args['target'] == 'counsel_oper_name') {
			if($keyword != '') {
				$search .= 'AND Q1.oper_name LIKE "%'. $keyword .'%" ';
			}
		}
		// 요청에 의한 추가 : dylan 2017.09.12, danvistory.com
		// 내담자 전화번호 뒤 4자리 검색
		else if($args['target'] == 'counsel_csl_tel') {
			if($keyword != '') {
				$search .= 'AND Q1.csl_tel LIKE "%'. $keyword .'%" ';
			}
		}
		// 전체검색
		else if($args['target'] == '') {
			if($keyword != '') {
				$search .= ' AND ( Q1.csl_title LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_content LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_reply LIKE "%'. $keyword .'%" '
					.'OR Q1.oper_name LIKE "%'. $keyword .'%" '
					.'OR Q1.csl_name LIKE "%'. $keyword .'%" '
					.'OR Q1.work_kind_etc LIKE "%'. $keyword .'%" ' // 업종 기타
					.'OR Q1.comp_kind_etc LIKE "%'. $keyword .'%" ' // 직종 기타
					.'OR Q1.csl_tel LIKE "%'. $keyword .'%" ' // 내담자 전화번호 뒤 4자리 - 추가 : dylan 2017.09.12, danvistory.com
					.') ';
			}
		}
		else {
			if($keyword != '') {
				$search .= 'AND '. $args['target'] .' LIKE "%'. $keyword .'%" ';
			}
		}
		
		// 검색 끝 ------------------------------------------------------


		// fields
		// 필드명 변경, reg_date->csl_date - 2015.11.23 요청에 의한 변경 delee
		// 필드명 추가 : 원글 key 2017.02.01 dylan
		$fields2 = 'Q1.seq,Q1.csl_title,Q1.csl_content,Q1.csl_date,Q1.oper_name,Q1.csl_method_nm'
			.',Q1.oper_id,Q1.oper_kind,Q1.csl_proc_rst,Q1.csl_name,Q1.csl_tel,Q1.gender,Q1.ages,Q1.ages_etc,Q1.live_addr,Q1.live_addr_etc,Q1.work_kind'
			.',Q1.work_kind_etc,Q1.comp_kind,Q1.comp_addr,Q1.comp_addr_etc,Q1.emp_kind,Q1.emp_kind_etc,Q1.emp_cnt,Q1.emp_cnt_etc,Q1.emp_paper_yn'
			.',Q1.emp_insured_yn,Q1.ave_pay_month,Q1.work_time_week,Q1.csl_reply,Q1.csl_share_yn,Q1.s_code,Q1.csl_ref_seq,Q1.S2code_name,Q1.S3code_name '
			.',Q1.csl_proc_rst_etc,Q1.S4code_name,Q1.comp_kind_etc ';
		$fields1 = 'C.seq,C.csl_title,C.csl_content,C.csl_date,O.oper_name,O.s_code as asso_code,S.code_name as csl_method_nm'
			.',O.oper_id,O.oper_kind,C.csl_proc_rst,C.csl_name,C.csl_tel,C.gender,C.ages,C.ages_etc,C.live_addr,C.live_addr_etc,C.work_kind,C.work_kind_etc'
			.',C.comp_kind,C.comp_addr,C.comp_addr_etc,C.emp_kind,C.emp_kind_etc,C.emp_cnt,C.emp_cnt_etc,C.emp_paper_yn,C.emp_insured_yn'
			.',C.ave_pay_month,C.work_time_week,C.csl_reply,C.csl_share_yn,S.s_code,C.csl_ref_seq,S2.code_name as S2code_name,S3.code_name as S3code_name '
			.',C.csl_proc_rst_etc,S4.code_name as S4code_name,C.comp_kind_etc ';
		// if($inner_join_counsel_keyword != '') {
		// 	$fields2 .= ',Q1.CS1code_name ';
		// 	$fields1 .= ',CS1.code_name as CS1code_name ';
		// }

		$query1 = 'SELECT '. $fields2 .' ';
		$query2 = 'FROM (SELECT '. $fields1 .' ' 
			.'FROM '. $this->tbl_counsel .' C '
			.$inner_join_counsel_keyword
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
			.'INNER JOIN '. $this->tbl_sub_code .' S3 ON C.work_kind = S3.s_code ' // 직종
			.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.$where .') Q1 ';
		
		// $orderby = '';
		// $orderby .= '';//INNER JOIN '. $this->tbl_sub_code .' SC ON Q1.asso_code=SC.s_code ';

		// 엑셀 다운로드 일 경우 limit 제외
		$orderby = ' ORDER BY Q1.csl_date DESC, Q1.seq DESC ';
		if(! isset($args['is_excel'])) {
			$orderby .= 'LIMIT '. $offset .', '. $limit;
		}

		$q = $query1 . $query2 . $search . $orderby ;
		// $rstRtn['query'] = $q;
// 		echof($q);

		// query
		$rs = $this->db->query($q);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		// $q =  $query1 . $query2 .') Q1 INNER JOIN sub_code SC ON Q1.asso_code=SC.s_code ' . $search;
		$q =  $query1 . $query2 . $search;
		$rs = $this->db->query($q);
		$tmp = $rs->result();
		// $rstRtn['query2'] = $q;

		$rstRtn['tot_cnt'] = count($tmp);
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_counsel : counsel NEW 저장
	//-----------------------------------------------------------------------------------------------------------------
	public function add_counsel($args) {
		$args['reg_date'] = get_date();
		$args['regular'] = 1;

		$csl_sub_code = $args['csl_sub_code'];
		unset($args['csl_sub_code']);

		unset($args['is_del_file']);
		
		$rs = $this->db->insert($this->tbl_counsel, $args);
		$new_id = $this->db->insert_id();
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;

			// 상담유형,주제어 저장
			// - 고용형태
			$csl_sub_code['csl_seq'] = $new_id;
			self::_insert_csl_sub_code($csl_sub_code);
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_counsel : counsel update
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_counsel($args) {
		$args['mod_date'] = get_date();
		
		$is_del_file = $args['is_del_file'];
		unset($args['is_del_file']);
		if($is_del_file != 1) {
			$csl_sub_code = $args['csl_sub_code'];
			unset($args['csl_sub_code']);
		}
		
		$where['seq'] = $args['seq'];

		$rs = $this->db->where($where)->update($this->tbl_counsel, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 파일 삭제가 아닌 경우
			if($is_del_file != 1) {
				// 상담유형,주제어 저장 - 기존 코드 삭제후 다시 저장한다.
				// - 기존 코드 삭제
				$this->db->delete($this->tbl_counsel_sub, array('csl_seq'=>$args['seq']));
				// - 새로 저장
				$csl_sub_code['csl_seq'] = $args['seq'];
				self::_insert_csl_sub_code($csl_sub_code);
			}
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_counsel_ref : <관련상담> update 
	//-----------------------------------------------------------------------------------------------------------------
	// "관련상담" 처리 로직
	// 원글과 원글 가져와 작성된 상담1, 2, ...n 모두 공유항목을 업데이트 처리한다. 
	// 원글을 가져와 작성한 상담2,3,...n을 다시 가져와 작성한 경우도 마찬가지로 처리한다.
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_counsel_ref($args) {
		
		$args['mod_date'] = get_date();

		// 1.원글의 csl_ref_seq 컬럼 업데이트
		$where = array(
			'seq' => $args['csl_ref_seq']
		);
		$data = array(
			'csl_ref_seq' => $args['csl_ref_seq']
		);
		$this->db->where($where)->update($this->tbl_counsel, $data);
		
		// 2.모든 csl_ref_seq 이 같은 상담 업데이트
		$data = array(
			'csl_name' => $args['csl_name']
			,'csl_tel' => $args['csl_tel']
			,'gender' => $args['gender']
			,'ages' => $args['ages']
			,'ages_etc' => $args['ages_etc']
			,'live_addr' => $args['live_addr']
			,'live_addr_etc' => $args['live_addr_etc']
			,'work_kind' => $args['work_kind']
			,'work_kind_etc' => $args['work_kind_etc']
			,'comp_kind' => $args['comp_kind']
			,'comp_addr' => $args['comp_addr']
			,'comp_addr_etc' => $args['comp_addr_etc']
			,'emp_kind' => $args['emp_kind']
			,'emp_kind_etc' => $args['emp_kind_etc']
			,'emp_cnt' => $args['emp_cnt']
			,'emp_cnt_etc' => $args['emp_cnt_etc']
			,'emp_paper_yn' => $args['emp_paper_yn']
			,'emp_insured_yn' => $args['emp_insured_yn']
			,'ave_pay_month' => $args['ave_pay_month']
			,'work_time_week' => $args['work_time_week']
		);
		$where = array(
			'csl_ref_seq' => $args['csl_ref_seq']
		);
		$this->db->where($where)->update($this->tbl_counsel, $data);

		return true;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// _insert_csl_sub_code : 상담 insert, update 시 고용형태,상담유형,주제어 테이블 처리 helper
	//-----------------------------------------------------------------------------------------------------------------
	private function _insert_csl_sub_code($args) {

		$data = array();
		
		// - 고용형태 - 중복체크 가능하게 요청 뒤 원복 요청으로 주석처리
		// if(count($args['arr_emp_kind']) > 0) {
		// 	// 추가된 counsel 테이블의 row id 추가
		// 	for($i=0; $i<count($args['arr_emp_kind']); $i++) {
		// 		$args['arr_emp_kind'][$i]['csl_seq'] = $args['csl_seq'];
		// 	}
		// 	$this->db->insert_batch($this->tbl_counsel_sub, $args['arr_emp_kind']);
		//	$data = array(); // 기존코드 디버그, 2019.06.25 dylan
		// }

		// - 상담유형
		if(count($args['arr_csl_kind']) > 0) {
			// 추가된 counsel 테이블의 row id 추가
			foreach ($args['arr_csl_kind'] as $key => $item) {
				$data[$key]['csl_seq'] = $args['csl_seq'];
				$data[$key]['s_code'] = base64_decode($item);
			}
			$this->db->insert_batch($this->tbl_counsel_sub, $data);
			$data = array(); // 기존코드 디버그, 2019.06.25 dylan
		}
		// - 주제어
		if(count($args['arr_keyword']) > 0) {
			// 추가된 counsel 테이블의 row id 추가
			foreach ($args['arr_keyword'] as $key => $item) {
				$data[$key]['csl_seq'] = $args['csl_seq'];
				$data[$key]['s_code'] = base64_decode($item);
			}
			$this->db->insert_batch($this->tbl_counsel_sub, $data);
		}
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_counsel : 다중 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_counsel($args) {

		$exist_seq = '';

		// 참조된 게시물이 있는지 검사하여 있으면 삭제하지 않도록 한다.
		if($args['check_ref'] === TRUE) {
			$seqs = $args['seqs'];
			$arr_seq = explode(CFG_AUTH_CODE_DELIMITER, $seqs);
			
			foreach($arr_seq as $id) {
				$rst = $this->db->select('seq')->get_where($this->tbl_counsel, array('csl_ref_seq'=>$id));

				if($rst->num_rows > 0) {
					if($exist_seq != '') $exist_seq .= CFG_AUTH_CODE_DELIMITER;
					$exist_seq .= $id;
				}
				else {
					// 첨부파일 정보 가져오기
					$rst = $this->db->select('file_name')->get_where($this->tbl_counsel, array('seq'=>$id));
					$file_data = $rst->result();
					foreach ($file_data as $key => $fnames->file_name) {
						if($fnames->file_name) {
							$arr_fname = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $fnames);
							foreach ($arr_fname as $key => $fname) {
								if($fname) {
									@unlink(CFG_UPLOAD_PATH . $fname);
									clearstatcache();
								}
							}
						}
					}
					
					// 서브 테이블 삭제
					$query = 'DELETE FROM '. $this->tbl_counsel_sub .' WHERE csl_seq IN ("'. $id .'") ';
					$rst = $this->db->query($query);

					// 게시물 삭제
					$query = 'DELETE FROM '. $this->tbl_counsel .' WHERE seq IN ("'. $id .'") ';
					$rst = $this->db->query($query);
				}
			}
		}
		else {
			$seqs = str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['seqs']);
			// 첨부파일 정보 가져와 삭제처리
			$query = 'SELECT file_name FROM '. $this->tbl_counsel .' WHERE seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);
			$file_data = $rst->result();
			foreach ($file_data as $key => $fnames) {
				if($fnames->file_name) {
					$arr_fname = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $fnames->file_name);
					foreach ($arr_fname as $key => $fname) {
						if($fname) {
							@unlink(CFG_UPLOAD_PATH . $fname);
							clearstatcache();
						}
					}
				}
			}
					
			// 서브 테이블 삭제
			$query = 'DELETE FROM '. $this->tbl_counsel_sub .' WHERE csl_seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);

			// 게시물 삭제
			$query = 'DELETE FROM '. $this->tbl_counsel .' WHERE seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);
		}
		
		return $exist_seq;
	}

	
	
	/*
	//-----------------------------------------------------------------------------------------------------------------
	// del_counsel : counsel 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_counsel($args) {
		$where = array(
			'csl_ref_seq' => $args['seq']
		);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		
		// 참조된 게시물이 있는지 검사하여 있으면 삭제하지 않도록 한다.
		if($args['check_ref'] === TRUE) {

			// 운영자에게 할당된 내용이 있으면 삭제하지 못한다.
			$rs = $this->db->select('seq')->get_where($this->tbl_counsel, $where);
			
			// echof($rs->num_rows);

			if($rs->num_rows > 0) {
				$rstRtn['rst'] = 'exist';
				$rstRtn['msg'] = 'exist group id on DB';
			}
			else {
				$this->db->delete($this->tbl_oper, $where);
				
				if($this->db->affected_rows() > 0) {
					$rstRtn['rst'] = 'succ';
					$rstRtn['msg'] = 'ok';
				}
			}
		}
		// 그냥 삭제 처리한다.
		else {
			$this->db->delete($this->tbl_counsel, $where);
			
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
		
		return $rstRtn;		
	}
	*/



	//-----------------------------------------------------------------------------------------------------------------
	// get_counsel : 상담 수정시 데이터 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_counsel($args) {

		$where = 'C.seq = '. $args['seq'] .' ';
		
		// 상담 작성자 정보를 operator 테이블에서 가져오도록 수정 - 20151228 dylan
		$fields = 'C.*, S.code_name as asso_name, O.oper_name,O.oper_kind,O.oper_auth_grp_id,O.s_code as asso_cd ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_counsel .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.asso_code = S.s_code '
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();

		$rstRtn['data'] = $rst;
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 고용형태,상담유형,주제어 가져오기
			$query = 'SELECT CONCAT(GROUP_CONCAT(s_code)) as s_code FROM '. $this->tbl_counsel_sub .' WHERE csl_seq='. $args['seq'] .' GROUP BY csl_seq';
			// query
			$rs2 = $this->db->query($query);
			$tmp2 = $rs2->result();
			$rstRtn['data']['csl_sub_code'] = '';
			if($this->db->affected_rows() > 0) {
				$rstRtn['data']['csl_sub_code'] = $tmp2[0]->s_code;
			}
		}
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_counsel_at : 통계->상담사례에서 상담보기 팝업에서 사용
	//-----------------------------------------------------------------------------------------------------------------
	public function get_counsel_at($args) {
		$rs = $this->db->select('*')->from($this->tbl_counsel)->join($this->tbl_oper, $this->tbl_oper .'.oper_id='. $this->tbl_counsel .'.oper_id')->where($args)->get();

		$rst = $rs->result();

		$rstRtn = '';
		if(count($rst)>0) {
			$rstRtn = $rst[0];
		}

		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_counsel_print : 인쇄용
	//-----------------------------------------------------------------------------------------------------------------
	public function get_counsel_print($args) {

		$where = 'C.seq = '. $args['seq'] .' ';
		
		$fields = 'C.*, O.oper_name '
			.',S.code_name as asso_name '
			.',S1.code_name as csl_method '
			.',S2.code_name as gender '
			.',S3.code_name as ages '
			.',S4.code_name as live_addr '
			.',S5.code_name as work_kind ' 
			.',S6.code_name as comp_kind '
			.',S7.code_name as comp_addr '
			.',S8.code_name as emp_kind '
			.',S9.code_name as emp_cnt '
			.',S10.code_name as emp_paper_yn '
			.',S11.code_name as emp_insured_yn '
			.',S12.code_name as csl_proc_rst '
			.',S13.code_name as csl_share_yn '
			.',S14.code_name as csl_motive '
			.',(SELECT GROUP_CONCAT(Z.code_name) '
			.'FROM '. $this->tbl_counsel_sub .' Q '
			.'INNER JOIN '. $this->tbl_sub_code .' Z ON Z.s_code=Q.s_code '
			.'WHERE Q.csl_seq=C.seq) as csl_kind ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_counsel .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.asso_code ' // 소속code
			.'LEFT JOIN '. $this->tbl_sub_code .' S1 ON S1.s_code = C.s_code ' // 상담방법
			.'LEFT JOIN '. $this->tbl_sub_code .' S2 ON S2.s_code = C.gender ' // 성별
			.'LEFT JOIN '. $this->tbl_sub_code .' S3 ON S3.s_code = C.ages ' // 연령대
			.'LEFT JOIN '. $this->tbl_sub_code .' S4 ON S4.s_code = C.live_addr ' // 거주지code
			.'LEFT JOIN '. $this->tbl_sub_code .' S5 ON S5.s_code = C.work_kind ' // 직종
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.comp_kind ' // 업종
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.comp_addr ' // 업종
			.'LEFT JOIN '. $this->tbl_sub_code .' S8 ON S8.s_code = C.emp_kind ' // 고용형태 
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.emp_cnt ' // 근로자수 
			.'LEFT JOIN '. $this->tbl_sub_code .' S10 ON S10.s_code = C.emp_paper_yn ' // 근로계약서 작성 여부 
			.'LEFT JOIN '. $this->tbl_sub_code .' S11 ON S11.s_code = C.emp_insured_yn ' // 4대보험가입 여부
			.'LEFT JOIN '. $this->tbl_sub_code .' S12 ON S12.s_code = C.csl_proc_rst ' // 상담 처리결과 
			.'LEFT JOIN '. $this->tbl_sub_code .' S13 ON S13.s_code = C.csl_share_yn ' // 상담내역공유
			.'LEFT JOIN '. $this->tbl_counsel_sub .' Q ON Q.csl_seq = C.seq ' // 상담유형 서브테이블
			.'LEFT JOIN '. $this->tbl_sub_code .' S14 ON S14.s_code = C.csl_motive_cd ' // 상담이용동기 - 추가 2016.08.02
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();

		$rstRtn['data'] = array();
		if(count($rst) > 0) {
			$rstRtn['data'] = (array)$rst[0];
		}

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}


	
	//#################################################################################################################
	// Lawhelp
	//#################################################################################################################
	
	//=================================================================================================================
	// Lawhelp
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_lawhelp_list
	// 엑셀 다운로드시에도 호출됨
	//-----------------------------------------------------------------------------------------------------------------
	public function get_lawhelp_list($args) {
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// 권한
		// - 기본 : 권익센터 직원만 글 등록 및 수정,삭제, 자치구센터는 작성하지 않는다. 조회만 가능
		// - 권익센터,서울시,자치자치구 계정 : 모든 상담글 조회 가능
		// - 옴부즈만 계정 : 모든 권한 없음
		$where = 'WHERE 1=0 ';
		// 기본 - 옴부즈만 외 전체
		if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) != CFG_OPERATOR_KIND_CODE_OK3) {
			$where = 'WHERE 1=1 ';
		}

		$keyword = $args['keyword'];
		$target_sprt_kind_cd = $args['target_sprt_kind_cd'];
		$target_apply_rst_cd = $args['target_apply_rst_cd'];
		
		// 검색
		
		// 전체검색
		if($args['target'] == 'all') {
			if($keyword != '') {
				$where .= 'AND ( C.lh_code LIKE "%'. $keyword .'%" ' // 접수번호
					.'OR C.lh_sprt_content LIKE "%'. $keyword .'%" ' // 권리구제지원내용
					.'OR C.lh_apply_nm LIKE "%'. $keyword .'%" ' // 신청자
					.'OR C.lh_labor_asso_nm LIKE "%'. $keyword .'%" ' // 대리인 - 소속
					.'OR C.lh_labor_nm LIKE "%'. $keyword .'%" ' // 대리인 - 이름
					.'OR S2.code_name LIKE "%'. $keyword .'%" ' // 신청기관
					.'OR S3.code_name LIKE "%'. $keyword .'%" ' // 대상기관
					.'OR C.sprt_organ_cd_etc LIKE "%'. $keyword .'%" ' // 대상기관 - 기타
					.' ) ';
			}
		}
		// 지원종류
		else if($args['target'] == 'sprt_kind_cd') {
			if($target_sprt_kind_cd != 'all') $where .= 'AND C.sprt_kind_cd = "'. $target_sprt_kind_cd .'" ';
			if($keyword !='') $where .= 'AND C.sprt_kind_cd_etc LIKE "%'. $keyword .'%" ';
		}
		// 지원결과
		else if($args['target'] == 'apply_rst_cd') {
			if($target_apply_rst_cd != 'all') $where .= 'AND C.apply_rst_cd = "'. $target_apply_rst_cd .'" ';
			if($keyword !='') $where .= 'AND C.apply_rst_cd_etc LIKE "%'. $keyword .'%" ';
		}
		// 대리인 - 소속,이름
		else if($args['target'] == 'lawhelp_lh_labor') {
			if($keyword !='') {
				$where .= 'AND ( C.lh_labor_asso_nm LIKE "%'. $keyword .'%" '
					.'OR C.lh_labor_nm LIKE "%'. $keyword .'%" ) ';
			}
		}
		// 신청기관
		else if($args['target'] == 'apply_organ_cd') {
			if($keyword !='') {
				$where .= 'AND S2.code_name LIKE "%'. $keyword .'%" ';
			}
		}
		// 대상기관
		else if($args['target'] == 'sprt_organ_cd') {
			if($keyword !='') {
				$where .= 'AND ( S3.code_name LIKE "%'. $keyword .'%" '
					.'OR C.sprt_organ_cd_etc LIKE "%'. $keyword .'%" ) ';
			}
		}
		else {
			if($keyword !='') {
				$where .= 'AND C.'. $args['target'] .' LIKE "%'. $keyword .'%" ';
			}
		}
		// echof($where);

		// 일자검색 : 기본 지원승인일 : search_date_begin, search_date_end
		$search_date_begin = $args['search_date_begin'];
		$search_date_end = $args['search_date_end'];
		// - 양쪽 다 있는 경우
		if($search_date_begin != '' && $search_date_end != '') {
			$where .= 'AND C.'. $args['search_date_target'] .' BETWEEN "'. $search_date_begin .'" AND "'. $search_date_end .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($search_date_begin != '' && $search_date_end == '') {
			$begin_year = explode(' ', $search_date_begin);
			$where .= 'AND C.'. $args['search_date_target'] .' LIKE "'. $begin_year[0] .'%" ';
		}

		// 년도 검색 : 대상 - 지원승인일 
		if($args['search_year'] != 'all') {
			$where .= 'AND C.lh_sprt_cfm_date LIKE "'. $args['search_year'] .'%" ';
		}

		// 검색 : 연령 검색
		if($args['search_ages'] != 'all') {
			$where .= 'AND C.ages_cd = "'. $args['search_ages'] .'" ';
		}

		// fields
		// 수정 : C.reg_oper_id 추가, 2017.01.18 by dylan
		$fields1 = 'C.reg_oper_id,C.seq,C.lh_code,C.lh_apply_nm,S5.code_name as ages_nm, C.ages_etc
			,S6.code_name as gender_cd_nm,S7.code_name as lh_kind_cd_nm
			,S8.code_name as work_kind_nm, C.work_kind_etc, S9.code_name as comp_kind_nm
			,C.comp_kind_etc, C.lh_apply_comp_nm
			,S.code_name as sprt_kind_cd_nm, C.sprt_kind_cd_etc
			,S2.code_name as apply_organ_cd_nm, C.lh_labor_asso_nm, C.lh_labor_nm
			,S3.code_name as sprt_organ_cd_nm, C.sprt_organ_cd_etc
			,C.lh_sprt_cfm_date,C.lh_accept_date,C.lh_case_end_date, S4.code_name as apply_rst_cd_nm
			,(
				SELECT GROUP_CONCAT(lid_date ORDER BY lid_date ASC) 
				FROM '. $this->tbl_lawhelp_invtgt_date .'
				WHERE seq=C.seq
			) as lid_date
			,(
				SELECT GROUP_CONCAT(SB2.code_name ORDER BY SB2.code_name ASC) 
				FROM '. $this->tbl_lawhelp_kind_sub_cd .' SB1
				INNER JOIN '. $this->tbl_sub_code .' SB2 ON SB1.kind_sub_cd=SB2.s_code
				WHERE SB1.seq=C.seq
			) as lh_kind_sub_cd_nm
			,apply_rst_cd_etc,lh_sprt_content,lh_etc'; // Excel download 용 추가 컬럼

		$query1 = 'SELECT '. $fields1 .' ' 
			.'FROM '. $this->tbl_lawhelp .' C '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.sprt_kind_cd ' // 지원종류
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON S2.s_code = C.apply_organ_cd ' // 신청기관
			.'INNER JOIN '. $this->tbl_sub_code .' S3 ON S3.s_code = C.sprt_organ_cd ' // 대상기관
			.'LEFT JOIN '. $this->tbl_sub_code .' S4 ON S4.s_code = C.apply_rst_cd ' // 지원결과 - 필수 아님
			.'LEFT JOIN '. $this->tbl_sub_code .' S5 ON S5.s_code = C.ages_cd ' // 연령대 16.09.12 추가 요청
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.gender_cd ' // 성별 18.07.05 추가 
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.lh_kind_cd ' // 구제유형 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S8 ON S8.s_code = C.work_kind ' // 직종 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.comp_kind ' // 업종 18.07.05 추가
			.$where .' ';
		
		$orderby = 'ORDER BY C.lh_code DESC, C.seq DESC ';

		// 엑셀 다운로드 일 경우 limit 제외
		if(! isset($args['is_excel'])) {
			$orderby .= 'LIMIT '. $offset .', '. $limit;
		}

		$q = $query1 . $orderby;
		// echof($q);

		// query
		$rs = $this->db->query($q);
		$rstRtn['data'] = $rs->result();
		// echof($rstRtn['data']);
		
		// total count
		$q =  $query1;
		$rs = $this->db->query($q);
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = count($tmp);
		
		return $rstRtn;
	}

	

	//-----------------------------------------------------------------------------------------------------------------
	// get_lawhelp : 수정 데이터 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_lawhelp($args) {

		// 권한
		// - 기본 : 권익센터 직원만 글 등록 및 수정,삭제, 자치구센터는 작성하지 않는다. 조회만 가능
		// - 권익센터,서울시,자치자치구 계정 : 모든 상담글 조회 가능
		// - 옴부즈만 계정 : 모든 권한 없음
		
		$where = 'C.seq = '. $args['seq'] .' ';		
		$fields = 'C.*, O.oper_name,O.oper_kind,O.oper_auth_grp_id ';

		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_lawhelp .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON O.oper_id=C.reg_oper_id '
			// .'LEFT JOIN '. $this->tbl_lawhelp_invtgt_date .' A ON A.seq = C.seq '
			.'WHERE '. $where;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();
		$rstRtn['data'] = $rst;

		$rstRtn['data']['lid_date_data'] = array();
		$rstRtn['data']['lh_kind_cd_data'] = array();

		$rstRtn['data'] = $rst;
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 출석조사일 가져오기
			$rs = $this->db->select('lid_seq,lid_date')->order_by('lid_date','asc')->get_where($this->tbl_lawhelp_invtgt_date, array('seq'=>$args['seq']));
			$rst = $rs->result();
			$rstRtn['data']['lid_date_data'] = $rst;

			// 권리구제 유형 가져오기 - 추가 2018.07.05
			$rs = $this->db->select('lh_seq,kind_sub_cd')->order_by('lh_seq','asc')->get_where($this->tbl_lawhelp_kind_sub_cd, array('seq'=>$args['seq']));
			$rst = $rs->result();
			$rstRtn['data']['lh_kind_cd_data'] = $rst;
		}
		
		return $rstRtn;
	}


	
	//-----------------------------------------------------------------------------------------------------------------
	// add_lawhelp : NEW 저장
	//-----------------------------------------------------------------------------------------------------------------
	public function add_lawhelp($args) {

		$args['reg_date'] = get_date();

		// 출석조사일
		$lid_date = $args['lid_date'];
		unset($args['lid_date']);
		// 권리구제 유형
		$lh_kind_sub_cd = $args['lh_kind_sub_cd'];
		unset($args['lh_kind_sub_cd']);
		
		$rs = $this->db->insert($this->tbl_lawhelp, $args);
		$new_id = $this->db->insert_id();

		// 접수번호 자동생성 - 직접입력으로 변경 : 최진혁 20160617
		// $year = get_date('Y');
		// $accept_no = $year .'-'. str_pad($new_id, 4, '0', STR_PAD_LEFT);
		// $this->db->where(array('seq'=>$new_id))->update($this->tbl_lawhelp, array('lh_code'=>$accept_no));
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;

			// 출석조사일 일자 저장
			if($lid_date != '') {
				$data = array(
					'seq' => $new_id
					,'lid_date' => $lid_date
				);
				$this->db->insert($this->tbl_lawhelp_invtgt_date, $data);
			}

			// 권리구제 유형 table 저장 - 추가 2018.07.05
			if($lh_kind_sub_cd) {
				foreach ($lh_kind_sub_cd as $key => $value) {
					$data = array(
						'seq' => $new_id
						,'kind_sub_cd' => base64_decode($value)
					);
					$this->db->insert($this->tbl_lawhelp_kind_sub_cd, $data);
				}
			}
			
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_lawhelp : lawhelp update
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_lawhelp($args) {
		
		$args['mod_date'] = get_date();
		
		// 출석조사일
		$lid_date = $args['lid_date'];
		unset($args['lid_date']);
		// 권리구제 유형
		$lh_kind_sub_cd = $args['lh_kind_sub_cd'];
		unset($args['lh_kind_sub_cd']);
		
		$where['seq'] = $args['seq'];

		$rs = $this->db->where($where)->update($this->tbl_lawhelp, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 출석조사일 추가
			if($lid_date != '') {
				$data = array(
					'seq' => $args['seq']
					,'lid_date' => $lid_date
				);
				$this->db->insert($this->tbl_lawhelp_invtgt_date, $data);
			}

			// 권리구제 유형 table 저장 - 추가 2018.07.05
			if(count($lh_kind_sub_cd) > 0) {
				// 기존 데이터 삭제
				$where = array(
					'seq' => $args['seq']
				);
				$this->db->delete($this->tbl_lawhelp_kind_sub_cd, $where);
				// 새로저장
				foreach ($lh_kind_sub_cd as $key => $value) {
					$data = array(
						'seq' => $args['seq']
						,'kind_sub_cd' => base64_decode($value)
					);
					$this->db->insert($this->tbl_lawhelp_kind_sub_cd, $data);
				}
			}
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_lawhelp_invtgt_date : 출석조사일 추가
	//-----------------------------------------------------------------------------------------------------------------
	public function add_lawhelp_invtgt_date($args) {
		
		$this->db->insert($this->tbl_lawhelp_invtgt_date, $args);
		$new_id = $this->db->insert_id();
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_lid_date : 출석조사일 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_lid_date($args) {

		$this->db->where($args)->delete($this->tbl_lawhelp_invtgt_date);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_lawhelp : 다중 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_lawhelp($args) {

		$seqs = str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['seqs']);
		// 첨부파일 정보 가져와 삭제처리
		$query = 'SELECT file_name FROM '. $this->tbl_lawhelp .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);
		$file_data = $rst->result();
		foreach ($file_data as $key => $fnames) {
			if($fnames->file_name) {
				$arr_fname = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $fnames->file_name);
				foreach ($arr_fname as $key => $fname) {
					if($fname) {
						@unlink(CFG_UPLOAD_PATH . $fname);
						clearstatcache();
					}
				}
			}
		}
				
		// 출석조사일 테이블 삭제
		$query = 'DELETE FROM '. $this->tbl_lawhelp_invtgt_date .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);

		// 권리구제 유형 테이블 삭제
		$query = 'DELETE FROM '. $this->tbl_lawhelp_kind_sub_cd .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);

		// 게시물 삭제
		$query = 'DELETE FROM '. $this->tbl_lawhelp .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);
		
		return true;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// get_lawhelp_print : 인쇄용
	//-----------------------------------------------------------------------------------------------------------------
	public function get_lawhelp_print($args) {

		$where = 'C.seq = '. $args['seq'] .' ';
		
		$fields = 'C.lh_code,C.lh_apply_nm,C.lh_apply_addr,C.lh_apply_comp_nm
			,C.lh_comp_addr, C.lh_apply_comp_nm,S.code_name as sprt_kind_cd_nm,C.sprt_kind_cd_etc
			,C.lh_accept_date,C.lh_sprt_content,C.lh_etc'
			.',S2.code_name as apply_organ_cd_nm,C.lh_labor_asso_nm,C.lh_labor_nm ,S3.code_name as sprt_organ_cd_nm'
			.',C.sprt_organ_cd_etc,C.lh_sprt_cfm_date,C.lh_case_end_date, S4.code_name as apply_rst_cd_nm,C.apply_rst_cd_etc, C.ages_etc '
			.',S5.code_name as ages, S6.code_name as gender_cd_nm,S7.code_name as lh_kind_cd_nm
			,S8.code_name as work_kind_nm,S9.code_name as comp_kind_nm '
			.',(SELECT GROUP_CONCAT(lid_date ORDER BY lid_date ASC) FROM '. $this->tbl_lawhelp_invtgt_date .' WHERE seq=C.seq ORDER BY lid_seq DESC) as lid_date
			,(
				SELECT GROUP_CONCAT(SB2.code_name ORDER BY SB2.code_name ASC) 
				FROM '. $this->tbl_lawhelp_kind_sub_cd .' SB1
				INNER JOIN '. $this->tbl_sub_code .' SB2 ON SB1.kind_sub_cd=SB2.s_code
				WHERE SB1.seq=C.seq
			) as lh_kind_sub_cd_nm
			, C.work_kind_etc, C.comp_kind_etc';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_lawhelp .' C '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.sprt_kind_cd ' // 지원종류
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON S2.s_code = C.apply_organ_cd ' // 신청기관
			.'INNER JOIN '. $this->tbl_sub_code .' S3 ON S3.s_code = C.sprt_organ_cd ' // 대상기관
			.'INNER JOIN '. $this->tbl_sub_code .' S5 ON S5.s_code = C.ages_cd ' // 연령대
			.'LEFT JOIN '. $this->tbl_sub_code .' S4 ON S4.s_code = C.apply_rst_cd ' // 지원결과 - 필수 아님
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.gender_cd ' // 성별 18.07.05 추가 
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.lh_kind_cd ' // 구제유형 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S8 ON S8.s_code = C.work_kind ' // 직종 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.comp_kind ' // 업종 18.07.05 추가
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();

		$rstRtn['data'] = array();
		if(count($rst) > 0) {
			$rstRtn['data'] = (array)$rst[0];
		}

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}




	//-----------------------------------------------------------------------------------------------------------------
	// chk_code : 상담 고유코드 중복여부 체크
	//-----------------------------------------------------------------------------------------------------------------
	public function chk_code($args) {

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		$rs = $this->db->select('count(lh_code) as cnt')->get_where($this->tbl_lawhelp, $args);
		$rst = $rs->result();

		if($rst[0]->cnt == 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}






	//#################################################################################################################
	// Operator Manage
	//#################################################################################################################
	
	//=================================================================================================================
	// Oper
	//=================================================================================================================

	//-----------------------------------------------------------------------------------------------------------------
	// change_pwd : 운영자 비빌번호 변경
	//-----------------------------------------------------------------------------------------------------------------
	public function change_pwd($args) {
		// 
		$enc_pwd = dv_hash($args['oper_pwd'], CFG_ENCRYPT_KEY);

		$where = array(
			'oper_id' => $args['oper_id']
			,'oper_pwd' => $enc_pwd
		);
		$rs = $this->db->select('oper_id')->get_where($this->tbl_oper, $where);
		$rst = $rs->result();

		// 비번이 일치하면, 변경처리
		if(count($rst) >= 1) {
			$data = array(
				'oper_pwd' => dv_hash($args['new_pwd'], CFG_ENCRYPT_KEY)
			);
			unset($where['oper_pwd']);
			$this->db->where($where)->update($this->tbl_oper, $data);
			
			$rstRtn['rst'] = 'succ';
		}
		else {
			$rstRtn['rst'] = 'fail';
		}
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// change_oper_pwd : 마스터 관리자가 운영자 비번 변경 처리 : 추가 2016.04.15 23:31:00
	//-----------------------------------------------------------------------------------------------------------------
	public function change_oper_pwd($args) {

		$where = 'oper_id = "'. $args['oper_id'] .'"';

		$data = array(
			'oper_pwd' => dv_hash($args['new_pwd'], CFG_ENCRYPT_KEY)
		);
		$this->db->where($where)->update($this->tbl_oper, $data);
			
		$rstRtn['rst'] = 'succ';
		
		return $rstRtn;
	}



	//-----------------------------------------------------------------------------------------------------------------
	// id_check : 아이디 중복 확인
	//-----------------------------------------------------------------------------------------------------------------
	public function id_check($args) {
		$where = 'oper_id = "'. $args['oper_id'] .'" ';
		
		$rs = $this->db->select('oper_id')->get_where($this->tbl_oper, $where);
		if($rs->num_rows > 0) {
			$rstRtn['rst'] = 'fail';
		}
		else {
			$rstRtn['rst'] = 'succ';
		}
		
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_oper_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper_list($args) {
		// is_master
		$where = '1=1 ';
		if($args['is_master'] == 0) {
			$where = 'O.oper_auth_grp_id <> "'. CFG_AUTH_CODE_ADM_MASTER .'" ';
		}
		
		// use_yn
		if($args['use_yn'] !== 'all') {
			$where .= 'AND O.use_yn = '. $args['use_yn'] .' ';
		}
		
		// keyword
		if($args['keyword']) {
			// 전체 검색
			if(empty($args['target'])) {
				$where .= 'AND (S.code_name LIKE "%'. $args['keyword'] .'%" OR '
					. 'A.oper_auth_name LIKE "%'. $args['keyword'] .'%" OR '
					. 'O.oper_id LIKE "%'. $args['keyword'] .'%" OR '
					. 'O.oper_name LIKE "%'. $args['keyword'] .'%") ';
			}
			else {
				$where .= 'AND '. $args['target'] .' LIKE "%'. $args['keyword'] .'%" ';
			}
		}

		// date
		if($args['search_date_begin']) {
			$where .= 'AND O.reg_date BETWEEN "'. $args['search_date_begin'] .' 00:00:00" AND "'. $args['search_date_end'] .' 23:59:59" ';
		}

		// 소속코드 : s_code
		if($args['s_code']) {
			$where .= 'AND O.s_code = "'. $args['s_code'] .'" ';
		}

		// 권한코드 : oper_auth_grp_id
		if($args['oper_auth_grp_id']) {
			$where .= ' AND O.oper_auth_grp_id = "'. $args['oper_auth_grp_id'] .'" ';
		}
		
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// fields
		$fields = 'O.oper_id, O.oper_name, O.use_yn, O.reg_date, S.code_name, A.oper_auth_name, O.oper_auth_grp_id ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_oper .' O '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON O.s_code = S.s_code '
			.'INNER JOIN '. $this->tbl_auth_grp .' A ON O.oper_auth_grp_id = A.oper_auth_grp_id '
			.'WHERE '. $where .' '
			.'ORDER BY O.reg_date DESC, O.oper_id ASC '
			.'LIMIT '. $offset .', '. $limit;
		// $rstRtn['query'] = $query;
		
		// query
		$rs = $this->db->query($query);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$rs = $this->db->query('SELECT COUNT(O.oper_id) as cnt '
			.'FROM '. $this->tbl_oper .' O '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON O.s_code = S.s_code '
			.'INNER JOIN '. $this->tbl_auth_grp .' A ON O.oper_auth_grp_id = A.oper_auth_grp_id '
			.'WHERE '. $where );
		$tmp = $rs->result();
		
		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}
	
		
	//-----------------------------------------------------------------------------------------------------------------
	// get_oper : operator 조회 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper($args) {
		$where = 'O.oper_id = "'. $args['oper_id'] .'" ';
		
		$fields = 'O.oper_id, O.oper_name, O.use_yn, O.reg_date, O.etc, O.hp, O.tel, O.email, O.s_code, O.oper_kind, O.oper_kind_sub'
			.',S.code_name, A.oper_auth_grp_id, A.oper_auth_name ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_oper .' O '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON O.s_code = S.s_code '
			.'INNER JOIN '. $this->tbl_auth_grp .' A ON O.oper_auth_grp_id = A.oper_auth_grp_id '
			.'WHERE '. $where;
			
		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);
		
		$tmp = $rs->result();
		
		$rstRtn['oper_id'] = $tmp[0]->oper_id;
		$rstRtn['oper_name'] = $tmp[0]->oper_name;
		$rstRtn['use_yn'] = $tmp[0]->use_yn;
		$rstRtn['s_code'] = $tmp[0]->s_code;
		$rstRtn['code_name'] = $tmp[0]->code_name;
		$rstRtn['oper_auth_grp_id'] = $tmp[0]->oper_auth_grp_id;
		$rstRtn['oper_auth_name'] = $tmp[0]->oper_auth_name;
		$rstRtn['oper_kind'] = $tmp[0]->oper_kind;
		$rstRtn['oper_kind_sub'] = $tmp[0]->oper_kind_sub;
		$rstRtn['hp'] = $tmp[0]->hp;
		$rstRtn['tel'] = $tmp[0]->tel;
		$rstRtn['email'] = $tmp[0]->email;
		$rstRtn['etc'] = $tmp[0]->etc;
		$rstRtn['reg_date'] = $tmp[0]->reg_date;
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_oper : operator NEW 저장 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function add_oper($args) {
		$args['reg_date'] = get_date();
		
		$rs = $this->db->insert($this->tbl_oper, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_oper : operator 수정 저장 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_oper($args) {

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = '';
		$rst = null;

		// 관리자가 아닌 경우만
		if($args['is_master'] != 1) {
			$enc_pwd = dv_hash($args['oper_pwd'], CFG_ENCRYPT_KEY);
			$where = 'oper_id = "'. $args['oper_id'] .'" AND oper_pwd="'. $enc_pwd .'" ';
			$rs = $this->db->select('oper_pwd')->get_where($this->tbl_oper, $where);
			$rst = $rs->result();
		}

		// 관리자 또는 데이터가 있으면 변경처리
		if($args['is_master'] == 1 || $args['is_master'] != 1 && count($rst) >= 1) {
			//
			$where = array(
				'oper_id' => $args['oper_id']
			);
			
			unset($args['oper_id']);
			unset($args['oper_pwd']);
			unset($args['is_master']);
			
			$args['mod_date'] = get_date();
			
			// echof($args);
			
			// 항상 1 리턴, 
			// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
			$rst = $this->db->where($where)->update($this->tbl_oper, $args);
		
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
		// 비번이 틀린 경우
		else {
			$rstRtn['msg'] = 'not_match_password';
		}
		
		return $rstRtn;
	}

	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_oper : operator 다중 삭제 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_oper($oper_id) {
		$arr_oper_id = explode(CFG_AUTH_CODE_DELIMITER, $oper_id);

		$exist_oper_id = '';
		foreach($arr_oper_id as $id) {
			$rst = $this->db->select('seq')->get_where($this->tbl_counsel, array('oper_id'=>$id));

			if($rst->num_rows > 0) {
				if($exist_oper_id != '') $exist_oper_id .= CFG_AUTH_CODE_DELIMITER;
				$exist_oper_id .= $id;
			}
			else {
				$query = 'DELETE FROM '. $this->tbl_oper .' WHERE oper_id IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			}
		}
		
		return $exist_oper_id;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// use_or_not_use_oper : operator 다중 사용/중지 처리 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function use_or_not_use_oper($args) {
		$oper_id = '"'. str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['oper_id']) . '"';
		
		$query = 'UPDATE '. $this->tbl_oper .' SET use_yn = '. $args['use_yn'] .' WHERE oper_id IN ('. $oper_id .') ';
		
		$rst = $this->db->query($query);
		
		return $rst;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_oper : oper 삭제 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function del_oper($args) {
		$where = array(
			'oper_id' => $args['oper_id']
		);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		
		// 운영자에게 할당된 내용이 있으면 삭제하지 못한다.
		$rs = $this->db->select('seq')->get_where($this->tbl_counsel, $where);
		// echof($rs->num_rows);
		
		if($rs->num_rows > 0) {
			$rstRtn['rst'] = 'exist';
			$rstRtn['msg'] = 'exist group id on DB';
		}
		else {
			$this->db->delete($this->tbl_oper, $where);
			
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
		
		return $rstRtn;		
	}
	
	
	
	
	//#################################################################################################################
	// Auth Group Manage
	//#################################################################################################################
	
	//=================================================================================================================
	// Auth Group
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_auth_grp_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_auth_grp_list($args) {
		// 마스터관리자 데이터는 가져오지 않는다.
		$where = ' oper_auth_grp_id <> "'. CFG_AUTH_CODE_ADM_MASTER .'" ';
		
		// use_yn
		if(isset($args['use_yn']) && $args['use_yn'] !== 'all') {
			$where .= ' AND use_yn = '. $args['use_yn'];
		}
		
		// keyword
		if(isset($args['oper_auth_name']) && $args['oper_auth_name']) {
			$where .= ' AND oper_auth_name LIKE "%'. $args['oper_auth_name'] .'%" ';
		}
		
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// fields
		$fields = 'oper_auth_grp_id, oper_auth_name, use_yn, reg_date';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_auth_grp .' '
			.'WHERE '. $where .' '
			.'ORDER BY reg_date DESC, oper_auth_name ASC '
			.'LIMIT '. $offset .', '. $limit;
		// $rstRtn['query'] = $query;
		
		// query
		$rs = $this->db->query($query);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$rs = $this->db->query('SELECT COUNT(oper_auth_grp_id) as cnt '
			.'FROM '. $this->tbl_auth_grp .' '
			.'WHERE '. $where);
		$tmp = $rs->result();
		
		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_auth_grp_all_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_auth_grp_all_list($columns=NULL) {
		// fields
		$fields = '*';
		if(!is_null($columns)) {
			$fields = $columns;
		}
		$where = 'WHERE oper_auth_grp_id <> "'. CFG_AUTH_CODE_ADM_MASTER .'" AND use_yn = 1 ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_auth_grp .' '
			. $where .' '
			.'ORDER BY reg_date DESC, oper_auth_name ASC ';
		// $rstRtn['query'] = $query;
		
		// query
		$rs = $this->db->query($query);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$rs = $this->db->query('SELECT COUNT(*) as cnt '
			.'FROM '. $this->tbl_auth_grp);
		$tmp = $rs->result();
		
		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_oper_asso_all_list : 운영자소속코드 데이터 리턴(대코드 M007)
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper_asso_all_list($columns=NULL) {
		// fields
		$fields = '*';
		if(!is_null($columns)) {
			$fields = $columns;
		}
		$where = 'WHERE m_code = "'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION .'" AND use_yn = 1 ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_sub_code .' '
			. $where
			.'ORDER BY dsp_order ASC, code_name ASC ';
		// $rstRtn['query'] = $query;
		
		// query
		$rs = $this->db->query($query);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$rs = $this->db->query('SELECT COUNT(*) as cnt '
			.'FROM '. $this->tbl_sub_code .' '
			. $where);
		$tmp = $rs->result();
		
		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// use_or_not_use_auth_grp : 권한그룹 다중 사용/중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function use_or_not_use_auth_grp($args) {
		$grp_id = '"'. str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['oper_auth_grp_id']) . '"';
		
		$query = 'UPDATE '. $this->tbl_auth_grp .' SET use_yn = '. $args['use_yn'] .' WHERE oper_auth_grp_id IN ('. $grp_id .') ';
		
		$rst = $this->db->query($query);
		
		return $rst;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_auth_grp : 권한그룹 다중 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_auth_grp($args) {
		$arr_grp_id = explode(CFG_AUTH_CODE_DELIMITER, $args['oper_auth_grp_id']);
		$exist_grp_id = '';
		foreach($arr_grp_id as $id) {
			$rst = $this->db->select('oper_id')->get_where($this->tbl_oper, array('oper_auth_grp_id'=>$id));
			if($rst->num_rows > 0) {
				if($exist_grp_id != '') $exist_grp_id .= CFG_AUTH_CODE_DELIMITER;
				$exist_grp_id .= $id;
			}
			else {
				$query = 'DELETE FROM '. $this->tbl_auth_grp .' WHERE oper_auth_grp_id IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			}
		}
		
		return $exist_grp_id;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_auth : 그룹 권한 수정
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_auth_grp($args) {
		$where = array(
			'oper_auth_grp_id' => $args['oper_auth_grp_id']
		);
		
		unset($args['oper_auth_grp_id']);
		
		$args['mod_date'] = get_date();
		
		// 항상 1 리턴, 
		// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
		$rst = $this->db->where($where)->update($this->tbl_auth_grp, $args);
		// $query = $this->get_last_query();
		// echof($query);
		
		return $rst;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_auth_grp : 그룹 추가
	//-----------------------------------------------------------------------------------------------------------------
	public function add_auth_grp($args) {
		// 그룹코드 생성
		$query = 'SELECT MAX(right(oper_auth_grp_id, 3)) as max_index FROM '
			. $this->tbl_auth_grp .' ';
		$rs = $this->db->query($query);
		
		$highest_index = 1;
		if($rs->num_rows > 0) {
			$rst = $rs->result();
			$highest_index = $highest_index + $rst[0]->max_index;
		}
		// 3자릿수 맞춤
		if(strlen($highest_index) == 1) $highest_index = '00'. $highest_index;
		if(strlen($highest_index) == 2) $highest_index = '0'. $highest_index;
		
		$new_grp_id = 'GRP'. $highest_index;
		
		// 저장할 데이터 조합
		$args['oper_auth_grp_id'] = $new_grp_id;
		$args['reg_date'] = get_date();
		$args['mod_date'] = get_date();
		
		$rs = $this->db->insert($this->tbl_auth_grp, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_auth_grp : 그룹 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_auth_grp($args) {
		$where = array(
			'oper_auth_grp_id' => $args['oper_auth_grp_id']
		);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		
		// 운영자에게 할당된 내용이 있으면 삭제하지 못한다.
		$rs = $this->db->select('oper_id')->get_where($this->tbl_oper, $where);
		
		// echof($rs->num_rows);
		
		if($rs->num_rows > 0) {
			$rstRtn['rst'] = 'exist';
			$rstRtn['msg'] = 'exist group id on DB';
		}
		else {
			// echof('select ok ');
			$this->db->delete($this->tbl_auth_grp, $where);
			// echof( $this->db->affected_rows()  );
			
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
		
		return $rstRtn;		
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_auth : 그룹 권한 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_auth($args) {
		$where = array(
			'oper_auth_grp_id' => $args['oper_auth_grp_id']
		);
		$rst = $this->db->select('oper_auth_grp_id, oper_auth_codes, oper_auth_name, etc')->get_where($this->tbl_auth_grp, $where);
		$tmp = $rst->result();
		
		$rstRtn['oper_auth_grp_id'] = $tmp[0]->oper_auth_grp_id;
		$rstRtn['oper_auth_codes'] = $tmp[0]->oper_auth_codes;
		$rstRtn['oper_auth_name'] = $tmp[0]->oper_auth_name;
		$rstRtn['etc'] = $tmp[0]->etc;
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_base_code : 그룹 권한 Data 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_base_code() {
		$query = 'SELECT S.s_code,S.code_name FROM sub_code S '
			. 'INNER JOIN master_code M '
			. 'ON S.m_code = M.m_code '
			. 'WHERE S.m_code IN ("'. CFG_MASTER_CODE_COUNSEL_MANAGE .'",'
			.' "'. CFG_MASTER_CODE_COUNSEL_STATISTIC .'",'
			.' "'. CFG_MASTER_CODE_BOARD .'",'
			.' "'. CFG_MASTER_CODE_OPERATOR .'",'
			.' "'. CFG_MASTER_CODE_AUTH .'",'
			.' "'. CFG_MASTER_CODE_MANAGE_CODE .'", '
			.' "'. CFG_MASTER_CODE_LAW_HELP .'") '
			. 'ORDER BY S.m_code, S.dsp_order';
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
		
		return $rstRtn;
	}
	
	
	
	
	//#################################################################################################################
	// Code Manage
	//#################################################################################################################
	
	//=================================================================================================================
	// Code
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_code : 코드 등록 - 소코드만 등록한다. 대/중 코드는 하드코딩 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function add_code($args) {
		$m_code = $args['m_code'];
		
		// 'C'로 시작하는 sub_code가 있는지 검사
		$rst = $this->db->query(
			'SELECT s_code FROM '. $this->tbl_sub_code .' WHERE s_code LIKE "C%" '
		);
		$tmp = $rst->result();
		
		// 처음
		if($rst->num_rows == 0) {
			$s_code_index = 0;
			$prefix = 'C';
		}
		else {
			$s_code = $tmp[$rst->num_rows - 1]->s_code;
			$prefix = $s_code[0];
			$s_code_index = substr($s_code, 1);
		}
		$highest_index = $s_code_index + 1;
		if(strlen($highest_index) == 1) $highest_index = '00'. $highest_index;
		if(strlen($highest_index) == 2) $highest_index = '0'. $highest_index;
		$highest_s_code = $prefix . $highest_index;
		
		$data = array(
			's_code' => $highest_s_code
			, 'm_code' => $m_code
			, 'code_name' => $args['code_name']
			, 'dsp_order' => $args['dsp_order']
			, 'use_yn' => $args['use_yn']
			, 'desc' => $args['desc']
			, 'reg_date' => get_date()
			, 'mod_date' => get_date()
		);
		$this->db->insert($this->tbl_sub_code, $data);
		
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = '';
		}
		
		return $rstRtn;
	}
	
	/**
	 * 권리구제 유형 전용
	 * @param array $args data
	 */
	public function add_code_lhkind($args) {
		$m_code = $args['m_code'];
		$pcode_prefix = substr($m_code, 0, 1);
		$sub_prefix = 'C';
		if($pcode_prefix == 'C') {
			$sub_prefix = 'B';
		}
		
		// $pcode_prefix로 시작하는 sub_code가 있는지 검사
		$rst = $this->db->query(
			'SELECT s_code FROM '. $this->tbl_sub_code .' WHERE s_code LIKE "'. $sub_prefix .'%" '
		);
		$tmp = $rst->result();
		
		// 처음
		if($rst->num_rows == 0) {
			$s_code_index = 0;
			$prefix = $sub_prefix;
		}
		else {
			$s_code = $tmp[$rst->num_rows - 1]->s_code;
			$prefix = $s_code[0];
			$s_code_index = substr($s_code, 1);
		}
		$highest_index = $s_code_index + 1;
		if(strlen($highest_index) == 1) $highest_index = '00'. $highest_index;
		if(strlen($highest_index) == 2) $highest_index = '0'. $highest_index;
		$highest_s_code = $prefix . $highest_index;
		
		$data = array(
			's_code' => $highest_s_code
			, 'm_code' => $m_code
			, 'code_name' => $args['code_name']
			, 'dsp_order' => $args['dsp_order']
			, 'use_yn' => $args['use_yn']
			, 'desc' => $args['desc']
			, 'reg_date' => get_date()
			, 'mod_date' => get_date()
		);
		$this->db->insert($this->tbl_sub_code, $data);
		
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = '';
		}
		
		return $rstRtn;
	}

	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_code : 코드 수정
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_code($args) {
		$where = array(
			's_code' => $args['s_code']
		);
		
		unset($args['s_code']);
		
		// 항상 1 리턴, 
		// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
		$rst = $this->db->where($where)->update($this->tbl_sub_code, $args);
		// $query = $this->get_last_query();
		// echof($query);
		
		return $rst;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_code : 코드 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_code($args) {
		$where = array(
			's_code' => $args['s_code']
		);
		$rst = $this->db->select('s_code,code_name,dsp_order,use_yn')->get_where($this->tbl_sub_code, $where);
		$tmp = $rst->result();
		
		$rstRtn['code'] = $tmp[0]->s_code;
		$rstRtn['code_name'] = $tmp[0]->code_name;
		$rstRtn['dsp_order'] = $tmp[0]->dsp_order;
		$rstRtn['use_yn'] = $tmp[0]->use_yn;
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_code : 코드 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_code($args) {
		$where = array(
			's_code' => $args['s_code']
		);
		
		$this->db->delete($this->tbl_sub_code, $where);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_code_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_code_list($args) {
		$where = 'm_code="'. $args['m_code'] .'" ';
		
		// use_yn
		if($args['use_yn'] !== 'all') {
			$where .= ' AND use_yn = '. $args['use_yn'];
		}
		
		// keyword
		if(isset($args['code_name']) && $args['code_name']) {
			$where .= ' AND code_name LIKE "%'. $args['code_name'] .'%" ';
		}
		
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// fields
		$fields = 's_code, m_code, code_name, dsp_order, use_yn, reg_date';
		
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_sub_code .' '
			.'WHERE '. $where .' '
			.'ORDER BY dsp_order ASC, s_code ASC '
			.'LIMIT '. $offset .', '. $limit;
		// $rstRtn['query'] = $query;
		
		// query
		$rs = $this->db->query($query);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$rs = $this->db->query('SELECT COUNT(s_code) as cnt '
			.'FROM '. $this->tbl_sub_code .' '
			.'WHERE '. $where);
		$tmp = $rs->result();
		
		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_code : sub_code 다중 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_code($code) {
		$arr_code = explode(CFG_AUTH_CODE_DELIMITER, $code);
		$exist_code = '';
		foreach($arr_code as $id) {
			// counsel 테이블 수정이 필요해서 주석처리
			//$rst = $this->db->select('s_code')->get_where($this->tbl_counsel, array('s_code'=>$id));
			//if($rst->num_rows > 0) {
				//if($exist_code != '') $exist_code .= CFG_AUTH_CODE_DELIMITER;
				//$exist_code .= $id;
			//}
			//else {
				$query = 'DELETE FROM '. $this->tbl_sub_code .' WHERE s_code IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			//}
		}
		
		return $exist_code;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// use_or_not_use_code : sub_code 다중 사용/중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function use_or_not_use_code($args) {
		$s_code = '"'. str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['s_code']) . '"';
		
		$query = 'UPDATE '. $this->tbl_sub_code .' SET use_yn = '. $args['use_yn'] .' WHERE s_code IN ('. $s_code .') ';
		
		$rst = $this->db->query($query);
		
		return $rst;
	}
	
	
	
		
	
	//=================================================================================================================
	// Common Functions
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_lnb_menu_sub_code_by_m_code
	//-----------------------------------------------------------------------------------------------------------------
	// - param $args : LNB 메뉴의 대코드
	//-----------------------------------------------------------------------------------------------------------------
	public function get_lnb_menu_sub_code_by_m_code($args) {
//		echof($args);
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$args = ' "'. $args .'" ';
		$fields = 'S.s_code,S.m_code,S.code_name as scode_name,S.dsp_order,M.code_name as mcode_name';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' S '.
			' INNER JOIN '. $this->tbl_master_code .' M ON M.m_code = S.m_code '.
			' WHERE S.use_yn = 1 AND S.m_code IN ('. $args .') ORDER BY S.m_code ASC, S.dsp_order ASC';
		$rs = $this->db->query($query);

		$rstRtn = $rs->result();
//		echof($$query);
		return $rstRtn;
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// get_code_nm_by_code
	//-----------------------------------------------------------------------------------------------------------------
	// - param $args : 코드명을 가져올 코드
	//-----------------------------------------------------------------------------------------------------------------
	public function get_code_nm_by_code($args) {
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$fields = 'code_name';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' '
			.' WHERE use_yn = 1 AND ';
		$query .= 's_code IN ('. $code .') ';
		
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
		
		return $rstRtn;
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// get_sub_code_by_m_code
	//-----------------------------------------------------------------------------------------------------------------
	// - param $args : 가져올 서브코드의 대코드
	//-----------------------------------------------------------------------------------------------------------------
	public function get_sub_code_by_m_code($args, $is_all=FALSE) {
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$fields = 's_code,m_code,code_name,dsp_order';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' '.
			' WHERE ';
		if($is_all == FALSE || $is_all == 0 ) {
			$query .= 'use_yn = 1 AND ';
		}
		$query .= 'm_code IN ('. $code .') ';
		$query .= 'ORDER BY dsp_order ASC, code_name ASC';
		
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
		
		return $rstRtn;
	}

	//-----------------------------------------------------------------------------------------------------------------
	// get_oper_id_by_s_code
	//-----------------------------------------------------------------------------------------------------------------
	// - 상담자 데이터를 가져오기 위한 함수
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper_id_by_s_code($args, $is_all=FALSE) {
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$fields = 'oper_id,oper_name';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_oper .' '.
			' WHERE ';
		if($is_all == FALSE || $is_all == 0 ) {
			$query .= 'use_yn = 1 AND ';
		}
		$query .= 's_code IN ('. $code .') ';
		//$query .= 'ORDER BY dsp_order ASC, code_name ASC';
		
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
		
		return $rstRtn;
	}
	

	
	//-----------------------------------------------------------------------------------------------------------------
	// get_sub_code_by_m_code_v2 : 서브코드를 ","로 구분된 문자열로 리턴한다.
	//-----------------------------------------------------------------------------------------------------------------
	// - param $args : 가져올 서브코드의 대코드
	//-----------------------------------------------------------------------------------------------------------------
	public function get_sub_code_by_m_code_v2($args) {
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$query = 'SELECT CONCAT(GROUP_CONCAT(s_code)) as s_code FROM '. $this->tbl_sub_code .' '
			.' WHERE use_yn = 1 AND m_code IN ('. $code .') '
			.'GROUP BY m_code '
			.'ORDER BY dsp_order ASC, code_name ASC';
		
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
		
		return $rstRtn;
	}

	
	
	//=================================================================================================================
	// Helper Functions
	//=================================================================================================================

	//-----------------------------------------------------------------------------------------------------------------
	// get_counsel_year : 상담 데이터에서 상담 년도만 추출해서 리턴
	//-----------------------------------------------------------------------------------------------------------------
  public function get_counsel_year() {

  	$query = 'SELECT EXTRACT(YEAR FROM csl_date) as year '
  		.'FROM '. $this->tbl_counsel .' '
  		.'GROUP BY EXTRACT(YEAR FROM csl_date) '
  		.'ORDER BY EXTRACT(YEAR FROM csl_date) DESC ';
  	$rs = $this->db->query($query);
  	$rst = $rs->result();

  	return $rst;
  }

	
	//-----------------------------------------------------------------------------------------------------------------
	// get_lawhelp_year : 권리구제지원 테이블의 년도만 추출해서 리턴 - 지원승인일
	//-----------------------------------------------------------------------------------------------------------------
  public function get_lawhelp_year() {

  	$query = 'SELECT EXTRACT(YEAR FROM lh_sprt_cfm_date) as year '
  		.'FROM '. $this->tbl_lawhelp .' '
  		.'GROUP BY EXTRACT(YEAR FROM lh_sprt_cfm_date) '
  		.'ORDER BY EXTRACT(YEAR FROM lh_sprt_cfm_date) DESC ';
  	$rs = $this->db->query($query);
  	$rst = $rs->result();

  	return $rst;
  }

  /**
   * 권리구제 - 구제유형코드 세(B)코드 조회
   * 
   * @param  array $args 
   * @return array
   */
  public function get_lhkind_sub_cd($args) {

  	$query = "SELECT * 
  		FROM ". $this->tbl_sub_code ." 
			WHERE use_yn = 1 
				AND m_code='". $args['m_code'] ."' 
			ORDER BY dsp_order ASC, code_name ASC";
		
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();

  	return $rstRtn;
  }

	
	//-----------------------------------------------------------------------------------------------------------------
	// get_last_query
	//-----------------------------------------------------------------------------------------------------------------
	public function get_last_query() {
		// ENVIRONMENT 상수는 /index.php 페이지 내에 있다.
		if( defined('ENVIRONMENT') && ENVIRONMENT == 'development') {
			$last_query = $this->db->last_query();
			
			return $last_query;
		}
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	// change_pwd_new_encrypt : 단반향 암호화로 기존 운영자 비빌번호 일괄 변경
	//-----------------------------------------------------------------------------------------------------------------
	public function change_pwd_new_encrypt() {
		/*
		$query = 'SELECT oper_id,oper_pwd '
			.'FROM '. $this->tbl_oper;
		$rs = $this->db->query($query);
		$rst = $rs->result();

		foreach ($rst as $key => $value) {
			
			$where = array(
				'oper_id' => $value->oper_id
			);
			
			$dec_pwd = AesCtr::decrypt($value->oper_pwd, CFG_ENCRYPT_KEY, 256);
			$data = array(
				'oper_pwd' => dv_hash($dec_pwd, CFG_ENCRYPT_KEY)
			);
			$this->db->update($this->tbl_oper, $data, $where);
		}
		*/
	}


}