<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// db_admin Class
require_once 'db_admin.php';



/**
 * 사용자상담 DB 처리 전용 Class
 * 
 * @author dylan
 *
 */
class Db_Biz_Counsel extends Db_Admin {

	
	/**
	 * Construct
	 * 
	 */
	function __construct() {
		parent::__construct();
		
  }

  
  /**
   * 상담 목록 조회
   *
   *	[권한] <br>
   *	서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건) <br>
   * 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능 <br>
   * 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능<br>
   * 자치구 공무원 : 소속 자치구의 옴부즈만 상담만 조회,인쇄 가능 - 2016.08.09 추가<br>
   * 
   * @param array $args
   * @return array
   */
  public function get_counsel_list($args) {
  	
  	// offset, limit
  	$offset = $args['offset'] ? $args['offset'] : $this->offset;
  	$limit = $args['limit'] ? $args['limit'] : $this->limit;

  	// 소속코드 : asso_code - 자신이 속한 소속의 상담만 가져온다.
  	// 고도화 - 위 조건 변경됨
  	// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
  	// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
  	// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
  	$where = '1=1 ';
  
  	// 주요상담사례 게시판에서 호출한 경우 - 공유된 전체 상담만 가져옴(권한과 무관)
  	if($args['is_board'] == 1) {
  		$where .= 'AND C.csl_share_yn_cd = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
  	}
  	// master 제외 모든 상담자 대상
  	else if($args['is_master'] != 1) {
  
  		// 권한 부분 쿼리
  		$where .= $this->_get_counsel_comm_query();
  	}  

  	$key = $this->get_key();
  	
  	// 검색 시작 ------------------------------------------------------
		$search = '';
  	$keyword = $args['keyword'];
		$is_sch_oper = FALSE;

  	// 상담방법 : csl_way
  	if($args['csl_way']) {
  		$search .= 'AND C.s_code = "'. $args['csl_way'] .'" ';
  	}
  	
  	// 상담일 : search_date_begin, search_date_end
  	// - 양쪽 다 있는 경우
  	if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
  		$search .= 'AND C.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
  	}
  	// 시작년월일만 있는 경우 - 해당 일만 검색
  	else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
  		$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND C.csl_date>="'. $args['search_date_begin'] .'" ';
  	}
  	 
  	// <검색>
  	// 업종
  	if($args['target'] == 'counsel_comp_kind') {
  		if($args['target_comp_kind'] != '') {
  			$search .= 'AND C.comp_kind_cd = "'. $args['target_comp_kind'] .'" ';
  		}
  		if($keyword != '') {
				$search .= 'AND INSTR(C.comp_kind_etc,"'. $keyword .'")>0 ';
  		}
  	}
  	// 주제어
  	else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') { 
				$search .= 'AND C.seq IN (
					SELECT TCS.csl_seq 
					FROM '. $this->tbl_biz_counsel_sub .' TCS 
					INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND TCS.s_code="'. $args['target_keyword'] .'"
				) ';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$search .= 'AND C.seq IN (
					SELECT TCS.csl_seq 
					FROM '. $this->tbl_biz_counsel_sub .' TCS 
					INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD .'" 
					GROUP BY TCS.csl_seq
				) ';
			}
  	}
  	// 처리결과
  	else if($args['target'] == 'counsel_proc_rst') {
  		if($args['target_csl_proc_rst'] != '') {
  			$search .= 'AND C.csl_proc_rst_cd = "'. $args['target_csl_proc_rst'] .'" ';
  		}
  		if($keyword != '') {
				$search .= 'AND INSTR(C.csl_proc_rst_etc,"'. $keyword .'")>0 ';
  		}
  	}
		// 상담자 id - 2015.11.23 요청에 의한 추가 delee
		else if($args['target'] == 'counsel_oper_id') {
			if($keyword != '') {
				$is_sch_oper = TRUE;
			}
		}
		// 상담자명 
		else if($args['target'] == 'counsel_oper_name') {
			if($keyword != '') {
				$is_sch_oper = TRUE;
			}
		}
		// 사업장명 
		else if($args['target'] == 'csl_cmp_nm') {
			if($keyword != '') {
				$search .= 'AND C.csl_cmp_nm=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ';
			}
		}
		// 내담자명 
		else if($args['target'] == 'csl_name') {
			if($keyword != '') {
				$search .= 'AND C.csl_name=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ';
			}
		}
		// 내담자 전화번호 뒤 4자리 검색
		else if($args['target'] == 'counsel_csl_tel') {
			if($keyword != '') {
				$search .= 'AND C.csl_tel_sch=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ';
			}
		}
		// 전체검색
		else if($keyword != '') {
  		// 전체검색
  		if($args['target'] == '') {
  			$search .= ' AND (
					INSTR(C.csl_title,"'. $keyword .'")>0 '
					.'OR INSTR(C.csl_content,"'. $keyword .'")>0 '
  				.'OR INSTR(C.csl_reply,"'. $keyword .'")>0 '
  				.'OR INSTR(C.comp_kind_etc,"'. $keyword .'")>0 ' // 직종 기타
  				.'OR INSTR(C.csl_proc_rst_etc,"'. $keyword .'")>0 ' // 처리결과 기타
  				.'OR C.csl_cmp_nm=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ' //인코딩
  				.'OR C.csl_name=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ' //인코딩
  				.'OR C.csl_tel_sch=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ' //인코딩, 내담자 전화번호 뒤 4자리 - 추가 : dylan 2017.09.12, danvistory.com
  				.'OR C.oper_id IN (
						SELECT oper_id
						FROM '. $this->tbl_oper .'
						WHERE INSTR(oper_name,"'. $keyword .'") > 0 || INSTR(oper_id,"'. $keyword .'") > 0
					)
				) ';
  		}
  		else {
				if($keyword != '') {
					$search .= 'AND INSTR('. $args['target'] .',"'. $keyword .'")>0 ';
				}
  		}
  	}
  	// 검색 끝 ------------------------------------------------------

  	$rstRtn = ['data'=>[], 'tot_cnt'=>0];

  	// 검색상태 여부
  	$is_search = $search == '' && $is_sch_oper == FALSE ? FALSE : TRUE;
  	
  	// - 엑셀다운로드 외
  	$table = 'v_biz_csl_list';
  	$columns = 'V.seq,V.csl_title,V.csl_date,V.oper_name,V.asso_name,V.csl_way';
  	// - 엑셀다운로드(상담목록, 통계는 db_admin 클래스에서 처리한다.)
  	if(isset($args['is_excel'])) {
  		$table = 'v_biz_csl_list_search'; // - 검색, 엑셀다운로드에서 사용
  		$columns = 'V.seq,V.csl_title,V.csl_date,V.oper_name,V.asso_name,V.csl_way,V.csl_content,V.csl_reply';
  	}
  	
  	// - 검색상태시 검색용 뷰 사용
  	if($is_search === TRUE) {
  		// - 운영자 검색인 경우
  		if($is_sch_oper === TRUE) {
  			$q1 = 'SELECT '. $columns .'
					FROM (
						SELECT C.seq
						FROM (
							SELECT oper_id /* operator 검색을 우선 수행한다. */
							FROM
							'. $this->tbl_oper .'
							WHERE INSTR(oper_name,"'. $keyword .'")>0 OR INSTR(oper_id,"'. $keyword .'")>0
						) I
						INNER JOIN '. $this->tbl_biz_counsel .' C ON I.oper_id=C.oper_id '
  					.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
  					.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst_cd = S2.s_code ' // 처리결과
  					.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind_cd = S4.s_code ' // 업종
  					.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code '
  					.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id
					WHERE '. $where . $search .'
					ORDER BY C.csl_date DESC, C.seq DESC';
  			$q2 = ' LIMIT '. $offset .', '. $limit;
  			$q3 = ') T
					INNER JOIN v_biz_csl_list_search V ON T.seq=V.seq';
  			// 개수
  			$q = 'SELECT count(*) as cnt
					FROM (
						SELECT oper_id /* operator 검색을 우선 수행한다. */
						FROM
						'. $this->tbl_oper .'
						WHERE INSTR(oper_name,"'. $keyword .'")>0 OR INSTR(oper_id,"'. $keyword .'")>0
					) I
					INNER JOIN '. $this->tbl_biz_counsel .' C ON I.oper_id=C.oper_id '
  				.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
  				.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst_cd = S2.s_code ' // 처리결과
  				.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind_cd = S4.s_code ' // 업종
  				.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code '
  				.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id
					WHERE '. $where . $search .'
					ORDER BY C.csl_date DESC, C.seq DESC';
  			$rs = $this->db->query($q);
  			$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
  		}
  		// - 운영자외 전체 검색
  		else {
  			$q1 = 'SELECT '. $columns .'
					FROM (
						SELECT C.seq
						FROM '. $this->tbl_biz_counsel .' C '
  					.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
  					.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst_cd = S2.s_code ' // 처리결과
  					.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind_cd = S4.s_code ' // 업종
  					.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
  					.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code
					WHERE '. $where . $search .'
					ORDER BY C.csl_date DESC, C.seq DESC';
  			$q2 = ' LIMIT '. $offset .', '. $limit;
  			$q3 = ') T
					INNER JOIN v_biz_csl_list_search V ON T.seq=V.seq';
  			// 개수
  			$q = 'SELECT count(*) as cnt
					FROM '. $this->tbl_biz_counsel .' C '
  				.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
  				.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst_cd = S2.s_code ' // 처리결과
  				.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind_cd = S4.s_code ' // 업종
  				.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
  				.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code
					WHERE '. $where . $search.' ';
  			$rs = $this->db->query($q);
  			$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
  		}
  		$q = $q1 . $q2 . $q3;
  		if(isset($args['is_excel'])) {// 엑셀다운로드 - limit 제거
  			$q = $q1 . $q3;
  		}
//   		echof($q);
  		$rs = $this->db->query($q);
  		$rstRtn['data'] = $rs->result();
  	}
  	// - 검색상태 아니면 단순 뷰 사용
  	else {
  		$q1 = 'SELECT '. $columns .'
				FROM (
					SELECT C.seq
					FROM '. $this->tbl_biz_counsel .' C
					INNER JOIN '. $this->tbl_oper .' O ON C.oper_id=O.oper_id
					WHERE '. $where .'
					ORDER BY C.csl_date DESC, C.seq DESC';
  		$q2 = ' LIMIT '. $offset .', '. $limit;
  		$q3 = ') T
				INNER JOIN '. $table .' V ON T.seq=V.seq';
  			
  		$q = $q1 . $q2 . $q3;
  		if(isset($args['is_excel'])) {// 엑셀다운로드 - limit 제거
  			$q = $q1 . $q3;
  		}
//   		echof($q);
  		$rs = $this->db->query($q);
  		$rstRtn['data'] = $rs->result();
  		// 개수
  		$q = 'SELECT count(*) as cnt FROM '. $this->tbl_biz_counsel .' C
				INNER JOIN '. $this->tbl_oper .' O ON C.oper_id=O.oper_id /*권한관련으로 필요함*/
				WHERE '. $where;
  		$rs = $this->db->query($q);
  		$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
  	}
  	
  	/*
  	// fields
  	$fields2 = 'Q1.seq,Q1.csl_title,Q1.csl_content
  		,Q1.csl_reply,Q1.csl_proc_rst_cd,Q1.csl_proc_rst_etc,Q1.s_code,Q1.csl_date,Q1.oper_name,Q1.oper_id,Q1.oper_kind
  		,Q1.csl_cmp_nm,Q1.csl_name,Q1.csl_tel_sch
			,Q1.comp_kind_cd,Q1.comp_kind_etc
  		,Q1.comp_addr_cd,Q1.comp_addr_etc
  		,Q1.emp_cnt_cd,Q1.emp_cnt_etc
  		,Q1.oper_period_cd
  		,Q1.emp_paper_yn_cd
			,Q1.emp_insured_yn_cd
			,Q1.emp_rules_yn_cd
  		,Q1.csl_ref_seq
			,Q1.csl_method_nm, Q1.asso_code_nm';
  	$fields2 .= ',Q1.csl_way,Q1.asso_name'; // 엑셀다운용
		$fields1 = 'C.seq,C.csl_title,C.csl_content
			,C.csl_reply,C.csl_proc_rst_cd,C.csl_proc_rst_etc,C.s_code,C.csl_date,O.oper_name,O.oper_id,O.oper_kind
			,AES_DECRYPT(C.csl_cmp_nm, HEX(SHA2("'. $key .'",512))) as csl_cmp_nm
			,AES_DECRYPT(C.csl_name, HEX(SHA2("'. $key .'",512))) as csl_name
			,AES_DECRYPT(C.csl_tel_sch, HEX(SHA2("'. $key .'",512))) as csl_tel_sch
			,C.comp_kind_cd,C.comp_kind_etc
			,C.comp_addr_cd,C.comp_addr_etc
			,C.emp_cnt_cd,C.emp_cnt_etc
			,C.oper_period_cd
			,C.emp_paper_yn_cd
			,C.emp_insured_yn_cd
			,C.emp_rules_yn_cd
			,C.csl_ref_seq
			,S.code_name as csl_method_nm, S1.code_name as asso_code_nm';
  	$fields1 .= ',S.code_name as csl_way,S1.code_name as asso_name'; // 엑셀다운용
  	//
		$query1 = 'SELECT '. $fields2 .' ';
		$query2 = 'FROM (SELECT '. $fields1 .' '
  		.'FROM '. $this->tbl_biz_counsel .' C '
			.$inner_join_counsel_keyword
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
			.'INNER JOIN '. $this->tbl_sub_code .' S1 ON C.asso_code = S1.s_code ' //소속코드
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst_cd = S2.s_code ' // 처리결과
			.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind_cd = S4.s_code ' // 업종
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.$where .') Q1 ';
		
		$orderby = ' ORDER BY Q1.csl_date DESC, Q1.seq DESC ';

		// 엑셀 다운로드 일 경우 limit 제외
		if(! isset($args['is_excel'])) {
			$orderby .= 'LIMIT '. $offset .', '. $limit;
		}
  
		$q = $query1 . $query2 . $search . $orderby ;
		// $rstRtn['query'] = $q;
// 		echof($q);
  
		// query
		$rs = $this->db->query($q);
  
		$rstRtn['data'] = $rs->result();
  
		// total count
		$q =  $query1 . $query2 . $search;
		$rs = $this->db->query($q);
		$tmp = $rs->result();
		// $rstRtn['query2'] = $q;
  
		$rstRtn['tot_cnt'] = count($tmp);
  	*/
  	
		return $rstRtn;
  }
  
  
  /**
   * 상담 데이터 조회
   * 
   * @param array $args
   * @return array
   */
	public function get_biz_counsel($args) {
	
		$where = 'C.seq = '. $args['seq'] .' ';

		$key = $this->get_key();
		
		// 
		$fields = "C.seq, C.oper_id, C.csl_ref_seq, C.asso_code, C.csl_date
			,AES_DECRYPT(C.csl_name, HEX(SHA2('$key',512))) as csl_name
			,AES_DECRYPT(C.csl_tel, HEX(SHA2('$key',512))) as csl_tel
			,AES_DECRYPT(C.csl_cmp_nm, HEX(SHA2('$key',512))) as csl_cmp_nm
			,AES_DECRYPT(C.csl_tel_sch, HEX(SHA2('$key',512))) as csl_tel_sch
			,C.s_code,C.s_code_etc, C.comp_addr_cd, C.comp_addr_etc, C.emp_cnt_cd, C.emp_cnt_etc, C.comp_kind_cd, C.comp_kind_etc
			,C.oper_period_cd, C.emp_paper_yn_cd, C.emp_insured_yn_cd, C.emp_rules_yn_cd, C.csl_title, C.csl_content, C.csl_reply
			,C.csl_proc_rst_cd, C.csl_proc_rst_etc, C.csl_share_yn_cd, C.reg_date, C.mod_date, C.mod_oper_id
			,S.code_name as asso_name, O.oper_name,O.oper_kind,O.oper_auth_grp_id,O.s_code as asso_cd ";
		$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_biz_counsel .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.asso_code = S.s_code '
			.'WHERE '. $where;
	
		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);
		$rst = $rs->result();

		$rstRtn['data'] = $rst;
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 고용형태,상담유형,주제어 가져오기
			$query = 'SELECT CONCAT(GROUP_CONCAT(s_code)) as s_code FROM '. $this->tbl_biz_counsel_sub .' WHERE csl_seq='. $args['seq'] .' GROUP BY csl_seq';
			// query
			$rs2 = $this->db->query($query);
			$tmp2 = $rs2->result();
			$rstRtn['data']['csl_sub_code'] = '';
			if($this->db->affected_rows() > 0) {
				$rstRtn['data']['csl_sub_code'] = $tmp2[0]->s_code;
			}
		}

		return $rstRtn;
	}
	

	/**
	 * 상담 데이터 조회
	 * 
	 * 통계->상담사례에서 상담보기 팝업에서 사용
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_counsel_at($args) {
		$rs = $this->db->select('*')
			->from($this->tbl_biz_counsel)
			->join($this->tbl_oper, $this->tbl_oper .'.oper_id='. $this->tbl_biz_counsel .'.oper_id')
			->where($args)->get();
	
		$rst = $rs->result();
	
		$rstRtn = '';
		if(count($rst)>0) {
			$rstRtn = $rst[0];
		}
	
		return $rstRtn;
	}
	

	/**
	 * 저장
	 *
	 * @param array $args
	 * @return array
	 */
	public function add_biz_counsel($args) {
	
		$this->db->set('csl_ref_seq', $args['csl_ref_seq']);
		$this->db->set('reg_date', get_date());// 상담내역에서 아래와같이 등록일을 처리하고 있어 동일한 방식 유지함
		$this->db->set('oper_id', $args['oper_id']);
		self::_set_data($args);
		$rs = $this->db->insert($this->tbl_biz_counsel);
		$new_id = $this->db->insert_id();
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';
	
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;
	
			// 상담유형,주제어 저장
			$csl_sub_code = $args['csl_sub_code'];
			$csl_sub_code['csl_seq'] = $new_id;
			self::_insert_biz_csl_sub_code($csl_sub_code);
		}
	
		return $rstRtn;
	}
	

	/**
	 * 업데이트
	 *
	 * @param array $args
	 * @return array
	 */
	public function edit_biz_counsel($args) {
		
		$args['mod_date'] = get_date();
	
		$csl_sub_code = $args['csl_sub_code'];
		unset($args['csl_sub_code']);
	
		$where['seq'] = $args['seq'];

		$this->db->set('csl_ref_seq', $args['csl_ref_seq']);
		$this->db->set('mod_date', get_date());// 상담내역에서 아래와같이 등록일을 처리하고 있어 동일한 방식 유지함
		$this->db->set('mod_oper_id', $args['oper_id']);
		self::_set_data($args);
		$rs = $this->db->where($where)->update($this->tbl_biz_counsel);
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
	
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
	
			// 상담유형,주제어 저장 - 기존 코드 삭제후 다시 저장한다.
			// - 기존 코드 삭제
			$this->db->delete($this->tbl_biz_counsel_sub, array('csl_seq'=>$args['seq']));
			// - 새로 저장
			$csl_sub_code['csl_seq'] = $args['seq'];
			self::_insert_biz_csl_sub_code($csl_sub_code);
		}
	
		return $rstRtn;
	}


	/**
	 * 관련상담 업데이트
	 *
	 *  "관련상담" 처리 로직 <br>
	 *  원글과 원글 가져와 작성된 상담1, 2, ...n 모두 공유항목을 업데이트 처리한다. <br>
	 *  원글을 가져와 작성한 상담2,3,...n을 다시 가져와 작성한 경우도 마찬가지로 처리한다.
	 *  
	 * @param array $args
	 * @return array
	 */
	public function edit_biz_counsel_ref($args) {
	
		$args['mod_date'] = get_date();
	
		// 1.원글의 csl_ref_seq 컬럼 업데이트
		$where = array(
			'seq' => $args['csl_ref_seq']
		);
		$data = array(
			'csl_ref_seq' => $args['csl_ref_seq']
		);
		$this->db->where($where)->update($this->tbl_biz_counsel, $data);

		// 암호화필드 처리
		$csl_name = $args['csl_name'];
		unset($args['csl_name']);
		if($csl_name) {
			$this->db->set('csl_name', $csl_name, FALSE);
		}
		$csl_tel = $args['csl_tel'];
		unset($args['csl_tel']);
		if($csl_tel) {
			$this->db->set('csl_tel', $csl_tel, FALSE);
		}
		$csl_tel_sch = $args['csl_tel_sch'];
		unset($args['csl_tel_sch']);
		if($csl_tel_sch) {
			$this->db->set('csl_tel_sch', $csl_tel_sch, FALSE);
		}

		// 2.모든 csl_ref_seq 이 같은 상담 업데이트
		$data = array(
			'comp_addr_cd' => $args['comp_addr_cd']
			,'comp_addr_etc' => $args['comp_addr_etc']
			,'emp_cnt_cd' => $args['emp_cnt_cd']
			,'emp_cnt_etc' => $args['emp_cnt_etc']
			,'comp_kind_cd' => $args['comp_kind_cd']
			,'comp_kind_etc' => $args['comp_kind_etc']
			,'oper_period_cd' => $args['oper_period_cd']
			,'emp_paper_yn_cd' => $args['emp_paper_yn_cd']
			,'emp_insured_yn_cd' => $args['emp_insured_yn_cd']
			,'emp_rules_yn_cd' => $args['emp_rules_yn_cd']
		);
		$where = array(
			'csl_ref_seq' => $args['csl_ref_seq']
		);
		if($csl_name) {
			$this->db->set('csl_name', $csl_name, FALSE);
		}
		if($csl_tel) {
			$this->db->set('csl_tel', $csl_tel, FALSE);
		}
		if($csl_tel_sch) {
			$this->db->set('csl_tel_sch', $csl_tel_sch, FALSE);
		}
		$this->db->where($where)->update($this->tbl_biz_counsel, $data);
		
		return true;
	}
	

	/**
	 * 상담 조회 - 인쇄용
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_biz_counsel_print($args) {
	
		$where = 'C.seq = '. $args['seq'] .' ';
	
		$key = $this->get_key();
		
		$fields = 'AES_DECRYPT(C.csl_cmp_nm, HEX(SHA2("'. $key .'",512))) as csl_cmp_nm
			,AES_DECRYPT(C.csl_name, HEX(SHA2("'. $key .'",512))) as csl_name
			,AES_DECRYPT(C.csl_tel, HEX(SHA2("'. $key .'",512))) as csl_tel
			,C.csl_content, C.csl_reply, C.s_code_etc, C.csl_date, C.comp_addr_etc, C.emp_cnt_etc, C.comp_kind_etc, C.csl_proc_rst_etc
			, O.oper_name '
			.',S.code_name as asso_name '
			.',S1.code_name as csl_method '
			.',S6.code_name as comp_kind_cd_nm '
			.',S7.code_name as comp_addr_cd_nm '
			.',S9.code_name as emp_cnt_cd_nm '
			.',S10.code_name as emp_paper_yn_cd_nm '
			.',S11.code_name as emp_insured_yn_cd_nm '
			.',S12.code_name as csl_proc_rst_cd_nm '
			.',S13.code_name as csl_share_yn_cd '
			.',S14.code_name as oper_period_cd_nm '
			.',S15.code_name as emp_rules_yn_cd_nm '
			.',(SELECT GROUP_CONCAT(Z.code_name) '
			.'FROM '. $this->tbl_biz_counsel_sub .' Q '
			.'INNER JOIN '. $this->tbl_sub_code .' Z ON Z.s_code=Q.s_code '
			.'WHERE Q.csl_seq=C.seq) as csl_kind ';
			$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_biz_counsel .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.asso_code ' // 소속code
			.'LEFT JOIN '. $this->tbl_sub_code .' S1 ON S1.s_code = C.s_code ' // 상담방법
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.comp_kind_cd ' // 업종
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.comp_addr_cd ' // 회사주소
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.emp_cnt_cd ' // 근로자수
			.'LEFT JOIN '. $this->tbl_sub_code .' S10 ON S10.s_code = C.emp_paper_yn_cd ' // 근로계약서 작성 여부
			.'LEFT JOIN '. $this->tbl_sub_code .' S11 ON S11.s_code = C.emp_insured_yn_cd ' // 4대보험가입 여부
			.'LEFT JOIN '. $this->tbl_sub_code .' S12 ON S12.s_code = C.csl_proc_rst_cd ' // 상담 처리결과
			.'LEFT JOIN '. $this->tbl_sub_code .' S13 ON S13.s_code = C.csl_share_yn_cd ' // 상담내역공유
			.'LEFT JOIN '. $this->tbl_sub_code .' S14 ON S14.s_code = C.oper_period_cd ' // 운영기간
			.'LEFT JOIN '. $this->tbl_sub_code .' S15 ON S15.s_code = C.emp_rules_yn_cd ' // 취업규칙 작성여부
			.'LEFT JOIN '. $this->tbl_biz_counsel_sub .' Q ON Q.csl_seq = C.seq ' // 상담유형 서브테이블
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);
		$rst = $rs->result();

		$rstRtn['data'] = array();
		if(count($rst) > 0) {
			$rstRtn['data'] = (array)$rst[0];
		}

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}

		return $rstRtn;
	}

	
	/**
	 * 상담 테이블에 저장된 상담 년도만 추출해 리턴
	 * 
	 * @return array
	 */
	public function get_counsel_year() {
		
		return parent::get_counsel_year('biz_counsel');
	}
	
	
	/**
	 * 상담 insert, update 시 상담유형,주제어 테이블 처리 
	 *
	 * @param array $args
	 * @return array
	 */
	private function _insert_biz_csl_sub_code($args) {
	
		$data = array();
		
		// - 상담유형
		if(count($args['arr_csl_kind']) > 0) {
			// 추가된 counsel 테이블의 row id 추가
			foreach ($args['arr_csl_kind'] as $key => $item) {
				$decode = base64_decode($item);
				$data[$key]['csl_seq'] = $args['csl_seq'];
				$data[$key]['s_code'] = $decode;
				// 통계용 dsp_code를 가져와 저장한다.
				$data[$key]['dsp_code'] = $this->db->select('dsp_code')->get_where($this->tbl_sub_code, ['s_code'=>$decode])->result()[0]->dsp_code;
			}
			$this->db->insert_batch($this->tbl_biz_counsel_sub, $data);
			$data = array();
		}
		// - 주제어
		if(count($args['arr_keyword']) > 0) {
			// 추가된 counsel 테이블의 row id 추가
			foreach ($args['arr_keyword'] as $key => $item) {
				$decode = base64_decode($item);
				$data[$key]['csl_seq'] = $args['csl_seq'];
				$data[$key]['s_code'] = $decode;
				// 통계용 dsp_code를 가져와 저장한다.
				$data[$key]['dsp_code'] = $this->db->select('dsp_code')->get_where($this->tbl_sub_code, ['s_code'=>$decode])->result()[0]->dsp_code;
			}
			$this->db->insert_batch($this->tbl_biz_counsel_sub, $data);
		}
	}
	
	
	/**
	 * 공통 데이터 세팅
	 * 
	 * @param unknown $args
	 * @return void
	 */
	private function _set_data($args) {

		$escape = TRUE;
		if($args['csl_name']) $escape = FALSE;
		$this->db->set('csl_name', $args['csl_name'], $escape);
		$escape = TRUE;
		if($args['csl_tel']) $escape = FALSE;
		$this->db->set('csl_tel', $args['csl_tel'], $escape);
		$escape = TRUE;
		if($args['csl_tel_sch']) $escape = FALSE;
		$this->db->set('csl_tel_sch', $args['csl_tel_sch'], $escape);
		$escape = TRUE;
		if($args['csl_cmp_nm']) $escape = FALSE;
		$this->db->set('csl_cmp_nm', $args['csl_cmp_nm'], $escape);
		//
		$this->db->set('asso_code', $args['asso_code']);
		$this->db->set('csl_date', $args['csl_date']);
		$this->db->set('s_code', $args['s_code']);
		$this->db->set('s_code_etc', $args['s_code_etc']);
		$this->db->set('comp_addr_cd', $args['comp_addr_cd']);
		$this->db->set('comp_addr_etc', $args['comp_addr_etc']);
		$this->db->set('emp_cnt_cd', $args['emp_cnt_cd']);
		$this->db->set('emp_cnt_etc', $args['emp_cnt_etc']);
		$this->db->set('comp_kind_cd', $args['comp_kind_cd']);
		$this->db->set('comp_kind_etc', $args['comp_kind_etc']);
		$this->db->set('oper_period_cd', $args['oper_period_cd']);
		$this->db->set('emp_paper_yn_cd', $args['emp_paper_yn_cd']);
		$this->db->set('emp_insured_yn_cd', $args['emp_insured_yn_cd']);
		$this->db->set('emp_rules_yn_cd', $args['emp_rules_yn_cd']);
		$this->db->set('csl_title', $args['csl_title']);
		$this->db->set('csl_content', $args['csl_content']);
		$this->db->set('csl_reply', $args['csl_reply']);
		$this->db->set('csl_proc_rst_cd', $args['csl_proc_rst_cd']);
		$this->db->set('csl_proc_rst_etc', $args['csl_proc_rst_etc']);
		$this->db->set('csl_share_yn_cd', $args['csl_share_yn_cd']);

// 		echof($args['csl_tel_sch']);
// 		exit;
	}

	
	/**
	 * 다중 삭제
	 * 
	 * @param $args array <br>
	 * @return mixed 참조된 게시물 미삭제시 참조된 게시물 seq 리턴, 삭제시 ""
	 */
	public function del_multi_counsel($args) {
		
		$exist_seq = '';
	
		// 참조된 게시물이 있는지 검사하여 있으면 삭제하지 않도록 한다.
		if($args['check_ref'] === TRUE) {
			$seqs = $args['seqs'];
			$arr_seq = explode(CFG_AUTH_CODE_DELIMITER, $seqs);
				
			foreach($arr_seq as $id) {
				$rst = $this->db->select('seq')->get_where($this->tbl_biz_counsel, array('csl_ref_seq'=>$id));
	
				if($rst->num_rows > 0) {
					if($exist_seq != '') $exist_seq .= CFG_AUTH_CODE_DELIMITER;
					$exist_seq .= $id;
				}
				// 서브 테이블 삭제
				$query = 'DELETE FROM '. $this->tbl_biz_counsel_sub .' WHERE csl_seq IN ("'. $id .'") ';
				$rst = $this->db->query($query);

				// 게시물 삭제
				$query = 'DELETE FROM '. $this->tbl_biz_counsel .' WHERE seq IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			}
		}
		else {
			$seqs = str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['seqs']);
				
			// 서브 테이블 삭제
			$query = 'DELETE FROM '. $this->tbl_biz_counsel_sub .' WHERE csl_seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);
	
			// 게시물 삭제
			$query = 'DELETE FROM '. $this->tbl_biz_counsel .' WHERE seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);
		}
	
		return $exist_seq;
	}
	

	/**
	 * 상담 목록 조회 <관련상담>,<불러오기>
	 *
	 * 사용하는 view : list_popup_view, rel_list_popup_view
	 *
	 *	[권한] <br>
	 *	서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건) <br>
	 * 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능 <br>
	 * 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능<br>
	 *
	 * 공유하기 팝업 호출인 경우 공유하기 인 상담만을 소속과 무관하게 보여준다. 2017.01.18 dylan<br>
	 * <관련상담> 수정인 경우 팝업 목록,검색시 보고있는 상담은 대상에서 제외한다.<br>
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_counsel_list_pop($args) {
			
		// 암호키
		$key = $this->get_key();
			
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// 기본적으로 자신이 속한 기관의 상담만 가져온다.
		$where = 'WHERE 1=1 ';
	
		// master 제외
		if(isset($args['is_master']) && $args['is_master'] != 1) {
	
			// 권한 부분 쿼리 (상담목록 쿼리와 동일)
			$where .= self::_get_counsel_comm_query();
		}
	
		// <관련상담> 수정인 경우 팝업 목록,검색시 보고있는 상담은 대상에서 제외한다.
		if($args['seq'] != '') {
			$where .= ' AND C.seq<>'. $args['seq'] .' ';
		}
	
		// 관련상담 팝업인 경우 - 사업장명과 동일한 상담을 가져온다.
		if(isset($args['csl_cmp_nm']) && trim($args['csl_cmp_nm']) != '') {
			$where .= ' AND C.csl_cmp_nm=AES_ENCRYPT("'. $args['csl_cmp_nm'] .'", HEX(SHA2("'. $key .'",512))) ';
		}
	
		// <불러오기> 팝업인 경우
		// - 관련상담 조건 + 공유하기 인 상담
		if(isset($args['csl_share_yn'])) {
			$where .= ' OR C.csl_share_yn_cd = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
		}
	
		// 검색 시작 ------------------------------------------------------
		$search = 'WHERE 1=1 ';
		$inner_join_counsel_keyword = '';
		$keyword = $args['keyword'];
	
		// 상담방법 : csl_way
		if($args['csl_way']) {
			$search .= 'AND Q1.s_code = "'. $args['csl_way'] .'" ';
		}
	
		// 상담일 : search_date_begin, search_date_end
		// - 양쪽 다 있는 경우
		if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
			$search .= 'AND Q1.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
			$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND Q1.csl_date LIKE "'. $begin_year[0] .'%" ';
		}
	
		// <검색>
		// 업종
		if($args['target'] == 'counsel_comp_kind') {
			if($args['target_comp_kind'] != '') {
				$search .= 'AND Q1.comp_kind_cd = "'. $args['target_comp_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.comp_kind_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 주제어
		else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') {
				$inner_join_counsel_keyword = 'INNER JOIN '. $this->tbl_biz_counsel_sub .' CS ON C.seq=CS.csl_seq AND CS.s_code="'. $args['target_keyword'] .'" '
						.'INNER JOIN '. $this->tbl_sub_code .' CS1 ON CS.s_code = CS1.s_code AND CS1.m_code="'. CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD .'"';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$inner_join_counsel_keyword = 'INNER JOIN (SELECT TCS.csl_seq FROM '. $this->tbl_biz_counsel_sub .' TCS INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD .'" GROUP BY TCS.csl_seq) CS1 ON C.seq=CS1.csl_seq ';
			}
		}
		// 처리결과
		else if($args['target'] == 'counsel_proc_rst') {
			if($args['target_csl_proc_rst'] != '') {
				$search .= 'AND Q1.csl_proc_rst_cd = "'. $args['target_csl_proc_rst'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 검색어가 있는 경우만 해당
		if($keyword != '') {
			// 전체검색
			if($args['target'] == '') {
				$search .= ' AND ( Q1.csl_title LIKE "%'. $keyword .'%"
					OR Q1.csl_content LIKE "%'. $keyword .'%"
					OR Q1.csl_reply LIKE "%'. $keyword .'%"
					OR Q1.oper_id LIKE "%'. $keyword .'%"
					OR Q1.oper_name LIKE "%'. $keyword .'%" '
				.'OR Q1.comp_kind_etc LIKE "%'. $keyword .'%" ' // 업종 기타
				.'OR Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ' // 처리결과 기타
				.'OR Q1.csl_cmp_nm="'. $keyword .'" '//사업장명
				.'OR Q1.csl_name="'. $keyword .'" '//대표자명
				.'OR Q1.csl_tel_sch="'. $keyword .'" ' //전화번호 뒤 4자리
				.') ';
			}
			else {
				// 상담자 id
				if($args['target'] == 'counsel_oper_id') {
					$search .= 'AND Q1.oper_id LIKE "%'. $keyword .'%" ';
				}
				// 상담자명
				else if($args['target'] == 'counsel_oper_name') {
					$search .= 'AND Q1.oper_name LIKE "%'. $keyword .'%" ';
				}
				// 사업장명
				else if($args['target'] == 'counsel_cmp_nm') {
					$search .= ' AND Q1.csl_cmp_nm="'. $keyword .'" ';
				}
				// 대표자명
				else if($args['target'] == 'counsel_csl_name') {
					$search .= ' AND Q1.csl_name="'. $keyword .'" ';
				}
				// 내담자 전화번호 뒤 4자리 검색
				else if($args['target'] == 'counsel_csl_tel') {
					$search .= ' AND Q1.csl_tel_sch="'. $keyword .'" ';
				}
				// 검색대상: 제목,상담내용,답변
				else {
					$search .= 'AND '. $args['target'] .' LIKE "%'. $keyword .'%" ';
				}
			}
		}
		// 검색 끝 ------------------------------------------------------
	
		// fields
		$fields2 = 'Q1.seq,Q1.csl_title,Q1.csl_content
  		,Q1.csl_reply,Q1.csl_proc_rst_cd,Q1.csl_proc_rst_etc,Q1.s_code,Q1.csl_date,Q1.oper_name,Q1.oper_id,Q1.oper_kind
  		,Q1.csl_cmp_nm,Q1.csl_name,Q1.csl_tel_sch
			,Q1.comp_kind_cd,Q1.comp_kind_etc
  		,Q1.comp_addr_cd,Q1.comp_addr_etc
  		,Q1.emp_cnt_cd,Q1.emp_cnt_etc
  		,Q1.oper_period_cd
  		,Q1.emp_paper_yn_cd
			,Q1.emp_insured_yn_cd
			,Q1.emp_rules_yn_cd
  		,Q1.csl_ref_seq
			,Q1.csl_method_nm';
		$fields1 = 'C.seq,C.csl_title,C.csl_content
			,C.csl_reply,C.csl_proc_rst_cd,C.csl_proc_rst_etc,C.s_code,C.csl_date,O.oper_name,O.oper_id,O.oper_kind
			,AES_DECRYPT(C.csl_cmp_nm, HEX(SHA2("'. $key .'",512))) as csl_cmp_nm
			,AES_DECRYPT(C.csl_name, HEX(SHA2("'. $key .'",512))) as csl_name
			,AES_DECRYPT(C.csl_tel_sch, HEX(SHA2("'. $key .'",512))) as csl_tel_sch
			,C.comp_kind_cd,C.comp_kind_etc
			,C.comp_addr_cd,C.comp_addr_etc
			,C.emp_cnt_cd,C.emp_cnt_etc
			,C.oper_period_cd
			,C.emp_paper_yn_cd
			,C.emp_insured_yn_cd
			,C.emp_rules_yn_cd
			,C.csl_ref_seq
			,S.code_name as csl_method_nm';
		$query1 = 'SELECT '. $fields2 .' ';
		$query2 = 'FROM (SELECT '. $fields1 .' '
			.'FROM '. $this->tbl_biz_counsel .' C '
			.$inner_join_counsel_keyword
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst_cd = S2.s_code ' // 처리결과
			.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind_cd = S4.s_code ' // 업종
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.$where .') Q1 ';

		// 엑셀 다운로드 일 경우 limit 제외
		$orderby = ' ORDER BY Q1.csl_date DESC, Q1.seq DESC ';
		$orderby .= 'LIMIT '. $offset .', '. $limit;

		// query
		$q = $query1 . $query2 . $search . $orderby ;
		$rs = $this->db->query($q);
		$rstRtn['data'] = $rs->result();
		// 		echof($q);

		// total count
		$q =  $query1 . $query2 . $search;
		$rs = $this->db->query($q);
		$tmp = $rs->result();
// 		echof($tmp);
		$rstRtn['tot_cnt'] = count($tmp);

		return $rstRtn;
	}
}