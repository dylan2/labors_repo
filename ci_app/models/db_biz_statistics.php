<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// db_admin Class
require_once 'db_admin.php';


/**
 * 사용자 상담 통계 전용 Class
 * 
 * @author dylan
 * @create 2019.07.08 
 */
class Db_biz_statistics extends Db_admin {
	
	/**
	 * Construct
	 * 
	 */
	function __construct() {
		parent::__construct();
		
  }
	
	/**
	 * 통계 처리 메서드
	 * 
	 * @param array $args
	 */
	public function get_sttc_list($args) {

		$rstRtn = array();
		
		$where = '1=1 ';

		// csl_date 상담일
		$where1 = '';
		if(isset($args['begin']) && $args['begin'] != '' && isset($args['end']) && $args['end'] != '' ) {
			$where1 .= 'AND C.csl_date BETWEEN "'. $args['begin'] .'" AND "'. $args['end'] .'" ';
		}
		
		// asso_code 소속
		$where2 = '';
		if(isset($args['asso_code']) && $args['asso_code'] != '') {
			$where2 = 'AND C.asso_code IN ('. $args['asso_code'] .') ';
		}

		// s_code 상담방법
		$where3 = '';
		if(isset($args['s_code']) && $args['s_code'] != '') {
			$where3 = 'AND C.s_code IN ('. $args['s_code'] .') ';
		}

		// csl_proc_rst 처리결과
		$where5 = '';
		if(isset($args['csl_proc_rst']) && $args['csl_proc_rst'] != '') {
			$where5 = 'AND C.csl_proc_rst IN ('. $args['csl_proc_rst'] .') ';
		}

		// master 관리자 여부
		$is_master = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);

		// 각 쿼리별 합계 - 고용형태,상담유형 인 별도 코드 사용
		$total_counting = 'count(C.seq)';

		// 각 쿼리별 합계 쿼리
		$query_tot = '';

		//---------------------------------------------------------
		// # 구분 1 : 상담사례
		//---------------------------------------------------------
		if($args['kind'] == 'down01') {

			$key = $this->get_key();
			 
			$where .= $where1 . $where2 . $where3 . $where5;
			//
			$select = 'SELECT ';

			$select .= 'C.seq, C.csl_date
				,IFNULL(AES_DECRYPT(C.csl_name, HEX(SHA2("'. $key .'",512))), "") as csl_name
				,IFNULL(AES_DECRYPT(C.csl_cmp_nm, HEX(SHA2("'. $key .'",512))), "") as csl_cmp_nm
				,O.oper_name
				,S1.dsp_code as csl_method_nm 
				,S2.dsp_code as comp_addr_cd 
				,S3.dsp_code as emp_cnt_cd 
				,S4.dsp_code as comp_kind_cd 
				,S5.dsp_code as oper_period_cd 
				,S6.dsp_code as emp_paper_yn_cd 
				,S7.dsp_code as emp_insured_yn_cd 
				,S8.dsp_code as emp_rules_yn_cd 
				,S9.code_name as asso_code_nm 
				,S10.dsp_code as csl_proc_rst_cd
				,C.asso_code '; // 상담사례 목록에서 상담내용보기 버튼 권한 체크용

			// 상담유형 코드
			$code['csl_kind'] = self::get_sub_code_by_m_code_v2(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND);
			$csl_kind_code = '"'. str_replace(',', '","', $code['csl_kind'][0]->s_code) .'"';
			$select .= ',(
				SELECT GROUP_CONCAT(CS2.dsp_code ORDER BY CS2.dsp_code)
				FROM biz_counsel_sub CS2
				WHERE C.seq = CS2.csl_seq AND CS2.s_code IN ('. $csl_kind_code .')
			) as csl_kind ';// 상담유형 코드만 가져온다
			
			// excel 다운로드 용 컬럼 추가
			if($args['exclude_seq'] == 1) {
				$select .= ',C.s_code_etc, C.comp_addr_etc, C.emp_cnt_etc, C.comp_kind_etc, ';
				$select .= '(SELECT GROUP_CONCAT(K2.code_name) FROM '. $this->tbl_counsel_sub .' K1 INNER JOIN '. $this->tbl_sub_code .' K2 ON K1.s_code=K2.s_code WHERE K1.csl_seq=C.seq AND K2.m_code="'. CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD .'") as csl_keywords ';
				// 상담사례 상담보기 권한 여부
				// master OR 타 기관 상담이라도 상담보기 권한이 있으면 상담내용,결과를 노출한다.
				$auth_csl_view = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_BIZ_CSL_VIEW);
				if($is_master == 1 || $auth_csl_view == 1) {
					$select .= ',C.csl_content, C.csl_reply ';
				}
				else {
					$select .= ',(SELECT EC.csl_content FROM '. $this->tbl_biz_counsel .' EC WHERE C.seq=EC.seq AND C.asso_code="'. $args['oper_asso_cd'] .'") as csl_content, ';
					$select .= '(SELECT EC.csl_reply FROM '. $this->tbl_biz_counsel .' EC WHERE C.seq=EC.seq AND C.asso_code="'. $args['oper_asso_cd'] .'") as csl_reply ';
				}
			}

			$q1 = 'FROM '. $this->tbl_biz_counsel .' C '
				.'INNER JOIN operator O ON O.oper_id=C.oper_id '
				.'LEFT JOIN sub_code S1 ON S1.s_code=C.s_code '//상담방법
				.'LEFT JOIN sub_code S2 ON S2.s_code=C.comp_addr_cd '//회사 소재지 코드
				.'LEFT JOIN sub_code S3 ON S3.s_code=C.emp_cnt_cd '//근로자수 코드
				.'LEFT JOIN sub_code S4 ON S4.s_code=C.comp_kind_cd '//업종 코드
				.'LEFT JOIN sub_code S5 ON S5.s_code=C.oper_period_cd '//운영기간 코드
				.'LEFT JOIN sub_code S6 ON S6.s_code=C.emp_paper_yn_cd '//근로계약서작성여부 코드
				.'LEFT JOIN sub_code S7 ON S7.s_code=C.emp_insured_yn_cd '//4대보험가입여부 코드
				.'LEFT JOIN sub_code S8 ON S8.s_code=C.emp_rules_yn_cd ' //취업규칙작성여부 코드
				.'LEFT JOIN sub_code S9 ON S9.s_code = C.asso_code '//소속
				.'LEFT JOIN sub_code S10 ON S10.s_code = C.csl_proc_rst_cd '//처리결과
				.'WHERE '. $where .' ';
			
			$q2 = 'ORDER BY C.csl_date DESC, C.seq DESC, C.asso_code ASC ';

			// page
			$limit = CFG_CSL_STTT01_CMM_PAGINATION_ITEM_PER_REQ;
			$page = $args['page'] ? (int)$args['page'] : 1;
			$offset = ($page * $limit) - $limit;
			$q3 = 'LIMIT '. $offset .', '. $limit;
				
			$query = $select . $q1 . $q2 . $q3;
			
			// # 쿼리 실행
			$rst = $this->db->query($query);
			$rstRtn['data'] = $rst->result();

			// 개수
			$q = 'SELECT count(*) as cnt FROM '. $this->tbl_biz_counsel .' C
				INNER JOIN '. $this->tbl_oper .' O ON C.oper_id=O.oper_id
				WHERE '. $where;
			$rs = $this->db->query($q);
			$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
		}

		//---------------------------------------------------------
		// 구분 02 : 기본통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down02') {

			$target_field1 = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$data_suj = '';
			
			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = ''; //, S.subject2 ';
			
			if($args['sch_kind'] == 0) {// 0.상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 1.소속
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);
				$target_field1 = 'C.asso_code';
				if($args['asso_code'] == "'C131'"){
					$datas1 = $datas1 = self::get_oper_id_by_s_code('C131');
					$target_field1 = 'C.asso_code';
				}
			}
			elseif($args['sch_kind'] == 2) {// 2.소재지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				$target_field1 = 'C.comp_addr_cd';
			}
			elseif($args['sch_kind'] == 3) { // 3.근로자수
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
				$target_field1 = 'C.emp_cnt_cd';
			}
			elseif($args['sch_kind'] == 4) { // 4.업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
				$target_field1 = 'C.comp_kind_cd';
			}
			elseif($args['sch_kind'] == 5) { // 5.운영기간
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD);
				$target_field1 = 'C.oper_period_cd';
			}
			elseif($args['sch_kind'] == 6) {// 6.근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN);
				$target_field1 = 'C.emp_paper_yn_cd';
			}
			elseif($args['sch_kind'] == 7) {// 7.4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN);
				$target_field1 = 'C.emp_insured_yn_cd';
			}
			elseif($args['sch_kind'] == 8) {// 8.취업규칙
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN);
				$target_field1 = 'C.emp_rules_yn_cd';
			}
			elseif($args['sch_kind'] == 9) { // 9.상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND);
				$target_field1 = 'CS2.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD2.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sch_kind'] == 10) { // 10.처리결과
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);
				$target_field1 = 'C.csl_proc_rst_cd';
			}
			
			// where - 상담일, 상담방법, 처리결과 검색
			$where .= $where1 . $where3 . $where5;
			
			foreach($datas1 as $data) {
				if($args['sch_kind'] == 1) {	//소속 검색 시, 선택한 소속만 데이터를 가져오도록 처리
					if($args['asso_code'] == "'C131'"){// 서울시인 경우
// 						echof($data->oper_name, '$data->oper_name');
						
// 						if(strpos($data->oper_name, '옴부즈만') !== FALSE){//옴부즈만인 경우 -- 이부분 주석처리한 이유는 작업자가 else에 대한 처리를 안해서 서울시소속 모두 출력되게 조치함 
							if($t1 != '') $t1 .= ', ';
							$t1 .= '"'.$data->oper_name. '" AS s'.$index;
							$t1 .= ', sum(M.ck'.$index.') AS t'.$index;
			
							if($t_tot != '') $t_tot .= ' + ';
							$t_tot .= 'sum(M.ck'.$index.')';
			
							if($t2 != '') $t2 .= ', ';
							$t2 .= ' IF(S.target = "'.$data->oper_id.'", S.ck1, 0) as ck'.$index;
			
							$index++;
// 						}
					}
					else{
						if(strpos($args['asso_code'], $data->s_code) !== FALSE){
							if($t1 != '') $t1 .= ', ';
							$t1 .= '"'.$data->code_name. '" AS s'.$index;
							$t1 .= ', sum(M.ck'.$index.') AS t'.$index;
			
							if($t_tot != '') $t_tot .= ' + ';
							$t_tot .= 'sum(M.ck'.$index.')';
			
							if($t2 != '') $t2 .= ', ';
							$t2 .= ' IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;
			
							$index++;
						}
					}
				}
				else{
					if($t1 != '') $t1 .= ', ';
					$t1 .= '"'.$data->code_name. '" AS s'.$index;
					$t1 .= ', sum(M.ck'.$index.') AS t'.$index;
			
					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'sum(M.ck'.$index.')';
			
					if($t2 != '') $t2 .= ', ';
					$t2 .= ' IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;
			
					$index++;
				}
			}
			
			$t1 .= ', "총계" AS s'.$index;
			// - 상담유형인 경우 
			if($args['sch_kind'] == 9){
				$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ")")';
			}
			if($args['sch_kind'] == 1 && $args['asso_code'] == "'C131'"){
				$t = 'SELECT '.$t1 .', '. $t_tot .' as tot FROM ( SELECT '.$t2. ' FROM ( select A.ck1, A.target '
					.' FROM (SELECT code_name, s_code FROM sub_code ';
					if(isset($args['asso_code']) && $args['asso_code'] != '') {
						$t .= 'WHERE s_code in (' .$args['asso_code']. '))';
					}
					$t .= ' DD LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id as target FROM biz_counsel C '
						.'WHERE ' .$where.' '
						.'GROUP BY C.asso_code, C.oper_id ) A ON A.asso_code = DD.s_code '
						.') S ) M ';
					//echof($t);
			}
			else{
				$t = 'SELECT '.$t1 .', '. $t_tot .' as tot FROM ( SELECT '.$t2. $csl_kind2 .' FROM ( select A.ck1, A.target ';
				// - 상담유형인 경우
				if($args['sch_kind'] == 9){
					$t .= ', (SELECT COUNT(*) as distinct_tot FROM biz_counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
						.'WHERE '.$where. $where2.') AS distinct_tot ';
				}
				$t .=' FROM (SELECT code_name, s_code FROM sub_code ';
				if(isset($args['asso_code']) && $args['asso_code'] != '') {
					$t .= 'WHERE s_code in (' .$args['asso_code']. '))';
				}
				$t .= ' DD LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, '.$target_field1.' as target FROM biz_counsel C ';
				// - 상담유형인 경우
				if($args['sch_kind'] == 9){
					$t .= 'INNER JOIN '. $this->tbl_biz_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}
			
				$t .= 'WHERE ' .$where.' '
					.'GROUP BY C.asso_code, C.oper_id, '.$target_field1.' ) A ON A.asso_code = DD.s_code '
					.') S ) M ';
			}
			//총계 쿼리
			$query_tot = $t;
// 			echof($t, '############::');
			$total = 1;
			
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
				$total = $rstRtn['data_tot'][0]->tot;
			
				$total = explode('(', $total);
				$total = $total[0];
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data_tot'][0]);
		}

		//------------------------------------------------------------------------------------------------------------------
		// 구분 03 : 교차통계
		//------------------------------------------------------------------------------------------------------------------
		else if($args['kind'] == 'down03') {

			$target_field1 = '';
			$csl_kind = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$where_asso = '';

			//교차 가로축 검색
			if($args['sel_col'] == 0){
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY); //상담방법
				$target_field1 = 'C.s_code';
			}
			elseif($args['sel_col'] == 1) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT); // 근로자수
				$target_field1 = 'C.emp_cnt_cd';
			}
			elseif($args['sel_col'] == 2) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND); //업종
				$target_field1 = 'C.comp_kind_cd';
			}
			elseif($args['sel_col'] == 3) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD); //운영기간
				$target_field1 = 'C.oper_period_cd';
			}
			elseif($args['sel_col'] == 4) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN); //근로계약서
				$target_field1 = 'C.emp_paper_yn_cd';
			}
			elseif($args['sel_col'] == 5) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN); //4대보험
				$target_field1 = 'C.emp_insured_yn_cd';
			}
			elseif($args['sel_col'] == 6) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN); //취업규칙
				$target_field1 = 'C.emp_rules_yn_cd';
			}
			elseif($args['sel_col'] == 7) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND); //상담유형
				$target_field1 = 'CS2.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sel_col'] == 8) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS); // 소재지
				$target_field1 = 'C.comp_addr_cd';
			}

			//교차 세로축 검색
			if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION) {// 소속
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
				$target_field2 = 'C.asso_code';
				if(isset($args['asso_code']) && $args['asso_code'] != '') {
					$where_asso = 'AND (SC.s_code in (' .$args['asso_code']. '))';
				}
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) {// 근로자수
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT;
				$target_field2 = 'C.emp_cnt_cd'; 
				$target_field3 = 'DT.emp_cnt_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
				$target_field2 = 'C.comp_kind_cd'; 
				$target_field3 = 'DT.comp_kind_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD) { // 운영기간
				$datas2 = CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD;
				$target_field2 = 'C.oper_period_cd'; 
				$target_field3 = 'DT.oper_period_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN) { // 근로계약서
				$datas2 = CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN;
				$target_field2 = 'C.emp_paper_yn_cd'; 
				$target_field3 = 'DT.emp_paper_yn_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN) {// 4대보험
				$datas2 = CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN;
				$target_field2 = 'C.emp_insured_yn_cd'; 
				$target_field3 = 'DT.emp_insured_yn_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN) { // 취업규칙
				$datas2 = CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN;
				$target_field2 = 'C.emp_rules_yn_cd'; 
				$target_field3 = 'DT.emp_rules_yn_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND) {// 상담유형
				$datas2 = CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND;
				$target_field2 = 'CS2.s_code';
				$target_field3 = 'DT.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
			}
			elseif($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS) { // 소재지
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field2 = 'C.comp_addr_cd'; 
				$target_field3 = 'DT.comp_addr_cd'; 
			}

			// 상담일자 외 - 소속/성별/직종/업종/거주지+회사소재지/고용형태/근로자수/근로계약서/4대보험/연령대
			if($args['csl_code'] != 'csl_date') {

				// where - 상담일, 상담방법, 처리결과 검색
				$where .= $where1 . $where2 . $where3 . $where5;

				// 총계쿼리
				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'SELECT S.subject ';
				$t3 = '';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';

					if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
						$t1 .= 'CONCAT(IFNULL(SUM(M.ck'.$index.'), 0), "(", IFNULL(M.distinct_ck'.$index.', 0), ")")';
					}
					else{
						$t1 .= 'SUM(M.ck'.$index.')';
					}
					if($t_tot != '') $t_tot .= ' + ';

					$t_tot .= 'SUM(M.ck'.$index.')';
			
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;
					if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
						$t2 .= ', S.distinct_ck'.$index;
					}
					if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
						if($t3 != '') $t3 .= ', ';
						$t3 .= '(SELECT count(*) as distinct_ck'.$index .' FROM '. $this->tbl_biz_counsel .' C 
							WHERE '.$where .' AND '.$target_field1.' = "'.$data->s_code.'" ) AS distinct_ck'.$index.' ';
					}
					$index++;
				}
				
				if($args['sel_col'] == 7 || $args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
					$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ")")';
				}
				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 . $csl_kind2 .' FROM ( SELECT DD.code_name AS subject, A.ck1, A.target ';
				if($args['sel_col'] == 7 || $args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
					$t .= ', (SELECT count(*) as distinct_tot FROM '. $this->tbl_biz_counsel .' C WHERE '.$where.' ) AS distinct_tot ';
				}
				if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
					$t .= ', '.$t3;
				}
				$t .= 'FROM (SELECT code_name, s_code, dsp_code FROM sub_code ';
				$t .=' WHERE m_code = "'.$datas2.'" ) DD ';
				$t .='LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_field2.' as target2, '.$target_field1.' as target FROM '. $this->tbl_biz_counsel .' C ';

				if($args['sel_col'] == 7 || $args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
					$t .= 'INNER JOIN '. $this->tbl_biz_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', '.$target_field2.' ) A ON A.target2 = DD.s_code '
					.') S ) M ';

				$query_tot = $t;
// 				echof($t);
				
				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
					
						$total = explode('(', $total);
						$total = $total[0];
					
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}
				if($args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){
					$csl_kind2 = '';
					$csl_kind3 = '';
				}
				
				// 메인쿼리
				$q = '';
				$index = 1;
				$q1 = 'SELECT M.subject ';
				$q_tot = '';
				$q2 = 'SELECT S.subject ';
				foreach($datas1 as $data) {
					$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
					$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") as ck'.$index;

					if($q_tot != '') $q_tot .= ' + ';

					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

					$index++;
				}
				
				if($total != 0){
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					if($args['sel_col'] == 7){//상담유형
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ") (", IFNULL('.$per.', 0),"%)")';
					}
					else{
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
					}
				}

				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.', S.dsp_code '.$csl_kind2.' from ( SELECT DD.code_name AS subject, A.ck1, A.target, DD.dsp_code '.$csl_kind3;				
				$q .= ' FROM (SELECT SC.code_name, SC.s_code, SC.dsp_code '.$csl_kind4.' FROM sub_code SC ';				
				if($args['sel_col'] == 7){
					$q .= 'LEFT JOIN (SELECT COUNT(*) as distinct_tot, '.$target_field2.' FROM '. $this->tbl_biz_counsel .' C
						WHERE '.$where.' GROUP BY '.$target_field2.') DT ON SC.s_code = '.$target_field3.' ';
				}
				$q .= 'WHERE m_code = "'.$datas2.'" '.$where_asso.') DD ';
				$q .= 'LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_field2.' as target2, '.$target_field1.' as target FROM '. $this->tbl_biz_counsel .' C ';
				
				if($args['sel_col'] == 7 || $args['csl_code'] == CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND){ //상담유형
					$q .= 'INNER JOIN '. $this->tbl_biz_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', '.$target_field2.' ) A ON A.target2 = DD.s_code '
					.') S ) M group by M.subject ORDER BY M.dsp_code ASC ';
				
				$query = $q;
// 			  echof($query);

				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}

				$rstRtn['tot_cnt'] = count($rstRtn['data']);
				
			}
			// 상담일자
			else {

				// 검색일자 - 전체인 경우 빈값이다.
				if(isset($args['csl_date']) && $args['csl_date'] != '') {
					$sch_date = $args['csl_date'];
				}
				else {
					$sch_date = 'C.csl_date BETWEEN "1900-01-01 00:00:00" AND "'. get_date() .'" ';
				}

				// where - 상담일, 상담방법, 처리결과 검색
				$where .= $where1 . $where2 . $where3 . $where5;

				// 총계쿼리
				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'select S.subject ';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';
					$t1 .= 'SUM(M.ck'.$index.')';
					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'SUM(M.ck'.$index.')';
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;

					$index++;
				}

				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 .' FROM ( SELECT DD.csl_date AS subject, A.ck1, A.target '
					.'FROM (SELECT DISTINCT C.csl_date FROM biz_counsel C '
					. 'WHERE 1=1 '.$where1.') DD '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.csl_date AS target2, '.$target_field1.' AS target FROM biz_counsel C '
					.'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', C.csl_date) A ON A.target2 = DD.csl_date '
					.') S ) M ';

				$query_tot = $t;
				// echof($t);
				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
					
					$total = explode('(', $total);
					$total = $total[0];
					
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}
				
				// 메인쿼리
				$q = '';
				$index = 1;
				$q1 = 'select M.subject ';
				$q_tot = '';
				$q2 = 'select S.subject ';
				foreach($datas1 as $data) {
					if(isset($total) && $total != 0){	
						$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
						$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") AS ck'.$index;
					}
					else{
						$q1 .= ', IFNULL(sum(M.ck'.$index.'), 0) AS ck'.$index;
					}
					if($q_tot != '') $q_tot .= ' + ';
					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;

					$index++;
				}
				
				if(isset($total) && $total != 0){	
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
				}
				else{
					$q_tot = 'IFNULL('.$q_tot.', 0)';
				}

				$q = $q1 .', '. $q_tot .' AS tot FROM ('.$q2.' FROM ( SELECT DD.csl_date AS subject, A.ck1, A.target '
					.'FROM (SELECT DISTINCT C.csl_date FROM biz_counsel C '
					. 'WHERE 1=1 '.$where1.') DD '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.csl_date AS target2, '.$target_field1.' AS target FROM biz_counsel C '
					. 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', C.csl_date ) A ON A.target2 = DD.csl_date '
					.') S ) M group by M.subject ORDER BY M.subject DESC ';
				
				$query = $q;

				// 상담일자인 경우, 데이터가 없으면 $q가 공백이라 오류가 발생하는 문제 처리
				if(empty($q)) {
					$query = 'SELECT seq FROM biz_counsel WHERE 1=0';
					$query_tot = $query;
				}
				else {
					$query = $q;
					$query_tot = $t;
				}
				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}
			}
			
			$rstRtn['tot_cnt'] = count($rstRtn['data']);
		}

		
		//------------------------------------------------------------------------------------------------------------------
		// 구분 04 : 소속별통계
		//------------------------------------------------------------------------------------------------------------------
		else if($args['kind'] == 'down04') {

			$target_field1 = '';
			$target_field3 = '';
			$csl_kind = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$where_asso = '';

			//교차 가로축 검색
			if($args['sch_kind'] == 0) {// 0.상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 소재지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				$target_field1 = 'C.comp_addr_cd';
			}
			elseif($args['sch_kind'] == 2) { // 근로자수
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
				$target_field1 = 'C.emp_cnt_cd';
			}
			elseif($args['sch_kind'] == 3) { // 업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
				$target_field1 = 'C.comp_kind_cd';
			}
			elseif($args['sch_kind'] == 4) { // 운영기간
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD);
				$target_field1 = 'C.oper_period_cd';
			}
			elseif($args['sch_kind'] == 5) {// 근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN);
				$target_field1 = 'C.emp_paper_yn_cd';
			}
			elseif($args['sch_kind'] == 6) {// 4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN);
				$target_field1 = 'C.emp_insured_yn_cd';
			}
			elseif($args['sch_kind'] == 7) {// 취업규칙
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN);
				$target_field1 = 'C.emp_rules_yn_cd';
			}
			elseif($args['sch_kind'] == 8) { // 상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND);
				$target_field1 = 'CS2.s_code';
				$target_field3 = 'DT.asso_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sch_kind'] == 9) { // 처리결과
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);
				$target_field1 = 'C.csl_proc_rst_cd';
			}
						
			//교차 세로축 - 소속
			$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
			$target_asso_cd = 'C.asso_code';
			if(isset($args['asso_code']) && $args['asso_code'] != '') {
				$where_asso = 'AND SC.s_code in (' .$args['asso_code']. ')';
			}
			
			// where - 상담일, 상담방법, 처리결과 검색
			$where .= $where1 . $where2 . $where3 . $where5;
		
			// 총계쿼리
			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = 'SELECT S.subject ';
			$t3 = '';
			foreach($datas1 as $data) {
				if($t1 != '') $t1 .= ', ';
				// 상담유형별 중복제거 건수는 불필요하여 해당 쿼리부분 주석처리 (소속별만 필요함)
// 				if($args['sch_kind'] == 8){
// 					$t1 .= 'CONCAT(IFNULL(SUM(M.ck'.$index.'), 0), "(", IFNULL(M.distinct_ck'.$index.', 0), ")")';
// 				}
// 				else{
					$t1 .= 'SUM(M.ck'.$index.')';
// 				}
				
				if($t_tot != '') $t_tot .= ' + ';
				$t_tot .= 'SUM(M.ck'.$index.')';
					
				$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;
// 				if($args['sch_kind'] == 8){
// 					$t2 .= ', S.distinct_ck'.$index;
// 					if($t3 != '') $t3 .= ', ';
// 					$t3 .= '(SELECT count(DISTINCT '. $target_field1 .') as distinct_ck'.$index .' FROM '. $this->tbl_biz_counsel .' C
// 						INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
// 						WHERE '.$where .' AND '.$target_field1.' = "'.$data->s_code.'" ) AS distinct_ck'.$index.' ';
// 				}
				$index++;
			}
			
			if($args['sch_kind'] == 8){
				$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), " (", IFNULL(M.distinct_tot, 0), ")")';
			}
			$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 . $csl_kind2 .' FROM ( SELECT DD.code_name AS subject, A.ck1, A.target ';
			if($args['sch_kind'] == 8){
				$t .= ', (SELECT count(DISTINCT C.seq) as distinct_tot FROM '. $this->tbl_biz_counsel .' C WHERE '.$where.' ) AS distinct_tot ';
			}
// 			if($args['sch_kind'] == 8){
// 				$t .= ', '.$t3;
// 			}
			$t .= 'FROM (SELECT code_name, s_code, dsp_code FROM sub_code ';
			$t .=' WHERE m_code = "'.$datas2.'" ) DD ';
			$t .='LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_asso_cd.' as target2, '.$target_field1.' as target FROM '. $this->tbl_biz_counsel .' C ';
			
			if($args['sch_kind'] == 8){
				$t .= 'INNER JOIN '. $this->tbl_biz_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
			}

			$where_ck = '';
			if($args['sch_kind'] == 8){ //상담유형이면 where 절 추가
				$csl_kind = self::get_sub_code_by_m_code_v2(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND);
				$csl_kind_code = '"'. str_replace(',', '","', $csl_kind[0]->s_code) .'"';
				$where_ck .= 'AND CS2.s_code IN ('. $csl_kind_code .') ';
			}
				
			$t .= 'WHERE '. $where .' '. $where_ck
				.'GROUP BY '.$target_field1.', '.$target_asso_cd .' ) A ON A.target2 = DD.s_code '
				.') S 
				) M ';
			
			$query_tot = $t;
// 			echof($t);

			$total = 1;
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
				$total = $rstRtn['data_tot'][0]->tot;
					
				$total = explode('(', $total);
				$total = $total[0];
					
				if($total == 0){
					$rstRtn['data_tot'][0]->tot .= ' (0%)';
				}
				else{
					$rstRtn['data_tot'][0]->tot .= ' (100%)';
				}
			}
			if($args['sch_kind'] != 8){
				$csl_kind2 = '';
				$csl_kind3 = '';
			}
			
			// 메인쿼리
			$q = '';
			$index = 1;
			$q1 = 'SELECT M.subject ';
			$q_tot = '';
			$q2 = 'SELECT S.subject ';
			foreach($datas1 as $data) {
				$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
				$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") as ck'.$index;

				if($q_tot != '') $q_tot .= ' + ';

				$q_tot .= 'sum(M.ck'.$index.')';
				$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

				$index++;
			}

			if($total != 0){
				$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
				if($args['sch_kind'] == 8){ //상담유형
					$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL(M.distinct_tot, 0), ") (", IFNULL('.$per.', 0),"%)")';
				}
				else {
					$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
				}
			}

			$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.', S.dsp_code '.$csl_kind2.' from ( SELECT DD.code_name AS subject, A.ck1, A.target, DD.dsp_code '. $csl_kind3;
			$q .= ' FROM (SELECT SC.code_name, SC.s_code, SC.dsp_code '.$csl_kind4.' FROM sub_code SC ';
			if($args['sch_kind'] == 8){
				$q .= 'LEFT JOIN (SELECT COUNT(*) as distinct_tot, '.$target_asso_cd.' FROM '. $this->tbl_biz_counsel .' C
						WHERE '.$where.' GROUP BY '.$target_asso_cd.') DT ON SC.s_code = '.$target_field3.' ';
			}
			$q .= 'WHERE m_code = "'.$datas2.'" '.$where_asso.') DD ';
			$q .= 'LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_asso_cd.' as target2, '.$target_field1.' as target FROM '. $this->tbl_biz_counsel .' C ';
			
			if($args['sch_kind'] == 8){ //상담유형
				$q .= 'INNER JOIN '. $this->tbl_biz_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
			}				
						
			$q .= 'WHERE ' . $where .' '. $where_ck
				.'GROUP BY '.$target_field1.', '.$target_asso_cd.') A ON A.target2 = DD.s_code '
				.') S 
				) M 
				GROUP BY M.subject ORDER BY M.dsp_code ASC ';
			
			$query = $q;
// 			echof($q);

			// # 쿼리 실행
			if(!empty($query)) {
				$rst = $this->db->query($query);
				$rstRtn['data'] = $rst->result();
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data']);			
		}

		//---------------------------------------------------------
		// 구분 05 : 월별 통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down05') {

			$current_year = $args['sel_year'];
			$half_year = $args['half_year'];
			if($half_year == 1){
				$first = 1;
				$last = 12;
			}
			elseif($half_year == 2){
				$first = 1;
				$last = 6;
			}
			elseif($half_year == 3){
				$first = 7;
				$last = 12;
			}

			// where - 상담방법, 처리결과 검색
			$where .= $where3 . $where5;

			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = 'select S.subject '; // S.subject2
			for($month=$first; $month<=$last; $month++) {
				if($month < 10) $month = '0'.$month;

				if($t1 != '') $t1 .= ', ';
				$t1 .= 'sum(M.ck'.$index.')';

				if($t_tot != '') $t_tot .= ' + ';

				$t_tot .= 'sum(M.ck'.$index.')';
				$t2 .= ', IF(S.target LIKE "%'.$current_year.'-'.$month.'%", S.ck1, 0) as ck'.$index;

				$index++;
			}
			// 소속이 서울시인 경우
			if($args['asso_code'] == "'C131'"){
				$t = 'select '.$t1 .', '. $t_tot .' as tot FROM ('.$t2.' from ( select DD2.oper_name AS subject, A.ck1, A.target '
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id ="GRP013") DD2 ON DD.s_code = DD2.s_code '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM biz_counsel C ';

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					.') S ) M ';
			}
			else{		
				$t = 'select '.$t1 .', '. $t_tot .' as tot FROM ('.$t2.' from ( select DD.code_name AS subject, A.ck1, A.target '
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM biz_counsel C ';

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code '
					.') S ) M ';
			}

			//총계 쿼리
			$query_tot = $t;
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
			}

			$q = '';
			$index = 1;
			$q1 = 'select M.subject ';
			$q_tot = '';
			$q2 = 'select S.subject, S.dsp_order ';
			for($month=$first; $month<=$last; $month++) {
				if($month < 10) $month = '0'.$month;

				$q1 .= ', sum(M.ck'.$index.') as ck'.$index;

				if($q_tot != '') $q_tot .= ' + ';

				$q_tot .= 'sum(M.ck'.$index.')';
				$q2 .= ', IF(S.target LIKE "%'.$current_year.'-'.$month.'%", S.ck1, 0) as ck'.$index;

				$index++;
			}
			// 소속이 서울시인 경우
			if($args['asso_code'] == "'C131'"){
				// GRP013: 서울시시민명예노동옴부즈만.. 전 작업자가 하드코딩을 해 놓은 현장...;;
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.' from ( select DD2.oper_name AS subject, A.ck1, A.target, DD.dsp_order '
					.' FROM (SELECT code_name, s_code, dsp_order FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id = "GRP013") DD2 ON DD.s_code = DD2.s_code '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM biz_counsel C ';
					$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					.') S ) M 
					group by M.subject
					ORDER BY M.dsp_order ';
			}
			else{
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.' from ( select DD.code_name AS subject, A.ck1, A.target, DD.dsp_order '
					.' FROM (SELECT code_name, s_code, dsp_order FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM biz_counsel C ';
				$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code '
					.') S ) M 
					group by M.subject
					ORDER BY M.dsp_order ';
			}
			$query = $q;
// 			echof($query);

			// # 쿼리 실행
			if(!empty($query)) {
				$rst = $this->db->query($query);
				$rstRtn['data'] = $rst->result();
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data']);
		}

		//---------------------------------------------------------
		// 구분 06 : 시계열통계 - 건수/비율 (임금/근로시간 해당없음)
		//---------------------------------------------------------
		else if($args['kind'] == 'down06') {

			// where - 소속, 상담방법, 처리결과 검색
			$where .= $where2 . $where3 . $where5;
			
			//기준 세로축 s_code
			$datas1 = array();
			//기존 세로축 m_code
			$tg_m_code = '';
				
			//기준 세로축 데이터
			if($args['sch_kind'] == 0) {// 상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY;
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 소속
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
				$target_field1 = 'C.asso_code';
			}
			elseif($args['sch_kind'] == 2) {// 소재지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field1 = 'C.comp_addr_cd';
			}
			elseif($args['sch_kind'] == 3) { // 근로자수
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT;
				$target_field1 = 'C.emp_cnt_cd';
			}
			elseif($args['sch_kind'] == 4) { // 업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
				$target_field1 = 'C.comp_kind_cd';
			}
			elseif($args['sch_kind'] == 5) { // 운영기간
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD);
				$tg_m_code = CFG_SUB_CODE_OF_BIZ_COUNSEL_OPER_PERIOD;
				$target_field1 = 'C.oper_period_cd';
			}
			elseif($args['sch_kind'] == 6) {// 근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN);
				$tg_m_code = CFG_SUB_CODE_OF_BIZ_COUNSEL_CONTRACT_YN;
				$target_field1 = 'C.emp_paper_yn_cd';
			}
			elseif($args['sch_kind'] == 7) {// 4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN);
				$tg_m_code = CFG_SUB_CODE_OF_BIZ_COUNSEL_INSURANCE4_YN;
				$target_field1 = 'C.emp_insured_yn_cd';
			}
			elseif($args['sch_kind'] == 8) {// 취업규칙
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN);
				$tg_m_code = CFG_SUB_CODE_OF_BIZ_COUNSEL_EMPLOY_RULES_YN;
				$target_field1 = 'C.emp_rules_yn_cd';
			}
			elseif($args['sch_kind'] == 9) { // 상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND);
				$tg_m_code = CFG_SUB_CODE_OF_BIZ_COUNSEL_KIND;
				$target_field1 = 'CS.s_code';
			}
			elseif($args['sch_kind'] == 10) { // 처리결과
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT);
				$tg_m_code = CFG_SUB_CODE_OF_BIZ_COUNSEL_RESULT;
				$target_field1 = 'C.csl_proc_rst_cd';
			}
				
			// target 컬럼의 s_code 문자열로 생성
			$tg_s_code = '';
			foreach ($datas1 as $i => $item) {
				if($tg_s_code != '') {
					$tg_s_code .= '","';
				}
				$tg_s_code .= $item->s_code;
			}
			$tg_s_code = ' "'. $tg_s_code .'" ';
			
			// 년도별 총계 쿼리 - 년별
			$q_tot = 'SELECT ';
			// SELECT year_2018, year_2019,
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$q_tot .= 'year_'. $y .', dst_year_'. $y .', ';
			}
			// (year_2018 + year_2019) AS tot
			$q_tot .= '(';
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$q_tot .= 'year_'. $y .'+';
			}
			$q_tot = substr($q_tot , 0, -1);// 끝 "+" 제거
			$q_tot .= ') AS tot
					FROM (';
			for($y=$args['begin']; $y<=$args['end']; $y++) {
				$q_tot .= '(SELECT COUNT(*) AS year_'. $y .', COUNT(DISTINCT C.seq) AS dst_year_'. $y .'
						FROM biz_counsel C ';
				if($args['sch_kind'] == 9) { // 상담유형
					$q_tot .= 'INNER JOIN biz_counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q_tot .= 'WHERE '. $where .' ';
				if($args['sch_kind'] == 9) { // 상담유형
					$q_tot .= 'AND CS.s_code IN ('. $tg_s_code .') ';
				}
				$q_tot .= 'AND DATE_FORMAT(csl_date, "%Y")="'. $y .'"
					) AS year_'. $y .',';
			}
			$q_tot = substr($q_tot , 0, -1);// 끝 "," 제거
			$q_tot .= '
					)';
// 			echof($q_tot);			
			$rst_tot_annually = [];
			$total = 0;
			$rst_tot = $this->db->query($q_tot)->result();
			if(count($rst_tot) > 0) {
				$rst_tot_annually = (array)$rst_tot[0];
// 				echof($rst_tot_annually);
				$total = $rst_tot_annually['tot'];
			}
			
			$rstRtn['data_tot'] = 0;
			$rstRtn['tot_cnt'] = 0;
			$rstRtn['data'] = [];
				
			if($total > 0) {
				// 메인쿼리
				$q_tot = 'SELECT ';
				$q_tot2 = ', IFNULL('; // 각 연도별 계의 합산
				$q_tot3 = ',CONCAT(IFNULL(ROUND((('; // 총합산 - 각 연도의 합산
				$q_tot4 = ''; // base쿼리에서 년도별 합계용 select
				// 조회 연도만큼 select 컬럼 생성
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					if($q_tot != 'SELECT ') {
						$q_tot .= ', ';
					}
					if($args['sch_kind'] == 9) {// 상담유형 - 중복제거 합계 추가
						$q_tot .= 'CONCAT(IFNULL(SUM(ck_'. $y .'), 0), "(", '. $rst_tot_annually['dst_year_'. $y] .', ")") AS ck_'. $y .',
							IFNULL(ROUND(((SUM(ck_'. $y .'))/ '. $rst_tot_annually['year_'. $y] .')*100, 1), 0) AS cr_'. $y;
					}
					else {
						$q_tot .= 'IFNULL(SUM(ck_'. $y .'), 0) AS ck_'. $y .', IFNULL(ROUND(((SUM(ck_'. $y .'))/ '. $rst_tot_annually['year_'. $y] .')*100, 1), 0) AS cr_'. $y;
					}
					//
					if($q_tot2 != ', IFNULL(') {
						$q_tot2 .= '+';
					}
					$q_tot2 .= 'SUM(ck_'. $y .')';
					//
					if($q_tot3 != ',CONCAT(IFNULL(ROUND(((') {
						$q_tot3 .= '+';
					}
					$q_tot3 .= 'SUM(ck_'. $y .')';
					//
					if($q_tot4 != '') {
						$q_tot4 .= ', ';
					}
					$q_tot4 .= 'IF(target="'. $y .'", cnt, 0) as ck_'. $y;
				}
			
				$target_field2 = str_replace('C.', 'T2.', $target_field1);
				if($args['sch_kind'] == 9) { // 상담유형
					$target_field2 = str_replace('CS.', 'T2.', $target_field1);
				}
				$q_tot2 .= ', 0) AS tot';
				$q_tot3 .= ')/ '. $rst_tot_annually['tot'] .' )*100, 1), 0), "%") AS tot_p';
				$q_tot .= $q_tot2 . $q_tot3 .'
					FROM (
						SELECT '. $q_tot4 . '
						FROM (
							SELECT T1.code_name AS subject, T1.s_code, T2.target, T2.cnt
							FROM (
								SELECT code_name, s_code
								FROM sub_code
								WHERE m_code = "'. $tg_m_code .'"
							) T1
							LEFT JOIN (
								SELECT DATE_FORMAT(C.csl_date, "%Y") AS target, COUNT(C.seq) AS cnt, '. $target_field1 .'
								FROM biz_counsel C ';
				if($args['sch_kind'] == 9) { // 상담유형
					$q_tot .= 'INNER JOIN biz_counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				else {
					$q_tot .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
				}
				$q_tot .= 'WHERE '. $where .' AND C.csl_date BETWEEN "'. $args['begin'] .'-01-01"
									AND "'. $args['end'] .'-12-31" AND '. $target_field1 .' IN ('. $tg_s_code .')
								GROUP BY '. $target_field1 .', DATE_FORMAT(C.csl_date, "%Y")
								ORDER BY DATE_FORMAT(C.csl_date, "%Y") ASC
							) T2 ON T1.s_code='. $target_field2 .'
							ORDER BY T1.code_name ASC, T2.target ASC
						) G
					) Z';
// 				echof($q_tot);
				$year_tot = $this->db->query($q_tot)->result();				
				$rstRtn['data_tot'] = count($year_tot) > 0 ? $year_tot[0] : 0;
				$rstRtn['data_tot']->tot = $total;
			
				// 총계 쿼리 - 조회 년도별 계, 전체 계 쿼리 생성
				$q = 'SELECT subject, ';
				$q2 = ', IFNULL('; // 각 연도별 계의 합산
				$q3 = ',CONCAT(IFNULL(ROUND((('; // 총합산 - 각 연도의 합산
				$q4 = ''; // base쿼리에서 년도별 합계용 select
				// 조회 연도만큼 select 컬럼 생성
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					if($q != 'SELECT subject, ') {
						$q .= ', ';
					}
					$q .= 'IFNULL(SUM(ck_'. $y .'), 0) AS ck_'. $y .', IFNULL(ROUND(((SUM(ck_'. $y .'))/ '. $rst_tot_annually['year_'. $y] .')*100, 1), 0) AS cr_'. $y;
					//
					if($q2 != ', IFNULL(') {
						$q2 .= '+';
					}
					$q2 .= 'SUM(ck_'. $y .')';
					//
					if($q3 != ',CONCAT(IFNULL(ROUND(((') {
						$q3 .= '+';
					}
					$q3 .= 'SUM(ck_'. $y .')';
					//
					if($q4 != '') {
						$q4 .= ', ';
					}
					$q4 .= 'IF(target="'. $y .'", cnt, 0) as ck_'. $y;
				}
					
				$q2 .= ', 0) AS tot';
				$q3 .= ')/ '. $rst_tot_annually['tot'] .' )*100, 1), 0), "%") AS tot_p';
				$q .= $q2 . $q3 . '
					FROM (
						SELECT subject, '. $q4 . ', dsp_order
						FROM (
							SELECT T1.code_name AS subject, T1.s_code, T2.target, T2.cnt, T1.dsp_order
							FROM (
								SELECT code_name, s_code, dsp_order
								FROM sub_code
								WHERE m_code = "'. $tg_m_code .'"
							) T1
							LEFT JOIN (
								SELECT DATE_FORMAT(C.csl_date, "%Y") AS target, COUNT(C.seq) AS cnt, '. $target_field1 .'
								FROM biz_counsel C ';
				if($args['sch_kind'] == 9) { // 상담유형
					$q .= 'INNER JOIN biz_counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				else {
					$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
				}
				$q .= 'WHERE '. $where .' AND C.csl_date BETWEEN "'. $args['begin'] .'-01-01" AND "'. $args['end'] .'-12-31"
									AND '. $target_field1 .' IN ('. $tg_s_code .')
								GROUP BY '. $target_field1 .', DATE_FORMAT(C.csl_date, "%Y")
								ORDER BY DATE_FORMAT(C.csl_date, "%Y") ASC
							) T2 ON T1.s_code=T2.'. $target_field2 .'
							ORDER BY T1.code_name ASC, T2.target ASC
						) G
					) Z
					GROUP BY subject
					ORDER BY dsp_order';
			
				$rstRtn['data'] = $this->db->query($q)->result();
				$rstRtn['tot_cnt'] = count($rstRtn['data']);
			}
		}

		return $rstRtn;
	}

		
}