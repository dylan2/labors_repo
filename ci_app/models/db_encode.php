<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/******************************************************************************************************
 * DB 관리 Class
 * Db_encode
 * 작성자 : delee
 *
 *
 *****************************************************************************************************/
class Db_encode extends CI_Model {
	
	
    //=================================================================================================================
    // construct
    //=================================================================================================================
	function __construct() {
		parent::__construct();

    }
	
	
	
	
	
	public function get_csl_data() {

		$query = 'SELECT seq,csl_name,csl_tel '
			.'FROM  `counsel` '
			.'WHERE csl_name <> "" '
			.'AND seq <5099 ';

		$rs = $this->db->query($query);
		$rst = $rs->result();

		return $rst;

	}

	public function update_csl_data($args) {

		$data = array(
			'csl_name' => $args['csl_name']
			,'csl_tel' => $args['csl_tel']
		);
		$where = array(
			'seq' => $args['seq']
		);
		$this->db->update('counsel', $data, $where);
	}
	



	public function get_csl_data2() {

		$query = 'SELECT seq,csl_name,csl_tel '
			.'FROM  `counsel` '
			.'WHERE csl_name <> "" ';

		$rs = $this->db->query($query);
		$rst = $rs->result();

		return $rst;

	}
	
}