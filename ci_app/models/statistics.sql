/**
 * 
 * 통계 쿼리가 복잡하여 쿼리 작성 시 만든 원본쿼리를 남긴다.
 * 
 * 2019.07.23 
 * dylan
 * 
 */

/**
 * 
 * 상담내역 통계 - 기본통계
 * 
 */

/**
 * 
 * # 기본통계
 * 
 */
/* 소속 : 권익센터 */
SELECT "서울노동권익센터" AS s1, SUM(M.ck1) AS t1, "총계" AS s2, SUM(M.ck1) AS tot
FROM (
	SELECT IF(S.target = "C108", S.ck1, 0) AS ck1
		FROM (
		SELECT A.ck1, A.target
		FROM (
			SELECT code_name, s_code
			FROM sub_code
			WHERE s_code IN ('C108')) DD
		LEFT JOIN (
			SELECT COUNT(C.seq) AS ck1, C.asso_code, C.oper_id, C.asso_code AS target
			FROM counsel C
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2018-12-31 23:59:59"
			GROUP BY C.asso_code, C.oper_id, C.asso_code
		) A ON A.asso_code = DD.s_code
	) S
) M



/**
 * 
 * # 임금근로시간 통계 
 * 
 */

/* 임금근로시간 (단순)통계 - 메인쿼리 : 성별(기준코드)  */
SELECT subject, IFNULL(C.target, "-") AS target,
	IFNULL(SUM(pay), 0) AS pay, IFNULL(SUM(p_cnt), 0) AS p_cnt, 
	IFNULL(SUM(works), 0) AS works, IFNULL(SUM(w_cnt), 0) AS w_cnt,
	(SELECT COUNT(C.seq)
	FROM counsel C
	WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
		AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
		AND C.gender IN ( "C007","C008","C006" ) AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS dst_csl_tot /*해당 where의 전체 상담건수 */
FROM (
	SELECT DD.code_name AS subject, B.target, B.ave_pay_month AS pay, B.ck1 AS p_cnt, 
		B.work_time_week AS works, B.ck2 AS w_cnt, DD.dsp_order
	FROM (
		SELECT code_name, s_code, dsp_order
		FROM sub_code
		WHERE m_code = "S012"
	) DD
	LEFT JOIN (
		SELECT *
		FROM (
			SELECT C.gender AS target, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, 
				COUNT(C.seq) AS ck1, "" AS work_time_week, 0 AS ck2, SC.dsp_order
			FROM counsel C
			INNER JOIN sub_code SC ON C.gender=SC.s_code
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
				AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
				AND C.gender IN ( "C007","C008","C006" )
				AND C.ave_pay_month <> ""
			GROUP BY C.gender
			UNION /**/
			SELECT C.gender AS target, "" AS ave_pay_month, 0 AS ck1, 
				ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS work_time_week, COUNT(C.seq) AS ck2, SC.dsp_order
			FROM counsel C
			INNER JOIN sub_code SC ON C.gender=SC.s_code
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
				AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
				AND C.gender IN ( "C007","C008","C006" )
				AND C.work_time_week <> ""
			GROUP BY C.gender
		) A
		GROUP BY target, ave_pay_month, work_time_week
		ORDER BY target, dsp_order
	) B ON B.target=DD.s_code
) C
GROUP BY C.target, C.subject
ORDER BY C.dsp_order

/* 임금근로시간 (단순)통계 - 메인쿼리 : 상담유형(기준코드)  */
SELECT subject, IFNULL(C.target, "-") AS target,
	IFNULL(SUM(pay), 0) AS pay, IFNULL(SUM(p_cnt), 0) AS p_cnt, 
	IFNULL(SUM(works), 0) AS works, IFNULL(SUM(w_cnt), 0) AS w_cnt, dst_p_tot, dst_w_tot,
	(SELECT COUNT(C.seq)
	FROM counsel C
	/**/ INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq
	WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
		AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
		AND /*C.gender*/CS.s_code IN ( /**/"C057","C058","C059","C060","C061","C062","C063","C064","C065","C066","C067","C145" ) 
		AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS dst_csl_tot /*해당 where의 전체 상담건수 */
FROM (
	SELECT DD.code_name AS subject, B.target, B.ave_pay_month AS pay, B.ck1 AS p_cnt, 
		B.work_time_week AS works, B.ck2 AS w_cnt, DD.dsp_order,
		/**/(SELECT COUNT(*) FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
		AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
		AND C.ave_pay_month <> "") AS dst_p_tot,
		(SELECT COUNT(*) FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
		AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
		AND C.work_time_week <> "") AS dst_w_tot
	FROM (
		SELECT code_name, s_code, dsp_order
		FROM sub_code
		WHERE m_code = /**/"S020"
	) DD
	LEFT JOIN (
		SELECT *
		FROM (
			SELECT /*C.gender*/CS.s_code AS target, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, 
				COUNT(C.seq) AS ck1, "" AS work_time_week, 0 AS ck2, SC.dsp_order
			FROM counsel C
			/**/ INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq
			INNER JOIN sub_code SC ON /*C.gender*/CS.s_code=SC.s_code
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
				AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
				AND /*C.gender*/ SC.s_code IN ( /**/"C057","C058","C059","C060","C061","C062","C063","C064","C065","C066","C067","C145" )
				AND C.ave_pay_month <> ""
			GROUP BY /*C.gender*/CS.s_code
			UNION /**/
			SELECT /*C.gender*/CS.s_code AS target, "" AS ave_pay_month, 0 AS ck1, 
				ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS work_time_week, COUNT(C.seq) AS ck2, SC.dsp_order
			FROM counsel C
			/**/ INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq
			INNER JOIN sub_code SC ON /*C.gender*/CS.s_code=SC.s_code
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-07-31 23:59:59"  
				AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
				AND /*C.gender*/CS.s_code IN ( /**/"C057","C058","C059","C060","C061","C062","C063","C064","C065","C066","C067","C145" )
				AND C.work_time_week <> ""
			GROUP BY /*C.gender*/CS.s_code
		) A
		GROUP BY target, ave_pay_month, work_time_week
		ORDER BY target, dsp_order
	) B ON B.target=DD.s_code
) C
GROUP BY C.target, C.subject
ORDER BY C.dsp_order


/* 임금근로시간 (교차)통계 - 메인쿼리 : 성별-연령대  */
SELECT
	subject, IFNULL(SUM(pay1), 0) AS pay1, IFNULL(SUM(p_cnt1), 0) AS p_cnt1, IFNULL(SUM(p_dst_cnt1), 0) AS p_dst_cnt1,
	IFNULL(SUM(work1), 0) AS work1, IFNULL(SUM(w_cnt1), 0) AS w_cnt1, IFNULL(SUM(w_dst_cnt1), 0) AS w_dst_cnt1,IFNULL(SUM(pay2), 0) AS pay2, IFNULL(SUM(p_cnt2), 0) AS p_cnt2, IFNULL(SUM(p_dst_cnt2), 0) AS p_dst_cnt2,
	IFNULL(SUM(work2), 0) AS work2, IFNULL(SUM(w_cnt2), 0) AS w_cnt2, IFNULL(SUM(w_dst_cnt2), 0) AS w_dst_cnt2,IFNULL(SUM(pay3), 0) AS pay3, IFNULL(SUM(p_cnt3), 0) AS p_cnt3, IFNULL(SUM(p_dst_cnt3), 0) AS p_dst_cnt3,
	IFNULL(SUM(work3), 0) AS work3, IFNULL(SUM(w_cnt3), 0) AS w_cnt3, IFNULL(SUM(w_dst_cnt3), 0) AS w_dst_cnt3,IFNULL(SUM(pay4), 0) AS pay4, IFNULL(SUM(p_cnt4), 0) AS p_cnt4, IFNULL(SUM(p_dst_cnt4), 0) AS p_dst_cnt4,
	IFNULL(SUM(work4), 0) AS work4, IFNULL(SUM(w_cnt4), 0) AS w_cnt4, IFNULL(SUM(w_dst_cnt4), 0) AS w_dst_cnt4,IFNULL(SUM(pay5), 0) AS pay5, IFNULL(SUM(p_cnt5), 0) AS p_cnt5, IFNULL(SUM(p_dst_cnt5), 0) AS p_dst_cnt5,
	IFNULL(SUM(work5), 0) AS work5, IFNULL(SUM(w_cnt5), 0) AS w_cnt5, IFNULL(SUM(w_dst_cnt5), 0) AS w_dst_cnt5,IFNULL(SUM(pay6), 0) AS pay6, IFNULL(SUM(p_cnt6), 0) AS p_cnt6, IFNULL(SUM(p_dst_cnt6), 0) AS p_dst_cnt6,
	IFNULL(SUM(work6), 0) AS work6, IFNULL(SUM(w_cnt6), 0) AS w_cnt6, IFNULL(SUM(w_dst_cnt6), 0) AS w_dst_cnt6,IFNULL(SUM(pay7), 0) AS pay7, IFNULL(SUM(p_cnt7), 0) AS p_cnt7, IFNULL(SUM(p_dst_cnt7), 0) AS p_dst_cnt7,
	IFNULL(SUM(work7), 0) AS work7, IFNULL(SUM(w_cnt7), 0) AS w_cnt7, IFNULL(SUM(w_dst_cnt7), 0) AS w_dst_cnt7,IFNULL(SUM(pay8), 0) AS pay8, IFNULL(SUM(p_cnt8), 0) AS p_cnt8, IFNULL(SUM(p_dst_cnt8), 0) AS p_dst_cnt8,
	IFNULL(SUM(work8), 0) AS work8, IFNULL(SUM(w_cnt8), 0) AS w_cnt8, IFNULL(SUM(w_dst_cnt8), 0) AS w_dst_cnt8,dst_p_tot, dst_w_tot, 
	(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')  AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS dst_csl_tot/* 중복제거 총상담건수 */,/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C007" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot1 ,/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C007" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot1 ,/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C008" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot2 ,/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C008" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot2 ,/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C006" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot3 ,/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C006" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot3 FROM (
	SELECT
		C.subject, C.target, C.target2,
		/* 교차대상 코드별 데이터 추출 */ IF( C.target2="C010", SUM(C.ave_pay_month), 0) AS pay1, IF( C.target2="C010", SUM(C.ck1), 0) AS p_cnt1, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C010", SUM(C.dst_ck1), 0) AS p_dst_cnt1,
	IF( C.target2="C010", SUM(C.work_time_week), 0) AS work1, IF( C.target2="C010", SUM(C.ck2), 0) AS w_cnt1, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C010", SUM(C.dst_ck2), 0) AS w_dst_cnt1, IF( C.target2="C011", SUM(C.ave_pay_month), 0) AS pay2, IF( C.target2="C011", SUM(C.ck1), 0) AS p_cnt2, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C011", SUM(C.dst_ck1), 0) AS p_dst_cnt2,
	IF( C.target2="C011", SUM(C.work_time_week), 0) AS work2, IF( C.target2="C011", SUM(C.ck2), 0) AS w_cnt2, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C011", SUM(C.dst_ck2), 0) AS w_dst_cnt2, IF( C.target2="C012", SUM(C.ave_pay_month), 0) AS pay3, IF( C.target2="C012", SUM(C.ck1), 0) AS p_cnt3, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C012", SUM(C.dst_ck1), 0) AS p_dst_cnt3,
	IF( C.target2="C012", SUM(C.work_time_week), 0) AS work3, IF( C.target2="C012", SUM(C.ck2), 0) AS w_cnt3, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C012", SUM(C.dst_ck2), 0) AS w_dst_cnt3, IF( C.target2="C013", SUM(C.ave_pay_month), 0) AS pay4, IF( C.target2="C013", SUM(C.ck1), 0) AS p_cnt4, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C013", SUM(C.dst_ck1), 0) AS p_dst_cnt4,
	IF( C.target2="C013", SUM(C.work_time_week), 0) AS work4, IF( C.target2="C013", SUM(C.ck2), 0) AS w_cnt4, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C013", SUM(C.dst_ck2), 0) AS w_dst_cnt4, IF( C.target2="C014", SUM(C.ave_pay_month), 0) AS pay5, IF( C.target2="C014", SUM(C.ck1), 0) AS p_cnt5, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C014", SUM(C.dst_ck1), 0) AS p_dst_cnt5,
	IF( C.target2="C014", SUM(C.work_time_week), 0) AS work5, IF( C.target2="C014", SUM(C.ck2), 0) AS w_cnt5, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C014", SUM(C.dst_ck2), 0) AS w_dst_cnt5, IF( C.target2="C015", SUM(C.ave_pay_month), 0) AS pay6, IF( C.target2="C015", SUM(C.ck1), 0) AS p_cnt6, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C015", SUM(C.dst_ck1), 0) AS p_dst_cnt6,
	IF( C.target2="C015", SUM(C.work_time_week), 0) AS work6, IF( C.target2="C015", SUM(C.ck2), 0) AS w_cnt6, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C015", SUM(C.dst_ck2), 0) AS w_dst_cnt6, IF( C.target2="C080", SUM(C.ave_pay_month), 0) AS pay7, IF( C.target2="C080", SUM(C.ck1), 0) AS p_cnt7, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C080", SUM(C.dst_ck1), 0) AS p_dst_cnt7,
	IF( C.target2="C080", SUM(C.work_time_week), 0) AS work7, IF( C.target2="C080", SUM(C.ck2), 0) AS w_cnt7, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C080", SUM(C.dst_ck2), 0) AS w_dst_cnt7, IF( C.target2="C009", SUM(C.ave_pay_month), 0) AS pay8, IF( C.target2="C009", SUM(C.ck1), 0) AS p_cnt8, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C009", SUM(C.dst_ck1), 0) AS p_dst_cnt8,
	IF( C.target2="C009", SUM(C.work_time_week), 0) AS work8, IF( C.target2="C009", SUM(C.ck2), 0) AS w_cnt8, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C009", SUM(C.dst_ck2), 0) AS w_dst_cnt8, /*중복제거 상담건수(임금,근로시간) -->*/(SELECT COUNT(*) FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
		AND C.ave_pay_month <> "") AS dst_p_tot,
		(SELECT COUNT(*) FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
		AND C.work_time_week <> "") AS dst_w_tot/*<--*/, C.dsp_order
	FROM (
		SELECT DD.code_name AS subject, B.target, B.target2, B.ave_pay_month, B.ck1, B.dst_ck1, B.work_time_week, B.ck2, B.dst_ck2, DD.dsp_order
		FROM (
			SELECT code_name, s_code, dsp_order
			FROM sub_code
			WHERE m_code="S012" /*target 과 같은 코드로 매치 */
			ORDER BY dsp_order
		) DD
		LEFT JOIN (
			SELECT *
			FROM (
				SELECT C.gender AS target, C.ages AS target2, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, 
					"" AS work_time_week, COUNT(C.seq) AS ck1, COUNT(DISTINCT(C.seq)) AS dst_ck1, 0 AS ck2, 0 AS dst_ck2, SC.dsp_order
				FROM counsel C INNER JOIN sub_code SC ON C.gender=SC.s_code
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
					AND /*target1*/C.gender IN (  'C007','C008','C006'  )
					AND /*target2*/C.ages IN (  'C010','C011','C012','C013','C014','C015','C080','C009'  )
					AND C.ave_pay_month <> ""
				GROUP BY /*target1*/C.gender, /*target2*/C.ages
				UNION /**/			
				SELECT C.gender AS target, C.ages AS target2, "" AS ave_pay_month, ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS ave_pay_month, 
					0 AS ck1, 0 AS dst_ck1, COUNT(C.seq) AS ck2, COUNT(DISTINCT(C.seq)) AS dst_ck2, SC.dsp_order
				FROM counsel C INNER JOIN sub_code SC ON /**/C.gender=SC.s_code
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
					AND /*target1*/C.gender IN (  'C007','C008','C006'  )
					AND /*target2*/C.ages IN (  'C010','C011','C012','C013','C014','C015','C080','C009'  )
					AND C.work_time_week <> ""
				GROUP BY /*target1*/C.gender, /*target2*/C.ages
			) A
			GROUP BY target, target2, ave_pay_month, work_time_week
		) B ON B.target=DD.s_code
	) C
	GROUP BY subject, target2
) T
GROUP BY subject
ORDER BY dsp_order

/* 임금근로시간 (교차)통계 - 메인쿼리 - 성별:상담유형(기준코드)  */
SELECT
	subject, IFNULL(SUM(pay1), 0) AS pay1, IFNULL(SUM(p_cnt1), 0) AS p_cnt1, IFNULL(SUM(p_dst_cnt1), 0) AS p_dst_cnt1,
	IFNULL(SUM(work1), 0) AS work1, IFNULL(SUM(w_cnt1), 0) AS w_cnt1, IFNULL(SUM(w_dst_cnt1), 0) AS w_dst_cnt1,IFNULL(SUM(pay2), 0) AS pay2, IFNULL(SUM(p_cnt2), 0) AS p_cnt2, IFNULL(SUM(p_dst_cnt2), 0) AS p_dst_cnt2,
	IFNULL(SUM(work2), 0) AS work2, IFNULL(SUM(w_cnt2), 0) AS w_cnt2, IFNULL(SUM(w_dst_cnt2), 0) AS w_dst_cnt2,IFNULL(SUM(pay3), 0) AS pay3, IFNULL(SUM(p_cnt3), 0) AS p_cnt3, IFNULL(SUM(p_dst_cnt3), 0) AS p_dst_cnt3,
	IFNULL(SUM(work3), 0) AS work3, IFNULL(SUM(w_cnt3), 0) AS w_cnt3, IFNULL(SUM(w_dst_cnt3), 0) AS w_dst_cnt3,IFNULL(SUM(pay4), 0) AS pay4, IFNULL(SUM(p_cnt4), 0) AS p_cnt4, IFNULL(SUM(p_dst_cnt4), 0) AS p_dst_cnt4,
	IFNULL(SUM(work4), 0) AS work4, IFNULL(SUM(w_cnt4), 0) AS w_cnt4, IFNULL(SUM(w_dst_cnt4), 0) AS w_dst_cnt4,IFNULL(SUM(pay5), 0) AS pay5, IFNULL(SUM(p_cnt5), 0) AS p_cnt5, IFNULL(SUM(p_dst_cnt5), 0) AS p_dst_cnt5,
	IFNULL(SUM(work5), 0) AS work5, IFNULL(SUM(w_cnt5), 0) AS w_cnt5, IFNULL(SUM(w_dst_cnt5), 0) AS w_dst_cnt5,IFNULL(SUM(pay6), 0) AS pay6, IFNULL(SUM(p_cnt6), 0) AS p_cnt6, IFNULL(SUM(p_dst_cnt6), 0) AS p_dst_cnt6,
	IFNULL(SUM(work6), 0) AS work6, IFNULL(SUM(w_cnt6), 0) AS w_cnt6, IFNULL(SUM(w_dst_cnt6), 0) AS w_dst_cnt6,IFNULL(SUM(pay7), 0) AS pay7, IFNULL(SUM(p_cnt7), 0) AS p_cnt7, IFNULL(SUM(p_dst_cnt7), 0) AS p_dst_cnt7,
	IFNULL(SUM(work7), 0) AS work7, IFNULL(SUM(w_cnt7), 0) AS w_cnt7, IFNULL(SUM(w_dst_cnt7), 0) AS w_dst_cnt7,IFNULL(SUM(pay8), 0) AS pay8, IFNULL(SUM(p_cnt8), 0) AS p_cnt8, IFNULL(SUM(p_dst_cnt8), 0) AS p_dst_cnt8,
	IFNULL(SUM(work8), 0) AS work8, IFNULL(SUM(w_cnt8), 0) AS w_cnt8, IFNULL(SUM(w_dst_cnt8), 0) AS w_dst_cnt8,IFNULL(SUM(pay9), 0) AS pay9, IFNULL(SUM(p_cnt9), 0) AS p_cnt9, IFNULL(SUM(p_dst_cnt9), 0) AS p_dst_cnt9,
	IFNULL(SUM(work9), 0) AS work9, IFNULL(SUM(w_cnt9), 0) AS w_cnt9, IFNULL(SUM(w_dst_cnt9), 0) AS w_dst_cnt9,IFNULL(SUM(pay10), 0) AS pay10, IFNULL(SUM(p_cnt10), 0) AS p_cnt10, IFNULL(SUM(p_dst_cnt10), 0) AS p_dst_cnt10,
	IFNULL(SUM(work10), 0) AS work10, IFNULL(SUM(w_cnt10), 0) AS w_cnt10, IFNULL(SUM(w_dst_cnt10), 0) AS w_dst_cnt10,IFNULL(SUM(pay11), 0) AS pay11, IFNULL(SUM(p_cnt11), 0) AS p_cnt11, IFNULL(SUM(p_dst_cnt11), 0) AS p_dst_cnt11,
	IFNULL(SUM(work11), 0) AS work11, IFNULL(SUM(w_cnt11), 0) AS w_cnt11, IFNULL(SUM(w_dst_cnt11), 0) AS w_dst_cnt11,IFNULL(SUM(pay12), 0) AS pay12, IFNULL(SUM(p_cnt12), 0) AS p_cnt12, IFNULL(SUM(p_dst_cnt12), 0) AS p_dst_cnt12,
	IFNULL(SUM(work12), 0) AS work12, IFNULL(SUM(w_cnt12), 0) AS w_cnt12, IFNULL(SUM(w_dst_cnt12), 0) AS w_dst_cnt12,dst_p_tot, dst_w_tot, 
	(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')  AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS dst_csl_tot/* 중복제거 총상담건수 */,/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
	FROM counsel C /**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C007" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot1 ,/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C /**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C007" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot1 ,/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
	FROM counsel C /**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C008" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot2 ,/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C /**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C008" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot2 ,/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
	FROM counsel C /**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C006" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot3 ,/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
	FROM counsel C /**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
	AND /*target1*/C.gender="C006" 
	AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot3 FROM (
	SELECT
		C.subject, C.target, C.target2,
		/* 교차대상 코드별 데이터 추출 */ IF( C.target2="C057", SUM(C.ave_pay_month), 0) AS pay1, IF( C.target2="C057", SUM(C.ck1), 0) AS p_cnt1, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C057", SUM(C.dst_ck1), 0) AS p_dst_cnt1,
	IF( C.target2="C057", SUM(C.work_time_week), 0) AS work1, IF( C.target2="C057", SUM(C.ck2), 0) AS w_cnt1, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C057", SUM(C.dst_ck2), 0) AS w_dst_cnt1, IF( C.target2="C058", SUM(C.ave_pay_month), 0) AS pay2, IF( C.target2="C058", SUM(C.ck1), 0) AS p_cnt2, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C058", SUM(C.dst_ck1), 0) AS p_dst_cnt2,
	IF( C.target2="C058", SUM(C.work_time_week), 0) AS work2, IF( C.target2="C058", SUM(C.ck2), 0) AS w_cnt2, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C058", SUM(C.dst_ck2), 0) AS w_dst_cnt2, IF( C.target2="C059", SUM(C.ave_pay_month), 0) AS pay3, IF( C.target2="C059", SUM(C.ck1), 0) AS p_cnt3, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C059", SUM(C.dst_ck1), 0) AS p_dst_cnt3,
	IF( C.target2="C059", SUM(C.work_time_week), 0) AS work3, IF( C.target2="C059", SUM(C.ck2), 0) AS w_cnt3, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C059", SUM(C.dst_ck2), 0) AS w_dst_cnt3, IF( C.target2="C060", SUM(C.ave_pay_month), 0) AS pay4, IF( C.target2="C060", SUM(C.ck1), 0) AS p_cnt4, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C060", SUM(C.dst_ck1), 0) AS p_dst_cnt4,
	IF( C.target2="C060", SUM(C.work_time_week), 0) AS work4, IF( C.target2="C060", SUM(C.ck2), 0) AS w_cnt4, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C060", SUM(C.dst_ck2), 0) AS w_dst_cnt4, IF( C.target2="C061", SUM(C.ave_pay_month), 0) AS pay5, IF( C.target2="C061", SUM(C.ck1), 0) AS p_cnt5, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C061", SUM(C.dst_ck1), 0) AS p_dst_cnt5,
	IF( C.target2="C061", SUM(C.work_time_week), 0) AS work5, IF( C.target2="C061", SUM(C.ck2), 0) AS w_cnt5, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C061", SUM(C.dst_ck2), 0) AS w_dst_cnt5, IF( C.target2="C062", SUM(C.ave_pay_month), 0) AS pay6, IF( C.target2="C062", SUM(C.ck1), 0) AS p_cnt6, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C062", SUM(C.dst_ck1), 0) AS p_dst_cnt6,
	IF( C.target2="C062", SUM(C.work_time_week), 0) AS work6, IF( C.target2="C062", SUM(C.ck2), 0) AS w_cnt6, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C062", SUM(C.dst_ck2), 0) AS w_dst_cnt6, IF( C.target2="C063", SUM(C.ave_pay_month), 0) AS pay7, IF( C.target2="C063", SUM(C.ck1), 0) AS p_cnt7, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C063", SUM(C.dst_ck1), 0) AS p_dst_cnt7,
	IF( C.target2="C063", SUM(C.work_time_week), 0) AS work7, IF( C.target2="C063", SUM(C.ck2), 0) AS w_cnt7, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C063", SUM(C.dst_ck2), 0) AS w_dst_cnt7, IF( C.target2="C064", SUM(C.ave_pay_month), 0) AS pay8, IF( C.target2="C064", SUM(C.ck1), 0) AS p_cnt8, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C064", SUM(C.dst_ck1), 0) AS p_dst_cnt8,
	IF( C.target2="C064", SUM(C.work_time_week), 0) AS work8, IF( C.target2="C064", SUM(C.ck2), 0) AS w_cnt8, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C064", SUM(C.dst_ck2), 0) AS w_dst_cnt8, IF( C.target2="C065", SUM(C.ave_pay_month), 0) AS pay9, IF( C.target2="C065", SUM(C.ck1), 0) AS p_cnt9, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C065", SUM(C.dst_ck1), 0) AS p_dst_cnt9,
	IF( C.target2="C065", SUM(C.work_time_week), 0) AS work9, IF( C.target2="C065", SUM(C.ck2), 0) AS w_cnt9, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C065", SUM(C.dst_ck2), 0) AS w_dst_cnt9, IF( C.target2="C066", SUM(C.ave_pay_month), 0) AS pay10, IF( C.target2="C066", SUM(C.ck1), 0) AS p_cnt10, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C066", SUM(C.dst_ck1), 0) AS p_dst_cnt10,
	IF( C.target2="C066", SUM(C.work_time_week), 0) AS work10, IF( C.target2="C066", SUM(C.ck2), 0) AS w_cnt10, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C066", SUM(C.dst_ck2), 0) AS w_dst_cnt10, IF( C.target2="C067", SUM(C.ave_pay_month), 0) AS pay11, IF( C.target2="C067", SUM(C.ck1), 0) AS p_cnt11, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C067", SUM(C.dst_ck1), 0) AS p_dst_cnt11,
	IF( C.target2="C067", SUM(C.work_time_week), 0) AS work11, IF( C.target2="C067", SUM(C.ck2), 0) AS w_cnt11, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C067", SUM(C.dst_ck2), 0) AS w_dst_cnt11, IF( C.target2="C145", SUM(C.ave_pay_month), 0) AS pay12, IF( C.target2="C145", SUM(C.ck1), 0) AS p_cnt12, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C145", SUM(C.dst_ck1), 0) AS p_dst_cnt12,
	IF( C.target2="C145", SUM(C.work_time_week), 0) AS work12, IF( C.target2="C145", SUM(C.ck2), 0) AS w_cnt12, 
	/* 상담유형용 중복제거 건수*/IF( C.target2="C145", SUM(C.dst_ck2), 0) AS w_dst_cnt12, /*중복제거 상담건수(임금,근로시간) -->*/(SELECT COUNT(*) FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
		AND C.ave_pay_month <> "") AS dst_p_tot,
		(SELECT COUNT(*) FROM counsel C WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
		AND C.work_time_week <> "") AS dst_w_tot/*<--*/, C.dsp_order
	FROM (
		SELECT DD.code_name AS subject, B.target, B.target2, B.ave_pay_month, B.ck1, B.dst_ck1, B.work_time_week, B.ck2, B.dst_ck2, DD.dsp_order
		FROM (
			SELECT code_name, s_code, dsp_order
			FROM sub_code
			WHERE m_code="S012" /*target 과 같은 코드로 매치 */
			ORDER BY dsp_order
		) DD
		LEFT JOIN (
			SELECT *
			FROM (
				SELECT C.gender AS target, CS.s_code AS target2, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, 
					"" AS work_time_week, COUNT(C.seq) AS ck1, COUNT(DISTINCT(C.seq)) AS dst_ck1, 0 AS ck2, 0 AS dst_ck2, SC.dsp_order
				FROM counsel C INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq INNER JOIN sub_code SC ON C.gender=SC.s_code
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
					AND /*target1*/C.gender IN (  'C007','C008','C006'  )
					AND /*target2*/CS.s_code IN (  'C057','C058','C059','C060','C061','C062','C063','C064','C065','C066','C067','C145'  )
					AND C.ave_pay_month <> ""
				GROUP BY /*target1*/C.gender, /*target2*/CS.s_code
				UNION /**/			
				SELECT C.gender AS target, CS.s_code AS target2, "" AS ave_pay_month, ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS ave_pay_month, 
					0 AS ck1, 0 AS dst_ck1, COUNT(C.seq) AS ck2, COUNT(DISTINCT(C.seq)) AS dst_ck2, SC.dsp_order
				FROM counsel C INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq INNER JOIN sub_code SC ON /**/C.gender=SC.s_code
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-08-13 23:59:59"  AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
					AND /*target1*/C.gender IN (  'C007','C008','C006'  )
					AND /*target2*/CS.s_code IN (  'C057','C058','C059','C060','C061','C062','C063','C064','C065','C066','C067','C145'  )
					AND C.work_time_week <> ""
				GROUP BY /*target1*/C.gender, /*target2*/CS.s_code
			) A
			GROUP BY target, target2, ave_pay_month, work_time_week
		) B ON B.target=DD.s_code
	) C
	GROUP BY subject, target2
) T
GROUP BY subject
ORDER BY dsp_order



/**
 * # 소속별 통계 
 * 
 * 임금근로시간 외 나머지 통계 - 상담방법,성별,연령대.....
 * 
 * 통계유형: 상담방법
 */
/* 총계 쿼리  */
SELECT SUM(M.ck1), SUM(M.ck2), SUM(M.ck3), SUM(M.ck4), SUM(M.ck5), SUM(M.ck6), SUM(M.ck1) + SUM(M.ck2) + SUM(M.ck3) + SUM(M.ck4) + SUM(M.ck5) + SUM(M.ck6) AS tot
FROM (
	SELECT S.subject, IF(S.target = "C001", S.ck1, 0) AS ck1, IF(S.target = "C002", S.ck1, 0) AS ck2, IF(S.target = "C003", S.ck1, 0) AS ck3, IF(S.target = "C004", S.ck1, 0) AS ck4, IF(S.target = "C005", S.ck1, 0) AS ck5, IF(S.target = "C194", S.ck1, 0) AS ck6
	FROM (
		SELECT DD.code_name AS subject, A.ck1, A.target
		FROM (
			SELECT code_name, s_code, dsp_code
			FROM sub_code
			WHERE m_code = "S023"
		) DD
		LEFT JOIN (
			SELECT COUNT(C.seq) AS ck1, C.asso_code AS target2, C.s_code AS target
			FROM counsel C
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-12-31 23:59:59" AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
			GROUP BY C.s_code, C.asso_code
		) A ON A.target2 = DD.s_code
	) S
) M


/* 메인 쿼리  */
SELECT M.subject, 
	CONCAT(IFNULL(SUM(M.ck1), 0), " (", IFNULL(ROUND(((SUM(M.ck1))/ /*총계*/7674)*100, 1), 0), "%)") AS ck1, 
	CONCAT(IFNULL(SUM(M.ck2), 0), " (", IFNULL(ROUND(((SUM(M.ck2))/7674)*100, 1), 0), "%)") AS ck2, 
	CONCAT(IFNULL(SUM(M.ck3), 0), " (", IFNULL(ROUND(((SUM(M.ck3))/7674)*100, 1), 0), "%)") AS ck3, 
	CONCAT(IFNULL(SUM(M.ck4), 0), " (", IFNULL(ROUND(((SUM(M.ck4))/7674)*100, 1), 0), "%)") AS ck4, 
	CONCAT(IFNULL(SUM(M.ck5), 0), " (", IFNULL(ROUND(((SUM(M.ck5))/7674)*100, 1), 0), "%)") AS ck5, 
	CONCAT(IFNULL(SUM(M.ck6), 0), " (", IFNULL(ROUND(((SUM(M.ck6))/7674)*100, 1), 0), "%)") AS ck6, 
	CONCAT(IFNULL(SUM(M.ck1) + SUM(M.ck2) + SUM(M.ck3) + SUM(M.ck4) + SUM(M.ck5) + SUM(M.ck6), 0), " (", IFNULL(ROUND(((SUM(M.ck1) + SUM(M.ck2) + SUM(M.ck3) + SUM(M.ck4) + SUM(M.ck5) + SUM(M.ck6))/7674)*100, 1), 0), "%)") AS tot
FROM (
	SELECT S.subject, IF(S.target = "C001", S.ck1, 0) AS ck1, IF(S.target = "C002", S.ck1, 0) AS ck2, IF(S.target = "C003", S.ck1, 0) AS ck3, IF(S.target = "C004", S.ck1, 0) AS ck4, IF(S.target = "C005", S.ck1, 0) AS ck5, IF(S.target = "C194", S.ck1, 0) AS ck6, S.dsp_order
	FROM (
		SELECT DD.code_name AS subject, A.ck1, A.target, DD.dsp_order
		FROM (
			SELECT SC.code_name, SC.s_code, SC.dsp_order
			FROM sub_code SC
			WHERE m_code = "S023" /**/
		) DD
		LEFT JOIN (
			SELECT COUNT(C.seq) AS ck1, C.asso_code AS target2, C.s_code AS target
			FROM counsel C
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01 00:00:00" AND "2019-12-31 23:59:59" AND C.asso_code IN ("C108", "C131", "C133", "C135", "C136", "C137", "C179", "C180", "C184", "C186", "C187", "C191", "C203", "C204", "C205")
			GROUP BY C.s_code, C.asso_code
		) A ON A.target2 = DD.s_code
	) S
) M
GROUP BY M.subject
ORDER BY M.dsp_order


/* 시계열통계 총계쿼리 : 상담유형 - 건수,비율, 2019~2019년도 */
SELECT year_2018, dst_year_2018, year_2019, dst_year_2019, (year_2018+year_2019) AS tot
FROM (
	(
		SELECT COUNT(*) AS year_2018, COUNT(DISTINCT C.seq) AS dst_year_2018
		FROM counsel C
		INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq
		WHERE 1=1 AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
			AND CS.s_code IN ("C057","C058","C059","C060","C061","C062","C063","C064","C065","C066","C067","C145") 
			AND DATE_FORMAT(csl_date, "%Y")="2018"
	) AS year_2018,
	(
		SELECT COUNT(*) AS year_2019, COUNT(DISTINCT C.seq) AS dst_year_2019
		FROM counsel C
		INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq
		WHERE 1=1 AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
			AND CS.s_code IN ("C057","C058","C059","C060","C061","C062","C063","C064","C065","C066","C067","C145") 
			AND DATE_FORMAT(csl_date, "%Y")="2019"
	) AS year_2019
)


/* 시계열통계 메인쿼리 : 상담방법 - 임금,근로시간, 2019년도 */
SELECT subject, CONCAT(IFNULL(SUM(p_2019), 0), " (", IFNULL(SUM(p_cnt_2019), 0), ")") AS p_2019, CONCAT(IFNULL(SUM(w_2019), 0), " (", IFNULL(SUM(w_cnt_2019), 0), ")") AS w_2019 
FROM ( 
	SELECT subject, IF(target="p_2019", summ, 0) as p_2019, IF(target="p_2019", cnt, 0) as p_cnt_2019, IF(target="w_2019", summ, 0) as w_2019, IF(target="w_2019", cnt, 0) as w_cnt_2019, dsp_order
	FROM (
		SELECT T1.code_name AS subject, T1.s_code, T2.target, T2.summ, T1.dsp_order, T2.cnt
		FROM (
			SELECT code_name, s_code, dsp_order
			FROM sub_code
			WHERE m_code = "S011"
		) T1
		LEFT JOIN
		(
			SELECT CONCAT("p_", DATE_FORMAT(C.csl_date, "%Y")) AS target, ROUND(SUM(C.ave_pay_month)/COUNT(C.ave_pay_month),0) AS summ, COUNT(*) AS cnt, C.s_code
			FROM counsel C 
			INNER JOIN sub_code SC ON C.s_code=SC.s_code 
			WHERE 1=1 AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205') 
				AND C.csl_date BETWEEN "2019-01-01" AND "2019-12-31" 
				AND SC.s_code IN ( "C001","C002","C003","C004","C005","C194" ) 
				AND C.ave_pay_month <> ""
			GROUP BY DATE_FORMAT(C.csl_date, "%Y"), C.s_code 
			UNION
			SELECT CONCAT("w_", DATE_FORMAT(C.csl_date, "%Y")) AS target, ROUND(SUM(C.work_time_week)/COUNT(C.work_time_week),2) AS summ, COUNT(*) AS cnt, C.s_code
			FROM counsel C 
			INNER JOIN sub_code SC ON C.s_code=SC.s_code 
			WHERE 1=1 AND C.asso_code IN ('C108', 'C131', 'C133', 'C135', 'C136', 'C137', 'C179', 'C180', 'C184', 'C186', 'C187', 'C191', 'C203', 'C204', 'C205')
				AND C.csl_date BETWEEN "2019-01-01" AND "2019-12-31" 
				AND SC.s_code IN ( "C001","C002","C003","C004","C005","C194" ) 
				AND C.work_time_week <> ""
			GROUP BY DATE_FORMAT(C.csl_date, "%Y"), C.s_code
		) T2 ON T1.s_code=T2.s_code
		GROUP BY subject, target
	) TT
) G
GROUP BY subject
ORDER BY dsp_order




/**
 * 
 * 사용자상담 통계 
 * 
 */

/* 임금근로시간 교차통계 - 메인쿼리 : 소속-성별 */



/* 시계열통계 총계쿼리 : 상담유형 - 건수/비율 */
SELECT year_2018, dst_year_2018, year_2019, dst_year_2019, (year_2018+year_2019) AS tot
FROM (
	(
		SELECT COUNT(*) AS year_2018, COUNT(DISTINCT C.seq) AS dst_year_2018
		FROM biz_counsel C
		INNER JOIN biz_counsel_sub CS ON C.seq=CS.csl_seq
		WHERE 1=1 AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
			AND CS.s_code IN ("C228","C229","C230","C231","C232","C233","C234","C235") 
			AND DATE_FORMAT(csl_date, "%Y")="2018"
	) AS year_2018,
	(
		SELECT COUNT(*) AS year_2019, COUNT(DISTINCT C.seq) AS dst_year_2019
		FROM biz_counsel C
		INNER JOIN biz_counsel_sub CS ON C.seq=CS.csl_seq
		WHERE 1=1 AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
			AND CS.s_code IN ("C228","C229","C230","C231","C232","C233","C234","C235") 
			AND DATE_FORMAT(csl_date, "%Y")="2019"
	) AS year_2019
)

/* 소속별통계 총계쿼리 - 상담유형 */
SELECT CONCAT(IFNULL(SUM(M.ck1), 0), "(", IFNULL(M.distinct_ck1, 0), ")"), CONCAT(IFNULL(SUM(M.ck2), 0), "(", IFNULL(M.distinct_ck2, 0), ")"), CONCAT(IFNULL(SUM(M.ck3), 0), "(", IFNULL(M.distinct_ck3, 0), ")"), CONCAT(IFNULL(SUM(M.ck4), 0), "(", IFNULL(M.distinct_ck4, 0), ")"), CONCAT(IFNULL(SUM(M.ck5), 0), "(", IFNULL(M.distinct_ck5, 0), ")"), CONCAT(IFNULL(SUM(M.ck6), 0), "(", IFNULL(M.distinct_ck6, 0), ")"), CONCAT(IFNULL(SUM(M.ck7), 0), "(", IFNULL(M.distinct_ck7, 0), ")"), CONCAT(IFNULL(SUM(M.ck8), 0), "(", IFNULL(M.distinct_ck8, 0), ")"), CONCAT(IFNULL(SUM(M.ck1) + SUM(M.ck2) + SUM(M.ck3) + SUM(M.ck4) + SUM(M.ck5) + SUM(M.ck6) + SUM(M.ck7) + SUM(M.ck8), 0), "(", IFNULL(M.distinct_tot, 0), ")") AS tot
FROM (
	SELECT S.subject, IF(S.target = "C228", S.ck1, 0) AS ck1, S.distinct_ck1, IF(S.target = "C229", S.ck1, 0) AS ck2, S.distinct_ck2, IF(S.target = "C230", S.ck1, 0) AS ck3, S.distinct_ck3, IF(S.target = "C231", S.ck1, 0) AS ck4, S.distinct_ck4, IF(S.target = "C232", S.ck1, 0) AS ck5, S.distinct_ck5, IF(S.target = "C233", S.ck1, 0) AS ck6, S.distinct_ck6, IF(S.target = "C234", S.ck1, 0) AS ck7, S.distinct_ck7, IF(S.target = "C235", S.ck1, 0) AS ck8, S.distinct_ck8, S.distinct_tot
	FROM (
		SELECT DD.code_name AS subject, A.ck1, A.target,
			(
				SELECT COUNT(DISTINCT C.seq) AS distinct_tot
				FROM biz_counsel C
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205')
			) AS distinct_tot, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck1
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C228"
			) AS distinct_ck1, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck2
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C229"
			) AS distinct_ck2, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck3
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C230"
			) AS distinct_ck3, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck4
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C231"
			) AS distinct_ck4, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck5
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C232"
			) AS distinct_ck5, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck6
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C233"
			) AS distinct_ck6, 
			(
				SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck7
				FROM biz_counsel C
				INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
				WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
					AND CS2.s_code = "C234"
			) AS distinct_ck7, 
			(
			SELECT COUNT(DISTINCT CS2.s_code) AS distinct_ck8
			FROM biz_counsel C
			INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
				AND CS2.s_code = "C235"
			) AS distinct_ck8
		FROM (
			SELECT code_name, s_code, dsp_code
			FROM sub_code
			WHERE m_code = "S023"
		) DD
		LEFT JOIN (
			SELECT COUNT(C.seq) AS ck1, C.asso_code AS target2, CS2.s_code AS target
			FROM biz_counsel C
			INNER JOIN biz_counsel_sub CS2 ON C.seq=CS2.csl_seq
			WHERE 1=1 AND C.csl_date BETWEEN "2019-01-01" AND "2019-07-30" AND C.asso_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205') 
				AND CS2.s_code IN ("C228","C229","C230","C231","C232","C233","C234","C235")
			GROUP BY CS2.s_code, C.asso_code) A ON A.target2 = DD.s_code
	) S
) M


/* 월별통계 - 전체 */
SELECT M.subject, SUM(M.ck1) AS ck1, SUM(M.ck2) AS ck2, SUM(M.ck3) AS ck3, SUM(M.ck4) AS ck4, SUM(M.ck5) AS ck5, SUM(M.ck6) AS ck6, SUM(M.ck7) AS ck7, SUM(M.ck8) AS ck8, SUM(M.ck9) AS ck9, SUM(M.ck10) AS ck10, SUM(M.ck11) AS ck11, SUM(M.ck12) AS ck12, SUM(M.ck1) + SUM(M.ck2) + SUM(M.ck3) + SUM(M.ck4) + SUM(M.ck5) + SUM(M.ck6) + SUM(M.ck7) + SUM(M.ck8) + SUM(M.ck9) + SUM(M.ck10) + SUM(M.ck11) + SUM(M.ck12) AS tot
FROM (
	SELECT S.subject, S.dsp_order, IF(S.target LIKE "%2019-01%", S.ck1, 0) AS ck1, IF(S.target LIKE "%2019-02%", S.ck1, 0) AS ck2, IF(S.target LIKE "%2019-03%", S.ck1, 0) AS ck3, IF(S.target LIKE "%2019-04%", S.ck1, 0) AS ck4, IF(S.target LIKE "%2019-05%", S.ck1, 0) AS ck5, IF(S.target LIKE "%2019-06%", S.ck1, 0) AS ck6, IF(S.target LIKE "%2019-07%", S.ck1, 0) AS ck7, IF(S.target LIKE "%2019-08%", S.ck1, 0) AS ck8, IF(S.target LIKE "%2019-09%", S.ck1, 0) AS ck9, IF(S.target LIKE "%2019-10%", S.ck1, 0) AS ck10, IF(S.target LIKE "%2019-11%", S.ck1, 0) AS ck11, IF(S.target LIKE "%2019-12%", S.ck1, 0) AS ck12
	FROM (
		SELECT DD.code_name AS subject, A.ck1, A.target, DD.dsp_order
		FROM (
			SELECT code_name, s_code, dsp_order
			FROM sub_code
			WHERE s_code IN ('C108','C131','C133','C135','C136','C137','C179','C180','C184','C186','C187','C191','C203','C204','C205')
		) DD
		LEFT JOIN (
			SELECT COUNT(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date AS target
			FROM biz_counsel C
			WHERE 1=1
			GROUP BY C.asso_code, C.csl_date
		) A ON A.asso_code = DD.s_code
	) S
) M
GROUP BY M.subject
ORDER BY M.dsp_order

/* 월별통계 - 소속:서울시 */
SELECT M.subject, SUM(M.ck1) AS ck1, SUM(M.ck2) AS ck2, SUM(M.ck3) AS ck3, SUM(M.ck4) AS ck4, SUM(M.ck5) AS ck5, SUM(M.ck6) AS ck6, SUM(M.ck7) AS ck7, SUM(M.ck8) AS ck8, SUM(M.ck9) AS ck9, SUM(M.ck10) AS ck10, SUM(M.ck11) AS ck11, SUM(M.ck12) AS ck12, SUM(M.ck1) + SUM(M.ck2) + SUM(M.ck3) + SUM(M.ck4) + SUM(M.ck5) + SUM(M.ck6) + SUM(M.ck7) + SUM(M.ck8) + SUM(M.ck9) + SUM(M.ck10) + SUM(M.ck11) + SUM(M.ck12) AS tot
FROM (
	SELECT S.subject, S.dsp_order, IF(S.target LIKE "%2019-01%", S.ck1, 0) AS ck1, IF(S.target LIKE "%2019-02%", S.ck1, 0) AS ck2, IF(S.target LIKE "%2019-03%", S.ck1, 0) AS ck3, IF(S.target LIKE "%2019-04%", S.ck1, 0) AS ck4, IF(S.target LIKE "%2019-05%", S.ck1, 0) AS ck5, IF(S.target LIKE "%2019-06%", S.ck1, 0) AS ck6, IF(S.target LIKE "%2019-07%", S.ck1, 0) AS ck7, IF(S.target LIKE "%2019-08%", S.ck1, 0) AS ck8, IF(S.target LIKE "%2019-09%", S.ck1, 0) AS ck9, IF(S.target LIKE "%2019-10%", S.ck1, 0) AS ck10, IF(S.target LIKE "%2019-11%", S.ck1, 0) AS ck11, IF(S.target LIKE "%2019-12%", S.ck1, 0) AS ck12
	FROM (
		SELECT DD2.oper_name AS subject, A.ck1, A.target, DD.dsp_order
		FROM (
			SELECT code_name, s_code, dsp_order
			FROM sub_code
			WHERE s_code IN ('C131')
		) DD
		LEFT JOIN (
			SELECT oper_name, s_code, oper_id
			FROM operator
			WHERE oper_auth_grp_id = "GRP013"
		) DD2 ON DD.s_code = DD2.s_code
		LEFT JOIN (
			SELECT COUNT(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date AS target
			FROM biz_counsel C
			WHERE 1=1
			GROUP BY C.asso_code, C.oper_id, C.csl_date
		) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id
	) S
) M
GROUP BY M.subject
ORDER BY M.dsp_order

