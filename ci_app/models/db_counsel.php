<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// db_admin Class
require_once 'db_admin.php';



/**
 * 상담(개인) DB 처리 전용 Class
 *
 * @author dylan
 *
 */
class Db_counsel extends Db_admin {
		
	
	/**
	 * Construct
	 * 
	 */
	function __construct() {
		parent::__construct();
		
	}

	
	/**
	 * 상담 목록 조회
	 *
	 *	[권한] <br>
	 *	서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건) <br>
	 * 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능 <br>
	 * 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능<br>
	 * 자치구 공무원 : 소속 자치구의 옴부즈만 상담만 조회,인쇄 가능 - 2016.08.09 추가<br>
	 *
	 * <호출> <br>
	 * 1.상담목록 <br>
	 * 2.엑셀다운로드(구분자 is_excel=1)
	 * 
	 * @param array $args
	 * @return array
	 */
	public function get_counsel_list($args) {
		
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		// 소속코드 : asso_code - 자신이 속한 소속의 상담만 가져온다.
		// 고도화 - 위 조건 변경됨
		// - 서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건)
		// - 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능
		// - 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능
		$where = '1=1 ';

		// 주요상담사례 게시판에서 호출한 경우 - 공유된 전체 상담만 가져옴(권한과 무관)
		if($args['is_board'] == 1) {
			$where .= 'AND C.csl_share_yn = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
		}
		// master 제외 모든 상담자 대상
		else if($args['is_master'] != 1) {

			// 권한 부분 쿼리
			$where .= self::_get_counsel_comm_query();
		}

		$key = $this->get_key();

		// 검색 시작 ------------------------------------------------------
		$search = '';
		$keyword = $args['keyword'];
		$is_sch_oper = FALSE;
		
		// 상담방법 : csl_way
		if($args['csl_way']) {
			$search .= 'AND C.s_code = "'. $args['csl_way'] .'" ';
		}

		// 상담일 : search_date_begin, search_date_end
		// - 양쪽 다 있는 경우
		if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
			$search .= 'AND C.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
			$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND C.csl_date>="'. $args['search_date_begin'] .'" ';
		}

		// 검색
		// 수정 : 직종,업종 처리 추가 2017.02.10 dylan
		// 직종
		if($args['target'] == 'counsel_work_kind') {
			if($args['target_work_kind'] != '') {
				$search .= 'AND C.work_kind = "'. $args['target_work_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND INSTR(C.work_kind_etc,"'. $keyword .'")>0 ';
			}
		}
		// 업종
		else if($args['target'] == 'counsel_comp_kind') {
			if($args['target_comp_kind'] != '') {
				$search .= 'AND C.comp_kind = "'. $args['target_comp_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND INSTR(C.comp_kind_etc,"'. $keyword .'")>0 ';
			}
		}
		// 주제어 
		else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') { 
				$search .= 'AND C.seq IN (
					SELECT TCS.csl_seq 
					FROM '. $this->tbl_counsel_sub .' TCS 
					INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND TCS.s_code="'. $args['target_keyword'] .'"
				) ';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$search .= 'AND C.seq IN (
					SELECT TCS.csl_seq 
					FROM '. $this->tbl_counsel_sub .' TCS 
					INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'" 
					GROUP BY TCS.csl_seq
				) ';
			}
		}
		// 처리결과
		else if($args['target'] == 'counsel_proc_rst') {
			if($args['target_csl_proc_rst'] != '') {
				$search .= 'AND C.csl_proc_rst = "'. $args['target_csl_proc_rst'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND INSTR(C.csl_proc_rst_etc,"'. $keyword .'")>0 ';
			}
		}
		// 상담자 id - 2015.11.23 요청에 의한 추가 delee
		else if($args['target'] == 'counsel_oper_id') {
			if($keyword != '') {
				$is_sch_oper = TRUE;
			}
		}
		// 상담자명 
		else if($args['target'] == 'counsel_oper_name') {
			if($keyword != '') {
				$is_sch_oper = TRUE;
			}
		}
		// 내담자명 
		else if($args['target'] == 'csl_name') {
			if($keyword != '') {
				$search .= 'AND C.csl_name=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ';
			}
		}
		// 추가 : dylan 2017.09.12, danvistory.com
		// 내담자 전화번호 뒤 4자리 검색
		else if($args['target'] == 'counsel_csl_tel') {
			if($keyword != '') {
				$search .= 'AND C.csl_tel_sch=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ';
			}
		}
		// 전체검색
		else if($args['target'] == '') {
			if($keyword != '') {
				$search .= ' AND (
					INSTR(C.csl_title,"'. $keyword .'")>0 '
					.'OR INSTR(C.csl_content,"'. $keyword .'")>0 '
					.'OR INSTR(C.csl_reply,"'. $keyword .'")>0 '
					.'OR INSTR(C.work_kind_etc,"'. $keyword .'")>0 ' // 업종 기타
					.'OR INSTR(C.comp_kind_etc,"'. $keyword .'")>0 ' // 직종 기타
					.'OR INSTR(C.csl_proc_rst_etc,"'. $keyword .'")>0 ' // 처리결과 기타
					.'OR C.csl_name=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ' //인코딩
					.'OR C.csl_tel_sch=AES_ENCRYPT("'. $keyword .'", HEX(SHA2("'. $key .'",512))) ' //인코딩, 내담자 전화번호 뒤 4자리 - 추가 : dylan 2017.09.12, danvistory.com
					.'OR C.oper_id IN (
						SELECT oper_id
						FROM 
						'. $this->tbl_oper .'
						WHERE INSTR(oper_name,"'. $keyword .'") > 0 || INSTR(oper_id,"'. $keyword .'") > 0
					)
				) ';// csl_tel -> csl_tel_sch 변경됨. csl_tel의 암호화적용으로 검색용으로 csl_tel_sch 필드 추가함. 2019.07.28
			}
		}
		else {
			if($keyword != '') {
				$search .= 'AND INSTR('. $args['target'] .',"'. $keyword .'")>0 ';
			}
		}		
		// 검색 끝 ------------------------------------------------------

		$rstRtn = ['data'=>[], 'tot_cnt'=>0];
		
		// 검색상태 여부
		$is_search = $search == '' && $is_sch_oper == FALSE ? FALSE : TRUE;

		// - 엑셀다운로드 외
		$table = 'v_counsel_list';
		$columns = 'V.seq,V.csl_title,V.csl_date,V.oper_name,V.asso_name,V.csl_way';
		// - 엑셀다운로드(상담목록, 통계는 db_admin 클래스에서 처리한다.)
		if(isset($args['is_excel'])) {
			$table = 'v_counsel_list_search'; // - 검색, 엑셀다운로드에서 호출
			$columns = 'V.seq,V.csl_title,V.csl_date,V.oper_name,V.asso_name,V.csl_way,V.csl_content,V.csl_reply';
		}

		// - 검색상태시 검색용 뷰 사용
		if($is_search === TRUE) {
			// - 운영자 검색인 경우
			if($is_sch_oper === TRUE) {
				$q1 = 'SELECT '. $columns .'
					FROM (
						SELECT C.seq
						FROM (
							SELECT oper_id /* operator 검색을 우선 수행한다. */
							FROM 
							'. $this->tbl_oper .'
							WHERE INSTR(oper_name,"'. $keyword .'")>0 OR INSTR(oper_id,"'. $keyword .'")>0
						) I
						INNER JOIN '. $this->tbl_counsel .' C ON I.oper_id=C.oper_id '
						.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
						.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
						.'INNER JOIN '. $this->tbl_sub_code .' S3 ON C.work_kind = S3.s_code ' // 직종
						.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
						.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code '
						.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id 
					WHERE '. $where . $search .'
					ORDER BY C.csl_date DESC, C.seq DESC';
				$q2 = ' LIMIT '. $offset .', '. $limit;
				$q3 = ') T
					INNER JOIN v_counsel_list_search V ON T.seq=V.seq';
				// 개수
				$q = 'SELECT count(*) as cnt 
					FROM (
						SELECT oper_id /* operator 검색을 우선 수행한다. */
						FROM 
						'. $this->tbl_oper .'
						WHERE INSTR(oper_name,"'. $keyword .'")>0 OR INSTR(oper_id,"'. $keyword .'")>0
					) I
					INNER JOIN '. $this->tbl_counsel .' C ON I.oper_id=C.oper_id '
					.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
					.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
					.'INNER JOIN '. $this->tbl_sub_code .' S3 ON C.work_kind = S3.s_code ' // 직종
					.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
					.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code '
					.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id
					WHERE '. $where . $search .'
					ORDER BY C.csl_date DESC, C.seq DESC';
				$rs = $this->db->query($q);
				$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
			}
			// - 운영자외 전체 검색
			else {
				$q1 = 'SELECT '. $columns .'
					FROM (
						SELECT C.seq
						FROM '. $this->tbl_counsel .' C '
						.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
						.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
						.'INNER JOIN '. $this->tbl_sub_code .' S3 ON C.work_kind = S3.s_code ' // 직종
						.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
						.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
						.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code
					WHERE '. $where . $search .'
					ORDER BY C.csl_date DESC, C.seq DESC';
				$q2 = ' LIMIT '. $offset .', '. $limit;
				$q3 = ') T
					INNER JOIN v_counsel_list_search V ON T.seq=V.seq';
				// 개수
				$q = 'SELECT count(*) as cnt 
					FROM '. $this->tbl_counsel .' C '
					.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
					.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
					.'INNER JOIN '. $this->tbl_sub_code .' S3 ON C.work_kind = S3.s_code ' // 직종
					.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
					.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
					.'INNER JOIN '. $this->tbl_sub_code .' SC ON C.asso_code=SC.s_code
					WHERE '. $where . $search.' ';
				$rs = $this->db->query($q);
				$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
			}
			$q = $q1 . $q2 . $q3;
			if(isset($args['is_excel'])) {// 엑셀다운로드 - limit 제거
				$q = $q1 . $q3;
			}
// 			echof($q);
			$rs = $this->db->query($q);
			$rstRtn['data'] = $rs->result();
		}
		// - 검색상태 아니면 단순 뷰 사용
		else {
			$q1 = 'SELECT '. $columns .'
				FROM (
					SELECT C.seq
					FROM '. $this->tbl_counsel .' C
					INNER JOIN '. $this->tbl_oper .' O ON C.oper_id=O.oper_id 
					WHERE '. $where .' 
					ORDER BY C.csl_date DESC, C.seq DESC';
			$q2 = ' LIMIT '. $offset .', '. $limit;
			$q3 = ') T
				INNER JOIN '. $table .' V ON T.seq=V.seq';
			
			$q = $q1 . $q2 . $q3;
			if(isset($args['is_excel'])) {// 엑셀다운로드 - limit 제거
				$q = $q1 . $q3;
			}
// 			echof($q);
			$rs = $this->db->query($q);
			$rstRtn['data'] = $rs->result();
			// 개수
			$q = 'SELECT count(*) as cnt FROM '. $this->tbl_counsel .' C
				INNER JOIN '. $this->tbl_oper .' O ON C.oper_id=O.oper_id /*권한관련으로 필요함*/
				WHERE '. $where;
			$rs = $this->db->query($q);
			$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
		}

		return $rstRtn;
	}

	
	/**
	 * 저장
	 *
	 * @param array $args
	 * @return array
	 */
	public function add_counsel($args) {
		$args['reg_date'] = get_date();
		$args['regular'] = 1;

		$csl_sub_code = $args['csl_sub_code'];
		unset($args['csl_sub_code']);
		unset($args['is_del_file']);
		
		// 암호화필드 처리
		$csl_name = $args['csl_name'];
		unset($args['csl_name']);
		if($csl_name) {
			$this->db->set('csl_name', $csl_name, FALSE);
		}
		$csl_tel = $args['csl_tel'];
		unset($args['csl_tel']);
		if($csl_tel) {
			$this->db->set('csl_tel', $csl_tel, FALSE);
		}
		$csl_tel_sch = $args['csl_tel_sch'];
		unset($args['csl_tel_sch']);
		if($csl_tel_sch) {
			$this->db->set('csl_tel_sch', $csl_tel_sch, FALSE);
		}

		$rs = $this->db->insert($this->tbl_counsel, $args);
		$new_id = $this->db->insert_id();
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;

			// 상담유형,주제어 저장
			// - 고용형태
			$csl_sub_code['csl_seq'] = $new_id;
			self::_insert_csl_sub_code($csl_sub_code);
		}
		
		return $rstRtn;
	}
	

	/**
	 * 업데이트
	 *
	 * @param array $args
	 * @return array
	 */
	public function edit_counsel($args) {
		$args['mod_date'] = get_date();
		
		$is_del_file = $args['is_del_file'];
		unset($args['is_del_file']);
		if($is_del_file != 1) {
			$csl_sub_code = $args['csl_sub_code'];
			unset($args['csl_sub_code']);
		}
		// 암호화필드 처리
		$csl_name = $args['csl_name'];
		unset($args['csl_name']);
		if($csl_name) {
			$this->db->set('csl_name', $csl_name, FALSE);
		}
		$csl_tel = $args['csl_tel'];
		unset($args['csl_tel']);
		if($csl_tel) {
			$this->db->set('csl_tel', $csl_tel, FALSE);
		}
		$csl_tel_sch = $args['csl_tel_sch'];
		unset($args['csl_tel_sch']);
		if($csl_tel_sch) {
			$this->db->set('csl_tel_sch', $csl_tel_sch, FALSE);
		}
		
		$where['seq'] = $args['seq'];

		$rs = $this->db->where($where)->update($this->tbl_counsel, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 파일 삭제가 아닌 경우
			if($is_del_file != 1) {
				// 상담유형,주제어 저장 - 기존 코드 삭제후 다시 저장한다.
				// - 기존 코드 삭제
				$this->db->delete($this->tbl_counsel_sub, array('csl_seq'=>$args['seq']));
				// - 새로 저장
				$csl_sub_code['csl_seq'] = $args['seq'];
				self::_insert_csl_sub_code($csl_sub_code);
			}
		}
		
		return $rstRtn;
	}
		

	/**
	 * 관련상담 업데이트
	 *
	 *  "관련상담" 처리 로직 <br>
	 *  원글과 원글 가져와 작성된 상담1, 2, ...n 모두 공유항목을 업데이트 처리한다. <br>
	 *  원글을 가져와 작성한 상담2,3,...n을 다시 가져와 작성한 경우도 마찬가지로 처리한다.
	 *
	 * @param array $args
	 * @return array
	 */
	public function edit_counsel_ref($args) {
		
		$args['mod_date'] = get_date();

		// 1.원글의 csl_ref_seq 컬럼 업데이트
		$where = array(
			'seq' => $args['csl_ref_seq']
		);
		$data = array(
			'csl_ref_seq' => $args['csl_ref_seq']
		);

		// 암호화필드 처리
		$csl_name = $args['csl_name'];
		unset($args['csl_name']);
		if($csl_name) {
			$this->db->set('csl_name', $csl_name, FALSE);
		}
		$csl_tel = $args['csl_tel'];
		unset($args['csl_tel']);
		if($csl_tel) {
			$this->db->set('csl_tel', $csl_tel, FALSE);
		}
		$csl_tel_sch = $args['csl_tel_sch'];
		unset($args['csl_tel_sch']);
		if($csl_tel_sch) {
			$this->db->set('csl_tel_sch', $csl_tel_sch, FALSE);
		}
		
		$this->db->where($where)->update($this->tbl_counsel, $data);
		
		
		// 2.모든 csl_ref_seq 이 같은 상담 업데이트
		$data = array(
			'gender' => $args['gender']
			,'ages' => $args['ages']
			,'ages_etc' => $args['ages_etc']
			,'live_addr' => $args['live_addr']
			,'live_addr_etc' => $args['live_addr_etc']
			,'work_kind' => $args['work_kind']
			,'work_kind_etc' => $args['work_kind_etc']
			,'comp_kind' => $args['comp_kind']
			,'comp_addr' => $args['comp_addr']
			,'comp_addr_etc' => $args['comp_addr_etc']
			,'emp_kind' => $args['emp_kind']
			,'emp_kind_etc' => $args['emp_kind_etc']
			,'emp_cnt' => $args['emp_cnt']
			,'emp_cnt_etc' => $args['emp_cnt_etc']
			,'emp_paper_yn' => $args['emp_paper_yn']
			,'emp_insured_yn' => $args['emp_insured_yn']
			,'ave_pay_month' => $args['ave_pay_month']
			,'work_time_week' => $args['work_time_week']
		);
		$where = array(
			'csl_ref_seq' => $args['csl_ref_seq']
		);
		if($csl_name) {
			$this->db->set('csl_name', $csl_name, FALSE);
		}
		if($csl_tel) {
			$this->db->set('csl_tel', $csl_tel, FALSE);
		}
		if($csl_tel_sch) {
			$this->db->set('csl_tel_sch', $csl_tel_sch, FALSE);
		}
		$this->db->where($where)->update($this->tbl_counsel, $data);

		return true;
	}


	/**
	 * 상담 데이터 조회
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_counsel($args) {

		$key = $this->get_key();
		
		$where = 'C.seq = '. $args['seq'] .' ';
		
		// 상담 작성자 정보를 operator 테이블에서 가져오도록 수정 - 20151228 dylan
		$fields = "C.seq, C.oper_id, C.asso_code
			,AES_DECRYPT(C.csl_name, HEX(SHA2('$key',512))) as csl_name
			,AES_DECRYPT(C.csl_tel, HEX(SHA2('$key',512))) as csl_tel
			,AES_DECRYPT(C.csl_tel_sch, HEX(SHA2('$key',512))) as csl_tel_sch
			,C.csl_title, C.csl_content, C.csl_date, C.s_code, C.s_code_etc, C.csl_motive_cd, C.csl_motive_etc, C.gender
			,C.ages, C.ages_etc, C.live_addr, C.live_addr_etc, C.work_kind, C.work_kind_etc, C.comp_kind, C.comp_kind_etc
			,C.comp_addr, C.comp_addr_etc, C.emp_kind, C.emp_kind_etc, C.emp_use_kind, C.emp_cnt, C.emp_cnt_etc
			,C.emp_paper_yn, C.emp_insured_yn, C.ave_pay_month, C.work_time_week, C.csl_ref_seq, C.csl_reply
			,C.csl_proc_rst, C.csl_proc_rst_etc, C.csl_share_yn, C.reg_date 
			,S.code_name as asso_name, O.oper_name,O.oper_kind,O.oper_auth_grp_id,O.s_code as asso_cd ";
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_counsel .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.asso_code = S.s_code '
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();

		$rstRtn['data'] = $rst;
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 고용형태,상담유형,주제어 가져오기
			$query = 'SELECT CONCAT(GROUP_CONCAT(s_code)) as s_code FROM '. $this->tbl_counsel_sub .' WHERE csl_seq='. $args['seq'] .' GROUP BY csl_seq';
			// query
			$rs2 = $this->db->query($query);
			$tmp2 = $rs2->result();
			$rstRtn['data']['csl_sub_code'] = '';
			if($this->db->affected_rows() > 0) {
				$rstRtn['data']['csl_sub_code'] = $tmp2[0]->s_code;
			}
		}
		
		return $rstRtn;
	}


	/**
	 * 상담 데이터 조회
	 * 
	 * 통계->상담사례에서 상담보기 팝업에서 사용
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_counsel_at($args) {
		$rs = $this->db->select('*')
			->from($this->tbl_counsel)
			->join($this->tbl_oper, $this->tbl_oper .'.oper_id='. $this->tbl_counsel .'.oper_id')
			->where($args)->get();

		$rst = $rs->result();

		$rstRtn = '';
		if(count($rst)>0) {
			$rstRtn = $rst[0];
		}

		return $rstRtn;
	}


	/**
	 * 상담 조회 - 인쇄용
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_counsel_print($args) {

		$where = 'C.seq = '. $args['seq'] .' ';
		
		$fields = 'C.*, O.oper_name '
			.',S.code_name as asso_name '
			.',S1.code_name as csl_method '
			.',S2.code_name as gender '
			.',S3.code_name as ages '
			.',S4.code_name as live_addr '
			.',S5.code_name as work_kind ' 
			.',S6.code_name as comp_kind '
			.',S7.code_name as comp_addr '
			.',S8.code_name as emp_kind '
			.',S9.code_name as emp_cnt '
			.',S10.code_name as emp_paper_yn '
			.',S11.code_name as emp_insured_yn '
			.',S12.code_name as csl_proc_rst '
			.',S13.code_name as csl_share_yn '
			.',S14.code_name as csl_motive '
			.',(SELECT GROUP_CONCAT(Z.code_name) '
			.'FROM '. $this->tbl_counsel_sub .' Q '
			.'INNER JOIN '. $this->tbl_sub_code .' Z ON Z.s_code=Q.s_code '
			.'WHERE Q.csl_seq=C.seq) as csl_kind ';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_counsel .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.asso_code ' // 소속code
			.'LEFT JOIN '. $this->tbl_sub_code .' S1 ON S1.s_code = C.s_code ' // 상담방법
			.'LEFT JOIN '. $this->tbl_sub_code .' S2 ON S2.s_code = C.gender ' // 성별
			.'LEFT JOIN '. $this->tbl_sub_code .' S3 ON S3.s_code = C.ages ' // 연령대
			.'LEFT JOIN '. $this->tbl_sub_code .' S4 ON S4.s_code = C.live_addr ' // 거주지code
			.'LEFT JOIN '. $this->tbl_sub_code .' S5 ON S5.s_code = C.work_kind ' // 직종
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.comp_kind ' // 업종
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.comp_addr ' // 업종
			.'LEFT JOIN '. $this->tbl_sub_code .' S8 ON S8.s_code = C.emp_kind ' // 고용형태 
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.emp_cnt ' // 근로자수 
			.'LEFT JOIN '. $this->tbl_sub_code .' S10 ON S10.s_code = C.emp_paper_yn ' // 근로계약서 작성 여부 
			.'LEFT JOIN '. $this->tbl_sub_code .' S11 ON S11.s_code = C.emp_insured_yn ' // 4대보험가입 여부
			.'LEFT JOIN '. $this->tbl_sub_code .' S12 ON S12.s_code = C.csl_proc_rst ' // 상담 처리결과 
			.'LEFT JOIN '. $this->tbl_sub_code .' S13 ON S13.s_code = C.csl_share_yn ' // 상담내역공유
			.'LEFT JOIN '. $this->tbl_counsel_sub .' Q ON Q.csl_seq = C.seq ' // 상담유형 서브테이블
			.'LEFT JOIN '. $this->tbl_sub_code .' S14 ON S14.s_code = C.csl_motive_cd ' // 상담이용동기 - 추가 2016.08.02
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();

		$rstRtn['data'] = array();
		if(count($rst) > 0) {
			$rstRtn['data'] = (array)$rst[0];
		}

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}


	/**
	 * 다중 삭제
	 *
	 * @param array $args
	 * @return int 삭제 실패한 key
	 */
	public function del_multi_counsel($args) {

		$exist_seq = '';

		// 참조된 게시물이 있는지 검사하여 있으면 삭제하지 않도록 한다.
		if($args['check_ref'] === TRUE) {
			$seqs = $args['seqs'];
			$arr_seq = explode(CFG_AUTH_CODE_DELIMITER, $seqs);
			
			foreach($arr_seq as $id) {
				$rst = $this->db->select('seq')->get_where($this->tbl_counsel, array('csl_ref_seq'=>$id));

				if($rst->num_rows > 0) {
					if($exist_seq != '') $exist_seq .= CFG_AUTH_CODE_DELIMITER;
					$exist_seq .= $id;
				}
				else {
					// 첨부파일 정보 가져오기
					$rst = $this->db->select('file_name')->get_where($this->tbl_counsel, array('seq'=>$id));
					$file_data = $rst->result();
					foreach ($file_data as $key => $fnames->file_name) {
						if($fnames->file_name) {
							$arr_fname = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $fnames);
							foreach ($arr_fname as $key => $fname) {
								if($fname) {
									@unlink(CFG_UPLOAD_PATH . $fname);
									clearstatcache();
								}
							}
						}
					}
					
					// 서브 테이블 삭제
					$query = 'DELETE FROM '. $this->tbl_counsel_sub .' WHERE csl_seq IN ("'. $id .'") ';
					$rst = $this->db->query($query);

					// 게시물 삭제
					$query = 'DELETE FROM '. $this->tbl_counsel .' WHERE seq IN ("'. $id .'") ';
					$rst = $this->db->query($query);
				}
			}
		}
		else {
			$seqs = str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['seqs']);
			// 첨부파일 정보 가져와 삭제처리
			$query = 'SELECT file_name FROM '. $this->tbl_counsel .' WHERE seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);
			$file_data = $rst->result();
			foreach ($file_data as $key => $fnames) {
				if($fnames->file_name) {
					$arr_fname = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $fnames->file_name);
					foreach ($arr_fname as $key => $fname) {
						if($fname) {
							@unlink(CFG_UPLOAD_PATH . $fname);
							clearstatcache();
						}
					}
				}
			}
					
			// 서브 테이블 삭제
			$query = 'DELETE FROM '. $this->tbl_counsel_sub .' WHERE csl_seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);

			// 게시물 삭제
			$query = 'DELETE FROM '. $this->tbl_counsel .' WHERE seq IN ("'. $seqs .'") ';
			$rst = $this->db->query($query);
		}
		
		return $exist_seq;
	}


	/**
	 * 상담 테이블에 저장된 상담 년도만 추출해 리턴
	 *
	 * @return array
	 */
	public function get_counsel_year() {
	
		return parent::get_counsel_year('counsel');
	}
	
	/*
	//-----------------------------------------------------------------------------------------------------------------
	// del_counsel : counsel 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_counsel($args) {
		$where = array(
			'csl_ref_seq' => $args['seq']
		);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		
		// 참조된 게시물이 있는지 검사하여 있으면 삭제하지 않도록 한다.
		if($args['check_ref'] === TRUE) {

			// 운영자에게 할당된 내용이 있으면 삭제하지 못한다.
			$rs = $this->db->select('seq')->get_where($this->tbl_counsel, $where);
			
			// echof($rs->num_rows);

			if($rs->num_rows > 0) {
				$rstRtn['rst'] = 'exist';
				$rstRtn['msg'] = 'exist group id on DB';
			}
			else {
				$this->db->delete($this->tbl_oper, $where);
				
				if($this->db->affected_rows() > 0) {
					$rstRtn['rst'] = 'succ';
					$rstRtn['msg'] = 'ok';
				}
			}
		}
		// 그냥 삭제 처리한다.
		else {
			$this->db->delete($this->tbl_counsel, $where);
			
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
		
		return $rstRtn;		
	}
	*/
	

	/**
	 * 상담 insert, update 시 고용형태,상담유형,주제어 테이블 처리
	 *
	 * @param array $args
	 * @return array
	 */
	private function _insert_csl_sub_code($args) {
		// - 고용형태 - 중복체크 가능하게 요청 뒤 원복 요청으로 주석처리
		// if(count($args['arr_emp_kind']) > 0) {
		// 	// 추가된 counsel 테이블의 row id 추가
		// 	for($i=0; $i<count($args['arr_emp_kind']); $i++) {
		// 		$args['arr_emp_kind'][$i]['csl_seq'] = $args['csl_seq'];
		// 	}
		// 	$this->db->insert_batch($this->tbl_counsel_sub, $args['arr_emp_kind']);
		// }

		// - 상담유형
		if(count($args['arr_csl_kind']) > 0) {
			$data = array();
			// 추가된 counsel 테이블의 row id 추가
			foreach ($args['arr_csl_kind'] as $key => $item) {
				$decode = base64_decode($item);
				$data[$key]['csl_seq'] = $args['csl_seq'];
				$data[$key]['s_code'] = $decode;
				// 통계용 dsp_code를 가져와 저장한다.
				$data[$key]['dsp_code'] = $this->db->select('dsp_code')->get_where($this->tbl_sub_code, ['s_code'=>$decode])->result()[0]->dsp_code;
			}
			$this->db->insert_batch($this->tbl_counsel_sub, $data);
			unset($data['s_code']);
		}
		// - 주제어
		if(count($args['arr_keyword']) > 0) {
			$data = array();
			// 추가된 counsel 테이블의 row id 추가
			foreach ($args['arr_keyword'] as $key => $item) {
				$decode = base64_decode($item);
				$data[$key]['csl_seq'] = $args['csl_seq'];
				$data[$key]['s_code'] = $decode;
				// 통계용 dsp_code를 가져와 저장한다.
				$data[$key]['dsp_code'] = $this->db->select('dsp_code')->get_where($this->tbl_sub_code, ['s_code'=>$decode])->result()[0]->dsp_code;
			}
			$this->db->insert_batch($this->tbl_counsel_sub, $data);
		}
	}
	
}