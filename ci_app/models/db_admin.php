
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * DB Model 최상위 Class
 * 
 * @author dylan
 */
class Db_Admin extends CI_Model {

	// < 테이블명 변수>
	// 개인상상담
	var $tbl_counsel = 'counsel';
	var $tbl_counsel_sub = 'counsel_sub';
	
	// 운영자관리
	var $tbl_oper = 'operator';
	
	// 권한관리
	var $tbl_auth_grp = 'oper_auth_grp';
	
	// 코드관리
	var $tbl_master_code = 'master_code';
	var $tbl_sub_code = 'sub_code';
	var $tbl_sub_code_desc = 'sub_code_desc';//코드설명글 테이블-추가 dylan 2019.07.05
	
	// 게시판
	var $tbl_brd_faq = 'brd_faq';
	var $tbl_brd_oper = 'brd_operation';

	// 권리구제지원
	var $tbl_lawhelp = 'lawhelp';
	var $tbl_lawhelp_invtgt_date = 'lawhelp_invtgt_date';
	var $tbl_lawhelp_kind_sub_cd = 'lawhelp_kind_sub_cd';

	// 사업주상담 : 추가 2019.06.10
	var $tbl_biz_counsel = 'biz_counsel';
	var $tbl_biz_counsel_sub = 'biz_counsel_sub';
	var $tbl_config = 'config';
	
	// 공통
	var $model = NULL;
	var $offset = 0;
	var $limit = CFG_MAIN_PAGINATION_ITEM_PER_PAGE;
	
	
	
	/**
	 * Construct
	 * 
	 */
	function __construct() {
		parent::__construct();
		
  }
	
	
  /**
   * 로그인 처리 <공통> 
   * 
   * @param array $args
   * @return string
   */
	public function get_login($args) {
		$rstRtn['rst'] = '';
		$rstRtn['msg'] = '';
		$rstRtn['oper_id'] = '';
		$rstRtn['oper_name'] = '';
		$rstRtn['oper_auth_grp_id'] = '';
		$rstRtn['oper_auth_codes'] = '';

		$fields ='OP.oper_kind, OP.oper_kind_sub, OP.oper_id, OP.oper_auth_grp_id, OP.oper_name, OP.use_yn, OP.s_code as asso_cd'
			.', GP.oper_auth_codes, S.code_name as asso_nm';
		$rs = $this->db->query('SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_oper .' OP '
			.'INNER JOIN '. $this->tbl_auth_grp .' GP ON OP.oper_auth_grp_id=GP.oper_auth_grp_id '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON OP.s_code=S.s_code '
			.'WHERE OP.oper_id = "'. $args['oper_id'] .'" AND OP.oper_pwd="'. dv_hash($args['oper_pwd'], CFG_ENCRYPT_KEY) .'"');
		$rst = $rs->result();

		if(count($rst) >= 1) {
			if($rst[0]->use_yn == 0) {
				$rstRtn['msg'] = 'err_login_02';
			}
			else {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = '';
				$rstRtn['oper_id'] = $rst[0]->oper_id;
				$rstRtn['oper_name'] = $rst[0]->oper_name;
				$rstRtn['oper_kind'] = $rst[0]->oper_kind;
				$rstRtn['oper_kind_sub'] = $rst[0]->oper_kind_sub;
				$rstRtn['oper_auth_grp_id'] = base64_encode($rst[0]->oper_auth_grp_id);
				$rstRtn[CFG_SESSION_ADMIN_AUTH_ASSO_CD] = $rst[0]->asso_cd;
				$rstRtn[CFG_SESSION_ADMIN_AUTH_ASSO_NM] = $rst[0]->asso_nm;
				
				// 권한코드 첫번째 코드
				$first_enc_code = '';

				// 암호화 하기 전 구분자로 나눈뒤 각 코드를 개별암호화하여 구분자로 붙여 보낸다.
				$codes = $rst[0]->oper_auth_codes;
				if($codes) {
					$tmp = explode(CFG_AUTH_CODE_DELIMITER, $codes);
					$tmp2 = '';
					foreach($tmp as $data) {
						if($tmp2 != '') {
							$tmp2 .= CFG_AUTH_CODE_DELIMITER;
						}
						$tmp2 .= base64_encode($data);
						//
						// 로그인 후 첫번째 로딩 페이지 용 권한코드(=서브메뉴 코드)
						if($first_enc_code == '') {
							$first_enc_code = base64_encode($data);
						}

						// 통계>상담사례 보기 권한 세팅 --> admin->login 메서드로 옮김. dylan 2019.08.05
// 						$auth_csl_view = 0;
// 						if($data == CFG_EXCEPTION_CODE_COUNSEL_STATISTIC_VIEW_AUTH) {
// 							$auth_csl_view = 1;
// 						}
// 						$this->session->set_userdata(CFG_SESSION_ADMIN_AUTH_CSL_VIEW, $auth_csl_view);
					}
					$enc_codes = $tmp2;
				}
				else {
					$enc_codes = '';
				}
				$rstRtn['oper_auth_codes'] = $enc_codes;
				$rstRtn[CFG_SESSION_ADMIN_AUTH_FIRST_CD] = $first_enc_code;
			}
		}
		else {
			$rstRtn['rst'] = 'fail';
			$rstRtn['msg'] = 'err_login_01';
		}
		
		return $rstRtn;
	}

	
	/**
	 * 
	 * 통계
	 * 
	 */
	
	/**
	 * 통계 처리 메서드
	 * 
	 * @param array $args
	 */
	public function get_sttc_list($args) {
		
		$rstRtn = array();
		
		$where = '1=1 ';

		//상담자 검색을 위한 조인문
// 		$oper_join = ' INNER JOIN operator O ON O.oper_id=C.oper_id ';
		
		// csl_date
		$where1 = '';
		if(isset($args['csl_date']) && $args['csl_date'] != '') {
			$where1 .= 'AND '. $args['csl_date'] .' ';
		}
		// asso_code 소속
		$where2 = '';
		if(isset($args['asso_code']) && $args['asso_code'] != '') {
			$where2 = 'AND C.asso_code IN (' .$args['asso_code']. ')';
		}

		// s_code 상담방법
		$where3 = '';
		if(isset($args['s_code']) && $args['s_code'] != '') {
			$where3 = 'AND C.s_code IN (' .$args['s_code']. ')';
		}

		// 상담자
		/*$where4 = '';
		if(isset($args['oper_id']) && $args['oper_id'] != '') {
			$where4 = 'AND (O.oper_id IN (' .$args['oper_id']. '))';
		}*/

		// csl_proc_rst 처리결과
		$where5 = '';
		if(isset($args['csl_proc_rst']) && $args['csl_proc_rst'] != '') {
			$where5 = 'AND C.csl_proc_rst IN (' .$args['csl_proc_rst']. ')';
		}

		// master 관리자 여부
		$is_master = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_IS_MASTER);

		// 각 쿼리별 합계 - 고용형태,상담유형 인 별도 코드 사용
		$total_counting = 'count(C.seq)';

		// 각 쿼리별 합계 쿼리
		$query_tot = '';

		
		//---------------------------------------------------------
		// # 구분 1 : 상담사례
		//---------------------------------------------------------
		if($args['kind'] == 'down01') {

			$key = $this->get_key();
			
			$where .= $where1 . $where2 . $where3 . $where5;
			//
			$select = 'SELECT C.seq, C.csl_date, S14.code_name as asso_name, O.oper_name
				,IFNULL(AES_DECRYPT(C.csl_name, HEX(SHA2("'. $key .'",512))), "") as csl_name '
				.',S4.dsp_code as csl_type, C.s_code_etc, S12.dsp_code as ages, S1.dsp_code as gender, '
				.'C.ages_etc,C.emp_cnt_etc,' // 디버깅, 2019.07.11
				.'S2.dsp_code as live_addr, C.live_addr_etc, S3.dsp_code as work_kind, C.work_kind_etc, S5.dsp_code as comp_kind, C.comp_kind_etc, '
				.'S6.dsp_code as comp_addr, C.comp_addr_etc, S8.dsp_code as emp_kind, C.emp_kind_etc, '
				// ,'S13.dsp_code as emp_use_kind, '// 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				.'S7.dsp_code as emp_cnt, S10.dsp_code as emp_paper_yn, '
				.'S9.dsp_code as emp_insured_yn, C.ave_pay_month, C.work_time_week, '
				.'S15.dsp_code as csl_proc_rst, '
				.'C.asso_code '; // 상담사례 목록에서 상담내용보기 버튼 권한 체크용

			// 상담유형 코드
			$code['csl_kind'] = self::get_sub_code_by_m_code_v2(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE);
			$csl_kind_code = '"'. str_replace(',', '","', $code['csl_kind'][0]->s_code) .'"';
			$select .= ',(
				SELECT GROUP_CONCAT(CS2.dsp_code ORDER BY CS2.dsp_code)
				FROM counsel_sub CS2
				WHERE C.seq = CS2.csl_seq AND CS2.s_code IN ('. $csl_kind_code .')
			) as csl_kind ';// 상담유형 코드만 가져온다
						
			// excel 다운로드 용 컬럼 추가
			if($args['exclude_seq'] == 1) {
				$select .= ', (SELECT GROUP_CONCAT(K2.code_name) 
					FROM '. $this->tbl_counsel_sub .' K1 
					INNER JOIN '. $this->tbl_sub_code .' K2 ON K1.s_code=K2.s_code 
					WHERE K1.csl_seq=C.seq AND K2.m_code="'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KEYWORD .'"
				) as csl_keywords, ';
					
				// 상담사례 상담보기 권한 여부
				// master OR 타 기관 상담이라도 상담보기 권한이 있으면 상담내용,결과를 노출한다.
				$auth_csl_view = $this->session->userdata(CFG_SESSION_ADMIN_AUTH_CSL_VIEW);
				// $auth_csl_view = $_SESSION[CFG_SESSION_ADMIN_AUTH_CSL_VIEW];
				if($is_master == 1 || $auth_csl_view == 1) {
					$select .= 'C.csl_content, C.csl_reply ';
				}
				else {
					$select .= '(SELECT EC.csl_content FROM '. $this->tbl_counsel .' EC WHERE C.seq=EC.seq AND C.asso_code="'. $args['oper_asso_cd'] .'") as csl_content, ';
					$select .= '(SELECT EC.csl_reply FROM '. $this->tbl_counsel .' EC WHERE C.seq=EC.seq AND C.asso_code="'. $args['oper_asso_cd'] .'") as csl_reply ';
				}
			}

			$q1 = 'FROM '. $this->tbl_counsel .' C '
				.'INNER JOIN operator O ON O.oper_id=C.oper_id '
				.'LEFT JOIN sub_code S1 ON S1.s_code=C.gender '
				.'LEFT JOIN sub_code S12 ON S12.s_code=C.ages '
				.'LEFT JOIN sub_code S2 ON S2.s_code=C.live_addr '
				.'LEFT JOIN sub_code S3 ON S3.s_code=C.work_kind '
				.'LEFT JOIN sub_code S4 ON S4.s_code=C.s_code '
				.'LEFT JOIN sub_code S5 ON S5.s_code=C.comp_kind '
				.'LEFT JOIN sub_code S6 ON S6.s_code=C.comp_addr '
				.'LEFT JOIN sub_code S7 ON S7.s_code=C.emp_cnt '
				.'LEFT JOIN sub_code S8 ON S8.s_code=C.emp_kind ' // 단일항목 체크
				// .'INNER JOIN counsel_sub CS ON C.seq = CS.csl_seq AND CS.s_code IN ('. $emp_kind_code .') '
				// .'INNER JOIN sub_code S8 ON CS.s_code = S8.s_code '
				.'LEFT JOIN sub_code S9 ON C.emp_insured_yn=S9.s_code '
				.'LEFT JOIN sub_code S10 ON C.emp_paper_yn=S10.s_code '
				// .'LEFT JOIN sub_code S13 ON S13.s_code = C.emp_use_kind ' // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
				.'LEFT JOIN sub_code S14 ON S14.s_code = C.asso_code '
				.'LEFT JOIN sub_code S15 ON S15.s_code = C.csl_proc_rst '
				.'WHERE '. $where .' ';
// 				.'GROUP BY ';
			// 수정: 조회, 엑셀다운 쿼리를 동일하지 않아 다른 결과가 나오는 문제 수정 20151228 dylan
			// if($args['exclude_seq'] != 1) { 
// 				$q .= 'C.seq, ';
			// }
// 			$q .= 'C.csl_date, C.asso_code, O.oper_name, C.csl_name, '
// 				.'S1.dsp_code, S12.dsp_code, S2.dsp_code, S3.dsp_code, '
// 				.'S4.dsp_code, S5.dsp_code, S6.dsp_code, S7.dsp_code, S8.dsp_code, '
// 				.'S9.dsp_code , S10.dsp_code'
				// .', S13.dsp_code' // 주석처리 : 2015.11.23 요청에 의해 주석 처리 delee
// 				.', C.ave_pay_month, C.work_time_week ';

			$q2 = 'ORDER BY C.csl_date DESC, C.seq DESC, C.asso_code ASC ';
				
			// page
			$q3 = '';
			if($args['exclude_seq'] != 1) {// 엑셀다운로드 제외
				$limit = CFG_CSL_STTT01_CMM_PAGINATION_ITEM_PER_REQ;
				$page = $args['page'] ? (int)$args['page'] : 1;
				$offset = ($page * $limit) - $limit;
				$q3 = 'LIMIT '. $offset .', '. $limit;
			}
			$query = $select . $q1 . $q2 . $q3;
			
			// # 쿼리 실행
			$rst = $this->db->query($query);
			$rstRtn['data'] = $rst->result();

			// 개수
			$q = 'SELECT count(*) as cnt FROM '. $this->tbl_counsel .' C
				INNER JOIN '. $this->tbl_oper .' O ON C.oper_id=O.oper_id 
				WHERE '. $where;
			$rs = $this->db->query($q);
			$rstRtn['tot_cnt'] = $rs->result()[0]->cnt;
		}
		
		
		//---------------------------------------------------------
		// 구분 11 : 교차통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down11') {

			$target_field1 = '';
			$csl_kind = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$where_omb = ''; //옴부즈만 검색
			$where_asso = '';

			//교차 가로축 검색
			if($args['sel_col'] == 0){
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY, TRUE); //상담방법
				$target_field1 = 'C.s_code';
			}
			elseif($args['sel_col'] == 1) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER, TRUE); //성별
				$target_field1 = 'C.gender';
			}
			elseif($args['sel_col'] == 2) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES, TRUE); //연령대
				$target_field1 = 'C.ages';
			}
			elseif($args['sel_col'] == 3) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE); //거주지
				$target_field1 = 'C.live_addr';
			}
			elseif($args['sel_col'] == 4) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE); //회사소재지
				$target_field1 = 'C.comp_addr';
			}
			elseif($args['sel_col'] == 5) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND, TRUE); //직종
				$target_field1 = 'C.work_kind';
			}
			elseif($args['sel_col'] == 6) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND, TRUE); //업종
				$target_field1 = 'C.comp_kind';
			}
			elseif($args['sel_col'] == 7) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND, TRUE); //고용형태
				$target_field1 = 'C.emp_kind';
			}
			elseif($args['sel_col'] == 8) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT, TRUE); //근로자수
				$target_field1 = 'C.emp_cnt';
			}
			elseif($args['sel_col'] == 9) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN, TRUE); //근로계약서
				$target_field1 = 'C.emp_paper_yn';
			}
			elseif($args['sel_col'] == 10) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN, TRUE); //4대보험
				$target_field1 = 'C.emp_insured_yn';
			}
			elseif($args['sel_col'] == 11) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE); //상담유형
				$target_field1 = 'CS2.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sel_col'] == 12) {
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE, TRUE); //상담동기
				$target_field1 = 'C.csl_motive_cd';
			}

			//교차 세로축 검색
			if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION) {// 소속
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
				$target_field2 = 'C.asso_code';
				if(isset($args['asso_code']) && $args['asso_code'] != '') {
					$where_asso = 'AND SC.s_code in (' .$args['asso_code']. ')';
				}
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {// 상담자
				$target_field2 = 'C.oper_id';
				$where_omb = 'AND O.oper_auth_grp_id = "GRP013" ';
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) {// 상담유형
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND;
				$target_field2 = 'CS2.s_code';
				$target_field3 = 'DT.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND) {// 직종
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND;
				$target_field2 = 'C.work_kind'; 
				$target_field3 = 'DT.work_kind'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND) {// 업종
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
				$target_field2 = 'C.comp_kind'; 
				$target_field3 = 'DT.comp_kind'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS) { // 거주지
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field2 = 'C.live_addr'; 
				$target_field3 = 'DT.live_addr'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP) { // 회사소재지
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field2 = 'C.comp_addr'; 
				$target_field3 = 'DT.comp_addr'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND) {// 고용형태
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND;
				$target_field2 = 'C.emp_kind'; 
				$target_field3 = 'DT.emp_kind'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT) { // 근로자수
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT;
				$target_field2 = 'C.emp_cnt'; 
				$target_field3 = 'DT.emp_cnt'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN) { // 근로계약서
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN;
				$target_field2 = 'C.emp_paper_yn';
				$target_field3 = 'DT.emp_paper_yn'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN) { // 4대보험
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN;
				$target_field2 = 'C.emp_insured_yn'; 
				$target_field3 = 'DT.emp_insured_yn'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_AGES) { // 연령대
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_AGES;
				$target_field2 = 'C.ages';
				$target_field3 = 'DT.ages'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_GENDER) { // 성별
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_GENDER;
				$target_field2 = 'C.gender';
				$target_field3 = 'DT.gender'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE) {// 상담동기
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE;
				$target_field2 = 'C.csl_motive_cd'; 
				$target_field3 = 'DT.csl_motive_cd'; 
			}
			else if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY) {// 상담방법
				$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY;
				$target_field2 = 'C.s_code'; 
				$target_field3 = 'DT.s_code'; 
			}

			// 상담일자 외 - 소속/성별/직종/업종/거주지+회사소재지/고용형태/근로자수/근로계약서/4대보험/연령대
			if($args['csl_code'] != 'csl_date') {

				// where - 상담일, 상담방법, 처리결과 검색
				$where .= $where1 . $where2 . $where3 . $where5 . $where_omb;

				//총계 쿼리
				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'SELECT S.subject ';
				$t3 = '';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';

					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
						$t1 .= 'CONCAT(IFNULL(SUM(M.ck'.$index.'), 0), "(", IFNULL(M.distinct_ck'.$index.', 0), ")")';
					}
					else{
						$t1 .= 'SUM(M.ck'.$index.')';
					}
					if($t_tot != '') $t_tot .= ' + ';

					$t_tot .= 'SUM(M.ck'.$index.')';
			
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;
					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
						$t2 .= ', S.distinct_ck'.$index;
					}
					if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
						if($t3 != '') $t3 .= ', ';
						$t3 .= '(SELECT count(*) as distinct_ck'.$index .' FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
							.'WHERE '.$where .' AND '.$target_field1.' = "'.$data->s_code.'" ) AS distinct_ck'.$index.' ';
					}
					$index++;
				}
				
				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ")")';
				}
				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 . $csl_kind2 .' FROM ( SELECT DD.code_name AS subject, A.ck1, A.target ';
				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t .= ', (SELECT count(*) as distinct_tot FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
					     .'WHERE '.$where.' ) AS distinct_tot ';
				}
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t .= ', '.$t3;
				}
				if($args['csl_code'] != CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {// 상담자 외
					$t .= 'FROM (SELECT code_name, s_code, dsp_code FROM sub_code ';
					$t .=' WHERE m_code = "'.$datas2.'" ) DD ';
				}
				elseif($args['csl_code'] == 'operator'){
					$t .= 'FROM (SELECT oper_name as code_name, oper_id as s_code, s_code as dsp_code FROM operator ) DD ';
				}
				$t .='LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_field2.' as target2, C.oper_id, '.$target_field1.' as target FROM counsel C '
					.'INNER JOIN operator O ON O.oper_id = C.oper_id ';

				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$t .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', '.$target_field2.' ) A ON A.target2 = DD.s_code '
					.') S ) M ';

				$query_tot = $t;
// 				echof($t);

				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
					$total = explode('(', $total);
					$total = $total[0];
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){
					$csl_kind2 = '';
					$csl_kind3 = '';
				}
				
				// 메인쿼리
				$q = '';
				$index = 1;
				$q1 = 'SELECT M.subject ';
				$q_tot = '';
				$q2 = 'SELECT S.subject ';
				foreach($datas1 as $data) {
					$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
					$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") as ck'.$index;

					if($q_tot != '') $q_tot .= ' + ';

					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

					$index++;
				}
				
				if($total != 0){	
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					//상담유형
					if($args['sel_col'] == 11){
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ") (", IFNULL('.$per.', 0),"%)")';
					}
					else{
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
					}
				}

				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.', S.dsp_code '.$csl_kind2.' FROM ( select DD.code_name AS subject, A.ck1, A.target, DD.dsp_code '.$csl_kind3;
				// 상담자 외
				if($args['csl_code'] != CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {
					$q .= ' FROM (SELECT SC.code_name, SC.s_code, SC.dsp_code '.$csl_kind4.' FROM sub_code SC ';
					if($args['sel_col'] == 11){
						$q .= 'LEFT JOIN (SELECT COUNT(*) as distinct_tot, '.$target_field2.' FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
						.'WHERE '.$where.' GROUP BY '.$target_field2.') DT ON SC.s_code = '.$target_field3.' ';
					}
					$q .= 'WHERE m_code = "'.$datas2.'" '.$where_asso.') DD ';
				}	
				//상담자
				else{	
					$q .= 'FROM (SELECT O.oper_name as code_name, O.oper_id as s_code, O.s_code as dsp_code FROM operator O WHERE 1=1 '.$where_omb.') DD ';
				}
				$q .= 'LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_field2.' as target2, '.$target_field1.' as target FROM counsel C ';
				// 상담자 검색
				if($args['csl_code'] != CFG_SUB_CODE_OF_MANAGE_CODE_OPERATOR) {
					$q .= 'INNER JOIN operator O ON O.oper_id = C.oper_id ';
				}
				//상담유형
				if($args['sel_col'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND){ 
					$q .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY '.$target_field1.', '.$target_field2.' ) A ON A.target2 = DD.s_code '
					.') S 
					) M 
					GROUP BY M.subject ORDER BY M.dsp_code ASC ';
				
				$query = $q;
// 	  	 	echof($query);

				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}

				$rstRtn['tot_cnt'] = count($rstRtn['data']);
			}
			
			/*
			 * 상담방법-상담일자,소속 형태의 교차통계를 요청에 의해 제거, 다른 코드와 동일하게 교차통계 처리함.
			 * 2019.08.21 dylan
			 * 
			// 상담일자
			else {

				// 검색일자 - 전체인 경우 빈값이다.
				if(isset($args['csl_date']) && $args['csl_date'] != '') {
					$sch_date = $args['csl_date'];
				}
				else {
					$sch_date = 'C.csl_date BETWEEN "1900-01-01 00:00:00" AND "'. get_date() .'" ';
				}

				// where - 상담일, 상담방법, 처리결과 검색
				$where .= $where1 . $where2 . $where3 . $where5;

				// 총계쿼리
				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'select S.subject ';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';
					$t1 .= 'SUM(M.ck'.$index.')';
					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'SUM(M.ck'.$index.')';
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;

					$index++;
				}

				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 .' FROM ( SELECT DD.csl_date AS subject, A.ck1, A.target '
				.'FROM (SELECT DISTINCT C.csl_date FROM counsel C '
				. 'WHERE 1=1 '.$where1.') DD '
				.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.csl_date AS target2, C.oper_id, '.$target_field1.' AS target FROM counsel C '
				.'INNER JOIN operator O ON O.oper_id = C.oper_id '
				.'WHERE ' .$where.' ' 
				.'GROUP BY '.$target_field1.', C.csl_date) A ON A.target2 = DD.csl_date '
				.') S ) M ';

				//총계 쿼리
				$query_tot = $t;
				// echof($t);
				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
					
					$total = explode('(', $total);
					$total = $total[0];
					
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}

				// 메인쿼리
				$q = '';
				$index = 1;
				$q1 = 'select M.subject ';
				$q_tot = '';
				$q2 = 'select S.subject ';
				foreach($datas1 as $data) {
					if(isset($total) && $total != 0){	
						$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
						$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") AS ck'.$index;
					}
					else{
						$q1 .= ', IFNULL(sum(M.ck'.$index.'), 0) AS ck'.$index;
					}
					if($q_tot != '') $q_tot .= ' + ';
					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;

					$index++;
				}
				
				if(isset($total) && $total != 0){	
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
				}
				else{
					$q_tot = 'IFNULL('.$q_tot.', 0)';
				}

				$q = $q1 .', '. $q_tot .' AS tot FROM ('.$q2.' FROM ( SELECT DD.csl_date AS subject, A.ck1, A.target '
					.'FROM (SELECT DISTINCT C.csl_date FROM counsel C '
					. 'WHERE 1=1 '.$where1.') DD '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.csl_date AS target2, '.$target_field1.' AS target FROM counsel C '
						.'INNER JOIN operator O ON O.oper_id = C.oper_id '
					. 'WHERE ' .$where.' ' 
						.'GROUP BY '.$target_field1.', C.csl_date ) A ON A.target2 = DD.csl_date '
						.') S ) M group by M.subject ORDER BY M.subject DESC ';
				
				$query = $q;

				// 상담일자인 경우, 데이터가 없으면 $q가 공백이라 오류가 발생하는 문제 처리
				if(empty($q)) {
					$query = 'SELECT seq FROM counsel WHERE 1=0';
					$query_tot = $query;
				}
				else {
					$query = $q;
					$query_tot = $t;
				}
				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}
			}
			*/
// 			echof($query);
			
			$rstRtn['tot_cnt'] = count($rstRtn['data']);
		}

		//---------------------------------------------------------
		// 구분 12 : 월별 통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down12') {

			$current_year = $args['sel_year'];
			$half_year = $args['half_year'];
			if($half_year == 1){
				$first = 1;
				$last = 12;
			}
			elseif($half_year == 2){
				$first = 1;
				$last = 6;
			}
			elseif($half_year == 3){
				$first = 7;
				$last = 12;
			}

			// where - 상담방법, 처리결과 검색
			$where .= $where3 . $where5;

			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = 'select S.subject '; // S.subject2
			for($month=$first; $month<=$last; $month++) {
				if($month < 10) $month = '0'.$month;

				if($t1 != '') $t1 .= ', ';
				$t1 .= 'sum(M.ck'.$index.')';

				if($t_tot != '') $t_tot .= ' + ';

				$t_tot .= 'sum(M.ck'.$index.')';
				$t2 .= ', IF(S.target LIKE "%'.$current_year.'-'.$month.'%", S.ck1, 0) as ck'.$index;

				$index++;
			}
			if($args['asso_code'] == "'C131'"){
				$t = 'select '.$t1 .', '. $t_tot .' as tot FROM ('.$t2.' from ( select DD2.oper_name AS subject, A.ck1, A.target ' //DD2.oper_name AS subject2
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id ="GRP013") DD2 ON DD.s_code = DD2.s_code '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					//.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code ' //  C.oper_id,
					.') S ) M ';
			}
			else{		
				$t = 'select '.$t1 .', '. $t_tot .' as tot FROM ('.$t2.' from ( select DD.code_name AS subject, A.ck1, A.target ' //DD2.oper_name AS subject2
					.' FROM (SELECT code_name, s_code FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					/*.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_id in ('.$args['oper_id'].')) DD2 ON DD.s_code = DD2.s_code '*/
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';

				$t .= 'WHERE ' .$where.' ' 
					//.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code ' //  C.oper_id,
					.') S ) M ';
			}

			//총계 쿼리
			$query_tot = $t;
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
			}

			$q = '';
			$index = 1;
			$q1 = 'select M.subject '; // M.subject2
			$q_tot = '';
			$q2 = 'select S.subject, S.dsp_order '; // S.subject2
			for($month=$first; $month<=$last; $month++) {
				if($month < 10) $month = '0'.$month;

				$q1 .= ', sum(M.ck'.$index.') as ck'.$index;

				if($q_tot != '') $q_tot .= ' + ';

				$q_tot .= 'sum(M.ck'.$index.')';
				$q2 .= ', IF(S.target LIKE "%'.$current_year.'-'.$month.'%", S.ck1, 0) as ck'.$index;

				$index++;
			}
			if($args['asso_code'] == "'C131'"){
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.' from ( select DD2.oper_name AS subject, A.ck1, A.target, DD.dsp_order '
					.' FROM (SELECT code_name, s_code, dsp_order FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT oper_name, s_code, oper_id FROM operator WHERE oper_auth_grp_id = "GRP013") DD2 ON DD.s_code = DD2.s_code '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';
					$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, C.csl_date ) A ON A.asso_code = DD.s_code AND DD2.oper_id = A.oper_id '
					.') S ) M group by M.subject ORDER BY M.dsp_order ';
			}
			else{
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.' from ( select DD.code_name AS subject, A.ck1, A.target, DD.dsp_order '
					.' FROM (SELECT code_name, s_code, dsp_order FROM sub_code WHERE s_code in ('.$args["asso_code"].')) DD '
					.'LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, C.csl_date as target FROM counsel C ';
				$q .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.csl_date ) A ON A.asso_code = DD.s_code '
					.') S ) M group by M.subject ORDER BY M.dsp_order ';
			}

			$query = $q;
// 		echof($query);

			// # 쿼리 실행
			if(!empty($query)) {
				$rst = $this->db->query($query);
				$rstRtn['data'] = $rst->result();
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data']);

			// $rstRtn['query'] = $query .'# '. $query_tot;
			
		}

		//---------------------------------------------------------
		// 구분 13 : 기본통계
		//---------------------------------------------------------
		else if($args['kind'] == 'down13') {

			$target_field1 = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$data_suj = '';

			$t = '';
			$index = 1;
			$t1 = '';
			$t_tot = '';
			$t2 = ''; //, S.subject2 ';

			if($args['sch_kind'] == 0) {// 0.상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY, TRUE);
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 1.소속
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION, TRUE);
				$target_field1 = 'C.asso_code';
				if($args['asso_code'] == "'C131'"){
					$datas1 = self::get_oper_id_by_s_code('C131');
					$target_field1 = 'C.asso_code';
				}
			}
			elseif($args['sch_kind'] == 2) {// 2.성별
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER, TRUE);
				$target_field1 = 'C.gender';
			}
			elseif($args['sch_kind'] == 3) { // 3.연령대
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES, TRUE);
				$target_field1 = 'C.ages'; 
			}
			elseif($args['sch_kind'] == 4) { // 4.거주지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE);
				$target_field1 = 'C.live_addr'; 
			}
			elseif($args['sch_kind'] == 5) { // 5.회사소재지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE);
				$target_field1 = 'C.comp_addr'; 
			}
			elseif($args['sch_kind'] == 6) {// 6.직종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND, TRUE);
				$target_field1 = 'C.work_kind'; 
			}
			elseif($args['sch_kind'] == 7) {// 7.업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND, TRUE);
				$target_field1 = 'C.comp_kind'; 
			}
			elseif($args['sch_kind'] == 8) {// 8.고용형태
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND, TRUE);
				$target_field1 = 'C.emp_kind'; 
			}
			elseif($args['sch_kind'] == 9) { // 9.근로자수
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT, TRUE);
				$target_field1 = 'C.emp_cnt'; 
			}
			elseif($args['sch_kind'] == 10) { // 10.근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN, TRUE);
				$target_field1 = 'C.emp_paper_yn'; 
			}
			elseif($args['sch_kind'] == 11) { // 11.4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN, TRUE);
				$target_field1 = 'C.emp_insured_yn'; 
			}
			elseif($args['sch_kind'] == 12) {// 12.상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE);
				$target_field1 = 'CS2.s_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD2.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sch_kind'] == 13) {// 13.처리결과
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST, TRUE);
				$target_field1 = 'C.csl_proc_rst'; 
			}
			elseif($args['sch_kind'] == 14) {// 14.상담동기
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE, TRUE);
				$target_field1 = 'C.csl_motive_cd'; 
			}
			// where - 상담일, 상담방법, 처리결과 검색
			$where .= $where1 . $where3 . $where5;
// echof($datas1);
			foreach($datas1 as $data) {
				if($args['sch_kind'] == 1) {	//소속 검색 시, 선택한 소속만 데이터를 가져오도록 처리
					if($args['asso_code'] == "'C131'"){// 서울시
// 						if(strpos($data->oper_name, '옴부즈만') !== FALSE){//옴부즈만인 경우 -- 이부분 주석처리한 이유는 작업자가 else에 대한 처리를 안해서 서울시소속 모두 출력되게 조치함
							if($t1 != '') $t1 .= ', ';
							$t1 .= '"'.$data->oper_name. '" AS s'.$index;
							$t1 .= ', sum(M.ck'.$index.') AS t'.$index;

							if($t_tot != '') $t_tot .= ' + ';
							$t_tot .= 'sum(M.ck'.$index.')';

							if($t2 != '') $t2 .= ', ';
							$t2 .= ' IF(S.target = "'.$data->oper_id.'", S.ck1, 0) as ck'.$index;

							$index++;
// 						}
					}
					else{
						if(strpos($args['asso_code'], $data->s_code) !== FALSE){
							if($t1 != '') $t1 .= ', ';
							$t1 .= '"'.$data->code_name. '" AS s'.$index;
							$t1 .= ', sum(M.ck'.$index.') AS t'.$index;

							if($t_tot != '') $t_tot .= ' + ';
							$t_tot .= 'sum(M.ck'.$index.')';

							if($t2 != '') $t2 .= ', ';
							$t2 .= ' IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;
							$index++;
						}
					}
				}
				else{
					if($t1 != '') $t1 .= ', ';
					$t1 .= '"'.$data->code_name. '" AS s'.$index;
					$t1 .= ', sum(M.ck'.$index.') AS t'.$index;

					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'sum(M.ck'.$index.')';

					if($t2 != '') $t2 .= ', ';
					$t2 .= ' IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;

					$index++;
				}
			}
// 			echof($args['sch_kind']);
			$t1 .= ', "총계" AS s'.$index;
			if($args['sch_kind'] == 12){//통계유형:상담유형
				$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ")")';
			}
			if($args['sch_kind'] == 1 && $args['asso_code'] == "'C131'"){ // 통계유형:소속, 소속:서울시 인 경우
				$t = 'SELECT '.$t1 .', '. $t_tot .' as tot FROM ( SELECT '.$t2. ' FROM ( select A.ck1, A.target '
					.' FROM (SELECT code_name, s_code FROM sub_code ';
				if(isset($args['asso_code']) && $args['asso_code'] != '') {
					$t .= 'WHERE s_code in (' .$args['asso_code']. '))';
				}
				$t .= ' DD LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id as target FROM counsel C '
					.'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id ) A ON A.asso_code = DD.s_code '
					.') S ) M ';
					//echof($t);
			}
			else{
				$t = 'SELECT '.$t1 .', '. $t_tot .' as tot FROM ( SELECT '.$t2. $csl_kind2 .' FROM ( select A.ck1, A.target ';
				if($args['sch_kind'] == 12){
					$t .= ', (SELECT COUNT(*) as distinct_tot FROM counsel C INNER JOIN operator O ON O.oper_id = C.oper_id '
					.'WHERE '.$where. $where2.') AS distinct_tot ';
				}
				$t .=' FROM (SELECT code_name, s_code FROM sub_code ';
				if(isset($args['asso_code']) && $args['asso_code'] != '') {
					$t .= 'WHERE s_code in (' .$args['asso_code']. '))';
				}
				$t .= ' DD LEFT JOIN (SELECT count(C.seq) AS ck1, C.asso_code, C.oper_id, '.$target_field1.' as target FROM counsel C ';

				if($args['sch_kind'] == 12){
					$t .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}

				$t .= 'WHERE ' .$where.' ' 
					.'GROUP BY C.asso_code, C.oper_id, '.$target_field1.' ) A ON A.asso_code = DD.s_code '
					.') S ) M ';
			}
			//총계 쿼리
			$query_tot = $t;
// 			echof($t);
			$total = 1;
			if(!empty($query_tot)) {
				$rst = $this->db->query($query_tot);
				$rstRtn['data_tot'] = $rst->result();
				$total = $rstRtn['data_tot'][0]->tot;
				
				$total = explode('(', $total);
				$total = $total[0];
				
			}

			$rstRtn['tot_cnt'] = count($rstRtn['data_tot'][0]);
		}

		//---------------------------------------------------------
		// 구분 07 : 시계열통계 통계
		//---------------------------------------------------------
		else if(stripos($args['kind'], 'down07_') !== FALSE) {

			// 공통
			// where - 소속, 상담방법, 처리결과 검색
			$where .= $where2 . $where3 . $where5;
			
			//기준 세로축 s_code
			$datas1 = array();
			//기존 세로축 m_code
			$tg_m_code = '';
			
			//기준 세로축 데이터
			if($args['sch_kind'] == 0) {// 상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY;
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 소속
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
				$target_field1 = 'C.asso_code';
			}
			elseif($args['sch_kind'] == 2) {// 성별
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_GENDER;
				$target_field1 = 'C.gender';
			}
			elseif($args['sch_kind'] == 3) { // 연령대
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_AGES;
				$target_field1 = 'C.ages';
			}
			elseif($args['sch_kind'] == 4) { // 직종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND;
				$target_field1 = 'C.work_kind';
			}
			elseif($args['sch_kind'] == 5) { // 업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
				$target_field1 = 'C.comp_kind';
			}
			elseif($args['sch_kind'] == 6) { // 고용형태
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND;
				$target_field1 = 'C.emp_kind';
			}
			elseif($args['sch_kind'] == 7) {// 근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN;
				$target_field1 = 'C.emp_paper_yn';
			}
			elseif($args['sch_kind'] == 8) {// 4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN;
				$target_field1 = 'C.emp_insured_yn';
			}
			elseif($args['sch_kind'] == 9) { // 상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND;
				$target_field1 = 'CS.s_code';
			}
			
			// target 컬럼의 s_code 문자열로 생성
			$tg_s_code = '';
			foreach ($datas1 as $i => $item) {
				if($tg_s_code != '') {
					$tg_s_code .= '","';
				}
				$tg_s_code .= $item->s_code;
			}
			$tg_s_code = ' "'. $tg_s_code .'" ';

			// 통계 구분값: 상담유형 - 공통
			$sch_kind_cslkind = 9;
			
			// ############################
			// 구분 06 : 시계열통계 - 건수/비율
			if($args['kind'] == 'down07_1') {

				// 년도별 총계 쿼리 - 년별
				$q_tot = 'SELECT ';
				// SELECT year_2018, year_2019,
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					$q_tot .= 'year_'. $y .', dst_year_'. $y .', ';
				}
				// (year_2018 + year_2019) AS tot
				$q_tot .= '(';
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					$q_tot .= 'year_'. $y .'+';
				}
				$q_tot = substr($q_tot , 0, -1);// 끝 "+" 제거
				$q_tot .= ') AS tot
					FROM (';
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					$q_tot .= '(SELECT COUNT(*) AS year_'. $y .', COUNT(DISTINCT C.seq) AS dst_year_'. $y .'
						FROM counsel C ';
				if($args['sch_kind'] == $sch_kind_cslkind) { // 상담유형
					$q_tot .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q_tot .= 'WHERE '. $where .' ';
				if($args['sch_kind'] == $sch_kind_cslkind) { // 상담유형
					$q_tot .= 'AND CS.s_code IN ('. $tg_s_code .') ';
				}
				$q_tot .= 'AND DATE_FORMAT(csl_date, "%Y")="'. $y .'"
					) AS year_'. $y .',';
				}
				$q_tot = substr($q_tot , 0, -1);// 끝 "," 제거
				$q_tot .= '
					)';
// 				echof($q_tot);
				$rst_tot_annually = [];
				$total = 0;
				$rst_tot = $this->db->query($q_tot)->result();
				if(count($rst_tot) > 0) {
					$rst_tot_annually = (array)$rst_tot[0];
					$total = $rst_tot_annually['tot'];
				}
				
				$rstRtn['data_tot'] = 0;
				$rstRtn['tot_cnt'] = 0;
				$rstRtn['data'] = [];

				// 총계쿼리
				if($total > 0) {
					$q_tot = 'SELECT ';
					$q_tot2 = ', IFNULL('; // 각 연도별 계의 합산
					$q_tot3 = ',CONCAT(IFNULL(ROUND((('; // 총합산 - 각 연도의 합산
					$q_tot4 = ''; // base쿼리에서 년도별 합계용 select
					// 조회 연도만큼 select 컬럼 생성
					for($y=$args['begin']; $y<=$args['end']; $y++) {
						if($q_tot != 'SELECT ') {
							$q_tot .= ', ';
						}
						if($args['sch_kind'] == $sch_kind_cslkind) {// 상담유형 - 중복제거 합계 추가
							$q_tot .= 'CONCAT(IFNULL(SUM(ck_'. $y .'), 0), "(", '. $rst_tot_annually['dst_year_'. $y] .', ")") AS ck_'. $y .',
							IFNULL(ROUND(((SUM(ck_'. $y .'))/ '. $rst_tot_annually['year_'. $y] .')*100, 1), 0) AS cr_'. $y;
						}
						else {
							$q_tot .= 'IFNULL(SUM(ck_'. $y .'), 0) AS ck_'. $y .', IFNULL(ROUND(((SUM(ck_'. $y .'))/ '. $rst_tot_annually['year_'. $y] .')*100, 1), 0) AS cr_'. $y;
						}
						//
						if($q_tot2 != ', IFNULL(') {
							$q_tot2 .= '+';
						}
						$q_tot2 .= 'SUM(ck_'. $y .')';
						//
						if($q_tot3 != ',CONCAT(IFNULL(ROUND(((') {
							$q_tot3 .= '+';
						}
						$q_tot3 .= 'SUM(ck_'. $y .')';
						//
						if($q_tot4 != '') {
							$q_tot4 .= ', ';
						}
						$q_tot4 .= 'IF(target="'. $y .'", cnt, 0) as ck_'. $y;
					}
						
					$target_field2 = str_replace('C.', 'T2.', $target_field1);
					if($args['sch_kind'] == $sch_kind_cslkind) { // 상담유형
						$target_field2 = str_replace('CS.', 'T2.', $target_field1);
					}
					$q_tot2 .= ', 0) AS tot';
					$q_tot3 .= ')/ '. $rst_tot_annually['tot'] .' )*100, 1), 0), "%") AS tot_p';
					$q_tot .= $q_tot2 . $q_tot3 .'
					FROM (
						SELECT '. $q_tot4 . '
						FROM (
							SELECT T1.code_name AS subject, T1.s_code, T2.target, T2.cnt
							FROM (
								SELECT code_name, s_code
								FROM sub_code
								WHERE m_code = "'. $tg_m_code .'"
							) T1
							LEFT JOIN (
								SELECT DATE_FORMAT(C.csl_date, "%Y") AS target, COUNT(C.seq) AS cnt, '. $target_field1 .'
								FROM counsel C ';
					if($args['sch_kind'] == $sch_kind_cslkind) { // 상담유형
						$q_tot .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
					}
					else {
						$q_tot .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
					}
					$q_tot .= 'WHERE '. $where .' AND C.csl_date BETWEEN "'. $args['begin'] .'-01-01"
									AND "'. $args['end'] .'-12-31" AND '. $target_field1 .' IN ('. $tg_s_code .')
								GROUP BY '. $target_field1 .', DATE_FORMAT(C.csl_date, "%Y")
								ORDER BY DATE_FORMAT(C.csl_date, "%Y") ASC
							) T2 ON T1.s_code='. $target_field2 .'
							ORDER BY T1.code_name ASC, T2.target ASC
						) G
					) Z';
						
					$year_tot = $this->db->query($q_tot)->result();
					$rstRtn['data_tot'] = count($year_tot) > 0 ? $year_tot[0] : 0;
					$rstRtn['data_tot']->tot = $total;					
// 					echof($q_tot);						
					// 메인쿼리 - 조회 년도별 계, 전체 계 쿼리 생성
					$q = 'SELECT subject, ';
					$q2 = ', IFNULL('; // 각 연도별 계의 합산
					$q3 = ',CONCAT(IFNULL(ROUND((('; // 총합산 - 각 연도의 합산
					$q4 = ''; // base쿼리에서 년도별 합계용 select
					// 조회 연도만큼 select 컬럼 생성
					for($y=$args['begin']; $y<=$args['end']; $y++) {
						if($q != 'SELECT subject, ') {
							$q .= ', ';
						}
						$q .= 'IFNULL(SUM(ck_'. $y .'), 0) AS ck_'. $y .', IFNULL(ROUND(((SUM(ck_'. $y .'))/ '. $rst_tot_annually['year_'. $y] .')*100, 1), 0) AS cr_'. $y;
						//
						if($q2 != ', IFNULL(') {
							$q2 .= '+';
						}
						$q2 .= 'SUM(ck_'. $y .')';
						//
						if($q3 != ',CONCAT(IFNULL(ROUND(((') {
							$q3 .= '+';
						}
						$q3 .= 'SUM(ck_'. $y .')';
						//
						if($q4 != '') {
							$q4 .= ', ';
						}
						$q4 .= 'IF(target="'. $y .'", cnt, 0) as ck_'. $y;
					}
						
					$q2 .= ', 0) AS tot';
					$q3 .= ')/ '. $rst_tot_annually['tot'] .' )*100, 1), 0), "%") AS tot_p';
					$q .= $q2 . $q3 . '
					FROM (
						SELECT subject, dsp_order, '. $q4 . '
						FROM (
							SELECT T1.code_name AS subject, T1.s_code, T2.target, T2.cnt, T1.dsp_order
							FROM (
								SELECT code_name, s_code, dsp_order
								FROM sub_code
								WHERE m_code = "'. $tg_m_code .'"
							) T1
							LEFT JOIN (
								SELECT DATE_FORMAT(C.csl_date, "%Y") AS target, COUNT(C.seq) AS cnt, '. $target_field1 .'
								FROM counsel C ';
					if($args['sch_kind'] == $sch_kind_cslkind) { // 상담유형
						$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
					}
					else {
						$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
					}
					$q .= 'WHERE '. $where .' AND C.csl_date BETWEEN "'. $args['begin'] .'-01-01" AND "'. $args['end'] .'-12-31"
									AND '. $target_field1 .' IN ('. $tg_s_code .')
								GROUP BY '. $target_field1 .', DATE_FORMAT(C.csl_date, "%Y")
								ORDER BY DATE_FORMAT(C.csl_date, "%Y") ASC
							) T2 ON T1.s_code='. $target_field2 .'
							ORDER BY T1.code_name ASC, T2.target ASC
						) G
					) Z
					GROUP BY subject
					ORDER BY dsp_order';
// 					echof($q);
					
					$rstRtn['data'] = $this->db->query($q)->result();
					$rstRtn['tot_cnt'] = count($rstRtn['data']);
				}
			}
			// ############################
			// 구분 down07_2  : 시계열통계 - 임금/근로시간(월평균)
			else {

				$target_field2 = str_replace('C.', 'T2.', $target_field1);
				$target_field3 = 'SC.s_code';
				if($args['sch_kind'] == $sch_kind_cslkind) { // 상담유형
					$target_field2 = str_replace('CS.', 'T2.', $target_field1);
					$target_field3 = 'CS.s_code';
				}
				
				// 년도별 총계 쿼리 - 년별
				$q_tot_sub_p1 = ''; // 임금용
				$q_tot_sub_p2 = '';
				$q_tot_sub_w1 = ''; // 근로시간용
				$q_tot_sub_w2 = '';
				$q_tot_sub_sum = '';
				for($y=$args['begin']; $y<=$args['end']; $y++) {
					//임금
					if($q_tot_sub_p1 != '') {
						$q_tot_sub_p1 .= ',';
					}
					$q_tot_sub_p1 .= 'pay_tcnt_'. $y .'.cnt AS pay_tcnt_'. $y .', IFNULL(ROUND(pay_tcnt_'. $y .'.summ/pay_tcnt_'. $y .'.cnt, 0), 0) AS pay_sum_'. $y;
					if($q_tot_sub_p2 != '') {
						$q_tot_sub_p2 .= ',';
					}
					$q_tot_sub_p2 .= '(SELECT COUNT(C.ave_pay_month) AS cnt, SUM(C.ave_pay_month) AS summ
						FROM counsel C ';
					if($args['sch_kind'] == $sch_kind_cslkind) {//상담유형
						$q_tot_sub_p2 .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
					}
					else {
						$q_tot_sub_p2 .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
					}
					$q_tot_sub_p2 .= 'WHERE '. $where .'
						AND DATE_FORMAT(csl_date, "%Y")="'. $y .'" 
						AND '. $target_field3 .' IN ( '. $tg_s_code .' ) 
						AND ( C.ave_pay_month <> "" )) AS pay_tcnt_'. $y .' ';
					//근로시간
					if($q_tot_sub_w1 != '') {
						$q_tot_sub_w1 .= ',';
					}
					$q_tot_sub_w1 .= 'work_tcnt_'. $y .'.cnt AS work_tcnt_'. $y .', IFNULL(ROUND(work_tcnt_'. $y .'.summ/work_tcnt_'. $y .'.cnt, 0), 0) AS work_sum_'. $y;
					if($q_tot_sub_w2 != '') {
						$q_tot_sub_w2 .= ',';
					}
					$q_tot_sub_w2 .= '(SELECT COUNT(C.work_time_week) AS cnt, SUM(C.work_time_week) AS summ
						FROM counsel C ';
					if($args['sch_kind'] == $sch_kind_cslkind) {//상담유형
						$q_tot_sub_w2 .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
					}
					else {
						$q_tot_sub_w2 .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
					}
					$q_tot_sub_w2 .= 'WHERE '. $where .'
						AND DATE_FORMAT(csl_date, "%Y")="'. $y .'" 
						AND '. $target_field3 .' IN ( '. $tg_s_code .' ) 
						AND ( C.work_time_week <> "" )) AS work_tcnt_'. $y .' ';
					// sum
					if($q_tot_sub_sum != '') {
						$q_tot_sub_sum .= ' + ';
					}
					$q_tot_sub_sum .= 'pay_tcnt_'. $y .'.cnt + work_tcnt_'. $y .'.cnt';
				}
				//쿼리 조합
				$q_tot = 'SELECT ('. $q_tot_sub_sum . ') AS tot_cnt, '. $q_tot_sub_p1 .','. $q_tot_sub_w1 .'
					FROM ( '. $q_tot_sub_p2 .','. $q_tot_sub_w2 .')';
// 				echof($q_tot);
				
				$rst_tot = $this->db->query($q_tot)->result();
				
				$rstRtn['data'] = [];
				$rstRtn['data_tot'] = [];
				$rstRtn['tot_cnt'] = 0;
				$total = 0;
				
				if(count($rst_tot) > 0) {
					$total = $rst_tot[0]->tot_cnt;
					$rstRtn['data_tot'] = (array)$rst_tot[0];
					
					// 메인쿼리 - 년도별, 임금,근로시간 합계 쿼리
					$q_sub1 = '';
					$q_sub2 = '';
					if($total > 0) {
						$q = 'SELECT subject, ';
						for($y=$args['begin']; $y<=$args['end']; $y++) {
							if($q_sub1 != '') {
								$q_sub1 .= ',';
							}
							$q_sub1 .= 'CONCAT(IFNULL(SUM(p_'. $y .'), 0), " (", IFNULL(SUM(p_cnt_'. $y .'), 0), ")") AS p_'. $y .'
								, CONCAT(IFNULL(SUM(w_'. $y .'), 0), " (", IFNULL(SUM(w_cnt_'. $y .'), 0), ")") AS w_'. $y .' ';
							if($q_sub2 != '') {
								$q_sub2 .= ',';
							}
							$q_sub2 .= 'IF(target="p_'. $y .'", summ, 0) as p_'. $y .', IF(target="p_'. $y .'", cnt, 0) as p_cnt_'. $y .'
								, IF(target="w_'. $y .'", summ, 0) as w_'. $y .', IF(target="w_'. $y .'", cnt, 0) as w_cnt_'. $y .' ';
						}
						$q .= $q_sub1 .' 
							FROM ( 
								SELECT subject, '. $q_sub2 .', dsp_order
								FROM (
									SELECT T1.code_name AS subject, T1.s_code, T2.target, T2.summ, T1.dsp_order, T2.cnt
									FROM (
										SELECT code_name, s_code, dsp_order
										FROM sub_code
										WHERE m_code = "'. $tg_m_code .'"
									) T1
									LEFT JOIN
									(
										SELECT CONCAT("p_", DATE_FORMAT(C.csl_date, "%Y")) AS target, ROUND(SUM(C.ave_pay_month)/COUNT(C.ave_pay_month),0) AS summ, COUNT(*) AS cnt, '. $target_field1 .'
										FROM counsel C ';
						if($args['sch_kind'] == $sch_kind_cslkind) {//상담유형
							$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
						}
						else {
							$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
						}
						$q .= 'WHERE '. $where .' 
											AND C.csl_date BETWEEN "'. $args['begin'] .'-01-01" AND "'. $args['end'] .'-12-31" 
											AND '. $target_field3 .' IN ('. $tg_s_code .') 
											AND C.ave_pay_month <> ""
										GROUP BY DATE_FORMAT(C.csl_date, "%Y"), '. $target_field1 .' 
										UNION
										SELECT CONCAT("w_", DATE_FORMAT(C.csl_date, "%Y")) AS target, ROUND(SUM(C.work_time_week)/COUNT(C.work_time_week),1) AS summ, COUNT(*) AS cnt, '. $target_field1 .'
										FROM counsel C ';
						if($args['sch_kind'] == $sch_kind_cslkind) {//상담유형
							$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
						}
						else {
							$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code ';
						}
							$q .= 'WHERE '. $where .'
											AND C.csl_date BETWEEN "'. $args['begin'] .'-01-01" AND "'. $args['end'] .'-12-31" 
											AND '. $target_field3 .' IN ('. $tg_s_code .') 
											AND C.work_time_week <> ""
										GROUP BY DATE_FORMAT(C.csl_date, "%Y"), '. $target_field1 .'
									) T2 ON T1.s_code='. $target_field2 .'
									GROUP BY subject, target
								) TT
							) G
							GROUP BY subject
							ORDER BY dsp_order';						
// 						echof($q);
	
						$rstRtn['data'] = $this->db->query($q)->result();
						$rstRtn['tot_cnt'] = count($rstRtn['data']);
// 						echof($rstRtn);
					}
				}
			}
			//
		}

		
		//------------------------------------------------------------------------------------------------------------------
		// 구분 04 : 임금근로시간 통계
		//------------------------------------------------------------------------------------------------------------------
		else if($args['kind'] == 'down04') {

			// where - 상담기간, 소속, 상담방법, 처리결과 검색
			$where .= $where1 . $where2 . $where3 . $where5;
			
			// get_sttt_mcode 메서드 내 구분자가 sch_kind이기 때문에 sel_col로 넘어온 구분자를 대입해 준다.
			$args['sch_kind'] = $args['sel_col'];
			
// 			echof($args['csl_code']);
			// csl_code 가 공백이면 임금근로시간 단순 통계 임 ( csl_code에 코드가 있으면 임금근로시간 교차통계 )
			// # 임금근로시간 단순통계
			if($args['csl_code'] == '') {

				// 기준데이터 조회
				$tmp = self::get_sttt_mcode($args);
				
				$datas1 = $tmp['datas1'];// 기준코드 sub_code 배열
				$datas2 = $tmp['datas2'];//교차대상 sub_code 배열
				$tg_m_code = $tmp['tg_m_code'];
				$tg1_s_code = $tmp['tg_s_code'];
				$target_field1 = $tmp['target_field1'];
				$target_field2 = $tmp['target_field2'];
					
				// 메인쿼리 (총계쿼리 생략)
				$q = '
					SELECT subject, IFNULL(C.target, "-") AS target,
						IFNULL(SUM(pay), 0) AS pay, IFNULL(SUM(p_cnt), 0) AS p_cnt, 
						IFNULL(SUM(works), 0) AS works, IFNULL(SUM(w_cnt), 0) AS w_cnt,';
				if($args['sch_kind'] == 11) { // 상담유형
					$q .='dst_p_tot, dst_w_tot,';
				}
				$q .= '(SELECT COUNT(DISTINCT(C.seq))
						FROM counsel C ';
				if($args['sch_kind'] == 11) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'WHERE '. $where .'
							AND '. $target_field1 .' IN ('. $tg1_s_code .')) AS dst_csl_tot/* 중복제거 총상담건수 */
					FROM (
						SELECT DD.code_name AS subject, B.target, B.ave_pay_month AS pay, B.ck1 AS p_cnt, 
							B.work_time_week AS works, B.ck2 AS w_cnt, DD.dsp_order ';
				if($args['sch_kind'] == 11) { // 상담유형
					$q .= ', (SELECT COUNT(*) FROM counsel C WHERE '. $where .'
						AND C.ave_pay_month <> "") AS dst_p_tot,
						(SELECT COUNT(*) FROM counsel C WHERE '. $where .'
						AND C.work_time_week <> "") AS dst_w_tot ';
				}
				$q .= 'FROM (
							SELECT code_name, s_code, dsp_order
							FROM sub_code
							WHERE m_code = "'. $tg_m_code .'"
						) DD
						LEFT JOIN (
							SELECT *
							FROM (
								SELECT '. $target_field1 .' AS target, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, 
									COUNT(C.seq) AS ck1, "" AS work_time_week, 0 AS ck2, SC.dsp_order
								FROM counsel C ';
				if($args['sch_kind'] == 11) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
								WHERE '. $where .'
									AND '. $target_field1 .' IN ('. $tg1_s_code .')
									AND C.ave_pay_month <> ""
								GROUP BY '. $target_field1 .'
								UNION /**/
								SELECT '. $target_field1 .' AS target, "" AS ave_pay_month, 0 AS ck1, 
									ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS work_time_week, COUNT(C.seq) AS ck2, SC.dsp_order
								FROM counsel C ';
				if($args['sch_kind'] == 11) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
								WHERE '. $where .'
									AND '. $target_field1 .' IN ('. $tg1_s_code .')
									AND C.work_time_week <> ""
								GROUP BY '. $target_field1 .'
							) A
							GROUP BY target, ave_pay_month, work_time_week
							ORDER BY target, dsp_order
						) B ON B.target=DD.s_code
					) C
					GROUP BY C.target, C.subject
					ORDER BY C.dsp_order';
// 				echof($q);
				
				$rstRtn['data'] = $this->db->query($q)->result();
				$rstRtn['tot_cnt'] = count($rstRtn['data']);
				$rstRtn['data_tot'] = count($datas1);
				$rstRtn['dst_csl_tot'] = 0;
				if($rstRtn['tot_cnt'] > 0) {
					$rstRtn['dst_csl_tot'] = $rstRtn['data'][0]->dst_csl_tot;
				}
// 				echof($rstRtn);
			}
			// # 임금근로시간 교차통계
			else {

				// 기준데이터 조회
				$tmp = self::get_sttt_mcode($args, TRUE);
				
				$datas1 = $tmp['datas1'];// 기준코드 sub_code 배열
				$datas2 = $tmp['datas2'];//교차대상 sub_code 배열
				$tg_m_code = $tmp['tg_m_code'];
				$tg2_m_code = $tmp['tg2_m_code'];
				$tg1_s_code = $tmp['tg_s_code'];
				$tg2_s_code = $tmp['tg2_s_code'];
				$target_field1 = $tmp['target_field1'];
				$target_field2 = $tmp['target_field2'];
				
				// 메인쿼리
				$q = '/* 임금근로시간 교차통계 - 메인쿼리 */
					SELECT
						subject, ';
				foreach($datas2 as $i => $item) {
					$j = $i+1;
					$q .= 'IFNULL(SUM(pay'. $j .'), 0) AS pay'. $j .', IFNULL(SUM(p_cnt'. $j .'), 0) AS p_cnt'. $j .', IFNULL(SUM(p_dst_cnt'. $j .'), 0) AS p_dst_cnt'. $j .',
						IFNULL(SUM(work'. $j .'), 0) AS work'. $j .', IFNULL(SUM(w_cnt'. $j .'), 0) AS w_cnt'. $j .', IFNULL(SUM(w_dst_cnt'. $j .'), 0) AS w_dst_cnt'. $j .',';
				}
				$q .= 'dst_p_tot, dst_w_tot, 
						(SELECT COUNT(DISTINCT(C.seq))
						FROM counsel C ';
				if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'WHERE '. $where .'  AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS dst_csl_tot/* 중복제거 총상담건수 */';
				// target1 기준 교차코드(가로) 상담 합계 건수 쿼리 영역
				foreach($datas1 as $i => $item) {
					$j = $i+1;
					$q .= ',/* target1 기준 교차코드(가로) 상담 합계 건수 */(SELECT COUNT(C.seq)
						FROM counsel C ';
					if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
						$q .= '/**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
					}
					$q .= 'WHERE '. $where .'
						AND /*target1*/'. $target_field1 .'="'. $item->s_code .'" 
						AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_tot'. $j .' ';
					$q .= ',/* target1 기준 교차코드(가로) 상담 합계 중복제거 건수 */(SELECT COUNT(DISTINCT(C.seq))
						FROM counsel C ';
					if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
						$q .= '/**/INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
					}
					$q .= 'WHERE '. $where .'
						AND /*target1*/'. $target_field1 .'="'. $item->s_code .'" 
						AND (C.ave_pay_month <> "" OR C.work_time_week <> "")) AS tg1_col_dst_tot'. $j .' ';
				}
				//
				$q .= 'FROM (
						SELECT
							C.subject, C.target, C.target2,
							/* 교차대상 코드별 데이터 추출 */ ';
				foreach($datas2 as $i => $item) {
					$j = $i+1;
					$q .='IF( C.target2="'. $item->s_code .'", SUM(C.ave_pay_month), 0) AS pay'. $j .', IF( C.target2="'. $item->s_code .'", SUM(C.ck1), 0) AS p_cnt'. $j .', 
						/* 상담유형용 중복제거 건수*/IF( C.target2="'. $item->s_code .'", SUM(C.dst_ck1), 0) AS p_dst_cnt'. $j .',
						IF( C.target2="'. $item->s_code .'", SUM(C.work_time_week), 0) AS work'. $j .', IF( C.target2="'. $item->s_code .'", SUM(C.ck2), 0) AS w_cnt'. $j .', 
						/* 상담유형용 중복제거 건수*/IF( C.target2="'. $item->s_code .'", SUM(C.dst_ck2), 0) AS w_dst_cnt'. $j .', ';
				}
				$q .= '/*중복제거 상담건수(임금,근로시간) -->*/(SELECT COUNT(*) FROM counsel C WHERE '. $where .' 
							AND C.ave_pay_month <> "") AS dst_p_tot,
							(SELECT COUNT(*) FROM counsel C WHERE '. $where .' 
							AND C.work_time_week <> "") AS dst_w_tot/*<--*/, C.dsp_order
						FROM (
							SELECT DD.code_name AS subject, B.target, B.target2, B.ave_pay_month, B.ck1, B.dst_ck1, B.work_time_week, B.ck2, B.dst_ck2, DD.dsp_order
							FROM (
								SELECT code_name, s_code, dsp_order
								FROM sub_code
								WHERE m_code="'. $tg_m_code .'" /*target 과 같은 코드로 매치 */
								ORDER BY dsp_order
							) DD
							LEFT JOIN (
								SELECT *
								FROM (
									SELECT '. $target_field1 .' AS target, '. $target_field2 .' AS target2, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, 
										"" AS work_time_week, COUNT(C.seq) AS ck1, COUNT(DISTINCT(C.seq)) AS dst_ck1, 0 AS ck2, 0 AS dst_ck2, SC.dsp_order
									FROM counsel C ';
				if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
									WHERE '. $where .' 
										AND /*target1*/'. $target_field1 .' IN ( '. $tg1_s_code .' )
										AND /*target2*/'. $target_field2 .' IN ( '. $tg2_s_code .' )
										AND C.ave_pay_month <> ""
									GROUP BY /*target1*/'. $target_field1 .', /*target2*/'. $target_field2 .'
									UNION /**/			
									SELECT '. $target_field1 .' AS target, '. $target_field2 .' AS target2, "" AS ave_pay_month, ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS ave_pay_month, 
										0 AS ck1, 0 AS dst_ck1, COUNT(C.seq) AS ck2, COUNT(DISTINCT(C.seq)) AS dst_ck2, SC.dsp_order
									FROM counsel C ';
				if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'INNER JOIN sub_code SC ON /**/'. $target_field1 .'=SC.s_code
									WHERE '. $where .' 
										AND /*target1*/'. $target_field1 .' IN ( '. $tg1_s_code .' )
										AND /*target2*/'. $target_field2 .' IN ( '. $tg2_s_code .' )
										AND C.work_time_week <> ""
									GROUP BY /*target1*/'. $target_field1 .', /*target2*/'. $target_field2 .'
								) A
								GROUP BY target, target2, ave_pay_month, work_time_week
							) B ON B.target=DD.s_code
						) C
						GROUP BY subject, target2
					) T
					GROUP BY subject
					ORDER BY dsp_order';
// 				echof($q);

				$rstRtn['data'] = $this->db->query($q)->result();
				$rstRtn['tot_cnt'] = count($rstRtn['data']);
				$rstRtn['data_tot'] = count($datas1);
				$rstRtn['dst_csl_tot'] = 0;
				if($rstRtn['tot_cnt'] > 0) {
					$rstRtn['dst_csl_tot'] = $rstRtn['data'][0]->dst_csl_tot;
				}
// 				echof($rstRtn);
				/*
				// target1기준 중복제거 총건수 쿼리
				$rstRtn['dst_tot'] = [];
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) {
					$q = 'SELECT COUNT(C.seq) AS cnt
				FROM counsel C
				INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
				WHERE '. $where .'
						AND '. $target_field1 .' IN ( '. $tg1_s_code .' )
					GROUP BY '. $target_field1 .'
					ORDER BY SC.dsp_order';
					$rstRtn['dst_tot'] = $this->db->query($q)->result();
				}
				
				// target2기준 총 상담건수 쿼리
				$q = 'SELECT '. $target_field2 .' AS target2, COUNT(C.seq) AS t_cnt ';
				$q .= 'FROM counsel C ';
				if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'INNER JOIN sub_code SC ON '. $target_field2 .'=SC.s_code
					WHERE '. $where .'  
						AND '. $target_field2 .' IN ( '. $tg2_s_code .' )
					GROUP BY '. $target_field2 .'
					ORDER BY SC.dsp_order';
// 				echof($q);
				$rstRtn['target2_tot'] = $this->db->query($q)->result();
				
				// target2기준 중복제거 총건수 쿼리
				if($args['sch_kind'] == 11) {
					$q = 'SELECT COUNT(C.seq) AS cnt
					FROM counsel C
					INNER JOIN sub_code SC ON '. $target_field2 .'=SC.s_code
					WHERE '. $where .'  
							AND '. $target_field2 .' IN ( '. $tg2_s_code .' )
						GROUP BY '. $target_field2 .'
						ORDER BY SC.dsp_order';
					$rstRtn['dst_tot'] = $this->db->query($q)->result();// 가로,세로 둘중 한쪽만 필요하다.
				}
				*/
				// target1기준 중복제거 전체건수 쿼리
				$rstRtn['dst_tot'] = [];
				if($args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) {// 상담유형
					$q = '/*target1기준 중복제거 전체건수 쿼리*/
						SELECT IFNULL(C.cnt, 0) as cnt
						FROM (
							SELECT code_name, s_code, dsp_order
							FROM sub_code
							WHERE m_code="'. $tg_m_code .'"
							ORDER BY dsp_order
						) A
						LEFT JOIN (
							SELECT '. $target_field1 .', COUNT(C.seq) AS cnt
							FROM counsel C
							INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
							WHERE '. $where .'
								AND '. $target_field1 .' IN ( '. $tg1_s_code .' )
							GROUP BY '. $target_field1 .'
						) C ON A.s_code='. $target_field1 .'
						ORDER BY A.dsp_order';
					$rstRtn['dst_tot'] = $this->db->query($q)->result();
				}
				
				// target2기준 전체건수 쿼리
				$target_field22 = $target_field2 == 'CS.s_code' ? 'C.s_code' : $target_field2;
				$q = '/*target2기준 전체건수*/
					SELECT '. $target_field22 .' AS target2, IFNULL(C.t_cnt, 0) as t_cnt
					FROM (
						SELECT code_name, s_code, dsp_order
						FROM sub_code
						WHERE m_code="'. $tg2_m_code .'"
						ORDER BY dsp_order
					) A
					LEFT JOIN (
						SELECT '. $target_field2 .', COUNT(C.seq) AS t_cnt ';
				$q .= 'FROM counsel C ';
				if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
					$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
				}
				$q .= 'INNER JOIN sub_code SC ON '. $target_field2 .'=SC.s_code
						WHERE '. $where .'  
							AND '. $target_field2 .' IN ( '. $tg2_s_code .' )
						GROUP BY '. $target_field2 .'
					) C ON A.s_code='. $target_field22 .'
					ORDER BY A.dsp_order';
// 				echof($q);
				$rstRtn['target2_tot'] = $this->db->query($q)->result();
				
				// target2기준 중복제거 전체건수 쿼리
				if($args['sch_kind'] == 11) {// 상담유형
					$q = '/*target2기준 중복제거 전체건수*/
						SELECT IFNULL(C.cnt, 0) as cnt
						FROM (
							SELECT code_name, s_code, dsp_order
							FROM sub_code
							WHERE m_code="'. $tg_m_code .'"
							ORDER BY dsp_order
						) A
						LEFT JOIN (
							SELECT '. $target_field1 .', COUNT(C.seq) AS cnt
							FROM counsel C
							INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq 
							INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
							WHERE '. $where .'
								AND '. $target_field1 .' IN ( '. $tg1_s_code .' )
							GROUP BY '. $target_field1 .'
						) C ON A.s_code=C.s_code
						ORDER BY A.dsp_order';
// 					echof($q);
					$rstRtn['dst_tot'] = $this->db->query($q)->result();// 가로,세로 둘중 한쪽만 필요하다.
				}
			}
			
			// target1기준(y축) 전체상담건수 쿼리 (단순,교차 공통)
			$target_field1 = $target_field1 == 'CS.s_code' ? 'C.s_code' : $target_field1;
			$q = '/*target1기준 전체상담건수*/
				SELECT '. $target_field1 .' AS target1, IFNULL(C.t_cnt, 0) as t_cnt
				FROM (
					SELECT code_name, s_code, dsp_order
					FROM sub_code
					WHERE m_code="'. $tg_m_code .'" /*target 과 같은 코드로 매치 */
					ORDER BY dsp_order
				) A
				LEFT JOIN (
					SELECT '. $target_field1 .', COUNT(C.seq) AS t_cnt 
					FROM counsel C ';
			if($args['sch_kind'] == 11 || $args['csl_code'] == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND) { // 상담유형
				$q .= 'INNER JOIN counsel_sub CS ON C.seq=CS.csl_seq ';
			}
			$q .= 'INNER JOIN sub_code SC ON '. $target_field1 .'=SC.s_code
					WHERE '. $where .'  
						AND '. $target_field1 .' IN ( '. $tg1_s_code .' ) 
					GROUP BY '. $target_field1 .'
				) C ON A.s_code='. $target_field1 .'
				ORDER BY A.dsp_order';
// 			echof($q);
			$rstRtn['target1_tot'] = $this->db->query($q)->result();
				
		}// 임금,근로시간 교차통계 끝
		
		
		//------------------------------------------------------------------------------------------------------------------
		// 구분 05 : 소속별통계
		//------------------------------------------------------------------------------------------------------------------
		else if($args['kind'] == 'down05') {
			
			$rstRtn = array();
			$target_field1 = '';
			$target_field3 = '';
			$csl_kind = '';
			$csl_kind2 = '';
			$csl_kind3 = '';
			$csl_kind4 = '';
			$where_asso = '';
		
			//교차 가로축 검색
			if($args['sch_kind'] == 0) {// 상담방법
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY;
				$target_field1 = 'C.s_code';
			}
			elseif($args['sch_kind'] == 1) {// 성별
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_GENDER;
				$target_field1 = 'C.gender';
			}
			elseif($args['sch_kind'] == 2) { // 연령대
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_AGES;
				$target_field1 = 'C.ages';
			}
			elseif($args['sch_kind'] == 3) { // 거주지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
				$target_field1 = 'C.live_addr';
			}
			elseif($args['sch_kind'] == 4) { // 회사소재지
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE);//회사 소재지 코드는 거주지코드를 사용한다.
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP;
				$target_field1 = 'C.comp_addr';
			}
			elseif($args['sch_kind'] == 5) { // 직종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND;
				$target_field1 = 'C.work_kind';
			}
			elseif($args['sch_kind'] == 6) { // 업종
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
				$target_field1 = 'C.comp_kind';
			}
			elseif($args['sch_kind'] == 7) { // 고용형태
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND;
				$target_field1 = 'C.emp_kind';
			}
			elseif($args['sch_kind'] == 8) {// 근로자수
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT;
				$target_field1 = 'C.emp_cnt';
			}
			elseif($args['sch_kind'] == 9) {// 근로계약서
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN;
				$target_field1 = 'C.emp_paper_yn';
			}
			elseif($args['sch_kind'] == 10) {// 4대보험
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN;
				$target_field1 = 'C.emp_insured_yn';
			}
			elseif($args['sch_kind'] == 11) { // 상담유형
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE);
				$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND;
				$target_field1 = 'CS2.s_code';
				$target_field3 = 'DT.asso_code';
				$csl_kind2 = ', S.distinct_tot ';
				$csl_kind3 = ', DD.distinct_tot ';
				$csl_kind4 = ', DT.distinct_tot ';
			}
			elseif($args['sch_kind'] == 12) { // 처리결과
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_PROC_RST, TRUE);
				$target_field1 = 'C.csl_proc_rst';
			}
			elseif($args['sch_kind'] == 13) { // 임금근로시간 - 좌측 기준은 소속으로 한다.
				$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION, TRUE);
				$target_field1 = 'C.asso_code';
			}

			//교차 세로축 - 소속 
			$datas2 = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
			$target_asso_cd = 'C.asso_code';
			
			// target 컬럼의 s_code 문자열로 생성
			$tg_s_code = '';
			foreach ($datas1 as $i => $item) {
				if($tg_s_code != '') {
					$tg_s_code .= '","';
				}
				$tg_s_code .= $item->s_code;
			}
			$tg_s_code = ' "'. $tg_s_code .'" ';
			
			// 통계 구분값: 상담유형
			$sch_kind_cslkind = 11;
			
			// where - 상담일, 상담방법, 처리결과 검색
			$where .= $where1 . $where2 . $where3 . $where5;
		
			// 임금 근로시간
			if($args['sch_kind'] == 13) {

				// 메인쿼리 (총계쿼리 생략)
				$q = '
					SELECT subject, IFNULL(C.target, "-") AS target, 
						IFNULL(SUM(pay), 0) AS pay, IFNULL(SUM(works), 0) AS works, 
						IFNULL(SUM(p_cnt), 0) AS p_cnt, IFNULL(SUM(w_cnt), 0) AS w_cnt
					FROM (
						SELECT DD.code_name AS subject, B.target, B.ave_pay_month AS pay, B.ck1 AS p_cnt, B.work_time_week AS works, B.ck2 AS w_cnt, DD.dsp_order
						FROM (
							SELECT code_name, s_code, dsp_order
							FROM sub_code
							WHERE m_code = "'. $datas2 .'"
						) DD
						LEFT JOIN (
							SELECT *
							FROM (
								SELECT C.asso_code AS target, ROUND(SUM(C.ave_pay_month)/COUNT(C.seq),0) AS ave_pay_month, COUNT(C.seq) AS ck1, "" AS work_time_week, 0 AS ck2, SC.dsp_order
								FROM counsel C
								INNER JOIN sub_code SC ON C.asso_code=SC.s_code
								WHERE '. $where .'
									AND C.asso_code IN ('. $tg_s_code .')
									AND C.ave_pay_month <> ""
								GROUP BY C.asso_code		
								UNION /**/				
								SELECT C.asso_code AS target, "" AS ave_pay_month, 0 AS ck1, ROUND(SUM(C.work_time_week)/COUNT(C.seq),0) AS work_time_week, COUNT(C.seq) AS ck2, SC.dsp_order
								FROM counsel C
								INNER JOIN sub_code SC ON C.asso_code=SC.s_code
								WHERE '. $where .' 
									AND C.asso_code IN ('. $tg_s_code .')
									AND C.work_time_week <> ""
								GROUP BY C.asso_code
							) A
							GROUP BY target, ave_pay_month, work_time_week
							ORDER BY target, dsp_order
						) B ON B.target=DD.s_code
					) C
					GROUP BY C.target, C.subject
					ORDER BY C.dsp_order';
// 				echof($q);

				$rstRtn['data'] = $this->db->query($q)->result();
				$rstRtn['tot_cnt'] = count($rstRtn['data']);
				$rstRtn['data_tot'] = count($datas1);

				// target1기준 총 상담건수 쿼리 (단순,교차 공통)
				$q = 'SELECT code_name as target, IFNULL(t_cnt, 0) as t_cnt
					FROM (
						SELECT code_name, s_code, dsp_order
						FROM sub_code
						WHERE m_code = "'. $datas2 .'"
					) A
					LEFT JOIN (
						SELECT C.asso_code, COUNT(C.seq) AS t_cnt
						FROM counsel C 
						INNER JOIN sub_code SC ON C.asso_code=SC.s_code
						WHERE '. $where .'
							AND C.asso_code IN ('. $tg_s_code .')
						GROUP BY C.asso_code
						ORDER BY SC.dsp_order
				) B ON A.s_code=B.asso_code
				ORDER BY A.dsp_order';
// 				echof($q);
				$rstRtn['target1_tot'] = $this->db->query($q)->result();
				
			}
			// 임금근로시간 외 나머지
			else {
				
				// 총계쿼리
				$t = '';
				$index = 1;
				$t1 = '';
				$t_tot = '';
				$t2 = 'SELECT S.subject ';
				$t3 = '';
				foreach($datas1 as $data) {
					if($t1 != '') $t1 .= ', ';
					// 상담유형별 중복제거 건수는 불필요하여 해당 쿼리부분 주석처리
// 					if($args['sch_kind'] == $sch_kind_cslkind){
// 						$t1 .= 'CONCAT(IFNULL(SUM(M.ck'.$index.'), 0), "(", IFNULL(M.distinct_ck'.$index.', 0), ")")';
// 					}
// 					else{
						$t1 .= 'SUM(M.ck'.$index.')';
// 					}
			
					if($t_tot != '') $t_tot .= ' + ';
					$t_tot .= 'SUM(M.ck'.$index.')';
						
					$t2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) AS ck'.$index;
// 					if($args['sch_kind'] == $sch_kind_cslkind){
// 						$t2 .= ', S.distinct_ck'.$index;
// 						if($t3 != '') $t3 .= ', ';
// 						$t3 .= '(SELECT count(*) as distinct_ck'.$index .' FROM '. $this->tbl_counsel .' C
// 							INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq
// 							WHERE '.$where .' AND '.$target_field1.' = "'.$data->s_code.'" ) AS distinct_ck'.$index.' ';
// 					}
					$index++;
				}
					
				if($args['sch_kind'] == $sch_kind_cslkind){
					$t_tot = 'CONCAT(IFNULL('.$t_tot.', 0), " (", IFNULL(M.distinct_tot, 0), ")")';
				}
				$t = 'SELECT '.$t1 .', '. $t_tot .' AS tot FROM ('. $t2 . $csl_kind2 .' FROM ( SELECT DD.code_name AS subject, A.ck1, A.target ';
				if($args['sch_kind'] == $sch_kind_cslkind){
					$t .= ', (SELECT count(*) as distinct_tot FROM '. $this->tbl_counsel .' C WHERE '.$where.' ) AS distinct_tot ';
				}
// 				if($args['sch_kind'] == $sch_kind_cslkind){
// 					$t .= ', '.$t3;
// 				}
				$t .= 'FROM (SELECT code_name, s_code, dsp_code FROM sub_code ';
				$t .=' WHERE m_code = "'.$datas2.'" ) DD ';
				$t .='LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_asso_cd.' as target2, '.$target_field1.' as target FROM '. $this->tbl_counsel .' C ';
					
				if($args['sch_kind'] == $sch_kind_cslkind){
					$t .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}
			
				$where_ck = '';
				if($args['sch_kind'] == $sch_kind_cslkind){ //상담유형이면 where 절 추가
					$csl_kind = self::get_sub_code_by_m_code_v2(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE);
					$csl_kind_code = '"'. str_replace(',', '","', $csl_kind[0]->s_code) .'"';
					$where_ck .= 'AND CS2.s_code IN ('. $csl_kind_code .') ';
				}
			
				$t .= 'WHERE '. $where .' '. $where_ck
					.'GROUP BY '.$target_field1.', '.$target_asso_cd .' ) A ON A.target2 = DD.s_code '
					.') S
					) M ';
							
				$query_tot = $t;
				//echof($t);
	
				$total = 1;
				if(!empty($query_tot)) {
					$rst = $this->db->query($query_tot);
					$rstRtn['data_tot'] = $rst->result();
					$total = $rstRtn['data_tot'][0]->tot;
						
					$total = explode('(', $total);
					$total = $total[0];
						
					if($total == 0){
						$rstRtn['data_tot'][0]->tot .= ' (0%)';
					}
					else{
						$rstRtn['data_tot'][0]->tot .= ' (100%)';
					}
				}
				if($args['sch_kind'] != $sch_kind_cslkind){
					$csl_kind2 = '';
					$csl_kind3 = '';
				}
					
				// 메인쿼리
				$q = '';
				$index = 1;
				$q1 = 'SELECT M.subject ';
				$q_tot = '';
				$q2 = 'SELECT S.subject ';
				foreach($datas1 as $data) {
					$per = 'ROUND(((sum(M.ck'.$index.'))/'.$total.' )*100, 1)';
					$q1 .= ', CONCAT(IFNULL(sum(M.ck'.$index.'), 0), " (", IFNULL('.$per.', 0), "%)") as ck'.$index;
	
					if($q_tot != '') $q_tot .= ' + ';
	
					$q_tot .= 'sum(M.ck'.$index.')';
					$q2 .= ', IF(S.target = "'.$data->s_code.'", S.ck1, 0) as ck'.$index;
	
					$index++;
				}
	
				if($total != 0){
					$per = 'ROUND((('.$q_tot.')/'.$total.' )*100, 1)';
					if($args['sch_kind'] == $sch_kind_cslkind){ //상담유형
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), "(", IFNULL(M.distinct_tot, 0), ") (", IFNULL('.$per.', 0),"%)")';
					}
					else {
						$q_tot = 'CONCAT( IFNULL('.$q_tot.', 0), " (", IFNULL('.$per.', 0), "%)")';
					}
				}
	
				$q = $q1 .', '. $q_tot .' as tot FROM ('.$q2.', S.dsp_order '.$csl_kind2.' from ( SELECT DD.code_name AS subject, A.ck1, A.target, DD.dsp_order '. $csl_kind3;
				$q .= ' FROM (SELECT SC.code_name, SC.s_code, SC.dsp_order '.$csl_kind4.' FROM sub_code SC ';
				if($args['sch_kind'] == $sch_kind_cslkind){
					$q .= 'LEFT JOIN (SELECT COUNT(*) as distinct_tot, '.$target_asso_cd.' FROM '. $this->tbl_counsel .' C
					WHERE '.$where.' GROUP BY '.$target_asso_cd.') DT ON SC.s_code = '.$target_field3.' ';
				}
				$q .= 'WHERE m_code = "'.$datas2.'" /*'.$where_asso.'*/) DD ';
				$q .= 'LEFT JOIN (SELECT count(C.seq) AS ck1, '. $target_asso_cd.' as target2, '.$target_field1.' as target FROM '. $this->tbl_counsel .' C ';
					
				if($args['sch_kind'] == $sch_kind_cslkind){ //상담유형
					$q .= 'INNER JOIN '. $this->tbl_counsel_sub .' CS2 ON C.seq=CS2.csl_seq ';
				}
	
				$q .= 'WHERE ' . $where .' '. $where_ck
					.'GROUP BY '.$target_field1.', '.$target_asso_cd.') A ON A.target2 = DD.s_code '
							.') S
					) M
					GROUP BY M.subject ORDER BY M.dsp_order ';
									
				$query = $q;
				//echof($q);
	
				// # 쿼리 실행
				if(!empty($query)) {
					$rst = $this->db->query($query);
					$rstRtn['data'] = $rst->result();
				}
	
				$rstRtn['tot_cnt'] = count($rstRtn['data']);
			}
		}

		return $rstRtn;
	}

	
	
	/**
	 * 
	 * 게시판
	 * 
	 */
	
	/**
	 * 게시판 목록 조회 메서드
	 * 
	 * @param array $args
	 * @return array
	 */
	public function get_board_list($args) {
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		$where = 'WHERE 1=1 ';

		// target
		// - 전체 검색
		if($args['target'] == 'all') {
			// keyword
			if($args['keyword']) {
				$where .= ' AND (B.title LIKE "%'. $args['keyword'] .'%" OR '
					.'B.content LIKE "%'. $args['keyword'] .'%" OR '
					.'O.oper_id LIKE "%'. $args['keyword'] .'%" OR '
					.'O.oper_name LIKE "%'. $args['keyword'] .'%") ';
			}
		}
		else {
			// keyword
			if($args['keyword']) {
				$where .= ' AND '. $args['target'] .' LIKE "%'. $args['keyword'] .'%" ';
			}
		}
		
		// 상담 방법 코드 : s_code
		if($args['csl_way']) {
			$where .= ' AND B.s_code = "'. $args['csl_way'] .'" ';
		}

		// 상담일 : search_date_begin, search_date_end
		if($args['search_date_begin']) {
			$where .= ' AND B.reg_date BETWEEN "'. $args['search_date_begin'] .' 00:00:00" AND "'. $args['search_date_end'] .' 23:59:59" ';
		}
		
		$brd_id = $args['brd_id'];

		// fields
		$fields = 'B.* ';
		

		$query1 = 'SELECT '. $fields .', O.oper_name ';
		$query2 = 'FROM '. $brd_id .' B '
			.'INNER JOIN '. $this->tbl_oper .' O ON B.oper_id = O.oper_id ';
			
		// 운영게시판 외 join 추가
		if($args['brd_id'] != $this->tbl_brd_oper) {
			$query1 .= ', S.code_name ';
			$query2 .= 'INNER JOIN '. $this->tbl_sub_code .' S ON B.s_code = S.s_code ';
		}
		$query2 .= ' '. $where .' ';

		$query3 = 'ORDER BY B.seq DESC '
			.'LIMIT '. $offset .', '. $limit;

		$q = $query1 . $query2 . $query3;
		// echof($q);exit;
		
		// query
		$rs = $this->db->query($q);
		
		$rstRtn['data'] = $rs->result();
		
		// total count
		$query1 =  'SELECT COUNT(B.seq) as cnt ';
		$rs = $this->db->query($query1 . $query2);
		$tmp = $rs->result();

		// $rstRtn['query'] = $q;

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;
		
		return $rstRtn;
	}

	
	/**
	 * 해당 게시판의 공지글만 가져온다
	 * 
	 * @param array $args
	 * @return array
	 */
	public function get_noti_articles($args) {
		$table_name = $args['tbl_name'];

		$query = 'SELECT B.*, O.oper_name, "notice_article" '
			.'FROM '. $table_name .' B '
			.'INNER JOIN '. $this->tbl_oper .' O ON B.oper_id = O.oper_id '
			.'WHERE NOW() BETWEEN B.notice_dt_begin AND B.notice_dt_end '
			.'ORDER BY B.notice_dt_end ASC';
		$rs = $this->db->query($query);
		$rstRtn['data'] = $rs->result();

		return $rstRtn;
	}


	/**
	 * 운영자게시판의 공지 중 팝업 공지 데이터 리턴
	 * 
	 * @param array $args
	 * @return array
	 */
	public function get_notice_popup_data($args) {

		$seq = $args['seq'];
		$where = array(
			'seq' => $seq
		);
		$rs = $this->db->select($this->tbl_brd_oper.'.*,' . $this->tbl_oper.'.oper_name')
			->join($this->tbl_oper, $this->tbl_oper.'.oper_id='.$this->tbl_brd_oper.'.oper_id')
			->get_where($this->tbl_brd_oper, $where);
		$rstRtn = $rs->result();

		return $rstRtn;

	}

	
	/**
	 * 해당 게시판의 글 수정시 해당 게시물의 첨부파일의 이름을 리턴한다.
	 * 
	 * @param array $args
	 * @return array
	 */
	public function get_exist_file_data_board($args) {
		$rst = $this->db->select('file_name,file_name_org')->get_where($args['table_name'], array('seq'=> $args['seq']) );
		$rstRtn = $rst->result();

		return $rstRtn;
	}


	/**
	 * 등록 메서드
	 *
	 * @param array $args
	 * @return array
	 */
	public function add_board($args) {
		$args['reg_date'] = get_date();
		
		$table_name = $args['table_name'];
		unset($args['table_name']);
		unset($args['seq']);
		
		$this->db->insert($table_name, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'error-fail insert';
		$rstRtn['new_id'] = '';
		
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = '';
			$rstRtn['new_id'] = $this->db->insert_id();
		}
		
		return $rstRtn;
	}	

	
	/**
	 * 수정 메서드
	 *
	 * @param array $args
	 * @return boolean
	 */
	public function edit_board($args) {
		$where = array(
			'seq' => $args['seq']
		);
		
		$args['mod_date'] = get_date();

		$table_name = $args['table_name'];

		// 불필요한 데이터 삭제
		unset($args['table_name']);
		unset($args['seq']);
		
		// 항상 1 리턴, 
		// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
		$rst = $this->db->where($where)->update($table_name, $args);
		// $this->get_last_query();
		
		return $rst;
	}


	/**
	 * 삭제 메서드
	 *
	 * @param array $args
	 * @return array $rstRtn['img_nm'] - 이미지 이름
	 */
	public function del_board($args) {
		$where = array(
			'seq' => $args['seq']
		);

		// 이미지 이름 반환
		$rs = $this->db->select('file_name')->get_where($args['brd_id'], $where);
		$rst = $rs->result();

		$rstRtn['file_name'] = '';
		if(count($rst) > 0) {
			$rstRtn['file_name'] = $rst[0]->file_name;
		}

		// 삭제
		$this->db->delete($args['brd_id'], $where);
		
		$affected_rows = $this->db->affected_rows();
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($affected_rows > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
	
	
	/**
	 * 게시물 조회
	 * 
	 * @param array $args
	 * @return array 
	 */
	public function get_board($args) {

		// view인 경우, counting
		if(isset($args['kind']) && $args['kind'] == 'view') {
			$query = 'UPDATE '. $args['brd_id'] .' SET view_cnt = view_cnt+1 WHERE seq = "'. $args['seq'] .'" ';
			$this->db->query($query);
		}

		$where = 'B.seq = "'. $args['seq'] .'" ';
		
		$fields = 'B.*, O.oper_name  ';
		// 공유게시판 외 상담유형코드 추가
		if($args['brd_id'] != $this->tbl_brd_oper) {
			$fields .= ', S.code_name ';
		}
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $args['brd_id'] .' B '
			.'INNER JOIN '. $this->tbl_oper.' O ON O.oper_id = B.oper_id ';
		
			// 공유게시판 외 join 추가
			if($args['brd_id'] != $this->tbl_brd_oper) {
				$query .= 'INNER JOIN '. $this->tbl_sub_code .' S ON B.s_code = S.s_code ';
			}

			$query .= 'WHERE '. $where;
			
		// $rstRtn['query'] = $query;
		// echof($query);


		// query
		$rs = $this->db->query($query);
		
		$rstRtn= $rs->result();
				
		return $rstRtn;
	}
	

	/**
	 * 기존 이미지 파일명 가져오기
	 *
	 * @param array $args
	 * @return string image name
	 */
	public function get_exist_img_name($args) {
		$where = array(
			'seq' => $args['seq']
		);
		$rst = $this->db->select('file_name')->get_where($args['brd_id'], $where);
		
		$file_nm = '';
		if(count($rst) > 0) {
			$rstRtn = $rst->result();
			$file_nm = $rstRtn[0]->file_name;
		}
		
		return $img_nm;
	}


	/**
	 * 운영자게시판 공지게시물 중 팝업 게시물 seq 리턴
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_popup_notice() {

		$query = 'SELECT GROUP_CONCAT(seq) as seq '
			.'FROM '. $this->tbl_brd_oper .' '
			.'WHERE notice_yn=1 AND popup_yn=1 AND "'. get_date() .'" BETWEEN notice_dt_begin AND notice_dt_end';
		$rs = $this->db->query($query);

		$rst = $rs->result();
		$rstRtn = $rst[0]->seq;

		return $rstRtn;
	}
	
	

	/****************************************************************************
	 * 
	 * 
	 * 
	 * 
	 * 상담 (개인,사용자) 공통
	 * 
	 * 
	 * 
	 * 
	 ***************************************************************************/
	
	
	/**
	 * 상담 목록 조회 <관련상담>,<불러오기>
	 *
	 * 사용하는 view : list_popup_view, rel_list_popup_view
	 *
	 *	[권한] <br>
	 *	서울시 계정 : 운영자구분이 옴부즈만인 계정의 모든 상담글 조회,수정,삭제 가능(수정,삭제는 20160728 요청건) <br>
	 * 옴부즈만 계정 : 자신의 상담글만 조회, 수정, 삭제 가능 <br>
	 * 일반 계정(권익센터 포함 OO센터로 되어 있는 계정) : 자신의 글만 전체 권한, 자신이 속한 소속 상담원의 상담 조회, 인쇄만 가능<br>
	 *
	 * 공유하기 팝업 호출인 경우 공유하기 인 상담만을 소속과 무관하게 보여준다. 2017.01.18 dylan<br>
	 * <관련상담> 수정인 경우 팝업 목록,검색시 보고있는 상담은 대상에서 제외한다.<br>
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_counsel_list_pop($args) {
		 
		//   	echof($args);
	
		// 암호키
		$key = $this->get_key();
		 
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
	
		// 기본적으로 자신이 속한 기관의 상담만 가져온다.
		$where = 'WHERE 1=1 ';
	
		// master 제외
		if(isset($args['is_master']) && $args['is_master'] != 1) {
	
			// 권한 부분 쿼리 (상담목록 쿼리와 동일)
			$where .= self::_get_counsel_comm_query();
		}
	
		// <관련상담> 수정인 경우 팝업 목록,검색시 보고있는 상담은 대상에서 제외한다.
		if($args['seq'] != '') {
			$where .= ' AND C.seq<>'. $args['seq'] .' ';
		}
	
		// <불러오기> 팝업인 경우
		// - 관련상담 조건 + 공유하기 인 상담
		if(isset($args['csl_share_yn'])) {
			$where .= ' OR C.csl_share_yn = "'. CFG_SUB_CODE_CSL_SHARE_YN .'" ';
		}
		
		// 검색 시작 ------------------------------------------------------
		$search = 'WHERE 1=1 ';
		$inner_join_counsel_keyword = '';
		$keyword = $args['keyword'];
	
		// 상담방법 : csl_way
		if($args['csl_way']) {
			$search .= 'AND Q1.s_code = "'. $args['csl_way'] .'" ';
		}
	
		// 상담일 : search_date_begin, search_date_end
		// - 양쪽 다 있는 경우
		if($args['search_date_begin'] != '' && $args['search_date_end'] != '') {
			$search .= 'AND Q1.csl_date BETWEEN "'. $args['search_date_begin'] .'" AND "'. $args['search_date_end'] .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($args['search_date_begin'] != '' && $args['search_date_end'] == '') {
			$begin_year = explode(' ', $args['search_date_begin']);
			$search .= 'AND Q1.csl_date LIKE "'. $begin_year[0] .'%" ';
		}
	
		// <검색>
		// 업종
		if($args['target'] == 'counsel_comp_kind') {
			if($args['target_comp_kind'] != '') {
				$search .= 'AND Q1.comp_kind = "'. $args['target_comp_kind'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.comp_kind_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 주제어
		else if($args['target'] == 'counsel_keyword') {
			// 해당 코드로 검색
			if($args['target_keyword'] != '') {
				$inner_join_counsel_keyword = 'INNER JOIN '. $this->tbl_counsel_sub .' CS ON C.seq=CS.csl_seq AND CS.s_code="'. $args['target_keyword'] .'" '
						.'INNER JOIN '. $this->tbl_sub_code .' CS1 ON CS.s_code = CS1.s_code AND CS1.m_code="'. CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD .'"';
			}
			// 주제어가 있는 레코드만 추출
			else{
				$inner_join_counsel_keyword = 'INNER JOIN (SELECT TCS.csl_seq FROM '. $this->tbl_counsel_sub .' TCS INNER JOIN '. $this->tbl_sub_code .' SC1 ON TCS.s_code = SC1.s_code AND SC1.m_code="'. CFG_SUB_CODE_OF_BIZ_COUNSEL_KEYWORD .'" GROUP BY TCS.csl_seq) CS1 ON C.seq=CS1.csl_seq ';
			}
		}
		// 처리결과
		else if($args['target'] == 'counsel_proc_rst') {
			if($args['target_csl_proc_rst'] != '') {
				$search .= 'AND Q1.csl_proc_rst = "'. $args['target_csl_proc_rst'] .'" ';
			}
			if($keyword != '') {
				$search .= 'AND Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ';
			}
		}
		// 검색어가 있는 경우만 해당
		if($keyword != '') {
			// 전체검색
			if($args['target'] == '') {
				$search .= ' AND ( Q1.csl_title LIKE "%'. $keyword .'%"
					OR Q1.csl_content LIKE "%'. $keyword .'%"
					OR Q1.csl_reply LIKE "%'. $keyword .'%"
					OR Q1.oper_id LIKE "%'. $keyword .'%"
					OR Q1.oper_name LIKE "%'. $keyword .'%" '
				.'OR Q1.comp_kind_etc LIKE "%'. $keyword .'%" ' // 업종 기타
				.'OR Q1.csl_proc_rst_etc LIKE "%'. $keyword .'%" ' // 처리결과 기타
				.'OR Q1.csl_name="'. $keyword .'" '//대표자명
				.'OR Q1.csl_tel_sch="'. $keyword .'" ' //전화번호 뒤 4자리
				.') ';
			}
			else {
				// 상담자 id
				if($args['target'] == 'counsel_oper_id') {
					$search .= 'AND Q1.oper_id LIKE "%'. $keyword .'%" ';
				}
				// 상담자명
				else if($args['target'] == 'counsel_oper_name') {
					$search .= 'AND Q1.oper_name LIKE "%'. $keyword .'%" ';
				}
				// 대표자명
				else if($args['target'] == 'counsel_csl_name') {
					$search .= ' AND Q1.csl_name="'. $keyword .'" ';
				}
				// 내담자 전화번호 뒤 4자리 검색
				else if($args['target'] == 'counsel_csl_tel') {
					$search .= ' AND Q1.csl_tel_sch="'. $keyword .'" ';
				}
				// 검색대상: 제목,상담내용,답변
				else {
					$search .= 'AND '. $args['target'] .' LIKE "%'. $keyword .'%" ';
				}
			}
		}
		// 검색 끝 ------------------------------------------------------
	
		// fields
		$fields2 = 'Q1.seq,Q1.csl_title,Q1.csl_content
  		,Q1.csl_reply,Q1.csl_proc_rst,Q1.csl_proc_rst_etc,Q1.s_code,Q1.csl_date,Q1.oper_name,Q1.oper_id,Q1.oper_kind
  		,Q1.csl_name,Q1.csl_tel, Q1.csl_tel_sch
			,Q1.comp_kind,Q1.comp_kind_etc
  		,Q1.comp_addr,Q1.comp_addr_etc
  		,Q1.emp_cnt,Q1.emp_cnt_etc
  		,Q1.emp_paper_yn
			,Q1.emp_insured_yn
  		,Q1.csl_ref_seq
			,Q1.csl_method_nm';
		$fields2 .= ',Q1.gender,Q1.ages,Q1.live_addr,Q1.live_addr_etc,Q1.work_kind,Q1.work_kind_etc,Q1.emp_kind,Q1.emp_kind_etc
			,Q1.ave_pay_month,Q1.work_time_week ';//관련상담에서 사용하는 컬럼
		$fields1 = 'C.seq,C.csl_title,C.csl_content
			,C.csl_reply,C.csl_proc_rst,C.csl_proc_rst_etc,C.s_code,C.csl_date,O.oper_name,O.oper_id,O.oper_kind
			,AES_DECRYPT(C.csl_name, HEX(SHA2("'. $key .'",512))) as csl_name
			,AES_DECRYPT(C.csl_tel, HEX(SHA2("'. $key .'",512))) as csl_tel
			,AES_DECRYPT(C.csl_tel_sch, HEX(SHA2("'. $key .'",512))) as csl_tel_sch
			,C.comp_kind,C.comp_kind_etc
			,C.comp_addr,C.comp_addr_etc
			,C.emp_cnt,C.emp_cnt_etc
			,C.emp_paper_yn
			,C.emp_insured_yn
			,C.csl_ref_seq
			,S.code_name as csl_method_nm';
		$fields1 .= ',C.gender,C.ages,C.live_addr,C.live_addr_etc,C.work_kind,C.work_kind_etc,C.emp_kind,C.emp_kind_etc
			,C.ave_pay_month,C.work_time_week ';//관련상담에서 사용하는 컬럼
		$query1 = 'SELECT '. $fields2 .' ';
		$query2 = 'FROM (SELECT '. $fields1 .' '
			.'FROM '. $this->tbl_counsel .' C '
			.$inner_join_counsel_keyword
			.'INNER JOIN '. $this->tbl_sub_code .' S ON C.s_code = S.s_code ' // 상담방법
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON C.csl_proc_rst = S2.s_code ' // 처리결과
			.'INNER JOIN '. $this->tbl_sub_code .' S4 ON C.comp_kind = S4.s_code ' // 업종
			.'INNER JOIN '. $this->tbl_oper .' O ON C.oper_id = O.oper_id '
			.$where .') Q1 ';

		// 엑셀 다운로드 일 경우 limit 제외
		$orderby = ' ORDER BY Q1.csl_date DESC, Q1.seq DESC ';
		$orderby .= 'LIMIT '. $offset .', '. $limit;

		// query
		$q = $query1 . $query2 . $search . $orderby ;
		$rs = $this->db->query($q);
		$rstRtn['data'] = $rs->result();
// 		echof($rstRtn['data']);
		// 		echof($q);

		// total count
		$q =  $query1 . $query2 . $search;
		$rs = $this->db->query($q);
		$tmp = $rs->result();
		$rstRtn['tot_cnt'] = count($tmp);

		return $rstRtn;
	}
	
	
	
	
	/**
	 * 
	 * 상담목록, 관련상담/불러오기 팝업 목록에서 사용하는 권한에 따른 공통 퀴리 생성 함수
	 *
	 * @return string $query query string
	 */
	protected function _get_counsel_comm_query() {
		$query = '';
		// 기본 - 권익센터,OO센터 직원 : 소속 상담만 노출
		if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK1) {
			$query .= 'AND O.s_code = "'. $this->session->userdata(CFG_SESSION_ADMIN_AUTH_ASSO_CD) .'" ';
		}
		// 서울시 : 자신의 글과 옴부즈만 상담만 노출(참고 : 서울시는 상담등록을 하지 않는다고 한다.)
		else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK2) {
			$query .= 'AND ('
				.'O.oper_kind = "'. CFG_OPERATOR_KIND_CODE_OK3 .'" OR C.oper_id = "'. $this->session->userdata(CFG_SESSION_ADMIN_ID) .'"'
			.') ';
		}
		// 옴부즈만 : 자신의 상담만 노출
		else if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) == CFG_OPERATOR_KIND_CODE_OK3) {
			$query .= 'AND C.oper_id = "'. $this->session->userdata(CFG_SESSION_ADMIN_ID) .'" ';
		}
		// 자치구 공무원 : 소속 자치구의 옴부즈만 상담만 조회,인쇄 가능 - 2016.08.09 추가
		else {
			$query .= 'AND ('
				.'O.oper_kind = "'. CFG_OPERATOR_KIND_CODE_OK3 .'" AND O.oper_kind_sub = "'. $this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE_SUB) .'"'
			.') ';
		}
	
		return $query;
	}
	
	
	
	
	
	//#################################################################################################################
	// Operator Manage
	//#################################################################################################################
	
	//=================================================================================================================
	// Oper
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// change_pwd : 운영자 비빌번호 변경
	//-----------------------------------------------------------------------------------------------------------------
	public function change_pwd($args) {
		//
		$enc_pwd = dv_hash($args['oper_pwd'], CFG_ENCRYPT_KEY);
	
		$where = array(
				'oper_id' => $args['oper_id']
				,'oper_pwd' => $enc_pwd
		);
		$rs = $this->db->select('oper_id')->get_where($this->tbl_oper, $where);
		$rst = $rs->result();
	
		// 비번이 일치하면, 변경처리
		if(count($rst) >= 1) {
			$data = array(
					'oper_pwd' => dv_hash($args['new_pwd'], CFG_ENCRYPT_KEY)
			);
			unset($where['oper_pwd']);
			$this->db->where($where)->update($this->tbl_oper, $data);
				
			$rstRtn['rst'] = 'succ';
		}
		else {
			$rstRtn['rst'] = 'fail';
		}
	
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// change_oper_pwd : 마스터 관리자가 운영자 비번 변경 처리 : 추가 2016.04.15 23:31:00
	//-----------------------------------------------------------------------------------------------------------------
	public function change_oper_pwd($args) {
	
		$where = 'oper_id = "'. $args['oper_id'] .'"';
	
		$data = array(
				'oper_pwd' => dv_hash($args['new_pwd'], CFG_ENCRYPT_KEY)
		);
		$this->db->where($where)->update($this->tbl_oper, $data);
			
		$rstRtn['rst'] = 'succ';
	
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// id_check : 아이디 중복 확인
	//-----------------------------------------------------------------------------------------------------------------
	public function id_check($args) {
		$where = 'oper_id = "'. $args['oper_id'] .'" ';
	
		$rs = $this->db->select('oper_id')->get_where($this->tbl_oper, $where);
		if($rs->num_rows > 0) {
			$rstRtn['rst'] = 'fail';
		}
		else {
			$rstRtn['rst'] = 'succ';
		}
	
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_oper_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper_list($args) {
		// is_master
		$where = '1=1 ';
		if($args['is_master'] == 0) {
			$where = 'O.oper_auth_grp_id <> "'. CFG_AUTH_CODE_ADM_MASTER .'" ';
		}
	
		// use_yn
		if($args['use_yn'] !== 'all') {
			$where .= 'AND O.use_yn = '. $args['use_yn'] .' ';
		}
	
		// keyword
		if($args['keyword']) {
			// 전체 검색
			if(empty($args['target'])) {
				$where .= 'AND (S.code_name LIKE "%'. $args['keyword'] .'%" OR '
					. 'A.oper_auth_name LIKE "%'. $args['keyword'] .'%" OR '
					. 'O.oper_id LIKE "%'. $args['keyword'] .'%" OR '
					. 'O.oper_name LIKE "%'. $args['keyword'] .'%") ';
			}
			else {
				$where .= 'AND '. $args['target'] .' LIKE "%'. $args['keyword'] .'%" ';
			}
		}
	
		// date
		if($args['search_date_begin']) {
			$where .= 'AND O.reg_date BETWEEN "'. $args['search_date_begin'] .' 00:00:00" AND "'. $args['search_date_end'] .' 23:59:59" ';
		}
	
		// 소속코드 : s_code
		if($args['s_code']) {
			$where .= 'AND O.s_code = "'. $args['s_code'] .'" ';
		}
	
		// 권한코드 : oper_auth_grp_id
		if($args['oper_auth_grp_id']) {
			$where .= ' AND O.oper_auth_grp_id = "'. $args['oper_auth_grp_id'] .'" ';
		}
	
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
	
		// fields
		$fields = 'O.oper_id, O.oper_name, O.use_yn, O.reg_date, S.code_name, A.oper_auth_name, O.oper_auth_grp_id ';
		$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_oper .' O '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON O.s_code = S.s_code '
			.'INNER JOIN '. $this->tbl_auth_grp .' A ON O.oper_auth_grp_id = A.oper_auth_grp_id '
			.'WHERE '. $where .' '
			.'ORDER BY O.reg_date DESC, O.oper_id ASC '
			.'LIMIT '. $offset .', '. $limit;
		// $rstRtn['query'] = $query;

		// query
		$rs = $this->db->query($query);

		$rstRtn['data'] = $rs->result();

		// total count
		$rs = $this->db->query('SELECT COUNT(O.oper_id) as cnt '
			.'FROM '. $this->tbl_oper .' O '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON O.s_code = S.s_code '
			.'INNER JOIN '. $this->tbl_auth_grp .' A ON O.oper_auth_grp_id = A.oper_auth_grp_id '
			.'WHERE '. $where );
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;

		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_oper : operator 조회 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper($args) {
		$where = 'O.oper_id = "'. $args['oper_id'] .'" ';
	
		$fields = 'O.oper_id, O.oper_name, O.use_yn, O.reg_date, O.etc, O.hp, O.tel, O.email, O.s_code, O.oper_kind, O.oper_kind_sub'
			.',S.code_name, A.oper_auth_grp_id, A.oper_auth_name ';
		$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_oper .' O '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON O.s_code = S.s_code '
			.'INNER JOIN '. $this->tbl_auth_grp .' A ON O.oper_auth_grp_id = A.oper_auth_grp_id '
			.'WHERE '. $where;
				
		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);

		$tmp = $rs->result();

		$rstRtn['oper_id'] = $tmp[0]->oper_id;
		$rstRtn['oper_name'] = $tmp[0]->oper_name;
		$rstRtn['use_yn'] = $tmp[0]->use_yn;
		$rstRtn['s_code'] = $tmp[0]->s_code;
		$rstRtn['code_name'] = $tmp[0]->code_name;
		$rstRtn['oper_auth_grp_id'] = $tmp[0]->oper_auth_grp_id;
		$rstRtn['oper_auth_name'] = $tmp[0]->oper_auth_name;
		$rstRtn['oper_kind'] = $tmp[0]->oper_kind;
		$rstRtn['oper_kind_sub'] = $tmp[0]->oper_kind_sub;
		$rstRtn['hp'] = $tmp[0]->hp;
		$rstRtn['tel'] = $tmp[0]->tel;
		$rstRtn['email'] = $tmp[0]->email;
		$rstRtn['etc'] = $tmp[0]->etc;
		$rstRtn['reg_date'] = $tmp[0]->reg_date;

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}

		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_oper : operator NEW 저장 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function add_oper($args) {
		$args['reg_date'] = get_date();
	
		$rs = $this->db->insert($this->tbl_oper, $args);
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
	
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_oper : operator 수정 저장 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_oper($args) {
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = '';
		$rst = null;
	
		// 관리자가 아닌 경우만
		if($args['is_master'] != 1) {
			$enc_pwd = dv_hash($args['oper_pwd'], CFG_ENCRYPT_KEY);
			$where = 'oper_id = "'. $args['oper_id'] .'" AND oper_pwd="'. $enc_pwd .'" ';
			$rs = $this->db->select('oper_pwd')->get_where($this->tbl_oper, $where);
			$rst = $rs->result();
		}
	
		// 관리자 또는 데이터가 있으면 변경처리
		if($args['is_master'] == 1 || $args['is_master'] != 1 && count($rst) >= 1) {
			//
			$where = array(
					'oper_id' => $args['oper_id']
			);
				
			unset($args['oper_id']);
			unset($args['oper_pwd']);
			unset($args['is_master']);
				
			$args['mod_date'] = get_date();
				
			// echof($args);
				
			// 항상 1 리턴,
			// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
			$rst = $this->db->where($where)->update($this->tbl_oper, $args);
	
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
		// 비번이 틀린 경우
		else {
			$rstRtn['msg'] = 'not_match_password';
		}
	
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_oper : operator 다중 삭제 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_oper($oper_id) {
		$arr_oper_id = explode(CFG_AUTH_CODE_DELIMITER, $oper_id);
	
		$exist_oper_id = '';
		foreach($arr_oper_id as $id) {
			$rst = $this->db->select('seq')->get_where($this->tbl_counsel, array('oper_id'=>$id));
	
			if($rst->num_rows > 0) {
				if($exist_oper_id != '') $exist_oper_id .= CFG_AUTH_CODE_DELIMITER;
				$exist_oper_id .= $id;
			}
			else {
				$query = 'DELETE FROM '. $this->tbl_oper .' WHERE oper_id IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			}
		}
	
		return $exist_oper_id;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// use_or_not_use_oper : operator 다중 사용/중지 처리 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function use_or_not_use_oper($args) {
		$oper_id = '"'. str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['oper_id']) . '"';
	
		$query = 'UPDATE '. $this->tbl_oper .' SET use_yn = '. $args['use_yn'] .' WHERE oper_id IN ('. $oper_id .') ';
	
		$rst = $this->db->query($query);
	
		return $rst;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_oper : oper 삭제 - 관리자 전용
	//-----------------------------------------------------------------------------------------------------------------
	public function del_oper($args) {
		$where = array(
			'oper_id' => $args['oper_id']
		);
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
	
		// 운영자에게 할당된 내용이 있으면 삭제하지 못한다.
		$rs = $this->db->select('seq')->get_where($this->tbl_counsel, $where);
		// echof($rs->num_rows);
	
		if($rs->num_rows > 0) {
			$rstRtn['rst'] = 'exist';
			$rstRtn['msg'] = 'exist group id on DB';
		}
		else {
			$this->db->delete($this->tbl_oper, $where);
				
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
	
		return $rstRtn;
	}
	
	
	
	
	//#################################################################################################################
	// Auth Group Manage
	//#################################################################################################################
	
	//=================================================================================================================
	// Auth Group
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_auth_grp_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_auth_grp_list($args) {
		// 마스터관리자 데이터는 가져오지 않는다.
		$where = ' oper_auth_grp_id <> "'. CFG_AUTH_CODE_ADM_MASTER .'" ';
	
		// use_yn
		if(isset($args['use_yn']) && $args['use_yn'] !== 'all') {
			$where .= ' AND use_yn = '. $args['use_yn'];
		}
	
		// keyword
		if(isset($args['oper_auth_name']) && $args['oper_auth_name']) {
			$where .= ' AND oper_auth_name LIKE "%'. $args['oper_auth_name'] .'%" ';
		}
	
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
	
		// fields
		$fields = 'oper_auth_grp_id, oper_auth_name, use_yn, reg_date';
		$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_auth_grp .' '
			.'WHERE '. $where .' '
			.'ORDER BY reg_date DESC, oper_auth_name ASC '
			.'LIMIT '. $offset .', '. $limit;
		// $rstRtn['query'] = $query;

		// query
		$rs = $this->db->query($query);

		$rstRtn['data'] = $rs->result();

		// total count
		$rs = $this->db->query('SELECT COUNT(oper_auth_grp_id) as cnt '
			.'FROM '. $this->tbl_auth_grp .' '
			.'WHERE '. $where);
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;

		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_auth_grp_all_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_auth_grp_all_list($columns=NULL) {
		// fields
		$fields = '*';
		if(!is_null($columns)) {
			$fields = $columns;
		}
		$where = 'WHERE oper_auth_grp_id <> "'. CFG_AUTH_CODE_ADM_MASTER .'" AND use_yn = 1 ';
		$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_auth_grp .' '
			. $where .' '
			.'ORDER BY reg_date DESC, oper_auth_name ASC ';
		// $rstRtn['query'] = $query;

		// query
		$rs = $this->db->query($query);

		$rstRtn['data'] = $rs->result();

		// total count
		$rs = $this->db->query('SELECT COUNT(*) as cnt '
			.'FROM '. $this->tbl_auth_grp);
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;

		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_oper_asso_all_list : 운영자소속코드 데이터 리턴(대코드 M007)
	//-----------------------------------------------------------------------------------------------------------------
	public function get_oper_asso_all_list($columns=NULL) {
		// fields
		$fields = '*';
		if(!is_null($columns)) {
			$fields = $columns;
		}
		$where = 'WHERE m_code = "'. CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION .'" AND use_yn = 1 ';
		$query = 'SELECT '. $fields .' '
			.'FROM '. $this->tbl_sub_code .' '
			. $where
			.'ORDER BY dsp_order ASC, code_name ASC ';
		// $rstRtn['query'] = $query;

		// query
		$rs = $this->db->query($query);

		$rstRtn['data'] = $rs->result();

		// total count
		$rs = $this->db->query('SELECT COUNT(*) as cnt '
			.'FROM '. $this->tbl_sub_code .' '
			. $where);
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;

		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// use_or_not_use_auth_grp : 권한그룹 다중 사용/중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function use_or_not_use_auth_grp($args) {
		$grp_id = '"'. str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['oper_auth_grp_id']) . '"';
	
		$query = 'UPDATE '. $this->tbl_auth_grp .' SET use_yn = '. $args['use_yn'] .' WHERE oper_auth_grp_id IN ('. $grp_id .') ';
	
		$rst = $this->db->query($query);
	
		return $rst;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_auth_grp : 권한그룹 다중 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_auth_grp($args) {
		$arr_grp_id = explode(CFG_AUTH_CODE_DELIMITER, $args['oper_auth_grp_id']);
		$exist_grp_id = '';
		foreach($arr_grp_id as $id) {
			$rst = $this->db->select('oper_id')->get_where($this->tbl_oper, array('oper_auth_grp_id'=>$id));
			if($rst->num_rows > 0) {
				if($exist_grp_id != '') $exist_grp_id .= CFG_AUTH_CODE_DELIMITER;
				$exist_grp_id .= $id;
			}
			else {
				$query = 'DELETE FROM '. $this->tbl_auth_grp .' WHERE oper_auth_grp_id IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			}
		}
	
		return $exist_grp_id;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_auth : 그룹 권한 수정
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_auth_grp($args) {
		$where = array(
			'oper_auth_grp_id' => $args['oper_auth_grp_id']
		);
	
		unset($args['oper_auth_grp_id']);
	
		$args['mod_date'] = get_date();
	
		// 항상 1 리턴,
		// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
		$rst = $this->db->where($where)->update($this->tbl_auth_grp, $args);
		// $query = $this->get_last_query();
		// echof($query);
	
		return $rst;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_auth_grp : 그룹 추가
	//-----------------------------------------------------------------------------------------------------------------
	public function add_auth_grp($args) {
		// 그룹코드 생성
		$query = 'SELECT MAX(right(oper_auth_grp_id, 3)) as max_index FROM '
			. $this->tbl_auth_grp .' ';
		$rs = $this->db->query($query);

		$highest_index = 1;
		if($rs->num_rows > 0) {
			$rst = $rs->result();
			$highest_index = $highest_index + $rst[0]->max_index;
		}
		// 3자릿수 맞춤
		if(strlen($highest_index) == 1) $highest_index = '00'. $highest_index;
		if(strlen($highest_index) == 2) $highest_index = '0'. $highest_index;

		$new_grp_id = 'GRP'. $highest_index;

		// 저장할 데이터 조합
		$args['oper_auth_grp_id'] = $new_grp_id;
		$args['reg_date'] = get_date();
		$args['mod_date'] = get_date();

		$rs = $this->db->insert($this->tbl_auth_grp, $args);

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}

		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_auth_grp : 그룹 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_auth_grp($args) {
		$where = array(
			'oper_auth_grp_id' => $args['oper_auth_grp_id']
		);
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
	
		// 운영자에게 할당된 내용이 있으면 삭제하지 못한다.
		$rs = $this->db->select('oper_id')->get_where($this->tbl_oper, $where);
	
		// echof($rs->num_rows);
	
		if($rs->num_rows > 0) {
			$rstRtn['rst'] = 'exist';
			$rstRtn['msg'] = 'exist group id on DB';
		}
		else {
			// echof('select ok ');
			$this->db->delete($this->tbl_auth_grp, $where);
			// echof( $this->db->affected_rows()  );
				
			if($this->db->affected_rows() > 0) {
				$rstRtn['rst'] = 'succ';
				$rstRtn['msg'] = 'ok';
			}
		}
	
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_auth : 그룹 권한 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_auth($args) {
		$where = array(
				'oper_auth_grp_id' => $args['oper_auth_grp_id']
		);
		$rst = $this->db->select('oper_auth_grp_id, oper_auth_codes, oper_auth_name, etc')->get_where($this->tbl_auth_grp, $where);
		$tmp = $rst->result();
	
		$rstRtn['oper_auth_grp_id'] = $tmp[0]->oper_auth_grp_id;
		$rstRtn['oper_auth_codes'] = $tmp[0]->oper_auth_codes;
		$rstRtn['oper_auth_name'] = $tmp[0]->oper_auth_name;
		$rstRtn['etc'] = $tmp[0]->etc;
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
	
		return $rstRtn;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_base_code : 그룹 권한 Data 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_base_code() {
		$query = 'SELECT S.s_code,S.code_name FROM sub_code S '
			. 'INNER JOIN master_code M '
			. 'ON S.m_code = M.m_code '
			. 'WHERE S.m_code IN ("'. CFG_MASTER_CODE_COUNSEL_MANAGE .'",'
			.' "'. CFG_MASTER_CODE_COUNSEL_STATISTIC .'",'
			.' "'. CFG_MASTER_CODE_BOARD .'",'
			.' "'. CFG_MASTER_CODE_OPERATOR .'",'
			.' "'. CFG_MASTER_CODE_AUTH .'",'
			.' "'. CFG_MASTER_CODE_MANAGE_CODE .'", '
			.' "'. CFG_MASTER_CODE_LAW_HELP .'") '
			. 'ORDER BY S.m_code ASC, S.dsp_order ASC';
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();

		return $rstRtn;
	}
	
	
	
	
	//#################################################################################################################
	// Code Manage
	//#################################################################################################################
	
	//=================================================================================================================
	// Code
	//=================================================================================================================
	
	//-----------------------------------------------------------------------------------------------------------------
	// add_code : 코드 등록 - 소코드만 등록한다. 대/중 코드는 하드코딩 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function add_code($args) {
		$m_code = $args['m_code'];
	
		// 'C'로 시작하는 sub_code가 있는지 검사
		$rst = $this->db->query(
			'SELECT s_code FROM '. $this->tbl_sub_code .' WHERE s_code LIKE "C%" '
			);
		$tmp = $rst->result();
	
		// 처음
		if($rst->num_rows == 0) {
			$s_code_index = 0;
			$prefix = 'C';
		}
		else {
			$s_code = $tmp[$rst->num_rows - 1]->s_code;
			$prefix = $s_code[0];
			$s_code_index = substr($s_code, 1);
		}
		$highest_index = $s_code_index + 1;
		if(strlen($highest_index) == 1) $highest_index = '00'. $highest_index;
		if(strlen($highest_index) == 2) $highest_index = '0'. $highest_index;
		$highest_s_code = $prefix . $highest_index;
	
		$data = array(
			's_code' => $highest_s_code
			, 'm_code' => $m_code
			, 'code_name' => $args['code_name']
			, 'dsp_order' => $args['dsp_order']
			, 'dsp_code' => $args['dsp_code']
			, 'use_yn' => $args['use_yn']
			, 'desc' => $args['desc']
			, 'code_desc_yn' => $args['code_desc_yn']
			, 'reg_date' => get_date()
			, 'mod_date' => get_date()
		);
		if(isset($args['dsp_code'])) {
			$data['dsp_code'] = $args['dsp_code'];
		}
		$this->db->insert($this->tbl_sub_code, $data);
		
		// 설명글 테이블 저장
		if($args['code_desc_yn'] == 'Y') {
			$data = array(
				'code' => $highest_s_code
				,'code_desc' => $args['code_desc']
			);
			$this->db->insert($this->tbl_sub_code_desc, $data);
		}
		
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = '';
		}
	
		return $rstRtn;
	}
	
	/**
	 * 권리구제 유형 전용
	 * @param array $args data
	 */
	public function add_code_lhkind($args) {
		$m_code = $args['m_code'];
		$pcode_prefix = substr($m_code, 0, 1);
		$sub_prefix = 'C';
		if($pcode_prefix == 'C') {
			$sub_prefix = 'B';
		}
	
		// $pcode_prefix로 시작하는 sub_code가 있는지 검사
		$rst = $this->db->query(
			'SELECT s_code FROM '. $this->tbl_sub_code .' WHERE s_code LIKE "'. $sub_prefix .'%" '
			);
		$tmp = $rst->result();
	
		// 처음
		if($rst->num_rows == 0) {
			$s_code_index = 0;
			$prefix = $sub_prefix;
		}
		else {
			$s_code = $tmp[$rst->num_rows - 1]->s_code;
			$prefix = $s_code[0];
			$s_code_index = substr($s_code, 1);
		}
		$highest_index = $s_code_index + 1;
		if(strlen($highest_index) == 1) $highest_index = '00'. $highest_index;
		if(strlen($highest_index) == 2) $highest_index = '0'. $highest_index;
		$highest_s_code = $prefix . $highest_index;
	
		$data = array(
			's_code' => $highest_s_code
			, 'm_code' => $m_code
			, 'code_name' => $args['code_name']
			, 'dsp_order' => $args['dsp_order']
			, 'dsp_code' => $args['dsp_code']
			, 'use_yn' => $args['use_yn']
			, 'desc' => $args['desc']
			, 'reg_date' => get_date()
			, 'mod_date' => get_date()
		);
		$this->db->insert($this->tbl_sub_code, $data);
	
		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = '';
		}
	
		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// edit_code : 코드 수정
	//-----------------------------------------------------------------------------------------------------------------
	public function edit_code($args) {
		$where = array(
			's_code' => $args['s_code']
		);
		unset($args['s_code']);

		// 권리구제지원 - 사건유형에서 호출할 경우 code_desc 없어 예외처리
		$code_desc = isset($args['code_desc']) ? $args['code_desc'] : '';
		unset($args['code_desc']);
		
		// 항상 1 리턴,
		// - affected_rows() 함수는 정상 실행되었다 해도 실제 값이 변경되지 않은 경우는 0을 리턴하기 때문에 여기선 사용하지 않는다.
		$rst = $this->db->where($where)->update($this->tbl_sub_code, $args);
		// $query = $this->get_last_query();
		// echof($query);
		
		// 권리구제지원 - 사건유형에서는 미사용
		if(isset($args['code_desc_yn'])) {
			// 코드 설명글 테이블 업데이트
			$data = array(
				'code_desc' => $code_desc
			);
			if($args['code_desc_yn'] == 'Y') {
				// 있으면 update
				$w = array('code'=>$where['s_code']);
				// 등록된 설명글이 있는지 체크
				$rst = $this->db->select('count(*) as cnt')->get_where($this->tbl_sub_code_desc, $w);
	// 			log_message("ERROR", $rst->result()[0]->cnt);
				// 있으면 업데이트
				if($rst->result()[0]->cnt > 0) {
					log_message("ERROR", "update");
					$this->db->where($w)->update($this->tbl_sub_code_desc, $data);
				}
				// 없으면 insert
				else {
					log_message("ERROR", "insert");
					$data['code'] = $where['s_code'];
					$this->db->insert($this->tbl_sub_code_desc, $data);
				}
			}
			else {// 미사용
				$this->db->where($where)->update($this->tbl_sub_code, ['code_desc_yn'=>'N']);
			}
		}
	
		return $rst;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_code : 코드 조회
	//-----------------------------------------------------------------------------------------------------------------
	public function get_code($args) {
		$where = array(
			'A.s_code' => $args['s_code']
		);
		$rst = $this->db->select('A.s_code,A.code_name,A.dsp_code,A.dsp_order,A.use_yn,A.code_desc_yn,A.dsp_code,B.code_desc')
			->from($this->tbl_sub_code .' A')
			->join($this->tbl_sub_code_desc .' B', 'A.s_code=B.code', 'LEFT')
			->where($where)
			->get();
		$tmp = $rst->result();
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		if($this->db->affected_rows() > 0) 
		{
			$rstRtn = (array)$tmp[0];
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_code : 코드 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_code($args) {

		$rst1 = $this->db->select('s_code')->get_where($this->tbl_counsel, array('s_code'=>$args['s_code']));
		$rst2 = $this->db->select('s_code')->get_where($this->tbl_counsel_sub, array('s_code'=>$args['s_code']));
		$rst3 = $this->db->select('lh_seq')->get_where($this->tbl_lawhelp_kind_sub_cd, array('kind_sub_cd'=>$args['s_code']));
		
		$used_code = 'n';
		if($rst1->num_rows > 0 || $rst2->num_rows > 0 || $rst3->num_rows > 0) {
			$used_code = 'y';
		}
		else {
			// 설명글 테이블 삭제
			$this->db->delete($this->tbl_sub_code_desc, ['code'=>$args['s_code']]);
			
			// 코드 테이블 삭제
			$where = array(
				's_code' => $args['s_code']
			);
			$this->db->delete($this->tbl_sub_code, $where);
		}
	
		return $used_code;
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// get_code_list
	//-----------------------------------------------------------------------------------------------------------------
	public function get_code_list($args) {
		$where = 'A.m_code="'. $args['m_code'] .'" ';
	
		// use_yn
		if($args['use_yn'] !== 'all') {
			$where .= ' AND A.use_yn = '. $args['use_yn'];
		}
	
		// keyword
		if(isset($args['code_name']) && $args['code_name']) {
			$where .= ' AND A.code_name LIKE "%'. $args['code_name'] .'%" ';
		}
	
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
	
		// fields
		$fields = 'A.*, IFNULL(B.code_desc, "") as code_desc';
		$query = 'SELECT '. $fields .' 
			FROM '. $this->tbl_sub_code .' A 
			LEFT JOIN '. $this->tbl_sub_code_desc .' B ON A.s_code=B.code
			WHERE '. $where .'
			ORDER BY A.dsp_order ASC, A.code_name ASC, A.s_code ASC
			LIMIT '. $offset .', '. $limit;
		// $rstRtn['query'] = $query;

		// query
		$rs = $this->db->query($query);

		$rstRtn['data'] = $rs->result();

		// total count
		$rs = $this->db->query('SELECT COUNT(A.s_code) as cnt '
				.'FROM '. $this->tbl_sub_code .' A '
				.'WHERE '. $where);
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = $tmp[0]->cnt;

		return $rstRtn;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// del_multi_code : sub_code 다중 삭제
	//-----------------------------------------------------------------------------------------------------------------
	public function del_multi_code($code) {
		
		$arr_code = explode(CFG_AUTH_CODE_DELIMITER, $code);
		$used_codes = '';
		
		foreach($arr_code as $id) {
			// 이미 사용되었는지 체크
			$rst1 = $this->db->select('s_code')->get_where($this->tbl_counsel, array('s_code'=>$id));
			$rst2 = $this->db->select('s_code')->get_where($this->tbl_counsel_sub, array('s_code'=>$id));
			$rst3 = $this->db->select('lh_seq')->get_where($this->tbl_lawhelp_kind_sub_cd, array('kind_sub_cd'=>$id));

			if($rst1->num_rows > 0 || $rst2->num_rows > 0 || $rst3->num_rows > 0) {
				$used_codes .= 'y';
			}
			else {
				$used_codes .= 'n';
				$query = 'DELETE FROM '. $this->tbl_sub_code .' WHERE s_code IN ("'. $id .'") ';
				$rst = $this->db->query($query);
			}
		}
	
		return stripos($used_codes, 'y') !== FALSE ? 'y' : 'n';
	}
	
	
	
	//-----------------------------------------------------------------------------------------------------------------
	// use_or_not_use_code : sub_code 다중 사용/중지 처리
	//-----------------------------------------------------------------------------------------------------------------
	public function use_or_not_use_code($args) {
		$s_code = '"'. str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['s_code']) . '"';
	
		$query = 'UPDATE '. $this->tbl_sub_code .' SET use_yn = '. $args['use_yn'] .' WHERE s_code IN ('. $s_code .') ';
	
		$rst = $this->db->query($query);
	
		return $rst;
	}
	
	
	/**
	 * 코드 설명글 가져오기
	 * 
	 * 2종류 결과 리턴: <br> 
	 * code_m: 상위코드용, 
	 * code_s: 상위코드제외 하위코드만 
	 * 
	 * @param string $code 가져올 코드의 상위코드
	 * @return array 가져올 코드의 상위코드용, 상위코드제외 하위코드만 
	 */
	public function get_code_desc($code) {
		
		// 상위코드 설명글
		$rst = $this->db->select("A.code_name,IFNULL(B.code_desc, '') as code_desc", FALSE)
			->from($this->tbl_sub_code .' A')
			->join($this->tbl_sub_code_desc .' B', 'A.s_code=B.code', 'LEFT')
			->where(['A.code_desc_yn'=>'Y', 'A.m_code'=>$code])
			->get();
		$rstData = $rst->result();
		$rstRtn['code_m'] = array_map(function($std){
			$arr['code_name'] = $std->code_name;
			$arr['code_desc'] = str_replace('"', '\"', $std->code_desc);
			return $arr;
		}, $rstData);

		// 하위코드 설명글
		$rst = $this->db->select("A.code_name,A.s_code,IFNULL(B.code_desc, '') as code_desc", FALSE)
			->from($this->tbl_sub_code .' A')
			->join($this->tbl_sub_code_desc .' B', 'A.s_code=B.code', 'LEFT')
			->where(['A.code_desc_yn'=>'Y'])
			->where(['A.m_code<>"'=>$code .'"'], NULL, FALSE)
			->get();
		$rstData = $rst->result();
		$rstRtn['code_s'] = array_map(function($std){
			$arr['s_code'] = $std->s_code;
			$arr['code_name'] = $std->code_name;
			$arr['code_desc'] = str_replace('"', '\"', $std->code_desc);
			return $arr;
		}, $rstData);
		
		return $rstRtn;
	}
	
	
	
	
	
	/****************************************************************************
	 * 
	 * Helper Functions
	 * 
	 * 
	 ***************************************************************************/	

	/**
	 * get_lnb_menu_sub_code_by_m_code
	 *
	 * @param string $args LNB 메뉴의 대코드
	 * @return array
	 */
	public function get_lnb_menu_sub_code_by_m_code($args) {
		//		echof($args);
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$args = ' "'. $args .'" ';
		$fields = 'S.s_code,S.m_code,S.code_name as scode_name,S.dsp_order,S.dsp_code,M.code_name as mcode_name';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' S '.
				' INNER JOIN '. $this->tbl_master_code .' M ON M.m_code = S.m_code '.
				' WHERE S.use_yn = 1 AND S.m_code IN ('. $args .') ORDER BY S.m_code ASC, S.dsp_order ASC';
		$rs = $this->db->query($query);
	
		$rstRtn = $rs->result();
		
		return $rstRtn;
	}

	/**
	 * 권한관리 전용
	 *
	 * @param string $args LNB 메뉴의 대코드
	 * @return array
	 */
	public function get_sub_code_by_m_code_auth($args) {
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$args = ' "'. $args .'" ';
		$fields = 'S.s_code,S.m_code,S.code_name as scode_name,S.dsp_order,S.dsp_code,M.code_name as mcode_name';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' S '.
				' INNER JOIN '. $this->tbl_master_code .' M ON M.m_code = S.m_code '.
				' WHERE S.use_yn = 1 AND S.m_code IN ('. $args .') ORDER BY S.auth_mng_order ASC, S.dsp_order ASC';
		$rs = $this->db->query($query);
	
		$rstRtn = $rs->result();
	
		return $rstRtn;
	}

	/**
	 * get_code_nm_by_code
	 *
	 * @param string $args 코드명을 가져올 코드
	 * @return array
	 */
	public function get_code_nm_by_code($args) {
		// 사용중인 데이터만 가져온다
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$fields = 'code_name';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' '
			.' WHERE use_yn = 1 AND ';
		$query .= 's_code IN ('. $code .') ';

		$rs = $this->db->query($query);
		$rstRtn = $rs->result();

		return $rstRtn;
	}

	
	/**
	 * 상위코드에 해당하는 하위코드 조회
	 *
	 * get_sub_code_by_m_code_v2 메서드와 차이점 <BR>
	 * 1.2개 이상 처리 가능 동일
	 * 2.s_code 외 다수 컬럼을 <배열>로 리턴
	 * 
	 * @param string $args 가져올 서브코드의 대코드
	 * @param boolean $is_all 전체를 다 가져올지 여부, default FALSE
	 * @return array
	 */
	public function get_sub_code_by_m_code($args, $is_all=FALSE) {
		
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$fields = 's_code,m_code,code_name,dsp_order,dsp_code';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_sub_code .' '.
				' WHERE ';
		if($is_all == FALSE || $is_all == 0 ) {
			$query .= 'use_yn = 1 AND ';
		}
		$query .= 'm_code IN ('. $code .') ';
		$query .= 'ORDER BY dsp_order ASC, code_name ASC';
	
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
	
		return $rstRtn;
	}
	

	/**
	 * 입력된 대코드의 하위코드를 리턴한다. 
	 * 
	 * get_sub_code_by_m_code 메서드와 차이점 <BR>
	 * 1.2개 이상 처리 가능
	 * 2.s_code만 <문자열>로 리턴
	 *
	 * @param string $args 가져올 서브코드의 대코드
	 * @param boolean $is_all 전체를 다 가져올지 여부, default FALSE
	 * @return array
	 */
	public function get_sub_code_by_m_code_v2($args, $is_all=FALSE) {
		
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$query = 'SELECT CONCAT(GROUP_CONCAT(s_code)) as s_code 
			FROM '. $this->tbl_sub_code .'
			WHERE ';
		if($is_all == FALSE || $is_all == 0 ) {
			$query .= 'use_yn = 1 AND ';
		}
		$query .= 'm_code IN ('. $code .')
			GROUP BY m_code
			ORDER BY dsp_order ASC, code_name ASC';

			$rs = $this->db->query($query);
			$rstRtn = $rs->result();

			return $rstRtn;
	}

	
	/**
	 * 입력된 소속코드에 해당하는 운영자(사용자) 정보 조회
	 *
	 * @param string $args 가져올 소속코드
	 * @param boolean $is_all 전체를 다 가져올지 여부, default FALSE
	 * @return array
	 */
	public function get_oper_id_by_s_code($args, $is_all=FALSE) {
		
		if(strpos($args, ',') !== FALSE) {
			$args = str_replace(',', '","', $args);
		}
		$code = ' "'. $args .'" ';
		$fields = 'oper_id,oper_name,s_code';
		$query = 'SELECT '. $fields .' FROM '. $this->tbl_oper .' '.
				' WHERE ';
		if($is_all == FALSE || $is_all == 0 ) {
			$query .= 'use_yn = 1 AND ';
		}
		$query .= 's_code IN ('. $code .') ';
		//$query .= 'ORDER BY dsp_order ASC, code_name ASC';
	
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
	
		return $rstRtn;
	}
	
		
	/**
	 * 상담 데이터에서 상담 년도만 추출해서 리턴
	 * 
	 * @param $args string 테이블명
	 * @return array
	 */
	public function get_counsel_year($args) {
	
		$query = 'SELECT EXTRACT(YEAR FROM csl_date) as year '
			.'FROM '. $args .' '
			.'GROUP BY EXTRACT(YEAR FROM csl_date) '
			.'ORDER BY EXTRACT(YEAR FROM csl_date) DESC ';
			$rs = $this->db->query($query);
			$rst = $rs->result();

			return $rst;
	}

	
	/**
	 * 권리구제지원 테이블의 년도만 추출해서 리턴 - 지원승인일
	 * 
	 * @param void
	 * @return array
	 */
	public function get_lawhelp_year() {
	
		$query = 'SELECT EXTRACT(YEAR FROM lh_sprt_cfm_date) as year '
			.'FROM '. $this->tbl_lawhelp .' '
			.'GROUP BY EXTRACT(YEAR FROM lh_sprt_cfm_date) '
			.'ORDER BY EXTRACT(YEAR FROM lh_sprt_cfm_date) DESC ';
			$rs = $this->db->query($query);
			$rst = $rs->result();

			return $rst;
	}
	
	/**
	 * 권리구제 - 구제유형코드 세(B)코드 조회
	 *
	 * @param  array $args
	 * @return array
	 */
	public function get_lhkind_sub_cd($args) {
	
		$query = "SELECT *
  		FROM ". $this->tbl_sub_code ."
			WHERE use_yn = 1
				AND m_code='". $args['m_code'] ."'
			ORDER BY dsp_order ASC, code_name ASC";
	
		$rs = $this->db->query($query);
		$rstRtn = $rs->result();
	
		return $rstRtn;
	}
	

	/**
	 * 최종 실행된 쿼리 리턴
	 *
	 * @param void
	 * @return string
	 */
	public function get_last_query() {
		// ENVIRONMENT 상수는 /index.php 페이지 내에 있다.
		if( defined('ENVIRONMENT') && ENVIRONMENT == 'development') {
			$last_query = $this->db->last_query();
				
			return $last_query;
		}
	}


	/**
	 * 암호화키 리턴
	 *
	 */
	public function get_key() {
		return $this->db->select('pkey')->limit(1)->get($this->tbl_config)->result()[0]->pkey . CFG_ENCRYPT_KEY;
	}
	
	/**
	 * 통계에서 사용하는 공통 코드
	 * 
	 * @param array $args
	 * @param boolean $isCross 교차통계인지 여부, default 
	 * @return array 
	 */
	private function get_sttt_mcode($args, $isCross=FALSE) {

		$datas1 = array();
		$datas2 = array();
		$tg_m_code = '';
		$tg2_m_code = $args['csl_code'];
		$tg_s_code = '';
		$tg2_s_code = '';
		$target_field1 = '';
		$target_field2 = '';
		$target_field3 = '';
		$csl_kind2 = '';
		$csl_kind3 = '';
		$csl_kind4 = '';
		
		// 기준코드(target) 정보
		if($args['sch_kind'] == 0) {// 상담방법
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY;
			$target_field1 = 'C.s_code';
		}
		elseif($args['sch_kind'] == 1) {// 성별
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_GENDER, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_GENDER;
			$target_field1 = 'C.gender';
		}
		elseif($args['sch_kind'] == 2) { // 연령대
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_AGES, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_AGES;
			$target_field1 = 'C.ages';
		}
		elseif($args['sch_kind'] == 3) { // 거주지
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
			$target_field1 = 'C.live_addr';
		}
		elseif($args['sch_kind'] == 4) { // 회사소재지
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS, TRUE);//회사 소재지 코드는 거주지코드를 사용한다.
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
			$target_field1 = 'C.comp_addr';
		}
		elseif($args['sch_kind'] == 5) { // 직종
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND;
			$target_field1 = 'C.work_kind';
		}
		elseif($args['sch_kind'] == 6) { // 업종
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND;
			$target_field1 = 'C.comp_kind';
		}
		elseif($args['sch_kind'] == 7) { // 고용형태
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND;
			$target_field1 = 'C.emp_kind';
		}
		elseif($args['sch_kind'] == 8) {// 근로자수
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT;
			$target_field1 = 'C.emp_cnt';
		}
		elseif($args['sch_kind'] == 9) {// 근로계약서
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN;
			$target_field1 = 'C.emp_paper_yn';
		}
		elseif($args['sch_kind'] == 10) {// 4대보험
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN;
			$target_field1 = 'C.emp_insured_yn';
		}
		elseif($args['sch_kind'] == 11) { // 상담유형
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND;
			$target_field1 = 'CS.s_code';
		}
		elseif($args['sch_kind'] == 12) { // 상담동기
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE;
			$target_field1 = 'C.csl_motive_cd';
		}
		elseif($args['sch_kind'] == 13) { // 임금근로시간 - 좌측 기준은 소속으로 한다.
			$datas1 = self::get_sub_code_by_m_code(CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION, TRUE);
			$tg_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION;
			$target_field1 = 'C.asso_code';
		}

		// target1 컬럼의 s_code 문자열로 생성
		if(!empty($datas1)) {
			foreach ($datas1 as $i => $item) {
				if($tg_s_code != '') {
					$tg_s_code .= "','";
				}
				$tg_s_code .= $item->s_code;
			}
			$tg_s_code = " '". $tg_s_code ."' ";
		}
		
		// 교차대상코드(target2)의 컬럼명
		switch($tg2_m_code) {
			case CFG_SUB_CODE_OF_MANAGE_CODE_GENDER://성별
				$target_field2 = 'C.gender';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_AGES://연령대
				$target_field2 = 'C.ages';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS://거주지
				$target_field2 = 'C.live_addr';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP://회사소재지
					$target_field2 = 'C.comp_addr';
					break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_WORK_KIND://직종
				$target_field2 = 'C.work_kind';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_COMP_KIND://업종
				$target_field2 = 'C.comp_kind';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_EMP_KIND://고용형태
				$target_field2 = 'C.emp_kind';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_EMP_CNT://근로자수
				$target_field2 = 'C.emp_cnt';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_EMP_PAPER_YN://근로계약서
				$target_field2 = 'C.emp_paper_yn';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_EMP_INSURED_YN://4대보험
				$target_field2 = 'C.emp_insured_yn';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_CSL_KIND://상담유형
				$target_field2 = 'CS.s_code';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_CSL_MOTIVE://상담동기
				$target_field2 = 'C.csl_motive_cd';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_CSL_WAY://상담방법
				$target_field2 = 'C.s_code';
				break;
			case CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ASSOCIATION://소속
				$target_field2 = 'C.asso_code';
				break;
		}

		// 교차통계인 경우, 교차코드 s_code 리턴
		if($isCross == TRUE) {
			// 회사소재지 코드인 경우, 거주지코드를 사용하기 때문에 거주지코드로 바꿔준다. - 이거때문메 target_field2 코드 아래쪽에 위치해야 한다.
			if($tg2_m_code == CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS_COMP) {
				$tg2_m_code = CFG_SUB_CODE_OF_MANAGE_CODE_CSL_ADDRESS;
			}
			$datas2 = self::get_sub_code_by_m_code($tg2_m_code, TRUE);
		}

		// 교차대상코드(target2) 컬럼의 s_code 문자열로 생성
		if(!empty($datas2)) {
			foreach ($datas2 as $i => $item) {
				if($tg2_s_code != '') {
					$tg2_s_code .= "','";
				}
				$tg2_s_code .= $item->s_code;
			}
			$tg2_s_code = " '". $tg2_s_code ."' ";
		}
		
		return [
			'datas1' => $datas1
			,'datas2' => $datas2
			,'tg_m_code' => $tg_m_code
			,'tg2_m_code' => $tg2_m_code
			,'tg_s_code' => $tg_s_code
			,'tg2_s_code' => $tg2_s_code
			,'target_field1' => $target_field1
			,'target_field2' => $target_field2
			,'target_field3' => $target_field3
			,'csl_kind2' => $csl_kind2
			,'csl_kind3' => $csl_kind3
			,'csl_kind4' => $csl_kind4
		];
	}
}