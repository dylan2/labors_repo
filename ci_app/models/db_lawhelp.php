<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// db_admin Class
require_once 'db_admin.php';



/**
 * 권리구제지원 DB 처리 전용 Class
 *
 * @author dylan
 *
 */
class Db_lawhelp extends Db_Admin {
		
	
	/**
	 * Construct
	 * 
	 */
	function __construct() {
		parent::__construct();
	}


	/**
	 * 상담 목록 조회
	 *
	 * [권한] <br>
	 * 기본 : 권익센터 직원만 글 등록 및 수정,삭제, 자치구센터는 작성하지 않는다. 조회만 가능 <br>
	 * 권익센터,서울시,자치자치구 계정 : 모든 상담글 조회 가능 <br>
	 * 옴부즈만 계정 : 모든 권한 없음 <br>
	 * 엑셀 다운로드시에도 호출됨
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_lawhelp_list($args) {

		$key = $this->get_key();
		
		// offset, limit
		$offset = $args['offset'] ? $args['offset'] : $this->offset;
		$limit = $args['limit'] ? $args['limit'] : $this->limit;
		
		$where = 'WHERE 1=0 ';
		// 기본 - 옴부즈만 외 전체
		if($this->session->userdata(CFG_SESSION_ADMIN_KIND_CODE) != CFG_OPERATOR_KIND_CODE_OK3) {
			$where = 'WHERE 1=1 ';
		}

		$keyword = $args['keyword'];
		$target_sprt_kind_cd = $args['target_sprt_kind_cd'];
		$target_apply_rst_cd = $args['target_apply_rst_cd'];
		
		// 검색		
		// 전체검색
		if($args['target'] == 'all') {
			if($keyword != '') {
				$where .= 'AND ( C.lh_code LIKE "%'. $keyword .'%" ' // 접수번호
					.'OR C.lh_sprt_content LIKE "%'. $keyword .'%" ' // 권리구제지원내용
					.'OR C.lh_apply_nm="'. $keyword .'" ' // 신청자 - 암호화로 like검색에서 매칭으로 변경
					.'OR C.lh_labor_asso_nm LIKE "%'. $keyword .'%" ' // 대리인 - 소속
					.'OR C.lh_labor_nm LIKE "%'. $keyword .'%" ' // 대리인 - 이름
					.'OR S2.code_name LIKE "%'. $keyword .'%" ' // 신청기관
					.'OR S3.code_name LIKE "%'. $keyword .'%" ' // 대상기관
					.'OR C.sprt_organ_cd_etc LIKE "%'. $keyword .'%" ' // 대상기관 - 기타
					.' ) ';
			}
		}
		// 지원종류
		else if($args['target'] == 'sprt_kind_cd') {
			if($target_sprt_kind_cd != 'all') $where .= 'AND C.sprt_kind_cd = "'. $target_sprt_kind_cd .'" ';
			if($keyword !='') $where .= 'AND C.sprt_kind_cd_etc LIKE "%'. $keyword .'%" ';
		}
		// 지원결과
		else if($args['target'] == 'apply_rst_cd') {
			if($target_apply_rst_cd != 'all') $where .= 'AND C.apply_rst_cd = "'. $target_apply_rst_cd .'" ';
			if($keyword !='') $where .= 'AND C.apply_rst_cd_etc LIKE "%'. $keyword .'%" ';
		}
		// 대리인 - 소속,이름
		else if($args['target'] == 'lawhelp_lh_labor') {
			if($keyword !='') {
				$where .= 'AND ( C.lh_labor_asso_nm LIKE "%'. $keyword .'%" '
					.'OR C.lh_labor_nm LIKE "%'. $keyword .'%" ) ';
			}
		}
		// 신청기관
		else if($args['target'] == 'apply_organ_cd') {
			if($keyword !='') {
				$where .= 'AND S2.code_name LIKE "%'. $keyword .'%" ';
			}
		}
		// 대상기관
		else if($args['target'] == 'sprt_organ_cd') {
			if($keyword !='') {
				$where .= 'AND ( S3.code_name LIKE "%'. $keyword .'%" '
					.'OR C.sprt_organ_cd_etc LIKE "%'. $keyword .'%" ) ';
			}
		}
		else {
			//신청자명
			if($args['target'] == 'lh_apply_nm') {
				$where .= "AND C.lh_apply_nm=AES_ENCRYPT('". $keyword ."', HEX(SHA2('". $key ."',512))) ";
			}
			else if($keyword !='') {
				$where .= 'AND C.'. $args['target'] .' LIKE "%'. $keyword .'%" ';
			}
		}
		// echof($where);

		// 일자검색 : 기본 지원승인일 : search_date_begin, search_date_end
		$search_date_begin = $args['search_date_begin'];
		$search_date_end = $args['search_date_end'];
		// - 양쪽 다 있는 경우
		if($search_date_begin != '' && $search_date_end != '') {
			$where .= 'AND C.'. $args['search_date_target'] .' BETWEEN "'. $search_date_begin .'" AND "'. $search_date_end .'" ';
		}
		// 시작년월일만 있는 경우 - 해당 일만 검색
		else if($search_date_begin != '' && $search_date_end == '') {
			$begin_year = explode(' ', $search_date_begin);
			$where .= 'AND C.'. $args['search_date_target'] .' LIKE "'. $begin_year[0] .'%" ';
		}

		// 년도 검색 : 대상 - 지원승인일 
		if($args['search_year'] != 'all') {
			$where .= 'AND C.lh_sprt_cfm_date LIKE "'. $args['search_year'] .'%" ';
		}

		// 검색 : 연령 검색
		if($args['search_ages'] != 'all') {
			$where .= 'AND C.ages_cd = "'. $args['search_ages'] .'" ';
		}

		// fields
		// 수정 : C.reg_oper_id 추가, 2017.01.18 by dylan
		// 암호화필드 처리 2019.07.29 dylan
		$fields1 = 'C.reg_oper_id,C.seq,C.lh_code
			,AES_DECRYPT(C.lh_apply_nm, HEX(SHA2("'. $key .'",512))) as lh_apply_nm
			,AES_DECRYPT(C.lh_apply_comp_nm, HEX(SHA2("'. $key .'",512))) as lh_apply_comp_nm
			,S5.code_name as ages_nm, C.ages_etc
			,S6.code_name as gender_cd_nm,S7.code_name as lh_kind_cd_nm
			,S8.code_name as work_kind_nm, C.work_kind_etc, S9.code_name as comp_kind_nm
			,C.comp_kind_etc
			,S.code_name as sprt_kind_cd_nm, C.sprt_kind_cd_etc
			,S2.code_name as apply_organ_cd_nm, C.lh_labor_asso_nm, C.lh_labor_nm
			,S3.code_name as sprt_organ_cd_nm, C.sprt_organ_cd_etc
			,C.lh_sprt_cfm_date,C.lh_accept_date,C.lh_case_end_date, S4.code_name as apply_rst_cd_nm
			,(
				SELECT GROUP_CONCAT(lid_date ORDER BY lid_date ASC) 
				FROM '. $this->tbl_lawhelp_invtgt_date .'
				WHERE seq=C.seq
			) as lid_date
			,(
				SELECT GROUP_CONCAT(SB2.code_name ORDER BY SB2.code_name ASC) 
				FROM '. $this->tbl_lawhelp_kind_sub_cd .' SB1
				INNER JOIN '. $this->tbl_sub_code .' SB2 ON SB1.kind_sub_cd=SB2.s_code
				WHERE SB1.seq=C.seq
			) as lh_kind_sub_cd_nm
			,apply_rst_cd_etc,lh_sprt_content,lh_etc'; // Excel download 용 추가 컬럼

		$query1 = 'SELECT '. $fields1 .' ' 
			.'FROM '. $this->tbl_lawhelp .' C '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.sprt_kind_cd ' // 지원종류
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON S2.s_code = C.apply_organ_cd ' // 신청기관
			.'INNER JOIN '. $this->tbl_sub_code .' S3 ON S3.s_code = C.sprt_organ_cd ' // 대상기관
			.'LEFT JOIN '. $this->tbl_sub_code .' S4 ON S4.s_code = C.apply_rst_cd ' // 지원결과 - 필수 아님
			.'LEFT JOIN '. $this->tbl_sub_code .' S5 ON S5.s_code = C.ages_cd ' // 연령대 16.09.12 추가 요청
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.gender_cd ' // 성별 18.07.05 추가 
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.lh_kind_cd ' // 구제유형 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S8 ON S8.s_code = C.work_kind ' // 직종 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.comp_kind ' // 업종 18.07.05 추가
			.$where .' ';
		
		$orderby = 'ORDER BY C.lh_code DESC, C.seq DESC ';

		// 엑셀 다운로드 일 경우 limit 제외
		if(! isset($args['is_excel'])) {
			$orderby .= 'LIMIT '. $offset .', '. $limit;
		}

		$q = $query1 . $orderby;
		// echof($q);

		// query
		$rs = $this->db->query($q);
		$rstRtn['data'] = $rs->result();
		// echof($rstRtn['data']);
		
		// total count
		$q =  $query1;
		$rs = $this->db->query($q);
		$tmp = $rs->result();

		$rstRtn['tot_cnt'] = count($tmp);
		
		return $rstRtn;
	}


	/**
	 * 상담 데이터 조회
	 * 
	 * [권한] <br>
	 * 기본 : 권익센터 직원만 글 등록 및 수정,삭제, 자치구센터는 작성하지 않는다. 조회만 가능 <br>
	 * 권익센터,서울시,자치자치구 계정 : 모든 상담글 조회 가능 <br>
	 * 옴부즈만 계정 : 모든 권한 없음 <br>
	 * 
	 * @param array $args
	 * @return array
	 */
	public function get_lawhelp($args) {

		$key = $this->get_key();
		
		$where = 'C.seq = '. $args['seq'] .' ';		
		$fields = "C.seq, C.lh_code, C.sprt_kind_cd, C.sprt_kind_cd_etc, C.lh_sprt_cfm_date
			,AES_DECRYPT(C.lh_apply_nm, HEX(SHA2('$key',512))) as lh_apply_nm
			,AES_DECRYPT(C.lh_apply_addr, HEX(SHA2('$key',512))) as lh_apply_addr
			,AES_DECRYPT(C.lh_apply_comp_nm, HEX(SHA2('$key',512))) as lh_apply_comp_nm	
			,AES_DECRYPT(C.lh_comp_addr, HEX(SHA2('$key',512))) as lh_comp_addr	
			,C.gender_cd, C.ages_cd, C.ages_etc, C.work_kind, C.work_kind_etc
			,C.comp_kind, C.comp_kind_etc, C.apply_organ_cd, C.lh_labor_asso_nm, C.lh_labor_nm
			,C.sprt_organ_cd, C.sprt_organ_cd_etc, C.lh_accept_date, C.lh_case_end_date
			,C.lh_sprt_content, C.apply_rst_cd, C.apply_rst_cd_etc, C.lh_etc, C.reg_oper_id, C.lh_kind_cd
			,C.file_name, C.file_name_org, C.reg_date
			,O.oper_name,O.oper_kind,O.oper_auth_grp_id ";

		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_lawhelp .' C '
			.'INNER JOIN '. $this->tbl_oper .' O ON O.oper_id=C.reg_oper_id '
			// .'LEFT JOIN '. $this->tbl_lawhelp_invtgt_date .' A ON A.seq = C.seq '
			.'WHERE '. $where;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();
		$rstRtn['data'] = $rst;

		$rstRtn['data']['lid_date_data'] = array();
		$rstRtn['data']['lh_kind_cd_data'] = array();

		$rstRtn['data'] = $rst;
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 출석조사일 가져오기
			$rs = $this->db->select('lid_seq,lid_date')->order_by('lid_date','asc')->get_where($this->tbl_lawhelp_invtgt_date, array('seq'=>$args['seq']));
			$rst = $rs->result();
			$rstRtn['data']['lid_date_data'] = $rst;

			// 권리구제 유형 가져오기 - 추가 2018.07.05
			$rs = $this->db->select('lh_seq,kind_sub_cd')->order_by('lh_seq','asc')->get_where($this->tbl_lawhelp_kind_sub_cd, array('seq'=>$args['seq']));
			$rst = $rs->result();
			$rstRtn['data']['lh_kind_cd_data'] = $rst;
		}
		
		return $rstRtn;
	}


	/**
	 * 저장
	 *
	 * @param array $args
	 * @return array
	 */
	public function add_lawhelp($args) {

		$args['reg_date'] = get_date();

		// 출석조사일
		$lid_date = $args['lid_date'];
		unset($args['lid_date']);
		// 권리구제 유형
		$lh_kind_sub_cd = $args['lh_kind_sub_cd'];
		unset($args['lh_kind_sub_cd']);

		// 암호화필드 처리 2019.07.29 dylan
		$lh_apply_nm = $args['lh_apply_nm'];
		unset($args['lh_apply_nm']);
		if($lh_apply_nm) {
			$this->db->set('lh_apply_nm', $lh_apply_nm, FALSE);
		}
		$lh_apply_addr = $args['lh_apply_addr'];
		unset($args['lh_apply_addr']);
		if($lh_apply_addr) {
			$this->db->set('lh_apply_addr', $lh_apply_addr, FALSE);
		}
		$lh_apply_comp_nm = $args['lh_apply_comp_nm'];
		unset($args['lh_apply_comp_nm']);
		if($lh_apply_comp_nm) {
			$this->db->set('lh_apply_comp_nm', $lh_apply_comp_nm, FALSE);
		}
		$lh_comp_addr = $args['lh_comp_addr'];
		unset($args['lh_comp_addr']);
		if($lh_comp_addr) {
			$this->db->set('lh_comp_addr', $lh_comp_addr, FALSE);
		}
		
		$rs = $this->db->insert($this->tbl_lawhelp, $args);
		$new_id = $this->db->insert_id();

		// 접수번호 자동생성 - 직접입력으로 변경 : 최진혁 20160617
		// $year = get_date('Y');
		// $accept_no = $year .'-'. str_pad($new_id, 4, '0', STR_PAD_LEFT);
		// $this->db->where(array('seq'=>$new_id))->update($this->tbl_lawhelp, array('lh_code'=>$accept_no));
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;

			// 출석조사일 일자 저장
			if($lid_date != '') {
				$data = array(
					'seq' => $new_id
					,'lid_date' => $lid_date
				);
				$this->db->insert($this->tbl_lawhelp_invtgt_date, $data);
			}

			// 권리구제 유형 table 저장 - 추가 2018.07.05
			if($lh_kind_sub_cd) {
				foreach ($lh_kind_sub_cd as $key => $value) {
					$data = array(
						'seq' => $new_id
						,'kind_sub_cd' => base64_decode($value)
					);
					$this->db->insert($this->tbl_lawhelp_kind_sub_cd, $data);
				}
			}
			
		}
		
		return $rstRtn;
	}

	
	/**
	 * 업데이트
	 *
	 * @param array $args
	 * @return array
	 */
	public function edit_lawhelp($args) {
		
		$args['mod_date'] = get_date();
		
		// 출석조사일
		$lid_date = $args['lid_date'];
		unset($args['lid_date']);
		// 권리구제 유형
		$lh_kind_sub_cd = $args['lh_kind_sub_cd'];
		unset($args['lh_kind_sub_cd']);
		
		$where['seq'] = $args['seq'];

		// 암호화필드 처리 2019.07.29 dylan
		$lh_apply_nm = $args['lh_apply_nm'];
		unset($args['lh_apply_nm']);
		if($lh_apply_nm) {
			$this->db->set('lh_apply_nm', $lh_apply_nm, FALSE);
		}
		$lh_apply_addr = $args['lh_apply_addr'];
		unset($args['lh_apply_addr']);
		if($lh_apply_addr) {
			$this->db->set('lh_apply_addr', $lh_apply_addr, FALSE);
		}
		$lh_apply_comp_nm = $args['lh_apply_comp_nm'];
		unset($args['lh_apply_comp_nm']);
		if($lh_apply_comp_nm) {
			$this->db->set('lh_apply_comp_nm', $lh_apply_comp_nm, FALSE);
		}
		$lh_comp_addr = $args['lh_comp_addr'];
		unset($args['lh_comp_addr']);
		if($lh_comp_addr) {
			$this->db->set('lh_comp_addr', $lh_comp_addr, FALSE);
		}
		
		$rs = $this->db->where($where)->update($this->tbl_lawhelp, $args);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';

			// 출석조사일 추가
			if($lid_date != '') {
				$data = array(
					'seq' => $args['seq']
					,'lid_date' => $lid_date
				);
				$this->db->insert($this->tbl_lawhelp_invtgt_date, $data);
			}

			// 권리구제 유형 table 저장 - 추가 2018.07.05
			if(count($lh_kind_sub_cd) > 0) {
				// 기존 데이터 삭제
				$where = array(
					'seq' => $args['seq']
				);
				$this->db->delete($this->tbl_lawhelp_kind_sub_cd, $where);
				// 새로저장
				foreach ($lh_kind_sub_cd as $key => $value) {
					$data = array(
						'seq' => $args['seq']
						,'kind_sub_cd' => base64_decode($value)
					);
					$this->db->insert($this->tbl_lawhelp_kind_sub_cd, $data);
				}
			}
		}
		
		return $rstRtn;
	}
		

	/**
	 * 출석조사일 추가
	 *
	 * @param array $args
	 * @return array
	 */
	public function add_lawhelp_invtgt_date($args) {

		$this->db->insert($this->tbl_lawhelp_invtgt_date, $args);
		$new_id = $this->db->insert_id();
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
		$rstRtn['new_id'] = '';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
			$rstRtn['new_id'] = $new_id;
		}
		
		return $rstRtn;
	}
	

	/**
	 * 출석조사일 삭제
	 *
	 * @param array $args
	 * @return array
	 */
	public function del_lid_date($args) {

		$this->db->where($args)->delete($this->tbl_lawhelp_invtgt_date);
		
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}
		

	/**
	 * 다중 삭제
	 *
	 * @param array $args
	 * @return array
	 */
	public function del_multi_lawhelp($args) {

		$seqs = str_replace(CFG_AUTH_CODE_DELIMITER, '","', $args['seqs']);
		// 첨부파일 정보 가져와 삭제처리
		$query = 'SELECT file_name FROM '. $this->tbl_lawhelp .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);
		$file_data = $rst->result();
		foreach ($file_data as $key => $fnames) {
			if($fnames->file_name) {
				$arr_fname = explode(CFG_UPLOAD_FILE_NAME_DELIMITER, $fnames->file_name);
				foreach ($arr_fname as $key => $fname) {
					if($fname) {
						@unlink(CFG_UPLOAD_PATH . $fname);
						clearstatcache();
					}
				}
			}
		}
				
		// 출석조사일 테이블 삭제
		$query = 'DELETE FROM '. $this->tbl_lawhelp_invtgt_date .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);

		// 권리구제 유형 테이블 삭제
		$query = 'DELETE FROM '. $this->tbl_lawhelp_kind_sub_cd .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);

		// 게시물 삭제
		$query = 'DELETE FROM '. $this->tbl_lawhelp .' WHERE seq IN ("'. $seqs .'") ';
		$rst = $this->db->query($query);
		
		return true;
	}


	/**
	 * 상담조회 - 인쇄용
	 *
	 * @param array $args
	 * @return array
	 */
	public function get_lawhelp_print($args) {

		$key = $this->get_key();
			
		$where = 'C.seq = '. $args['seq'] .' ';

		// 암호화필드 처리 2019.07.29 dylan
		$fields = 'C.lh_code
			,AES_DECRYPT(C.lh_apply_nm, HEX(SHA2("'. $key .'",512))) as lh_apply_nm
			,AES_DECRYPT(C.lh_apply_addr, HEX(SHA2("'. $key .'",512))) as lh_apply_addr
			,AES_DECRYPT(C.lh_apply_comp_nm, HEX(SHA2("'. $key .'",512))) as lh_apply_comp_nm
			,AES_DECRYPT(C.lh_comp_addr, HEX(SHA2("'. $key .'",512))) as lh_comp_addr
			,S.code_name as sprt_kind_cd_nm,C.sprt_kind_cd_etc
			,C.lh_accept_date,C.lh_sprt_content,C.lh_etc'
			.',S2.code_name as apply_organ_cd_nm,C.lh_labor_asso_nm,C.lh_labor_nm ,S3.code_name as sprt_organ_cd_nm'
			.',C.sprt_organ_cd_etc,C.lh_sprt_cfm_date,C.lh_case_end_date, S4.code_name as apply_rst_cd_nm,C.apply_rst_cd_etc, C.ages_etc '
			.',S5.code_name as ages, S6.code_name as gender_cd_nm,S7.code_name as lh_kind_cd_nm
			,S8.code_name as work_kind_nm,S9.code_name as comp_kind_nm '
			.',(SELECT GROUP_CONCAT(lid_date ORDER BY lid_date ASC) FROM '. $this->tbl_lawhelp_invtgt_date .' WHERE seq=C.seq ORDER BY lid_seq DESC) as lid_date
			,(
				SELECT GROUP_CONCAT(SB2.code_name ORDER BY SB2.code_name ASC) 
				FROM '. $this->tbl_lawhelp_kind_sub_cd .' SB1
				INNER JOIN '. $this->tbl_sub_code .' SB2 ON SB1.kind_sub_cd=SB2.s_code
				WHERE SB1.seq=C.seq
			) as lh_kind_sub_cd_nm
			, C.work_kind_etc, C.comp_kind_etc';
		$query = 'SELECT '. $fields .' ' 
			.'FROM '. $this->tbl_lawhelp .' C '
			.'INNER JOIN '. $this->tbl_sub_code .' S ON S.s_code = C.sprt_kind_cd ' // 지원종류
			.'INNER JOIN '. $this->tbl_sub_code .' S2 ON S2.s_code = C.apply_organ_cd ' // 신청기관
			.'INNER JOIN '. $this->tbl_sub_code .' S3 ON S3.s_code = C.sprt_organ_cd ' // 대상기관
			.'INNER JOIN '. $this->tbl_sub_code .' S5 ON S5.s_code = C.ages_cd ' // 연령대
			.'LEFT JOIN '. $this->tbl_sub_code .' S4 ON S4.s_code = C.apply_rst_cd ' // 지원결과 - 필수 아님
			.'LEFT JOIN '. $this->tbl_sub_code .' S6 ON S6.s_code = C.gender_cd ' // 성별 18.07.05 추가 
			.'LEFT JOIN '. $this->tbl_sub_code .' S7 ON S7.s_code = C.lh_kind_cd ' // 구제유형 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S8 ON S8.s_code = C.work_kind ' // 직종 18.07.05 추가
			.'LEFT JOIN '. $this->tbl_sub_code .' S9 ON S9.s_code = C.comp_kind ' // 업종 18.07.05 추가
			.'WHERE '. $where;

		// $rstRtn['query'] = $query;
		// query
		$rs = $this->db->query($query);		
		$rst = $rs->result();

		$rstRtn['data'] = array();
		if(count($rst) > 0) {
			$rstRtn['data'] = (array)$rst[0];
		}

		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'select error on DB';

		if($this->db->affected_rows() > 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
		
		return $rstRtn;
	}


	/**
	 * 상담 고유코드 중복여부 체크
	 *
	 * @param array $args
	 * @return array
	 */
	public function chk_code($args) {
	
		$rstRtn['rst'] = 'fail';
		$rstRtn['msg'] = 'delete error on DB';
	
		$rs = $this->db->select('count(lh_code) as cnt')->get_where($this->tbl_lawhelp, $args);
		$rst = $rs->result();
	
		if($rst[0]->cnt == 0) {
			$rstRtn['rst'] = 'succ';
			$rstRtn['msg'] = 'ok';
		}
	
		return $rstRtn;
	}
	
}