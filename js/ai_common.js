/**
 * jquery ajax 요청 처리 함수
 * param : url
 * param : data
 * param : fn_succes
 * param : fn_error
 * param : b_loading default true
 * param : method default post
 * param : data_type default json
 */
function req_ajax(url, data, fn_succes, fn_error, b_loading, method, data_type, mime_type, content_type, process_data) {
    if(!url) return;

    // default value
    data = data ? data : '';
    method = method ? method : 'post';
    data_type = data_type ? data_type : 'json';
    b_loading = b_loading == undefined ? true : b_loading;
    mime_type = mime_type ? mime_type : 'application/x-www-form-urlencoded';
    content_type = content_type ? content_type : 'application/x-www-form-urlencoded; charset=UTF-8';
    process_data = process_data ? process_data : true;

    // loading indicator를 호출할 때 딜레이 타임
    var delay = 0;

    // loading indicator
    if(b_loading) {
        delay = 300;
        $('#loading').show();
    }

    if(!fn_error) {
        fn_error = function(data) {
            if(is_local) objectPrint(data);
            
            var msg = CFG_MSG[CFG_LOCALE]['info_cmm_02'];
            if(data && data.msg) msg += '[' + data.msg +']';
            alert(msg);
        };
    }
    var fn_hide_loading = function(data) {
        if(b_loading) $('#loading').hide();
        fn_succes(data);
    };

    // request
    setTimeout(function(){
        // request
        $.ajax({
            url: url
            ,data: data
            ,cache: false
            ,async: false
            ,method: method
            ,dataType: data_type
            ,mimeType: mime_type
            ,contentType: content_type
            ,processData: process_data
            ,success: fn_hide_loading
            ,error: fn_hide_loading
            ,beforeSend: function(xhr) {
                if(b_loading) $('#loading').show();
            }
        })
        .done(function( data ) {
            if(b_loading) $('#loading').hide();
        });
    }, delay);
}


/**
 * alertJQ
 */
function alertJQ(params) {
    params.msg ? $("#dialog").text(params.msg) : '';
    $("#dialog").dialog(
    {
        modal: true, 
        resizable: false,
        closeOnEscape: true,
        title: params.title ? params.title : 'information',
        show: { 
            effect: 'fadeIn'
        },
        hide: {
            effect: 'fadeOut'
        },
        buttons: params.yesno ? [{
            text: "OK",
            click: function() { 
                $(this).dialog("close");
                typeof params.callback != 'undefined' ?
                    params.callback(
                        typeof params.callbackparams == 'undefined' ? '' : params.callbackparams
                    ) : '';
            }
        },{
            text: 'Cancel',
            click: function(){
                $(this).dialog("close");
            }
        }] : [{
            text: "OK",
            click: function() { 
                $(this).dialog("close"); 
                typeof params.callback != 'undefined' ?
                    params.callback(
                        typeof params.callbackparams == 'undefined' ? '' : params.callbackparams
                    ) : '';
            }
        }]
    });
}



/**
 * genTwoDigit
 */
function genTwoDigit(data) {
    if(data.toString().length == 1)
        return '0' + data;
    else 
        return data;
}



/**
 * jQuery local storage
 * - jq data = require element id : eg) 'mainBody''
 * - setData(), getData(), clearData()
 */
function setData(key, value)
{
    $("#mainBody").data(key, value);
}

function getData(key)
{
    return $("#mainBody").data(key);
}

function clearData()
{
    $.each($("#mainBody"), function(key, value){
        $("#mainBody")[key] = '';
    });
}


/**
 * objectPrint
 */
function objectPrint(data) {
	if(typeof data === 'object') {
		for(var key in data) {
			console.log('data['+ key +'] = '+ data[key]);
		}
	}
	else {
		console.log('err = '+ data);
	}
}


/**
 * check a number
 */
function isNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
}


/**
 * replaceAll
 */
String.prototype.replaceAll = function(as_rex, as_out) {
	return this.replace(new RegExp(as_rex, "g"), as_out);
};


/**
 * 체크박스 전체 선택
 * attr로 안된다. ie11에서.. prop으로..
 */
/* 
$('input.checkAll').click(function(e) {
	var status = $(this).prop('checked');
	$("input[name=chk_item]:checkbox").each(function(i) {
		$(this).prop("checked", status);
	});
});
*/


/**
 * 오늘 일자 반환 yyyy-mm-dd
 */
function get_today() {
	var now = new Date();
	return now.getFullYear() +'-'+ genTwoDigit(now.getMonth()+1) +'-'+ genTwoDigit(now.getDate());
}

/**
 * 오늘 기준으로 일주일전 일자 반환 yyyy-mm-dd
 */
function get_week_ago() {
    var date = new Date();
    var last = new Date(date.getTime() - (6 * 24 * 60 * 60 * 1000));
    var day = last.getDate();
    var month = +last.getMonth()+1;
    var year = last.getFullYear();

    return year +'-'+ genTwoDigit(month) +'-'+ genTwoDigit(day);
}

/**
 * 오늘 기준으로 입력된 개월수 만큼 이전 일자 반환 yyyy-mm-dd
 */
function get_xmonth_ago(month) {
    var now = new Date();
    var y = now.getFullYear();
    var m = +now.getMonth() - (month - 1);
    if(m <= 0) {
        y = +y - 1;
        m = m + 12;
    }
    return y +'-'+ genTwoDigit(m) +'-'+ genTwoDigit(+now.getDate()+1);
}


/*
$(document).ready(function(){
	// 체크박스 전체 선택
	$('input.checkAll').click(function(e) {
		$('input:checkbox').not(this).prop('checked', this.checked);
	});
});
*/


/**
  * 숫자를 통화로 변환
  */
function numberToCurrency(n) {
	if(isNaN(n)){return 0;}
	var reg = /(^[+-]?\d+)(\d{3})/;
	n += '';
	while (reg.test(n))
		n = n.replace(reg, '$1' + ',' + '$2');

	return n;
}

/**
  * 숫자에서 콤마를 빼고 반환
  */
function currencyToNumber(n){
	n=n.replace(/,/g,"");
	if(isNaN(n)){return 0;}
	else{return n;}
}


/**
//  discuss at: http://phpjs.org/functions/bin2hex/
// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
// bugfixed by: Onno Marsman
// bugfixed by: Linuxworld
// improved by: ntoniazzi (http://phpjs.org/functions/bin2hex:361#comment_177616)
//   example 1: bin2hex('Kev');
//   returns 1: '4b6576'
//   example 2: bin2hex(String.fromCharCode(0x00));
//   returns 2: '00'
*/
function bin2hex(s) {
    var i, l, o = '', n;
    s += '';
    for (i = 0, l = s.length; i < l; i++) {
        n = s.charCodeAt(i)
        .toString(16);
        o += n.length < 2 ? '0' + n : n;
    }
    return o;
}

/**
 * jQuery form submit helper
 */
function getDoc(frame) {
    var doc = null;

    // IE8 cascading access check
    try {
        if (frame.contentWindow) {
            doc = frame.contentWindow.document;
        }
    } catch(err) {
    }

    if (doc) { // successful getting content
        return doc;
    }

    try { // simply checking may throw in ie8 under ssl or mismatched protocol
        doc = frame.contentDocument ? frame.contentDocument : frame.document;
    } catch(err) {
        // last attempt
        doc = frame.document;
    }
    return doc;
}


/**
 * 공통 jQuery 이벤트 핸들러
 */
 /*
 $(document).ready(function(){

    // input tag가 금액인 경우 - 0,공백 처리
    setTimeout(function() {
        set_money_input();
    }, 300); // 동적생성되는 input tag를 위해 지연시간을 둔다.

    // 검색 - 오늘,일주일,한달,전체
    $('#search_area').bind('click', '#btnToday,#btnWeek,#btnMonth,#btnAllTerm', function(e){
        tid = e.target.id;
        if(tid && tid == 'btnToday' || tid == 'btnWeek' || tid == 'btnMonth' || tid == 'btnAllTerm' || tid == 'btnSizMonth') {
            var date_begin='', date_end=get_today();
            if(tid == 'btnToday') {
                date_begin = get_today();
            }
            else if(tid == 'btnWeek') {
                date_begin = get_week_ago();
            }
            else if(tid == 'btnMonth') {
                date_begin = get_xmonth_ago(1);
            }
            else if(tid == 'btnSizMonth') {
                date_begin = get_xmonth_ago(6);
            }
            else {
                date_end = '';
            }

            $('input[name=search_date_begin]').val(date_begin);
            $('input[name=search_date_end]').val(date_end);
        }
    }); 
});

// 공통 jQuery 이벤트 핸들러 에서 사용
function set_money_input() {

    // input 태그 중 "money" 클래스가 있으면 입력 길이 제한한다.
    $('input.money').prop('maxlength', 12);

    // 최초 값이 공백이면 0처리
    $('input.money').each(function(i){
        if(!$(this).val() || $(this).val() == '') {
            $(this).val(0);
        }
    });  
     
    // focusIN인 경우 0이면 공백처리, OUT 인 경우 공백이면 0처리
    // $('input.money').unbind('focus').unbind('focusout');
    $('input.money').focus(function(e){
        if($(this).val() == 0 || !$(this).val()) {
            $(this).val('');
        }
    })
    .focusout(function(e){
      if($(this).val() == '' || !$(this).val()) {
            $(this).val(0);
        }  
    });
}
*/

/**
 * 첫글자 영문자만 허용
 */
function chk_begin_eng(data) {
    var char1 = data.substr(0, 1);
    var pattern = /^[a-z]+$/i;
    return pattern.test(char1);
}

/**
 * 특수문자 체크 - $, &, %, ', ", =, #, ?, ;, \
 */
function chk_not_allow_char(data) {
    var pattern = /[\$\&\|\%\'\"\=\#\?\;\\]/i;
    var test = data.match(pattern);
    return test == null;
}


/**
 * trim
 */
String.prototype.trim = function() {
    return this.replace(/(^\s*)|(\s*$)/g,"");
};

/**
 * 다음 - 우편번호 찾기
 */
function daum_foldDaumPostcode() {
    // iframe을 넣은 element를 안보이게 한다.
    element_wrap.style.display = 'none';
}
function daum_execPostcode() {
    // 현재 scroll 위치를 저장해놓는다.
    var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    new daum.Postcode({
        oncomplete: function(data) {
            // console.log(data);
            s_sel_addr_type = data.userSelectedType.toLowerCase();
            // 검색 결과에서 도로명/지번 중 선택에 따른 처리
            if(s_sel_addr_type == 'j') { // 지번 주소를 선택한 경우
                dv_addr = data.jibunAddress;
                dv_bungi = data.jibunAddressEnglish.split(',')[0];
                dv_addr = dv_addr.replace(dv_bungi,'').trim();
            }
            else { // 도로명 선택한 경우
                dv_addr = data.roadAddress;
                dv_bungi = data.roadAddressEnglish.split(',')[0];
            }
            $("input[name=addr_type][value='"+ s_sel_addr_type +"']").prop('checked', true);

            /*
            dv_sel_addr_type = $("input[name=addr_type]:checked").val();
            if(dv_sel_addr_type == 'j') {
                dv_addr = data.jibunAddress;
                dv_bungi = data.jibunAddressEnglish.split(',')[0];
                dv_addr = dv_addr.replace(dv_bungi,'').trim();
            }
            else {
                dv_addr = data.roadAddress;
                dv_bungi = data.roadAddressEnglish.split(',')[0];
                // dv_addr = dv_addr.replace(dv_bungi,'').trim();
            }
            */

            document.getElementById('dv_addr_post').value = data.zonecode;
            document.getElementById('dv_addr').value = dv_addr;
            $('#dv_addr_etc').focus();

            // iframe을 넣은 element를 안보이게 한다.
            // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
            element_wrap.style.display = 'none';

            // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
            document.body.scrollTop = currentScroll;
        },
        // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
        onresize : function(size) {
            element_wrap.style.height = size.height+'px';
        },
        width : '100%',
        height : '100%'
    }).embed(element_wrap);

    // iframe을 넣은 element를 보이게 한다.
    element_wrap.style.display = 'block';
}


// 이메일 선택
function chg_email(obj1, obj2) {
    var mselect = obj1.selectedIndex;
    if(obj1.options[mselect] != '') {
        obj2.value = obj1.options[mselect].value;
    }
}


// 문자열에서 html 태그 제거
// allowed : 허용하려는 태그 eg) strip_tags('<p>Kevin</p> <b>van</b> <i>Zonneveld</i>', '<i><b>'); -> result: Kevin <b>van</b> <i>Zonneveld</i>
function strip_tags (data, allowed) {
    // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');

    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    
    return data.replace(commentsAndPhpTags, '').replace('&nbsp;', '').replace(tags, 
        function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        }
    );
}


// return yyyy-mm-dd : 구분자는 delimiter로 나뉜다.
function getYYYYMMDD(delimiter) {
    delimiter = delimiter == undefined ? '-' : delimiter;
    var arr = [];
    var d = new Date();
    var y = d.getFullYear(),
        m = d.getMonth() + 1,
        dd = d.getDay() + 1;
    arr[0] = y.toString().length <= 1 ? '0'+y : y;
    arr[1] = m.toString().length <= 1 ? '0'+m : m;
    arr[2] = dd.toString().length <= 1 ? '0'+dd : dd;

    return arr.join(delimiter);
}


// return hh:mm:ss : 구분자는 delimiter로 나뉜다.
function getHHMMSS(delimiter) {
    delimiter = delimiter == undefined ? ':' : delimiter;
    var arr = [];
    var d = new Date();
    var h = d.getHours(),
        m = d.getMinutes(),
        s = d.getSeconds();
    arr[0] = h.toString().length <= 1 ? '0'+h : h;
    arr[1] = m.toString().length <= 1 ? '0'+m : m;
    arr[2] = s.toString().length <= 1 ? '0'+s : s;

    return arr.join(delimiter);
}

// 입력된 년, 월에 해당하는 마지막 일을 리턴한다.
// eg) getLastDayInMonth(2016, 2) => 29
function getLastDayInMonth(yyyy, mm) {
    return new Date(yyyy, mm, 0).getDate();
}


// email 형식 체크
function chk_email(data) {
    var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
    return data.match(regExp);
}

// 한글 체크
function is_korean(data) {
    var p = /([^가-힣ㄱ-ㅎㅏ-ㅣ\x20])/i;
    return p.test(data);
}

// 숫자만 허용
function is_numberic(data) {
    var p = /^[0-9]*$/;
    return p.test(data);
}

// 숫자, ","만 허용
function is_money(data) {
    var p = /[^\d,.]/g;
    return !p.test(data);
}

// 알파벳만 허용
function is_alphabet(data) {
    var p = /^[A-za-z]/g;
    return p.test(data);
}


// 입력된 문자열이 null, undefined인 경우 공백 리턴
function remove_null(args) {
    return !args || args == 'null' || args == undefined || typeof args == 'undefined' || args == 'undefined' ? '' : args;
}

// 입력값이 0, undefined인 경우 제거해서 리턴
function remove_zero(data) {
    var tmp =  !data || data == 0 ? '' : data;

    return tmp;
}

// 입력값이 '', undefined인 경우 0 리턴
function convert_number(data) {
    var num = 0;
    if(data) num = String(data).replace(/\,/g,'');
    return !num || num == '' || isNaN(parseInt(num)) ? 0 : parseInt(num);
}


/**
 * popup window 가운데로 띄우기(듀얼 모니터 대응)
 */
function open_popup_center_dual(url, title, w, h) {
    // Fixes dual-screen position Most browsers Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
    width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
}


// 입력된 id의 input tag에 값이 숫자가 아닐 경우 0, 숫자면 값 리턴
function genInputValToInt(selector, index, selectorType) {
    // alert('>> selector = '+ selector + ' / index = '+ index);
    index = !index ? 0 : index;
    selectorType = !selectorType ? 'selector' : selectorType;

    var tmp = $('input[name^='+ selector +']').eq(index).val();
    if(selectorType == 'css') {
        tmp = $('input.'+ selector).eq(index).val();
    }

    tmp = tmp ? tmp.replace(/\,/g,'') : 0; // "," 제거

    return isNaN(tmp) ? 0 : parseInt(tmp);
}

// 통계에서 사용되는 '맨위로 이동'' 버튼 처리
$(function() {
    if ($('#go_to_top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#go_to_top').addClass('show');
                } else {
                    $('#go_to_top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#go_to_top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
});

/**
 * 에디터의 불필요한 태그 제거하여 에디터에 적용
 * 
 * @param object editor 네이버se2 에디터 객체 
 * @param string id 컨텐츠 textarea id 
 * @return void
 */
function removeGargageTag(editor, id) {
	cont = editor.getById[id].getIR();
	cont = cont.replace('<p>'+ String.fromCharCode(160) +'</p>', '').replace('<p>&nbsp;</p>', '').replace('<p><br></p>', '');
	cont = cont.replace("<div id=\"dicLayer\" style=\"color: rgb(0, 0, 0); font-size: 12px; font-family: Arial; background: -webkit-linear-gradient(bottom, rgb(255, 235, 0), rgb(255, 220, 0)); display: none;\"><div id=\"dicLayerContents\"><\/div><div id=\"dicLayerArrowB\"><\/div><div id=\"dicLayerArrowF\"><\/div><\/div><div id=\"dicRawData\" style=\"display: none;\"><\/div>", '');
	editor.getById[id].setIR( cont ); 
	// 에디터 내용 textarea에 적용
	editor.getById[id].exec("UPDATE_CONTENTS_FIELD", []);	
};

/**
 * QueryString 형태의 문자열에서 입력받은 key를 찾아 입력받은 value로 대체한 뒤 리턴한다.
 * 
 * @param string str QueryString 형태의 문자열 
 * @param string key value를 대체할 key 
 * @param string val key에 해당하는 value를 대체한다. 
 */
var replaceDataInSearchData = function(str, key, val) {
	if(!str) return;
	var rstRtn = "";
	var arrStr = str.split("&");
	$(arrStr).each(function(i,o){
		if(rstRtn != "") {
			rstRtn += "&";
		}
		if(key == o.split("=")[0]) {
			rstRtn += key +"="+ val;
		}
		else {
			rstRtn += o;
		}
	});
	return rstRtn;
};

/**
 * QueryString 형태의 문자열에서 입력받은 key를 찾아 value를 디코딩한 뒤 리턴
 * 
 * @param string str encoded QueryString 형태의 문자열 
 * @param string key 찾을 키
 */
var getValueInGetStringByKey = function(str, key) {
	if(typeof str == 'undefined') {
		return "";
	}
	var arrStr = str.split("&"),
		rstRtn = "";
	$(arrStr).each(function(i,o){
		if(o.split("=")[0] == key) {
			rstRtn = o.split("=")[1];
			return;
		}
	});
	rstRtn = typeof rstRtn == 'undefined' ? "" : rstRtn;
	// 디코딩 후 공백이 +로 표기되어 리턴되는 문제 해결
	return decodeURIComponent(decodeURI(rstRtn).replace(/\+/g, " "));
};


/**
 * 스크립트 실행시간 측정용
 * 
 */
var _exec = {
    _duration:0,
    begin: function(msg) { if(msg) console.log(msg); this._duration = new Date().getTime(); },
    end: function(msg) { if(!msg) msg=''; console.log(msg, '실행시간: '+ (new Date().getTime() - this._duration) +' ms'); },
};

/**
 * 입력값이 숫자면 통화표기 처리를 해서 리턴한다.
 * 예외적으로 입력값에 "("가 있으면 앞뒤 숫자 모두 통화처리한다.
 * eg) 2321(1821) => 2,321(1,821)
 *  
 * @param mixed val
 * @returns mixed
 */
function applyComma(val) {
	var rst = val;
	if(rst.indexOf("(") != -1) {// 상담유형인 경우, 총계(중복제거총계) 형식이다.
		var tmp = rst.split("(");
		var rst1 = tmp[0].trim();
		if(isFinite(rst1.trim())) { // 숫자(숫자) 형태인 경우
			rst1 = numberToCurrency(rst1);
		}
		var rst2 = tmp[1].substr(0, tmp[1].length-1),
      rst3 = '',
			rst2Percent = '';
    if(tmp.length > 2) {
      rst2 = tmp[1].substr(0, tmp[1].trim().indexOf(')'));
      rst3 = ' ('+ tmp[2];
    }
		if(rst2.indexOf('%') != -1) {
			rst2 = rst2.replace('%', '');
			if(isFinite(rst2.trim())) {
				rst2 = numberToCurrency(rst2.trim());
				rst2Percent = '%';
			}
		}
    else {
      if(isFinite(rst2.trim())) {
				rst2 = numberToCurrency(rst2.trim());
			}
    }
		rst = rst1 +" ("+ rst2 + rst2Percent +")" + rst3;
	}
	else {
		rst = isFinite(rst) ? numberToCurrency(rst) : rst;
	}
	return rst;
}