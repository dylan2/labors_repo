QueryString = function(str) {
	var str = str ? str : document.location.href;
	this.argv = new Array();
	this.queryString = str.replace(/^[^\?]+\?/, '').replace(/#(.*)$/, '');
	if (!this.queryString) this.queryString = '';
	var _argv = this.queryString.split('&');
	for(var i=0; i<_argv.length; i++) {
		var _key = _argv[i].substring(0, _argv[i].indexOf('='));
		var _val = _argv[i].substring(_argv[i].indexOf('=')+1);
		if(!_key || _argv[i].indexOf('=') == -1) continue;
		this.argv[_key] = _val;
	}
};
QueryString.prototype.setVar = function(key,val) {
	if (typeof key == 'object') {
		for (var item in key) this.argv[item] = key[item];
	} else {
		this.argv[key] = val;
	}
	return this.getVar();
};
QueryString.prototype.getVar = function(key) {
	if (key) {
		return this.argv[key] ? this.argv[key] : '';
	} else {
		var _item = new Array();
		for (var x in this.argv) {
			if (this.argv[x]) _item[_item.length] = x + '=' + this.argv[x];
			else continue;
		}
		return '?' + _item.join('&');
	}
};
Pagination = function(cfg) {
	this.config = {
		currentPage: 1,
		linkFunc: '',
		pageVariable: 'pno',
		numberFormat: '%n',
		showFirstLast: true,	// 맨처음, 맨 마지막으로 가는 링크를 만들것인가.
		thisPageStyle: 'font-weight:bold;',
		otherPageStyle: 'text-decoration:none',
		itemPerPage: 15,	// 리스트 목록수
		pagePerView: 10,	// 페이지당 네비게이션 항목수
		prevIcon: null,	// 이전페이지 아이콘
		nextIcon: null,	// 다음페이지 아이콘
		firstIcon: null,	// 첫페이지로 아이콘
		lastIcon: null,	// 마지막페이지 아이콘
		width: '100%', // paging 넓이
		height: 25, // paging 높이
		align: 'center' // paging horizontal 정렬
	}
	
	// mergi config object
	if(typeof cfg === 'object') {
		if(cfg.hasOwnProperty('total_item')) {
			this.totalItem = cfg['total_item'];
			delete(cfg['total_item']);
		}
		else {
			this.totalItem = 1;
		}
		for(key in cfg) {
			this.config[key] = cfg[key];
		}
	}
	
	this.qs = new QueryString;

	this.calculate = function() {
		this.totalPage = Math.ceil(this.totalItem / this.config.itemPerPage);
		// this.currentPage = this.qs.getVar(this.config.pageVariable);
		this.currentPage = this.config.currentPage;
		if (!this.currentPage) this.currentPage = 1;
		if (this.totalPage == 0) this.currentPage = this.totalPage; // <-- if (this.currentPage > this.totalPage) this.currentPage = this.totalPage;
		this.lastPageItems = this.totalPage % this.config.itemPerPage;

		this.prevPage = this.currentPage-1;
		this.nextPage = this.currentPage+1;
		this.seek = this.prevPage * this.config.itemPerPage;
		this.currentScale = parseInt(this.currentPage / this.config.pagePerView);
		if (this.currentPage % this.config.pagePerView < 1) this.currentScale--;
		this.totalScale = parseInt(this.totalPage / this.config.pagePerView);
		this.lastScalePages = this.totalPage % this.config.pagePerView;
		if (this.lastScalePages == 0) this.totalScale--;
		this.prevPage = this.currentScale * this.config.pagePerView;
		this.nextPage = this.prevPage + this.config.pagePerView + 1;
	}

	this.toString = function() {
		var ss, se;
		var firstBtn = '';
		var lastBtn = '';
		var prevBtn = '';
		var nextBtn = '';

		this.calculate();

		// 처음,마지막 페이지
		if (this.config.showFirstLast) {
			if (this.config.firstIcon) firstBtn = '<img src="'+this.config.firstIcon+'" border="0" align="absmiddle">';
			else firstBtn = '|◀ ';//'[처음]' 
			if (this.currentPage > 1) {
				if(this.config.linkFunc) {
					firstBtn = '<li class="prev"><a href="javascript:'+ this.config.linkFunc +'(1)">'+ firstBtn +'</a></li>';
				}
				else {
					firstBtn = firstBtn.link(this.qs.setVar(this.config.pageVariable,1));
				}
			}
			else {
				firstBtn = '<li class="prev"><span>'+ firstBtn +'</span></li>';
			}
			
			if (this.config.lastIcon) lastBtn = '<img src="'+this.config.lastIcon+'" border="0" align="absmiddle">';
			else lastBtn = ' ▶|';//'[마지막]' 
			if (this.totalPage > this.currentPage) {
				if(this.config.linkFunc) {
					lastBtn = '<li class="next"><a href="javascript:'+ this.config.linkFunc +'('+ (this.totalPage) +')">'+ lastBtn +'</a></li>';
				}
				else {
					lastBtn = lastBtn.link(this.qs.setVar(this.config.pageVariable,this.totalPage));
				}
			}
			else {
				lastBtn = '<li class="next"><span>'+ lastBtn +'</span></li>';
			}
		} else {
			firstBtn = lastBtn = '';
		}

		// 이전 페이지
		if (this.config.prevIcon) prevBtn ='<img src="'+this.config.prevIcon+'" border="0" align="absmiddle">';
		else prevBtn = '◀ '; 
		if (this.currentPage > 1) {
			if(this.config.linkFunc) {
				prevBtn = '<li class="prev"><a href="javascript:'+ this.config.linkFunc +'('+ ((this.currentPage/1)-1) +')">'+ prevBtn +'</a></li>';
			}
			else {
				prevBtn = prevBtn.link(this.qs.setVar(this.config.pageVariable,((this.currentPage/1)-1)));
			}
		}
		else {
			prevBtn = '<li class="prev"><span>'+ prevBtn +'</span></li>';
		}

		ss = this.prevPage + 1;
		if ((this.currentScale >= this.totalScale) && (this.lastScalePages != 0)) se = ss + this.lastScalePages;
		else if (this.currentScale <= -1) se = ss;
		else se = ss + this.config.pagePerView;

		// page 번호 링크 함수 처리
		var navBtn = '';
		for(var i = ss; i<se; i++) {
			var pageText = this.config.numberFormat.replace(/%n/g,i);
			if (i == this.currentPage) {
				_btn = '<li style="'+this.config.thisPageStyle+'" class="active" style="text-decoration:none !important;"><span>'+pageText+'</span></li> ';
			} else {
				if(this.config.linkFunc) {
					_btn = '<li><a href="javascript:'+ this.config.linkFunc +'('+i+')" style="'+this.config.otherPageStyle+'">'+pageText+'</a></li>';
				}
				else {
					_btn = '<li><a href="'+this.qs.setVar(this.config.pageVariable,i)+'" style="'+this.config.otherPageStyle+'">'+pageText+'</a></li>';
				}
			}
			navBtn += _btn;
		}

		// 다음 페이지
		if (this.config.prevIcon) nextBtn ='<img src="'+this.config.nextIcon+'" border="0" align="absmiddle">';
		else nextBtn = ' ▶';
		if (this.totalPage > this.currentPage) {
			if(this.config.linkFunc) {
				nextBtn = '<li class="next"><a href="javascript:'+ this.config.linkFunc +'('+ ((this.currentPage/1)+1) +')">'+ nextBtn +'</a></li>';
			}
			else {
				nextBtn = nextBtn.link(this.qs.setVar(this.config.pageVariable,((this.currentPage/1)+1)));
			}
		}
		else {
			nextBtn = '<li class="next"><span>'+ nextBtn +'</span></li>';
		}

		// 출력 설정
		var html = '';
		if(navBtn==''){
			html += '&nbsp;';
		}else{
			html += firstBtn +' '+ prevBtn +' '+ navBtn +' '+ nextBtn +' '+ lastBtn;
		}
		
		return html;
	}
};