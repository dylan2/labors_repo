
$(document).ready(function(){
	// 저장
	$('#frmNoti').submit(function(e){
		e.preventDefault();
		
		if($('#noti_title').val() == undefined || $('#noti_title').val() == '') {
			alert('제목을 입력하세요');
			$('#noti_title').focus();
			return false;
		}
		if($('#noti_contents').val() == undefined || $('#noti_contents').val() == '') {
			alert('내용을 입력하세요');
			$('#noti_contents').focus();
			return false;
		}
		
		var url = 'update_notice';
		if($('#noti_cid').val() == '') {
			url = 'insert_notice';
		}
		
		if(confirm('저장하시겠습니까?')) { 
			$.ajax({
				url: '/?c=admin&m='+ url
				, method: 'post'
				, data: $('#frmNoti').serialize()
				, cache: false
				, async: false
				, dataType: 'json'
				, success: function(data) {
					chkSession(data.data);
					
					if(data.rst == 'succ') {
						alert('저장되었습니다.');
					}
					else {
						alert('장애가 발생하였습니다.\n*'+ data.msg);
					}
					get_list(1);
					closeLayerPopup(gLayerId);
				}
				, error: function(data) {
					console.log('err = '+ data);
				}
			});
		}
	});
	
	// 삭제
	$('#btnDelSubmit').click(function(e){
		e.preventDefault();
		if(confirm('삭제하시겠습니까?')) {
			$.ajax({
				url: '/?c=admin&m=delete_notice'
				, method: 'post'
				, data: {cid: $('#noti_cid').val() }
				, cache: false
				, async: false
				, dataType: 'json'
				, success: function(data) {
					chkSession(data.data);
					
					if(data.rst == 'succ') {
						alert('삭제되었습니다.');
					}
					else {
						alert('장애가 발생하였습니다.\n*'+ data.msg);
					}
					get_list(1);
					closeLayerPopup(gLayerId);
				}
				, error: function(data) {
					console.log('err = '+ data);
				}
			});
		}
	});

	$('#noti_title').focus();
	
	// 삭제버튼
	if($('#noti_cid').val() == '') {
		$('#btnDelSubmit').hide();
		$('li.noti_column_10>div').css('width', 90);
	}
	else {
		$('#btnDelSubmit').show();
		$('li.noti_column_10>div').css('width', 180);
	}
});