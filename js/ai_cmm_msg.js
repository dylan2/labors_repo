/**
공통 메시지 정의
- 구분 :
msg type _ work kind _ index number
- eg) 
애러 : err_counsel_01
정보 : info_statistic_01
*/
var CFG_LOCALE_KOR = 'kor';
var CFG_LOCALE_ENG = 'eng';

// 해당 언어에 맞는 값을 대입하여야 한다.
var CFG_LOCALE = CFG_LOCALE_KOR;

var CFG_MSG = new Array(CFG_LOCALE_KOR, CFG_LOCALE_ENG);
CFG_MSG[CFG_LOCALE_KOR] = new Array();
CFG_MSG[CFG_LOCALE_ENG] = new Array();

// INFORMATION
// - common
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_01'] = '처리되었습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_02'] = '장애가 발생했습니다.\n잠시 후 다시 시도해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_03'] = '숫자를 입력하세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_04'] = '항목을 정확히 입력하세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_05'] = '수정하시겠습니까?';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_06'] = '저장하시겠습니까?';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_07'] = '이미 사용된 코드는 삭제할 수 없습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_08'] = '이미 사용된 데이터 x개를 제외하고 삭제하였습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_09'] = '접속 유효시간이 경과하였습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_10'] = '삭제하시겠습니까?';
CFG_MSG[CFG_LOCALE_KOR]['info_cmm_11'] = '검색어를 입력해 주세요.';
// - login
CFG_MSG[CFG_LOCALE_KOR]['info_login_01'] = '아이디를 입력하세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_login_02'] = '비밀번호를 입력하세요.';
// - subscribe operator
CFG_MSG[CFG_LOCALE_KOR]['info_subs_01'] = '이름을 입력하세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_02'] = '아이디 중복확인을 해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_03'] = '비밀번호가 일치하지 않습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_04'] = '휴대폰 번호를 정확하게 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_05'] = '연락처를 정확하게 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_06'] = '아이디가 중복됩니다.\n다른 아이디를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_07'] = '사용가능한 아이디입니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_08'] = '저장하지 못했습니다.\n비밀번호를 확인해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_09'] = '비밀번호가 일치하지 않습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_10'] = '운영자구분이 \"옴부즈만\", \"자치구공무원\"인 경우 소속자치구를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_subs_11'] = '비고는 100자까지 입력 가능합니다.';
// - auth
CFG_MSG[CFG_LOCALE_KOR]['info_auth_01'] = '권한이 없습니다.\n관리자에게 문의해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_auth_02'] = '잘못된 접근입니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_auth_03'] = '권한을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_auth_04'] = '하나 이상 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_auth_05'] = '소속을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_auth_06'] = '권한이 변경된 사용자는 다시 \"로그인\" 하여야 적용됩니다.';
// - counsel
CFG_MSG[CFG_LOCALE_KOR]['info_csl_01'] = '기관명을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_02'] = '상담일을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_03'] = '상담제목을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_04'] = '상담내용을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_05'] = '상담 답변을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_06'] = '상담 처리결과를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_07'] = '거주지 정보를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_08'] = '소재지 정보를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_09'] = '다른 상담글에서 참조한 글은 삭제할 수 없습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_10'] = '다른 상담글에서 참조한 글 x개를 제외하고 삭제하였습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_11'] = '내담자 성명을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_12'] = '내담자 연락처를 정확히 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_13'] = '고용형태를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_14'] = '상담유형을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_15'] = '주제어를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_16'] = '상담유형은 3개까지 선택할 수 있습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_17'] = '성별을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_18'] = '처리결과의 기타를 선택한 경우 검색어를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_19'] = '상담방법을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_20'] = '연령대를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_21'] = '숫자와 콤마, 점만 입력 가능합니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_22'] = '접근 권한이 없습니다.\n관리자에게 문의해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_23'] = '상담이용동기를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_csl_24'] = '주당 근로시간을 정확히 입력해 주세요.';
// - board
CFG_MSG[CFG_LOCALE_KOR]['info_board_01'] = '공지글로 등록할 경우 게시 기간을 선택하셔야 합니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_board_02'] = '유형을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_board_03'] = '제목을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_board_04'] = '내용을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_board_05'] = '선택한 파일이 기존 파일명과 같습니다.\n파일이 다르더라도 파일명이 같으면 업로드되지 않습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_board_06'] = '업로드 가능 개수는 x개 입니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_board_07'] = '업로드 가능한 용량을 초과했습니다.\n-선택한 파일 용량 : xMB';
CFG_MSG[CFG_LOCALE_KOR]['info_board_08'] = '업로드 가능한 파일 종류가 아닙니다.';
// - statistic
CFG_MSG[CFG_LOCALE_KOR]['info_sttc_01'] = '다운로드할 내용이 없습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_sttc_02'] = '선택된 상담자가 없습니다. \n하나 이상 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_sttc_03'] = '검색 결과가 없습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_sttc_04'] = '교차통계는 차트보기 기능을 지원하지 않습니다.';
CFG_MSG[CFG_LOCALE_KOR]['info_sttc_05'] = '통계유형이 변경되었습니다. 검색버튼을 눌러주세요.';

// - operator
CFG_MSG[CFG_LOCALE_KOR]['info_oper_01'] = '변경하시겠습니까?';
CFG_MSG[CFG_LOCALE_KOR]['info_oper_02'] = '현재 비밀번호를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_oper_03'] = '변경할 비밀번호를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_oper_04'] = '비밀번호를 한번 더 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_oper_05'] = '현재 비밀번호가 일치하지 않습니다.\n다시 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_oper_06'] = '마스터 계정 전용입니다. 해당 권한은 모든 권한이 있습니다. 계속하시겠습니까?';

// - lawhelp
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_01'] = '사건유형를 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_02'] = '지원승인일을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_03'] = '신청자를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_04'] = '거주지를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_05'] = '회사를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_06'] = '회사소재지를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_07'] = '신청기관을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_08'] = '대리인-소속을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_09'] = '대리인-이름을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_10'] = '대상기관을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_11'] = '권리구제지원내용을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_12'] = '접수번호를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_13'] = '출석일을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_14'] = '접수번호가 중복됩니다.\n다른 번호를 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_15'] = '성별을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_16'] = '권리구제 유형을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_lawhelp_17'] = '권리구제 세부유형을 선택해 주세요.';


// code
CFG_MSG[CFG_LOCALE_KOR]['info_code_01'] = '권리구제 유형을 선택해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_code_02'] = '순번을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_code_03'] = '유형명을 입력해 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['info_code_04'] = '사용여부를 선택해 주세요.';

// 사용자 상담
CFG_MSG[CFG_LOCALE_KOR]['info_biz_csl_01'] = '사업장명을 입력해 주세요.';


// WARNING
// - code manage
// - common
CFG_MSG[CFG_LOCALE_KOR]['warn_cmm_01'] = '삭제하면 데이터를 복구할 수 없습니다.\n삭제하시겠습니까?';
CFG_MSG[CFG_LOCALE_KOR]['warn_cmm_02'] = '게시물 수정과 상관없이 삭제됩니다.\n삭제하시겠습니까?';

// - board
CFG_MSG[CFG_LOCALE_KOR]['warn_board_01'] = '파일이 존재하지 않습니다.';
CFG_MSG[CFG_LOCALE_KOR]['warn_board_02'] = '본인이 작성한 글만 수정할 수 있습니다.';



// ERROR
// - login
CFG_MSG[CFG_LOCALE_KOR]['err_login_01'] = '인증에 실패하였습니다.\n다시 시도하여 주세요.';
CFG_MSG[CFG_LOCALE_KOR]['err_login_02'] = '인증에 실패하였습니다.\n사용 중지된 계정입니다.'
	CFG_MSG[CFG_LOCALE_KOR]['err_login_03'] = '인증에 실패하였습니다.\n잘못된 요청입니다.'

// - operator
CFG_MSG[CFG_LOCALE_KOR]['err_oper_01'] = '해당 계정이 작성한 글이 있으면 삭제할 수 없습니다.';
CFG_MSG[CFG_LOCALE_KOR]['err_oper_02'] = '자신의 계정은 사용,중지,삭제 처리할 수 없습니다.';