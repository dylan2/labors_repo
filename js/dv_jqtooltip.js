
$(document).ready(function(){

	// jquery tooltip - 태그에 title 에 지정된 텍스트가 대상임
	$(this).tooltip({
		track: true
		,position: {
			my: "center bottom-10",
			at: "center top",
			using: function( position, feedback ) {
				
				// 툴팁의 top, bottom이 화면에 가려질 경우 처리
				if(position.top < 0 || $('#list').offset().top + position.top + $(this).height() > window.screen.height) {
					position.top = 10;
					$(this).css({
						width: 1000
						,minWidth: 1000
					});
				}

				$(this).css(position).css({
					background: '#fff'
					,zIndex: 999999
				});//.css('box-shadow', '0 0 3px black');

			}
		}
    });

});
