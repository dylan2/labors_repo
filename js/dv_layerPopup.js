/****************************************************************
 * 
 * 파일명 : layerPopup.js
 * 설  명 : 공통 layer popup 기능 JavaScript
 *
 * **************************************************************/

/**
 * Layer Modal Popup 
 * @param url : url link
 * @param w : width
 * @param h : height 
 * @param title : popup title
 * @param prepareCallback : 팝업 오픈 이벤트 콜백 함수 
 * @param loadedCallback : 문서 로딩 완료 이벤트 콜백 함수 
 * @param closedCallback : 팝업 닫기 이벤트 콜백 함수 
 * @param ltype : 1->url , 2->html ,  공백이나  null일 경우는 url
 * @param useExternalTitle : 컨텐츠 페이지 타이틀 사용유무, true인경우 layerpopup의 타이틀 제거
 * @param css : css 객체 eg) css = {width:100%,height:100%,...}
 * @param isModal : 모달 팝업 여부 1:모달,0:모달리스, 기본:모달
 * @param isNotice : 공지용(메인) 팝업 여부 1:공지용,0:일반, 기본:일반 - 공지용인 경우 배경 투명도를 0으로 설정
 */
// fadeIn milliseconds
var __layer_fadeMs = 150;
function openLayerModalPopup(url, w, h, title, prepareCallback, loadedCallback, closedCallback, ltype, useExternalTitle, css, isModal, isNotice) {

	isModal = isModal || isModal == undefined ? true : false;

	// 닫기 버튼 이미지 경로
	var _btnClosePath = './images/layerpopup/btnLayerPopupClose.png';
	
	if(!url) {
		alert('url 정보가 없습니다.');
		return;
	}
	
	// prevent cache
	var dele = '?';
	if(url.lastIndexOf('&') != -1) {
		dele = '&';
	}
	url += dele +'rnd='+ Math.random();
	
	// layerPopup 크기 설정
	if(w == undefined) {
		w = '400px'; // default size
	}
	else {
		// if(w.toString().toLowerCase().indexOf('px') != -1) {
			// w = w.toString().toLowerCase().replace('px')[0];
		if(w.toString().toLowerCase().indexOf('%') == -1 && w.toString().toLowerCase().indexOf('px') == -1) {
			w = w +'px';
		}
	}
	if(h == undefined) {
		h = '300'; // default size
	}
	else {
		if(h.toString().toLowerCase().indexOf('px') != -1) h = h.toString().toLowerCase().replace('px')[0];
	}
	
	// title
	if( ! title) {
		title = '';
	}
	// loadedCallback
	if( ! loadedCallback) {
		loadedCallback = function(){};
	}

	// isNotice
	isNotice = isNotice == undefined ? false : true;
	var bgOpacity = 0.1;
	var bgShadow = '0 5px 60px #000';
	if(isNotice) {
		bgOpacity = 0;
		bgShadow = '0 1px 2px #000';
	}

	//var deck = _isparent && _isparent == 'true' ? $('body') : top.window.document.body;
	var deck = window.document.body;
	
	// layerpopup data stack
	if(!deck.layerPopupStack) {
		deck.layerPopupStack = [];
	}
	// layerpopup data object
	deck.layerPopup =  {
		id: '', // layerId
		layer: undefined,
		layerBg: undefined
	};
	var len = $(deck).find('>div.lyr_wrap').length;
	var layerId = 'layer_'+ len;
	
	// setup Background
	var thisLayerBg = $('<div class="lyr_bg" id="lyr_bg_'+ layerId +
		'" style="display:none;position:fixed;top:0;left:0;width:100%;height:100%;background:#000;opacity:'+ bgOpacity +';filter:alpha(opacity='+ bgOpacity +');-ms-filter:alpha(opacity='+ bgOpacity +');z-index:10000;" >').appendTo(deck);
	
	// 배경에 가려지지 않는 객체가 존재할 경우 가리기 용도(activeX 등의 object)
	$('<iframe id="lyr_bg_fra_'+ layerId +
		'" style="position:fixed;top:0;left:0;width:100%;height:100%;background:#000;opacity:'+ bgOpacity +';filter:alpha(opacity='+ bgOpacity +');-ms-filter:alpha(opacity='+ bgOpacity +');z-index:10000;"></iframe>').appendTo(thisLayerBg);
	
	// 배경 클릭시 닫기 기능 수행용 레이어
	var thisLayerEventListener = $('<div class="lyr_bg2" id="lyr_bg2_'+ layerId +
		'" style="position:fixed;top:0;left:0;width:100%;height:100%;z-index:10000;" >').appendTo(thisLayerBg);
	$(thisLayerEventListener).click(function(){
		// 모달인 경우, 닫기 방지, 모달리스인 경우만 닫히게 한다.
		if( !isModal ) {
			closeLayerPopup();
		}
	});
	
	// Fade in Background
	$(thisLayerBg).css({'filter':'alpha(opacity='+ bgOpacity +');-ms-filter:alpha(opacity='+ bgOpacity +');'}).fadeIn(__layer_fadeMs);

	// close button html
	var close_button_html = '<a class="close" style="cursor:pointer;width:28px;height:28px;float:right;background-image:url('+_btnClosePath +');background-repeat:no-repeat;" title="" onclick="closeLayerPopup(\''+ layerId +'\')"><!--닫기--></a>';
	
	// setup modalLayer
	var thisLayer = $('<div class="lyr_wrap" id="'+ layerId +
		'" style="padding:10px; display:none; position:fixed; top:50%; left:50%; float:left; background:'+ (css && css.background ? css.background : '#fff') +'; overflow:hidden; box-shadow:'+ bgShadow +'; z-index:10000;">'+
		'<div class="lyr_bo">'+
		'<div class="pop_wrap">'+
			'<div class="pop_header" style="height:50px;width:100%;cursor:move;"><span id="ly_title" style="float:left;margin:10px 0 30px 20px;padding-bottom:5px;border-bottom-style:solid;border-width:3px;font-size:2em;font-weight:bold;">'+ 
				title +'</span>&nbsp;<!-- //메인 타이틀 -->'+ close_button_html +'</div><!-- //header -->'+
			'<div class="lyr_modal" style="width:'+ w +'; height:'+ h +'px;overflow-y:hidden;padding:0 10px 0 10px;clear:both;">'+
				'<div class="container">로딩중입니다.</div>'+// 2019.07.06 팝업 오픈 후 문서로딩 전까지 로딩중 문구 추가
					'<div class="indicator" style="position: absolute;height: 20px;text-align: center;top: 0;bottom: 0;left: 0;right: 0;margin: auto;font-weight: bold;font-size: 14px;">로딩중입니다.</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'</div>'+
		'</div>').appendTo(deck);

	// drag area id
	var dragHanddle = 'div.pop_header';

	// 자체 타이틀 처리
	if(useExternalTitle) {
		$('div.pop_header').remove();
		$('div.lyr_wrap').css('padding', 0);
		$('div.lyr_modal').css('padding', 0);
		// 드레그 존 추가
		$(thisLayer).prepend('<div class="pop_drag_pannel" style="position:absolute;left:0px;top:0px;height:50px;width:100%;cursor:move;">'+close_button_html+'</div>');
		$('a.close').css({margin:'15px 15px 0 0'});
		dragHanddle = 'div.pop_drag_pannel';
	}
	// dragging
	$(thisLayer).draggable({handle: dragHanddle, opacity:bgOpacity});
	
	// 제목이 없는 경우 블렛을 숨긴다.
	if(title == '') {
		$('#'+ layerId +' #ly_title').hide();
	}
	
	// 2019.07.06 팝업 오픈 후 문서로딩 전까지 로딩중 문구 추가
	var innerLoadedCallback = function(){
		$('.lyr_modal .indicator').fadeOut(100);//로딩중 div 숨김
		loadedCallback();
	};
	//type 별로 레이어 사용할수 있도록 수정 20140820 kimmj
	if(ltype == "2") {
		$('#'+ layerId +' .container').html(url, innerLoadedCallback); //html
	}
	else {
		$('#'+ layerId +' .container').empty();
		$('#'+ layerId +' .container').load(url, innerLoadedCallback); //loading url
	}

	// 레이어팝업 id 배열
	deck.layerPopup.id 		= layerId;
	deck.layerPopup.layer 	= thisLayer;
	deck.layerPopup.layerBg = thisLayerBg;
	deck.layerPopupStack.push(deck.layerPopup);
	
	// closedCallback 등록 - 함수외 object형 추가
	if(closedCallback && typeof deck.layerPopup.layer.closedCallback === 'undefined' && (typeof closedCallback === 'function' || typeof closedCallback === 'object')) {
		deck.layerPopup.layer.closedCallback = closedCallback;
	}
	
	//Fade in the Popup
	$(thisLayer).fadeIn(__layer_fadeMs, function(){
		if(typeof prepareCallback == 'function') {
			eval('prepareCallback()');
		}
	});
	
	//Define margin for center alignment (vertical + horizontal) - we add 80 to the height/width to accomodate for the padding + border width defined in the css
	var popMargTop  = thisLayer[0].offsetHeight / 2;
	var popMargLeft = thisLayer[0].offsetWidth / 2;

	// set position to container 
	$(thisLayer).css({ 
		'margin-top'  :-popMargTop, 
		'margin-left' :-popMargLeft
	});
	
	// Set focus this layer
	$('#lyr_bg_fra_'+ layerId).focus();
	// body 태그의 스크롤바로인한 화면 덜컥거림 방지
	try {
		if($('body').hasScrollBar()) {
			$('body').css('overflow-y', 'scroll').css('position', 'fixed');
		}
	}
	catch(e){}

	// reloadLayerPopupById 함수를 위해 추가된 코드
	return deck.layerPopup.id;
}

/**
 * Reload document on latest LayerPopup
 * @param id : latest LayerPopup id
 * @param url : document url
 * @param loadedCallback : callback function
 */
function reloadLayerPopupById(id, url, loadedCallback) {
	if(id) {
		$('#'+ id +' .container').load(url, loadedCallback);
	}
}

/**
 * Close Popups and Fade Layer
 * @param id : close target id
 * @param rtnObj : 팝업 호출시 close콜백을 등록한 경우 해당 콜백으로 데이터를 전달할 때 사용
 */
function closeLayerPopup() {
	var layer = undefined;
	var layerBg = undefined;
	var deck = window.document.body;

	$('body').css('overflow-y', 'auto').css('position', 'static');

	// aguments 세팅
	var id = undefined;
	var rtnObj = undefined;
	if(typeof arguments[0] === 'object') {
		rtnObj = arguments[0];
	}
	else if(typeof arguments[1] === 'object') {
		rtnObj = arguments[1];
		id = arguments[0] ? arguments[0] : undefined;
	}
	
	// console.log(id);
	// console.log(typeof arguments[0]);
	
	// 잘못 호출되는 문제에 대한 처리 
	// - 찾아내지 못한 문제 : 
	// 1. 일자 : 2015.07.03, 상태 : 미결
	//     폼 submit 후 closeLayerPopup 함수 호출시 하단 "if(deck.layerPopupStack && deck.layerPopupStack.length > 0) {" 이후 부분이 두번 호출되는 문제가 발견되었다. 
	if( ! deck.layerPopupStack ) {
		return;
	}
	
	// id가 없으면, 직전 레이어팝업을 제거한다.
	if(!id) {
		var index = deck.layerPopupStack && deck.layerPopupStack.length ? deck.layerPopupStack.length-1 : 0;
		id = deck.layerPopupStack[index].id;
	}

	for(var i = 0; i < deck.layerPopupStack.length; i++) {
		if(deck.layerPopupStack[i].id == id) {
			layer = deck.layerPopupStack[i].layer;
			layerBg = deck.layerPopupStack[i].layerBg;
		}
	}
	if(typeof layer === 'undefined' || typeof layerBg === 'undefined') return;
	
	// remove all layer objects
	$.map([layer, layerBg], function(obj, i) {
		$(obj).fadeOut(__layer_fadeMs, function() {
			//alert('remove ok '+ this.id);
			// obj가 layer라면 콜백함수 실행
			if(this.id.indexOf('lyr_bg_') == -1) {
				// execute callback function
				if(typeof layer.closedCallback === 'function') {
					layer.closedCallback(rtnObj);
				}
				else if(typeof layer.closedCallback === 'object') {
					layer.closedCallback.exec(layer.closedCallback.param);
					layer.closedCallback.param = '';
					layer.closedCallback.exec = '';
				}
				delete layer.closedCallback;
			}
			$(this).remove();
		});
	});
	
	// 레이어팝업 id 배열에서 제거
	if(deck.layerPopupStack && deck.layerPopupStack.length > 0) {
		var objRemove = deck.layerPopupStack.pop();

		objRemove.id = null;
		objRemove.layer = null;
		objRemove.layerBg = null;
		objRemove = null;
		
		if(deck.layerPopupStack.length <= 0) {
			deck.layerPopup = null;
			deck.layerPopupStack = null;
		}
	}
}

/**
 * Close All LayerPopup
 * @returns empty
 */
function closeAllLayerPopup() {
	var deck = window.document.body;
	if(deck.layerPopupStack && deck.layerPopupStack.length > 0) {
		for(var i = deck.layerPopupStack.length - 1; i >= 0; i--) {
			closeLayerPopup(deck.layerPopupStack[i].id);
		}
	}
}

/**
 * 첫번째 생성된 레이어팝업인지 여부
 * @returns {Boolean}
 */
function isFirstModalLayer() {
	var len = getLayerPopupLength();
	return len == 0;
}

/**
 * 레이어팝업이 보여지는(하나 이상 생성된) 상태인지 여부 값
 * @returns {boolean}
 */
function isModalLayerMode() {
	var len = getLayerPopupLength();
	return len != 0;
}

/**
 * 생성되어 있는 레이어팝업 개수
 * @returns {int}
 */
function getLayerPopupLength() {
	var deck = window.document.body;
	if(deck.layerPopupStack) {
		return deck.layerPopupStack.length;
	}
	else {
		return 0;
	}
}


/* 
 * body에 y 스크롤바가 있는지 체크하는 플러그인 
 * - tested working on Firefox, Chrome, IE6,7,8
 */
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);