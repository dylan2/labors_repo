/**
 * 
 * 검색영역 : 쿠키에 저장된 소속,상담방법,처리결과 검색정보를 가져와 세팅한다.
 *  
 */
$(document).ready(function(){
	
	var schData = $.cookie("laborsSearchDataStatis");
	// - 상담일 검색
	var schDateBegin = getValueInGetStringByKey(schData, "search_date_begin");
	$('input[name=search_date_begin]').val(schDateBegin);
	var schDateEnd = getValueInGetStringByKey(schData, "search_date_end");
	$('input[name=search_date_end]').val(schDateEnd);
	if(!schDateBegin && !schDateEnd) {
		// page_id는 상담사례 통계에서만 사용한다.(당월로 세팅, 나머지 통계는 당해로 세팅)
		fill_list_search_dates('', typeof page_id != 'undefined' && page_id==1 ? 1 : 2);
	}
	// - 년월 selectbox
	var schSelYear = getValueInGetStringByKey(schData, "sel_year");
	$('select[name=sel_year] option[value="'+ schSelYear +'"]').prop('selected', true);
	var schSelMonth = getValueInGetStringByKey(schData, "sel_month");
	$('select[name=sel_month] option[value="'+ schSelMonth +'"]').prop('selected', true);
	if(!schSelYear && !schSelMonth) {
		$('#sel_year').find('option:eq(0)').prop('selected', true);
		$('#sel_month').find('option:eq(0)').prop('selected', true);
	}
	// - 소속
	var assoCode = getValueInGetStringByKey(schData, "asso_code");
	var assoCodeNm = getValueInGetStringByKey(schData, "asso_code_name");
	if(assoCode) {
		$('input[name="asso_code"]').val(assoCode);
		$('input[name=asso_code_name]').val(assoCodeNm);
		$('#asso_code_name').text(assoCodeNm);
	}
	else {
		$('#asso_code_name').text("전체");
	}
	// - 상담방법
	var sCode = getValueInGetStringByKey(schData, "s_code");
	$('input[name="s_code"]').val( sCode );
	var sCodeNm = getValueInGetStringByKey(schData, "s_code_name");
	$('input[name=s_code_name]').val(sCodeNm);
	if(sCodeNm == '') {
		$('#s_code_name').text("전체");
	}
	else {
		$('#s_code_name').text(sCodeNm);
	}
	// - 처리결과
	var cslProcRst = getValueInGetStringByKey(schData, "csl_proc_rst");
	$('input[name="csl_proc_rst"]').val( cslProcRst );
	var cslProcRstNm = getValueInGetStringByKey(schData, "csl_proc_rst_name");
	$('input[name=csl_proc_rst_name]').val(cslProcRstNm);
	if(cslProcRstNm == '') {
		$('#csl_proc_rst_name').text("전체");
	}
	else {
		$('#csl_proc_rst_name').text(cslProcRstNm);
	}
	
});
/**
 * 
 * 검색영역 : 끝
 *  
 */