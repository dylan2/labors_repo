<%
Server.ScriptTimeOut = 30 * 60 '30분

Response.ContentType = "text/html;charset=EUC-KR"
Response.Expires = 0
Response.AddHeader "Pragma", "no-cache"
Response.AddHeader "Cache-Control", "no-store"

Dim FileName : FileName= Request.ServerVariables("HTTP_FILE_NAME")
Dim FileNameExt : FileNameExt = LCase(Mid(FileName, InstrRev(FileName, ".") + 1))

''' 이미지 파일 검사 (bmp|gif|jpg|jpeg|png)
If Not (StrComp(FileNameExt,"bmp") = 0 Or StrComp(FileNameExt,"gif") = 0 Or StrComp(FileNameExt,"jpg") = 0  Or StrComp(FileNameExt,"jpeg") = 0  Or StrComp(FileNameExt,"png") = 0) Then
	Response.End
End IF

Dim temp : temp = Request.TotalBytes
Dim BytesRead : BytesRead=0
Dim PartSize : PartSize=0

Dim myStream : Set myStream = CreateObject("ADODB.Stream")
myStream.Type = 1 ' binary
myStream.Mode = 3
myStream.Open

Do While BytesRead < temp
	PartSize = 64*1024   '64KB

	If PartSize + BytesRead > temp Then
		PartSize = temp - BytesRead
	End if

	DataPart = Request.BinaryRead(PartSize)
	BytesRead = BytesRead + PartSize
	myStream.Write(DataPart)
Loop

myStream.SaveToFile("D:\test\img_upload\" & FileName)
myStream.Close : Set myStream = Nothing

Dim sFileInfo : sFileInfo = "&bNewLine=true"
sFileInfo = sFileInfo & "&sFileName=" & FileName
sFileInfo = sFileInfo & "&sFileURL=/pds/test/img_upload/" & FileName
Response.Write sFileInfo
Response.End
%>