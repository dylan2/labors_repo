<?php
include_once "file_upload_const.php";


// default redirection
$url = $_REQUEST["callback"].'?callback_func='.$_REQUEST["callback_func"];


$bSuccessUpload = is_uploaded_file($_FILES['Filedata']['tmp_name']);

// SUCCESSFUL
if($bSuccessUpload) {
	$tmp_name = $_FILES['Filedata']['tmp_name'];
	$name = $_FILES['Filedata']['name'];
	
	//  아래 원본 코드가 오류가 나서 수정
	// $filename_ext = strtolower(array_pop(explode('.',$name)));
	
	// 수정 시작
	$exp = explode('.',$name);
	$tmp = array_pop($exp);
	$ext = strtolower($tmp);
	// 수정 끝
	
	$allow_file = array("bmp","gif","jpg","jpeg","png");
	
	if(!in_array($ext, $allow_file)) {
		$url .= '&errstr='.$name;
	} else {
		if(!is_dir(CFG_SE2_UPLOAD_DIR)){
			mkdir(CFG_SE2_UPLOAD_DIR, 0777);
		}
		
		// $newPath = CFG_SE2_UPLOAD_DIR. urlencode($_FILES['Filedata']['name']);
		$file_name = md5($name . microtime(true));
		$file_name = 'se2_'. $file_name .'.'. $ext;
		$newPath = CFG_SE2_UPLOAD_DIR. $file_name;
		
		@move_uploaded_file($tmp_name, $newPath);
		
		$url .= "&bNewLine=true";
		// $url .= "&sFileName=".urlencode(urlencode($name));
		// $url .= "&sFileURL=". CFG_SE2_UPLOAD_DIR . urlencode(urlencode($name));
		$url .= "&sFileName=". $file_name;
		$url .= "&sFileURL=". CFG_SE2_UPLOAD_PATH . $file_name;
	}
}
// FAILED
else {
	$url .= '&errstr=error';
}
	
header('Location: '. $url);
?>